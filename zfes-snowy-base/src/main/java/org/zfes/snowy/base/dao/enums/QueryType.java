/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.dao.enums;

public enum QueryType {
	S_O_C,//SQL_OFFSET_COUNT
	S_O_NC,//SQL_OFFSET_NOCOUNT
	S_NO_C,//SQL_NOOFFSET_COUNT
	S_NO_NC,//SQL_NOOFFSET_COUNT
	H_O_C,//HQL_OFFSET_COUNT
	H_O_NC,//HQL_OFFSET_NOCOUNT
	H_NO_C,//HQL_NOOFFSET_COUNT
	H_NO_NC,//HQL_NOOFFSET_COUNT
}
