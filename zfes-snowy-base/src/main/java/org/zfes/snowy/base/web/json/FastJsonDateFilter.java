/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.web.json;

import java.util.Date;

import org.zfes.snowy.core.util.ZDateUtil;
import org.zfes.snowy.core.util.datex.ZDateStyle;

import com.alibaba.fastjson.serializer.ValueFilter;


public class FastJsonDateFilter implements ValueFilter{

	@Override
	public Object process(Object object, String name, Object value) {
		if(value==null){return value;}
		if(value instanceof java.util.Date){
			return ZDateUtil.toString((Date)value, ZDateStyle.YYYY_MM_DD);
		}
		if(value instanceof java.sql.Timestamp){
			return ZDateUtil.toString((Date)value, ZDateStyle.YYYY_MM_DD_HH_MM_SS);
		}
		return value;
	}

}
