/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.dao.enums;

public enum DataBaseType {
	
	MySQL(" select * from ( {0}) zht_$table limit {1},{2}"),
	Oracle(" select * from (select zht_$table.*,rownum as kmzht_$rownum from ({0}) zht_$table where rownum <= {1} ) where kmzht_$rownum>{2} "),
	SQLServer( "select * from ( select row_number() over(order by tempColumn) tempRowNumber, * from (select top {1} tempColumn = 0, {0}) t ) tt where tempRowNumber > {2} "),
	PostgreSQL( "select * from ( {0}) zht_$table limit {2} offset {1} "),
	Sqlite( " select * from ( {0}) zht_$table limit {2} offset {1} ");
	//http://blog.csdn.net/weoln/article/details/3466006
	private String value;
	 DataBaseType(String value) {
		this.value = value;
	}
	public String getValue() {
		return new String(value);
	}
}
