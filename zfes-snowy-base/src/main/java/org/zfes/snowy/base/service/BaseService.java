/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.zfes.snowy.base.dao.jdbc.ISnowyJdbcBaseDao;

public class BaseService {
	
	@Autowired
	protected ISnowyJdbcBaseDao jdbcDao;

	protected ISnowyJdbcBaseDao getJdbcBaseDao() {
		return jdbcDao;
	}
}
