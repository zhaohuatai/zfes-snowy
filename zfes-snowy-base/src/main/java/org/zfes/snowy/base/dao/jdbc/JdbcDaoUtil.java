/**
 * Copyright (c) 2017 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.dao.jdbc;


import java.util.Optional;


import org.zfes.snowy.base.dao.enums.DataBaseType;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.consts.FrameConst;

public class JdbcDaoUtil {
	
	public static Object[] genSQL(DataBaseType dbType,ParamMap params){
		Optional<String> start= params.getStr(FrameConst.PAGE_OFFSET.START);
		Optional<String>   end= params.getStr(FrameConst.PAGE_OFFSET.END);
		Optional<String>  size= params.getStr(FrameConst.PAGE_OFFSET.SIZE);
		Object[] formatParam = new Object[3];
		switch (dbType) {
		case MySQL:
			formatParam[0]=dbType.getValue();
			formatParam[1]=start.get();
			formatParam[2]=size.get();
			break;
		case Oracle:
			formatParam[0]=dbType.getValue();
			formatParam[1]=end.get();
			formatParam[2]=start.get();
			break;
		case PostgreSQL:
			formatParam[0]=dbType.getValue();
			formatParam[1]=start.get();
			formatParam[2]=size.get();
			break;
		case Sqlite:
			formatParam[0]=dbType.getValue();
			formatParam[1]=start.get();
			formatParam[2]=size.get();
			break;
		case SQLServer:
			formatParam[0]=dbType.getValue();
			formatParam[1]=start.get();
			formatParam[2]=end.get();
			break;
		default:
			formatParam[0]=dbType.getValue();
			formatParam[1]=start.get();
			formatParam[2]=size.get();
			break;
		}
		return formatParam;
	}
	
	
}
