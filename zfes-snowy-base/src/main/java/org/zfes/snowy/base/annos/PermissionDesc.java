package org.zfes.snowy.base.annos;

public @interface  PermissionDesc {
	String value();
}
