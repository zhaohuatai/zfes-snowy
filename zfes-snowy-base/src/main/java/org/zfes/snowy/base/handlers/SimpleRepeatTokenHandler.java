/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.base.handlers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.JSONMSG;
import org.zfes.snowy.core.util.ZWebUtil;

public class SimpleRepeatTokenHandler  {
	
	private String defaultRepeatDirectUrl="/303.html";
	
	
	public void onRepeatSubmit(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException {
		if(ZWebUtil.isAjax(request)){
			ZWebUtil.respJSON(response, JSONMSG.newMSG(FrameConst.SC.SC_303, "请不要重复提交"));
			return;
		}
		ZWebUtil.redirect(request, response,defaultRepeatDirectUrl);
	}

	public String getDefaultRepeatDirectUrl() {
		return defaultRepeatDirectUrl;
	}

	public void setDefaultRepeatDirectUrl(String defaultRepeatDirectUrl) {
		this.defaultRepeatDirectUrl = defaultRepeatDirectUrl;
	}
	
}
