/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.base.dao.mybatis;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface BaseMapper<T>{
	
	T selectByPrimaryKey(Long id);
	
    int deleteByPrimaryKey(Long id);

    int insertSelective(T record);
    
    int insert(T record);

    int updateByPrimaryKeySelective(T record);
    
    int update(T record);
}
