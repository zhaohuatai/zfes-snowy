///**
// * Copyright (c) 2016-2020 https://github.com/zhaohuatai
// *
// * contact z_huatai@qq.com
// *  
// */
//package org.zfes.snowy.base.web.file;
//
//import java.io.File;
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.math.BigInteger;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.commons.fileupload.FileUploadBase;
//import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
//import org.apache.commons.io.FilenameUtils;
//import org.apache.commons.lang3.time.DateFormatUtils;
//import org.springframework.web.multipart.MultipartFile;
//import org.zfes.snowy.core.util.ZAlert;
//import org.zfes.snowy.core.util.ZStrUtil;
//import org.zfes.snowy.core.util.encypt.MD5Util;
//
//public class FileUploadUtils {
//	
//	public static final long DEFAULT_MAX_SIZE = 52428800 * 2 * 10 * 100;
//	
//	// 默认上传的地址
//	private static String defaultBaseDir = "upload";
//	
//	private static String rootDir = "";
//	
//	// 默认的文件名最大长度
//	public static final int DEFAULT_FILE_NAME_LENGTH = 256;
//	
//	public static final String[] IMAGE_EXTENSION = { "bmp", "gif", "jpg", "jpeg", "png" };
//	
//	public static final String[] FLASH_EXTENSION = { "swf", "flv" };
//	public static final String[] DOCX_EXTENSION = { "doc", "docx", "xls", "xlsx", "ppt", "pptx", "pdf" };
//	
//	public static final String[] MEDIA_EXTENSION = { "swf", "flv", "mp3", "wav", "wma", "wmv", "mid", "avi", "mpg", "asf",
//			"rm", "rmvb" };
//	
//	public static final String[] DEFAULT_ALLOWED_EXTENSION = {
//			// 图片
//			"bmp", "gif", "jpg", "jpeg", "png",
//			// word excel powerpoint
//			"doc", "docx", "xls", "xlsx", "ppt", "pptx",
//			
//			"html", "htm", "txt",
//			// 压缩文件
//			"rar", "zip", "gz", "bz2",
//			// pdf
//			"pdf" };
//	
//	private static int counter = 0;
//	
//	public static void setDefaultBaseDir(String defaultBaseDir) {
//		FileUploadUtils.defaultBaseDir = defaultBaseDir;
//	}
//	
//	public static String getDefaultBaseDir() {
//		return defaultBaseDir;
//	}
//	
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：文件上传
//	 * @param file 上传的文件
//	 * @param appKey 来源系统appkey
//	 * @param opraType 系统对应的子业务
//	 * @return
//	 * 2016年9月19日 下午5:35:42
//	 */
//	public static final Map<String, Object> upload(MultipartFile file,String appKey,String opraType) {
//		return upload(file,appKey+File.separator+opraType);
//	}
//	
//	//public static final Map<String, Object> uploadMongoDB(MultipartFile file,BizMode bizMode){
//		
//	//}
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：文件上传
//	 * @param file 上传的文件
//	 * @param baseDir 基础路径，来源系统/来源业务,前后无斜杠
//	 * @return
//	 * 2016年9月19日 下午5:34:04
//	 */
//	public static final Map<String, Object> upload(MultipartFile file,String baseDir) {
//		try {
//			return upload(sysParamUploadDir(), baseDir, file, true);
//		} catch (IOException e) {
//			ZAlert.Error("upload.server.error");
//		} catch (InvalidExtensionException.InvalidImageExtensionException e) {
//			ZAlert.Error("upload.not.allow.image.extension");
//		} catch (InvalidExtensionException.InvalidFlashExtensionException e) {
//			ZAlert.Error("upload.not.allow.flash.extension");
//		} catch (InvalidExtensionException.InvalidMediaExtensionException e) {
//			ZAlert.Error("upload.not.allow.media.extension");
//		} catch (InvalidExtensionException e) {
//			ZAlert.Error("upload.not.allow.extension");
//		} catch (FileUploadBase.FileSizeLimitExceededException e) {
//			ZAlert.Error("upload.exceed.maxSize");
//		} catch (FileNameLengthLimitExceededException e) {
//			ZAlert.Error("upload.filename.exceed.length");
//		}
//		return null;
//	}
//	
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：文件上传
//	 * @param request HttpServletRequest
//	 * @param baseDir 基础路径，来源系统/来源业务,前后无斜杠
//	 * @param file 上传的文件
//	 * @return
//	 * 2016年9月19日 下午5:36:27
//	 */
//	public static final Map<String, Object> upload(HttpServletRequest request,String baseDir, MultipartFile file) {
//		try {
//			return upload(extractUploadDir(request), baseDir, file, true);
//		} catch (IOException e) {
//			ZAlert.Error("upload.server.error");
//		} catch (InvalidExtensionException.InvalidImageExtensionException e) {
//			ZAlert.Error("upload.not.allow.image.extension");
//		} catch (InvalidExtensionException.InvalidFlashExtensionException e) {
//			ZAlert.Error("upload.not.allow.flash.extension");
//		} catch (InvalidExtensionException.InvalidMediaExtensionException e) {
//			ZAlert.Error("upload.not.allow.media.extension");
//		} catch (InvalidExtensionException e) {
//			ZAlert.Error("upload.not.allow.extension");
//		} catch (FileUploadBase.FileSizeLimitExceededException e) {
//			ZAlert.Error("upload.exceed.maxSize");
//		} catch (FileNameLengthLimitExceededException e) {
//			ZAlert.Error("upload.filename.exceed.length");
//		}
//		return null;
//	}
//	
//	/**
//	 * 文件上传
//	 *
//	 * @param rootPath
//	 *          根目录
//	 * @param baseDir
//	 *          相对应用的基目录
//	 * @param file
//	 *          上传的文件
//	 * @param needDatePathAndRandomName
//	 *          是否需要日期目录和随机文件名前缀
//	 * @return 返回成功上传后的文件相关信息，map
//	 * @throws InvalidExtensionException
//	 *           如果MIME类型不允许
//	 * @throws FileSizeLimitExceededException
//	 *           如果超出最大大小
//	 * @throws FileNameLengthLimitExceededException
//	 *           文件名太长
//	 * @throws IOException
//	 *           比如读写文件出错时
//	 */
//	public static final Map<String, Object> upload(String rootPath, String baseDir, MultipartFile file, boolean needDatePathAndRandomName)
//			throws InvalidExtensionException, FileSizeLimitExceededException, IOException,
//			FileNameLengthLimitExceededException {
//		Map<String, Object> resFileInfo = new HashMap<String, Object>();
//		resFileInfo.put("oldFileName", file.getOriginalFilename());
//		resFileInfo.put("fileType", file.getContentType());
//		String suffix=FilenameUtils.getExtension(file.getOriginalFilename());
//		resFileInfo.put("suffix", suffix);
//		
//		int fileNamelength = file.getOriginalFilename().length();
//		int fileNameMaxLength=FileUploadUtils.DEFAULT_FILE_NAME_LENGTH;
//		SysParam sp=ZSysParamUtil.getSysParamByCode("file_name_max_length");
//		if(sp!=null){
//			fileNameMaxLength=Integer.parseInt(sp.getValue());
//		}
//		if (fileNamelength > fileNameMaxLength) {
//			throw new FileNameLengthLimitExceededException(file.getOriginalFilename(), fileNamelength,fileNameMaxLength);
//		}
//		
//		String fileSimpleType=assertAllowed(file);
//		
//		resFileInfo.put("simpleClass", fileSimpleType);
//		
//		//对文件的可允许上传校验之后进行md5加密
//		String fileMd5=fileToMd5(file);
//		resFileInfo.put("sign", fileMd5);
//		
//		if(ZStrUtil.isEmpty(baseDir)){ 
//			baseDir=getDefaultBaseDir();
//		}
//		
//		String filename = extractFilename(file, baseDir,fileSimpleType, needDatePathAndRandomName);
//		filename=filename+"."+suffix;
//		
//		resFileInfo.put("newFileName", filename.substring(filename.lastIndexOf("\\")+1, filename.length()));
//		resFileInfo.put("filePath", filename);
//		
//		//String uploadDir=extractUploadDir(request);
//		File desc = getAbsoluteFile(rootPath, filename);
//		
//		resFileInfo.put("absolutePath", rootPath+File.separator+filename);
//		
//		file.transferTo(desc);
//		return resFileInfo;
//	}
//	
//	private static final File getAbsoluteFile(String uploadDir, String filename) throws IOException {
//		
//		uploadDir = FilenameUtils.normalizeNoEndSeparator(uploadDir);
//		
//		File desc = new File(uploadDir + File.separator + filename);
//		
//		if (!desc.getParentFile().exists()) {
//			desc.getParentFile().mkdirs();
//		}
//		if (!desc.exists()) {
//			desc.createNewFile();
//		}
//		return desc;
//	}
//	
//	public static final String extractFilename(MultipartFile file, String baseDir,String fileSimpleType, boolean needDatePathAndRandomName)
//			throws UnsupportedEncodingException {
//		String filename = file.getOriginalFilename();
//		int slashIndex = filename.indexOf("/");
//		if (slashIndex >= 0) {
//			filename = filename.substring(slashIndex + 1);
//		}
//		baseDir=baseDir + File.separator + fileSimpleType + File.separator;
//		if (needDatePathAndRandomName) {
//			filename = baseDir + datePath("yyyyMM") + File.separator + encodingFilename(filename);
//		} else {
//			filename = baseDir + filename;
//		}
//		
//		return filename;
//	}
//	
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：根据文件名当前系统时间，和文件记录随增数组合加密成新的文件名
//	 * @param filename
//	 * @return
//	 * 2016年9月23日 下午4:17:15
//	 */
//	private static final String encodingFilename(String filename) {
//		filename = filename.replace("_", " ");
//		//filename = MD5Util.encrypt(filename + System.nanoTime() + counter++) + "_" + filename;
//		filename = MD5Util.encrypt(filename + System.nanoTime() + counter++);
//		return filename;
//	}
//	
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：日期路径
//	 * @param format,路徑格式，可为空，默认yyyymm
//	 * @return 2016年9月19日 上午11:46:07
//	 */
//	private static final String datePath(String format) {
//		Date now = new Date();
//		if (ZStrUtil.isEmpty(format)) {
//			format = "yyyyMM";
//		}
//		return DateFormatUtils.format(now, format);
//	}
//	
//	/**
//	 * 是否允许文件上传
//	 *
//	 * @param file
//	 *          上传的文件
//	 * @param allowedExtension
//	 *          文件类型 null 表示允许所有
//	 * @param maxSize
//	 *          最大大小 字节为单位 -1表示不限制
//	 * @return
//	 * @throws InvalidExtensionException
//	 *           如果MIME类型不允许
//	 * @throws FileSizeLimitExceededException
//	 *           如果超出最大大小
//	 */
//	public static final String assertAllowed(MultipartFile file)	throws InvalidExtensionException, FileSizeLimitExceededException {
//		List<SysParam> sysParamList=ZSysParamUtil.getSysParamByCodePrefix("file_type_extension_");
//		String fileType=file.getContentType();
//		String filename = file.getOriginalFilename();
//		String extension = FilenameUtils.getExtension(filename);
//		
//		
//		String fTypeAndFExt=extension+":"+fileType;
//		//验证和匹配文件类型，并返回文件匹配码
//		String fTypeAndFExtCode=matchAndVerifiFile(fTypeAndFExt, sysParamList,filename);
//		if(ZStrUtil.isEmpty(fTypeAndFExtCode)){
//			ZAlert.Error("指定文件类型不是被允许上传的的类型！");
//		}
//		
//		long size = file.getSize();
//		long maxSize=DEFAULT_MAX_SIZE;
//		String maxFileSize=ZSysParamUtil.getSysParamByCode("upload_max_file_size").getValue();
//		if(!ZStrUtil.isEmpty(maxFileSize)){
//			maxSize=Long.parseLong(maxFileSize);
//		}
//		if (maxSize != -1 && size > maxSize) {
//			throw new FileSizeLimitExceededException("超出可允许上传的文件大小！", size, maxSize);
//		}
//		String[] codes=fTypeAndFExtCode.split("_");
//		return codes[codes.length-1];
//	}
//	
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：匹配文件简单类型并校验文件
//	 * @param typeAndExt
//	 * @param fileTypeAndExtList
//	 * @param filename
//	 * @return
//	 * @throws InvalidExtensionException
//	 * 2016年9月19日 下午4:53:34
//	 */
//	public static final String matchAndVerifiFile(String typeAndExt, List<SysParam> fileTypeAndExtList,String filename) throws InvalidExtensionException {
//		String result="";
//		String fileTypeAndExt="";
//		for (SysParam sysParam : fileTypeAndExtList) {
//			String[] ftAndFe=sysParam.getValue().split(",");
//			boolean isThis=false;
//			for (String str : ftAndFe) {
//				String fileType=str.split(":")[0];
//				String fileTypeNow=typeAndExt.split(":")[0];
//				if(fileType.equalsIgnoreCase(fileTypeNow)){
//					isThis=true;
//				}
//				if (str.equalsIgnoreCase(typeAndExt)) {
//					result=sysParam.getCode();
//				}
//			}
//			if(isThis){
//				fileTypeAndExt=sysParam.getValue();
//			}
//		}
//		if(ZStrUtil.isEmpty(result)){
//			if(!ZStrUtil.isEmpty(fileTypeAndExt)){
//				String[] ftAndFe=fileTypeAndExt.split(",");
//				String[] fileExt=new String[ftAndFe.length];
//				for (int i = 0; i < ftAndFe.length; i++) {
//					fileExt[i]=ftAndFe[i].split(":")[1];
//				}
//				throw new InvalidExtensionException(fileExt, typeAndExt.split(":")[1], filename);
//			}else{
//				ZAlert.Error("未知不允许的上传文件");
//			}
//		}
//		return result;
//	}
//	
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：优先获取系统配置根目录，如果没有会获取项目应用根目录
//	 * @param request
//	 * @return
//	 * 2016年9月22日 上午9:04:40
//	 */
//	public static final String extractUploadDir(HttpServletRequest request) {
//		rootDir=ZSysParamUtil.getSysParamByCode("upload_file_root_path").getValue();
//		if (ZStrUtil.isEmpty(rootDir)) {
//			return request.getServletContext().getRealPath("/");
//		} else {
//			return rootDir;
//		}
//	}
//	/**
//	 * 
//	 * @author：曹仁道
//	 * @描述：获取系统配置根目录，没有则抛出异常
//	 * @return
//	 * 2016年9月22日 上午9:05:20
//	 */
//	public static final String sysParamUploadDir(){
//		SysParam sp=ZSysParamUtil.getSysParamByCode("upload_file_root_path");
//		if(sp==null){
//			ZAlert.Error("请配置系统参数：upload_file_root_path");
//		}
//		return sp.getValue();
//	}
//	
//	public static final void delete(String filePath){
//		if (ZStrUtil.isEmpty(filePath)) {
//			return;
//		}
//		File desc = new File(filePath);
//		if (desc.exists()) {
//			desc.delete();
//		}
//	}
//	
//	public static final String fileToMd5(MultipartFile file){
//		long fileMd5Start=System.currentTimeMillis();//计时开始
//		String fileMd5="";
//		try {
//			byte[] fileByte=file.getBytes();
//			MessageDigest md5=MessageDigest.getInstance("MD5");
//			byte[] digest = md5.digest(fileByte);
//	    String hashString = new BigInteger(1, digest).toString(16);
//	    fileMd5=hashString.toString();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			ZAlert.Error("文件转换异常");
//			e.printStackTrace();
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			ZAlert.Error("文件加密异常");
//			e.printStackTrace();
//		}
//		long fileMd5End=System.currentTimeMillis();//计时结束
//		System.out.println("对文件MD5加密耗时："+(fileMd5End-fileMd5Start));
//		return fileMd5;
//	}
//}
