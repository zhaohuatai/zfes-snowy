/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.dao;

import java.io.Serializable;
import java.time.Instant;

public class IBaseModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	protected Long id;
	
	protected Long recordTime;
	
	protected Boolean isEnabled;
	
	protected Byte status;

	public Long getId() {
		return id;
	}

	public IBaseModel setId(Long id) {
		this.id = id;
		return this;
	}

	public Long getRecordTime() {
		if(this.recordTime==null){
			recordTime=Instant.now().toEpochMilli();
		}
		return recordTime;
	}

	public IBaseModel setRecordTime(Long recordTime) {
		if(recordTime!=null){
			this.recordTime = recordTime;
		}
		recordTime=Instant.now().toEpochMilli();
		return this;
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public IBaseModel setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
		return this;
	}

	public Byte getStatus() {
		return status;
	}

	public IBaseModel setStatus(Byte status) {
		this.status = status;
		return this;
	}


	
	
}
