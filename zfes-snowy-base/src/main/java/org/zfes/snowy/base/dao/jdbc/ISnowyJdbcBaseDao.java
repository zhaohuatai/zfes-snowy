/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.dao.jdbc;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.DataSet;

public interface ISnowyJdbcBaseDao{
	
	 public int insert(String insertSQL, Map<String,Object> params );
	 
	 public int[] batchInsert(String batchInsertSQL,Map<String,?>[] params);
	 
	 int update(String updateSQL,Map<String,Object> params );
	
	 int[] batchUpdate(String batchUpdateSQL, Map<String,?>[] params);
	
	 int delete(String deleteSQL,Map<String,Object> params );
	
	 int[] batchDelete(String batchDeleteSQL, Map<String,?>[] params);
//---------------------------------------------------------------------------------------------------------
	 DataSet loadDataSet(String querySQL,ParamMap params );
	
	 List<Map<String, Object>> queryMapList(String querySQL,Map<String,Object> params );
	 
	 long queryCount(String countSQL,Map<String,Object> params);
	 
	 Map<String, Object> queryOneRowByUnque(String queryStr,Map<String,Object> params );
	 
	 public Optional<Object> queryOneObject(String querySQL, Map<String,Object> params,Class<?> clazz) ;
	//-----------------------------------------------
	 public Optional<Long> queryUniqueLong(String querySQL, Map<String, Object> params);
	 public Optional<Integer> queryUniqueInteger(String querySQL, Map<String, Object> params);
	 public Optional<String> queryUniqueStr(String querySQL,Map<String,Object> params);
	 
	//-------------------------------------------------------
	 public List<String> queryStrResList(String querySQL,Map<String,Object> params);
	 public List<Long> queryLongList(String querySQL,Map<String,Object> params);
	 public List<Integer> queryIntegerList(String querySQL,Map<String,Object> params);
	 public List<Short> queryShortList(String querySQL,Map<String,Object> params);
	 public List<Byte> queryByteList(String querySQL,Map<String,Object> params);
	 public List<Float> queryFloatList(String querySQL,Map<String,Object> params);
	 public List<Double> queryDoubleList(String querySQL,Map<String,Object> params);
	 public List<Boolean> queryBooleanList(String querySQL,Map<String,Object> params);


}
