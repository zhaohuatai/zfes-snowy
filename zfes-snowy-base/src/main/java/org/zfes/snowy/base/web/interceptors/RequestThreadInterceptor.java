/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.web.interceptors;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.zfes.snowy.core.request.RequestThreadLocal;
/**
 * springmvc拦截器，实现队请求的拦截，将请求信息放入RequestThreadLocal 中
 * @author zhaohuatai
 *
 */
public class RequestThreadInterceptor extends HandlerInterceptorAdapter{

	@Override
	public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex)throws Exception {
		super.afterCompletion(request, response, handler, ex);
		try{
			RequestThreadLocal.clearCurrentReqestInfo();
		}catch(Exception e){}
		
	}

	@Override
	public void postHandle(HttpServletRequest request,	HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {
		try{
			RequestThreadLocal.clearCurrentReqestInfo();
		}catch(Exception e){}
		
	}
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		 if (handler instanceof HandlerMethod) {
			 RequestThreadLocal.setServletRequest(request);
	            return true;
	        } else {
	            return super.preHandle(request, response, handler);
	        }
		
	}

}
