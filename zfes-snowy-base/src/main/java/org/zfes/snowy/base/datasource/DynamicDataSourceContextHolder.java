package org.zfes.snowy.base.datasource;

import java.util.HashSet;
import java.util.Set;

public class DynamicDataSourceContextHolder {

    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
    
    public static Set<String> dataSourceIds = new HashSet<>();

    public static void setDataSourceType(String dataSourceType) {
        contextHolder.set(dataSourceType);
    }

    public static String getDataSourceType() {
    	String dst=contextHolder.get();
        return dst;
    }

    public static void clearDataSourceType() {
        contextHolder.remove();
    }

    
    public static boolean containsDataSource(String dataSourceId){
        return dataSourceIds.contains(dataSourceId);
    }
}
