/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.dao;

import java.io.Serializable;
import java.time.Instant;

public class BaseModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	protected Long id;
	
	protected Long recordTime;
	
	protected Boolean isEnabled;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRecordTime() {
		if(this.recordTime==null){
			recordTime=Instant.now().toEpochMilli();
		}
		return recordTime;
	}

	public void setRecordTime(Long recordTime) {
		if(recordTime!=null){
			this.recordTime = recordTime;
		}
		recordTime=Instant.now().toEpochMilli();
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	
}
