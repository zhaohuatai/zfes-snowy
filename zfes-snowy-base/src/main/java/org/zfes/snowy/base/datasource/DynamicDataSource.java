package org.zfes.snowy.base.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
//https://yq.aliyun.com/articles/8302
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceType();
    }
}
