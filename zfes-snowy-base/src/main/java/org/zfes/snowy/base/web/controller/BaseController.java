/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.base.web.controller;


import java.util.Objects;
import java.util.Optional;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.JSONMSG;

public class BaseController {

	protected JSONMSG ajaxDone(int statusCode, String message, String forwardUrl,String confirmMsg) {
		JSONMSG msg = new JSONMSG();
		msg.setStatusCode(statusCode);
		msg.setMessage(message);
		msg.setForwardUrl(forwardUrl);
		msg.setConfirmMsg(confirmMsg);
		return msg;
	}
	protected JSONMSG ajaxDone(int statusCode, String message) {
		return ajaxDone(statusCode,message,"","");
	}
	protected JSONMSG ajaxSuccess() {
		return ajaxDone(FrameConst.SC.SC_200, "");
	}
	protected JSONMSG ajaxDoneSuccess(String message) {
		return ajaxDone(FrameConst.SC.SC_200, message);
	}
	protected JSONMSG ajaxDoneError(String message) {
		return ajaxDone(FrameConst.SC.SC_300, message);
	}
	
	protected JSONMSG ajaxRunTimeError(String message) {
		return ajaxDone(FrameConst.SC.SC_500, message);
	}
	
	protected JSONMSG ajaxServerError(String message) {
		return ajaxDone(FrameConst.SC.SC_500, message);
	}
	protected JSONMSG ajaxAuthError(String message) {
		return ajaxDone(FrameConst.SC.SC_401, message);
	}
	protected JSONMSG ajaxAuthError(int statusCode,String message) {
		return ajaxDone(statusCode, message);
	}
	protected JSONMSG ajaxQuery(String key,Optional<?>  value){
    	if(value.isPresent()){
			 return ajaxSuccess().put(key, value.get());
		}else{
			return ajaxSuccess().put(key, new Object());
			//return ajaxDoneError(FrameConst.WEB_MSG.QUERY_FAILURE);
		}
    }
	protected JSONMSG ajaxQuery(String key,Object value){
    	if(!Objects.isNull(value)){
			 return ajaxSuccess().put(key, value);
		}else{
			return ajaxSuccess().put(key, new Object());
			//return ajaxDoneError(FrameConst.WEB_MSG.QUERY_FAILURE);
		}
    }
	
	protected static final String viewPath="view";
	
	protected  Object forward(String viewPath,String viewName) {
		if(!viewPath.startsWith("/")){
			viewPath="/"+viewPath;
		}
		return "forward:"+viewPath+viewName;
	}
	
	protected Object redirect(String viewPath,String viewName) {
		if(!viewPath.startsWith("/")){
			viewPath="/"+viewPath;
		}
		return "redirect:"+viewPath+viewName;
	}
	protected static Object convert(Object object){
		if(object==null){
			return new Object();
		}
		return object;
	}
	protected static Object convert(DataSet dataSet){
		if(dataSet==null){
			return new DataSet(0L,null);
		}
		return dataSet;
	}
	
	//--------------------------------------------------AAAA----------------------------------------------
	protected static void setDataAttribute(Model model, Object data,String dataName) {
		if(model==null||data==null||dataName==null){
			return;
		}
		model.addAttribute(dataName,data);
	}
//-----------------------------------------------------------------------------------------------------------	
	
	protected static void setDataAttribute(ModelAndView modelView, Object data,String dataName) {
		if(modelView==null||data==null||dataName==null){
			return;
		}
		modelView.addObject(dataName,data);
	}
	
//-----------------------------------------------------------------------------------------------------------	
	

	protected ModelAndView returnServiceLogicError(String errorMsg) {
		ModelAndView mav = new ModelAndView("error/serviceLogicError");
		mav.addObject("serviceLogicError", errorMsg);
		return mav;
	}
		
	protected  ModelAndView returnMeaasgeMV(String viewName,String msg) {
		ModelAndView mav = new ModelAndView(viewName);
		mav.addObject("message", msg);
		return mav;
	}
	
	
}
