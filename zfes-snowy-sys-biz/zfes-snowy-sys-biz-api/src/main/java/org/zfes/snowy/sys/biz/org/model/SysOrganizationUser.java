package org.zfes.snowy.sys.biz.org.model;
import org.zfes.snowy.base.dao.IBaseModel;

public class SysOrganizationUser extends IBaseModel {

	private static final long serialVersionUID = 1L;
	
	public SysOrganizationUser() {}
	public SysOrganizationUser(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	public SysOrganizationUser(Long id,Long organizationId, Long authUserId) {
		super();
		this.setId(id);
		this.orgId = organizationId;
		this.authUserId = authUserId;
	}
	 @javax.validation.constraints.NotNull(message="组织机构唯一编码不能为空")
	
	  private Long orgId;
	 @javax.validation.constraints.NotNull(message="用户ID不能为空")

	  private Long authUserId;

	//<-------------------------------------------->
	public SysOrganizationUser setOrgId(Long orgId){
	 this.orgId=orgId;
	 return this;
	}
	public Long getOrgId(){
		return this.orgId;
	}
	public SysOrganizationUser setAuthUserId(Long authUserId){
	 this.authUserId=authUserId;
	 return this;
	}
	public Long getAuthUserId(){
		return this.authUserId;
	}

}
