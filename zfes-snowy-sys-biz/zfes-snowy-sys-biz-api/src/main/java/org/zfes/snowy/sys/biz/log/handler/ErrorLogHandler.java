/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.log.handler;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.zfes.snowy.core.events.ErrorAppearEvent;
import org.zfes.snowy.sys.biz.log.model.LogError;
import org.zfes.snowy.sys.biz.log.util.ZLogUtil;

/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */

@Component
public class ErrorLogHandler {
	
	private Boolean isPersistDB=false;
    @Async
    @EventListener  
    public void handleErrorAppearEvent(ErrorAppearEvent errorAppearEvent) {
    	LogError logError=null;
    	if(errorAppearEvent.getRequest()!=null){
    		 logError=ZLogUtil.fillErrorLog(errorAppearEvent.getRequest());
    	}
    	logError=(logError==null?new LogError():logError);
    	
    	//Class<?> clazz=errorAppearEvent.getClazz();
    	//Throwable e=errorAppearEvent.getE();
    	String message=errorAppearEvent.getMessage();
    	//String methodName=errorAppearEvent.getMethodName();
    	logError.setException(message);
    	ZLogUtil.logError(logError, isPersistDB);
    }
	public Boolean getIsPersistDB() {
		return isPersistDB;
	}
	public void setIsPersistDB(Boolean isPersistDB) {
		this.isPersistDB = isPersistDB;
	}  
    
    
    
}
