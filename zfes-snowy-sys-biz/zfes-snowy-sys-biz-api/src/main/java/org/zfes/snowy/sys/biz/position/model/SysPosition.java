/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.model;
import java.util.Date;

import org.zfes.snowy.base.dao.BaseModel;
public class SysPosition extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public SysPosition() {}
	public SysPosition(Long id) {
		this.setId(id);
	}
	
    public SysPosition(Long id,String name, Boolean enabled, Long creater, Date createTime, String orgTreeCode,
			Long authRoleId, String remark,String code) {
		super();
		this.id=id;
		this.name = name;
		this.enabled = enabled;
		this.creater = creater;
		this.createTime = createTime;
		this.orgTreeCode = orgTreeCode;
		this.authRoleId = authRoleId;
		this.remark = remark;
		this.code = code;
	}

	//<-------------------------------------------->

	  @javax.validation.constraints.NotNull(message="名称不能为空")
	  @org.hibernate.validator.constraints.Length(min=0,max=40,message="名称长度不能大于40")
	  private java.lang.String name;
	 
	  @javax.validation.constraints.NotNull(message="编码不能为空")
	  @org.hibernate.validator.constraints.Length(min=0,max=20,message="编码长度不能大于20")
	  private java.lang.String code;
		
	  @javax.validation.constraints.NotNull(message="状态不能为空")
	  private java.lang.Boolean enabled;
	  
	  private java.lang.Long creater;
	  private java.util.Date createTime;
	  
	  @javax.validation.constraints.NotNull(message="所属组织机构不能为空")
	  private java.lang.String orgTreeCode;
	  
	  
	  @javax.validation.constraints.NotNull(message="所角色不能为空")
	  private java.lang.Long authRoleId;
	  
	  @org.hibernate.validator.constraints.Length(min=0,max=60,message="备注长度不能大于60")
	  private java.lang.String remark;

	

	//<-------------------------------------------->
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}

	public java.lang.Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(java.lang.Boolean enabled) {
		this.enabled = enabled;
	}
	public void setCreateTime(java.util.Date createTime){
	 this.createTime=createTime;
	}
	public java.lang.Long getCreater() {
		return creater;
	}
	public void setCreater(java.lang.Long creater) {
		this.creater = creater;
	}
	public java.util.Date getCreateTime(){
		return this.createTime;
	}
	public void setAuthRoleId(java.lang.Long authRoleId){
	 this.authRoleId=authRoleId;
	}
	
	public java.lang.String getOrgTreeCode() {
		return orgTreeCode;
	}
	public void setOrgTreeCode(java.lang.String orgTreeCode) {
		this.orgTreeCode = orgTreeCode;
	}
	public java.lang.Long getAuthRoleId(){
		return this.authRoleId;
	}
	public void setRemark(java.lang.String remark){
		this.remark=remark==null?null:remark.trim();
	}
	public java.lang.String getRemark(){
		return this.remark;
	}
	public java.lang.String getCode() {
		return code;
	}
	public void setCode(java.lang.String code) {
		this.code = code;
	}

}
