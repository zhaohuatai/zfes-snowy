package org.zfes.snowy.sys.biz.org.model;
import org.zfes.snowy.base.dao.IBaseModel;

public class SysOrganization extends IBaseModel {

	private static final long serialVersionUID = 1L;
	
	public SysOrganization() {}
	public SysOrganization(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->

	 @javax.validation.constraints.NotNull(message="名称不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=40,message="名称长度不能大于40")
	  private String name;
	 @javax.validation.constraints.NotNull(message="是否可用不能为空")

	  private Boolean enabled;


	  private java.util.Date modifyTime;


	  private Long parentId;

	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="备注长度不能大于60")
	  private String remark;


	  private Long creater;


	  private java.util.Date createTime;

	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="描述长度不能大于60")
	  private String description;


	  private String appId;
	 @javax.validation.constraints.NotNull(message="编码不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=20,message="编码长度不能大于20")
	  private String code;

	 @org.hibernate.validator.constraints.Length(min=0,max=50,message="唯一编码Code长度不能大于50")
	  private String orgTreeCode;


	  private Integer level;

	//<-------------------------------------------->
	public SysOrganization setName(String name){
		this.name=name==null?null:name.trim();
	 return this;
	}
	public String getName(){
		return this.name;
	}
	public SysOrganization setEnabled(Boolean enabled){
	 this.enabled=enabled;
	 return this;
	}
	public Boolean getEnabled(){
		return this.enabled;
	}
	public SysOrganization setModifyTime(java.util.Date modifyTime){
	 this.modifyTime=modifyTime;
	 return this;
	}
	public java.util.Date getModifyTime(){
		return this.modifyTime;
	}
	public SysOrganization setParentId(Long parentId){
	 this.parentId=parentId;
	 return this;
	}
	public Long getParentId(){
		return this.parentId;
	}
	public SysOrganization setRemark(String remark){
		this.remark=remark==null?null:remark.trim();
	 return this;
	}
	public String getRemark(){
		return this.remark;
	}
	public SysOrganization setCreater(Long creater){
	 this.creater=creater;
	 return this;
	}
	public Long getCreater(){
		return this.creater;
	}
	public SysOrganization setCreateTime(java.util.Date createTime){
	 this.createTime=createTime;
	 return this;
	}
	public java.util.Date getCreateTime(){
		return this.createTime;
	}
	public SysOrganization setDescription(String description){
		this.description=description==null?null:description.trim();
	 return this;
	}
	public String getDescription(){
		return this.description;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public SysOrganization setCode(String code){
		this.code=code==null?null:code.trim();
	 return this;
	}
	public String getCode(){
		return this.code;
	}
	public SysOrganization setOrgTreeCode(String orgTreeCode){
		this.orgTreeCode=orgTreeCode==null?null:orgTreeCode.trim();
	 return this;
	}
	public String getOrgTreeCode(){
		return this.orgTreeCode;
	}
	public SysOrganization setLevel(Integer level){
	 this.level=level;
	 return this;
	}
	public Integer getLevel(){
		return this.level;
	}

}
