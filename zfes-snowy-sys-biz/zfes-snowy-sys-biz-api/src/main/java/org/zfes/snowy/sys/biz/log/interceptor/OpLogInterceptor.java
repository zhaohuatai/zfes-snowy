/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.sys.biz.log.interceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.zfes.snowy.sys.biz.log.model.LogOperation;
import org.zfes.snowy.sys.biz.log.util.ZLogUtil;

public class OpLogInterceptor extends HandlerInterceptorAdapter{
	
	
	@Override
	public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex)throws Exception {
		super.afterCompletion(request, response, handler, ex);
	
	}


	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		LogOperation opLog=ZLogUtil.fillOperationLog(request);
		opLog.setAppKey("AppKey");
		opLog.setUserId(null);
		//opLog.setUserCharId(SecurityUtils.getSubject().getPrincipal().toString());
		//opLog.setUserName(SecurityUtils.getSubject().getPrincipal().toString());
		ZLogUtil.log(opLog,true);
		return super.preHandle(request, response, handler);
	            
	}
}
