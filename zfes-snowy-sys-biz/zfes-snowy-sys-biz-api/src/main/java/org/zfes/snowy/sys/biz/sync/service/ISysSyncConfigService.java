/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.service;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.sync.model.SysSyncConfig;
public interface ISysSyncConfigService{

	 Optional<SysSyncConfig> loadSysSyncConfigById(Long id);
	
	 void createSysSyncConfig(SysSyncConfig sysSyncConfig);
	
	 void updateSysSyncConfig(SysSyncConfig sysSyncConfig);

	 void deletSysSyncConfig(Long id);
	
	 DataSet loadSysSyncConfigDataSet(Map<String, Object> params);
	 

}