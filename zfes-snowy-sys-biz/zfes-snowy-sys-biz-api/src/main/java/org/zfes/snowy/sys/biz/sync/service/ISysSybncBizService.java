/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.service;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.sync.model.SysSybncBiz;
public interface ISysSybncBizService{

	 Optional<SysSybncBiz> loadSysSybncBizById(Long id);
	
	 void createSysSybncBiz(SysSybncBiz sysSybncBiz);
	
	 void updateSysSybncBiz(SysSybncBiz sysSybncBiz);

	 void deletSysSybncBiz(Long id);
	
	 DataSet loadSysSybncBizDataSet(Map<String, Object> params);
	 

}