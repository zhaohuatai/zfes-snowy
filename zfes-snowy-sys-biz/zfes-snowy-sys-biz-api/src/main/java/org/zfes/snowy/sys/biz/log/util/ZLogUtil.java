/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.sys.biz.log.util;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zfes.snowy.core.util.AppCtxUtil;
import org.zfes.snowy.core.util.IPUtil;
import org.zfes.snowy.core.util.ZWebUtil;
import org.zfes.snowy.sys.biz.log.consts.SnowyLogConsts;
import org.zfes.snowy.sys.biz.log.model.LogError;
import org.zfes.snowy.sys.biz.log.model.LogOperation;
import org.zfes.snowy.sys.biz.log.service.ILogErrorService;
import org.zfes.snowy.sys.biz.log.service.ILogOperationService;

public class ZLogUtil {
	
	 public static final Logger operationLog = LoggerFactory.getLogger(SnowyLogConsts.LOG_APPENDER.log_operation);
	
	 public static final Logger errorLog = LoggerFactory.getLogger(SnowyLogConsts.LOG_APPENDER.log_error);
	 
	 
	 public static void log(LogOperation operation,boolean isPersistDB){
	      operationLog.info(OperationLogToStr(operation));
	      if(isPersistDB){
	    	  ILogOperationService logOperationService= AppCtxUtil.getBean(SnowyLogConsts.LOG_SERVICE_BEAN_NAME.operationLogService);
				 if(logOperationService!=null){
					 logOperationService.createLogOperation(operation);
				 }
			 }
	 }
	 public static LogOperation fillOperationLog(HttpServletRequest request){
			String sessionId = request.getRequestedSessionId();
	        String remoteIpAddr = IPUtil.getClientIpAddr(request);
	        String accept = request.getHeader("accept");
	        String userAgent = request.getHeader("User-Agent");
	        String requestURL = request.getRequestURL().toString();
	        String methed=request.getMethod();
	        String params = ZWebUtil.getRequestParams(request);
	        String referer=request.getHeader("Referer");
	        return new LogOperation(sessionId,new Date(),accept,userAgent,requestURL,methed,referer,remoteIpAddr,params);
	 }
	 public static LogError fillErrorLog(HttpServletRequest request){
	     	String sessionId = request.getRequestedSessionId();
	        String remoteIpAddr = IPUtil.getClientIpAddr(request);
	        String accept = request.getHeader("accept");
	        String userAgent = request.getHeader("User-Agent");
	        String requestURL = request.getRequestURL().toString();
	        String methed=request.getMethod();
	        String params = ZWebUtil.getRequestParams(request);
	        String referer=request.getHeader("Referer");
		 return new LogError(sessionId,new Date(),accept,userAgent,requestURL,methed,referer,remoteIpAddr,params);
	 }
//---------------------------------------------------------------------------------
	 private static void persist(LogError logError ){
		 ILogErrorService logErrorService= AppCtxUtil.getBean(SnowyLogConsts.LOG_SERVICE_BEAN_NAME.errorLogService); 
		 logErrorService.createLogError(logError);
	 }
	 
	 public static void logError(LogError logError,boolean isPersistDB ){
		 errorLog.error(errorLogToStr(logError));
		 if(isPersistDB){
			 persist(logError);
		 }
	 }
	 
	 public static void logError(String message, Throwable e) {
	        StringBuilder stringBuilder = new StringBuilder();
	        stringBuilder.append("[").append(message).append("]");
	        errorLog.error(stringBuilder.toString(), e);
	  }
	 
	  public static void logError(String ClassName,String methodName,String message, Throwable e) {
	        StringBuilder stringBuilder = new StringBuilder();
	        stringBuilder.append("[").append("ClassName:").append(methodName).append("]");
	        stringBuilder.append("[").append("methodName:").append(methodName).append("]");
	        stringBuilder.append("[").append("Exception:").append(message).append("]");
	        errorLog.error(stringBuilder.toString(), e);
	  }
	  
	  public static void  logError(Class<?> clzz,String methodName,String message, Throwable e) {
	        StringBuilder stringBuilder = new StringBuilder();
	        stringBuilder.append("[").append("ClassName:").append(clzz.getName()).append("]");
	        stringBuilder.append("[").append("methodName:").append(methodName).append("]");
	        stringBuilder.append("[").append("Exception:").append(message).append("]");
	        errorLog.error(stringBuilder.toString(), e);
	  }
	 
	 private static String OperationLogToStr(LogOperation operation ){
		 	StringBuilder stringBuilder = new StringBuilder();
		 	stringBuilder
		 	.append("[").append("appKey:").append(operation.getAppKey()).append("]")
		 	.append("[").append("userId:").append(operation.getUserId()).append("]")
		 	.append("[").append("userCharId:").append(operation.getUserCharId()).append("]")
		 	.append("[").append("userName:").append(operation.getUserName()).append("]")
		 	.append("[").append("operateTime:").append(operation.getOperateTime()).append("]")
	        .append("[").append("sessionId:").append(operation.getSessionId()).append("]")
	        .append("[").append("requestURL:").append(operation.getRequestUrl()).append("]")
	        .append("[").append("remoteIpAddr:").append(operation.getRemoteIp()).append("]")
	        .append("[").append("accept:").append(operation.getAccept()).append("]")
	        .append("[").append("userAgent:").append(operation.getUseragent()).append("]")
	        .append("[").append("referer:").append(operation.getReferer()).append("]")
	        .append("[").append("methed:").append(operation.getMethed()).append("]")
	        .append("[").append("params:").append(operation.getParams()).append("]");
	        return stringBuilder.toString();
	 }
	 private static String errorLogToStr(LogError logError ){
		 	StringBuilder stringBuilder = new StringBuilder();
		 	stringBuilder
		 	.append("[").append("appKey:").append(logError.getAppKey()).append("]")
		 	.append("[").append("userId:").append(logError.getUserId()).append("]")
		 	.append("[").append("userCharId:").append(logError.getUserCharId()).append("]")
		 	.append("[").append("userName:").append(logError.getUserName()).append("]")
		 	.append("[").append("operateTime:").append(logError.getOperateTime()).append("]")
	        .append("[").append("sessionId:").append(logError.getSessionId()).append("]")
	        .append("[").append("requestURL:").append(logError.getRequestUrl()).append("]")
	        .append("[").append("remoteIpAddr:").append(logError.getRemoteIp()).append("]")
	        .append("[").append("accept:").append(logError.getAccept()).append("]")
	        .append("[").append("userAgent:").append(logError.getUseragent()).append("]")
	        .append("[").append("referer:").append(logError.getReferer()).append("]")
	        .append("[").append("methed:").append(logError.getMethed()).append("]")
	        .append("[").append("params:").append(logError.getParams()).append("]")
		 	.append("[").append("exception:").append(logError.getException()).append("]");
	        return stringBuilder.toString();
	 }
}
