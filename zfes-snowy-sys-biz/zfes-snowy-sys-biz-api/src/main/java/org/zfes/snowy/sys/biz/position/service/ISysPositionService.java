/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.position.model.SysPosition;
public interface ISysPositionService{

	 Optional<SysPosition> loadSysPosition(Long id);
		
	 void createSysPosition(SysPosition sysPosition);
	
	 void updateSysPosition(SysPosition sysPosition);

	 void deletSysPosition(Long id);
	
	 DataSet loadPositionDataSet(Map<String, Object> params);
	
	 List<Long> loadPositionIdsByRoleId(Long roleId,Boolean enabled);

     List<Long> loadPositionIdsByOrgTreeCode(String orgTreeCode,Boolean enabled);
    
     void setPositionStatus(Long id, Boolean enabled);
    /**
     * 加载组织机构下的岗位信息
     * @param organizationId ：{岗位名称： name String <br> 是否可用 ：enabled Boolean <br>所属组织结构 organizationId :long }
     * @return
     */
     public DataSet loadPositionByOrgTreeCode(String orgTreeCode,Map<String, Object> params);

    
}