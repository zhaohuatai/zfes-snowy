package org.zfes.snowy.sys.biz.consts;

public class SysBizConsts {
	public static final String SYSORG_TREECODE_SPLIT_CHAR="_";

	public static final String tree_table_rootnode_paarent_fk  = "-1";//

	public static final class ISINORGANIZATION{
		public static final String inorganization="inOrganization";
		public static final String notInOrganization="notInOrganization";
	}
}
