package org.zfes.snowy.sys.biz.org.service;

import org.zfes.snowy.core.data.ComboboVo;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.org.model.SysOrganization;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ISysOrganizationService {

	 Optional<SysOrganization> loadSysOrganizationById(Long id);
	
	 void createSysOrganization(SysOrganization sysOrganization);
	
	 void updateSysOrganization(SysOrganization sysOrganization);

	 void deleteSysOrganization(Long id);
	
	 DataSet loadSysOrganizationDataSet(Map<String, Object> params);
	 

 	void setSysOrganizationStatus(Long id, Byte status);

	/**
	 * 加载组织机构树
	 * @param params
	 * @return
	 */

	public List<Map<String, Object>> loadOrganizationTree(Map<String, Object> params);

	/**
	 * 加载在或者不在组织机构内人员信息
	 * @param params ：{ organizationId:组织机构Id,isInOrganization:加载人员是否在组织机构内标志，
	 * <br>（inOrganization：表示加载组织机构内的人员信息）<br>
	 *   (notInOrganization：表示加载不在组织机构内的人员信息)
	 *  }
	 * @return
	 */
	public DataSet loadUserForOrganization(Map<String, Object> params);

	/**
	 * 加载组织机构内用户的信息
	 * @param params
	 * @return
	 */
	public DataSet loadUserFormOrganization(Map<String, Object> params);
	/**
	 * 加载不在组织机构内人员信息
	 * @param params
	 * @return
	 */

	public DataSet loadUserNotInOrganization(Map<String, Object> params);

	/**
	 * 向人员组织机构表中添加组织结构Id为deptId,用户id在userIds中的用户
	 * @param appIds
	 */
	public void addUserToOrganization(List<Long> userIds, List<Long> appIds, Long organizationId);
	/**
	 * 从Id为deptId的组织机构中移除用户Id在userIds的用户
	 * @param userIds :用户Id集合
	 * @param uniqueCode  :组织机构唯一标识编码
	 */
	public void removeUserFromOrganization(List<Long> userIds, Long organizationId);

	/**
	* @Author : Victor
	* @Description : 通过用户id查询他的所属组织机构
	 * param  Long userId  用户id
	* @Date : 17:09 2017/11/9
	*
	*/
	SysOrganization selectSysOrganizationListByUserId(Long userId);

	/**
	 * @Author : Victor
	 * @Description : 通过
	 * @Param :
	 * @Date : 18:06 2017/11/21
	 *
	 */
	public List<Map<String, Object>> loadOrganizationTabelDic();

	/**
	 * @Author : Victor
	 * @Description : 查询组织机构下拉框
	 * @Param :
	 * @Date : 21:23 2017/11/27
	 *
	 */
	public List<ComboboVo> selectOrgranizationComboboV(Boolean firstEmpty, String level, Long pid);

	/**
	 * @Author : Victor
	 * @Description : 通过组织机构名称查询组织机构信息
	 * @Param :
	 * @Date : 20:32 2017/12/11
	 *
	 */
	SysOrganization selectSysOrganizationListByName(String name);

	/**
	 * @Author : Victor
	 * @Description : 通过组织机构code查询组织机构信息
	 * @Param :
	 * @Date : 11:36 2017/12/13
	 *
	 */
	SysOrganization selectSysOrganizationListByCode(String code);

}