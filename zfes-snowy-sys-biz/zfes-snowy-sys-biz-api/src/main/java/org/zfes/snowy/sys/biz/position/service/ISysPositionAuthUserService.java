/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.service;

import java.util.List;
import java.util.Map;

import org.zfes.snowy.core.data.DataSet;

public interface ISysPositionAuthUserService{
    
    /**
     * 查询用户所拥有的岗位Id的集合
     * @param userId
     * @param enabled
     * @return
     */
     List<Long> loadPositionIdsByUserId(Long userId,Boolean enabled);
    
	/**
	 * 根据用户Id 加载岗位编码的集合
	 * @param userId
	 * @param enabled
	 * @return
	 */
	
	 List<String> loadPositionCodesByUserId(Long userId,Boolean enabled);
	
   /**
    * 为岗位增加用户
    * @param userIds :用户Id集合
    * @param PositionId :岗位Id
    */
     void addUserToPosition(List<Long> userIds, Long positionId);
    /**
     * 从岗位中移除用户
     * @param userIds :用户Id 集合
     * @param PositionId :岗位ID
     */
     void removeUserFromPosition(List<Long> userIds, Long positionId);

      DataSet loadPositionDataSetUserHave(Long userId,Map<String, Object> params);
     
      DataSet loadPositionDataSetUserNotHave(Long userId,Map<String, Object> params);
  
}