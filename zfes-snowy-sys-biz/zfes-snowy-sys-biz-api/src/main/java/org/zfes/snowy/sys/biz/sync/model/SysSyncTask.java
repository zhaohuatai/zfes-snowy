/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.model;
import org.zfes.snowy.base.dao.BaseModel;
public class SysSyncTask extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public SysSyncTask() {}
	public SysSyncTask(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	
	  private java.lang.Long sybncbizid;
	
	  private java.util.Date createtime;
	
	  private java.lang.Integer excutetimes;
	
	  private java.lang.Integer isexcuteover;
	
	  private java.lang.Integer isexcutesuccess;
	
	  private java.util.Date starttime;
	
	  private java.util.Date entime;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="备注长度不能大于60")
	  private java.lang.String remark;
	
	//<-------------------------------------------->
	public void setSybncbizid(java.lang.Long sybncbizid){
	 this.sybncbizid=sybncbizid;
	}
	public java.lang.Long getSybncbizid(){
		return this.sybncbizid;
	}
	public void setCreatetime(java.util.Date createtime){
	 this.createtime=createtime;
	}
	public java.util.Date getCreatetime(){
		return this.createtime;
	}
	public void setExcutetimes(java.lang.Integer excutetimes){
	 this.excutetimes=excutetimes;
	}
	public java.lang.Integer getExcutetimes(){
		return this.excutetimes;
	}
	public void setIsexcuteover(Integer isexcuteover){
	 this.isexcuteover=isexcuteover;
	}
	public Integer getIsexcuteover(){
		return this.isexcuteover;
	}
	public void setIsexcutesuccess(Integer isexcutesuccess){
	 this.isexcutesuccess=isexcutesuccess;
	}
	public Integer getIsexcutesuccess(){
		return this.isexcutesuccess;
	}
	public void setStarttime(java.util.Date starttime){
	 this.starttime=starttime;
	}
	public java.util.Date getStarttime(){
		return this.starttime;
	}
	public void setEntime(java.util.Date entime){
	 this.entime=entime;
	}
	public java.util.Date getEntime(){
		return this.entime;
	}
	public void setRemark(java.lang.String remark){
		this.remark=remark==null?null:remark.trim();
	}
	public java.lang.String getRemark(){
		return this.remark;
	}

}
