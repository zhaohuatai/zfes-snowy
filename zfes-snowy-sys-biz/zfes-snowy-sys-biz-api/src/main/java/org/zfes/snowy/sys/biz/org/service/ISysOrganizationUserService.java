package org.zfes.snowy.sys.biz.org.service;

import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.org.model.SysOrganizationUser;

import java.util.Map;
import java.util.Optional;

public interface ISysOrganizationUserService {

	 Optional<SysOrganizationUser> loadSysOrganizationUserById(Long id);
	
	 void createSysOrganizationUser(SysOrganizationUser sysOrganizationUser);
	
	 void updateSysOrganizationUser(SysOrganizationUser sysOrganizationUser);

	 void deleteSysOrganizationUser(Long id);
	
	 DataSet loadSysOrganizationUserDataSet(Map<String, Object> params);
	 

 	void setSysOrganizationUserStatus(Long id, Byte status);
}