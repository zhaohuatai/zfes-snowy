/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.model;
import org.zfes.snowy.base.dao.BaseModel;
public class SysPositionAuthUser extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public SysPositionAuthUser() {}
	public SysPositionAuthUser(Long id) {
		this.setId(id);
	}
	public SysPositionAuthUser(Long id,Long positionId, Long authUserId) {
		super();
		this.setId(id);
		this.positionId = positionId;
		this.authUserId = authUserId;
	}
	//<-------------------------------------------->

	 @javax.validation.constraints.NotNull(message="所属部门不能为空")
	  private java.lang.Long positionId;
	 @javax.validation.constraints.NotNull(message="用户不能为空")
	  private java.lang.Long authUserId;
	
	//<-------------------------------------------->
	public void setPositionId(java.lang.Long positionId){
	 this.positionId=positionId;
	}
	public java.lang.Long getPositionId(){
		return this.positionId;
	}
	public void setAuthUserId(java.lang.Long authUserId){
	 this.authUserId=authUserId;
	}
	public java.lang.Long getAuthUserId(){
		return this.authUserId;
	}

}
