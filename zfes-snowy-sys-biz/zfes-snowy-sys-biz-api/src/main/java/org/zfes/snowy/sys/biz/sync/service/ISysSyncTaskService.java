/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.service;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.sync.model.SysSyncTask;
public interface ISysSyncTaskService{

	 Optional<SysSyncTask> loadSysSyncTaskById(Long id);
	
	 void createSysSyncTask(SysSyncTask sysSyncTask);
	
	 void updateSysSyncTask(SysSyncTask sysSyncTask);

	 void deletSysSyncTask(Long id);
	
	 DataSet loadSysSyncTaskDataSet(Map<String, Object> params);
	 

}