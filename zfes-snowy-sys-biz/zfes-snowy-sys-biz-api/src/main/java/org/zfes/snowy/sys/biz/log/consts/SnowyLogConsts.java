package org.zfes.snowy.sys.biz.log.consts;

public class SnowyLogConsts {
	
	public static final class LOG_APPENDER{
		public static final String log_error = "log_error";
		public static final String log_operation = "log_operation";
	}
	
	
	public static final class LOG_SERVICE_BEAN_NAME{
		public static final String errorLogService = "zfes_snowy_errorLogService";
		public static final String operationLogService = "zfes_snowy_operationLogService";
	}
}
