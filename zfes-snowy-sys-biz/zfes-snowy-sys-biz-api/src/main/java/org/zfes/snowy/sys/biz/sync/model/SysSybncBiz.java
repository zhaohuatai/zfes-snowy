/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.model;
import org.zfes.snowy.base.dao.BaseModel;
public class SysSybncBiz extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public SysSybncBiz() {}
	public SysSybncBiz(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->

	
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="长度不能大于30")
	  private java.lang.String syncbizname;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="长度不能大于30")
	  private java.lang.String syncbizcode;
	
	
	  private java.lang.Boolean enabled;
	
	//<-------------------------------------------->
	public void setSyncbizname(java.lang.String syncbizname){
		this.syncbizname=syncbizname==null?null:syncbizname.trim();
	}
	public java.lang.String getSyncbizname(){
		return this.syncbizname;
	}
	public void setSyncbizcode(java.lang.String syncbizcode){
		this.syncbizcode=syncbizcode==null?null:syncbizcode.trim();
	}
	public java.lang.String getSyncbizcode(){
		return this.syncbizcode;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}

}
