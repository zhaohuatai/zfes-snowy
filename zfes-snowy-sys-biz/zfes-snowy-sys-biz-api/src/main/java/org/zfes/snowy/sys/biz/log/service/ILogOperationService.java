/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.log.service;

import java.util.Map;

import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.log.model.LogOperation;

public interface ILogOperationService{

	public LogOperation loadLogOperation(Long id);
		
	public void createLogOperation(LogOperation logOperation);
	
	public void deletLogOperation(Long id);
	
	public DataSet loadLogOperationList(Map<String, Object> params);


}