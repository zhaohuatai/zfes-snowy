/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.model;
import org.zfes.snowy.base.dao.BaseModel;
public class SysSyncConfig extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public SysSyncConfig() {}
	public SysSyncConfig(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->

	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="同步表名长度不能大于60")
	  private java.lang.String tablename;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="字段名长度不能大于60")
	  private java.lang.String setfieldname;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="查询字段长度不能大于60")
	  private java.lang.String wherefieldname;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="新值长度不能大于60")
	  private java.lang.String newvalue;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="条件值长度不能大于60")
	  private java.lang.String wherevalue;
	
	
	  private java.lang.Long syncbizid;
	
	
	  private java.lang.Boolean enabled;
	
	//<-------------------------------------------->
	public void setTablename(java.lang.String tablename){
		this.tablename=tablename==null?null:tablename.trim();
	}
	public java.lang.String getTablename(){
		return this.tablename;
	}
	public void setSetfieldname(java.lang.String setfieldname){
		this.setfieldname=setfieldname==null?null:setfieldname.trim();
	}
	public java.lang.String getSetfieldname(){
		return this.setfieldname;
	}
	public void setWherefieldname(java.lang.String wherefieldname){
		this.wherefieldname=wherefieldname==null?null:wherefieldname.trim();
	}
	public java.lang.String getWherefieldname(){
		return this.wherefieldname;
	}
	public void setNewvalue(java.lang.String newvalue){
		this.newvalue=newvalue==null?null:newvalue.trim();
	}
	public java.lang.String getNewvalue(){
		return this.newvalue;
	}
	public void setWherevalue(java.lang.String wherevalue){
		this.wherevalue=wherevalue==null?null:wherevalue.trim();
	}
	public java.lang.String getWherevalue(){
		return this.wherevalue;
	}
	public void setSyncbizid(java.lang.Long syncbizid){
	 this.syncbizid=syncbizid;
	}
	public java.lang.Long getSyncbizid(){
		return this.syncbizid;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}

}
