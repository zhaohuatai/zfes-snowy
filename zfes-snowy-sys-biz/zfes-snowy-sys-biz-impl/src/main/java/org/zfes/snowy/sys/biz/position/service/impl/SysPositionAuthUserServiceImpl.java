/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.service.impl;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.sys.biz.position.dao.SysPositionAuthUserMapper;
import org.zfes.snowy.sys.biz.position.model.SysPositionAuthUser;
import org.zfes.snowy.sys.biz.position.service.ISysPositionAuthUserService;

@Service
@Lazy(true)
public class SysPositionAuthUserServiceImpl implements ISysPositionAuthUserService{

 	@Autowired
	private SysPositionAuthUserMapper sysPositionAuthUserMapper;
	

	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<Long> loadPositionIdsByUserId(Long userId, Boolean enabled) {
		if(userId==null || userId==0){
			return Collections.emptyList();
		}
		List<Long> positionIds=sysPositionAuthUserMapper.selectPositionIdsByUserId(userId,enabled);
		return positionIds;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<String> loadPositionCodesByUserId(Long userId,Boolean enabled){
		if(userId==null || userId==0){
			return Collections.emptyList();
		}
		List<String> positionIds=sysPositionAuthUserMapper.selectPositionCodesByUserId(userId, enabled);//.selectPositionIdsByUserId(userId,enabled);
		return ZObjectUtil.emptyListIfNull(positionIds);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void addUserToPosition(List<Long> userIds, Long positionId) {
		if(userIds==null || ZObjectUtil.isEmpty(userIds)){
			ZAlert.Error("请选择数据");
		}
		userIds=userIds.stream().distinct().collect(Collectors.toList());
		
		removeUserFromPosition(userIds,positionId);
       
		List<SysPositionAuthUser> PositionUserList=userIds.stream()
				.map(userId->new SysPositionAuthUser(IDGenerator.genLongId(),positionId,userId))  
				.collect(Collectors.toList());
		
		sysPositionAuthUserMapper.batchInsertPositionUser(PositionUserList);
	}
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeUserFromPosition(List<Long> userIds, Long positionId) {
		if(positionId==null ||ZObjectUtil.isEmpty(userIds)){
			ZAlert.Error("请选择数据");
		}
		userIds=userIds.stream().distinct().collect(Collectors.toList());
		sysPositionAuthUserMapper.deleteUserFromUserPosition(userIds, positionId);
	}

	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
    public DataSet loadPositionDataSetUserHave(Long userId,Map<String, Object> params){
    	if(userId==null){
    		return DataSet.emptyDS();
    	}
    	ParamMap pm=ParamMap.filterParam(params);
    	pm.updateParam("userId", userId);
    	pm.getBoolean("enabled").ifPresent(v->pm.updateParam("enabled", v));
		pm.getStr("posName").ifPresent(v->pm.updateParam("posName", v));
		pm.getStr("posCode").ifPresent(v->pm.updateParam("posCode", v));
		DataSet ds=DataSet.newDS(sysPositionAuthUserMapper.selectPositionCountUserHave(userId,pm), sysPositionAuthUserMapper.selectPositionListUserHave(userId,pm));
		return ds;
    }
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
    public DataSet loadPositionDataSetUserNotHave(Long userId,Map<String, Object> params){
    	if(userId==null){
    		return DataSet.emptyDS();
    	}
    	ParamMap pm=ParamMap.filterParam(params);
    	pm.updateParam("userId", userId);
    	pm.getBoolean("enabled").ifPresent(v->pm.updateParam("enabled", v));
		pm.getStr("posName").ifPresent(v->pm.updateParam("posName", v));
		pm.getStr("posCode").ifPresent(v->pm.updateParam("posCode", v));
		DataSet ds=DataSet.newDS(sysPositionAuthUserMapper.selectPositionCountUserNotHave(userId,pm), sysPositionAuthUserMapper.selectPositionListUserNotHave(userId,pm));
		return ds;
    }
}