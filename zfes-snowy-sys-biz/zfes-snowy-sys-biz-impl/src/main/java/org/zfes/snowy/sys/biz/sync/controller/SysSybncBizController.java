/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.controller;

import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.sync.model.SysSybncBiz;
import org.zfes.snowy.sys.biz.sync.service.ISysSybncBizService;
import org.zfes.snowy.base.web.controller.BaseController;
@Controller 
@RequestMapping("/sys/sync/sybncBiz")
public class SysSybncBizController extends BaseController {

	private String viewPathPrefix="/sys";
	
	@Autowired
	private ISysSybncBizService sysSybncBizService;
	
	@RequestMapping(value={"/",""}, method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public Object index(){
		return forward(viewPathPrefix,"sysSybncBiz.html");
    }
	
	@RequiresPermissions(value="sysSybncBiz:loadSysSybncBizList",desc="数据同步业务列表")
    @RequestMapping(value="/loadSysSybncBizDataSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysSybncBizDataSet(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  sysSybncBizService.loadSysSybncBizDataSet(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="sysSybncBiz:addSysSybncBiz",desc="数据同步业务添加")
    @RequestMapping(value="/addSysSybncBiz", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addSysSybncBiz(SysSybncBiz sysSybncBiz){
		sysSybncBizService.createSysSybncBiz(sysSybncBiz);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysSybncBiz:loadSysSybncBiz",desc="数据同步业务加载")
    @RequestMapping(value="/loadSysSybncBiz", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysSybncBiz(Long id){
		Optional<SysSybncBiz> ops = sysSybncBizService.loadSysSybncBizById(id);
		return ajaxQuery("sysSybncBiz",ops);
    }
	
	@RequiresPermissions(value="sysSybncBiz:updateSysSybncBiz",desc="数据同步业务更新")
    @RequestMapping(value="/updateSysSybncBiz", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateSysSybncBiz(SysSybncBiz sysSybncBiz){
		sysSybncBizService.updateSysSybncBiz(sysSybncBiz);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysSybncBiz:deletSysSybncBiz",desc="数据同步业务删除")
    @RequestMapping(value="/deletSysSybncBiz", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletSysSybncBiz(Long id){
		sysSybncBizService.deletSysSybncBiz(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

}