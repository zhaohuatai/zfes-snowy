/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.service.impl;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.exceptions.ServiceLogicalException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.sys.biz.position.dao.SysPositionMapper;
import org.zfes.snowy.sys.biz.position.model.SysPosition;
import org.zfes.snowy.sys.biz.position.service.ISysPositionService;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.core.idcenter.IDGenerator;

@Service
@Lazy(true)
public class SysPositionServiceImpl implements ISysPositionService{
 	@Autowired
	private SysPositionMapper sysPositionMapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createSysPosition(SysPosition sysPosition) {
		ZBeanUtil.validateBean(sysPosition);
		SysPosition pos=sysPositionMapper.selectPositionByCode(sysPosition.getCode());
		if(!ZObjectUtil.isEmpty(pos)){
			ZAlert.Error("该编码对应的岗位已经存在");
		}
		sysPosition.setId(IDGenerator.genLongId());
		sysPositionMapper.insertSelective(sysPosition);
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<SysPosition> loadSysPosition(Long id) {
		if(id!=null){
			return Optional.ofNullable(sysPositionMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateSysPosition(SysPosition sysPosition) {
		ZBeanUtil.validateBean(sysPosition);
		SysPosition pos=sysPositionMapper.selectPositionByCode(sysPosition.getCode());
		if(!ZObjectUtil.isEmpty(pos) && (!(pos.getId().equals(sysPosition.getId())))){
			ZAlert.Error("该编码对应的岗位已经存在");
		}
		sysPositionMapper.updateByPrimaryKeySelective(sysPosition);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletSysPosition(Long id) {
		if(id!=null){
			sysPositionMapper.deleteByPrimaryKey(id);
		}else{
			throw new ServiceLogicalException("请选择要删除的数据");
		}
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void setPositionStatus(Long id, Boolean enabled) {
		ZAssert.notNull(id, "请选择数据");
	    ZAssert.notNull(enabled, "状态参数不能为空");
	   sysPositionMapper.updatePositionStatus(id,enabled);
	}
	
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadPositionDataSet(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		pm.getStr("name").ifPresent(v->pm.updateParam("name", "%"+v+"%"));
		    
		pm.getBoolean("enabled").ifPresent(v->pm.updateParam("enabled", v));
		
		pm.getLong("orgId").ifPresent(v->pm.updateParam("orgId", v));
			
		DataSet ds=DataSet.newDS(sysPositionMapper.selectSysPositionListCount(pm), sysPositionMapper.selectSysPositionList(pm));
		return ds;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<Long> loadPositionIdsByRoleId(Long roleId, Boolean enabled) {
		if(roleId==null || roleId==0){
			return Collections.emptyList();
		}
		List<Long> PositionIds=sysPositionMapper.selectIdsByRoleId(roleId,enabled);
		return PositionIds;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<Long> loadPositionIdsByOrgTreeCode(String deptId, Boolean enabled) {
		if(deptId==null){
			return Collections.emptyList();
		}
		List<Long> positionIds=sysPositionMapper.selectIdsByOrgTreeCode(deptId,enabled);
		return positionIds;
	}

	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadPositionByOrgTreeCode(String orgTreeCode,Map<String, Object> params) {
		if(ZStrUtil.hasNoText(orgTreeCode)){
			return DataSet.emptyDS();
		}
		 ParamMap pm =ParamMap.filterParam(params);
		 pm.updateParam("orgTreeCode", orgTreeCode);
		 DataSet ds=DataSet.newDS(sysPositionMapper.selectSysPositionListCount(pm), sysPositionMapper.selectSysPositionList(pm));
		return ds;
	}

}