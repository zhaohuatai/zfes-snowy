/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.dao;
import java.util.List;
import java.util.Map;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.sys.biz.position.model.SysPosition;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface SysPositionMapper extends BaseMapper<SysPosition>{
	/**
	 * @param params: {orgTreeCode like,name like,authRoleId =,code =  }
	 * @return
	 */
	List<Map<String, Object>> selectSysPositionList(ParamMap params);
	/**
	 * @param params: {orgTreeCode like,name like,authRoleId =,code =  }
	 * @return
	 */
	Long selectSysPositionListCount(ParamMap params);
	
	
	int batchInsertPosition(@Param("positionList")List<SysPosition> positionList);
	
	SysPosition selectPositionByCode(@Param("code")String code);
    
	int updatePositionStatus(@Param("id")Long id, @Param("enabled")Boolean enabled);
    
	int batchUpdatePositionStatus(@Param("positionIds")List<Long> positionIds, @Param("enabled")Boolean enabled);

	List<Long> selectIdsByRoleId(@Param("roleId")Long roleId, @Param("enabled")Boolean enabled);

	List<Long> selectIdsByOrgTreeCode(@Param("orgTreeCode")String orgTreeCode , @Param("enabled")Boolean enabled);

    SysPosition selectByRoleIdAndOrgTreeCode(@Param("orgTreeCode")String orgTreeCode, @Param("orgId")Long orgId,@Param("enabled")Boolean enabled);
	
	
}
