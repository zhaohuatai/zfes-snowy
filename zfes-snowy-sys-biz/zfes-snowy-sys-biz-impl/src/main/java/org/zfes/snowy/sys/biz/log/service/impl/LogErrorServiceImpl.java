/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.log.service.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.exceptions.ServiceLogicalException;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.sys.biz.log.dao.LogErrorMapper;
import org.zfes.snowy.sys.biz.log.model.LogError;
import org.zfes.snowy.sys.biz.log.service.ILogErrorService;
import org.zfes.snowy.sys.biz.log.consts.SnowyLogConsts;
@Service(SnowyLogConsts.LOG_SERVICE_BEAN_NAME.errorLogService)
public class LogErrorServiceImpl  implements ILogErrorService{
 	@Autowired
	private LogErrorMapper logErrorMapper;
	
 	@Async
 	@Override
	public void createLogError(LogError logError) {
		logError.setId(IDGenerator.genLongId());
		logErrorMapper.insertSelective(logError);
	}
	@Override
	public LogError loadLogError(Long id) {
		if(id!=null){
			return logErrorMapper.selectByPrimaryKey(id);
		}
		return null;
		
	}
	@Override
	public void deletLogError(Long id) {
		if(id!=null){
			logErrorMapper.deleteByPrimaryKey(id);
		}else{
			throw new ServiceLogicalException("请选择要删除的数据");
		}
		
	}
	@Override
	public DataSet loadLogErrorList(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		
		pm.getStr("username").ifPresent(value->pm.updateParam("username", "%"+value+"%"));
		
		DataSet ds=DataSet.newDS(logErrorMapper.selectLogErrorListCount(pm), logErrorMapper.selectLogErrorList(pm));
		return ds;
	}
}