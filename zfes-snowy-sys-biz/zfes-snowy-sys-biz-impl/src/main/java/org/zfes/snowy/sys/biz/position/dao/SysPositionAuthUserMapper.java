/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.position.dao;
import java.util.List;
import java.util.Map;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.sys.biz.position.model.SysPositionAuthUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
@Mapper
@Lazy(true)
public interface SysPositionAuthUserMapper extends BaseMapper<SysPositionAuthUser>{
	
    List<Long> selectUserIdsByPositionId(@Param("positionId")Long positionId, @Param("enabled")boolean enabled);
	
    Long selectUserCountByPositionId(@Param("positionId")Long positionId,@Param("enabled")Boolean enabled);

    List<Long> selectPositionIdsByUserId(@Param("userId")Long userId, @Param("enabled")Boolean enabled);
    
    List<String> selectPositionCodesByUserId(@Param("userId")Long userId, @Param("enabled")Boolean enabled);
	
	void deleteUserFromUserPosition(@Param("userIds")List<Long> userIds, @Param("positionId")Long positionId);
   
	void deletePositionFromUserPosition(@Param("positionIds")List<Long> positionIds, @Param("userId")Long userId);
	   
	void batchInsertPositionUser(@Param("positionUserList")List<SysPositionAuthUser> positionUserList);
	/**
	 * 
	 * @param userId
	 * @param pm:{orgTreeCode like,name like,authRoleId = ,code =  }
	 * @return
	 */
	List<Map<String, Object>> selectPositionListUserHave(@Param("userId")Long userId,ParamMap pm);
	
	/**
	 * @param userId
	 * @param pm :{orgTreeCode like,name like,authRoleId = ,code =  }
	 * @return
	 */
	long selectPositionCountUserHave(@Param("userId")Long userId,ParamMap pm);

	/**
	 * 
	 * @param userId
	 * @param pm:{orgTreeCode like,name like,authRoleId = ,code =  }
	 * @return
	 */
	List<Map<String, Object>> selectPositionListUserNotHave(@Param("userId")Long userId,ParamMap pm);
	/**
	 * 
	 * @param userId
	 * @param pm:{orgTreeCode like,name like,authRoleId = ,code =  }
	 * @return
	 */
	long selectPositionCountUserNotHave(@Param("userId")Long userId,ParamMap pm);
}
