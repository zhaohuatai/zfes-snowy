package org.zfes.snowy.sys.biz.org.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.org.model.SysOrganizationUser;
import org.zfes.snowy.sys.biz.org.service.ISysOrganizationUserService;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/sysOrganizationUser")
public class SysOrganizationUserController extends BaseController {

	private String viewPathPrefix="/";
	
	@Autowired
	private ISysOrganizationUserService sysOrganizationUserService;
	
	@RequestMapping(value={"/",""}, method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public Object index(){
		return forward(viewPathPrefix,"sysOrganizationUser.html");
    }
	
	@RequiresPermissions(value="sysOrganizationUser:loadSysOrganizationUserDataSet",desc="组织机构人员列表")
    @RequestMapping(value="/loadSysOrganizationUserDataSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysOrganizationUserDataSet(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  sysOrganizationUserService.loadSysOrganizationUserDataSet(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="sysOrganizationUser:createSysOrganizationUser",desc="组织机构人员添加")
    @RequestMapping(value="/createSysOrganizationUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object createSysOrganizationUser(SysOrganizationUser sysOrganizationUser){
		sysOrganizationUserService.createSysOrganizationUser(sysOrganizationUser);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysOrganizationUser:loadSysOrganizationUser",desc="组织机构人员加载")
    @RequestMapping(value="/loadSysOrganizationUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysOrganizationUser(Long id){
		Optional<SysOrganizationUser> ops = sysOrganizationUserService.loadSysOrganizationUserById(id);
		return ajaxQuery("sysOrganizationUser",ops);
    }
	
	@RequiresPermissions(value="sysOrganizationUser:updateSysOrganizationUser",desc="组织机构人员更新")
    @RequestMapping(value="/updateSysOrganizationUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateSysOrganizationUser(SysOrganizationUser sysOrganizationUser){
		sysOrganizationUserService.updateSysOrganizationUser(sysOrganizationUser);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysOrganizationUser:deletSysOrganizationUser",desc="组织机构人员删除")
    @RequestMapping(value="/deleteSysOrganizationUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deleteSysOrganizationUser(Long id){
		sysOrganizationUserService.deleteSysOrganizationUser(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

}