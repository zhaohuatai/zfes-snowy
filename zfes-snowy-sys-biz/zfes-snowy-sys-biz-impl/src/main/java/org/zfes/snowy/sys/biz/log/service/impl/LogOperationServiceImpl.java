/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.log.service.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.sys.biz.log.dao.LogOperationMapper;
import org.zfes.snowy.sys.biz.log.model.LogOperation;
import org.zfes.snowy.sys.biz.log.service.ILogOperationService;
import org.zfes.snowy.sys.biz.log.consts.SnowyLogConsts;
@Service(SnowyLogConsts.LOG_SERVICE_BEAN_NAME.operationLogService)
public class LogOperationServiceImpl  implements ILogOperationService{
 	@Autowired
	private LogOperationMapper logOperationMapper;
 	@Async
 	@Override
	public void createLogOperation(LogOperation logOperation) {
		logOperation.setId(IDGenerator.genLongId());
		logOperationMapper.insertSelective(logOperation);
	
		
	}
	@Override
	public LogOperation loadLogOperation(Long id) {
		if(id!=null){
			return logOperationMapper.selectByPrimaryKey(id);
		}
		return null;
		
	}
	@Override
	public void deletLogOperation(Long id) {
		if(id!=null){
			logOperationMapper.deleteByPrimaryKey(id);
		}
	}
	@Override
	public DataSet loadLogOperationList(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		pm.getStr("username").ifPresent(value->pm.updateParam("userName", "%"+value+"%"));
		DataSet ds=DataSet.newDS(logOperationMapper.selectLogOperationListCount(pm), logOperationMapper.selectLogOperationList(pm));
		return ds;
	}
}