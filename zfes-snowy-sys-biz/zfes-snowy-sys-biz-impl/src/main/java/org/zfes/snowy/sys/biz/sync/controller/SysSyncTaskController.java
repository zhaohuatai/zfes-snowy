///**
// * Copyright (c) 2016-2020 https://github.com/zhaohuatai
// *
// * contact 824069438@qq.com
// *
// */
//package org.zfes.snowy.sys.biz.sync.controller;
//
//import java.util.Map;
//import java.util.Optional;
//import org.apache.shiro.authz.annotation.RequiresPermissions;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.zfes.snowy.core.data.DataSet;
//import org.zfes.snowy.sys.biz.sync.model.SysSyncTask;
//import org.zfes.snowy.sys.biz.sync.service.ISysSyncTaskService;
//import org.zfes.snowy.base.web.controller.BaseController;
//@Controller
//@RequestMapping("/sys/sync/syncTask")
//public class SysSyncTaskController extends BaseController {
//
//	private String viewPathPrefix="/sys";
//
//	@Autowired
//	private ISysSyncTaskService sysSyncTaskService;
//
//	@RequestMapping(value={"/",""}, method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
//    public Object index(){
//		return forward(viewPathPrefix,"sysSyncTask.html");
//    }
//
//	@RequiresPermissions(value="sysSyncTask:loadSysSyncTaskList",desc="数据同步任务列表")
//    @RequestMapping(value="/loadSysSyncTaskDataSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public @ResponseBody  Object loadSysSyncTaskDataSet(@RequestParam Map<String,Object> params){
//	    DataSet dataSet=  sysSyncTaskService.loadSysSyncTaskDataSet(params);
//	    return dataSet;
//    }
//
//	@RequiresPermissions(value="sysSyncTask:addSysSyncTask",desc="数据同步任务添加")
//    @RequestMapping(value="/addSysSyncTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public  @ResponseBody Object addSysSyncTask(SysSyncTask sysSyncTask){
//		sysSyncTaskService.createSysSyncTask(sysSyncTask);
//    	return ajaxDoneSuccess("数据操作成功 ");
//    }
//
//	@RequiresPermissions(value="sysSyncTask:loadSysSyncTask",desc="数据同步任务加载")
//    @RequestMapping(value="/loadSysSyncTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public @ResponseBody  Object loadSysSyncTask(Long id){
//		Optional<SysSyncTask> ops = sysSyncTaskService.loadSysSyncTaskById(id);
//		return ajaxQuery("sysSyncTask",ops);
//    }
//
//	@RequiresPermissions(value="sysSyncTask:updateSysSyncTask",desc="数据同步任务更新")
//    @RequestMapping(value="/updateSysSyncTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public @ResponseBody  Object updateSysSyncTask(SysSyncTask sysSyncTask){
//		sysSyncTaskService.updateSysSyncTask(sysSyncTask);
//		return ajaxDoneSuccess("数据操作成功 ");
//    }
//
//	@RequiresPermissions(value="sysSyncTask:deletSysSyncTask",desc="数据同步任务删除")
//    @RequestMapping(value="/deletSysSyncTask", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public @ResponseBody  Object deletSysSyncTask(Long id){
//		sysSyncTaskService.deletSysSyncTask(id);
//		return ajaxDoneSuccess("数据操作成功 ");
//    }
//
//}