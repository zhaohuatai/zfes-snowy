/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.controller;

import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.sync.model.SysSyncConfig;
import org.zfes.snowy.sys.biz.sync.service.ISysSyncConfigService;
import org.zfes.snowy.base.web.controller.BaseController;
@Controller 
@RequestMapping("/sys/sync/syncConfig")
public class SysSyncConfigController extends BaseController {

	private String viewPathPrefix="/sys";
	
	@Autowired
	private ISysSyncConfigService sysSyncConfigService;
	
	@RequestMapping(value={"/",""}, method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public Object index(){
		return forward(viewPathPrefix,"sysSyncConfig.html");
    }
	
	@RequiresPermissions(value="sysSyncConfig:loadSysSyncConfigList",desc="数据同步配置列表")
    @RequestMapping(value="/loadSysSyncConfigDataSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysSyncConfigDataSet(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  sysSyncConfigService.loadSysSyncConfigDataSet(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="sysSyncConfig:addSysSyncConfig",desc="数据同步配置添加")
    @RequestMapping(value="/addSysSyncConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addSysSyncConfig(SysSyncConfig sysSyncConfig){
		sysSyncConfigService.createSysSyncConfig(sysSyncConfig);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysSyncConfig:loadSysSyncConfig",desc="数据同步配置加载")
    @RequestMapping(value="/loadSysSyncConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysSyncConfig(Long id){
		Optional<SysSyncConfig> ops = sysSyncConfigService.loadSysSyncConfigById(id);
		return ajaxQuery("sysSyncConfig",ops);
    }
	
	@RequiresPermissions(value="sysSyncConfig:updateSysSyncConfig",desc="数据同步配置更新")
    @RequestMapping(value="/updateSysSyncConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateSysSyncConfig(SysSyncConfig sysSyncConfig){
		sysSyncConfigService.updateSysSyncConfig(sysSyncConfig);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysSyncConfig:deletSysSyncConfig",desc="数据同步配置删除")
    @RequestMapping(value="/deletSysSyncConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletSysSyncConfig(Long id){
		sysSyncConfigService.deletSysSyncConfig(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

}