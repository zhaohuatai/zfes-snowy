package org.zfes.snowy.sys.biz.org.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.ComboboVo;
import org.zfes.snowy.core.data.Record;
import org.zfes.snowy.sys.biz.org.model.SysOrganization;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Mapper
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {
	
	Long selectListCount(ParamMap params);
	
	List<Record> selectMapList(ParamMap params);
	
	List<SysOrganization> selectModelList(ParamMap params);
	
	int updateStatus(Long id, Byte status);

	/**
	 * 查询组织机构树
	 * @param pm
	 * @return
	 */
	List<Map<String, Object>> selectOrganizationTree(ParamMap pm);

	/**
	 * 查询组织机构根节点
	 * @param rootId
	 * @return
	 */
	List<Long> selectRootIdList(@Param("rootId")Long rootId);

	/**
	* @Author : Victor
	* @Description : 通过用户id查询所属组织机构集合
	* @Date : 17:11 2017/11/9
	*
	*/
	SysOrganization selectSysOrganizationList(Long userId);



	List<Map<String, Object>> loadOrganizationByLevelCode(@Param("level")String level);

	List<ComboboVo> selectOrgranizationComboboV(ParamMap pm);

	SysOrganization selectSysOrganizationListByName(@Param("name")String name);

	SysOrganization selectSysOrganizationListByCode(@Param("code")String code);
}
