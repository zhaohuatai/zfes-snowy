/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.dao;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.sys.biz.sync.model.SysSybncBiz;
@Mapper
public interface SysSybncBizMapper extends BaseMapper<SysSybncBiz>{
	
	Long selectListCount(ParamMap params);
	
	List<Map<String, Object>> selectList(ParamMap params);
	
	
}
