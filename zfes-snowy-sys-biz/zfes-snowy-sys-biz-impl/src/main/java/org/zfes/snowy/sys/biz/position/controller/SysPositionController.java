package org.zfes.snowy.sys.biz.position.controller;
/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.shiro.util.SecurityUtil;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.sys.biz.position.model.SysPosition;
import org.zfes.snowy.sys.biz.position.service.ISysPositionAuthUserService;
import org.zfes.snowy.sys.biz.position.service.ISysPositionService;
@Controller 
@RequestMapping("/sys/sysPosition")
public class SysPositionController extends BaseController {
	
	@Autowired
	private ISysPositionService sysPositionService;
	@Autowired
	private ISysPositionAuthUserService positionAuthUserService;
	
	@RequiresPermissions(value="sysPosition:loadSysPositionList",desc="岗位信息列表")
    @RequestMapping(value="/loadSysPositionList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody Object loadSysPositionList(@RequestParam Map<String,Object> params){
	    DataSet dataSet= sysPositionService.loadPositionDataSet(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="sysPosition:addSysPosition",desc="岗位信息添加")
    @RequestMapping(value="/addSysPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addSysPosition(SysPosition sysPosition){
		sysPosition.setCreater(SecurityUtil.getUserId().get());
		sysPosition.setCreateTime(new Date());
		sysPositionService.createSysPosition(sysPosition);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysPosition:loadSysPosition",desc="岗位信息加载")
    @RequestMapping(value="/loadSysPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysPosition(Long id){
		Optional<SysPosition> sysPosition=sysPositionService.loadSysPosition(id);
		return ajaxQuery("sysPosition", sysPosition);
    }
	
	@RequiresPermissions(value="sysPosition:updateSysPosition",desc="岗位信息更新")
    @RequestMapping(value="/updateSysPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateSysPosition(SysPosition sysPosition){
		sysPositionService.updateSysPosition(sysPosition);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysPosition:deletSysPosition",desc="岗位信息删除")
    @RequestMapping(value="/deletSysPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletSysPosition(Long id){
		sysPositionService.deletSysPosition(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysPosition:loadPositionByOrgId",desc="加载组织机构下的岗位信息")
    @RequestMapping(value="/loadPositionByOrgId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadPositionByOrgId(String orgTreeCode,@RequestParam Map<String,Object> params){
		DataSet dataSet = sysPositionService.loadPositionByOrgTreeCode(orgTreeCode, params);
		return dataSet;
	}
	
	@RequiresPermissions(value="sysPosition:updatePositionStatus",desc="更新岗位的使用状态")
    @RequestMapping(value="/updatePositionStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updatePositionStatus(Long id,Boolean enabled){
		sysPositionService.setPositionStatus(id,enabled);
		return ajaxDoneSuccess("操作成功 ");
    }
	
	@RequiresPermissions(value="sysPosition:removeUserFromPosition",desc="从岗位中移除用户")
    @RequestMapping(value="/removeUserFromPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object removeUserFromPosition(@ListParam(Long.class)List<Long> userIds,Long positionId){
		positionAuthUserService.removeUserFromPosition(userIds, positionId);
	    return ajaxDoneSuccess("数据操作成功 ");
    }
	@RequiresPermissions(value="sysPosition:addUserToPosition",desc="为岗位增加用户")
    @RequestMapping(value="/addUserToPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object addUserToPosition(@ListParam(Long.class)List<Long> userIds,Long positionId){
		positionAuthUserService.addUserToPosition(userIds, positionId);
	    return ajaxDoneSuccess("数据操作成功 ");
    }
	@RequiresPermissions(value="sysPosition:loadPositionListByUserId",desc="根据用户Id加载岗位信息")
    @RequestMapping(value="/loadPositionListByUserId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadPositionListByUserId(Long userId,@RequestParam Map<String,Object> params){
			String posType=(String) params.get("posType");
			DataSet ds=DataSet.emptyDS();
			if(ZStrUtil.hasText(posType)){
				if("inPosition".equals(posType)){
					ds=positionAuthUserService.loadPositionDataSetUserHave(userId, params);
				}else if("notInPosition".equals(posType)){
					ds=positionAuthUserService.loadPositionDataSetUserNotHave(userId, params);
				}
			}
		return ds;
		
    }
}