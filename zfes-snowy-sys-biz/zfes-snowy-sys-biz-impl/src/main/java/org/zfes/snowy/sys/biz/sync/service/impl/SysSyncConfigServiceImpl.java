/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.service.impl;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.zfes.snowy.base.service.BaseService;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.sys.biz.sync.dao.SysSyncConfigMapper;
import org.zfes.snowy.sys.biz.sync.model.SysSyncConfig;
import org.zfes.snowy.sys.biz.sync.service.ISysSyncConfigService;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;

@Service
public class SysSyncConfigServiceImpl  implements ISysSyncConfigService{
 	@Autowired
	private SysSyncConfigMapper sysSyncConfigMapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createSysSyncConfig(SysSyncConfig sysSyncConfig) {
		ZBeanUtil.validateBean(sysSyncConfig);
		sysSyncConfig.setId(IDGenerator.genLongId());
		sysSyncConfigMapper.insertSelective(sysSyncConfig);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<SysSyncConfig> loadSysSyncConfigById(Long id) {
		if(id!=null){
			return Optional.ofNullable(sysSyncConfigMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateSysSyncConfig(SysSyncConfig sysSyncConfig) {
		ZBeanUtil.validateBean(sysSyncConfig);
		sysSyncConfigMapper.updateByPrimaryKeySelective(sysSyncConfig);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletSysSyncConfig(Long id) {
		if(id!=null){
			sysSyncConfigMapper.deleteByPrimaryKey(id);
			return;
		}
		ZAlert.Error("请选择要删除的数据");
		
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadSysSyncConfigDataSet(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		DataSet ds=DataSet.newDS(sysSyncConfigMapper.selectListCount(pm), sysSyncConfigMapper.selectList(pm));
		return ds;
	}
	
}