/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.sys.biz.sync.service.impl;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.zfes.snowy.base.service.BaseService;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.sys.biz.sync.dao.SysSybncBizMapper;
import org.zfes.snowy.sys.biz.sync.model.SysSybncBiz;
import org.zfes.snowy.sys.biz.sync.service.ISysSybncBizService;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;

@Service
public class SysSybncBizServiceImpl  implements ISysSybncBizService{
 	@Autowired
	private SysSybncBizMapper sysSybncBizMapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createSysSybncBiz(SysSybncBiz sysSybncBiz) {
		ZBeanUtil.validateBean(sysSybncBiz);
		sysSybncBiz.setId(IDGenerator.genLongId());
		sysSybncBizMapper.insertSelective(sysSybncBiz);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<SysSybncBiz> loadSysSybncBizById(Long id) {
		if(id!=null){
			return Optional.ofNullable(sysSybncBizMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateSysSybncBiz(SysSybncBiz sysSybncBiz) {
		ZBeanUtil.validateBean(sysSybncBiz);
		sysSybncBizMapper.updateByPrimaryKeySelective(sysSybncBiz);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletSysSybncBiz(Long id) {
		if(id!=null){
			sysSybncBizMapper.deleteByPrimaryKey(id);
			return;
		}
		ZAlert.Error("请选择要删除的数据");
		
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadSysSybncBizDataSet(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		DataSet ds=DataSet.newDS(sysSybncBizMapper.selectListCount(pm), sysSybncBizMapper.selectList(pm));
		return ds;
	}
	
}