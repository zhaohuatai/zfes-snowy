package org.zfes.snowy.sys.biz.org.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.Record;
import org.zfes.snowy.sys.biz.org.model.SysOrganizationUser;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysOrganizationUserMapper extends BaseMapper<SysOrganizationUser> {
	
	Long selectListCount(ParamMap params);
	
	List<Record> selectMapList(ParamMap params);
	
	List<SysOrganizationUser> selectModelList(ParamMap params);
	
	int updateStatus(Long id, Byte status);


	/**
	 * 加载组织机构内人员信息列表
	 * @param pm
	 * @return
	 */
	List<Map<String, Object>> selectUserFromOrganizationList(ParamMap pm);
	/**
	 * 统计组织机构内人员数
	 * @param pm
	 * @return
	 */
	Long selectUserFromOrganizationCount(ParamMap pm);
	/**
	 * 加载不再组织机构内的用户信息
	 * @param pm
	 * @return
	 */
	List<Map<String, Object>> selectUserNotInOrganizationList(ParamMap pm);
	/**
	 * 统计不在组织机构内用户的数量
	 * @param pm
	 * @return
	 */
	Long selectUserNotInOrganizationCount(ParamMap pm);


	/**
	 * 从组织机构为depId的组织机构中移除用户id在userIds的用户
	 * @param userIds :用户Id 集合
	 * @param deptId :部门Id
	 */
	void deleteUserFromDeptment(@Param("userIds")List<Long> userIds, @Param("orgId")Long orgId);
	/**
	 * 给部门批量添加用户
	 * @param departmentUserList
	 */
	void batchInsertDepartmentUser(@Param("departmentUserList")List<SysOrganizationUser> departmentUserList);
}
