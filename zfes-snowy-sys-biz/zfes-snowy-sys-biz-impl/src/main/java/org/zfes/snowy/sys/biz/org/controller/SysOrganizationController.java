package org.zfes.snowy.sys.biz.org.controller;

import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.shiro.util.SecurityUtil;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.ComboboVo;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.sys.biz.org.model.SysOrganization;
import org.zfes.snowy.sys.biz.org.service.ISysOrganizationService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/sys/sysOrganization")
public class SysOrganizationController extends BaseController {

	private String viewPathPrefix="/";
	
	@Autowired
	private ISysOrganizationService sysOrganizationService;
	
	@RequestMapping(value={"/",""}, method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public Object index(){
		return forward(viewPathPrefix,"sysOrganization.html");
    }
	
	@RequiresPermissions(value="sysOrganization:loadSysOrganizationDataSet",desc="组织机构列表")
    @RequestMapping(value="/loadSysOrganizationDataSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysOrganizationDataSet(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  sysOrganizationService.loadSysOrganizationDataSet(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="sysOrganization:createSysOrganization",desc="组织机构添加")
    @RequestMapping(value="/createSysOrganization", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object createSysOrganization(SysOrganization sysOrganization){
		sysOrganization.setCreater(SecurityUtil.getUserId().get());
		sysOrganizationService.createSysOrganization(sysOrganization);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	//@RequiresPermissions(value="sysOrganization:loadSysOrganization",desc="组织机构加载")
    @RequestMapping(value="/loadSysOrganization", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadSysOrganization(Long id){
		Optional<SysOrganization> ops = sysOrganizationService.loadSysOrganizationById(id);
		return ajaxQuery("sysOrganization",ops);
    }
	
	@RequiresPermissions(value="sysOrganization:updateSysOrganization",desc="组织机构更新")
    @RequestMapping(value="/updateSysOrganization", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateSysOrganization(SysOrganization sysOrganization){
		sysOrganizationService.updateSysOrganization(sysOrganization);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="sysOrganization:deletSysOrganization",desc="组织机构删除")
    @RequestMapping(value="/deleteSysOrganization", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deleteSysOrganization(Long id){
		sysOrganizationService.deleteSysOrganization(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

	//@RequiresPermissions(value="sysOrganization:loadOrganizationTree",desc="加载组织机构树")
	@RequestMapping(value="/loadOrganizationTree", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody  Object loadOrganizationTree(@RequestParam Map<String,Object> params){
		List<Map<String,Object>> organizationTree=sysOrganizationService.loadOrganizationTree(params);
		return organizationTree;
	}

	@RequiresPermissions(value="sysOrganization:UserFromOrganization",desc="组织机构中用户管理")
	@RequestMapping(value="/loadUserForOrganization")
	public @ResponseBody  Object loadUserForOrganization(@RequestParam Map<String,Object> params){
		DataSet ds=sysOrganizationService.loadUserForOrganization(params);
		return ds;
	}


	@RequiresPermissions(value="sysOrganization:UserFromOrganization",desc="组织机构中用户管理")
	@RequestMapping(value="/removeUserFromOrganization")
	public @ResponseBody  Object removeUserFromOrganization(@ListParam(Long.class)List<Long> userIds,Long organizationId){
		sysOrganizationService.removeUserFromOrganization(userIds, organizationId);
		return ajaxDoneSuccess("数据操作成功 ");
	}

	@RequiresPermissions(value="sysOrganization:UserFromOrganization",desc="组织机构中用户管理")
	@RequestMapping(value="/addUserToOrganization")
	public @ResponseBody  Object addUserToOrganization(@ListParam(Long.class)List<Long> userIds, @ListParam(Long.class)List<Long> appIds, Long organizationId){
		sysOrganizationService.addUserToOrganization(userIds,appIds,organizationId);
		return ajaxDoneSuccess("数据操作成功 ");
	}

	@RequestMapping(value = {"/loadOrganizationTableDic"},method = {RequestMethod.POST},produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public Object loadOrganizationByMappingCode() {
		List<Map<String, Object>> organizationList = sysOrganizationService.loadOrganizationTabelDic();
		return organizationList;
	}

	@RequestMapping(value="/loadOrgranizationComboboV", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody  Object loadOrgranizationComboboV( Boolean firstEmpty,String level,Long pid){
		List<ComboboVo> list=sysOrganizationService.selectOrgranizationComboboV(firstEmpty,level,pid);
		return ajaxQuery("snowyselectdata", list);
	}
}