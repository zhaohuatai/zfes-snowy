package org.zfes.snowy.sys.biz.org.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.sys.biz.org.dao.SysOrganizationUserMapper;
import org.zfes.snowy.sys.biz.org.model.SysOrganizationUser;
import org.zfes.snowy.sys.biz.org.service.ISysOrganizationUserService;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

//import org.zfes.snowy.base.service.BaseService;

//extends BaseService
@Service
public class SysOrganizationUserServiceImpl implements ISysOrganizationUserService {
 	@Autowired
	private SysOrganizationUserMapper sysOrganizationUserMapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createSysOrganizationUser(SysOrganizationUser sysOrganizationUser) {
		ZBeanUtil.validateBean(sysOrganizationUser);
		sysOrganizationUser.setId(IDGenerator.genLongId());
		sysOrganizationUserMapper.insertSelective(sysOrganizationUser);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<SysOrganizationUser> loadSysOrganizationUserById(Long id) {
		if(id!=null){
			return Optional.ofNullable(sysOrganizationUserMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateSysOrganizationUser(SysOrganizationUser sysOrganizationUser) {
		ZBeanUtil.validateBean(sysOrganizationUser);
		sysOrganizationUserMapper.updateByPrimaryKeySelective(sysOrganizationUser);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deleteSysOrganizationUser(Long id) {
			ZAssert.notNull(id, "请选择要操作的数据");
			sysOrganizationUserMapper.deleteByPrimaryKey(id);
			return;
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void setSysOrganizationUserStatus(Long id, Byte status) {
		ZAssert.notNull(id, "请选择要操作的数据");
		ZAssert.notNull(status, "状态参数错误");
		if(!Stream.of(new Byte[] {0,1,2}).anyMatch(val->val.equals(status))) {
			ZAssert.notNull(status, "状态参数错误");
		}
		sysOrganizationUserMapper.updateStatus( id,  status) ;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadSysOrganizationUserDataSet(Map<String, Object> params) {
		ParamMap pm= ParamMap.filterParam(params);
		DataSet ds= DataSet.newDS2(sysOrganizationUserMapper.selectListCount(pm), sysOrganizationUserMapper.selectMapList(pm));
		return ds;
	}
	
}