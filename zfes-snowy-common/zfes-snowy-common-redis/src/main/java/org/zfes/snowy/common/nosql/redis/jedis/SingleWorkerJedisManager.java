/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.common.nosql.redis.jedis;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisException;

public class SingleWorkerJedisManager implements SnowyJedisManager{
	public JedisPool jedisPool ;//APPC.getBean(ShardedJedisPool.class);
	
//------------------------keys------------------------------------------------ 	
	@Override
	public Set<String> keys(String pattern) {
		Set<String> keys =Collections.emptySet();
		Jedis jedis =getSingleResource();
		try{
			 keys = jedis.keys(pattern);
		}finally {
			returnResource(jedis);
		}
		return keys;
	}
	
	@Override
	public Set<String> keysBinary(String pattern) {
		Set<String> keys =Collections.emptySet();
		Jedis jedis =getSingleResource();
		try{
			Set<byte[]> res  = jedis.keys(toBytes(pattern));
			keys=res.stream().map(val->toString(val)).collect(Collectors.toSet());
		}finally {
			returnResource(jedis);
		}
		return keys;
	}
	
	@Override
	public long del(String key) {
		long result = 0;
		Jedis jedis = getSingleResource();
		try{
		  if (jedis.exists(key)){
		       result = jedis.del(key);
		  }
		} catch (Exception e) {
       	 throw new AppRuntimeException(e.getCause());
       }finally {
			returnResource(jedis);
		}  
		return result;
	}
	
	@Override
	public long delBinary(String key){
		long result = 0;
		Jedis jedis = getSingleResource();
		byte[] keyx=toBytes(key);
		try{
		  if (jedis.exists(keyx)){
		       result = jedis.del(keyx);
		  }
		}finally {
			returnResource(jedis);
		}  
		return result;
	}
	
	@Override
	public boolean exists(String key) {
		 boolean result = false;
		 Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.exists(key);
		 } catch (Exception e) {
        	 throw new AppRuntimeException(e.getCause());
        }finally {
			returnResource(jedis);
		} 
	  return result;
	}
	
	@Override
	public boolean existsBinary(String key) {
		 boolean result = false;
		 Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.exists(toBytes(key));
		 } catch (Exception e) {
        	 throw new AppRuntimeException(e.getCause());
        }finally {
			returnResource(jedis);
		} 
	  return result;
	}
	
	@Override
	public String type(String key) {
		 String result = "";
		 Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.type(key);
		 } catch (Exception e) {
        	 throw new AppRuntimeException(e.getCause());
        }finally {
			returnResource(jedis);
		 } 
	  return result;
	}
	
	@Override
	public String typeBinary(String key) {
		 String result = "";
		 Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.type(toBytes(key));
		 } catch (Exception e) {
        	 throw new AppRuntimeException(e.getCause());
        }finally {
			returnResource(jedis);
		 } 
	  return result;
	}
	
	@Override
	public Long expire( String key,  int seconds){
		Long result=0L ;
		Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.expire(key,seconds);
			} catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
				returnResource(jedis);
		  }
	     return result;
	}
	
	@Override
	public Long expireBinary( String key,  int seconds){
		Long result=0L ;
		Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.expire(toBytes(key),seconds);
			} catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
				returnResource(jedis);
		  }
	     return result;
	}
	
	@Override
	public Long expireAt( String key,  long unixTime){
		Long result=0L ;
		Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.expireAt(key,unixTime);
			} catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
				returnResource(jedis);
		  }
	     return result;
	}
	@Override
	public Long expireAtBinary( String key,  long unixTime){
		Long result=0L ;
		Jedis jedis = getSingleResource();
	     try{
	    	 result = jedis.expireAt(toBytes(key),unixTime);
			}catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
				returnResource(jedis);
		  }
	     return result;
	}
//---------------------String------------------------------------------------------ 
	
	@Override
	public String stringGet(String key){
		 String value = null;
		 Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	                value = jedis.get(key);
	                value =(ZStrUtil.hasText(value)&&!ZStrUtil.equalsIgnoreCase(nilChars, value)) ? value : null;
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
    }
    
	@Override
	public String stringGetBinary(String key){
		 String value = null;
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                byte[] res = jedis.get(keyx);
	                value=toString(res); 
	                value =(ZStrUtil.hasText(value)&&!ZStrUtil.equalsIgnoreCase(nilChars, value)) ? value : null;
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
  
	@Override
	public String stringSet(String key, String value, int cacheSeconds) {
		String result = null;
		Jedis jedis =getSingleResource();
        try {
            result = jedis.set(key, value);
            if (cacheSeconds != 0) {
                jedis.expire(key, cacheSeconds);
            }
        } catch (Exception e) {
        	 throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
    
	@Override
	public String stringSetBinary(String key, String value, int cacheSeconds) {
		String result = null;
		Jedis jedis =getSingleResource();
		byte[] keyx=toBytes(key);
        try {
            result = jedis.set(keyx, toBytes(value));
            if (cacheSeconds != 0) {
                jedis.expire(keyx, cacheSeconds);
            }
        } catch (Exception e) {
        	 throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
    
	//---------------------String for Object----------------------------------------------------- 
	
		@Override
		public Object stringForObjGet(String key){
			Object value = null;
			 Jedis jedis = getSingleResource();
			 byte[] keyx=toBytes(key);
		      try {
		            if (jedis.exists(keyx)) {
		                value = toObject(jedis.get(keyx));
		            }
		        }finally {
		            returnResource(jedis);
		        }
		    return value;
		}
	  
	    
		@Override
		public String stringForObjSet(String key, Object value, int cacheSeconds) {
			String result = null;
			Jedis jedis =getSingleResource();
			byte[] keyx=toBytes(key);
	        try {
	            result = jedis.set(keyx, toBytes(value));
	            if (cacheSeconds != 0) {
	                jedis.expire(keyx, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
		}

//-------------------------List---------------------------------------------------------------   
	@Override
	public List<String> listGet(String key) {
		  List<String> value = Collections.emptyList();
		  Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	                value = jedis.lrange(key, 0, -1);
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return value;
	}

	
	@Override
	public List<String> listGetBinary(String key) {
		  List<String> value = Collections.emptyList();
		  Jedis jedis = getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	List<byte[]> valuex = jedis.lrange(keyx, 0, -1);
	            	value=valuex.stream().map(val->toString(val)).collect(Collectors.toList());
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	
	@Override
	public List<String> listGet(String key ,long start, long end){
		  List<String> value = Collections.emptyList();
		  Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	            	value = jedis.lrange(key, start,end);
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return value;
    }
    
	@Override
	public List<String> listGBinary(String key ,long start, long end){
		  List<String> value = Collections.emptyList();
		  Jedis jedis = getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	List<byte[]> valuex = jedis.lrange(keyx, start,end);
	            	value=valuex.stream().map(val->toString(val)).collect(Collectors.toList());
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return value;
    }
	
	@Override
	public long listSet(String key, List<String> value, int cacheSeconds){
		 long result = 0;
		   Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	                jedis.del(key);
	            }
	            result = jedis.rpush(key, (String[])value.toArray());
	            if (cacheSeconds != 0) {
	                jedis.expire(key, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return result;
	 }
	
	@Override
	public long listSetBinary(String key, List<String> value, int cacheSeconds){
		 long result = 0;
		   Jedis jedis = getSingleResource();
		   byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                jedis.del(keyx);
	            }
	            List<byte[]> values= value.stream().map(val->toBytes(val)).collect(Collectors.toList());
	            result = jedis.rpush(keyx,(byte[][])values.toArray() );
	            if (cacheSeconds != 0) {
	                jedis.expire(key, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public long listLAdd(String key, String value) {
		 long result = 0;
		 Jedis jedis = getSingleResource();
	        try {
	            result = jedis.lpush(key, value);
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public long listLAddBinary(String key, String value) {
		 long result = 0;
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            result = jedis.lpush(keyx,toBytes(value));
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public long listRAdd(String key,String value) {
		 long result = 0;
		 Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            result = jedis.rpush(key, value);
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public long listRAddBinary(String key,String value) {
		 long result = 0;
		 Jedis jedis = null;
		 byte[] keyx=toBytes(key);
	        try {
	            jedis = getSingleResource();
	            result = jedis.rpush(keyx, toBytes(value));
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public long listRemove(String key,long count,  String value){
		 long result = 0;
		 Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            result = jedis.lrem(key, count, value);
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
    
	@Override
	public long listRemoveBinary(String key,long count, String value){
		 long result = 0;
		 Jedis jedis = null;
		 byte[] keyx=toBytes(key);
	        try {
	            jedis = getSingleResource();
	            result = jedis.lrem(keyx, count, toBytes(value));
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}

	

//--------------------------List for Object---------------------------------------------------------------------------------------------

	@Override
	public List<?> listForObjGet(String key) {
		 List<Object> value = Collections.emptyList();
		 Jedis jedis =getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                List<byte[]> list = jedis.lrange(keyx, 0, -1);
	                value = list.stream().map(item->toObject(item)).collect(Collectors.toList());
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	@Override
	public List<?> listForObjGet(String key ,long start, long end) {
		 List<Object> value = Collections.emptyList();
		 Jedis jedis =getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                List<byte[]> list = jedis.lrange(keyx, start, end);
	                value = list.stream().map(item->toObject(item)).collect(Collectors.toList());
	            }
	        }  catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        }finally {
	            returnResource(jedis);
	        }
	        return value;
	}
//	public List<Object> listForObjGet(String key ,long start, long end,Class<?> objClazz){
//		
//	}

	@Override
	public long listForObjSet(String key, List<?> value, int cacheSeconds) {
		 long result = 0;
		 Jedis jedis =getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                jedis.del(keyx);
	            }
	            List<byte[]> list=value.stream().map(item->toBytes(item)).collect(Collectors.toList());
	            
	            result = jedis.rpush(toBytes(key), (byte[][])list.toArray());
	            if (cacheSeconds != 0) {
	                jedis.expire(key, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}

	
	@Override
	public long listForObjAdd(String key, Object value,boolean isFromLeft) {
		  long result = 0;
		  Jedis jedis = getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	        	if(isFromLeft){
	        		result = jedis.lpush(keyx, toBytes(value));
	        	}else{
	        		result = jedis.lpush(keyx, toBytes(value));
	        	}
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public long listForObjRemove(String key,long count, Object value){
		 long result = 0;
		 Jedis jedis = null;
		 byte[] keyx=toBytes(key);
	        try {
	            jedis = getSingleResource();
	            result = jedis.lrem(keyx, count, toBytes(value));
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public long listLen(String key) {
		  long result = 0;
		  Jedis jedis =getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	        	result =jedis.llen(keyx);
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public long listLenBinary(String key) {
		  long result = 0;
		  Jedis jedis =getSingleResource();
	        try {
	        	result =jedis.llen(toBytes(key));
	        } catch (Exception e) {
	        	 throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	
//--------------------------------------Set--------------------------------------------------------------- 
	@Override
	public Set<String> setGet(String key) {
		 Set<String> value =Collections.emptySet();
		 Jedis jedis =  getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	                value = jedis.smembers(key);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	@Override
	public Set<String> setGetBinary(String key) {
		 Set<String> value =Collections.emptySet();
		 Jedis jedis =  getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	Set<byte[]> valuex = jedis.smembers(keyx);
	            	value=valuex.stream().map(val->toString(val)).collect(Collectors.toSet());
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	@Override
	public long setSet(String key, Set<String> value, int cacheSeconds) {
		  long result = 0;
		  Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            if (jedis.exists(key)) {
	                jedis.del(key);
	            }
	            result = jedis.sadd(key, (String[])value.toArray());
	            if (cacheSeconds != 0) {
	                jedis.expire(key, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public long setSetBinary(String key, Set<String> value, int cacheSeconds) {
		  long result = 0;
		  Jedis jedis =getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                jedis.del(keyx);
	            }
	            List<byte[]> values= value.stream().map(val->toBytes(val)).collect(Collectors.toList());
	            result = jedis.sadd(keyx,(byte[][])values.toArray() );
	            if (cacheSeconds != 0) {
	                jedis.expire(keyx, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}

	@Override
	public long setAdd(String key, String value, int cacheSeconds) {
		long result = 0;
		Jedis jedis = null;
        try {
            jedis = getSingleResource();
            result = jedis.sadd(key, value);
        } catch (Exception e) {
        	throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
    
	@Override
	public long setAddBinary(String key,String value, int cacheSeconds) {
		long result = 0;
		Jedis jedis = null;
        try {
            jedis = getSingleResource();
            result = jedis.sadd(toBytes(key), toBytes(value));
        } catch (Exception e) {
        	throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
	
	@Override
	public long setRemvoe(String key, String value) {
		long result = 0;
		Jedis jedis = null;
        try {
            jedis = getSingleResource();
            result = jedis.srem(key, value);
        } catch (Exception e) {
        	throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
	
	@Override
	public long setRemvoeBinary(String key, String value) {
		long result = 0;
		Jedis jedis = null;
        try {
            jedis = getSingleResource();
            result = jedis.srem(toBytes(key), toBytes(value));
        } catch (Exception e) {
        	throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
	
	@Override
	public Set<Object> setForObjGet(String key){
		 Set<Object> value =Collections.emptySet();
		 Jedis jedis =  getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	Set<byte[]> valuex = jedis.smembers(keyx);
	            	value=valuex.stream().map(val->toObject(val)).collect(Collectors.toSet());
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
    
	@Override
	public long setForObjSet(String key,Set<?> values,int cacheSeconds){
		  long result = 0;
		  Jedis jedis =getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                jedis.del(keyx);
	            }
	            List<byte[]> valuex= values.stream().map(val->toBytes(val)).collect(Collectors.toList());
	            result = jedis.sadd(keyx,(byte[][])valuex.toArray() );
	            if (cacheSeconds != 0) {
	                jedis.expire(keyx, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	    return result;
	}
   
	@Override
	public long setForObjAdd(String key, Object value) {
		long result = 0;
		Jedis jedis = null;
        try {
            jedis = getSingleResource();
            result = jedis.sadd(toBytes(key), toBytes(value));
        } catch (Exception e) {
        	throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
    
	@Override
	public long setForObjRemvoe(String key, Object value) {
		long result = 0;
		Jedis jedis = null;
        try {
            jedis = getSingleResource();
            result = jedis.srem(toBytes(key), toBytes(value));
        } catch (Exception e) {
        	throw new AppRuntimeException(e.getCause());
        } finally {
            returnResource(jedis);
        }
        return result;
	}
//--------------------------Map--hash--------------------------------------------------------	

	@Override
	public Map<String, String> hashGetAll(String key) {
		Map<String, String> value = Collections.emptyMap();
		 Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	                value = jedis.hgetAll(key);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	@Override
	public Map<String, String> hashGetAllBinary(String key) {
		Map<String, String> value = Collections.emptyMap();
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	Map<byte[],byte[]>  valuex = jedis.hgetAll(keyx);
	            	value=zforPrivateCovertByteMapToStrMap(valuex);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	
	
    
	@Override
	public String hashGet(String key, String mapKey) {
		String value = "";
		 Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	            	value=jedis.hget(key, mapKey);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
    }
    
	@Override
	public String hashGetBinary(String key, String mapKey){
		String value = "";
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	byte[] valuex=jedis.hget(keyx, toBytes(mapKey));
	            	value=toString(valuex);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
    
    
	@Override
	public String hashSetAll(String key, Map<String, String> value, int cacheSeconds) {
		 String result = null;
		  Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	                jedis.del(key);
	            }
	            result = jedis.hmset(key, value);
	            if (cacheSeconds != 0) {
	                jedis.expire(key, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public String hashSetAllBinary(String key, Map<String, String> value, int cacheSeconds) {
		 String result = null;
		  Jedis jedis = getSingleResource();
		  byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	                jedis.del(keyx);
	            }
	            result = jedis.hmset(keyx, zforPrivateCovertStrMaptoByteMap(value));
	            if (cacheSeconds != 0) {
	                jedis.expire(keyx, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}

	@Override
	public String hashSet(String key, String mapKey,String  value) {
		 String result = null;
		 Jedis jedis =getSingleResource();
	        try {
	            Map<String, String> valuex=Collections.emptyMap();
	            valuex.put(mapKey, value);
	            result = jedis.hmset(key, valuex);
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public String hashSetBinary(String key, String mapKey,String  value) {
		 String result = null;
		 Jedis jedis = getSingleResource();
	        try {
	            Map<byte[],byte[]> valuex=Collections.emptyMap();
	            valuex.put(toBytes(mapKey), toBytes(value));
	            result = jedis.hmset(toBytes(key), valuex);
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public long hashRemove(String key, String mapKey) {
		 long result = 0;
	        Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            result = jedis.hdel(key, mapKey);
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public long hashRemoveBinary(String key, String mapKey) {
		 long result = 0;
	        Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            result = jedis.hdel(toBytes(key), toBytes(mapKey));
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	
	@Override
	public boolean hashExists(String key, String mapKey) {
		boolean result = false;
		 Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            result = jedis.hexists(key, mapKey);
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
    }
    
	@Override
	public  boolean hashExistsBinary(String key, String mapKey) {
		 boolean result = false;
		  Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            result = jedis.hexists(toBytes(key), toBytes(mapKey));
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
    }
	@Override
	public  Set<String> hashKeysBinary(String key){
		Set<String> values= Collections.emptySet();
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	Set<byte[]> keysx=jedis.hkeys(keyx);
	            	values=keysx.stream().map(val-> toString(val)).collect(Collectors.toSet());
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	      return values;
	}
	@Override
	public  Set<String> hashKeys(String key){
		Set<String> keys= Collections.emptySet();
		 Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	            	 keys=jedis.hkeys(key);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return keys;
	}
	@Override
	public  long hashLen(String key){
		long result = 0;
		 Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(key)) {
	            	result= jedis.hlen(key);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
  	  
	@Override
	public  long hashLenBinary(String key){
		long result = 0;
		 Jedis jedis = getSingleResource();
	        try {
	            if (jedis.exists(toBytes(key))) {
	            	result= jedis.hlen(toBytes(key));
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
  	  
//-------------------------hash for Boject----------------------------------------------------------------	
	@Override
	public String hashForObjSetAll(String key, Map<String, Object> value, int cacheSeconds){
		 String result = null;
		 Jedis jedis = null;
		 byte[] keyx=toBytes(key);
	        try {
	            jedis = getSingleResource();
	            if (jedis.exists(keyx)) {
	                jedis.del(keyx);
	            }
	            Map<byte[], byte[]> map = zforPrivateCovertObjMapToByteMap(value);
	            result = jedis.hmset(toBytes(key), (Map<byte[], byte[]>)map);
	            if (cacheSeconds != 0) {
	                jedis.expire(key, cacheSeconds);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public Map<String, Object> hashForObjGetAll(String key){
		Map<String, Object> value = Collections.emptyMap();
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	Map<byte[],byte[]>  valuex = jedis.hgetAll(keyx);
	            	value=zforPrivateCovertByteMapToObjMap(valuex);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	@Override
	public String hashForObjSet(String key, String mapKey, Object value) {
		String result = null;
		 Jedis jedis = null;
	        try {
	            jedis = getSingleResource();
	            Map<byte[],byte[]> valuex=new HashMap<>();
	            valuex.put(toBytes(mapKey), toBytes(value));
	            result = jedis.hmset(toBytes(key), valuex);
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return result;
	}
	@Override
	public Object hashForObjGet(String key, String mapKey){
		Object value = null;
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
	        try {
	            if (jedis.exists(keyx)) {
	            	byte[] valuex=jedis.hget(keyx, toBytes(mapKey));
	            	value=toObject(valuex);
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	}
	@Override
	public List<?> hashForObjMGet(String key, List<String> mapKeys){
		List<Object> value = Collections.emptyList();
		if(mapKeys==null||mapKeys.isEmpty()){
			return value;
		}
		 Jedis jedis = getSingleResource();
		 byte[] keyx=toBytes(key);
		 byte[][] fieldsParam=mapKeys.stream().map(val-> toBytes(val)).toArray(byte[][]::new);
	        try {
	            if (jedis.exists(keyx)) {
	            	List<byte[]> valuex=jedis.hmget(keyx, fieldsParam);
	            	value=valuex.stream().filter(valx ->(!ZObjectUtil.isEmpty(valx)) ).map(val-> toObject(val)).collect(Collectors.toList());
	            }
	        } catch (Exception e) {
	        	throw new AppRuntimeException(e.getCause());
	        } finally {
	            returnResource(jedis);
	        }
	        return value;
	  }
//-------------------------over-------------------------------------------------	
	@Override
	public ShardedJedis getShardedResource() throws JedisException {
		return null;
	}

	@Override
	public Jedis getSingleResource() throws JedisException {
      Jedis jedis = null;
      try {
          jedis = getJedisPool().getResource();
      } catch (Exception e) {
          throw new JedisConnectionException(e);
      }
      return jedis;
	}

	public JedisPool getJedisPool() {
		return jedisPool;
	}

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}

	@Override
	public void flushDB() {
		getSingleResource().flushDB();
		
	}

	@Override
	public Long dbSize() {
		return getSingleResource().dbSize();
	}



	
}

