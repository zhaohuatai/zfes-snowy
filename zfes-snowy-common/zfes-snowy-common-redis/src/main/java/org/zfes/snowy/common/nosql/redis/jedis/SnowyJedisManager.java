/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.common.nosql.redis.jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zfes.snowy.core.util.ZSerialUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.exceptions.JedisException;
public interface SnowyJedisManager {
	
	 static final String nilChars="nil";
	 void flushDB();
	 Long dbSize();
	
//------------------keys--------------------------------------------
	
	 Set<String> keys(String pattern);
	 
	 Set<String> keysBinary(String pattern);
	 
     long del(String key) ;
     
     long delBinary(String key) ;
     
     boolean exists(String key) ;
     
     boolean existsBinary(String key) ;
     
     String type( String key);
     
     String typeBinary( String key);
     
     Long expire( String key,  int seconds);
     
     Long expireBinary( String key,  int seconds);
     
     Long expireAt( String key,  long unixTime);
     
     Long expireAtBinary( String key,  long unixTime);
	
//---------------------------String------------------------------------------------------------- 	
      String stringGet(String key);
      String stringGetBinary(String key);
    
      String stringSet(String key, String value, int cacheSeconds) ;
      
      String stringSetBinary(String key, String value, int cacheSeconds) ;
    
//---------------------------String for Object-------------------------------------------------------------       
      Object stringForObjGet(String key);
    
      String stringForObjSet(String key, Object value, int cacheSeconds) ; 
      
//-------------------------List------------------------------------------------------   
      List<String> listGet(String key);
      
      List<String> listGetBinary(String key);
      
      List<String> listGet(String key ,long start, long end);
      
      List<String> listGBinary(String key ,long start, long end);
      
      long listSet(String key, List<String> values, int cacheSeconds);
      
      long listSetBinary(String key, List<String> values, int cacheSeconds);
      
      long listLAdd(String key,  String values);
      
      long listLAddBinary(String key,  String value);
      
      long listRAdd(String key,  String values);
      
      long listRAddBinary(String key,  String value);
      
      long listRemove(String key,long count,  String value);
      
      long listRemoveBinary(String key,long count,  String value);
      
      long listLen(String key) ;
      
      long listLenBinary(String key);
      
//-------------------------List for Object------------------------------------------------------      
      
      List<?> listForObjGet(String key) ;
      
      List<?> listForObjGet(String key ,long start, long end);
      
//      public List<Object> listForObjGet(String key ,long start, long end,Class<?> objClazz);
      
      long listForObjSet(String key, List<?> value, int cacheSeconds);
    
      long listForObjAdd(String key, Object value,boolean isFromLeft) ;
      /**
       * @param key
       * @param count : <br/>count > 0: 从表头开始向表尾搜索，移除与 VALUE 相等的元素，数量为 COUNT 。<br/>count < 0 : 从表尾开始向表头搜索，移除与 VALUE 相等的元素，数量为 COUNT 的绝对值。<br/>count = 0 : 移除表中所有与 VALUE 相等的值。<br/><br/>
       * @param value
       * @return
       */
      long listForObjRemove(String key,long count, Object value);
      
//-------------------------Set--------------------------------------------------------------    
     Set<String> setGet(String key);
     
     Set<String> setGetBinary(String key);
     
     long setSet(String key, Set<String> value, int cacheSeconds) ;
     
     long setSetBinary(String key, Set<String> value, int cacheSeconds) ;
     
     long setAdd(String key, String value, int cacheSeconds) ;
     
     long setAddBinary(String key, String value, int cacheSeconds) ;
     
     long setRemvoe(String key, String value) ;
     
     long setRemvoeBinary(String key, String value);
//-------------------------Set for Object------------------------------------------------------   
     
     Set<?> setForObjGet(String key);
     
     long setForObjSet(String key,Set<?> values,int cacheSeconds);
    
     long setForObjAdd(String key, Object value) ;
     
     long setForObjRemvoe(String key, Object value) ;
    
//--------------------------hash----------------------------------------------------------
     
      Map<String, String> hashGetAll(String key) ;
      
      Map<String, String> hashGetAllBinary(String key) ;
      
      String hashSetAll(String key, Map<String, String> value, int cacheSeconds) ;
      
      String hashSetAllBinary(String key, Map<String, String> value, int cacheSeconds) ;
      
      String hashGet(String key, String mapKey) ;
      
      String hashGetBinary(String key, String mapKey);
      
      String hashSet(String key, String mapKey,String  value) ;
      
      String hashSetBinary(String key, String mapKey,String  value) ;
      
      
 //-----------------------------------------------------------    
      
      long hashRemove(String key, String mapKey) ;
      
      long hashRemoveBinary(String key, String mapKey) ;
      
      boolean hashExists(String key, String mapKey) ;
      
      boolean hashExistsBinary(String key, String mapKey) ;
      
      Set<String> hashKeysBinary(String key);
      
  	  Set<String> hashKeys(String key);
  	  
  	  long hashLen(String key);
  	  
  	  long hashLenBinary(String key);
  	
 //--------------------------Map for Boject----------------------------------------------------------      
    
      Map<String, Object> hashForObjGetAll(String key);
      
      
      String hashForObjSetAll(String key, Map<String, Object> value, int cacheSeconds);
      
      String hashForObjSet(String key, String mapKey, Object value) ;
      
      Object hashForObjGet(String key, String mapKey);
      
      List<?> hashForObjMGet(String key, List<String> mapKeys);
      
    
//-----------------------------------------------------------------------------
    
     ShardedJedis getShardedResource() throws JedisException ;
    
     Jedis getSingleResource() throws JedisException;
    
     default void returnBrokenResource(ShardedJedis ShardedJedis){
    	if (ShardedJedis != null) {
		 	ShardedJedis.close();
		 	if(ShardedJedis != null){
		 		// shardedJedisPool.returnBrokenResource(ShardedJedis);
		 	}
            
        }
    }
     default void returnBrokenResource(Jedis jedis){
    	if (jedis != null) {
    		jedis.close();
		 	if(jedis != null){
		 		// jedisPool.returnResource(jedis);
		 	}
            
        }
    }
    
     default  void returnResource(ShardedJedis jedis){
    	if (jedis != null) {
    		jedis.close();
		 	if(jedis != null){
		 		// shardedJedisPool.returnResource(ShardedJedis);
		 	}
            
        }
    }
     default  void returnResource(Jedis jedis){
    	if (jedis != null) {
    		jedis.close();
		 	if(jedis != null){
		 		// jedisPool.returnResource(jedis);
		 	}
            
        }
    }
//-----------------------------------------------------------------------------   
    
   default byte[] toBytes(Object object){
        return ZSerialUtil.fstSerialize(object);
    } 
    default Object toObject(byte[] bytes){
        return ZSerialUtil.fstDeserialize(bytes);
    }
   
     default String toString(byte[] bytes){
        return (String)toObject(bytes);
    }
     
 	 default Map<byte[],byte[]> zforPrivateCovertStrMaptoByteMap(Map<String, String> value){
		Map<byte[],byte[]>  valuex = new HashMap<>();
    	for(Map.Entry<String, String> item :value.entrySet()){  
    		valuex.put(toBytes(item.getKey()), toBytes(item.getValue()));
        }
    	return valuex;
	}
	default Map<byte[],byte[]> zforPrivateCovertObjMapToByteMap(Map<String, Object> value){
		Map<byte[],byte[]>  valuex = new HashMap<>();
    	for(Map.Entry<String, Object> item :value.entrySet()){  
    		valuex.put(toBytes(item.getKey()), toBytes(item.getValue()));
        }
    	return valuex;
	}
	default Map<String, String>  zforPrivateCovertByteMapToStrMap(Map<byte[],byte[]> value){
		Map<String, String> valuex = new HashMap<>();
    	for(Map.Entry<byte[],byte[]> item :value.entrySet()){  
    		valuex.put(toString(item.getKey()), toString(item.getValue()));
        }  
    	return valuex;
	}
	default Map<String,Object>  zforPrivateCovertByteMapToObjMap(Map<byte[],byte[]> value){
		Map<String, Object> valuex = new HashMap<>();
    	for(Map.Entry<byte[],byte[]> item :value.entrySet()){  
    		valuex.put(toString(item.getKey()), toObject(item.getValue()));
        }  
    	return valuex;
	}
	
}
