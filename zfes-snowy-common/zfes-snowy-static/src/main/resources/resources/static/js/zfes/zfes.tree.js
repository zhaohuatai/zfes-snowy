const zfesZTree = {
		getTreeNodes:function(eleId){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			let nodes = treeObj.transformToArray(treeObj.getNodes());
			return nodes;
		},
		getTreeNodesParam:function(eleId,proName,proValue,pid){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			if(zfesUtil.isStrNull(pid)){
				pid=null
			}
			let nodes = treeObj.transformToArray(treeObj.getNodesByParam(proName, proValue, pid));
			return nodes;
		},
		getTreeNodeById:function(eleId,id){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			let nodes = treeObj.transformToArray(treeObj.getNodesByParam("id", id, null));
			return nodes[0];//仅仅返回当前节点，子节点不返回
		},
		getTreeNodeByCode:function(eleId,code){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			let nodes = treeObj.transformToArray(treeObj.getNodesByParam("code", code, null));
			return nodes[0];//仅仅返回当前节点，子节点不返回
		},
		reloadTree:function(eleId){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			treeObj.refresh();
		},
		refreshTreeAll:function(eleId,param){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			treeObj.setting.async.otherParam=param;
			treeObj.reAsyncChildNodes(null, "refresh");
		},
		refreshTreeSelectNode:function(eleId){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			let nodes = treeObj.getSelectedNodes();
			if (nodes.length>0) {
				treeObj.reAsyncChildNodes(nodes[0], "refresh");
			}
		},
		getCheckedNodes:function(eleId){
			let treeObj = $.fn.zTree.getZTreeObj(eleId);
			let nodes = treeObj.getCheckedNodes();
			return nodes;
		},
		getCheckedNode:function(eleId){
			return ZFTree.getCheckedNodes(eleId)[0];
		}	
};
