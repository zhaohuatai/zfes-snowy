const zfesEditor={
		init:function(ele){
			var height=$("#"+ele).css("height");
			var width=$("#"+ele).css("width");
			var minHeight=$("#"+ele).css("min-height");
			$("#"+ele).Editor();
			$("#"+ele).Editor("setText", $("#"+ele).val());
			//监听光标获得
			$(".Editor-editor").each(function(){
				$(this).on("blur",function(e){
					var elex=e.target;
					var textareaEle=$(elex).closest(".Editor-container").prev();
					var taeId=$(textareaEle).attr("id");
					$(textareaEle).val(zfesEditor.getEditorText(taeId));
				});
			});
			if(!zfesUtil.isStrNull(height)){
				$(".Editor-editor").css("height",height);
			}
			if(!zfesUtil.isStrNull(width)){
				//$(".Editor-editor").css("width",width);
			}
			if(!zfesUtil.isStrNull(minHeight)){
				$(".Editor-editor").css("min-height",minHeight);
			}
		},
		getEditorText:function(ele){
			return $("#"+ele).Editor("getText");
		}
}