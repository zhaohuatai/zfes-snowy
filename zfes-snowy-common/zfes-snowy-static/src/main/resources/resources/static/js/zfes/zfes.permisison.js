const zfesPermStore={
		permCodesJsonName:"permCodes",
		permCodesStoreName:"zfes_permission_storage",
		roleCodesJsonName:"roleCodes",
		roleCodesStoreName:"zfes_role_storage",
		initPerm:function(url,param,callback){
			zfesAjax.ajax(url,param, function(data){
				let permCodesJsonName=zfesPermStore.permCodesJsonName;
				let permArray=data.data[permCodesJsonName];
				
				let roleCodesJsonName=zfesPermStore.roleCodesJsonName;
				let roleCode=data.data[roleCodesJsonName];
				
				zfesPermStore.setPerms(permArray);
				zfesPermStore.setRole(roleCode);
				if(callback){
					callback();
				}
	    	});
		},
		setPerms:function(permsArray){
			let finalData = {};
			 if(!zfesSessionStore.isEmpty(permsArray)){
				 let len=permsArray.length;
				 for( let i=0; i<len; i++ ){//遍历数组[p1,p2,p3]，--》转化为对象{p1:1,p2:1,P3:1}
					let permCode = permsArray[i];
					let key=zfesPermStore.convertCode(permCode);
					finalData[key]=1;//遍历数组[p1,p2,p3]，--》转化为对象{p1:1,p2:1,P3:1}
				 } 
			  zfesSessionStore.remove(zfesPermStore.permCodesStoreName);
			  zfesSessionStore.set(zfesPermStore.permCodesStoreName,finalData);
			}
		},
		setRole:function(roleCode){
			 if(!zfesSessionStore.isEmpty(roleCode)){
				  zfesSessionStore.remove(zfesPermStore.roleCodesStoreName);
				  zfesSessionStore.set(zfesPermStore.roleCodesStoreName,roleCode);
				} 
		},
		getRole:function(){
			let roleCode=zfesSessionStore.get(zfesPermStore.roleCodesStoreName);
			return roleCode;
		},
		
		havePermission:function(permCode){
			let strx=zfesPermStore.convertCode(permCode);
			let permCodes=zfesSessionStore.get(zfesPermStore.permCodesStoreName);
			if(zfesSessionStore.isEmpty(permCodes)){
				return false;
			}
			if(!zfesSessionStore.isEmpty(permCodes[strx])){
				return true;
			}
		},
		convertCode:function(permCode){
			return permCode.replace(":","_");
		},
		reConvertCode:function(permCode){
			return permCode.replace("_",":");
		}
}