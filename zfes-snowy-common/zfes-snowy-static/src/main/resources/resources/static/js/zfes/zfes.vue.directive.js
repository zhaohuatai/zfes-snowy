const zfesVueDirective={
	zfesKselectOnchange: "zfes_kselect_onchange",
	zfesKselectInited:"zfes_kselect_inited",
	zfesVueEventBus:new Vue(),
	newBus:function(){
		zfesVueDirective.zfesVueEventBus=null;
		zfesVueDirective.zfesVueEventBus=new Vue();
		return zfesVueDirective.zfesVueEventBus;
	},
	getBus:function(){
		return zfesVueDirective.zfesVueEventBus;
	},
	getKselectOnchangeEventName:function(domId){
		return zfesVueDirective.zfesKselectOnchange+"_"+domId;
	},
	getKselectOnInitEventName:function(domId){
		return zfesVueDirective.zfesKselectInited+"_"+domId;
	}
};
/**
 * <ul id="menuTreeShow" 
 * v-ktree="{url:'auth/authMenu/loadAuthMenuTree',onClick:'zTreeOnClick',onRightClick:'zTreeOnRightClick',chkEnable:false,chkStyle:'checkbox'}" 
 * class="ztree"></ul>
				
 */
Vue.directive('ktree', {
	 inserted: function (el, binding,vnode) {
		 let contextData=vnode.context._data;
		 let param=binding.value;
		 let ajaxParam=param.ajaxParam;
		 let url=param.url;
		 if(!url){
			 alert("Vue指令使用错误：url 未正确设置");
		 }
		 
		 let chkEnable=param.chkEnable;
		 if(!(chkEnable==false||chkEnable=='false'||chkEnable==true||chkEnable=='true')){
			 alert("Vue指令使用错误：chkEnable 未正确设置 [bool 类型： true or false ]");
			 return;
		 }
		 let chkStyle=param.chkStyle;
		 if(chkEnable&&chkEnable==true&&!chkStyle){
			 alert("Vue指令使用错误：chkStyle 未正确设置 ");
			 return;
		 }
		 if(!(chkStyle=='checkbox'||chkStyle=='radio')){
			 alert("Vue指令使用错误：chkEnable 未正确设置 [checkbox or radio ]");
			 return;
		 }
		 onClickFunName=param.onClick;
		 onRightClickFunName=param.onRightClick;
		 let clickFun=undefined;
		 let onRightClick=undefined;
		 
		 if(onClickFunName){
			  clickFun=vnode.context[onClickFunName];
			 if(!clickFun){
				 alert("Vue指令使用错误： 未定义点击回调函数：["+onClickFunName+"]");
			 } 
		 }
		 if(onRightClickFunName){
			 onRightClick=vnode.context[onRightClickFunName];
			 if(!onRightClick){
				 alert("Vue指令使用错误： 未定义点击回调函数：["+onRightClickFunName+"]");
			 } 
		 }
		 let chkboxType=param.chkboxType;
		
		let settings = {
					async:{
						enable: true,
						url:url,
						autoParam:["id=pid"],
						otherParam:$.extend({},ajaxParam)
					},
					check: {
						enable: chkEnable,
						chkStyle: chkStyle,
						chkboxType:chkboxType
					},
					edit: {enable: false},
					view: {fontCss: {"font-size":"14px"}},
					data: {
						simpleData: {
							enable: true,
							idKey: "id",
							pIdKey: "pId",
							rootPId: "-1"
						}
					},
					callback: {
						onClick:clickFun,
						onRightClick:onRightClick
						
					}
			};
		$.fn.zTree.init($(el), settings, null);
	  },
	  componentUpdated:function(){
		 console.log("componentUpdated");
	  }
	});
Vue.directive('kperms', {
	inserted: function (el, binding) {
	  let valuex=binding.value;
	  if(valuex){
		  let permCode=valuex.perm;
		  if(zfesUtil.isNotEmpty(permCode)){
			 if(!zfesPermStore.havePermission(permCode)){
				 // el.remove();  
			  }
		  }
	  }
  }
});
/**
 *
	说明：1先初始化下拉框的数据列表，2然后修改vue实例中 ==>v-model绑定的值，有数据双向绑定触发-select的值的变化。。。。
	v-kselect:
	url: 异步请求下拉列表地址,返回数据格式： {"confirmMsg":"","data":{"snowyselectdata":[{"text":"xxx","value":"xxxx"}]},"forwardUrl":"","message":"","statusCode":200}
	vmodel: dom 节点绑定的Vue data 对象
	firstEmpty： 下拉第一行是否空显示
	linkToDomId: 如果是联动的 子级，用于指定 自己节点监听的节点变化，
	ajaxParamKey： 如果是联动的 子级，用于指定 自己节点监听的节点 作为ajax的url参数，
}
例如： <select id="appKey" name="appKey" v-model="searchForm.appKey"  v-kselect="{url:'auth/authApp/loadAuthAppComboboVo',vmodel:'searchForm.appKey',firstEmpty:'false'}" class=" form-control"> </select>
 <select
   id="moduleId" 
   name="moduleId"
    v-model="searchForm.moduleId" 
	v-kselect="{url:'auth/authModule/loadModuleComboboVo',vmodel:'searchForm.moduleId',firstEmpty:'true',linkToDomId:'appKey',ajaxParamKey:'appKey'}" 
	class=" form-control"> </select>
 */
 Vue.directive('kselect', {
	 inserted: function (el, binding,vnode) {
		 let contextData=vnode.context._data;
		 let param=binding.value;
		 let vmodel=param.vmodel;
		 let bus=vnode.context._data.bus;
		 if(!bus){
			 alert("Vue指令使用错误：kselect指令依赖context data 中定义的 [ bus:new Vue()]");
		 }
		 if(!vmodel){
			 alert("Vue指令使用错误：vmodel 未正确设置");
		 }
		 let url=param.url;
		 if(!url){
			 alert("Vue指令使用错误：url 未正确设置");
		 }
		 let firstEmpty=(!(param.firstEmpty) ? false:param.firstEmpty);
		 let thisDomId=el.id;
		 if(!thisDomId){
			 alert("Vue指令使用错误：kselect指令所在元素必须设置id属性");
		 }
		 let linkToDomId=param.linkToDomId;
		 let isLinkedTo=false;
		 if(linkToDomId){
			  isLinkedTo=true; 
		}
//----------------------------参数初始化结束--------------------------------------------
		  $(el).select().on('change', function () {
			  bus.$emit(zfesVueDirective.getKselectOnchangeEventName(thisDomId), {selectedValue:this.value});
	      });
		  bus.$on(zfesVueDirective.getKselectOnInitEventName(linkToDomId), function (msg) {
			  bus.$emit(zfesVueDirective.getKselectOnchangeEventName(linkToDomId), msg);
		  });
		  bus.$on(zfesVueDirective.getKselectOnchangeEventName(linkToDomId), function (msg) {
		      let selectedValue=msg.selectedValue;
			  let ajaxParamKey=param.ajaxParamKey;
			  let ajaxParam={};
			  ajaxParam[ajaxParamKey] =selectedValue;
			  ajaxParam['firstEmpty'] =firstEmpty;
			//---------------------------------------------------------------------------------------------
				  zfesAjax.ajaxTodo(url, ajaxParam, function(data) {//text,value //异步加载数据
					  el.innerHTML = "";
					  let selectoptions= data.data.snowyselectdata;
						if(zfesUtil.isNotEmpty(selectoptions)){
							let firstEmptyDefaultValue=undefined;
							 for( let i = 0; i < selectoptions.length; i ++){
								 let op=new Option(selectoptions[i].text,selectoptions[i].value);
								 el.options.add(op);
								 if(i==0){
									 firstEmptyDefaultValue=selectoptions[i].value;
								 }
							}
							 if((firstEmpty==false||firstEmpty=='false')&&firstEmptyDefaultValue){
								 let arr=vmodel.split(".");
								 if(!arr||arr.length<0){
									 alert("vmodel与Vue-data中定义对象不一致");
									 return ;
								 }
								 let bingdingObjKey=arr[0];
								 let bingdingObj=contextData[bingdingObjKey];
								 if(arr.length==1){
									 contextData[bingdingObjKey]=firstEmptyDefaultValue;
								 }else{
									 for(let j=1;j<arr.length;j++){
										  let propName=arr[j];
										  if(!bingdingObj.hasOwnProperty(propName)){
											  alert("vmodel与Vue-data中定义对象不一致");
											  return;
										  }
										  if((j==arr.length-1)){
											  bingdingObj[propName]=firstEmptyDefaultValue;
										  }else{
											  bingdingObj=bingdingObj[propName];
										  }
									 }
								 }
							 }
							// console.log("searchForm-directive |"+ JSON.stringify(contextData['searchForm']));
						}
				
					//回调回来时发送事件
					bus.$emit(zfesVueDirective.getKselectOnInitEventName(thisDomId), {selectedValue:$(el).val()});
				  }); 
				  
				//------------------------------------------------------------------------------------------------
	  });
		  if(isLinkedTo){
			  return;
		  }
//----------------------------联动监听结束--------------------------------------------		  
		  zfesAjax.ajaxTodo(url, {firstEmpty:firstEmpty}, function(data) {//text,value //异步加载数据
			  el.innerHTML = "";
			  let selectoptions= data.data.snowyselectdata;
				if(zfesUtil.isNotEmpty(selectoptions)){
					let firstEmptyDefaultValue=undefined;
					 for( let i = 0; i < selectoptions.length; i ++){
						 let op=new Option(selectoptions[i].text,selectoptions[i].value);
						 el.options.add(op);
						 if(i==0){
							 firstEmptyDefaultValue=selectoptions[i].value;
						 }
					}
					 if((firstEmpty==false||firstEmpty=='false')&&firstEmptyDefaultValue){
						 let arr=vmodel.split(".");
						 if(!arr||arr.length<0){
							 alert("vmodel与Vue-data中定义对象不一致");
							 return ;
						 }
						 let bingdingObjKey=arr[0];
						 let bingdingObj=contextData[bingdingObjKey];
						 if(arr.length==1){
							 contextData[bingdingObjKey]=firstEmptyDefaultValue;
						 }else{
							 for(let j=1;j<arr.length;j++){
								  let propName=arr[j];
								  if(!bingdingObj.hasOwnProperty(propName)){
									  alert("vmodel与Vue-data中定义对象不一致");
									  return;
								  }
								  if((j==arr.length-1)){
									  bingdingObj[propName]=firstEmptyDefaultValue;
								  }else{
									  bingdingObj=bingdingObj[propName];
								  }
							 }
						 }
					 }
				}
			//回调回来时发送事件
			bus.$emit(zfesVueDirective.getKselectOnInitEventName(thisDomId),  {selectedValue:$(el).val()});
		  }); 
	  },
	  componentUpdated:function(){
		 //console.log("componentUpdated");
	  }
	});
 
