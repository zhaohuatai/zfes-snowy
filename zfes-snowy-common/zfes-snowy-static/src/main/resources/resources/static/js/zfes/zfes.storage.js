const zfesLocalStore={
		init:function(storageName){
			var obj={};
			store.set(storageName,obj);
		},
		set:function(storageName,obj){
			store.set(storageName,obj);
		},
		get:function(storageName){
			return store.get(storageName);
		},
		remove:function(storageName){
			store.remove(storageName);
		},
		setItem:function(storageName,key,val){
			let obj=store.get(storageName);
			if(!zfesLocalStore.isEmpty(obj)){
				let item={key:val};
				$.extend(obj, item);
				zfesLocalStore.set(storageName,obj);
			}
		},
		removeItem:function(storageName,key){
			let obj=store.get(storageName);
			if(!zfesLocalStore.isEmpty(obj)){
				delete obj.key;
				zfesLocalStore.set(storageName,obj);
			}
		},
		clear:function(){
			store.clear();
		},
		isEmpty:function(valuex){
			if(!valuex||valuex === "[]"||valuex === "{}"||typeof (valuex) == "undefined"||""==valuex||null==valuex||"null"==valuex||"undefined"==valuex||undefined==valuex||""==$.trim(valuex)){
				return true;
			}
			return false;
		}
}

var zfesSessionStore={
		init:function(storageName){
			var obj={};
			window.sessionStorage.setItem(storageName,JSON.stringify(obj));
		},
		set:function(storageName,obj){
			window.sessionStorage.setItem(storageName,JSON.stringify(obj));
		},
		get:function(storageName){
			return JSON.parse(window.sessionStorage.getItem(storageName));
		},
		remove:function(storageName){
			return JSON.parse(window.sessionStorage.getItem(storageName));
		},
		setItem:function(storageName,key,val){
			let obj=window.sessionStorage.getItem(storageName);
			if(!zfesSessionStore.isEmpty(obj)){
				let item={key:val};
				$.extend(obj, item);
				zfesSessionStore.set(storageName,obj);
			}
		},
		removeItem:function(storageName,key){
			let obj=zfesSessionStore.get(storageName);
			if(!zfesSessionStore.isEmpty(obj)){
				delete obj.key;
				zfesSessionStore.set(storageName,obj);
			}
		},
		clear:function(){
			window.sessionStorage.clear();
		},
		isEmpty:function(valuex){
			if(!valuex||valuex === "[]"||valuex === "{}"||typeof (valuex) == "undefined"||""==valuex||null==valuex||"null"==valuex||"undefined"==valuex||undefined==valuex||""==$.trim(valuex)){
				return true;
			}
			return false;
		}
}
