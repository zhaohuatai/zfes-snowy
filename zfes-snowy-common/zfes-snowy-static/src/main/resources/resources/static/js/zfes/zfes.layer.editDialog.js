const zfesLayerEditDg={
		//layerIndex:-1,
		layerParamKey:"zfes_layer_param_key",
		callBackParamKey:"zfes_callback_param_key",
		layerIndex:[] ,
		setLayerParam:function(param){
			store.set(zfesLayerEditDg.layerParamKey+"_", param);
		},
		getLayerParam:function(){
			let param=store.get(zfesLayerEditDg.layerParamKey+"_");
			//store.remove(zfesCore.layerParamKey+"_"+index);
			return param;
		},
		setCallBackParam:function(param){
			store.set(zfesLayerEditDg.callBackParamKey+"_", param);
		},
		getCallBackParam:function(){
			let param=store.get(zfesLayerEditDg.callBackParamKey+"_");
			//store.remove(zfesCore.callBackParamKey+"_");
			return param;
		},
		open:function(url,param,title,maxWidthx,endCallback){
			if(param){zfesLayerEditDg.setLayerParam(param);}
			let windowWidth=$(document.body).width();
			let realWidth=windowWidth>600?(maxWidthx>windowWidth?windowWidth*0.9:maxWidthx):(windowWidth*0.9);
			//var realWidth=maxWidthx>windowWidth?windowWidth*0.9:maxWidthx;
			 zfesAjax.ajaxHtml(url, {}, function(data){
				 let contentx="<div  style=\"overflow: hidden;\"><div style=\"width: "+realWidth+"px;height: 0px;\">&nbsp;</div>"+data+"</div>";
				 let otptionx=$.extend({
					    type: 1,
					    title: title,
					    fix: true,
					    scrollbar: false,
					    content: contentx,
					    end :endCallback
					},{});
					var indexx=layer.open(otptionx);
					zfesLayerEditDg.layerIndex.push(indexx);
					//zfesLayerEditDg.layerIndex = layer.open(otptionx);
					//ZVue.init();
			});
		},
		
		lookback:function(url,param,title,maxWidthx,endCallback){
			if(param){zfesLayerEditDg.setCallBackParam(param);}
			let windowWidth=$(document.body).width();
			let realWidth=windowWidth>600?(maxWidthx>windowWidth?windowWidth*0.9:maxWidthx):(windowWidth*0.9);
			//var realWidth=maxWidthx>windowWidth?windowWidth*0.9:maxWidthx;
			 zfesAjax.ajaxHtml(url, {}, function(data){
				 let contentx="<div  style=\"overflow: hidden;\"><div style=\"width: "+realWidth+"px;height: 0px;\">&nbsp;</div>"+data+"</div>";
				 let otptionx=$.extend({
					    type: 1,
					    title: title,
					    fix: true,
					    scrollbar: false,
					    content: contentx,
					    end :endCallback
					},{});
				 let indexx=layer.open(otptionx);
					zfesLayerEditDg.layerIndex.push(indexx);
			});
		},
		
		openOption:function(url,param,title,maxWidthx,layeroption,endCallback){
			if(param){zfesLayerEditDg.setLayerParam(param);}
			let windowWidth=$(document.body).width();
			let realWidth=windowWidth>600?(maxWidthx>windowWidth?windowWidth*0.9:maxWidthx):(windowWidth*0.9);
			 zfesAjax.ajaxHtml(url, {}, function(data){
				 let contentx="<div style=\"overflow: hidden;\"><div style=\"width: "+realWidth+"px;height: 0px;\">&nbsp;</div>"+data+"</div>";
				 let otptionx=$.extend({
					    type: 1,
					    title: title,
					    fix: true,
					    scrollbar: false,
					    content: contentx,
					    end :endCallback
					},layeroption);
				 let indexx=layer.open(otptionx);
					zfesLayerEditDg.layerIndex.push(indexx);
					//zfesLayerEditDg.layerIndex = layer.open(otptionx);
			});
		},
		close:function(){
			//$("#zfes_layer_div_container").remove();
			//layer.close(zfesLayerEditDg.layerIndex);
			layer.close(zfesLayerEditDg.layerIndex.pop());
		},
		full:function(index){layer.full(zfesLayerEditDg.layerIndex);},
}	 
