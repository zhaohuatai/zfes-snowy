var ajaxEditor;
var KindEditInit={
		ajaxKind:function(element,uploadUrl){
			$.getScript('static/libs/kindeditor/kindeditor.js', function() {
		    	KindEditor.basePath = 'static/libs/kindeditor/';
		    	ajaxEditor=KindEditor.create('#'+element,{
		    		cssPath : 'static/libs/kindeditor/plugins/code/prettify.css',
					uploadJson : uploadUrl,
					allowFileManager : false,
					filterMode:false,
					zIndex:19891016,//层级比layer高
					afterBlur: function(){this.sync();}
		    	});
		    });
		},
		/**
		 * 初始化编辑器_窗口模式
		 */
		kindEdit_win:function(element,isReadonly,uploadUrl){
			if(Common.isNull(isReadonly)){
				isReadonly=false;
			}
			$.getScript('static/libs/kindeditor/kindeditor.js', function() {
			    	KindEditor.basePath = 'static/libs/kindeditor/';
			    	ajaxEditor=KindEditor.create('#'+element,{
			    		resizeType : 1,
			    		items : ['fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
								'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
								'insertunorderedlist', '|', 'emoticons', 'image', 'link'],
			    		cssPath : 'static/libs/kindeditor/plugins/code/prettify.css',
						uploadJson : uploadUrl,
						allowFileManager : false,
						filterMode:false,
						readonlyMode:isReadonly,//初始化位只读模式
						width:"80%",
						afterBlur: function(){this.sync();}
			    	});
			    });
		},
		/**
		 * 初始化编辑器_最简单形式（只能表情、图片、链接，其他的都不行）
		 */
		kindEdit_mini:function(element,isReadonly,uploadUrl){
			$.getScript('static/libs/kindeditor/kindeditor.js', function() {
			    	KindEditor.basePath = 'static/libs/kindeditor/';
			    	ajaxEditor=KindEditor.create('#'+element,{
			    		resizeType : 1,
			    		items : ['emoticons', 'image', 'link'],
			    		cssPath : 'static/libs/kindeditor/plugins/code/prettify.css',
						uploadJson : uploadUrl,
						allowFileManager : false,
						filterMode:false,
						readonlyMode:false,//初始化位只读模式
						afterBlur: function(){this.sync();}
			    	});
			    });
		}
};


