/**
 * 该js用于设置专门的统计模块，该js已全部抽取，不需关心其他操作方式
 */
var Statistics={
		Common:function(container,option){
			opt = ECharts.ChartConfig(container, option);   	 
		    ECharts.Charts.RenderChart(opt); 
		},
		
		//饼状图统计公用方法——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器
		PieCommon:function(data,name,title,subTitle,container){
		    var option = ECharts.ChartOptionTemplates.Pie(data,name,title,subTitle);   	 
		    Statistics.Common(container,option);
		},

		//内嵌饼状图统计方法——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器；dataIn和dataOut分别为内置统计和外圈统计园
		NestPieCommon:function(dataIn,dataOut,name,title,subTitle,container){
		    var option = ECharts.ChartOptionTemplates.NestPie(dataIn,dataOut,name,title,subTitle);   	 
		    Statistics.Common(container,option);
		},

		//折线图统计公用方法——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器；is_stack:是否需要堆积；is_min：是否显示最小值；
		//is_max：是否显示最大值；is_avg：是否计算平均值
		LineCommon:function(data,name,is_stack,is_min,is_max,is_avg,title,subTitle,container){
		    var option = ECharts.ChartOptionTemplates.Lines(data,name,is_stack,is_min,is_max,is_avg,title,subTitle);   	 
		    Statistics.Common(container,option);
		},
		//柱状图统计公用方法——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器；is_stack:是否需要堆积；is_min：是否显示最小值；
		//is_max：是否显示最大值；is_avg：是否计算平均值
		BarCommon:function(data,name,is_stack,is_min,is_max,is_avg,title,subTitle,container){
		    var option = ECharts.ChartOptionTemplates.Bars(data,name,is_stack,is_min,is_max,is_avg,title,subTitle);   	 
		    Statistics.Common(container,option);
		},
		//柱线混合图——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器；is_stack:是否需要堆积；is_min：是否显示最小值；
		//is_max：是否显示最大值；is_avg：是否计算平均值，data0/data1和name0/name1分别为对应柱状图部分和线状图部分的数据和名称（注意：必须前后两者data和name对应）
		BarAndLine: function(data0,data1, name0,name1, is_stack,is_min,is_max,is_avg,title,subTitle,container){
			var option = ECharts.ChartOptionTemplates.BarandLines(data0,data1, name0,name1, is_stack,is_min,is_max,is_avg,title,subTitle);   	 
			Statistics.Common(container,option);
		},
		
		//散点图——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器；is_stack:是否需要堆积；is_min：是否显示最小值；
		//is_max：是否显示最大值；is_avg：是否计算平均值
		Scatter: function(data,name,is_stack,is_min,is_max,is_avg,title,subTitle,container){
			var option = ECharts.ChartOptionTemplates.Scatter(data,name,is_stack,is_min,is_max,is_avg,title,subTitle);   	 
			Statistics.Common(container,option);
		},
		//地图——data格式[{name:xxx,value:xxx},{name:xxx,value:xxx}..];name:数据名称；title:统计标题；
		//subTitle：统计副标题；container:用于放置统计图div容器
		Map: function(data,name,title,subTitle,container){
			var option = ECharts.ChartOptionTemplates.Map(data,name,title,subTitle);   	 
			Statistics.Common(container,option);
		}
};
