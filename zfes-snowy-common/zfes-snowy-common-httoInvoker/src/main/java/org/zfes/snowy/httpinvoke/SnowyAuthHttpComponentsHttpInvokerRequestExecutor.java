package org.zfes.snowy.httpinvoke;

import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.springframework.remoting.httpinvoker.HttpComponentsHttpInvokerRequestExecutor;
import org.springframework.remoting.httpinvoker.HttpInvokerClientConfiguration;

public class SnowyAuthHttpComponentsHttpInvokerRequestExecutor extends HttpComponentsHttpInvokerRequestExecutor {
	
	
	public SnowyAuthHttpComponentsHttpInvokerRequestExecutor(HttpClient httpClient) {
		super(httpClient);
	}
	
	
	@Override
	protected HttpPost createHttpPost(HttpInvokerClientConfiguration config) throws IOException {
		HttpPost httpPost =super.createHttpPost(config);
		
		String httpInvokeToken=HttpInvokeTokenUtil.readHttpInvokeToken();
		httpPost.setHeader("httpInvokeToken",httpInvokeToken);
		return httpPost;
	}
}
