/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.httpinvoke;

import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.UUIDUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.core.util.encypt.EncyptFileUtil;

public class HttpInvokeTokenUtil {
	
	private static String httpInvokeToken=null;
	public static final String signature_path="signaturekey"+"/";
	public static final String httpinvoke_token_file_name="httpinvoke_token";
	
	private static final String maven_middle_path="src/main/resources/";
	private static final String target="target";
	private static final String clazzPath=Thread.currentThread().getContextClassLoader().getResource("").getPath().replaceAll("%20", " ");
	private static final String path=ZStrUtil.substringBeforeLast(clazzPath,target )+maven_middle_path+signature_path;
	
	public static String createHttpInvokeToken(){
		String httpInvokeToken=UUIDUtil.base58Uuid();
		
		 EncyptFileUtil.saveKey(httpInvokeToken, path,httpinvoke_token_file_name);
		 
		 return httpInvokeToken;
	}
	public static String readHttpInvokeToken(){
		if(httpInvokeToken==null){
			synchronized (HttpInvokeTokenUtil.class) {
				 if (httpInvokeToken == null) {
					 httpInvokeToken=EncyptFileUtil.readKey(path,httpinvoke_token_file_name);
				       if(!ZStrUtil.hasText(httpInvokeToken)){
				    	   throw new AppRuntimeException("系统未生成 httpInvokeToken");
				       }
			        }  
			}
		}
		return httpInvokeToken;
	}
	

	public static void main(String[] args){    
		System.out.println("create:||"+HttpInvokeTokenUtil.createHttpInvokeToken());;
		System.out.println("read:||"+HttpInvokeTokenUtil.readHttpInvokeToken());;

	}
	
}
