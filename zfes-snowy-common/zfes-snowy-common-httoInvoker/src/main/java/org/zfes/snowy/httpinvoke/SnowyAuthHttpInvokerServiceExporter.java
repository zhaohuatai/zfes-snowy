package org.zfes.snowy.httpinvoke;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.web.util.NestedServletException;

public class SnowyAuthHttpInvokerServiceExporter extends HttpInvokerServiceExporter{
	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		String auth=request.getHeader("httpInvokeToken");
		String httpInvokeToken=HttpInvokeTokenUtil.readHttpInvokeToken();
		if(!httpInvokeToken.equals(auth)){
			throw new NestedServletException("httpInvokeToken error ");
		}
		super.handleRequest(request, response);
	}
}
