/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.common.variable.repository;

import java.util.Map;

/**
//<key,Map<key,value>>
 * 
 * 
 * 
 * 
//varibaleName : indexKey--->final value
 */

public interface IVariableRepository {

	 void putVariable(String varibaleName,Map<String,Object> variable);
	
	 Map<String,Object> getVariable(String varibaleName);
	
	 void removeVariable(String varibaleName);
	
//-----------------------------------------------------	
	
	 Object getVariableItem(String varibaleName,String indexKey);
	
	 void refreshVariableItem(String varibaleName,String indexKey,Object value);

	 void newVariableItem(String varibaleName,String indexKey,Object value);
	
	 void removeVariableItem(String varibaleName,String indexKey);
	
	
}
