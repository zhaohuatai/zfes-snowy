/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.common.variable.manager;

import java.util.Map;

import javax.annotation.Resource;

import org.zfes.snowy.common.variable.repository.IVariableRepository;

public class SimpleVariableManger implements IVariableManger{

	@Resource
	private IVariableRepository variableRepository;
	
	
	public Object getVariableItem(String varibaleName,String indexKey){
		return variableRepository.getVariableItem(varibaleName, indexKey);
	}
	
	public void refreshVariableItem(String varibaleName,String indexKey,Object value){
		variableRepository.refreshVariableItem(varibaleName, indexKey, value);
	}

	public void newVariableItem(String varibaleName,String indexKey,Object value){
		variableRepository.newVariableItem(varibaleName, indexKey, value);
	}
	
	public void removeVariableItem(String varibaleName,String indexKey){
		variableRepository.removeVariableItem(varibaleName, indexKey);
	}

	@Override
	public void putVariable(String varibaleName, Map<String, Object> variable) {
		variableRepository.putVariable(varibaleName, variable);
		
	}

	@Override
	public Map<String, Object> getVariable(String varibaleName) {
		return variableRepository.getVariable(varibaleName);
	}

	@Override
	public void removeVariable(String varibaleName) {
		variableRepository.removeVariable(varibaleName);
		
	}
}
