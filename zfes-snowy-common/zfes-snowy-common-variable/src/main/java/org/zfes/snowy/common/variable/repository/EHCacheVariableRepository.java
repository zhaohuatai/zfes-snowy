/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.common.variable.repository;

import java.util.Map;
import org.springframework.cache.Cache;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZStrUtil;
@Deprecated
public class EHCacheVariableRepository extends AbstractVariableRepository {
	
	private org.springframework.cache.ehcache.EhCacheCacheManager cacheCacheManager;

	private final String variableCacheName="variableCache";
	private Cache getCache(){
		Cache cache=cacheCacheManager.getCache(variableCacheName);
		if(cache==null){
			throw new AppRuntimeException("Cache with name variableCache is not  initialed");
		}
		return cache;
	}
	@Override
	public void doPutVariable(String varibaleName, Map<String, Object> variable) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		if(variable==null||variable.isEmpty()){
			throw new AppRuntimeException("variable can not be null or empty ");
		}
		getCache().put(varibaleName, variable);
	}


	@Override
	protected Map<String, Object> doGetVariable(String varibaleName) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		@SuppressWarnings("unchecked")
		Map<String, Object> variable=(Map<String, Object>) getCache().get(varibaleName);
		return variable;
	}
	@Override
	protected void doRemoveVariable(String varibaleName) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		getCache().evict(varibaleName);
		
	}

	
	
}
