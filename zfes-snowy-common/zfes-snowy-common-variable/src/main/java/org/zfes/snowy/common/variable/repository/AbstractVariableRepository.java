/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.common.variable.repository;

import java.util.HashMap;
import java.util.Map;

import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZStrUtil;
//<key,Map<key,value>>
//varibaleName : indexKey--->final value
public abstract class AbstractVariableRepository implements IVariableRepository {

	protected abstract Map<String, Object>  doGetVariable(String varibaleName);
	
	protected abstract void  doPutVariable(String varibaleName, Map<String, Object> variable);
	
	protected abstract void doRemoveVariable(String varibaleName);

	public void putVariable(String varibaleName,Map<String,Object> variable){
		doPutVariable( varibaleName,  variable);
	}
	
	public Map<String,Object> getVariable(String varibaleName){
		return doGetVariable( varibaleName);
	}
	
	public void removeVariable(String varibaleName){
		doRemoveVariable(varibaleName);
	}

	@Override
	public Object getVariableItem(String varibaleName, String indexKey) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		if(!ZStrUtil.hasText(indexKey)){
			throw new AppRuntimeException("indexKey can not be null or empty ");
		}
		Map<String, Object> variable=doGetVariable( varibaleName);
        
		if(variable==null||variable.isEmpty()){
			return null;
		}
		return variable.get(indexKey);
	}

	@Override
	public void refreshVariableItem(String varibaleName, String indexKey, Object value) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		if(!ZStrUtil.hasText(indexKey)){
			throw new AppRuntimeException("indexKey can not be null or empty ");
		}
		if(value==null){
			throw new AppRuntimeException("value can not be null or empty ");
		}
		Map<String, Object> variable=doGetVariable( varibaleName);
		if(variable==null){
			variable=new HashMap<String, Object>(1);
			variable.put(indexKey, value);
			return;
		}
		variable.put(indexKey, value);
		this.doPutVariable(varibaleName, variable);
	}

	@Override
	public void newVariableItem(String varibaleName, String indexKey, Object value) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		if(!ZStrUtil.hasText(indexKey)){
			throw new AppRuntimeException("indexKey can not be null or empty ");
		}
		if(value==null){
			throw new AppRuntimeException("value can not be null or empty ");
		}
		Map<String, Object> variable=doGetVariable( varibaleName);
		if(variable==null){
			variable=new HashMap<>(1);
			variable.put(indexKey, value);
			return;
		}
		variable.put(indexKey, value);
		this.doPutVariable(varibaleName, variable);
	}

	@Override
	public void removeVariableItem(String varibaleName, String indexKey) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		if(!ZStrUtil.hasText(indexKey)){
			throw new AppRuntimeException("indexKey can not be null or empty ");
		}
		Map<String, Object> variable=doGetVariable( varibaleName);
		if(variable==null){
			return;
		}
		variable.remove(indexKey);
        this.doPutVariable( varibaleName,variable);
	}



}
