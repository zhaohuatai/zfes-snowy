/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.common.variable.repository;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZStrUtil;

public class MemoryHashVariableRepository extends AbstractVariableRepository {

	private Map<String,Map<String,Object>> variableMemoryHashStorage;
	
	private Map<String,Map<String,Object>> getVariableMemoryHashStorage(){
		if(variableMemoryHashStorage!=null){
			return variableMemoryHashStorage;
		}
		if(variableMemoryHashStorage==null){
			 variableMemoryHashStorage=new ConcurrentHashMap<String,Map<String,Object>>(1);
		}
		return variableMemoryHashStorage;
	}
	
	@Override
	protected Map<String,Object> doGetVariable(String varibaleName) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		Map<String,Object> variable=getVariableMemoryHashStorage().get(varibaleName);
		return variable;
	}
	@Override
	protected void doPutVariable(String varibaleName, Map<String,Object> variable) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		if(variable==null||variable.isEmpty()){
			throw new AppRuntimeException("variable can not be null or empty ");
		}
		 getVariableMemoryHashStorage().put(varibaleName, variable);
	}

	@Override
	protected void doRemoveVariable(String varibaleName) {
		if(!ZStrUtil.hasText(varibaleName)){
			throw new AppRuntimeException("varibaleName can not be null or empty ");
		}
		 getVariableMemoryHashStorage().remove(varibaleName);
	}

	
	
}
