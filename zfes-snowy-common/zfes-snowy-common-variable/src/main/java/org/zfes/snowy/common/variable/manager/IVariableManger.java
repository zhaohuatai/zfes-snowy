/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.common.variable.manager;

import java.util.Map;

public interface IVariableManger {

	public void putVariable(String varibaleName,Map<String,Object> variable);
	
	public Map<String,Object> getVariable(String varibaleName);
	
	public void removeVariable(String varibaleName);
	
	
	public Object getVariableItem(String varibaleName,String indexKey);
	
	public void refreshVariableItem(String varibaleName,String indexKey,Object value);

	public void newVariableItem(String varibaleName,String indexKey,Object value);
	
	public void removeVariableItem(String varibaleName,String indexKey);
}
