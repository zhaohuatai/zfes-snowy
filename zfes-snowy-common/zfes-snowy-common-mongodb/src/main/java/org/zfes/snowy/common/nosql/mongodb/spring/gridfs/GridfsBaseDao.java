/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.common.nosql.mongodb.spring.gridfs;

import java.io.InputStream;
import java.util.List;

import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

public class GridfsBaseDao {
	
	private GridFsOperations operations;

	/**
	 * Stores the given content into a file with the given name.
	 * 
	 * @param content must not be {@literal null}.
	 * @param filename must not be {@literal null} or empty.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, String filename){
		return operations.store(content, filename);
	}

	/**
	 * Stores the given content into a file with the given name.
	 * 
	 * @param content must not be {@literal null}.
	 * @param metadata can be {@literal null}.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, Object metadata){
		return operations.store(content, metadata);
	}

	/**
	 * Stores the given content into a file with the given name.
	 * 
	 * @param content must not be {@literal null}.
	 * @param metadata can be {@literal null}.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, DBObject metadata){
		return operations.store(content, metadata);
	}

	/**
	 * Stores the given content into a file with the given name and content type.
	 * 
	 * @param content must not be {@literal null}.
	 * @param filename must not be {@literal null} or empty.
	 * @param contentType can be {@literal null}.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, String filename, String contentType){
		return operations.store(content, filename,contentType);
	}

	/**
	 * Stores the given content into a file with the given name using the given metadata. The metadata object will be
	 * marshalled before writing.
	 * 
	 * @param content must not be {@literal null}.
	 * @param filename must not be {@literal null} or empty.
	 * @param metadata can be {@literal null}.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, String filename, Object metadata){
		return operations.store(content, filename,metadata);
	}

	/**
	 * Stores the given content into a file with the given name and content type using the given metadata. The metadata
	 * object will be marshalled before writing.
	 * 
	 * @param content must not be {@literal null}.
	 * @param filename must not be {@literal null} or empty.
	 * @param contentType can be {@literal null}.
	 * @param metadata can be {@literal null}
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, String filename, String contentType, Object metadata){
		return operations.store( content,  filename,  contentType,  metadata);
	}

	/**
	 * Stores the given content into a file with the given name using the given metadata.
	 * 
	 * @param content must not be {@literal null}.
	 * @param filename must not be {@literal null} or empty.
	 * @param metadata can be {@literal null}.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, String filename, DBObject metadata){
		return operations.store( content,  filename,  metadata);
	}

	/**
	 * Stores the given content into a file with the given name and content type using the given metadata.
	 * 
	 * @param content must not be {@literal null}.
	 * @param filename must not be {@literal null} or empty.
	 * @param contentType can be {@literal null}.
	 * @param metadata can be {@literal null}.
	 * @return the {@link GridFSFile} just created
	 */
	GridFSFile store(InputStream content, String filename, String contentType, DBObject metadata){
		return operations.store( content,  filename,  contentType,  metadata);
	}

	/**
	 * Returns all files matching the given query. Note, that currently {@link Sort} criterias defined at the
	 * {@link Query} will not be regarded as MongoDB does not support ordering for GridFS file access.
	 * 
	 * @see https://jira.mongodb.org/browse/JAVA-431
	 * @param query
	 * @return
	 */
	List<GridFSDBFile> find(Query query){
		return operations.find(query);
	}
	List<GridFSDBFile> findByFileName(String fileName){
		return operations.find(new Query(Criteria.where("filename").is(fileName)));
	}
	
	GridFSDBFile findOneByFileName(String fileName){
		return operations.findOne(new Query(Criteria.where("filename").is(fileName)));
	}
	
	GridFSDBFile findByMd5(String MD5Str){
		return operations.findOne(new Query(Criteria.where("md5").is(MD5Str)));
	}
	
	/**
	 * Returns a single file matching the given query or {@literal null} in case no file matches.
	 * 
	 * @param query
	 * @return
	 */
	GridFSDBFile findOne(Query query){
		return operations.findOne(query);
	}
	
	GridFSDBFile findById(String id){
		return operations.findOne(new Query(Criteria.where("_id").is(id)));
	}
	/**
	 * Deletes all files matching the given {@link Query}.
	 * 
	 * @param query
	 */
	void delete(Query query){
		 operations.delete(query);
	}
	
	void deleteById(String id){
		 operations.delete(new Query(Criteria.where("_id").is(id)));
	}

	/**
	 * Returns all {@link GridFsResource} with the given file name.
	 * 
	 * @param filename
	 * @return the resource if it exists or {@literal null}.
	 * @see ResourcePatternResolver#getResource(String)
	 */
	GridFsResource getResource(String filename){
		return operations.getResource(filename);
	}

	/**
	 * Returns all {@link GridFsResource}s matching the given file name pattern.
	 * 
	 * @param filenamePattern
	 * @return
	 * @see ResourcePatternResolver#getResources(String)
	 */
	GridFsResource[] getResources(String filenamePattern){
		return operations.getResources(filenamePattern);
	}
	
}
