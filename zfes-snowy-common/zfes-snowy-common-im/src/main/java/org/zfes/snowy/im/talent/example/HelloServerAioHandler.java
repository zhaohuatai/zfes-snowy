/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.im.talent.example;

import com.talent.aio.common.Aio;
import com.talent.aio.common.ChannelContext;
import com.talent.aio.server.intf.ServerAioHandler;

public class HelloServerAioHandler extends HelloAbsAioHandler   implements ServerAioHandler<Object, HelloPacket, Object> {
	    @Override
	    public Object handler(HelloPacket packet, ChannelContext<Object, HelloPacket, Object> channelContext) throws Exception{
	        byte[] body = packet.getBody();
	        if (body != null){
	            String str = new String(body, HelloPacket.CHARSET);
	            System.out.println("收到消息：" + str);
	 
	            HelloPacket resppacket = new HelloPacket();
	            resppacket.setBody(("收到了你的消息，你的消息是:" + str).getBytes(HelloPacket.CHARSET));
	            Aio.send(channelContext, resppacket);
	        }
	        return null;
	    }
}
