/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.im.talent.example;

import com.talent.aio.client.intf.ClientAioHandler;
import com.talent.aio.common.ChannelContext;

public class HelloClientAioHandler extends HelloAbsAioHandler implements ClientAioHandler<Object, HelloPacket, Object> {
	  @Override
	    public Object handler(HelloPacket packet, ChannelContext<Object, HelloPacket, Object> channelContext) throws Exception{
	        byte[] body = packet.getBody();
	        if (body != null){
	            String str = new String(body, HelloPacket.CHARSET);
	            System.out.println("收到消息：" + str);
	        }
	 
	        return null;
	    }
	 
	    private static HelloPacket heartbeatPacket = new HelloPacket();
	 
	    /** 
	     * 此方法如果返回null，框架层面则不会发心跳；如果返回非null，框架层面会定时发本方法返回的消息包
	     */
	    @Override
	    public HelloPacket heartbeatPacket(){
	        return heartbeatPacket;
	    }
}
