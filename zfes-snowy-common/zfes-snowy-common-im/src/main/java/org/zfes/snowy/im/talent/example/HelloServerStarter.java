/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.im.talent.example;

import java.io.IOException;

import com.talent.aio.server.AioServer;
import com.talent.aio.server.ServerGroupContext;
import com.talent.aio.server.intf.ServerAioHandler;
import com.talent.aio.server.intf.ServerAioListener;

public class HelloServerStarter {
	static ServerGroupContext<Object, HelloPacket, Object> serverGroupContext = null;
    static AioServer<Object, HelloPacket, Object> aioServer = null; //可以为空
    static ServerAioHandler<Object, HelloPacket, Object> aioHandler = null;
    static ServerAioListener<Object, HelloPacket, Object> aioListener = null;
    static String ip = null;
    static int port =Const.PORT;
 
    public static void main(String[] args) throws IOException {
        aioHandler = new HelloServerAioHandler();
        aioListener = null; //可以为空
        serverGroupContext = new ServerGroupContext<>(ip, port, aioHandler, aioListener);
        aioServer = new AioServer<>(serverGroupContext);
        aioServer.start();
    }
}
