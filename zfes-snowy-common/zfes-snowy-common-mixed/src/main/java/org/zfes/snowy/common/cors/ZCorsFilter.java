/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.common.cors;



import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsProcessor;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.DefaultCorsProcessor;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;
import org.zfes.snowy.core.util.AppCtxUtil;

public class ZCorsFilter extends OncePerRequestFilter{
	
	
	private final UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource();
	private  CorsProcessor processor = new DefaultCorsProcessor();
	
	//需要注入map--- <url,CorsConfiguration>
	private java.util.LinkedHashMap<String,CorsConfiguration> corsConfigurations;
	
	public ZCorsFilter() {
		if(corsConfigurations==null){
			corsConfigurations=AppCtxUtil.getBean("corsConfigurations");
		}
		configSource.setCorsConfigurations(corsConfigurations);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if (CorsUtils.isCorsRequest(request)) {
			CorsConfiguration corsConfiguration = this.configSource.getCorsConfiguration(request);
			if (corsConfiguration != null) {
				boolean isValid = this.processor.processRequest(corsConfiguration, request, response);
				if (!isValid || CorsUtils.isPreFlightRequest(request)) {
					return;
				}
			}
		}
		filterChain.doFilter(request, response);
	}

	public java.util.LinkedHashMap<String, CorsConfiguration> getCorsConfigurations() {
		return corsConfigurations;
	}

	public void setCorsConfigurations(java.util.LinkedHashMap<String, CorsConfiguration> corsConfigurations) {
		this.corsConfigurations = corsConfigurations;
	}
	
}
