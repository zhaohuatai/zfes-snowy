/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.Filter;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.handler.UnauthorizedHandler;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.util.ReflectionUtils;
import org.zfes.snowy.auth.shiro.cache.manger.ShiroRedisCacheManager;
import org.zfes.snowy.auth.shiro.credential.SnowyCredentialsMatcher;
import org.zfes.snowy.auth.shiro.filter.authc.SnowyCapchaProducerFilter;
import org.zfes.snowy.auth.shiro.filter.authc.SnowyLoginPassThruAuthenticationFilter;
import org.zfes.snowy.auth.shiro.filter.authc.SnowyLogoutFilter;
import org.zfes.snowy.auth.shiro.filter.authc.SnowyTokenAuthenticationFilter;
import org.zfes.snowy.auth.shiro.filter.csrf.CSRFTokenFilter;
import org.zfes.snowy.auth.shiro.filter.sso.SSOSyncCenterfilter;
import org.zfes.snowy.auth.shiro.filter.sso.SSOSyncClientFilter;
import org.zfes.snowy.auth.shiro.handlers.IAuthecFailureHandler;
import org.zfes.snowy.auth.shiro.handlers.IAuthecLogoutHandler;
import org.zfes.snowy.auth.shiro.handlers.IAuthecSuccessHandler;
import org.zfes.snowy.auth.shiro.handlers.IUnAuthecHandler;
import org.zfes.snowy.auth.shiro.handlers.SimpleAuthecCsrfFailureHandler;
import org.zfes.snowy.auth.shiro.handlers.SimpleAuthecFailureHandler;
import org.zfes.snowy.auth.shiro.handlers.SimpleAuthecLogoutHandler;
import org.zfes.snowy.auth.shiro.handlers.SimpleAuthecSuccessHandler;
import org.zfes.snowy.auth.shiro.handlers.SimpleUnauthecHandler;
import org.zfes.snowy.auth.shiro.handlers.SimpleUnauthorizedHandler;
import org.zfes.snowy.auth.shiro.jwt.filter.SnowyJwtAuthTokenFilter;
import org.zfes.snowy.auth.shiro.realm.AppClientRealm;
import org.zfes.snowy.auth.shiro.realm.SnowyRemoteRealm;
import org.zfes.snowy.auth.shiro.remote.IRemoteAuthService;
import org.zfes.snowy.auth.shiro.session.dao.SnowyShiroSessionDAO;
import org.zfes.snowy.auth.shiro.session.repository.impl.RedisShiroSessionRepository;
//import org.zfes.snowy.auth.shiro.session.validate.SnowyExecutorServiceSessionValidationScheduler;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.AppCtxUtil;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.ss.config.properties.ShiroConfigProperties;

import com.google.code.kaptcha.Producer;
@Configuration
@ConditionalOnProperty(name = "shiro.isClientApp", havingValue = "true", matchIfMissing = true)
public class ClientSnowyShiroConfiguration {
	//http://blog.csdn.net/u012345283/article/details/44199791
   @Autowired
   private ShiroConfigProperties shiroConfigProperties; 
   @Autowired
   private Producer producer;

	@Autowired
	private HttpInvokeConfigration httpInvokeConfigration;
 //-------------------------------------------------------------------------------------------------------------------
   /**
    * shiro 自定义过滤器不能声明为全局的，roxiedFilterChain
    * 全局会导致else 之前的代码执行，执行容器级别的filter，  shiro里已经执行过一次了，，容器级别的在执行一次就错了
    * @return
    */
	@Bean(name = "shiroFilter")
	public ShiroFilterFactoryBean shiroFilterFactoryBean(){
		ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
		shiroFilterFactoryBean.setSecurityManager(securityManager());
		
		Map<String, Filter> filters = new LinkedHashMap<String, Filter>();
		filters.put("capcha", capchaProducerFilter());
		filters.put("zlogout", logoutFilter());
		filters.put("zauthc", tokenAuthenticationFilter() );
		filters.put("csrf", csrfTokenFilter());
		filters.put("loginThrust", loginPassThruAuthenticationFilter());
		filters.put("ssoSyncCenter", ssoSyncCenterfilter());
		filters.put("ssoSyncClient", ssoSyncClientFilter());
		Map<String,String> cosumfilters=shiroConfigProperties.getFiltersMap();
		try {
			if(cosumfilters!=null&&!cosumfilters.isEmpty()){
				for(Map.Entry<String, String> entry : cosumfilters.entrySet()){
					String value=entry.getValue();
					String clazz=ZStrUtil.substringBeforeLast(value, ".");
					String methodName=ZStrUtil.substringAfterLast(value, ".");
					Method initmethod;
					initmethod = ReflectionUtils.findMethod(Class.forName(clazz), methodName);
					initmethod.setAccessible(true);
					filters.put(entry.getKey(), (Filter) ReflectionUtils.invokeMethod(initmethod, AppCtxUtil.getBean(Class.forName(clazz))));
				}
			
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		shiroFilterFactoryBean.setFilters(filters);
		Map<String,String> filterChainDefinitionMap= new LinkedHashMap<>();
		filterChainDefinitionMap.put(shiroConfigProperties.getSsoCenterSyncPath(), "ssoSyncCenter");
		filterChainDefinitionMap.put(shiroConfigProperties.getSsoClientSyncPath(), "ssoSyncClient");
		filterChainDefinitionMap.putAll(shiroConfigProperties.getFilterChainDefinitionMap());
		shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
		
		return shiroFilterFactoryBean;
	}

//------------------------------------remoteAuthService-------------------------------------------------------------------------------
		   @ConditionalOnProperty(name = "shiro.isClientApp", havingValue = "true", matchIfMissing = true)
		   @Bean(name="remoteAuthService")
		   public HttpInvokerProxyFactoryBean remoteAuthService(){
				  HttpInvokerProxyFactoryBean bean = new HttpInvokerProxyFactoryBean();    
			       bean.setServiceUrl(shiroConfigProperties.getSsoCenterDomain()+"/remoteAuthService");    
			       bean.setServiceInterface(IRemoteAuthService.class);    
			       bean.setHttpInvokerRequestExecutor(httpInvokeConfigration.customHttpInvoker());
			       return bean; 
		   } 
			
		    @Bean(name = "authorizingRealm")
		    @DependsOn(value={"lifecycleBeanPostProcessor","appCtxUtil"}) 
		    public SnowyRemoteRealm authorizingRealm() {  
		    	SnowyRemoteRealm userRealm=(AppClientRealm)new AppClientRealm();
		    		 userRealm = (AppClientRealm)new AppClientRealm();
		    		 //相互依赖
		    		 IRemoteAuthService  remoteAuthService=AppCtxUtil.getBean("remoteAuthService");
		    		 if(remoteAuthService==null){
		    			 remoteAuthService=(IRemoteAuthService) remoteAuthService();
		    		 }
		    		// RemoteAuthServiceImpl remoteAuthService=AppCtxUtil.getBean("remoteAuthService");
		    		 //remoteAuthService.setSnowyRealm(userRealm);
		    		 ((AppClientRealm)userRealm).setRemoteAuthService(remoteAuthService);
		    		 
		    	
		    	userRealm.setAuthenticationCachingEnabled(shiroConfigProperties.getAuthenticationCachingEnabled());
//		    	userRealm.setAuthenticationCacheName(shiroConfigProperties.getAuthenticationCacheName());
		    	userRealm.setAuthenticationCacheName(shiroConfigProperties.getAuthenticationCacheName()+"_"+AppCtxUtil.getAppKey().get());
		    	
		    	userRealm.setAuthorizationCachingEnabled(shiroConfigProperties.getAuthorizationCachingEnabled());
		    	//userRealm.setAuthorizationCacheName(shiroConfigProperties.getAuthorizationCacheName());
		       
		    	userRealm.setAuthorizationCacheName(shiroConfigProperties.getAuthorizationCacheName()+"_"+AppCtxUtil.getAppKey().get());
		    	
		        userRealm.setCredentialsMatcher(hashedCredentialsMatcher());
		        
		        userRealm.setCacheManager(shiroCacheManager()); 
		        userRealm.setAppKey(AppCtxUtil.getAppKey().get());
		        return userRealm;
		    }
		    
//----------------------------------shiro---------------------------------------------------------------------------------	     
    @Bean(name="securityManager")  
    public DefaultWebSecurityManager securityManager() {  
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();  
        manager.setRealm(authorizingRealm());  
        manager.setCacheManager(shiroCacheManager());  
        manager.setSessionManager(sessionManager());  
        manager.setRememberMeManager(rememberMeManager());
        return manager;  
    } 
    
    @Bean(name="sessionManager")  
    public DefaultWebSessionManager sessionManager() {  
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();  
        sessionManager.setCacheManager(shiroCacheManager()); 
        
        sessionManager.setGlobalSessionTimeout(shiroConfigProperties.getGlobalSessionTimeout());  
        sessionManager.setDeleteInvalidSessions(shiroConfigProperties.getDeleteInvalidSessions());  
        sessionManager.setSessionValidationSchedulerEnabled(shiroConfigProperties.getSessionValidationSchedulerEnabled()); 
        
        //相互依赖
//         ExecutorServiceSessionValidationScheduler sessionValidationScheduler=sessionValidationScheduler();
//        SnowyExecutorServiceSessionValidationScheduler sessionValidationScheduler=sessionValidationScheduler();
//		 sessionValidationScheduler.setSessionManager(sessionManager);
//		 sessionManager.setSessionValidationScheduler(sessionValidationScheduler);
		 
         sessionManager.setSessionIdCookie(sessionIdCookie());
         sessionManager.setSessionIdCookieEnabled(shiroConfigProperties.getSessionIdCookieEnabled());
        
        sessionManager.setSessionDAO(sessionDAO());
        return sessionManager;  
    }  
    
//	 @Bean(name = "sessionValidationScheduler")
//	 public SnowyExecutorServiceSessionValidationScheduler sessionValidationScheduler() {
//		 SnowyExecutorServiceSessionValidationScheduler sessionValidationScheduler= new SnowyExecutorServiceSessionValidationScheduler();
//		 sessionValidationScheduler.setInterval(shiroConfigProperties.getInterval());
//		 sessionValidationScheduler.setUseRedisValidation(shiroConfigProperties.getUseRedisValidation());
//		 
//		// sessionValidationScheduler.setSessionManager(sessionManager());
//		return  sessionValidationScheduler;
//	 }
    
//	 @Bean(name = "sessionValidationScheduler")
//	 public ExecutorServiceSessionValidationScheduler sessionValidationScheduler() {
//		 ExecutorServiceSessionValidationScheduler sessionValidationScheduler= new ExecutorServiceSessionValidationScheduler();
//		 sessionValidationScheduler.setInterval(shiroConfigProperties.getInterval());
//		// sessionValidationScheduler.setSessionManager(sessionManager());
//		return  sessionValidationScheduler;
//	 }
	 
	@Bean(name = "sessionDAO")
	@DependsOn(value="lifecycleBeanPostProcessor")  
	public SessionDAO sessionDAO() {  
		if(shiroConfigProperties.getSessionDaoType().equals("EnterpriseCacheSessionDAO")){
			EnterpriseCacheSessionDAO sessionDAO=new EnterpriseCacheSessionDAO();
			sessionDAO.setActiveSessionsCache(shiroCacheManager().getCache(shiroConfigProperties.getActiveSessionsCacheName()));
			sessionDAO.setSessionIdGenerator(sessionIdGenerator());
			return sessionDAO;
		}else if(shiroConfigProperties.getSessionDaoType().equals("SnowyShiroSessionDAO")){
			SnowyShiroSessionDAO sessionDAO=new SnowyShiroSessionDAO();
			sessionDAO.setActiveSessionsCacheName(shiroConfigProperties.getActiveSessionsCacheName());
			sessionDAO.setGlobalSessionTimeout(shiroConfigProperties.getGlobalSessionTimeout());
			
			sessionDAO.setShiroSessionRepository(jedisShiroSessionRepository());
			sessionDAO.setSessionIdGenerator(sessionIdGenerator());
			return sessionDAO;
		}else{
			throw new AppRuntimeException("sessionDAO 类型配置错误，或者不支持");
		}
	}
	
	
	@Bean(name = "jedisShiroSessionRepository")
	@Lazy(true)
    public RedisShiroSessionRepository jedisShiroSessionRepository() {  
		RedisShiroSessionRepository redisShiroSessionRepository = new RedisShiroSessionRepository();  
		Boolean isCluster=shiroConfigProperties.getShiroUseRedisCluster();
		if(isCluster==null||!isCluster){
			redisShiroSessionRepository.setJedisManager(AppCtxUtil.getBean(RedisSingleConfigration.class).JedisManager());
		}else{
			redisShiroSessionRepository.setJedisManager(AppCtxUtil.getBean(RedisShardConfigration.class).JedisManager());
		}
//		redisShiroSessionRepository.setJedisManager(zfesSnowyConfigration.JedisManager());
        return redisShiroSessionRepository;
    }
	


	@Bean(name = "hashedCredentialsMatcher")
	public SnowyCredentialsMatcher hashedCredentialsMatcher() {
		SnowyCredentialsMatcher credentialsMatcher = new SnowyCredentialsMatcher();
		credentialsMatcher.setHashAlgorithmName(shiroConfigProperties.getHashAlgorithmName());
		credentialsMatcher.setHashIterations(shiroConfigProperties.getHashIterations());
		credentialsMatcher.setStoredCredentialsHexEncoded(shiroConfigProperties.getStoredCredentialsHexEncoded());
		return credentialsMatcher;
	}
	
    @Bean(name = "shiroCacheManager")
    @DependsOn("lifecycleBeanPostProcessor") 
    public CacheManager shiroCacheManager() {  
    	String shiroCacheType=shiroConfigProperties.getShiroCacheType();
    	if("ehcache".equals(shiroCacheType)){
    		EhCacheManager ehCacheManager = new EhCacheManager();  
    		ehCacheManager.setCacheManagerConfigFile(shiroConfigProperties.getCacheConfig());
    		return ehCacheManager; 
    	}else if("memory".equals(shiroCacheType)){
    		return new MemoryConstrainedCacheManager();
    	}else if("redis".equals(shiroCacheType)){
    		ShiroRedisCacheManager redisCacheManager=new  ShiroRedisCacheManager();
    		Boolean isCluster=shiroConfigProperties.getShiroUseRedisCluster();
    		if(isCluster==null||!isCluster){
    			redisCacheManager.setJedisManager(AppCtxUtil.getBean(RedisSingleConfigration.class).JedisManager());
    		}else{
    			redisCacheManager.setJedisManager(AppCtxUtil.getBean(RedisShardConfigration.class).JedisManager());
    		}
    		//redisCacheManager.setJedisManager(zfesSnowyConfigration.JedisManager());
    		return redisCacheManager;  
    	}
        throw new AppRuntimeException("缓存配置错误");  
    } 
    
    
    @Bean(name = "rememberMeManager")
    public RememberMeManager rememberMeManager() {  
    	CookieRememberMeManager rememberMeManager = new org.apache.shiro.web.mgt.CookieRememberMeManager(); 
    	byte[] cipherKey = Base64.decode(shiroConfigProperties.getRememberMePublickKey());
    	rememberMeManager.setCipherKey(cipherKey);
    	rememberMeManager.setCookie(rememberMeCookie() );
        return rememberMeManager;  
    }     
    
    @Bean(name = "sessionIdCookie")
	 public Cookie sessionIdCookie() {
    	SimpleCookie cookie=new org.apache.shiro.web.servlet.SimpleCookie(shiroConfigProperties.getSessionCookieName());
    	cookie.setHttpOnly(shiroConfigProperties.getSessionCookieHttpOnly());
    	cookie.setMaxAge(shiroConfigProperties.getSessionCookieMaxAge());
    	cookie.setName(shiroConfigProperties.getSessionCookieName());
	    return cookie;
	 }
    
    @Bean(name = "rememberMeCookie")
	 public Cookie rememberMeCookie() {
    	SimpleCookie cookie=new org.apache.shiro.web.servlet.SimpleCookie(shiroConfigProperties.getRememberMeCookieName());
    	cookie.setHttpOnly(shiroConfigProperties.getRememberMeCookieHttpOnly());
    	cookie.setMaxAge(shiroConfigProperties.getRememberMeCookieMaxAge());
	    return cookie;
	 }
    
     
  
	  @Bean(name = "sessionIdGenerator")
	  public SessionIdGenerator sessionIdGenerator() {
		  return new org.zfes.snowy.auth.shiro.session.idgen.SnowySessionIdGenerator();
		  //return new org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator();
	 }    
	 
	@Bean
	@ConditionalOnMissingBean
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
	    DefaultAdvisorAutoProxyCreator daap = new DefaultAdvisorAutoProxyCreator();
	    daap.setProxyTargetClass(true);
	    return daap;
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
	   AuthorizationAttributeSourceAdvisor aasa = new AuthorizationAttributeSourceAdvisor();
	   aasa.setSecurityManager(securityManager);
	   return aasa;
	 }
	 
//------------------------------------------------------------------------------------------------
	
	
	  public SnowyLoginPassThruAuthenticationFilter loginPassThruAuthenticationFilter() {  
		  SnowyLoginPassThruAuthenticationFilter loginPassThruAuthenticationFilter= new SnowyLoginPassThruAuthenticationFilter(); 
		  setP( loginPassThruAuthenticationFilter );
	      return loginPassThruAuthenticationFilter; 
	  } 
	  
	  public SnowyTokenAuthenticationFilter tokenAuthenticationFilter() {  
		  SnowyTokenAuthenticationFilter tokenAuthenticationFilter = new  SnowyTokenAuthenticationFilter();  
		  setP( tokenAuthenticationFilter );
	      return tokenAuthenticationFilter;  
	  } 
	  
	   private void setP(SnowyTokenAuthenticationFilter tokenAuthenticationFilter ){
		      tokenAuthenticationFilter.setLoginPortUrl(shiroConfigProperties.getLoginPortUrl());// from SnowyShiroAccessControlFilter
			  tokenAuthenticationFilter.setLoginSubmitUrl(shiroConfigProperties.getLoginSubmitUrl());
			  tokenAuthenticationFilter.setIsEnableSnowySSOClient(shiroConfigProperties.getSsoEnabled());
			  
			  tokenAuthenticationFilter.setUsernameParam(shiroConfigProperties.getUsernameParam());
			  tokenAuthenticationFilter.setPasswordParam(shiroConfigProperties.getPasswordParam());
			  tokenAuthenticationFilter.setRememberMeParam(shiroConfigProperties.getRememberMeParam());
			  tokenAuthenticationFilter.setLoginOrginParam(shiroConfigProperties.getLoginOrginParam());
			 
			  tokenAuthenticationFilter.setAuthecFailureHandler(authecFailureHandler());
			  tokenAuthenticationFilter.setAuthecSuccessHandler(authecSuccessHandler());
			  tokenAuthenticationFilter.setAuthecUnAuthecHandler(authecUnAuthecHandler());
			  
			  tokenAuthenticationFilter.setMaxSession(shiroConfigProperties.getMaxSession());
			  tokenAuthenticationFilter.setIsEnableSessionStrategy(shiroConfigProperties.getIsEnableSessionStrategy());
			  tokenAuthenticationFilter.setIsEnableCSRFToken(shiroConfigProperties.getIsEnableCSRFToken());
	   }
	  
	  
	  public CSRFTokenFilter csrfTokenFilter() {  
		 CSRFTokenFilter csrfTokenFilter = new  CSRFTokenFilter(); 
		 csrfTokenFilter.setCsrfAuthenticationFailureHandler(csrfFailureHandler());
		 csrfTokenFilter.setCsrfTokenName(shiroConfigProperties.getCsrfTokenName());
	     return csrfTokenFilter;  
	  } 
	  
	  public SnowyJwtAuthTokenFilter jwtAuthTokenFilter() {  
		SnowyJwtAuthTokenFilter jwtAuthTokenFilter = new  SnowyJwtAuthTokenFilter();  
	    return jwtAuthTokenFilter;  
	  } 
	 
	  public SnowyCapchaProducerFilter capchaProducerFilter() {  
		  SnowyCapchaProducerFilter capchaFilter = new  SnowyCapchaProducerFilter();  
		  capchaFilter.setCaptchaImageUrl(shiroConfigProperties.getCaptchaImageUrl());
		  capchaFilter.setCaptchaProducer(producer);
	      return capchaFilter;  
	  } 
	  public SnowyLogoutFilter logoutFilter() {  
		  SnowyLogoutFilter logoutFilter = new  SnowyLogoutFilter(); 
		  logoutFilter.setAuthecLogoutHandler(authecLogoutHandler());
	      return logoutFilter;  
	  } 
		public SSOSyncClientFilter ssoSyncClientFilter() {
			SSOSyncClientFilter clientFilter=new SSOSyncClientFilter();
			if(ZStrUtil.hasText(shiroConfigProperties.getSsoClientSyncPath())){
				clientFilter.setSsoClientSyncPath(shiroConfigProperties.getSsoClientSyncPath());
			}
			return clientFilter;
		}
		public SSOSyncCenterfilter ssoSyncCenterfilter() {
			SSOSyncCenterfilter centerfilter= new SSOSyncCenterfilter();
			if(!ZObjectUtil.isEmpty(shiroConfigProperties.getAllowedSSOClientDomains())){
				centerfilter.setAllowedSSOClientDomains(shiroConfigProperties.getAllowedSSOClientDomains());
			}
			if(ZStrUtil.hasText(shiroConfigProperties.getSsoCenterSyncPath())){
				centerfilter.setSsoCenterSyncPath(shiroConfigProperties.getSsoCenterSyncPath());
			}
			
			return centerfilter;
		}
		
	
	  @Bean(name="authecSuccessHandler")  
	  public IAuthecSuccessHandler authecSuccessHandler() {  
		  SimpleAuthecSuccessHandler handler = new  SimpleAuthecSuccessHandler();  
		  handler.setDefaultAuthecSuccessUrl(shiroConfigProperties.getDefaultAuthecSuccessUrl());
		  handler.setAuthecSuccessForwardToDestination(shiroConfigProperties.getAuthecSuccessForwardToDestination());
	      return handler;  
	  } 
	  @Bean(name="authecFailureHandler")  
	  public IAuthecFailureHandler authecFailureHandler() {  
		  SimpleAuthecFailureHandler handler = new  SimpleAuthecFailureHandler(); 
		  handler.setDefaultAuthecFailureUrl(shiroConfigProperties.getDefaultAuthecSuccessUrl());
		  handler.setAuthecFailureForwardToDestination(shiroConfigProperties.getAuthecFailureForwardToDestination());
	      return handler;  
	  } 
	  @Bean(name="authecLogoutHandler")  
	  public IAuthecLogoutHandler authecLogoutHandler() {  
		  SimpleAuthecLogoutHandler handler = new  SimpleAuthecLogoutHandler();
		  handler.setDefaultLogoutDirectUrl(shiroConfigProperties.getDefaultLogoutDirectUrl());
		  handler.setLogoutForwardToDestination(shiroConfigProperties.getLogoutForwardToDestination());
	      return handler;  
	  } 
	  @Bean(name="authecUnAuthecHandler")  
	  public IUnAuthecHandler authecUnAuthecHandler() {  
		  SimpleUnauthecHandler handler = new  SimpleUnauthecHandler();  
		  handler.setDefaultUnAuthecDirectUrl(shiroConfigProperties.getDefaultUnAuthecDirectUrl());
		  handler.setUnAuthecForwardToDestination(shiroConfigProperties.getUnAuthecForwardToDestination());
	      return handler;  
	  } 
	  
	  @Bean(name="unauthorizedHandler")  
	  public UnauthorizedHandler unauthorizedHandler() {  
		  SimpleUnauthorizedHandler handler = new  SimpleUnauthorizedHandler();  
		  handler.setDefaultUnauthorizedUrl(shiroConfigProperties.getDefaultUnauthorizedUrl());
		  handler.setForwardToDestination(shiroConfigProperties.getUnauthorizedForwardToDestination());
	      return handler;  
	  } 
	  
	 @Bean(name="csrfFailureHandler")  
	  public IAuthecFailureHandler csrfFailureHandler() {  
		  SimpleAuthecCsrfFailureHandler handler = new  SimpleAuthecCsrfFailureHandler();  
		  handler.setDefaultCsrfValidateFailureUrl(shiroConfigProperties.getDefaultCsrfValidateFailureUrl());
		  handler.setCsrfValidateForwardToDestination(shiroConfigProperties.getCsrfValidateForwardToDestination());
	      return handler;  
	  } 
}
