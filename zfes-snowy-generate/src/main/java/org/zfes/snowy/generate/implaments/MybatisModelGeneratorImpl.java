/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.implaments;

import java.util.HashMap;
import java.util.Map;

import org.zfes.snowy.generate.config.GenConst;
import org.zfes.snowy.generate.interfaces.AbstractGenerator;
import org.zfes.snowy.generate.interfaces.MybatisModelGenerator;
import org.zfes.snowy.generate.model.MybatisEntityModel;
import org.zfes.snowy.generate.util.GenUtil;
public class MybatisModelGeneratorImpl extends AbstractGenerator implements MybatisModelGenerator{
	
	@Override
	public void generatorModel(MybatisEntityModel mybatisEntityModel) {
		
		 Map<String, Object> data = new HashMap<String, Object>();
	     data.put("model", mybatisEntityModel);
	     String packageName=mybatisEntityModel.getPackageName();
	     String simpleClassName=mybatisEntityModel.getEntitySimpleClassName();
	     
	     String outputFilePath=new String(GenConst.project_path+GenConst.maven_middle_path+GenUtil.package2path(packageName)+"/"+simpleClassName+".java");
//	     String templateFilepath=GenConst.mybatis_gen_tpl_path+"/Model.java.ftl";
//	     super.generate(templateFilepath, data, filePath);
	     
	     super.generate2(GenConst.gen_tpl_path, GenConst.Tpl.tpl_name_Model, outputFilePath, data);
	}




}
