/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.genarator;

import java.io.FileNotFoundException;
import java.util.List;

import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.generate.implaments.BizControllerGeneratorImpl;
import org.zfes.snowy.generate.implaments.BizServiceImplGeneratorImpl;
import org.zfes.snowy.generate.implaments.BizServiceInterfaceGeneratorImpl;
import org.zfes.snowy.generate.interfaces.BizControllerGenerator;
import org.zfes.snowy.generate.interfaces.BizServiceImplGenerator;
import org.zfes.snowy.generate.interfaces.BizServiceInterfaceGenerator;
import org.zfes.snowy.generate.model.BizControllerModel;
import org.zfes.snowy.generate.model.BizServiceImplModel;
import org.zfes.snowy.generate.model.BizServiceInterfaceModel;
import org.zfes.snowy.generate.model.MybatisProperty;


public class BizGenerator {
	
	   public static void simpleGen(String domainDesc,String controllerNameSpace, String entityPackageName,String entitySimpleClassName,List<MybatisProperty> propertyModelList) throws FileNotFoundException {
	    		
	    		String entityFullClassName=entityPackageName+"."+entitySimpleClassName;
	    		
	    		String servicePackageName=ZStrUtil.substringBeforeLast(entityPackageName, ".")+".service";
	    		String controllerPackageName=ZStrUtil.substringBeforeLast(entityPackageName, ".")+".controller";
	    		String controllerSimpleClassName=entitySimpleClassName+"Controller";
	    		
	    		BizControllerModel controllerModel=new BizControllerModel( entityFullClassName,  entitySimpleClassName,  servicePackageName,
	    				 controllerPackageName,  controllerSimpleClassName,  controllerNameSpace);
	    		controllerModel.setDomainDesc(domainDesc);
	    		
	    		
	    		 String serviceInterfacePackageName=ZStrUtil.substringBeforeLast(entityPackageName, ".")+".service";;
	    		 String serviceInterfaceSimpleClassName="I"+entitySimpleClassName+"Service";
	    		BizServiceInterfaceModel serviceInterfaceModel=new BizServiceInterfaceModel( entityFullClassName, entitySimpleClassName,serviceInterfacePackageName,
	    				serviceInterfaceSimpleClassName);
	    		serviceInterfaceModel.setPropertyModelList(propertyModelList);
	    		
	    		 String serviceImplPackageName=serviceInterfacePackageName+".impl";
	    		 String serviceImplSimpleClassName=entitySimpleClassName+"ServiceImpl";
	    		 
	    		 String mapperPackageName=ZStrUtil.substringBeforeLast(entityPackageName, ".")+".dao";
	    		 
	    		BizServiceImplModel serviceImplModel=new BizServiceImplModel( entityFullClassName,  entitySimpleClassName,  serviceImplPackageName,
	    				 serviceImplSimpleClassName, mapperPackageName,  serviceInterfacePackageName);
	    		serviceImplModel.setPropertyModelList(propertyModelList);
	    		
	    		BizControllerGenerator sControllerGenerator=new BizControllerGeneratorImpl();
	    		sControllerGenerator.generateController(controllerModel);
	    		
	    		BizServiceInterfaceGenerator serviceInterfaceGenerator=new BizServiceInterfaceGeneratorImpl();
	    		serviceInterfaceGenerator.generatorServiceInterface(serviceInterfaceModel);
	    		
	    		BizServiceImplGenerator serviceImplGenerator=new BizServiceImplGeneratorImpl();
	    		serviceImplGenerator.generatorServiceImpl(serviceImplModel);
	    		
	    	}
}
