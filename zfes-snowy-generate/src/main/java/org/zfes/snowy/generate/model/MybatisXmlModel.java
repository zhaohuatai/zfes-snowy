/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;

import java.util.List;

public class MybatisXmlModel {
	private String modelSimpleClassName;
	
	private String modelFullClassName;
	private String mapperFullClassName;
	private String mapperSimpleClassName;
	private String mapperPackageName;
	private String tableName;
	
	private List<MybatisXmlProperty> modelRalList;
	
	public List<MybatisXmlProperty> getModelRalList() {
		return modelRalList;
	}
	public void setModelRalList(List<MybatisXmlProperty> modelRalList) {
		this.modelRalList = modelRalList;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getModelFullClassName() {
		return modelFullClassName;
	}
	public void setModelFullClassName(String modelFullClassName) {
		this.modelFullClassName = modelFullClassName;
	}
	public String getMapperFullClassName() {
		return mapperFullClassName;
	}
	public void setMapperFullClassName(String mapperFullClassName) {
		this.mapperFullClassName = mapperFullClassName;
	}
	public String getMapperSimpleClassName() {
		return mapperSimpleClassName;
	}
	public void setMapperSimpleClassName(String mapperSimpleClassName) {
		this.mapperSimpleClassName = mapperSimpleClassName;
	}
	public String getMapperPackageName() {
		return mapperPackageName;
	}
	public void setMapperPackageName(String mapperPackageName) {
		this.mapperPackageName = mapperPackageName;
	}
	public String getModelSimpleClassName() {
		return modelSimpleClassName;
	}
	public void setModelSimpleClassName(String modelSimpleClassName) {
		this.modelSimpleClassName = modelSimpleClassName;
	}
	
	

}
