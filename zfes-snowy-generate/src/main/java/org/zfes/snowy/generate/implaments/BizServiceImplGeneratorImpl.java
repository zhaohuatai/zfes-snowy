/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.implaments;

import java.util.HashMap;
import java.util.Map;

import org.zfes.snowy.generate.config.GenConst;
import org.zfes.snowy.generate.interfaces.AbstractGenerator;
import org.zfes.snowy.generate.interfaces.BizServiceImplGenerator;
import org.zfes.snowy.generate.model.BizServiceImplModel;
import org.zfes.snowy.generate.util.GenUtil;

public class BizServiceImplGeneratorImpl extends AbstractGenerator implements BizServiceImplGenerator{
	
	@Override
	public void generatorServiceImpl(BizServiceImplModel serviceImplModel) {
		
		 Map<String, Object> data = new HashMap<String, Object>();
	     data.put("model", serviceImplModel);
	     String packageName=serviceImplModel.getServiceImplPackageName();
	     String simpleClassName=serviceImplModel.getServiceImplSimpleClassName();
	     
	     
	     String outputFilePath=new String(GenConst.project_path+GenConst.maven_middle_path+GenUtil.package2path(packageName)+"/"+simpleClassName+".java");
//	     String templateFilepath=GenConst.clazz_tpl_path+"/ServiceImpl.java.ftl";
//	     super.generate(templateFilepath, data, outputFilePath);
	     
	     super.generate2(GenConst.gen_tpl_path, GenConst.Tpl.tpl_name_ServiceImpl, outputFilePath, data);
	     
	}




}
