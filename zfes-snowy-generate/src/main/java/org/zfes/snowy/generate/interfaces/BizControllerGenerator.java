/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.interfaces;

import org.zfes.snowy.generate.model.BizControllerModel;

public interface BizControllerGenerator extends  Generator{
	
	public void generateController(BizControllerModel controllerModel);
	
}
