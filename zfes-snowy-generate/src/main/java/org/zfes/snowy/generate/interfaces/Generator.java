/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.interfaces;
import java.util.Map;

public interface Generator {
	/**
	 * 基于freemarker代码生成接口
	 * 
	 * @param tplFullPathFileName :代码生成模板文件全路径文件名
	 * @param data : freemarker模板所需数据
	 * @param outPutfileFullPathName : 生成文件全路径文件名
	 */
	@Deprecated
	public void generate(String tplFullPathFileName, Map<?,?> data,String outPutfileFullPathName) ;
	
	public void generate2(String tplPath,String tplFileName,String outPutfileFullPathName, Map<?,?> data) ;
	
	
}
