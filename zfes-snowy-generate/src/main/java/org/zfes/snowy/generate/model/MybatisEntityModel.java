/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */

package org.zfes.snowy.generate.model;

import java.util.List;


public class MybatisEntityModel  {
	
	private String entityFullClassName;
	private String entitySimpleClassName;
	private String packageName;
	private List<MybatisProperty> propertyModelList;

	public String getEntityFullClassName() {
		return entityFullClassName;
	}

	public void setEntityFullClassName(String entityFullClassName) {
		this.entityFullClassName = entityFullClassName;
	}

	public String getEntitySimpleClassName() {
		return entitySimpleClassName;
	}

	public void setEntitySimpleClassName(String entitySimpleClassName) {
		this.entitySimpleClassName = entitySimpleClassName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

//	public String getTableName() {
//		return tableName;
//	}
//
//	public void setTableName(String tableName) {
//		this.tableName = tableName;
//	}

	public List<MybatisProperty> getPropertyModelList() {
		return propertyModelList;
	}

	public void setPropertyModelList(List<MybatisProperty> propertyModelList) {
		this.propertyModelList = propertyModelList;
	}



	

	

}
