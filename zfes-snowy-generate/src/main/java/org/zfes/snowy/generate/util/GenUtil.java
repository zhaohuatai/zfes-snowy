/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.util;

import org.apache.commons.lang3.StringUtils;

public class GenUtil {
	/**
	 *包名转化为文件路径
	 * @param packageName
	 * @return
	 */
	public static String package2path(String packageName) {
		return packageName.replace('.', '/');
	}
	
	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public static  String  getFileName(String filePath) {
		String fileName=StringUtils.substringAfterLast(filePath, "/");
		 if(fileName.equals("")||fileName==null){
			 fileName=StringUtils.substringAfterLast(filePath, "\\");
		 }
		 return fileName;
	}

	/**
	 * 首字母大写
	 * @param string
	 * @return
	 */
	public static  String capFirst(String string) {
		String s = String.valueOf(string.charAt(0)).toUpperCase();
		s = s + string.substring(1);
		return s;
	}

	/**
	 * 首字母变小写
	 * @param string
	 * @return
	 */
	public static  String uncapFirst(String string) {
		String s = String.valueOf(string.charAt(0)).toLowerCase();
		s = s + string.substring(1);
		return s;
	}
}
