/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.config;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class GenFreemarkerConfig {

	private static Configuration cfg = null;
	private static Configuration cfg2 = null;

	public static Configuration getConfiguration(String templateDir) throws IOException {
		if (null == cfg) {
			cfg = new Configuration(Configuration.VERSION_2_3_0);
	        File templateDirFile = new File(templateDir);
	        cfg.setDirectoryForTemplateLoading(templateDirFile);
	        cfg.setClassForTemplateLoading(GenFreemarkerConfig.class, "");
	      
	        cfg.setLocale(Locale.CHINA);
	        cfg.setDefaultEncoding("UTF-8");
		}
		return cfg;
	}
	
	public static Template getTemplate(String basePath,String templateFileName) throws IOException {
		if (null == cfg2) {
				cfg2 = new Configuration(Configuration.VERSION_2_3_0);
			
				cfg2.setLocale(Locale.CHINA);
				cfg2.setDefaultEncoding("UTF-8");
				cfg2.setClassForTemplateLoading(GenFreemarkerConfig.class, basePath);
			}
	      Template tpl=cfg2.getTemplate(templateFileName);
		return tpl;
	}
	
	public static void main(String[] args) throws IOException {
		Template tpl= GenFreemarkerConfig.getTemplate("/templates/", "Controller.java.ftl");
		System.out.println(tpl);
		Template tp2=GenFreemarkerConfig.getTemplate("/templates/", "DaoImpl.java.ftl");
		System.out.println(tp2);
	}
}
