/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;

import java.util.List;


public class BizServiceImplModel {

	public BizServiceImplModel(String entityFullClassName, String entitySimpleClassName, String serviceImplPackageName,
			String serviceImplSimpleClassName, String mapperPackageName, String serviceInterfacePackageName) {
		super();
		this.entityFullClassName = entityFullClassName;
		this.entitySimpleClassName = entitySimpleClassName;
		this.serviceImplPackageName = serviceImplPackageName;
		this.serviceImplSimpleClassName = serviceImplSimpleClassName;
		this.mapperPackageName = mapperPackageName;
		this.serviceInterfacePackageName = serviceInterfacePackageName;
	}


	private String entityFullClassName;
	private String entitySimpleClassName;
	
	private String serviceImplPackageName;
	
	private String serviceImplSimpleClassName;
	private String mapperPackageName;
	
	private String serviceInterfacePackageName;

	
	private List<MybatisProperty> propertyModelList;
	
	
	public String getEntityFullClassName() {
		return entityFullClassName;
	}


	public void setEntityFullClassName(String entityFullClassName) {
		this.entityFullClassName = entityFullClassName;
	}


	public String getEntitySimpleClassName() {
		return entitySimpleClassName;
	}


	public void setEntitySimpleClassName(String entitySimpleClassName) {
		this.entitySimpleClassName = entitySimpleClassName;
	}




	public String getMapperPackageName() {
		return mapperPackageName;
	}


	public void setMapperPackageName(String mapperPackageName) {
		this.mapperPackageName = mapperPackageName;
	}


	public String getServiceImplPackageName() {
		return serviceImplPackageName;
	}


	public void setServiceImplPackageName(String serviceImplPackageName) {
		this.serviceImplPackageName = serviceImplPackageName;
	}


	public String getServiceImplSimpleClassName() {
		return serviceImplSimpleClassName;
	}


	public void setServiceImplSimpleClassName(String serviceImplSimpleClassName) {
		this.serviceImplSimpleClassName = serviceImplSimpleClassName;
	}


	public String getServiceInterfacePackageName() {
		return serviceInterfacePackageName;
	}


	public void setServiceInterfacePackageName(String serviceInterfacePackageName) {
		this.serviceInterfacePackageName = serviceInterfacePackageName;
	}


	public List<MybatisProperty> getPropertyModelList() {
		return propertyModelList;
	}


	public void setPropertyModelList(List<MybatisProperty> propertyModelList) {
		this.propertyModelList = propertyModelList;
	}



	
	
}
