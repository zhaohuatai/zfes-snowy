/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.config;


public class GenConst {
	@Deprecated
	public static final String mybatis_gen_tpl_path="/templates/";
	@Deprecated
	public static final String clazz_tpl_path="/templates/";
	
	public static final String maven_middle_path="/src/main/java/";
	
	
	public static   String project_path="";
	
	public static  String mybatis_gen_configfile="";
	
	public static final String gen_tpl_path="/templates/";
	
	public static final  class Tpl{
		
		public static final  String tpl_name_Controller="Controller.java.ftl";
		public static final  String tpl_name_DaoImpl="DaoImpl.java.ftl";
		public static final  String tpl_name_DaoInterface="DaoInterface.java.ftl";
		public static final  String tpl_name_Mapper="Mapper.java.ftl";
		public static final  String tpl_name_Mapperxml="Mapper.xml.ftl";
		public static final  String tpl_name_Model="Model.java.ftl";
		public static final  String tpl_name_ServiceImpl="ServiceImpl.java.ftl";
		public static final  String tpl_name_ServiceInterface="ServiceInterface.java.ftl";
		
		public static final  String tpl_name_html_main="main.html.ftl";
		public static final  String tpl_name_html_add="add.html.ftl";
		public static final  String tpl_name_html_edit="edit.html.ftl";
		
	}

	
	
}
