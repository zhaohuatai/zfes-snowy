/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.zfes.snowy.generate.config.GenConst;
import org.zfes.snowy.generate.genarator.MybatisGenerator;

import com.google.common.collect.Maps;
public class GenMain {
    private static final String CONFIG_FILE = "-configfile"; 
    private static final String CONTEXT_IDS = "-contextids"; 
    private static final String TABLES = "-tables"; 
    
    public static void main(String[] args){
    	
    	String project_path="E:/workspace/neno20170426/zfes-snowy-project-demo/";
    	String mybatis_gen_configfile=project_path+"/src/main/resources/generatorConfig.xml";
    	
    	//String project_path=args[0];
    	//String mybatis_gen_configfile=args[1];
    	try{
    		doGen(project_path,mybatis_gen_configfile);
    	}catch (Exception e) {
    		e.printStackTrace();
		}
    	
    }

    public static void doGen(String project_path ,String mybatis_gen_configfile) throws FileNotFoundException {
    	
    	
    	setPath( project_path , mybatis_gen_configfile);
    	
        Map<String, String> arguments =Maps.newConcurrentMap();
        arguments.put(CONFIG_FILE, mybatis_gen_configfile);
        arguments.put(CONTEXT_IDS, "");
        List<String> warnings = new ArrayList<String>();
        String configfile = arguments.get(CONFIG_FILE);
        File configurationFile = new File(configfile);
        if (!configurationFile.exists()) {
        	throw new FileNotFoundException();
        }
        setPath( project_path , mybatis_gen_configfile);
        
        
        Set<String> fullyqualifiedTables = new HashSet<String>();
        if (arguments.containsKey(TABLES)) {
            StringTokenizer st = new StringTokenizer(arguments.get(TABLES), ","); //$NON-NLS-1$
            while (st.hasMoreTokens()) {
                String s = st.nextToken().trim();
                if (s.length() > 0) {
                    fullyqualifiedTables.add(s);
                }
            }
        }

        Set<String> contexts = new HashSet<String>();
        if (arguments.containsKey(CONTEXT_IDS)) {
            StringTokenizer st = new StringTokenizer(arguments.get(CONTEXT_IDS), ","); //$NON-NLS-1$
            while (st.hasMoreTokens()) {
                String s = st.nextToken().trim();
                if (s.length() > 0) {
                    contexts.add(s);
                }
            }
        }
        try {
        	System.out.println("--------------run------start--------------");
            ConfigurationParser cp = new ConfigurationParser(warnings);
            Configuration config = cp.parseConfiguration(configurationFile);
            MybatisGenerator myBatisGenerator = new MybatisGenerator(config,  warnings);
            myBatisGenerator.generate( contexts, fullyqualifiedTables);
            System.out.println("---------------run-----over--------------");
        } catch (Exception e) {
        	  e.printStackTrace();
            return;
        } 
    }
    private static void setPath(String project_path ,String mybatis_gen_configfile){
    	GenConst.project_path=project_path;
    	GenConst.mybatis_gen_configfile=mybatis_gen_configfile;
    	
    }

	
}
