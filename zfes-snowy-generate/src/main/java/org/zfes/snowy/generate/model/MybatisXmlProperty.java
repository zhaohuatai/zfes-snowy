/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;

public class MybatisXmlProperty {
	
	private String column;
	private String property;
	private String jdbcType;
	private String propertyType;//java.lang.String
	private Boolean unique;
	
	private Boolean isQueryField;
	private String queryLogic;
	public MybatisXmlProperty() {

	}
	public MybatisXmlProperty(String column, String property, String jdbcType) {
		super();
		this.column = column;
		this.property = property;
		this.jdbcType = jdbcType;
	}
	public MybatisXmlProperty(String column, String property, String jdbcType, String propertyType) {
		super();
		this.column = column;
		this.property = property;
		this.jdbcType = jdbcType;
		this.propertyType = propertyType;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getJdbcType() {
		return jdbcType;
	}
	public void setJdbcType(String jdbcType) {
		this.jdbcType = jdbcType;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public Boolean getUnique() {
		return unique;
	}
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}
	public Boolean getIsQueryField() {
		return isQueryField;
	}
	public void setIsQueryField(Boolean isQueryField) {
		this.isQueryField = isQueryField;
	}
	public String getQueryLogic() {
		return queryLogic;
	}
	public void setQueryLogic(String queryLogic) {
		this.queryLogic = queryLogic;
	}

}
