/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;

public class MybatisProperty {

	private String propertyName;
	private String propertyType;
	
	private String remarks;
	private Boolean nullable;
	private Integer length;
	private Integer scale;
	private Boolean unique;
	
	
	private Boolean isQueryField;
	private String queryLogic;
	
	public MybatisProperty(String propertyName,String propertyType){
		this.propertyName = propertyName;
		this.propertyType = propertyType;
	}
	
	
	public MybatisProperty(String propertyName, String propertyType, String remarks, Boolean nullable,
			Integer length, Integer scale) {
		super();
		this.propertyName = propertyName;
		this.propertyType = propertyType;
		this.remarks = remarks;
		this.nullable = nullable;
		this.length = length;
		this.scale = scale;
	}


	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public Boolean getNullable() {
		return nullable;
	}
	public void setNullable(Boolean nullable) {
		this.nullable = nullable;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getScale() {
		return scale;
	}
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Boolean getUnique() {
		return unique;
	}
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}
	public Boolean getIsQueryField() {
		return isQueryField;
	}
	public void setIsQueryField(Boolean isQueryField) {
		this.isQueryField = isQueryField;
	}
	public String getQueryLogic() {
		return queryLogic;
	}
	public void setQueryLogic(String queryLogic) {
		this.queryLogic = queryLogic;
	}
	
	
}
