/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.interfaces;

import org.zfes.snowy.generate.model.BizServiceInterfaceModel;

public interface BizServiceInterfaceGenerator extends  Generator{
	
	public void generatorServiceInterface(BizServiceInterfaceModel serviceModel);
	
}
