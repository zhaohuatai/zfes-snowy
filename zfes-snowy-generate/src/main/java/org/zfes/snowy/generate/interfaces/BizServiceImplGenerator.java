/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.interfaces;

import org.zfes.snowy.generate.model.BizServiceImplModel;

public interface BizServiceImplGenerator extends  Generator{
	
	public void generatorServiceImpl(BizServiceImplModel serviceImplModel);
	
}
