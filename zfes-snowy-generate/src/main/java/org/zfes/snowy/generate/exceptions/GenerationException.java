/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.exceptions;

public class GenerationException  extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GenerationException() {
		super();
	}

	public GenerationException(String message, Throwable cause) {
		super(message, cause);
	}

	public GenerationException(String message) {
		super(message);
	}

	public GenerationException(Throwable cause) {
		super(cause);
	}


}
