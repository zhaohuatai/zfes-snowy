/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;


public class BizControllerModel  {
	
	public BizControllerModel(String entityFullClassName, String entitySimpleClassName, String servicePackageName,
			String controllerPackageName, String controllerSimpleClassName, String controllerNameSpace) {
		super();
		this.entityFullClassName = entityFullClassName;
		this.entitySimpleClassName = entitySimpleClassName;
		this.servicePackageName = servicePackageName;
		this.controllerPackageName = controllerPackageName;
		this.controllerSimpleClassName = controllerSimpleClassName;
		this.controllerNameSpace = controllerNameSpace;
	}

	private String domainDesc;
	
	private String entityFullClassName;
	private String entitySimpleClassName;
	
	private String servicePackageName;
	
	private String controllerPackageName;
	private String controllerSimpleClassName;
	private String controllerNameSpace;

	public String getEntityFullClassName() {
		return entityFullClassName;
	}

	public void setEntityFullClassName(String entityFullClassName) {
		this.entityFullClassName = entityFullClassName;
	}

	public String getEntitySimpleClassName() {
		return entitySimpleClassName;
	}

	public void setEntitySimpleClassName(String entitySimpleClassName) {
		this.entitySimpleClassName = entitySimpleClassName;
	}



	public String getControllerPackageName() {
		return controllerPackageName;
	}

	public void setControllerPackageName(String controllerPackageName) {
		this.controllerPackageName = controllerPackageName;
	}

	public String getControllerNameSpace() {
		return controllerNameSpace;
	}

	public void setControllerNameSpace(String controllerNameSpace) {
		this.controllerNameSpace = controllerNameSpace;
	}

	public String getControllerSimpleClassName() {
		return controllerSimpleClassName;
	}

	public void setControllerSimpleClassName(String controllerSimpleClassName) {
		this.controllerSimpleClassName = controllerSimpleClassName;
	}

	public String getServicePackageName() {
		return servicePackageName;
	}

	public void setServicePackageName(String servicePackageName) {
		this.servicePackageName = servicePackageName;
	}

	public String getDomainDesc() {
		return domainDesc;
	}

	public void setDomainDesc(String domainDesc) {
		this.domainDesc = domainDesc;
	}

}
