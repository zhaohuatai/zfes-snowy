/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.implaments;

import java.util.HashMap;
import java.util.Map;

import org.zfes.snowy.generate.config.GenConst;
import org.zfes.snowy.generate.interfaces.AbstractGenerator;
import org.zfes.snowy.generate.interfaces.MybatisMapperGenerator;
import org.zfes.snowy.generate.model.MybatisMapperModel;
import org.zfes.snowy.generate.util.GenUtil;


public class MybatisMapperGeneratorImpl extends AbstractGenerator implements MybatisMapperGenerator{
	
	@Override
	public void generateMapper(MybatisMapperModel mybatisMapperModel) {
		
		 Map<String, Object> data = new HashMap<String, Object>();
	     data.put("model", mybatisMapperModel);
	     String packageName=mybatisMapperModel.getMapperPackageName();
	     String simpleClassName=mybatisMapperModel.getMapperSimpleClassName();
	     
	     String outputFilePath=new String(GenConst.project_path+GenConst.maven_middle_path+GenUtil.package2path(packageName)+"/"+simpleClassName+".java");
	   
//	     String tplPath = this.getClass().getClassLoader().getResource("").toString();
//	     String templateFilepath=tplPath+GenConst.mybatis_gen_tpl_path+"/Mapper.java.ftl";
//	     super.generate(templateFilepath, data, filePath);
	     
	     super.generate2(GenConst.gen_tpl_path, GenConst.Tpl.tpl_name_Mapper, outputFilePath, data);
	}

}
