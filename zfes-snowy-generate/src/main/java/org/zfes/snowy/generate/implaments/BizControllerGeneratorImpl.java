/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.implaments;

import java.util.HashMap;
import java.util.Map;

import org.zfes.snowy.generate.config.GenConst;
import org.zfes.snowy.generate.interfaces.AbstractGenerator;
import org.zfes.snowy.generate.interfaces.BizControllerGenerator;
import org.zfes.snowy.generate.model.BizControllerModel;
import org.zfes.snowy.generate.util.GenUtil;



public class BizControllerGeneratorImpl extends AbstractGenerator implements BizControllerGenerator{
	@Override
	public void generateController( BizControllerModel controllerModel) {
		
		 Map<String, Object> data = new HashMap<String, Object>();
	     data.put("model", controllerModel);
	
		
	     String packageName=controllerModel.getControllerPackageName();
	     String simpleClassName=controllerModel.getControllerSimpleClassName();
	     
	     String outputFilePath=new String(GenConst.project_path+GenConst.maven_middle_path+GenUtil.package2path(packageName)+"/"+simpleClassName+".java");
	   
	     //String templateFilepath=GenConst.clazz_tpl_path+"/Controller.java.ftl";
	    
	    // super.generate(templateFilepath, data, outputFilePath);
	     
	     super.generate2(GenConst.gen_tpl_path, GenConst.Tpl.tpl_name_Controller, outputFilePath, data);
	}

}
