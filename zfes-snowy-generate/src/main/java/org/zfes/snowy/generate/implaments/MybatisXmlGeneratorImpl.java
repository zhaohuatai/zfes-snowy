/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.implaments;

import java.util.HashMap;
import java.util.Map;

import org.zfes.snowy.generate.config.GenConst;
import org.zfes.snowy.generate.interfaces.AbstractGenerator;
import org.zfes.snowy.generate.interfaces.MybatisXmlGenerator;
import org.zfes.snowy.generate.model.MybatisXmlModel;
import org.zfes.snowy.generate.util.GenUtil;
public class MybatisXmlGeneratorImpl extends AbstractGenerator implements MybatisXmlGenerator{
	
	@Override
	public void generatorXml(MybatisXmlModel xmlModel) {
		
		 Map<String, Object> data = new HashMap<String, Object>();
	     data.put("model", xmlModel);
	     String packageName=xmlModel.getMapperPackageName();
	     String simpleClassName=xmlModel.getMapperSimpleClassName();
	     
	     String outputFilePath=new String(GenConst.project_path+GenConst.maven_middle_path+GenUtil.package2path(packageName)+"/"+simpleClassName+".xml");
//	     String templateFilepath=GenConst.mybatis_gen_tpl_path+"/Mapper.xml.ftl";
//	     super.generate(templateFilepath, data, filePath);
	     
	     super.generate2(GenConst.gen_tpl_path, GenConst.Tpl.tpl_name_Mapperxml, outputFilePath, data);
	}

}
