/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;

import java.util.List;



public class MybatisMapperModel {
	
	private String  mapperFullClassName;
	private String  mapperSimpleClassName;
	private String  mapperPackageName;
	
	private String  modelPackageName;
	private String  modelFullClassName;
	private String  modelSimpleClassName;
	
	
	private List<MybatisProperty> propertyModelList;
	
	
	public String getMapperFullClassName() {
		return mapperFullClassName;
	}
	public void setMapperFullClassName(String mapperFullClassName) {
		this.mapperFullClassName = mapperFullClassName;
	}
	public String getMapperSimpleClassName() {
		return mapperSimpleClassName;
	}
	public void setMapperSimpleClassName(String mapperSimpleClassName) {
		this.mapperSimpleClassName = mapperSimpleClassName;
	}
	public String getMapperPackageName() {
		return mapperPackageName;
	}
	public void setMapperPackageName(String mapperPackageName) {
		this.mapperPackageName = mapperPackageName;
	}
	public String getModelPackageName() {
		return modelPackageName;
	}
	public void setModelPackageName(String modelPackageName) {
		this.modelPackageName = modelPackageName;
	}
	public String getModelFullClassName() {
		return modelFullClassName;
	}
	public void setModelFullClassName(String modelFullClassName) {
		this.modelFullClassName = modelFullClassName;
	}
	public String getModelSimpleClassName() {
		return modelSimpleClassName;
	}
	public void setModelSimpleClassName(String modelSimpleClassName) {
		this.modelSimpleClassName = modelSimpleClassName;
	}
	public List<MybatisProperty> getPropertyModelList() {
		return propertyModelList;
	}
	public void setPropertyModelList(List<MybatisProperty> propertyModelList) {
		this.propertyModelList = propertyModelList;
	}
	

	
}
