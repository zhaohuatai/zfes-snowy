/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.generate.model;

import java.util.List;

public class BizServiceInterfaceModel {
	
	public BizServiceInterfaceModel(String entityFullClassName, String entitySimpleClassName, String serviceInterfacePackageName,
			String serviceInterfaceSimpleClassName) {
		super();
		this.entityFullClassName = entityFullClassName;
		this.entitySimpleClassName = entitySimpleClassName;
		this.serviceInterfacePackageName = serviceInterfacePackageName;
		this.serviceInterfaceSimpleClassName = serviceInterfaceSimpleClassName;
	}

	private String entityFullClassName;
	private String entitySimpleClassName;
	
	private String serviceInterfacePackageName;
	private String serviceInterfaceSimpleClassName;
	
	private List<MybatisProperty> propertyModelList;

	public String getEntityFullClassName() {
		return entityFullClassName;
	}

	public void setEntityFullClassName(String entityFullClassName) {
		this.entityFullClassName = entityFullClassName;
	}

	public String getEntitySimpleClassName() {
		return entitySimpleClassName;
	}

	public void setEntitySimpleClassName(String entitySimpleClassName) {
		this.entitySimpleClassName = entitySimpleClassName;
	}

	public String getServiceInterfacePackageName() {
		return serviceInterfacePackageName;
	}

	public void setServiceInterfacePackageName(String serviceInterfacePackageName) {
		this.serviceInterfacePackageName = serviceInterfacePackageName;
	}

	public String getServiceInterfaceSimpleClassName() {
		return serviceInterfaceSimpleClassName;
	}

	public void setServiceInterfaceSimpleClassName(String serviceInterfaceSimpleClassName) {
		this.serviceInterfaceSimpleClassName = serviceInterfaceSimpleClassName;
	}

	public List<MybatisProperty> getPropertyModelList() {
		return propertyModelList;
	}

	public void setPropertyModelList(List<MybatisProperty> propertyModelList) {
		this.propertyModelList = propertyModelList;
	}


	
}
