package ${model.mapperPackageName};
import java.util.List;
import java.util.Map;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.core.data.ParamMap;
import ${model.modelPackageName}.${model.modelSimpleClassName};
@BatisScan
public interface ${model.mapperSimpleClassName} extends BaseMapper<${model.modelSimpleClassName}>{
	
	${model.modelSimpleClassName} selectByPrimaryKey(Long id);
	
	int deleteByPrimaryKey(Long id);
	
	int insertSelective(${model.modelSimpleClassName} ${model.modelSimpleClassName  ? cap_first});
	
	int updateByPrimaryKeySelective(${model.modelSimpleClassName} ${model.modelSimpleClassName  ? cap_first});
	
	Long select${model.modelSimpleClassName}ListCount(ParamMap params);
	
	List<Map<String, Object>> select${model.modelSimpleClassName}List(ParamMap params);
	
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	${model.modelSimpleClassName} select${model.modelSimpleClassName}By${propertyx.propertyName ? cap_first}(${propertyx.propertyType} ${propertyx.propertyName});
	</#if></#if>
	</#list>
	
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	long select${model.modelSimpleClassName}CountBy${propertyx.propertyName ? cap_first}(${propertyx.propertyType} ${propertyx.propertyName});
	</#if></#if>
	</#list>
	
}
