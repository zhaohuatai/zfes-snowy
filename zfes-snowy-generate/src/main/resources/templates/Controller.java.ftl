package ${model.controllerPackageName};

import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.base.web.controller.BaseController;
import ${model.entityFullClassName};
import ${model.servicePackageName}.I${model.entitySimpleClassName}Service;
@Controller 
@RequestMapping("${model.controllerNameSpace}/${model.entitySimpleClassName ? uncap_first}")
public class ${model.controllerSimpleClassName} extends BaseController {

	
	@Autowired
	private I${model.entitySimpleClassName}Service ${model.entitySimpleClassName ? uncap_first}Service;
	
	@RequiresPermissions(value="${model.entitySimpleClassName ? uncap_first}:load${model.entitySimpleClassName}DataSet",desc="${model.domainDesc}列表")
    @RequestMapping(value="/load${model.entitySimpleClassName}DataSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object load${model.entitySimpleClassName}DataSet(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  ${model.entitySimpleClassName ? uncap_first}Service.load${model.entitySimpleClassName}DataSet(params);
	    return ajaxQuery("dataSet", dataSet);
    }
	
	@RequiresPermissions(value="${model.entitySimpleClassName ? uncap_first}:create${model.entitySimpleClassName}",desc="${model.domainDesc}添加")
    @RequestMapping(value="/create${model.entitySimpleClassName}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object create${model.entitySimpleClassName}(${model.entitySimpleClassName} ${model.entitySimpleClassName ? uncap_first}){
		${model.entitySimpleClassName ? uncap_first}Service.create${model.entitySimpleClassName}(${model.entitySimpleClassName ? uncap_first});
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="${model.entitySimpleClassName ? uncap_first}:load${model.entitySimpleClassName}",desc="${model.domainDesc}加载")
    @RequestMapping(value="/load${model.entitySimpleClassName}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object load${model.entitySimpleClassName}(Long id){
		Optional<${model.entitySimpleClassName}> ops = ${model.entitySimpleClassName ? uncap_first}Service.load${model.entitySimpleClassName}ById(id);
		return ajaxQuery("${model.entitySimpleClassName ? uncap_first}",ops);
    }
	
	@RequiresPermissions(value="${model.entitySimpleClassName ? uncap_first}:update${model.entitySimpleClassName}",desc="${model.domainDesc}更新")
    @RequestMapping(value="/update${model.entitySimpleClassName}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object update${model.entitySimpleClassName}(${model.entitySimpleClassName} ${model.entitySimpleClassName ? uncap_first}){
		${model.entitySimpleClassName ? uncap_first}Service.update${model.entitySimpleClassName}(${model.entitySimpleClassName ? uncap_first});
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="${model.entitySimpleClassName ? uncap_first}:delet${model.entitySimpleClassName}",desc="${model.domainDesc}删除")
    @RequestMapping(value="/delete${model.entitySimpleClassName}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object delete${model.entitySimpleClassName}(Long id){
		${model.entitySimpleClassName ? uncap_first}Service.delete${model.entitySimpleClassName}(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

}