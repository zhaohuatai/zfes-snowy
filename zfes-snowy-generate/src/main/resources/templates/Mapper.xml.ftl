<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${model.mapperFullClassName}" >
  <resultMap id="BaseResultMap" type="${model.modelFullClassName}" >
  <#list model.modelRalList as propertyx>
	<result column="${propertyx.column}" property="${propertyx.property}" jdbcType="${propertyx.jdbcType}" />
  </#list>
  </resultMap>
  <sql id="Base_Column_List" >
    <#list model.modelRalList as propertyx>${propertyx.column}<#if propertyx_index+1<model.modelRalList?size >,</#if></#list>
  </sql>
   <sql id="Alias_Base_Column_List" >
    <#list model.modelRalList as propertyx>${r'${'}alias${r'}'}.${propertyx.column}<#if propertyx_index+1<model.modelRalList?size >,</#if></#list>
  </sql>
  
   <select id="selectByPrimaryKey" resultMap="BaseResultMap" parameterType="java.lang.Long" >
    select  <include refid="Base_Column_List" /> from ${model.tableName}  where id = ${r'#'}{id,jdbcType=BIGINT}
  </select>
  
  <delete id="deleteByPrimaryKey" parameterType="java.lang.Long" >
    delete from ${model.tableName} where id = ${r'#'}{id,jdbcType=BIGINT}
  </delete>

 <update id="updateStatus">
    	update ${model.tableName}   set status = ${r'#'}{status,jdbcType=TINYINT} where id =${r'#'}{id,jdbcType=BIGINT}
 </update>
  
  <insert id="insertSelective" parameterType="${model.modelFullClassName}" useGeneratedKeys="true" keyProperty="id" >
    insert into ${model.tableName}
    <trim prefix="(" suffix=")" suffixOverrides="," >
     <#list model.modelRalList as propertyx>
       <if test="${propertyx.property} != null" >${propertyx.column},</if>
     </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides="," >
      <#list model.modelRalList as propertyx>
       <if test="${propertyx.property} != null" > ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}},</if>
     </#list>
    </trim>
  </insert>
  
    <insert id="insert" parameterType="${model.modelFullClassName}" useGeneratedKeys="true" keyProperty="id" >
    insert into ${model.tableName}
    <trim prefix="(" suffix=")" suffixOverrides="," >
     <#list model.modelRalList as propertyx>
       ${propertyx.column},
     </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides="," >
      <#list model.modelRalList as propertyx>
       ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}},
     </#list>
    </trim>
  </insert>
  
    <update id="updateByPrimaryKeySelective" parameterType="${model.modelFullClassName}" >
    update ${model.tableName}
    <set>
     <#list model.modelRalList as propertyx>
     <#if propertyx.property !='id' >
       <if test="${propertyx.property} != null" >${propertyx.column} = ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}},</if>
      </#if>
     </#list>
    </set>
    where id = ${r'#'}{id,jdbcType=BIGINT}
  </update>
  
      <update id="update" parameterType="${model.modelFullClassName}" >
    update ${model.tableName}
    <set>
     <#list model.modelRalList as propertyx>
     <#if propertyx.property !='id' >
       ${propertyx.column} = ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}},
      </#if>
     </#list>
    </set>
    where id = ${r'#'}{id,jdbcType=BIGINT}
  </update>
  
   <!-- 查询Unique -->
   
   	<#list model.modelRalList as propertyx>
	<#if (propertyx.property !='aid'&&propertyx.property !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	 <select id="selectBy${propertyx.property ? cap_first}" resultMap="BaseResultMap" parameterType = "${propertyx.propertyType}"  >
   	 select  <include refid="Base_Column_List" /> from ${model.tableName}  where ${propertyx.column} = ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}}
  	</select>
	</#if></#if>
	</#list>
	
	<#list model.modelRalList as propertyx>
	<#if (propertyx.property !='aid'&&propertyx.property !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	 <select id="selectIdBy${propertyx.property ? cap_first}" resultMap="BaseResultMap" parameterType = "${propertyx.propertyType}"  >
   	 select  id from ${model.tableName}  where ${propertyx.column} = ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}}
  	</select>
	</#if></#if>
	</#list>
  
  <!--  分页查询 -->
      <select id="selectMapList" resultType="org.zfes.snowy.core.data.Record" parameterType="java.util.Map">
		<include refid="org.zfes.snowy.base.dao.mybatis.BaseMapper.mysql_page_start" />
		SELECT
		<include refid="Alias_Base_Column_List"><property name="alias" value="zfest" /></include>
		FROM ${model.tableName} zfest WHERE 1=1
		<include refid="selectList_where"><property name="alias" value="zfest" /></include>
		<if test="sort != null">ORDER BY zfest.${r'${sort}'} ${r'${order}'}</if>
		<include refid="org.zfes.snowy.base.dao.mybatis.BaseMapper.mysql_page_end" />
	</select>
	
	 <select id="selectModelList"  resultMap="BaseResultMap"  parameterType="java.util.Map">
		<include refid="org.zfes.snowy.base.dao.mybatis.BaseMapper.mysql_page_start" />
		SELECT
		<include refid="Alias_Base_Column_List"><property name="alias" value="zfest" /></include>
		FROM ${model.tableName} zfest WHERE 1=1
		<include refid="selectList_where"><property name="alias" value="zfest" /></include>
		<if test="sort != null">ORDER BY zfest.${r'${sort}'} ${r'${order}'}</if>
		<include refid="org.zfes.snowy.base.dao.mybatis.BaseMapper.mysql_page_end" />
	</select>
	
	
	<select id="selectListCount" resultType="java.lang.Long" parameterType="java.util.Map">
		select count(1) from ${model.tableName} zfest WHERE 1=1
		<include refid="selectList_where"><property name="alias" value="zfest" /></include>
	</select>
	
	<sql id="selectList_where">
			<#list model.modelRalList as propertyx>
			<#if (propertyx.isQueryField?? && propertyx.isQueryField ==true)>
		<if test="${propertyx.property} != null" > 
		and zfest.${propertyx.column} ${propertyx.queryLogic} ${r'#'}{${propertyx.property},jdbcType=${propertyx.jdbcType}}
		</if>
			</#if>
			</#list>
	</sql>
</mapper>