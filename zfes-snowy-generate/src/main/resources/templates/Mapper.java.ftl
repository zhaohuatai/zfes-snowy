package ${model.mapperPackageName};
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import ${model.modelPackageName}.${model.modelSimpleClassName};
import org.zfes.snowy.core.data.Record;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ${model.mapperSimpleClassName} extends BaseMapper<${model.modelSimpleClassName}>{
	
	Long selectListCount(ParamMap params);
	
	List<Record> selectMapList(ParamMap params);
	
	List<${model.modelSimpleClassName}> selectModelList(ParamMap params);
	
	
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	${model.modelSimpleClassName} selectBy${propertyx.propertyName ? cap_first}(${propertyx.propertyType} ${propertyx.propertyName});
	
	</#if></#if>
	</#list>
	
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	Long selectIdBy${propertyx.propertyName ? cap_first}(${propertyx.propertyType} ${propertyx.propertyName});
	
	</#if></#if>
	</#list>
	int updateStatus(@Param("id")Long id, @Param("status")Byte status);
}
