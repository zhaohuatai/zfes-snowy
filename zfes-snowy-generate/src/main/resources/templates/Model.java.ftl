package ${model.packageName};
import org.zfes.snowy.base.dao.IBaseModel;
public class ${model.entitySimpleClassName} extends IBaseModel{

	private static final long serialVersionUID = 1L;
	
	public ${model.entitySimpleClassName}() {}
	public ${model.entitySimpleClassName}(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.nullable?exists && propertyx.nullable !=true)> @javax.validation.constraints.NotNull(message="${propertyx.remarks}不能为空")</#if>
	<#if ((propertyx.length?exists && propertyx.length >0) && propertyx.propertyType =='java.lang.String')> @org.hibernate.validator.constraints.Length(min=0,max=${propertyx.length?c},message="${propertyx.remarks}长度不能大于${propertyx.length}")</#if>
	<#if (propertyx.scale?exists && propertyx.scale >0)> @javax.validation.constraints.Digits(integer=${propertyx.length-propertyx.scale},fraction=${propertyx.scale},message="${propertyx.remarks}数据精度错误")</#if> <#rt>
 private ${propertyx.propertyType} ${propertyx.propertyName};</#if>
	</#list>
	
	//<-------------------------------------------->
	<#list model.propertyModelList as property>
	<#if (property.propertyName !='aid'&&property.propertyName !='id')>
	public ${model.entitySimpleClassName} set${property.propertyName ? cap_first}(${property.propertyType} ${property.propertyName}){
	  <#if (property.propertyType=='java.lang.String')>
		this.${property.propertyName}=${property.propertyName}==null?null:${property.propertyName}.trim();
	 <#else>
	 this.${property.propertyName}=${property.propertyName};
	 </#if>
	 return this;
	}
	public ${property.propertyType} get${property.propertyName ? cap_first}(){
		return this.${property.propertyName};
	}
	</#if>
	</#list>

}
