package ${model.packageName};

import org.springframework.stereotype.Repository;
import org.zht.framees.dao.hibernate.dac.impl.HibernateBaseDaoImpl;
import ${model.interfacePackageName}.I${model.entitySimpleClassName}Dao;

@Repository
public class ${model.entitySimpleClassName}DaoImpl extends HibernateBaseDaoImpl implements I${model.entitySimpleClassName}Dao {
 	
 
}