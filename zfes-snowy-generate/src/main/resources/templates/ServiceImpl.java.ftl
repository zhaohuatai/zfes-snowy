package ${model.serviceImplPackageName};

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
//import org.zfes.snowy.base.service.BaseService;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.core.util.ZDateUtil;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;
import ${model.serviceInterfacePackageName}.I${model.entitySimpleClassName}Service;
import ${model.mapperPackageName}.${model.entitySimpleClassName}Mapper;
import ${model.entityFullClassName};
import java.util.stream.Stream;
//extends BaseService 
@Service
public class ${model.entitySimpleClassName}ServiceImpl implements I${model.entitySimpleClassName}Service{
 	@Autowired
	private ${model.entitySimpleClassName}Mapper ${model.entitySimpleClassName ? uncap_first}Mapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void create${model.entitySimpleClassName}(${model.entitySimpleClassName} ${model.entitySimpleClassName ? uncap_first}) {
		ZBeanUtil.validateBean(${model.entitySimpleClassName ? uncap_first});
		<#list model.propertyModelList as propertyx>
		<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
		<#if (propertyx.unique?? && propertyx.unique ==true)>
		
		Object id${propertyx_index}=${model.entitySimpleClassName ? uncap_first}Mapper.selectIdBy${propertyx.propertyName ? cap_first}(${model.entitySimpleClassName ? uncap_first}.get${propertyx.propertyName  ? cap_first}());
		if(id${propertyx_index}!=null){
			ZAlert.Error("${propertyx.remarks}已经存在");
		}
		</#if></#if>
	</#list>
		${model.entitySimpleClassName ? uncap_first}.setId(IDGenerator.genLongId());
		${model.entitySimpleClassName ? uncap_first}.setStatus(Byte.valueOf("1"));
		${model.entitySimpleClassName ? uncap_first}.setCreateTime(ZDateUtil.newDate());
		${model.entitySimpleClassName ? uncap_first}.setUpdateTime(ZDateUtil.newDate());
		${model.entitySimpleClassName ? uncap_first}Mapper.insertSelective(${model.entitySimpleClassName ? uncap_first});
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<${model.entitySimpleClassName}> load${model.entitySimpleClassName}ById(Long id) {
		if(id!=null){
			return Optional.ofNullable(${model.entitySimpleClassName ? uncap_first}Mapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void update${model.entitySimpleClassName}(${model.entitySimpleClassName} ${model.entitySimpleClassName ? uncap_first}) {
		ZBeanUtil.validateBean(${model.entitySimpleClassName ? uncap_first});
		<#list model.propertyModelList as propertyx>
		<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
		<#if (propertyx.unique?? && propertyx.unique ==true)>
		Object id${propertyx_index}=${model.entitySimpleClassName ? uncap_first}Mapper.selectIdBy${propertyx.propertyName ? cap_first}(${model.entitySimpleClassName ? uncap_first}.get${propertyx.propertyName ? cap_first}());
		if(id${propertyx_index}!=null&&!id${propertyx_index}.equals(${model.entitySimpleClassName ? uncap_first}.getId())){
			ZAlert.Error("${propertyx.remarks}已经存在");
		}
		</#if></#if>
	</#list>
		${model.entitySimpleClassName ? uncap_first}.setUpdateTime(ZDateUtil.newDate());
		${model.entitySimpleClassName ? uncap_first}Mapper.updateByPrimaryKeySelective(${model.entitySimpleClassName ? uncap_first});
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void delete${model.entitySimpleClassName}(Long id) {
			ZAssert.notNull(id, "请选择要操作的数据");
			${model.entitySimpleClassName ? uncap_first}Mapper.deleteByPrimaryKey(id);
			return;
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void set${model.entitySimpleClassName}Status(Long id, Byte status) {
		ZAssert.notNull(id, "请选择要操作的数据");
		ZAssert.notNull(status, "状态参数错误");
		if(!Stream.of(new Byte[] {0,1,2}).anyMatch(val->val.equals(status))) {
			ZAlert.serviceLogicalError("状态参数错误");
		}
		${model.entitySimpleClassName ? uncap_first}Mapper.updateStatus( id,  status) ;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet load${model.entitySimpleClassName}DataSet(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		<#list model.propertyModelList as propertyx>
			<#if (propertyx.isQueryField?? && propertyx.isQueryField ==true)>
			<#if (propertyx.queryLogic=='like')>
		pm.getObj("${propertyx.propertyName}").ifPresent(v->pm.updateParam("${propertyx.propertyName}", "%"+v+"%"));
			</#if>
			</#if>
		</#list>
		DataSet ds=DataSet.newDS2(${model.entitySimpleClassName ? uncap_first}Mapper.selectListCount(pm), ${model.entitySimpleClassName ? uncap_first}Mapper.selectMapList(pm));
		return ds;
	}
	
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<${model.entitySimpleClassName}> loadBy${propertyx.propertyName ? cap_first}(${propertyx.propertyType} ${propertyx.propertyName}){
		if(${propertyx.propertyName}!=null){
			return Optional.ofNullable(${model.entitySimpleClassName ? uncap_first}Mapper.selectBy${propertyx.propertyName ? cap_first}(${propertyx.propertyName}));
		}
		return Optional.empty();
	}
	</#if></#if>
	</#list>
}