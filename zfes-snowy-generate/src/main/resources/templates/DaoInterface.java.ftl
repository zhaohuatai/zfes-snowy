package ${model.packageName};
import org.zht.framework.dao.hibernate.dac.IHibernateBaseDao;

public interface I${model.entitySimpleClassName}Dao extends IHibernateBaseDao{
	
}