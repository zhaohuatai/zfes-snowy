package ${model.serviceInterfacePackageName};

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import ${model.entityFullClassName};
public interface I${model.entitySimpleClassName}Service{

	 Optional<${model.entitySimpleClassName}> load${model.entitySimpleClassName}ById(Long id);
	
	 void create${model.entitySimpleClassName}(${model.entitySimpleClassName} ${model.entitySimpleClassName ?uncap_first});
	
	 void update${model.entitySimpleClassName}(${model.entitySimpleClassName} ${model.entitySimpleClassName ?uncap_first});

	 void delete${model.entitySimpleClassName}(Long id);
	
	 DataSet load${model.entitySimpleClassName}DataSet(Map<String, Object> params);
	 
	<#list model.propertyModelList as propertyx>
	<#if (propertyx.propertyName !='aid'&&propertyx.propertyName !='id')>
	<#if (propertyx.unique?? && propertyx.unique ==true)>
	Optional<${model.entitySimpleClassName}> loadBy${propertyx.propertyName ? cap_first}(${propertyx.propertyType} ${propertyx.propertyName});
	
	</#if></#if>
	</#list>

 	void set${model.entitySimpleClassName}Status(Long id, Byte status);
}