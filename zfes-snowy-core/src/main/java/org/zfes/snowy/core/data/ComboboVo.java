/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.data;

import java.io.Serializable;

public class ComboboVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ComboboVo() {
		
	}
	public static ComboboVo newEmptyFirst() {
		return new ComboboVo("","请选择数据...");
	}
	public ComboboVo(Object value, String text) {
		super();
		this.value = value;
		this.text = text;
	}
	private Object value;
	private String text;
	private Boolean selected;
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	
	


	
}
