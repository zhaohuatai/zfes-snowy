/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.util.encypt;

import java.util.Map;

import org.zfes.snowy.core.util.UUIDUtil;


public class RSATest {
	 static String publicKey;
	 static String privateKey;

	    static {
	        try {
	            Map<String, Object> keyMap = RSAUtil.genKeyPair();
	            publicKey = RSAUtil.getPublicKey(keyMap);
	            privateKey = RSAUtil.getPrivateKey(keyMap);
	            System.out.println("公钥:" + publicKey);
	            System.out.println("私钥:" + privateKey);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    static void testSignx() throws Exception {
	    	String ccc=UUIDUtil.base58Uuid();
	    	System.out.println("数据:\r" + ccc);
	        String sign = RSAUtil.sign(ccc.getBytes(), privateKey);
	        System.out.println("签名:\r" + sign);
	        boolean status = RSAUtil.verify(ccc.getBytes(), publicKey, sign);
	        System.out.println("验证结果:\r" + status);
	    }
	    public static void main(String[] args) throws Exception {
	    	 // test();
//	    	testFile();
//	    	testSign();
	    	testSignx();
	    }
	    static void testFile() throws Exception {
	        System.out.println("私钥加密——公钥解密");
	        String source = "这是一行测试RSA数字签名的无意义文字";
	        System.out.println("原文字：\r\n" + source);
	        byte[] data = source.getBytes();
	        byte[] encodedData = RSAUtil.encryptByPrivateKey(data, privateKey);
	        System.out.println("加密后：\r\n" + new String(encodedData));
	        
	        EncyptFileUtil.saveKey(privateKey, "D:/key/","rsa_private.key");
	        EncyptFileUtil.saveKey(publicKey,"D:/key/", "rsa_public.key");
	        
	        EncyptFileUtil.saveData(encodedData, "D:/key/","public_encryt.dat");
	        
	        publicKey=EncyptFileUtil.readKey("D:/key/","rsa_public.key");
	        
	        byte[] datax= EncyptFileUtil.readData("D:/key/","public_encryt.dat");
	        byte[] decodedData = RSAUtil.decryptByPublicKey(datax, publicKey);
	        
	        String target = new String(decodedData);
	        System.out.println("解密后: \r\n" + target);
	        System.out.println("私钥签名——公钥验证签名");
	        String sign = RSAUtil.sign(encodedData, privateKey);
	        System.out.println("签名:\r" + sign);
	        boolean status = RSAUtil.verify(encodedData, publicKey, sign);
	        System.out.println("验证结果:\r" + status);
	    }

	    static void test() throws Exception {
	        System.out.println("公钥加密——私钥解密");
	        String source = "这是一行没有任何意义的文字，你看完了等于没看，不是吗？";
	        System.out.println("\r加密前文字：\r\n" + source);
	        byte[] data = source.getBytes();
	        byte[] encodedData = RSAUtil.encryptByPublicKey(data, publicKey);
	        System.out.println("加密后文字：\r\n" + new String(encodedData));
	        byte[] decodedData = RSAUtil.decryptByPrivateKey(encodedData, privateKey);
	        String target = new String(decodedData);
	        System.out.println("解密后文字: \r\n" + target);
	    }

	    static void testSign() throws Exception {
	        System.out.println("私钥加密——公钥解密");
	        String source = "这是一行测试RSA数字签名的无意义文字";
	        System.out.println("原文字：\r\n" + source);
	        byte[] data = source.getBytes();
	        byte[] encodedData = RSAUtil.encryptByPrivateKey(data, privateKey);
	        System.out.println("加密后：\r\n" + new String(encodedData));
	        byte[] decodedData = RSAUtil.decryptByPublicKey(encodedData, publicKey);
	        String target = new String(decodedData);
	        System.out.println("解密后: \r\n" + target);
	        System.out.println("私钥签名——公钥验证签名");
	        String sign = RSAUtil.sign(encodedData, privateKey);
	        System.out.println("签名:\r" + sign);
	        boolean status = RSAUtil.verify(encodedData, publicKey, sign);
	        System.out.println("验证结果:\r" + status);
	    }
	    
}
