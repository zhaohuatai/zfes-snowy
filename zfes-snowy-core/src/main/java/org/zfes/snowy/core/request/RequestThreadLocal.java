/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.request;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestThreadLocal {
	
	protected final Logger log = LoggerFactory.getLogger(RequestThreadLocal.class);
	
	private static final ThreadLocal<ServletRequest> contextHolder = new ThreadLocal<ServletRequest>();

	public static void setServletRequest(ServletRequest currentReqestInfo) {
		contextHolder.set(currentReqestInfo);
	}

	public static ServletRequest getServletRequest() {
		return contextHolder.get();
	}

	public static void clearCurrentReqestInfo() {
		contextHolder.remove();
	}
}
