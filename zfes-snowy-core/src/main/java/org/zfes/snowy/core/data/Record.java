package org.zfes.snowy.core.data;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.zfes.snowy.core.util.ZStrUtil;

import com.alibaba.fastjson.JSON;
public class Record extends HashMap<String, Object>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2571468553860222373L;
	
	
	public String toJson() {
		return JSON.toJSONString(getColumns());
	}
	
	public Record getColumns() {
		return this;
	}
	public Record setColumns(Map<String, Object> columns) {
		this.getColumns().putAll(columns);
		return this;
	}	
	
	public Record setColumns(Record record) {
		getColumns().putAll(record.getColumns());
		return this;
	}
	public Record remove(String column) {
		getColumns().remove(column);
		return this;
	}
	public Record remove(String... columns) {
		if (columns != null)
			for (String c : columns)
				this.getColumns().remove(c);
		return this;
	}
	public Record clearz() {
		getColumns().clear();
		return this;
	}
	

	public Record set(String column, Object value) {
		getColumns().put(column, value);
		return this;
	}

	
	
	public Optional<String> getOpStr(String column) {
		Object res=getColumns().get(column);
		if(res==null||ZStrUtil.hasNoText((String)res)){
			return Optional.empty();
		}
		return Optional.ofNullable((String)res);
	}
	
	public String getStr(String column) {
		return (String)getColumns().get(column);
	}
	
	public Optional<Integer> getOpInt(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Integer)res);
	}
	
	public Integer getInt(String column) {
		return (Integer)getColumns().get(column);
	}
	
	public Optional<Long> getOpLong(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Long)res);
	}
	public Long getLong(String column) {
		return (Long)getColumns().get(column);
	}
	
	public Optional<BigInteger> getOpBigInteger(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((BigInteger)res);
	}
	public java.math.BigInteger getBigInteger(String column) {
		return (java.math.BigInteger)getColumns().get(column);
	}
	
	public Optional<Date> getOpDate(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Date)res);
	}
	public java.util.Date getDate(String column) {
		return (java.util.Date)getColumns().get(column);
	}
	
	public Optional<java.sql.Time> getOpTime(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((java.sql.Time)res);
	}
	public java.sql.Time getTime(String column) {
		return (java.sql.Time)getColumns().get(column);
	}
	
	public Optional<java.sql.Timestamp> getOpTimestamp(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((java.sql.Timestamp)res);
	}
	public java.sql.Timestamp getTimestamp(String column) {
		return (java.sql.Timestamp)getColumns().get(column);
	}
	
	public Optional<Double> getOpDouble(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Double)res);
	}
	public Double getDouble(String column) {
		return (Double)getColumns().get(column);
	}
	public Optional<Float> getOpFloat(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Float)res);
	}
	public Float getFloat(String column) {
		return (Float)getColumns().get(column);
	}
	
	public Optional<Boolean> getOpBoolean(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Boolean)res);
	}
	public Boolean getBoolean(String column) {
		return (Boolean)getColumns().get(column);
	}
	
	public Optional<java.math.BigDecimal> getOpBigDecimal(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((java.math.BigDecimal)res);
	}
	public java.math.BigDecimal getBigDecimal(String column) {
		return (java.math.BigDecimal)getColumns().get(column);
	}
	
	public Optional<byte[]> getOpBytes(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((byte[])res);
	}
	public byte[] getBytes(String column) {
		return (byte[])getColumns().get(column);
	}
	
	public Optional<Number> getOpNumber(String column) {
		Object res=getColumns().get(column);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Number)res);
	}
	public Number getNumber(String column) {
		return (Number)getColumns().get(column);
	}
	

	public String[] getColumnNames() {
		Set<String> attrNameSet = getColumns().keySet();
		return attrNameSet.toArray(new String[attrNameSet.size()]);
	}
	
	
	public Object[] getColumnValues() {
		java.util.Collection<Object> attrValueCollection = getColumns().values();
		return attrValueCollection.toArray(new Object[attrValueCollection.size()]);
	}
	
}
