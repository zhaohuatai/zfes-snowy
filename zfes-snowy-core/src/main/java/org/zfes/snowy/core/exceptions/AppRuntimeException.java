/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.exceptions;

public class AppRuntimeException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4823772842378630950L;
	public AppRuntimeException() {
        super();
    } 
    public AppRuntimeException(String s) {
        super(s);
    }
    public AppRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
    public AppRuntimeException(Throwable cause) {
        super(cause);
    } 
}
