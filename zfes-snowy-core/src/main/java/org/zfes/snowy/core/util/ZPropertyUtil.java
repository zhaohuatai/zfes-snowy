/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Deprecated
public class ZPropertyUtil {
	/***
	* 获得一个类的方法的值
	* @param invoke 执行对象
	* @param methodName 方法名称
	* @return
	*/
	@Deprecated
	public static Object getInvokeValue(Object entity, String methodName) {
		Object obj = null;
		Method m = null;
		for (Class<?> clazz = entity.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
				try {
					m = clazz.getDeclaredMethod(methodName);
				} catch (Exception e) {
					//e.printStackTrace();
				}
		}
		if (m != null) {
			try {
				obj = m.invoke(entity);
			} catch (IllegalArgumentException e) {
				return -972855736L;
			} catch (IllegalAccessException e) {
				return -972855736L;
			} catch (InvocationTargetException e) {
				return -972855736L;
			} catch (Exception e) {
				return -972855736L;
			}
		}
		return obj;
	}
	

	public static void main(String[] args){}
}
