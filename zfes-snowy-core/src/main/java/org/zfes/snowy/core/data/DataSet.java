/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class DataSet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long total;
	private List<Map<String,Object>> rows=new ArrayList<Map<String,Object>>();
	private List<Map<String,Object>> footer=new ArrayList<Map<String,Object>>();//合计列
	private String simpleFooter;//合计列
	
//	public DataSet() {
//	}
	public DataSet( List<Map<String,Object>>  rows) {
		this.total=(rows==null?0L:rows.size());
		this.rows = rows;
	}
	public DataSet(Long total, List<Map<String,Object>>  rows) {
		this.total = total;
		this.rows = rows;
	}
	@SuppressWarnings({"unchecked","rawtypes"})
	public DataSet( List<Record> records, Long total) {
		this.total = total;
		this.rows = (List<Map<String,Object>>)(List)records;
	}
	
	
	public static DataSet newDS(Long total, List<Map<String,Object>>  rows) {
		return new DataSet( total,  rows);
	}
	public static DataSet newDS2( Long total,List<Record> records ) {
		return new DataSet( records,  total);
	}
	
	public static DataSet emptyDS() {
		return new DataSet( 0L, new ArrayList<Map<String,Object>>());
	}
	
	public DataSet(Long total, List<Map<String,Object>>  rows,List<Map<String,Object>>  footer) {
		this.total = total;
		this.rows = rows;
		this.footer = footer;
	}
	
	
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public List<Map<String,Object>>  getRows() {
		return rows;
	}
	public void setRows(List<Map<String,Object>>  rows) {
		this.rows = rows;
	}
	public List<Map<String,Object>>  getFooter() {
		return footer;
	}
	public void setFooter(List<Map<String,Object>>  footer) {
		this.footer = footer;
	}

	public String getSimpleFooter() {
		return simpleFooter;
	}

	public void setSimpleFooter(String simpleFooter) {
		this.simpleFooter = simpleFooter;
	}
	


}
