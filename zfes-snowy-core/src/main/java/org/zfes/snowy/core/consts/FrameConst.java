/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.consts;

public class FrameConst {
	public static final class TOKENS{
		public static final String CSRF_TOKEN="snowy_csrf_token";
		public static final String JWT_AUTH_TOKEN="snowy_jwt_token";
	}
	
//---------------交互状态码-------------------------
	public static final class SC{
		public static final int SC_200 = 200;// Success
		public static final int SC_300 = 300;// Service_Error
		public static final int SC_301 = 301;// time_out--->dologin
		public static final int SC_303 = 303;// 重复提交
		public static final int SC_401 = 401;// Auth_Error
		public static final int SC_500 = 500;// Server_Error
		public static final int SC_402 = 402;// csrf token is invalid
		public static final int SC_4010 = 4010;//
		public static final int SC_4011 = 4011;//
	}
	public static final class WEB_MSG{
		public static final String AJAX_SUCCESS="数据处理成功 ";
		public static final String AJAX_ERROR="数据处理失败  ";
		public static final String VALIDATE_ERROR="数据校验失败 ";
		public static final String AUTH_ERROR= "权限授权认证失败 ";
		public static final String AUTH_UNREGISTER_ERROR= "未注册用户";
		public static final String AUTH_NONCHECKED_ERROR= "未审核用户";
		public static final String RUNTOME_ERROR= "服务器错误 ";
		public static final String QUERY_FAILURE= "未查询到数据";
		public static final String INVALID_SESSIONUSER= "用户未登录或超时";
	}

	public static final class ERROR_PAGE{
		public static final String ERROR_PAGE_301="/static/pages/301.html";
		public static final String ERROR_PAGE_401="/static/pages/401.html";
		public static final String ERROR_PAGE_300="/static/pages/300.html";
		public static final String ERROR_PAGE_500="/static/pages/500.html";
		public static final String ERROR_PAGE_402="/static/pages/402.html";
	}
	
	public static final class PAGE_OFFSET{
		public static final String DESC= " desc ";
		public static final String ASC= " asc ";
		public static final String PAGE = "page";
		public static final String ROWS = "rows";
		public static final String SORT = "sort";
		public static final String ORDER = "order";
		public static final String START = "start";
		public static final String END = "end";
		public static final String SIZE = "size";
		
		public static final long MAX_ROWS = 2000;
		public static final long DEFAULT_ROWS = 20;
		public static final long DEFAULT_PAGE = 1;
	}
	public static final String QUERYTYPE_KEY="zfes_snowy_query_type";
	
	public static final String ZFES_HEADER_MSG="zfes_header_msg";
	
	public static final class DB_TABLE{
		public static final String TREE_TABLE_ROOTNODE_PARENT_FK  = "-1";//
	}
	
	public static final String ZFES_SSO_CENTER_APPKEY="ZFES_SSO_CENTER";
	
}
