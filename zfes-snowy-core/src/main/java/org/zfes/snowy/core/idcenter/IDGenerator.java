/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.idcenter;

import org.zfes.snowy.core.util.UUIDUtil;
/**
 * #主键生成策略，snowflake方案a： 机器id 0-1023<br><br>
 * 
 * #主键生成策略，snowflake方案B：datacenter id 0-32;workerid:0-32 组合之后0-1023<br><br>
 * 
 * #主键生成策略，mysql 自增-genLongId()<br><br>
 */
public class IDGenerator {
	
	//赵华太  ---  2338 
	public static String genId(IdStrategy idStrategy){
		switch (idStrategy) {
		case uuid:
			return UUIDUtil.uuid();
		case uuidBase58:
			return UUIDUtil.base58Uuid();
		case uuidBase64:
			return UUIDUtil.base64Uuid();
		default:
			return UUIDUtil.uuid();
		}
	}
	
	/**4096000
	 * datacenterId:1<br>
	 * workerId：1<br>
	 * 1毫秒內不能超过4096次：--->1ms : 4096=> 1s:4096000： 400w<br>
	 * @return
	 */
	public static long genClusterSnowflakeA(long idGenSnowflakeAWorkerid){
		SnowflakeIDWorkerA idWorker=SnowflakeIDWorkerA.getIdWorkerA(idGenSnowflakeAWorkerid);
		return idWorker.getId();
	}
	public static long genClusterSnowflakeB(long idGenSnowflakeBDatacenter,long  idGenSnowflakeBWorkerid){
		SnowflakeIDWorkerB idWorker=SnowflakeIDWorkerB.getIdWorker(idGenSnowflakeBDatacenter, idGenSnowflakeBWorkerid);
		return idWorker.getId();
	}

	public static Long genLongId() {
		return null;
	}
	 
}
