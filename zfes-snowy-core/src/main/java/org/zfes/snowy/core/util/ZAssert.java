/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.CollectionUtils;

public class ZAssert {
	
	public static void equals(Object obj1,Object obj2, String message) {
		if (!Objects.equals(obj1, obj2))
			ZAlert.Error(message);
	}
	public static void notEquals(Object obj1,Object obj2, String message) {
		if (Objects.equals(obj1, obj2))
			ZAlert.Error(message);
	}
	public static void deepEquals(Object obj1,Object obj2, String message) {
		if (!Objects.deepEquals(obj1, obj2))
			ZAlert.Error(message);
	}
	
	
	/**
	 * 如内容不是true 则抛出ServiceLogicalException
	 * @param expression ：待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void isTrue(boolean expression, String message) {
		if (!expression)
			ZAlert.Error(message);
	}
	
	/**
	 * 如内容不是False 则抛出ServiceLogicalException
	 * @param expression ：待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void isFalse(boolean expression, String message) {
		if (expression)
			ZAlert.Error(message);
	}
	
	/**
	 * 判断内容是否 null ，如果不是null 则抛出ServiceLogicalException
	 * 
	 * @param object  ：待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void isNull(Object object, String message) {
		if (object != null)
			ZAlert.Error(message);
	}

	
	/**
	 * 判断内容是否  不是 null ，如果是null 则抛出ServiceLogicalException
	 * 
	 * @param object  ：待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void notNull(Object object, String message) {
		if (object == null)
			ZAlert.Error(message);
	}
	public static void notEmpty(Optional<?> op, String message) {
		if (op == null||!op.isPresent())
			ZAlert.Error(message);
	}

	public static void notEmpty(Object[] array, String message) {
		if (ZObjectUtil.isEmpty(array))
			ZAlert.Error(message);
	}
	
	/**
	 * 
	 * 判断字符串是否  不是 null 而且不为空， 如果是null或空字符  ，则抛出ServiceLogicalException<br/> <br/> 
	 * 
	 * (str != null) && (str.length() > 0);
	 * @param text ：待判断的内容
	 * @param message  : 抛出异常信息
	 */
	public static void hasLength(String text, String message) {
		if (!ZStrUtil.hasLength(text))
			ZAlert.Error(message);
	}

	/**
	 * 
	 * 判断字符串是否  不是 null 、不为空、且不是Whitespace ，<br/> <br/> 
	 *  如果是null 或 空字符 或者全是 Whitespace (blank)  ，则抛出ServiceLogicalException  (blank) <br/> <br/> 
	 * 
	 * @param text  : 待判断的内容
	 * @param message  : 抛出异常信息
	 */
	public static void hasText(String text, String message) {
		if (!ZStrUtil.hasText(text))
			ZAlert.Error(message);
	}
	
	public static void hasText(Object text, String message) {
		if(text==null){
			ZAlert.Error(message);
		}
		if (!ZStrUtil.hasText(String.valueOf(text))){
			ZAlert.Error(message);
		}
			
	}
	
	public static void bigOrEqThanZero(Float var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigOrEqThanZero(var.floatValue(),  message,  formatErrorMessage);
	}
	public static void bigThanZero(Float var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigThanZero(var.floatValue(),  message,  formatErrorMessage);
	}
	public static void bigOrEqThanZero(Double var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigOrEqThanZero(var.doubleValue(),  message,  formatErrorMessage);
	}
	public static void bigThanZero(Double var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigOrEqThanZero(var.doubleValue(),  message,  formatErrorMessage);
	}
	public static void bigThanZero(double var, String message, String formatErrorMessage) {
		if(var<0){
			ZAlert.Error(message);
		}
	}
	public static void bigOrEqThanZero(double var, String message, String formatErrorMessage) {
		if(var<0){
			ZAlert.Error(message);
		}
	}
	public static void bigOrEqThanZero(Integer var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigOrEqThanZero(var.intValue(),  message,  formatErrorMessage);
	}

	public static void bigThanZero(Integer var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigThanZero(var.intValue(),  message,  formatErrorMessage);
	}
	public static void bigOrEqThanZero(Long var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigOrEqThanZero(var.longValue(),  message,  formatErrorMessage);
	}
	public static void bigThanZero(Long var, String message, String formatErrorMessage) {
		if(var==null){
			ZAlert.Error(message);
		}
		bigThanZero(var.longValue(),  message,  formatErrorMessage);
	}
	public static void bigOrEqThanZero(long var, String message, String formatErrorMessage) {
		if(var<0){
			ZAlert.Error(message);
		}
	}
	public static void bigThanZero(long var, String message, String formatErrorMessage) {
		if(var<=0){
			ZAlert.Error(message);
		}
	}
	public static void bigOrEqThanZero(Object text, String message, String formatErrorMessage) {
		if(text==null){
			ZAlert.Error(formatErrorMessage);
		}
		if(!NumberUtils.isCreatable(ZStrUtil.trim(String.valueOf(text)))){
			ZAlert.Error(formatErrorMessage);
		}
		BigDecimal num=NumberUtils.createBigDecimal(ZStrUtil.trim(String.valueOf(text)));
		if(num.compareTo(BigDecimal.ZERO)<0){
			ZAlert.Error(message);
		}
	}
	
	public static void bigThanZero(Object text, String message, String formatErrorMessage) {
		if(text==null){
			ZAlert.Error(formatErrorMessage);
		}
		if(!NumberUtils.isCreatable(ZStrUtil.trim(String.valueOf(text)))){
			ZAlert.Error(formatErrorMessage);
		}
		BigDecimal num=NumberUtils.createBigDecimal(ZStrUtil.trim(String.valueOf(text)));
		if(num.compareTo(BigDecimal.ZERO)<=0){
			ZAlert.Error(message);
		}
	}
	
	public static void main(String[] asd){
		Double xx=new Double(-5.3);
		ZAssert.bigThanZero(xx, "缴费金额错误", "缴费金额错误");
	}

	/**
	 * textToSearch 不能包含substring， 如果包含则抛出ServiceLogicalException
	 * @param textToSearch  : 待判断的内容
	 * @param substring  : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void notContain(String textToSearch, String substring,String message) {
		if ((ZStrUtil.hasLength(textToSearch))
				&& (ZStrUtil.hasLength(substring))
				&& (textToSearch.contains(substring))) {
			ZAlert.Error(message);
		}
	}

	

	/**
	 * 数组array 必须不能为空，必须至少包含一个元素；否则抛出 ServiceLogicalException
	 * @param array : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void notEmptyArray(Object[] array, String message) {
		if (ZObjectUtil.isEmpty(array))
			ZAlert.Error(message);
	}


	/**
	 * 数组array 必须为null 或者 空，否则抛出 ServiceLogicalException
	 * @param array : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void emptyArray(Object[] array, String message) {
		if (!ZObjectUtil.isEmpty(array))
			ZAlert.Error(message);
	}
	
	/**
	 * 数组array 必须不能为空，且不能包含null 元素 ；否则抛出 ServiceLogicalException
	 * @param array : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void noNullElements(Object[] array, String message) {
		if (array != null)
			for (Object element : array)
				if (element == null)
					ZAlert.Error(message);
	}

	/**
	 * 集合collection必须不能为空，必须至少包含一个元素；否则抛出 ServiceLogicalException
	 * @param collection  : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void notEmpty(Collection<?> collection, String message) {
		if (CollectionUtils.isEmpty(collection))
			ZAlert.Error(message);
	}

	
	/**
	 * 集合collection必须null或者空；否则抛出 ServiceLogicalException
	 * @param collection  : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void empty(Collection<?> collection, String message) {
		if (!CollectionUtils.isEmpty(collection))
			ZAlert.Error(message);
	}
	
	
	/**
	 * map必须不能为空，必须至少包含一个元素；否则抛出 ServiceLogicalException
	 * @param collection  : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void notEmpty(Map<?, ?> map, String message) {
		if (CollectionUtils.isEmpty(map))
			ZAlert.Error(message);
	}
	
	/**
	 * Map必须为null 或者 空；否则抛出 ServiceLogicalException
	 * @param collection  : 待判断的内容
	 * @param message : 抛出异常信息
	 */
	public static void empty(Map<?, ?> map, String message) {
		if (!CollectionUtils.isEmpty(map))
			ZAlert.Error(message);
	}

	/**
	 * 判断个对象实例是否是一个类或接口的或其子类子接口的实例，否则抛出 ServiceLogicalException
	 * @param type :  待判断的类型
	 * @param obj :  待判断的对象
	 * @param message : 抛出异常信息
	 */
	public static void isInstanceOf(Class<?> type, Object obj, String message) {
		notNull(type, "待判断的类型参数不能为空");
		if (!type.isInstance(obj)) {
			ZAlert.Error(
					new StringBuilder()
							.append(ZStrUtil.hasLength(message) ?new StringBuilder().append(message).append(" ").toString(): "")
							.append("Object of class [")
							.append(obj != null ? obj.getClass().getName(): "null")
							.append("] must be an instance of ").append(type).toString());
		}
	}


	/**
	 * 判断一个类Class1和另一个类Class2是否相同或是另一个类的子类或接口，否则抛出 ServiceLogicalException
	 * @param superType :  待判断的上级类型
	 * @param subType :  待判断的子类型
	 * @param message : 抛出异常信息
	 */
	public static void isAssignable(Class<?> superType, Class<?> subType,String message) {
		
		notNull(superType, "待判断的类型参数不能为空");
		
		if ((subType == null) || (!superType.isAssignableFrom(subType)))
			ZAlert.Error(new StringBuilder()
					.append(message).append(subType)
					.append(" is not assignable to ").append(superType).toString());
	}

	public static void hasValidLength(String str, int min, int max,String message) {
		if(str==null){
			ZAlert.Error(message);
		}
		if(str.length()<min||str.length()>max){
			ZAlert.Error(message);
		}
	}

	public static void isNumber(String str, String message) {
		if(str==null){
			ZAlert.Error(message);
		}
		if(!NumberUtils.isDigits(str)){
			ZAlert.Error(message);
		}
	}
	//---------------------------------\
	public static void isUrl(CharSequence value, String message) {
	
	}
	
}
