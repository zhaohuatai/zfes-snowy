/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.util;

import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.exceptions.ServiceLogicalException;

public class ZAlert {
	
	public static void serviceLogicalError(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException(message);
	}
	
	public static void appRuntimeError(String message) throws ServiceLogicalException {
		throw new AppRuntimeException(message);
	}
	
	
	
	public static void Error(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException(message);
	}
	
	public static void isNull(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException(message);
	}

	public static void notExist(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException( message);
	}

	public static void FormatError(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException( message);
	}

	public static void SqlError(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException( message);
	}

	public static void IOError(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException(message);
	}

	public static void FileError(String message) throws ServiceLogicalException {
		throw new ServiceLogicalException(message);
	}


}
