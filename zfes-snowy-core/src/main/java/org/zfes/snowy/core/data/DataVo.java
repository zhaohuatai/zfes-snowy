package org.zfes.snowy.core.data;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZStrUtil;

import com.alibaba.fastjson.JSON;
public class DataVo extends HashMap<String, Object>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2571468553860222373L;
	
	
	public static DataVo newdv(){
		return new DataVo();
	}
	public static DataVo newdv(Record record){
		return new DataVo().addMap(record);
	}
	public DataVo add(String key,Object value){
		if(!ZStrUtil.hasText(key)){
			throw new AppRuntimeException("参数 key 不允许为空");
		}
		if(value!=null){
			this.put(key, ZStrUtil.trimToNull(value));
		}
		return this;
	}
	
	public DataVo addMap(Map<String,Object> params){
		Iterator<Map.Entry<String,Object>> entries = params.entrySet().iterator();
		while (entries.hasNext()) {
		    Map.Entry<String,Object> entry = entries.next();
		    String key=entry.getKey();
		    Object value=entry.getValue();
		    this.add( key, value);
		}
		return this;
	}
	public String toJson() {
		return JSON.toJSONString(getData());
	}
	
	public DataVo getData() {
		return this;
	}
	public DataVo remove(String key) {
		getData().remove(key);
		return this;
	}
	public DataVo clearz() {
		getData().clear();
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		return (T)getData().get(key);
	}
	
	public Optional<String> getOpStr(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((String)res);
	}
	
	public String getStr(String key) {
		return (String)getData().get(key);
	}
	public String setStr(String key) {
		return (String)getData().get(key);
	}
	public Optional<Integer> getOpInt(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Integer)res);
	}
	
	public Integer getInt(String key) {
		return (Integer)getData().get(key);
	}
	
	public Optional<Long> getOpLong(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Long)res);
	}
	public Long getLong(String key) {
		return (Long)getData().get(key);
	}
	
	public Optional<BigInteger> getOpBigInteger(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((BigInteger)res);
	}
	public java.math.BigInteger getBigInteger(String key) {
		return (java.math.BigInteger)getData().get(key);
	}
	
	public Optional<Date> getOpDate(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Date)res);
	}
	public java.util.Date getDate(String key) {
		return (java.util.Date)getData().get(key);
	}
	
	public Optional<java.sql.Time> getOpTime(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((java.sql.Time)res);
	}
	public java.sql.Time getTime(String key) {
		return (java.sql.Time)getData().get(key);
	}
	
	public Optional<java.sql.Timestamp> getOpTimestamp(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((java.sql.Timestamp)res);
	}
	public java.sql.Timestamp getTimestamp(String key) {
		return (java.sql.Timestamp)getData().get(key);
	}
	
	public Optional<Double> getOpDouble(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Double)res);
	}
	public Double getDouble(String key) {
		return (Double)getData().get(key);
	}
	public Optional<Float> getOpFloat(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Float)res);
	}
	public Float getFloat(String key) {
		return (Float)getData().get(key);
	}
	
	public Optional<Boolean> getOpBoolean(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Boolean)res);
	}
	public Boolean getBoolean(String key) {
		return (Boolean)getData().get(key);
	}
	
	public Optional<java.math.BigDecimal> getOpBigDecimal(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((java.math.BigDecimal)res);
	}
	public java.math.BigDecimal getBigDecimal(String key) {
		return (java.math.BigDecimal)getData().get(key);
	}
	
	public Optional<byte[]> getOpBytes(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((byte[])res);
	}
	public byte[] getBytes(String key) {
		return (byte[])getData().get(key);
	}
	
	public Optional<Number> getOpNumber(String key) {
		Object res=getData().get(key);
		if(res==null){
			return Optional.empty();
		}
		return Optional.ofNullable((Number)res);
	}
	public Number getNumber(String key) {
		return (Number)getData().get(key);
	}
	

	public String[] getColumnNames() {
		Set<String> attrNameSet = getData().keySet();
		return attrNameSet.toArray(new String[attrNameSet.size()]);
	}
	
	
	public Object[] getColumnValues() {
		java.util.Collection<Object> attrValueCollection = getData().values();
		return attrValueCollection.toArray(new Object[attrValueCollection.size()]);
	}
	
}
