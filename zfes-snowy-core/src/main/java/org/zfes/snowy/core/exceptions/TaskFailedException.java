/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.core.exceptions;

public class TaskFailedException  extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public TaskFailedException() {
		super();
	}

	public TaskFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public TaskFailedException(String message) {
		super(message);
	}

	public TaskFailedException(Throwable cause) {
		super(cause);
	}


}
