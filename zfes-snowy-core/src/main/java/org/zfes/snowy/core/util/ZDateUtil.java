/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.datex.ZDateStyle;
import org.zfes.snowy.core.util.datex.ZDateWeek;
/**
 * 
 * @author zhaohuatai<br/>
 *
 */
public class ZDateUtil {
	public static void main(String[] args) throws ParseException, InterruptedException {
    	
//    	System.out.println("-----------------001--------------------------------");
////    	String dd=getNowStr(ZDateStyle.YYYY_MM_DD);
//    	Date date=new Date();
//    	System.out.println("getdefaultDateStart-start: "+ toString(getDayStart(date), ZDateStyle.YYYY_MM_DD_HH_MM_SS) );
//    	System.out.println("getdefaultDateStart-end: "+ toString(getDayEnd(date), ZDateStyle.YYYY_MM_DD_HH_MM_SS));
//    	
//    	System.out.println("-------------------------------------------------");
//    	System.out.println(getIntervalMinites(parseDate("2017-06-09 22:21:02",  ZDateStyle.YYYY_MM_DD_HH_MM_SS), new Date()));
		Date a=new Date();
//		Thread.currentThread().sleep(20);
		System.out.println("-------------------------------------------------");
		Date b=new Date();
		System.out.println(isBefore(a, b));	
	}
	
	public static boolean isAfter(Date sourceDate,Date targetDate) {
		long  a=sourceDate.getTime();
		long  b=targetDate.getTime();
		System.out.println(a+"||"+b);
		return a>b;
	}
	public static boolean isBefore(Date sourceDate,Date targetDate) {
		long  a=sourceDate.getTime();
		long  b=targetDate.getTime();
		System.out.println(a+"||"+b);
		return b>a;
	}
	
	
	 public static Date newDate(){  
	    return	new Date();
	}  

	    /** 
	     * 获取当前日期 
	     * @return 
	     */  
	    public static String getNowStr(ZDateStyle dateFormat){  
	    	return	toString(new Date(), dateFormat);
	    }  
	    

		/**
		 * 获取日期。默认yyyy-MM-dd格式
		 * @param date 日期
		 * @return 日期
		 */
		public static String defaultDate(Date date) {
			return toString(date, ZDateStyle.YYYY_MM_DD);
		}


		/**
		 * 获取日期的时间。默认HH:mm:ss格式
		 * @param date 日期
		 * @return 时间
		 */
		public static String defaultTime(Date date) {
			return toString(date, ZDateStyle.HH_MM_SS);
		}
		/**
		 * 获取日期的时间。默认HH:mm:ss格式
		 * @param date 日期
		 * @return 时间
		 */
		public static String defaultDateTime(Date date) {
			return toString(date, ZDateStyle.YYYY_MM_DD_HH_MM);
		}
		/**
		 * 获取当天开始时间 
		 * @param dateStr 
		 * @return Date :YYYY_MM_DD_HH_MM_SS
		 * @throws ParseException 
		 */
		 public static Date getDayStart(Date date){
			 String dateStr=toString(date, ZDateStyle.YYYY_MM_DD);
			 dateStr=dateStr+" 00:00:00";
		     try {
		    	 return  parseDate(dateStr, ZDateStyle.YYYY_MM_DD_HH_MM_SS);
			} catch (Exception e) {
				throw new AppRuntimeException(e.getMessage());
			}
		 }
		 
		 /**
		  * 获取当当天结束时间 
		  * @param Date :YYYY_MM_DD_HH_MM_SS
		  * @return
		 * @throws ParseException 
		  */
		 public static Date getDayEnd(Date date) {
			 String dateStr=toString(date, ZDateStyle.YYYY_MM_DD);
			 dateStr=dateStr+" 23:59:59";
		     try {
		    	 return parseDate(dateStr, ZDateStyle.YYYY_MM_DD_HH_MM_SS);
				} catch (Exception e) {
					throw new AppRuntimeException(e.getMessage());
				}
		 } 
		 
	      
		
//-------------------------------String---Date--------S--------------------------------------------    
	  public static java.util.Date parseDate(String dateStr, ZDateStyle dateFormat) {
	     if (dateStr == null || dateFormat == null) {
	            throw new IllegalArgumentException("Date and Patterns must not be null");
	        }
	       SimpleDateFormat df = caseFormatter(dateFormat.getValue());
	     
	       try {
	    	   return df.parse(dateStr);
			} catch (Exception e) {
				throw new AppRuntimeException(e.getMessage());
			}
	   }
	
	  public static java.util.Date parseDate(String dateStr, Locale locale, ZDateStyle dateFormat) {
		  if (dateStr == null || dateFormat == null) {
	         throw new IllegalArgumentException("Date and Patterns must not be null");
	      }
	    SimpleDateFormat df;
	    if (locale == null) {
	    	df = caseFormatter(dateFormat.getValue());
	     } else {
	        df = caseFormatter(dateFormat.getValue(),locale);
	     }
	    	try {
	    	  return df.parse(dateStr);
			} catch (Exception e) {
				throw new AppRuntimeException(e.getMessage());
			}
	   
	 }
	 public static java.util.Date parseDate(String dateStr, String pattern)  {
		 try {
			 return DateUtils.parseDate(dateStr, pattern);
			} catch (Exception e) {
				throw new AppRuntimeException(e.getMessage());
			}
	   
	 }
	 public static java.util.Date parseDate(String dateStr, Locale locale, String pattern)  {
		 try {
			 return DateUtils.parseDate(dateStr,locale, pattern);
			} catch (Exception e) {
				throw new AppRuntimeException(e.getMessage());
			}
		    
	 }
	 
	public static String toString(java.util.Date date, ZDateStyle dateFormat) {
		 if (date == null || dateFormat == null) {
             throw new IllegalArgumentException("Date and Patterns must not be null");
         }
		 
		SimpleDateFormat sdf = caseFormatter(dateFormat.getValue());
		return sdf.format(date);
	}

	public static String toString(java.util.Date date, String pattern) {
		 if (date == null || pattern == null) {
             throw new IllegalArgumentException("Date and Patterns must not be null");
         }
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
//-------------------------------SQL---util----convert-----------------------------------  	    
	public static Date toUtilDate(java.sql.Date date) {
		 if (date == null ) {
             throw new IllegalArgumentException("Date must not be null");
         }
		java.util.Date utilDate=new java.util.Date (date.getTime());
		return utilDate;
	}
	
	public static Date toSqlDate(java.util.Date date) {
		if (date == null ) {
            throw new IllegalArgumentException("Date must not be null");
        }
		java.sql.Date sqlDate=new java.sql.Date( date.getTime());
		return sqlDate;
	}  
//------------------------------------------------------------------------------------- 
	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}
	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	public static int getDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	public static int getSecond(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.SECOND);
	}

	public static long getMillis(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getTimeInMillis();
	}
	
//------------------------------------------------------------------------------------- 
	
    /** 
     * 获取某周得第一天 
     * @param year 年 
     * @param week 周 
     * @return 
     */  
    public static Date getFirstDayOfWeek(int year, int week){  
        Calendar c = new GregorianCalendar();  
        c.set(Calendar.YEAR, year);  
        c.set(Calendar.WEEK_OF_YEAR,  week);  
        c.setFirstDayOfWeek(Calendar.MONDAY);  
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());  
        c.set(Calendar.HOUR, 0);  
        c.set(Calendar.MINUTE, 0);  
        c.set(Calendar.SECOND, 0);  
        return c.getTime();   
    }
    /** 
     * 获取某周得最后一天 
     * @param year 年 
     * @param week 周 
     * @return 
     */  
    public static Date getLastDayOfWeek(int year, int week){  
        Calendar c = new GregorianCalendar();  
          
        c.set(Calendar.YEAR, year);  
        c.set(Calendar.WEEK_OF_YEAR,  week);  
        c.setFirstDayOfWeek(Calendar.MONDAY);  
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6);  
        c.set(Calendar.HOUR, 23);  
        c.set(Calendar.MINUTE, 59);  
        c.set(Calendar.SECOND, 59);  
          
        return c.getTime();   
    }  
    /** 
     * 获取某月得第一天 
     * @param year 年 
     * @param month 月 
     * @return 
     */  
    public static Date getFirstDayOfMonth(int year, int month){  
        Calendar c = new GregorianCalendar();  
        c.set(year, month, 1);  
        c.set(Calendar.HOUR, 0);  
        c.set(Calendar.MINUTE, 0);  
        c.set(Calendar.SECOND, 0);  
        return c.getTime();   
    }  
    /** 
     * 获取某月得最后一天 
     * @param year 年 
     * @param month 月 
     * @return 
     */  
    public static Date getLastDayOfMonth(int year, int month){  
        Calendar c = new GregorianCalendar();  
        c.set(year, month, 1);  
        c.roll(Calendar.DATE, -1);  
        c.set(Calendar.HOUR, 23);  
        c.set(Calendar.MINUTE, 59);  
        c.set(Calendar.SECOND, 59);  
          
        return c.getTime();   
    }  
    /** 
     * 获取某季度得第一天 
     * @param year　年 
     * @param quarter　季度 
     * @return 
     */  
    public static Date getFirstDayOfQuarter(int year, int quarter){  
        Calendar c = new GregorianCalendar();  
          
        int month = 0;  
        switch (quarter) {  
        case 1:  
            month = 1-1;  
            break;  
        case 2:  
            month = 4-1;          
            break;  
        case 3:  
            month = 7-1;  
            break;  
        case 4:  
            month = 10-1;  
            break;  
        default:  
            month = c.get(Calendar.MONTH);  
            break;  
        }  
          
        return getFirstDayOfMonth(year, month);   
    }
      
    /** 
     * 获取某季度得最一天 
     * @param year　年 
     * @param quarter　季度 
     * @return 
     */  
    public static Date getLastDayOfQuarter(int year, int quarter){  
        Calendar c = new GregorianCalendar();  
        int month = 0;  
        switch (quarter) {  
        case 1:  
            month = 3-1;  
            break;  
        case 2:  
            month = 6-1;          
            break;  
        case 3:  
            month = 9-1;  
            break;  
        case 4:  
            month = 12-1;  
            break;  
        default:  
            month = c.get(Calendar.MONTH);  
            break;  
        }  
        return getLastDayOfMonth(year, month);   
    } 
    /** 
     * 获取某年得第一天 
     * @param year 年 
     * @return 
     */  
    public static Date getFirstDayOfYear(int year){  
        return getFirstDayOfMonth(year, 0);  
    }  
      
    /** 
     * 获取某年得最后一天 
     * @param year 年 
     * @return 
     */  
    public static Date getLastDayOfYear(int year){  
        return getLastDayOfMonth(year, 11);  
    }  
  //------------------------------------------------------------------------------ 
	public static Date addYear(Date date, int yearAmount) {
		return addInteger(date, Calendar.YEAR, yearAmount);
	}
	public static Date addMonth(Date date, int monthAmount) {
		return addInteger(date, Calendar.MONTH, monthAmount);
	}
	public static Date addDay(Date date, int dayAmount) {
		return addInteger(date, Calendar.DATE, dayAmount);
	}
	public static Date addHour(Date date, int hourAmount) {
		return addInteger(date, Calendar.HOUR_OF_DAY, hourAmount);
	}
	public static Date addMinute(Date date, int minuteAmount) {
		return addInteger(date, Calendar.MINUTE, minuteAmount);
	}
	public static Date addSecond(Date date, int secondAmount) {
		return addInteger(date, Calendar.SECOND, secondAmount);
	}
	public static Date addMilliseconds( Date date,  int millisecondsAmount) {
	    return addInteger(date, Calendar.MILLISECOND, millisecondsAmount);
	}
//------------------------------------------------------------------

    public static Date setYears( Date date,  int amount) {
        return setInteger(date, Calendar.YEAR, amount);
    }
    public static Date setMonths( Date date,  int amount) {
        return setInteger(date, Calendar.MONTH, amount);
    }
    public static Date setDays( Date date,  int amount) {
        return setInteger(date, Calendar.DAY_OF_MONTH, amount);
    }
    public static Date setHours( Date date,  int amount) {
        return setInteger(date, Calendar.HOUR_OF_DAY, amount);
    }
    public static Date setMinutes( Date date,  int amount) {
        return setInteger(date, Calendar.MINUTE, amount);
    }
    public static Date setSeconds( Date date,  int amount) {
        return setInteger(date, Calendar.SECOND, amount);
    }
    public static Date setMilliseconds( Date date,  int amount) {
        return setInteger(date, Calendar.MILLISECOND, amount);
    } 
	//------------------------------------------------------------------
	/**
	 * 获取日期的星期
	 * @param date 日期
	 * @return 星期
	 */
	public static ZDateWeek getWeek(Date date) {
		if (date == null ) {
            throw new IllegalArgumentException("Date must not be null");
        }
		ZDateWeek week = null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int weekNumber = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		switch (weekNumber) {
		case 0:
			week = ZDateWeek.SUNDAY;
			break;
		case 1:
			week = ZDateWeek.MONDAY;
			break;
		case 2:
			week = ZDateWeek.TUESDAY;
			break;
		case 3:
			week = ZDateWeek.WEDNESDAY;
			break;
		case 4:
			week = ZDateWeek.THURSDAY;
			break;
		case 5:
			week = ZDateWeek.FRIDAY;
			break;
		case 6:
			week = ZDateWeek.SATURDAY;
			break;
		}
		return week;
	}
	/**
	 *  
	 * @param year
	 * @return true 闰年，false 平年
	 */
	 public static boolean isLeapyear(Date date) { 
	       if (date == null) {
	           throw new IllegalArgumentException("The date must not be null");
	       }
	       int year=getYear(date);
	        if ((year % 4 == 0 && year % 100 != 0) || (year % 400) == 0) {  
	            return true;  
	        } else {  
	            return false;  
	        }  
	    }
	/**
	 *  
	 * @param year
	 * @return true 闰年，false 平年
	 */
    public static boolean isLeapyear(int year) {  
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400) == 0) {  
            return true;  
        } else {  
            return false;  
        }  
    } 
    public static String getMonthLastDay(int month) {  
        Date date = new Date();  
        int[][] day = { { 0, 30, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },  
                        { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } };  
        int year=getYear(date)+ 1900;  
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {  
            return day[1][month] + "";  
        } else {  
            return day[0][month] + "";  
        }  
    }  
    
    public static int getIntervalDays(Date date, Date otherDate) {
    	if (date == null || otherDate == null) {
            throw new IllegalArgumentException("The date and otherDate must not be null");
        }
		int num = -1;
		Date dateTmp = parseDate(toString(date, ZDateStyle.YYYY_MM_DD),ZDateStyle.YYYY_MM_DD);
		Date otherDateTmp = parseDate(toString(date, ZDateStyle.YYYY_MM_DD),ZDateStyle.YYYY_MM_DD);
		if (dateTmp != null && otherDateTmp != null) {
			long time = Math.abs(dateTmp.getTime() - otherDateTmp.getTime());
			num = (int) (time / (24 * 60 * 60 * 1000));
		}
		return num;
	}
    public static int getIntervalHours(Date date, Date otherDate) {
    	if (date == null || otherDate == null) {
            throw new IllegalArgumentException("The date and otherDate must not be null");
        }
    	long s1=date.getTime();
    	long s2=otherDate.getTime();
    	int num=new Long(((Math.abs(s1-s2))/1000)/60/60).intValue();
		return num;
	}
    public static int getIntervalMinites(Date date, Date otherDate) {
    	if (date == null || otherDate == null) {
            throw new IllegalArgumentException("The date and otherDate must not be null");
        }
    	long s1=date.getTime();
    	long s2=otherDate.getTime();
    	int num=new Long(((Math.abs(s1-s2))/1000)/60).intValue();
		return num;
	}
    public static long getIntervalSeconds(Date date, Date otherDate) {
    	if (date == null || otherDate == null) {
            throw new IllegalArgumentException("The date and otherDate must not be null");
        }
    	long s1=date.getTime();
    	long s2=otherDate.getTime();
    	long num=new Long(((Math.abs(s1-s2))/1000)).longValue();
		return num;
	}
  //-------------isSame------------------------------------------------------------------------   
    public static boolean isSameDay( Date date1,  Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }
    public static boolean isSameDay( Calendar cal1,  Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    public static boolean isSameTime( Date date1,  Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return date1.getTime() == date2.getTime();
    }
    public static boolean isSameTime( Calendar cal1,  Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return cal1.getTime().getTime() == cal2.getTime().getTime();
    }
    public static boolean isSameLocalTime( Calendar cal1,  Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        return (cal1.get(Calendar.MILLISECOND) == cal2.get(Calendar.MILLISECOND) &&
                cal1.get(Calendar.SECOND) == cal2.get(Calendar.SECOND) &&
                cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE) &&
                cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.getClass() == cal2.getClass());
    }
//-------------private------------------------------------------------------------------------
    private static Date addInteger(Date date, int dateType, int amount) {
		  if (date == null) {
	            throw new IllegalArgumentException("The date must not be null");
	      }
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(dateType, amount);
		return calendar.getTime();
	}
    
	private static Date setInteger( Date date,  int calendarField,  int amount) {
	       if (date == null) {
	           throw new IllegalArgumentException("The date must not be null");
	       }
	      Calendar c = Calendar.getInstance();
	      c.setLenient(false);
	      c.setTime(date);
	      c.set(calendarField, amount);
	      return c.getTime();
	 } 
	
    private static final ThreadLocal<SimpleDateFormat> threadLocal = new ThreadLocal<SimpleDateFormat>();
	private static final Object object = new Object();
	//线程中单个对象存在，每次调用通过 dateFormat.applyPattern(pattern)创建新的pattern
	private static SimpleDateFormat caseFormatter(String pattern){
		SimpleDateFormat dateFormat = threadLocal.get();
		if (dateFormat == null) {
			synchronized (object) {
				if (dateFormat == null) {
					dateFormat = new SimpleDateFormat(pattern);
					dateFormat.setLenient(false);
					threadLocal.set(dateFormat);
				}
			}
		}
		dateFormat.applyPattern(pattern);
		return dateFormat;
	}
	//线程中单个对象存在，每次调用通过 dateFormat.applyPattern(pattern)创建新的pattern
	private static SimpleDateFormat caseFormatter(String pattern,Locale locale)  {
		SimpleDateFormat dateFormat = threadLocal.get();
		if (dateFormat == null) {
			synchronized (object) {
				if (dateFormat == null) {
					dateFormat = new SimpleDateFormat(pattern,locale);
					dateFormat.setLenient(false);
					threadLocal.set(dateFormat);
				}
			}
		}
		dateFormat.applyPattern(pattern);
		return dateFormat;
	}
	
}