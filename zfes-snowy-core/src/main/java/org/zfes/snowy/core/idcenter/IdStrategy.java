package org.zfes.snowy.core.idcenter;

public enum IdStrategy {
	uuid("uuid"),
	uuidBase58("uuidBase58"),
	uuidBase64("uuidBase64"),
	SnowflakeA("SnowflakeA"),
	SnowflakeB("SnowflakeB"),;
	
	private String value;
	
	IdStrategy(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}

}
