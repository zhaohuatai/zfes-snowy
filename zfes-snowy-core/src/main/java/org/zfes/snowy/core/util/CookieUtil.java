/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Set;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Sets;

public class CookieUtil {

	public static void setCookie(HttpServletResponse response, String name, String value, int maxAge) {
		setCookie(response, name, value, "/", maxAge);
	}
	
	public static void setSessionCookie(HttpServletResponse response, String name, String value,boolean httpOnly) {
		setSessionCookie( response,  name,  value,  null, httpOnly);
	}
	public static void setSessionCookie(HttpServletResponse response, String name, String value, String path,boolean httpOnly) {
		setSessionCookie(response, name, value,null,path, httpOnly);
	}
	public static void setCookie(HttpServletResponse response, String name, String value, String path, int maxAge) {
		Cookie cookie = new Cookie(name, null);
		cookie.setPath(path);
		cookie.setMaxAge(maxAge);
		try {
			cookie.setValue(URLEncoder.encode(value, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		response.addCookie(cookie);
	}
	public static void setSessionCookie(HttpServletResponse response, String name, String value,String domain, String path,boolean httpOnly) {
		Cookie cookie = new Cookie(name, null);
		cookie.setHttpOnly(httpOnly);
		if(ZStrUtil.isNotEmpty(path)){
			cookie.setPath(path);
		}
		if(ZStrUtil.isNotEmpty(domain)){
			cookie.setDomain(domain);
		}
		try {
			cookie.setValue(URLEncoder.encode(value, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		response.addCookie(cookie);
	}


	
	/**
	 * 获得指定Cookie的值
	 * @param name 名称
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request, String name) {
		return getCookie(request, null, name, false);
	}
	
	public static boolean isHaveDiffValue(HttpServletRequest request, String name){
		Set<String> cookieSet=Sets.newHashSet();
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			try {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(name)) {
							String value = URLDecoder.decode(cookie.getValue(), "utf-8");
							if(cookieSet.contains(value)){
								return true;
							}
							cookieSet.add(value);
					}
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	/**
	 * 获得指定Cookie的值，并删除。
	 * @param name 名称
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name) {
		return getCookie(request, response, name, true);
	}
	/**
	 * 获得指定Cookie的值
	 * @param request 请求对象
	 * @param response 响应对象
	 * @param name 名字
	 * @param isRemove 是否移除
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name, boolean isRemove) {
		String value = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					try {
						value = URLDecoder.decode(cookie.getValue(), "utf-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					if (isRemove) {
						cookie.setMaxAge(0);
						response.addCookie(cookie);
					}
				}
			}
		}
		return value;
	}
	
	public static void deleteAllCookie(HttpServletRequest request, HttpServletResponse response, String name,String ignotValue){
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					if(ignotValue!=null&&!ignotValue.equals(cookie.getValue())){
						cookie.setMaxAge(0);
						response.addCookie(cookie);
					}
				}
			}
		}
	}
	
	public static void deleteCookie(HttpServletRequest request, HttpServletResponse response, String name,String path){
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					cookie.setMaxAge(0);
					cookie.setPath("/");
					response.addCookie(cookie);
				}
			}
		}
	}
}
