/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class DataSetDto<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long total;
	private List<T> rows=new ArrayList<T>();
	private List<Map<String,Object>> footer=new ArrayList<Map<String,Object>>();//合计列
	private String simpleFooter;//合计列
	
//	public DataSet() {
//	}
	public DataSetDto(List<T>  rows) {
		this.total=(rows==null?0L:rows.size());
		this.rows = rows;
	}
	public DataSetDto(Long total, List<T>  rows) {
		this.total = total;
		this.rows = rows;
	}
	
	public static <T> DataSetDto<T> newDS(Long total, List<T>  rows) {
		return new DataSetDto<T>( total, rows);
	}
	public static <T> DataSetDto<T> newEmptyDS() {
		return new DataSetDto<T>( 0L, Collections.emptyList());
	}
	
	public DataSetDto(Long total, List<T>  rows,List<Map<String,Object>>  footer) {
		this.total = total;
		this.rows = rows;
		this.footer = footer;
	}
//	public DataSetDto<T> addToRow(T t) {
//		this.rows.add(t);
//		return this;
//	}
	
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public List<T>  getRows() {
		return rows;
	}
	public void setRows(List<T>  rows) {
		this.rows = rows;
	}
	public List<Map<String,Object>>  getFooter() {
		return footer;
	}
	public void setFooter(List<Map<String,Object>>  footer) {
		this.footer = footer;
	}

	public String getSimpleFooter() {
		return simpleFooter;
	}

	public void setSimpleFooter(String simpleFooter) {
		this.simpleFooter = simpleFooter;
	}
	


}
