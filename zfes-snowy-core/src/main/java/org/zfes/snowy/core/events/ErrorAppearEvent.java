/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.events;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

public class ErrorAppearEvent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Class<?> clazz;
	private String methodName;
	private String message;
	private Throwable e;
	private HttpServletRequest request;
	
	public ErrorAppearEvent(Class<?> clzz, String methodName, String message, Throwable e) {
		super();
		this.clazz = clzz;
		this.methodName = methodName;
		this.message = message;
		this.e = e;
	}
	public ErrorAppearEvent(String message, Throwable e) {
		super();
		this.message = message;
		this.e = e;
	}
	public ErrorAppearEvent(HttpServletRequest request,String message, Throwable e) {
		super();
		this.request=request;
		this.message = message;
		this.e = e;
	}
	public Class<?> getClazz() {
		return clazz;
	}
	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Throwable getE() {
		return e;
	}
	public void setE(Throwable e) {
		this.e = e;
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	
}
