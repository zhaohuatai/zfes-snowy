/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.consts;

import java.util.HashSet;
import java.util.Set;

public class AppBizConst {

	public static final class BOOLEAN{
		public static final Boolean TRUE=true;
		public static final Boolean FALSE=false;
	}
	public static final class BOOLEAN_STR{
		public static final String TRUE="true";
		public static final String FALSE="false";
	}
	public static final class BOOLEAN_NUM{
		public static final String TRUE="1";
		public static final String FALSE="0";
	}
	
	public static final Set<String> TRUE_VALUES = new HashSet<String>(4);

	public static final Set<String> FALSE_VALUES = new HashSet<String>(4);

	static {
		TRUE_VALUES.add("true");
		TRUE_VALUES.add("on");
		TRUE_VALUES.add("yes");
		TRUE_VALUES.add("1");
		
		TRUE_VALUES.add("TRUE");
		TRUE_VALUES.add("ON");
		TRUE_VALUES.add("YES");
		

		FALSE_VALUES.add("false");
		FALSE_VALUES.add("off");
		FALSE_VALUES.add("no");
		FALSE_VALUES.add("0");
		
		FALSE_VALUES.add("FALSE");
		FALSE_VALUES.add("OFF");
		FALSE_VALUES.add("NO");
	}

}
