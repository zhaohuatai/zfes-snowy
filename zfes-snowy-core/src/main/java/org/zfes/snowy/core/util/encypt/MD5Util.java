/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */

package org.zfes.snowy.core.util.encypt;

import java.security.MessageDigest;

public class MD5Util {
	private static final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	
	public static String encrypt( String pssword, String salt){
		pssword = pssword + salt;
		return encrypt(pssword);
	}
	/**
	 * 模式： salt+pssword
	 * @param pssword
	 * @param salt
	 * @return
	 */
	public static String encryptA( String pssword, String salt){
		pssword = salt+pssword ;
		return encrypt(pssword);
	}
	/**
	 * 模式： salt+"{" + salt + "}"
	 * @param pssword
	 * @param salt
	 * @return
	 */
	public static String encryptB( String pssword, String salt){
		pssword = salt+"{" + salt + "}" ;
		return encrypt(pssword);
	}
	/**
	 * 模式："{" +salt+ "}" + salt
	 * @param pssword
	 * @param salt
	 * @return
	 */
	public static String encryptC( String pssword, String salt){
		pssword = "{" +salt+ "}" + salt ;
		return encrypt(pssword);
	}
	/**
	 * 模式："{" +salt+ "}" +"{" + salt+ "}"
	 * @param pssword
	 * @param salt
	 * @return
	 */
	public static String encryptD( String pssword, String salt){
		pssword = "{" +salt+ "}" +"{" + salt+ "}" ;
		return encrypt(pssword);
	}
	public static String encrypt(String str){
		try {
			byte[]strBytes = str.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte[] digest = md.digest(strBytes);
			return getFormattedText(digest);
		} catch (Exception e) {
			return null;
		}
	}
	
	   private static String getFormattedText(byte[] bytes) {
		  int len = bytes.length;
		   StringBuilder buf = new StringBuilder(len * 2);
		      for (int j = 0; j < len; j++) {
		           buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
		           buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
		      }
		      return buf.toString();
		 }
	    public static void main(String[] argd){//eef3a22a128d5adb5699e3c7da7a6fc8
	    	String userName="admin";
	    	System.out.println(MD5Util.encrypt(userName));;
	    }
}
