/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.core.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZStrUtil;
public class JSONMSG implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int statusCode ;//状态码，***交互必须----
	private String message;//提示信息***交互必须----
	private String forwardUrl;//server返回后，跳转url ***不是必须----
	private String confirmMsg;//server返回后确认信息，***不是必须----
	private String token;//认证码 ***不是必须----
	private Map<String,Object> data=new HashMap<String,Object>();;//数据，认证码 ***不是必须----
	//private String StrData;
	public JSONMSG() {
		super();
	}
	public JSONMSG(int statusCode) {
		super();
		this.statusCode = statusCode;
	}
	public static JSONMSG newMSG(int statusCode) {
		return new JSONMSG( statusCode,  null);
	}
	public static JSONMSG newMSG(int statusCode, String message) {
		return new JSONMSG( statusCode,  message);
	}
	
	public JSONMSG(int statusCode, String message) {
		super();
		this.statusCode = statusCode;
		this.message = message;
	}
	public JSONMSG(int statusCode, String message, String forwardUrl,String confirmMsg) {
		super();
		this.statusCode = statusCode;
		this.message = message;
		this.forwardUrl = forwardUrl;
		this.confirmMsg = confirmMsg;
	}
	
//--------------------------------------------------------	
	public JSONMSG put(String key,Object value) {
		if(!ZStrUtil.hasText(key)){
			ZAlert.appRuntimeError("参数 key 不允许为空");
		}
		this.getData().put(key, value);
		return this;
	}
	
	public Optional<Object> getObj(String key) {
		if(this.getData().isEmpty()||!ZStrUtil.hasText(key)){
			return Optional.empty();
		}
		return Optional.ofNullable(this.getData().get(key));
	}
	
	public Optional<String> getStr(String key) {
		return ZObjectUtil.castStr(getObj( key));
	}
	
	public Optional<Integer> getInt(String key) {
		return ZObjectUtil.castInteger(getObj( key));
	}
	
	public Optional<Long> getLong(String key) {
		return ZObjectUtil.castLong(getObj( key));
	}
	public Optional<Double> getDouble(String key) {
		return ZObjectUtil.castDouble(getObj( key));
	}
	
	public Optional<Float> getFloat(String key) {
		return ZObjectUtil.castFloat(getObj( key));
	}
	public Optional<Short> getShort(String key) {
		return ZObjectUtil.castShort(getObj( key));
	}
	
	public Optional<Byte> getByte(String key) {
		return ZObjectUtil.castByte(getObj( key));
	}
	
	public Optional<Date> getDate(String key) {
		return ZObjectUtil.castDate(getObj( key));
	}
	public Optional<BigDecimal> getBigDecimal(String key) {
		return ZObjectUtil.castBigDecimal(getObj( key));
	}
	public Optional<byte[]> getByteArray(String key) {
		return ZObjectUtil.castByteArray(getObj( key));
	}
	
//--------------------------------------------------------	
	public int getStatusCode() {
		return statusCode;
	}
	public JSONMSG setStatusCode(int statusCode) {
		this.statusCode = statusCode;
		return this;
	}
	public String getMessage() {
		return message;
	}
	public JSONMSG setMessage(String message) {
		this.message = message;
		return this;
	}
	public String getForwardUrl() {
		return forwardUrl;
	}
	public JSONMSG setForwardUrl(String forwardUrl) {
		this.forwardUrl = forwardUrl;
		return this;
	}
	public String getConfirmMsg() {
		return confirmMsg;
	}
	public JSONMSG setConfirmMsg(String confirmMsg) {
		this.confirmMsg = confirmMsg;
		return this;
	}
	public String getToken() {
		return token;
	}
	public JSONMSG setToken(String token) {
		this.token = token;
		return this;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public JSONMSG setData(Map<String, Object> data) {
		this.data = data;
		return this;
	}
//	public String getStrData() {
//		return StrData;
//	}
//	public JSONMSG setStrData(String strData) {
//		StrData = strData;
//		return this;
//	}
	@Override
	public String toString() {
		return "JSONMSG [statusCode=" + statusCode + ", message=" + message + ", forwardUrl=" + forwardUrl
				+ ", confirmMsg=" + confirmMsg + ", token=" + token + ", data=" + data + "]";
	}
	
}
