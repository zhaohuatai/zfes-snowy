/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.core.util.encypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.zfes.snowy.core.util.ZStrUtil;


public class SHAUtil {
	   private final static String KEY_SHA = "SHA";  
	   private final static String KEY_SHA1 = "SHA-1";
	   private final static String KEY_SHA_256 = "SHA-256";
	   private final static String KEY_SHA_512 = "SHA-512";
	   private final static String KEY_SHA_384 = "SHA-384";
	   
	   private final static String[] HEX_DIGITS = { "0", "1", "2", "3", "4", "5","6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };  
	   private static byte[] encryptSHA(byte[] data,String  algorithm ){
		   MessageDigest sha;
			sha = getDigest(algorithm);
			sha.update(data); 
			return  sha.digest(); 
	   }
	   private  static MessageDigest getDigest(String algorithm) {  
	        try {  
	            return MessageDigest.getInstance(algorithm);  
	        } catch (NoSuchAlgorithmException e) {  
	            throw new RuntimeException(e.getMessage());  
	        }  
	    }
	   
	   public static byte[] encryptSHA(byte[] data)  {  
	        return encryptSHA(data,KEY_SHA );  
	   }
	   public static byte[] encryptSHA1(byte[] data)  {  
	    	return encryptSHA(data,KEY_SHA1 ); 
	   }
	   public static byte[] encryptSHA256(byte[] data)  {  
	    	return encryptSHA(data,KEY_SHA_256 ); 
	   }
	   public static byte[] encryptSHA384(byte[] data)  {  
	    	return encryptSHA(data,KEY_SHA_384 ); 
	   }
	   public static byte[] encryptSHA512(byte[] data)  {  
	    	return encryptSHA(data,KEY_SHA_512 ); 
	   }
	   
	   public static String encryptSHA(String data) { 
	        if (!ZStrUtil.hasText(data)) {  
	            return null;  
	        }  
	        return getFormattedText(encryptSHA(data.getBytes()));
	   } 
	   public static String encryptSHA1(String data) { 
	        if (!ZStrUtil.hasText(data)) {  
	            return null;  
	        }  
	        return getFormattedText(encryptSHA1(data.getBytes()));
	   } 
	   public static String encryptSHA256(String data) { 
	        if (!ZStrUtil.hasText(data)) {  
	            return null;  
	        }  
	        return getFormattedText(encryptSHA256(data.getBytes()));
	   } 
	   public static String encryptSHA384(String data) { 
	        if (ZStrUtil.hasNoText(data)) {  
	            return null;  
	        }  
	        return getFormattedText(encryptSHA384(data.getBytes()));
	   } 
	   public static String encryptSHA512(String data) { 
	        if (ZStrUtil.hasNoText(data)) {  
	            return null;  
	        }  
	        return getFormattedText(encryptSHA512(data.getBytes()));
	   } 
	    
	    
		private static String getFormattedText(byte[] bytes) {
			  int len = bytes.length;
			   StringBuilder buf = new StringBuilder(len * 2);
			      for (int j = 0; j < len; j++) {
			           buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
			           buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
			      }
			      return buf.toString();
		}
	  
}
