package org.apache.shiro.authz.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Requires the current Subject to have been authenticated <em>during their current session</em> for the annotated
 * class/instance/method to be accessed or invoked.  This is <em>more</em> restrictive than the
 * {@link RequiresUser RequiresUser} annotation.
 * <p/>
 * This annotation basically ensures that
 * <code>{@link org.apache.shiro.subject.Subject subject}.{@link org.apache.shiro.subject.Subject#isAuthenticated() isAuthenticated()} === true</code>
 * <p/>
 * See the {@link RequiresUser RequiresUser} and
 * {@link org.apache.shiro.authc.RememberMeAuthenticationToken RememberMeAuthenticationToken} JavaDoc for an
 * explaination of why these two states are considered different.
 *
 * @see RequiresUser
 * @see RequiresGuest
 *
 * @since 0.9.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresAuthentication {
}