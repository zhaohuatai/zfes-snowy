package org.apache.shiro.authc;

import org.apache.shiro.ShiroException;
/**
 * 用户已注册，但是未审核通过
 * @author root
 *
 */
public class AuthenticationNonCheckedException extends ShiroException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new AuthenticationException.
     */
    public AuthenticationNonCheckedException() {
        super();
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param message the reason for the exception
     */
    public AuthenticationNonCheckedException(String message) {
        super(message);
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public AuthenticationNonCheckedException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public AuthenticationNonCheckedException(String message, Throwable cause) {
        super(message, cause);
    }
}
