package org.apache.shiro.authc;

import org.apache.shiro.ShiroException;
/**
 * 新增 登录用户但是未注册异常，
 * @author root
 *
 */
public class AuthenticationUnregisterException extends ShiroException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new AuthenticationException.
     */
    public AuthenticationUnregisterException() {
        super();
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param message the reason for the exception
     */
    public AuthenticationUnregisterException(String message) {
        super(message);
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public AuthenticationUnregisterException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public AuthenticationUnregisterException(String message, Throwable cause) {
        super(message, cause);
    }
}
