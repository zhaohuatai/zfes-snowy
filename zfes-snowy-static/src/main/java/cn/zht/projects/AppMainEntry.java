package cn.zht.projects;
import java.io.IOException;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.zfes.snowy.generate.config.GenFreemarkerConfig;

import freemarker.template.Template;
@EnableAutoConfiguration
@ComponentScan(basePackages = {"cn.zht.projects"})
public class AppMainEntry{

    public static void main(String[] args) {
  	  
//	SpringApplication.run(AppMainEntry.class, args);
//	System.out.println("系统已启动...");
    	
    	Template tpl;
		try {
			tpl = GenFreemarkerConfig.getTemplate("/templates/", "Controller.java.ftl");
			System.out.println(tpl);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
