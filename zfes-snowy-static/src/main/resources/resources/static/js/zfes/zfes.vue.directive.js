var zfesVueDirective={
	zfesKselectOnchange: "zfes_kselect_onchange",
	zfesKselectInited:"zfes_kselect_inited",
	zfesVueEventBus:new Vue(),
	newBus:function(){
		zfesVueDirective.zfesVueEventBus=null;
		zfesVueDirective.zfesVueEventBus=new Vue();
		return zfesVueDirective.zfesVueEventBus;
	},
	getBus:function(){
		return zfesVueDirective.zfesVueEventBus;
	},
	getKselectOnchangeEventName:function(domId){
		return zfesVueDirective.zfesKselectOnchange+"_"+domId;
	},
	getKselectOnInitEventName:function(domId){
		return zfesVueDirective.zfesKselectInited+"_"+domId;
	}
};
/**
 * <ul id="menuTreeShow" 
 * v-ktree="{url:'auth/authMenu/loadAuthMenuTree',onClick:'zTreeOnClick',onRightClick:'zTreeOnRightClick',chkEnable:false,chkStyle:'checkbox'}" 
 * class="ztree"></ul>
				
 */
Vue.directive('ktree', {
	 inserted: function (el, binding,vnode) {
		 var contextData=vnode.context._data;
		 var param=binding.value;
		 var ajaxParam=param.ajaxParam;
		 var url=param.url;
		 if(!url){
			 alert("Vue指令使用错误：url 未正确设置");
		 }
		 
		 var chkEnable=param.chkEnable;
		 if(!(chkEnable==false||chkEnable=='false'||chkEnable==true||chkEnable=='true')){
			 alert("Vue指令使用错误：chkEnable 未正确设置 [bool 类型： true or false ]");
			 return;
		 }
		 var chkStyle=param.chkStyle;
		 if(chkEnable&&chkEnable==true&&!chkStyle){
			 alert("Vue指令使用错误：chkStyle 未正确设置 ");
			 return;
		 }
		 if(!(chkStyle=='checkbox'||chkStyle=='radio')){
			 alert("Vue指令使用错误：chkEnable 未正确设置 [checkbox or radio ]");
			 return;
		 }
		 onClickFunName=param.onClick;
		 onRightClickFunName=param.onRightClick;
		 var clickFun=undefined;
		 var onRightClick=undefined;
		 
		 if(onClickFunName){
			  clickFun=vnode.context[onClickFunName];
			 if(!clickFun){
				 alert("Vue指令使用错误： 未定义点击回调函数：["+onClickFunName+"]");
			 } 
		 }
		 if(onRightClickFunName){
			 onRightClick=vnode.context[onRightClickFunName];
			 if(!onRightClick){
				 alert("Vue指令使用错误： 未定义点击回调函数：["+onRightClickFunName+"]");
			 } 
		 }
		 var chkboxType=param.chkboxType;
		
		 var settings = {
					async:{
						enable: true,
						url:url,
						autoParam:["id=pid"],
                        contentType: "application/x-www-form-urlencoded",
						otherParam:$.extend({},ajaxParam)
					},
					check: {
						enable: chkEnable,
						chkStyle: chkStyle,
						chkboxType:chkboxType
					},
					edit: {enable: false},
					view: {fontCss: {"font-size":"14px"}},
					data: {
						simpleData: {
							enable: true,
							idKey: "id",
							pIdKey: "pId",
							rootPId: "-1"
						}
					},
					callback: {
						onClick:clickFun,
						onRightClick:onRightClick
						
					}
			};
		$.fn.zTree.init($(el), settings, null);
	  },
	  componentUpdated:function(){
		 console.log("componentUpdated");
	  }
	});
Vue.directive('kperms', {
	inserted: function (el, binding) {
		var valuex=binding.value;
	  if(valuex){
		  var permCode=valuex.perm;
		  if(zfesUtil.isNotEmpty(permCode)){
			 if(!zfesPermStore.havePermission(permCode)){
				  //el.remove();
			  }
		  }
	  }
  }
});
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};
Vue.directive('kmdate', {
	inserted: function (el, binding,vnode) {
		var contextData=vnode.context._data;
		var param=binding.value;
		var format=param.format;
		 $(el).datepicker({
		        language: "zh-CN",
		        autoclose: true,//选中之后自动隐藏日期选择框
		        clearBtn: true,//清除按钮
		        format: format
		    }).on('changeDate',function(ev){
		    	var defaultValue=ev.date.Format(format);
		    	var vmodel=param.vmodel;
		    	var arr=vmodel.split(".");
				 if(!arr||arr.length<0){
					 alert("vmodel与Vue-data中定义对象不一致");
					 return ;
				 }
				 var bingdingObjKey=arr[0];
				 var bingdingObj=contextData[bingdingObjKey];
				 if(arr.length==1){
					 contextData[bingdingObjKey]=firstEmptyDefaultValue;
				 }else{
					 for(var j=1;j<arr.length;j++){
						 var propName=arr[j];
						  if(!bingdingObj.hasOwnProperty(propName)){
							  alert("vmodel与Vue-data中定义对象不一致");
							  return;
						  }
						  if((j==arr.length-1)){
							  bingdingObj[propName]=defaultValue;
						  }else{
							  bingdingObj=bingdingObj[propName];
						  }
					 }
				 }
		    
		    });
  }
});
Vue.directive('kmdatetime', {
	inserted: function (el, binding,vnode) {
		var contextData=vnode.context._data;
		var param=binding.value;
		var format=param.format;
		 $(el).datetimepicker({
		        language: "zh-CN",
		        autoclose: true,//选中之后自动隐藏日期选择框
		        clearBtn: true,//清除按钮
		        minView : 0,
		        minuteStep:1,
		        format: format
		    }).on('changeDate',function(ev){
		    	var defaultValue=ev.date.Format(format);
		    	var vmodel=param.vmodel;
		    	var arr=vmodel.split(".");
				 if(!arr||arr.length<0){
					 alert("vmodel与Vue-data中定义对象不一致");
					 return ;
				 }
				 var bingdingObjKey=arr[0];
				 var bingdingObj=contextData[bingdingObjKey];
				 if(arr.length==1){
					 contextData[bingdingObjKey]=firstEmptyDefaultValue;
				 }else{
					 for(var j=1;j<arr.length;j++){
						 var propName=arr[j];
						  if(!bingdingObj.hasOwnProperty(propName)){
							  alert("vmodel与Vue-data中定义对象不一致");
							  return;
						  }
						  if((j==arr.length-1)){
							  bingdingObj[propName]=defaultValue;
						  }else{
							  bingdingObj=bingdingObj[propName];
						  }
					 }
				 }
		    
		    });
  }
});

Vue.directive('kmtime', {
    inserted: function (el, binding,vnode) {
        var contextData=vnode.context._data;
        var param=binding.value;
        var format=param.format;
        $(el).datetimepicker({
            language: "zh-CN",
            autoclose: true,//选中之后自动隐藏日期选择框
            clearBtn: true,//清除按钮
            minView : 0,
            minuteStep:1,
            format: 'hh:ii',
            startView:1
        }).on('changeDate',function(ev){
        	var defaultValue=ev.date.Format(format);
        	var vmodel=param.vmodel;
        	var arr=vmodel.split(".");
            if(!arr||arr.length<0){
                alert("vmodel与Vue-data中定义对象不一致");
                return ;
            }
            var bingdingObjKey=arr[0];
            var bingdingObj=contextData[bingdingObjKey];
            if(arr.length==1){
                contextData[bingdingObjKey]=firstEmptyDefaultValue;
            }else{
                for(var j=1;j<arr.length;j++){
                	var propName=arr[j];
                    if(!bingdingObj.hasOwnProperty(propName)){
                        alert("vmodel与Vue-data中定义对象不一致");
                        return;
                    }
                    if((j==arr.length-1)){
                        bingdingObj[propName]=defaultValue;
                    }else{
                        bingdingObj=bingdingObj[propName];
                    }
                }
            }

        });
    }
});


/**
 *
	说明：1先初始化下拉框的数据列表，2然后修改vue实例中 ==>v-model绑定的值，有数据双向绑定触发-select的值的变化。。。。
	v-kselect:
	url: 异步请求下拉列表地址,返回数据格式： {"confirmMsg":"","data":{"snowyselectdata":[{"text":"xxx","value":"xxxx"}]},"forwardUrl":"","message":"","statusCode":200}
	vmodel: dom 节点绑定的Vue data 对象
	firstEmpty： 下拉第一行是否空显示
	linkToDomId: 如果是联动的 子级，用于指定 自己节点监听的节点变化，
	ajaxParamKey： 如果是联动的 子级，用于指定 自己节点监听的节点 作为ajax的url参数，
}
例如： <select id="appKey" name="appKey" v-model="searchForm.appKey"  v-kselect="{url:'auth/authApp/loadAuthAppComboboVo',vmodel:'searchForm.appKey',firstEmpty:'false'}" class=" form-control"> </select>
 <select
   id="moduleId" 
   name="moduleId"
    v-model="searchForm.moduleId" 
	v-kselect="{url:'auth/authModule/loadModuleComboboVo',vmodel:'searchForm.moduleId',firstEmpty:'true',linkToDomId:'appKey',ajaxParamKey:'appKey'}" 
	class=" form-control"> </select>
 */
 Vue.directive('kselect', {
	 inserted: function (el, binding,vnode) {
		 var contextData=vnode.context._data;
		 var param=binding.value;
		 var vmodel=param.vmodel;
		 var bus=vnode.context._data.bus;
		 if(!bus){
			 alert("Vue指令使用错误：kselect指令依赖context data 中定义的 [ bus:new Vue()]");
		 }
		 if(!vmodel){
			 alert("Vue指令使用错误：vmodel 未正确设置");
		 }
		 var url=param.url;
		 if(!url){
			 alert("Vue指令使用错误：url 未正确设置");
		 }
		 var firstEmpty=(!(param.firstEmpty) ? false:param.firstEmpty);
		 var thisDomId=el.id;
		 if(!thisDomId){
			 alert("Vue指令使用错误：kselect指令所在元素必须设置id属性");
		 }
		 var linkToDomId=param.linkToDomId;
		 var isLinkedTo=false;
		 if(linkToDomId){
			  isLinkedTo=true; 
		}
//----------------------------参数初始化结束--------------------------------------------
		  $(el).select().on('change', function () {
			  bus.$emit(zfesVueDirective.getKselectOnchangeEventName(thisDomId), {selectedValue:this.value});
	      });
		  bus.$on(zfesVueDirective.getKselectOnInitEventName(linkToDomId), function (msg) {
			  bus.$emit(zfesVueDirective.getKselectOnchangeEventName(linkToDomId), msg);
		  });
		  bus.$on(zfesVueDirective.getKselectOnchangeEventName(linkToDomId), function (msg) {
			  var selectedValue=msg.selectedValue;
			  var ajaxParamKey=param.ajaxParamKey;
			  var ajaxParam={};
			  ajaxParam[ajaxParamKey] =selectedValue;
			  ajaxParam['firstEmpty'] =firstEmpty;
			//---------------------------------------------------------------------------------------------
				  zfesAjax.ajaxTodo(url, ajaxParam, function(data) {//text,value //异步加载数据
					  el.innerHTML = "";
					  var selectoptions= data.data.snowyselectdata;
						if(zfesUtil.isNotEmpty(selectoptions)){
							var firstEmptyDefaultValue=undefined;
							 for( var i = 0; i < selectoptions.length; i ++){
								 var op=new Option(selectoptions[i].text,selectoptions[i].value);
								 el.options.add(op);
								 if(i==0){
									 firstEmptyDefaultValue=selectoptions[i].value;
								 }
							}
							 if((firstEmpty==false||firstEmpty=='false')&&firstEmptyDefaultValue){
								 var arr=vmodel.split(".");
								 if(!arr||arr.length<0){
									 alert("vmodel与Vue-data中定义对象不一致");
									 return ;
								 }
								 var bingdingObjKey=arr[0];
								 var bingdingObj=contextData[bingdingObjKey];
								 if(arr.length==1){
									 contextData[bingdingObjKey]=firstEmptyDefaultValue;
								 }else{
									 for(var j=1;j<arr.length;j++){
										 var propName=arr[j];
										  if(!bingdingObj.hasOwnProperty(propName)){
											  alert("vmodel与Vue-data中定义对象不一致");
											  return;
										  }
										  if((j==arr.length-1)){
											  bingdingObj[propName]=firstEmptyDefaultValue;
										  }else{
											  bingdingObj=bingdingObj[propName];
										  }
									 }
								 }
							 }
							// console.log("searchForm-directive |"+ JSON.stringify(contextData['searchForm']));
						}
				
					//回调回来时发送事件
					bus.$emit(zfesVueDirective.getKselectOnInitEventName(thisDomId), {selectedValue:$(el).val()});
				  }); 
				  
				//------------------------------------------------------------------------------------------------
	  });
		  if(isLinkedTo){
			  return;
		  }
//----------------------------联动监听结束--------------------------------------------		  
		  zfesAjax.ajaxTodo(url, {firstEmpty:firstEmpty}, function(data) {//text,value //异步加载数据
			  el.innerHTML = "";
			  var selectoptions= data.data.snowyselectdata;
				if(zfesUtil.isNotEmpty(selectoptions)){
					var firstEmptyDefaultValue=undefined;
					 for( var i = 0; i < selectoptions.length; i ++){
						 var op=new Option(selectoptions[i].text,selectoptions[i].value);
						 el.options.add(op);
						 if(i==0){
							 firstEmptyDefaultValue=selectoptions[i].value;
						 }
					}
					 if((firstEmpty==false||firstEmpty=='false')&&firstEmptyDefaultValue){
						 var arr=vmodel.split(".");
						 if(!arr||arr.length<0){
							 alert("vmodel与Vue-data中定义对象不一致");
							 return ;
						 }
						 var bingdingObjKey=arr[0];
						 var bingdingObj=contextData[bingdingObjKey];
						 if(arr.length==1){
							 contextData[bingdingObjKey]=firstEmptyDefaultValue;
						 }else{
							 for(var j=1;j<arr.length;j++){
								 var propName=arr[j];
								  if(!bingdingObj.hasOwnProperty(propName)){
									  alert("vmodel与Vue-data中定义对象不一致");
									  return;
								  }
								  if((j==arr.length-1)){
									  bingdingObj[propName]=firstEmptyDefaultValue;
								  }else{
									  bingdingObj=bingdingObj[propName];
								  }
							 }
						 }
					 }
				}
			//回调回来时发送事件
			bus.$emit(zfesVueDirective.getKselectOnInitEventName(thisDomId),  {selectedValue:$(el).val()});
		  }); 
	  },
	  componentUpdated:function(){
		 //console.log("componentUpdated");
	  }
	});
 
