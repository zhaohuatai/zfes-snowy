var zfesVueDirective={
	zfesKselectOnchange: "zfes_kselect_onchange",
	zfesKselectInited:"zfes_kselect_inited",
	zfesVueEventBus:new Vue(),
	newBus:function(){
		zfesVueDirective.zfesVueEventBus=null;
		zfesVueDirective.zfesVueEventBus=new Vue();
		return zfesVueDirective.zfesVueEventBus;
	},
	getBus:function(){
		return zfesVueDirective.zfesVueEventBus;
	},
	getKselectOnchangeEventName:function(domId){
		return zfesVueDirective.zfesKselectOnchange+"_"+domId;
	},
	getKselectOnInitEventName:function(domId){
		return zfesVueDirective.zfesKselectInited+"_"+domId;
	}
}

Vue.directive('kperms', {
	inserted: function (el, binding) {
		var valuex=binding.value;
	  if(valuex){
		  var permCode=valuex.perm;
		  if(zfesUtil.isNotEmpty(permCode)){
			 if(!zfesPermStore.havePermission(permCode)){
				 // el.remove();  
			  }
		  }
	  }
  }
});
/**
 *单选： <select  v-model="selectedB" v-zsselect="static/select.json;selectedB" > </select>
 *多选：<select  v-model="selectedB" v-zsselect="static/select.json;selectedB" multiple="multiple"> </select>
 * v-model ：绑定Vue data，业务数据，
 * v-zsselect;selectedB： 异步加载URL，selectedB：绑定Vue data中的对象名称，
 * 如果多选multiple="multiple" 则selectedB对象必须是数组
 * <select v-model="selecteda.ad" v-kselect="{url:'static/select.json',vmodel:'selectedB'}" multiple="multiple" class=" form-control"> </select>
 */
//  先初始化下拉框的数据列表，然后修改vue实例中 v-model绑定的值，有数据双向绑定触发-select的值的变化。。。。
 Vue.directive('kselect', {
	 inserted: function (el, binding,vnode) {
		 var onChangeListener=null;
		 var bus=vnode.context._data.bus;
		 var param=binding.value;
		 var thisDomId=el.id;
		 var linkToDomId=param.linkToDomId;
		 var url=param.url;
		 var isLinkedTo=false;
		 var firstEmpty=param.firstEmpty;
		  if(!firstEmpty){
			  firstEmpty=false;
		  }
		  if(linkToDomId){
			  isLinkedTo=true; 
			}
		  //vnode.context._data.searchForm.moduleId=9;
		 // vnode.context._data.searchForm.permCode='asdada';
		  $(el).select().on('change', function () {
			  bus.$emit(zfesVueDirective.getKselectOnchangeEventName(thisDomId), {selectedValue:this.value});
	      });
		  bus.$on(zfesVueDirective.getKselectOnInitEventName(linkToDomId), function (msg) {
			  bus.$emit(zfesVueDirective.getKselectOnchangeEventName(linkToDomId), msg);
		  });
		  bus.$on(zfesVueDirective.getKselectOnchangeEventName(linkToDomId), function (msg) {
			  var selectedValue=msg.selectedValue;
			  var ajaxParamKey=param.ajaxParamKey;
			  var ajaxParam={};
				  ajaxParam[ajaxParamKey] =selectedValue;
				  ajaxParam['firstEmpty'] =firstEmpty;
					 //---------------------------------------------------------------------------------------------
					  isLinkedTo=true;
					  var vmodel=param.vmodel;
					  var temData=vnode.context._data;
					  
					  if(vmodel&&temData){
						  var arr=vmodel.split(".");
						  for(var j=0;j<arr.length;j++){ //找到Vue data 里绑定该dom的对象名
							  var propName=arr[j];
							  if(!temData||!temData.hasOwnProperty(propName)){
								  break;
							  }
							  temData = temData[propName];
						  }
					  }
					  var isMultiple=el.multiple; //是否是多选
					  var defaultValue=temData;
				
					  zfesAjax.ajaxTodo(url, ajaxParam, function(data) {//text,value //异步加载数据
						 
						  el.innerHTML = "";
						  var selectoptions= data.data.snowyselectdata;
						if(zfesUtil.isNotEmpty(selectoptions)){
							//迭代异步返回的数据，进行dom 渲染
							 for( var i = 0; i < selectoptions.length; i ++){
								 var op=new Option(selectoptions[i].text,selectoptions[i].value);
								 el.options.add(op);
									if(!isMultiple){//select是单选的情况
										if(defaultValue){
											if (op.value == defaultValue) op.selected = true;  
										}
									}else{//select是多选的情况
										 if(Object.prototype.toString.call(defaultValue) == '[object Array]'){
											 for(var k=0;k<defaultValue.length;k++){//Vue data 里绑定该dom的对象 值必须是数组
												 if(defaultValue){
													 if (defaultValue[k] == op.value) {  
														 op.selected = true;   
											        } 
												 }
											} 
										} 
									}
							} 
						}
						//回调回来时发送事件
						bus.$emit(zfesVueDirective.getKselectOnInitEventName(thisDomId), {param:{}});
					  }); 
					  
					//------------------------------------------------------------------------------------------------
		  });
		  if(isLinkedTo){
			  return;
		  }
		  var vmodel=param.vmodel;
		  var temData=undefined;
		  var finalData=undefined;
		  if(vmodel&&temData){
			  var arr=vmodel.split(".");
			  for(var j=0;j<arr.length;j++){ //找到Vue data 里绑定该dom的对象名
				  var prop=arr[j];
				  temData=vnode.context._data;
				  if(!temData.hasOwnProperty(prop)){
					  break;
				  }
				  temData = temData[prop];
			  }
		  }
		  var isMultiple=el.multiple; //是否是多选
		  var defaultValue=temData;
		  zfesAjax.ajaxTodo(url, {firstEmpty:firstEmpty}, function(data) {//text,value //异步加载数据
			  el.innerHTML = "";
			  var selectoptions= data.data.snowyselectdata;
			if(zfesUtil.isNotEmpty(selectoptions)){
				//迭代异步返回的数据，进行dom 渲染
				 for( var i = 0; i < selectoptions.length; i ++){
					 var op=new Option(selectoptions[i].text,selectoptions[i].value);
					 el.options.add(op);
						if(!isMultiple){//select是单选的情况
							if(defaultValue){
								if (op.value == defaultValue) {
									//op.selected = true;  
									//vnode.context._data.searchForm.appKey='APP1';
									
								}
							}
						}else{//select是多选的情况
							 if(Object.prototype.toString.call(defaultValue) == '[object Array]'){
								 for(var k=0;k<defaultValue.length;k++){//Vue data 里绑定该dom的对象 值必须是数组
									 if(defaultValue){
										 if (defaultValue[k] == op.value) {  
											// op.selected = true;   
								        } 
									 }
								} 
							} 
						}
				} 
				// vnode.context._data.searchForm.appKey='APP1';
			}
			//回调回来时发送事件
			bus.$emit(zfesVueDirective.getKselectOnInitEventName(thisDomId),  {selectedValue:$(el).val()});
		  }); 
	  },
	  componentUpdated:function(){
		 //console.log("componentUpdated");
	  }
	});
 
 
 
 
