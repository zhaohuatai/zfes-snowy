//var ssoCenterHost="http://172.25.253.8:8080/";
var zfesSSOClient= {
		syncSid:function(ssoCenterHost){
			//if(zfesSSOClient.isSidSyn()){
				//return;
			//}
			zfesSSOClient.doSyncSid(ssoCenterHost);
		},
		getCookie:function getCookie(name) { 
		    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
		    if(arr=document.cookie.match(reg))
		        return unescape(arr[2]); 
		    else 
		        return null; 
		},
		isSidSyn:function(){
			var syncsid=zfesSSOClient.getCookie("syncsid");
			if(syncsid=="true"){
				return true;
			}
			return false;
		},
		doSyncSid:function(ssoCenterHost){
			 $.ajax({ 
				 url : ssoCenterHost+"/api/auth/sso/center/sync.so",
				 dataType : "jsonp",
				 jsonp : "jsonpCallback",
				 async: false
		         success : function(json){
		        	 var s_si_=json.s_si_;
		        	 var s_tk_=json.s_tk_;
		        	 var param={"s_si_":s_si_,"s_tk_":s_tk_};
		           	 $.ajax({ 
						 url : "api/auth/sso/client/sync.so",
						 dataType : "jsonp",
						 jsonp : "jsonpCallback",
						 data : param,
						 async: false
				         success : function(json){},
				         error:function(){ 
				        	 alert("系统运行错误，请联系运维人员");
				        	 if (window.stop){}else{document.execCommand("Stop");}
				            }
				        });
		            },
		            error:function(){ 
		            	alert("认证中心：系统未启动或者出现错误：请联系运维人员");
		            	if (window.stop){}else{document.execCommand("Stop");}
		            }
		        });
		}
}
