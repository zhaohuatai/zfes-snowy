var zfesPermStore={
		permCodesJsonName:"permCodes",
		permCodesStoreName:"zfes_permission_storage",
		roleCodesJsonName:"roleCodes",
		roleCodesStoreName:"zfes_role_storage",
		initPerm:function(url,param,callback){
			zfesAjax.ajax(url,param, function(data){
				var permCodesJsonName=zfesPermStore.permCodesJsonName;
				var permArray=data.data[permCodesJsonName];
				
				var roleCodesJsonName=zfesPermStore.roleCodesJsonName;
				var roleCode=data.data[roleCodesJsonName];
				
				zfesPermStore.setPerms(permArray);
				zfesPermStore.setRole(roleCode);
				if(callback){
					callback();
				}
	    	});
		},
		setPerms:function(permsArray){
			var finalData = {};
			 if(!zfesSessionStore.isEmpty(permsArray)){
				 var len=permsArray.length;
				 for( var i=0; i<len; i++ ){//遍历数组[p1,p2,p3]，--》转化为对象{p1:1,p2:1,P3:1}
					 var permCode = permsArray[i];
					 var key=zfesPermStore.convertCode(permCode);
					finalData[key]=1;//遍历数组[p1,p2,p3]，--》转化为对象{p1:1,p2:1,P3:1}
				 } 
			  zfesSessionStore.remove(zfesPermStore.permCodesStoreName);
			  zfesSessionStore.set(zfesPermStore.permCodesStoreName,finalData);
			}
		},
		setRole:function(roleCode){
			 if(!zfesSessionStore.isEmpty(roleCode)){
				  zfesSessionStore.remove(zfesPermStore.roleCodesStoreName);
				  zfesSessionStore.set(zfesPermStore.roleCodesStoreName,roleCode);
				} 
		},
		getRole:function(){
			var roleCode=zfesSessionStore.get(zfesPermStore.roleCodesStoreName);
			return roleCode;
		},
		
		havePermission:function(permCode){
			var strx=zfesPermStore.convertCode(permCode);
			var permCodes=zfesSessionStore.get(zfesPermStore.permCodesStoreName);
			if(zfesSessionStore.isEmpty(permCodes)){
				return false;
			}
			if(!zfesSessionStore.isEmpty(permCodes[strx])){
				return true;
			}
		},
		convertCode:function(permCode){
			return permCode.replace(":","_");
		},
		reConvertCode:function(permCode){
			return permCode.replace("_",":");
		}
}