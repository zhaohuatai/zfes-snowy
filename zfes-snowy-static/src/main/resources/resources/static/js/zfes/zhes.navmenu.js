function loadMenu(url,appKey){
	url=url+"?appkey="+appKey;
	zfesAjax.ajaxTodo(url, {}, function(data){
		var  statuscode=data.statusCode;
		 if(statuscode&&statuscode!=200){
			 alertSwal.errorText(data.message);
			 return;
		 }
		 $('#zfes_adminlet_sidebar_menu').empty();
			  renderNavMenu(data.data.navMenuData);
				 var xitem=$('.zfes-menu-item');
				 $.each(xitem, function(index, item) {
					   item.onclick = function(){
						   resetOthers();
						 this.setAttribute('style', 'color: white');
						 var url=$(this).attr("url").toString();
						 var title=$(this).attr("title").toString();
						 $("#zfes_adminlet_main_content_title_big").html(title);
						 $("#zfes_adminlet_main_content_title_small").html(title);
					    loadContent(url);
					   }
			 });
	});
}
function resetOthers(){
	 var xitem=$('.zfes-menu-item');
	 $.each(xitem, function(index, item) {
		 this.removeAttribute('style', 'color: white');
	 });
}
function handleSpecilPage(data, xhr){
	//http header 传递返回消息参数--300.html中出现
	var zfes_header_msg =xhr.getResponseHeader("zfes_header_msg");
	zfes_header_msg=decodeURI(zfes_header_msg);
	$("#zfes_adminlet_main_content").empty();
	var dom=$('<div>').append(data);
	var zfes_msg_div=dom.find("#zfes_header_msg");
	if(zfesUtil.isStrHavaValue(zfes_header_msg)){
		zfes_msg_div.html(zfes_header_msg);
	}
	//http header------------end 
	var divx=dom[0].lastElementChild;
	$("#zfes_adminlet_main_content").html(divx);
	dom[0].remove();
	dom[0]=null;
	return false;
}


/**/
function loadContent(url){
		Pace.restart();//加载进度
		$("#zfes_adminlet_main_content").empty();
		
		zfesAjax.ajaxHtml(url, {}, function(data, textStatus, jqXHR){
			if(data.indexOf("statusCode") > 0){
				zfesCore.ajaxDone($.parseJSON(data));
				return false;
			}
			if(data.indexOf("zfes_page_specil_mark") > 0 ){
				return handleSpecilPage(data,jqXHR);
			}
			$("#zfes_adminlet_main_content").html(data);
			//ZVue.init();
		},function(jqXHR, textStatus, errorThrown){
			var data =jqXHR.responseText;
			if(data.indexOf("zfes_page_specil_mark") > 0 ){
					return handleSpecilPage(data,jqXHR);
			}
			$("#zfes_adminlet_main_content").html(data);
		
		});
}

function renderNavMenu(data) {
	var menulist = "";
	$.each(data, function(index, value) {
		var icon=value.icon;
		var title=value.title;
		var url=value.url;
		var target=value.target;
		var appKey=value.appKey;
		var secondLevelItems=value.items;
		menulist += '<li class="treeview">';
		if(!isHaveChildLevel(url,secondLevelItems)){
			menulist += '<a href="javascript:;"  url=\"'+url+'\"  title=\"'+title+'\"  class="zfes-menu-item" onclick="loadContent("'+url+'");">'
			menulist += '<i class="fa fa-'+icon+'"></i><span>'+title+'</span>';
			menulist += '</a>';
		}else{
			menulist += '<a href="javascript:;">';
			menulist += '<i class="fa fa-'+icon+'"></i> <span>'+title+'</span>';
			menulist += '<i class="fa fa-angle-left pull-right"></i>';
			menulist += '</a>';
			menulist += addSecondLevel(secondLevelItems);
			
		}
		menulist += '</li>';
	});
	$('#zfes_adminlet_sidebar_menu').append(menulist);
}

function addSecondLevel(secondLevelItems){
	if(!secondLevelItems || secondLevelItems.length==0){
		return "";
	}
	var menulist ='<ul class="treeview-menu">';
	$.each(secondLevelItems, function(index_s, item_s) {
		var icon_s=item_s.icon;
		var url_s=item_s.url;
		var title_s=item_s.title;
		var target_s=item_s.target;
		var appKey=item_s.appKey;
		var thirdLevelItems=item_s.items;
	//	var thirdLevelItems=item_s.childMenus;
		menulist += '<li class="treeview">';
		if(!isHaveChildLevel(url_s,thirdLevelItems)){
			menulist += '<a href="javascript:;"  url=\"'+url_s+'\"  title=\"'+title_s+'\"  class="zfes-menu-item" onclick="loadContent("'+url_s+'");">'
			menulist += '<i class="fa fa-'+icon_s+'"></i><span>'+title_s+'</span>';
			menulist += '</a>';
		}else{
			menulist += '<a href="javascript:;">';
			menulist += '<i class="fa fa-'+icon_s+'"></i> <span>'+title_s+'</span>';
			menulist += '<i class="fa fa-angle-left pull-right"></i>';
			menulist += '</a>';
			menulist += addThirdLevel(thirdLevelItems);
		}
	});
	menulist += '</ul>';
	return menulist;
}

function addThirdLevel(thirdLevelItems){
	if(!thirdLevelItems || thirdLevelItems.length==0){
		return "";
	}
	var menulist = '<ul class="treeview-menu">';
	$.each(thirdLevelItems, function(index_t, item_t) {
		var icon_t=item_t.icon;
		var url_t=item_t.url;
		var title_t=item_t.title;
		var target_t=item_t.target;
		var appKey=item_t.appKey;
		menulist += '<li class="treeview">';
		menulist += '<a href="javascript:;"  url=\"'+url_t+'\"  title=\"'+title_t+'\"  class="zfes-menu-item" onclick="loadContent("'+url_t+'");">'
		menulist += '<i class="fa fa-'+icon_t+'"></i><span>'+title_t+'</span>';
		menulist += '</a>';
		menulist += '</i>';
	});
	menulist += '</ul>';
	return menulist;
}

function isHaveChildLevel(url,childLevelItems){
	if((childLevelItems && childLevelItems.length>0)){
	//if((!zfesUtil.isStrHavaValue(url)||url=="#")&&(childLevelItems && childLevelItems.length>0)){
		return true;
	}
	return false;
}

