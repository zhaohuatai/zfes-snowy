var zfesZTree = {
		getTreeNodes:function(eleId){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			var nodes = treeObj.transformToArray(treeObj.getNodes());
			return nodes;
		},
		getTreeNodesParam:function(eleId,proName,proValue,pid){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			if(zfesUtil.isStrNull(pid)){
				pid=null
			}
			var nodes = treeObj.transformToArray(treeObj.getNodesByParam(proName, proValue, pid));
			return nodes;
		},
		getTreeNodeById:function(eleId,id){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			var nodes = treeObj.transformToArray(treeObj.getNodesByParam("id", id, null));
			return nodes[0];//仅仅返回当前节点，子节点不返回
		},
		getTreeNodeByCode:function(eleId,code){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			var nodes = treeObj.transformToArray(treeObj.getNodesByParam("code", code, null));
			return nodes[0];//仅仅返回当前节点，子节点不返回
		},
		reloadTree:function(eleId){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			treeObj.refresh();
		},
		refreshTreeAll:function(eleId,param){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			treeObj.setting.async.otherParam=param;
			treeObj.reAsyncChildNodes(null, "refresh");
		},
		refreshTreeSelectNode:function(eleId){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			var nodes = treeObj.getSelectedNodes();
			if (nodes.length>0) {
				treeObj.reAsyncChildNodes(nodes[0], "refresh");
			}
		},
		getCheckedNodes:function(eleId){
			var treeObj = $.fn.zTree.getZTreeObj(eleId);
			var nodes = treeObj.getCheckedNodes();
			return nodes;
		},
		getCheckedNode:function(eleId){
			return ZFTree.getCheckedNodes(eleId)[0];
		}	
};
