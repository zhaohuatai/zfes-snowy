/**
 * 时间控件
 */
var ZDateTime={
		remove:function(eleId){
			$('#'+eleId).datetimepicker('remove');
		},
		show:function(eleId){
			$('#'+eleId).datetimepicker('show');
		},
		hide:function(eleId){
			$('#'+eleId).datetimepicker('hide');
		},
		update:function(eleId){
			$('#'+eleId).datetimepicker('update');
		},
		init:function(){
			$("body").find("input[type='datetime']").each(function(){
				$(this).datetimepicker({
			        format: 'yyyy-mm-dd hh:ii:ss',
			        language: 'zh-CN',
			        autoclose:true,
			        todayHighlight:true,
			        todayBtn:true,
			        weekStart:1,//从周一开始
			        minuteStep: 1,
			        minView:0
			      });
			});
			$("body").find("input[type='dates']").each(function(){
				$(this).datetimepicker({
			        format: 'yyyy-mm-dd',
			        language: 'zh-CN',
			        autoclose:true,
			        todayHighlight:true,
			        todayBtn:true,
			        minView:2,
			        weekStart:1 //从周一开始
			      });
			});
		}
};
