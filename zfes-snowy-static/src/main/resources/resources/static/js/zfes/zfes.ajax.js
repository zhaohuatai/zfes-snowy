var zfesAjax={
		validateFrom:function(form){
			var _form = $(form);
			return _form.valid();
		},
		submitForm:function(form, callback){
			if(!zfesAjax.validateFrom(form)){
				return false;
			}
			var $form = $(form);
			var _submitFn=function(){
				var param=zfesUtil.formToJson($form);
				ajaxTodo(url, param, callback);
			}
			return true;
		},
		ajax:function(url, param, callback,fialCallback){
			$.ajax({
				type : 'POST',
				url : url,
				data : param,
				cache : false,
				beforeSend: function(request){
					request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					if(sessionStorage){request.setRequestHeader(zfesCore.csrfTikenHeaderName, sessionStorage.getItem(zfesCore.csrfTokenStoreName));}
				}//token ...
			})
			.done(function(data){
				$.extend(data,{"handled":"true"});
				if(callback){
					callback(data);
				}else{
					alert("普通 ajax请求，必须自定义callback，此函数不予处理");
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown){
				if(fialCallback){
					fialCallback(jqXHR, textStatus, errorThrown);
				}else{
					zfesCore.ajaxError(jqXHR, textStatus, errorThrown);
				}
			})
			.always(function(){})//do nothing
			.then(function(){});//do nothing
		},
	  ajaxHtml:function(url, param, callback,fialCallback){
		$.ajax({
			type : 'GET',
			url : url,
			data : param,
			dataType : "html",
			cache : true,
			beforeSend: function(request){
				request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				if(sessionStorage){request.setRequestHeader(zfesCore.csrfTikenHeaderName, sessionStorage.getItem(zfesCore.csrfTokenStoreName))}
			}
		})
		.done(function(data, textStatus, jqXHR){
			$.extend(data,{"handled":"true"});
			if(callback){
				callback(data, textStatus, jqXHR);
			}else{
				alert("普通 ajax请求，必须自定义callback，此函数不予处理");
				//zfesCore.ajaxDone(data);
			}
		})
		.fail(function(jqXHR, textStatus, errorThrown){
				if(fialCallback){
					fialCallback(jqXHR, textStatus, errorThrown);
				}else{
					zfesCore.ajaxError(jqXHR, textStatus, errorThrown);
				}
		})
		.always(function(){})//do nothing
		.then(function(){});//do nothing
	},
	 ajaxDownload: function(url,data,method){
		    if(url && data){
		    	var inputs ='';
		    	 $.each(data, function(key, val) { 
		    		 inputs +='<input type="hidden" name="'+key+'" value="'+val+'"/>';
		    	 });
		        $('<form action="'+url+'" method="'+(method || 'post')+'">'+inputs+'</form>').appendTo('body').submit().remove();
		    }
		},
	ajaxGet:function(url, param, callback,fialCallback){
		$.ajax({
			type : 'GET',
			url : url,
			data : param,
			dataType : "json",
			cache : true,
			beforeSend: function(request){
				request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				if(sessionStorage){request.setRequestHeader(zfesCore.csrfTikenHeaderName, sessionStorage.getItem(zfesCore.csrfTokenStoreName))}
			}
		})
		.done(function(data, textStatus, jqXHR){
			$.extend(data,{"handled":"true"});
			if(callback){
				callback(data, textStatus, jqXHR);
			}else{
				alert("普通 ajax请求，必须自定义callback，此函数不予处理");
				//zfesCore.ajaxDone(data);
			}
		})
		.fail(function(jqXHR, textStatus, errorThrown){
				if(fialCallback){
					fialCallback(jqXHR, textStatus, errorThrown);
				}else{
					zfesCore.ajaxError(jqXHR, textStatus, errorThrown);
				}
		})
		.always(function(){})//do nothing
		.then(function(){});//do nothing
	},
	ajaxTodo:function(url, param, callback,fialCallback){
		$.ajax({
			type : 'POST',
			url : url,
			data : param,
			dataType : "json",
			cache : false,
			xhrFields: {
                withCredentials: true
			},
			beforeSend: function(request){
				request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				if(sessionStorage){request.setRequestHeader(zfesCore.csrfTikenHeaderName, sessionStorage.getItem(zfesCore.csrfTokenStoreName))}
			}//token ...
		})
		.done(function(data){
			$.extend(data,{"handled":"true"});
			if(data.statusCode==zfesCore.statusCode.success){
				callback(data);
			}else{
				if(fialCallback){
					fialCallback(data);
				}else{
					zfesCore.ajaxDone(data);
				}
			}
			//(data.statusCode==zfesCore.statusCode.success)?callback(data):(fialCallback?fialCallback:zfesCore.ajaxDone(data));
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			zfesCore.ajaxError(jqXHR, textStatus, errorThrown);
		})
		.always(function(){})//do nothing
		.then(function(){});//do nothing
	}
};


