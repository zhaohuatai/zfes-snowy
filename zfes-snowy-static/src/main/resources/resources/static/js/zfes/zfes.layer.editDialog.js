var zfesLayerEditDg={
		//layerIndex:-1,
		layerParamKey:"zfes_layer_param_key",
		callBackParamKey:"zfes_callback_param_key",
		layerIndex:[] ,
		setLayerParam:function(param){
			store.set(zfesLayerEditDg.layerParamKey+"_", param);
		},
		getLayerParam:function(){
			var param=store.get(zfesLayerEditDg.layerParamKey+"_");
			//store.remove(zfesCore.layerParamKey+"_"+index);
			return param;
		},
		setCallBackParam:function(param){
			store.set(zfesLayerEditDg.callBackParamKey+"_", param);
		},
		getCallBackParam:function(){
			var param=store.get(zfesLayerEditDg.callBackParamKey+"_");
			//store.remove(zfesCore.callBackParamKey+"_");
			return param;
		},
		open:function(url,param,title,maxWidthx,endCallback){
			if(param){zfesLayerEditDg.setLayerParam(param);}
			var windowWidth=$(document.body).width();
			var realWidth=windowWidth>600?(maxWidthx>windowWidth?windowWidth*0.9:maxWidthx):(windowWidth*0.9);
			//console.log("realWidth:"+realWidth);
			//var realWidth=maxWidthx>windowWidth?windowWidth*0.9:maxWidthx;
			 zfesAjax.ajaxHtml(url, {}, function(data){
				 var contentx="<div id='zfes_edidialog_layer_container' style=\"overflow: hidden;\"><div style=\"width: "+realWidth+"px;height: 0px;\">&nbsp;</div>"+data+"</div>";
				 //var contentx="<div id='zfes_edidialog_layer_container' style=\"overflow: hidden;\">"+data+"</div>";
				 
				 var otptionx=$.extend({
					    type: 1,
					    title: title,
					    fix: true,
					    scrollbar: false,
					    content: contentx,
					    area: [''+realWidth+'px', 'auto'],
					    end :function(){zfesLayerEditDg.clearOpen(endCallback);}
					},{});
					var indexx=layer.open(otptionx);
					zfesLayerEditDg.layerIndex.push(indexx);
					//zfesLayerEditDg.layerIndex = layer.open(otptionx);
					//ZVue.init();
			});
		},
		
		lookback:function(url,param,title,maxWidthx,endCallback){
			if(param){zfesLayerEditDg.setCallBackParam(param);}
			var windowWidth=$(document.body).width();
			var realWidth=windowWidth>600?(maxWidthx>windowWidth?windowWidth*0.9:maxWidthx):(windowWidth*0.9);
			//var realWidth=maxWidthx>windowWidth?windowWidth*0.9:maxWidthx;
			 zfesAjax.ajaxHtml(url, {}, function(data){
				 var contentx="<div id='zfes_edidialog_layer_lookback_container' style=\"overflow: hidden;\"><div style=\"width: "+realWidth+"px;height: 0px;\">&nbsp;</div>"+data+"</div>";
				 var otptionx=$.extend({
					    type: 1,
					    title: title,
					    fix: true,
					    scrollbar: false,
					    content: contentx,
					    end :function(){zfesLayerEditDg.clearLookback(endCallback);}
					},{});
				 var indexx=layer.open(otptionx);
					zfesLayerEditDg.layerIndex.push(indexx);
			});
		},
		
		openOption:function(url,param,title,maxWidthx,layeroption,endCallback){
			if(param){zfesLayerEditDg.setLayerParam(param);}
			var windowWidth=$(document.body).width();
			var realWidth=windowWidth>600?(maxWidthx>windowWidth?windowWidth*0.9:maxWidthx):(windowWidth*0.9);
			 zfesAjax.ajaxHtml(url, {}, function(data){
				 var contentx="<div  id='zfes_edidialog_layer_container'  style=\"overflow: hidden;\"><div style=\"width: "+realWidth+"px;height: 0px;\">&nbsp;</div>"+data+"</div>";
				 var otptionx=$.extend({
					    type: 1,
					    title: title,
					    fix: true,
					    scrollbar: false,
					    content: contentx,
					    end :endCallback
					},layeroption);
				 var indexx=layer.open(otptionx);
					zfesLayerEditDg.layerIndex.push(indexx);
					//zfesLayerEditDg.layerIndex = layer.open(otptionx);
			});
		},
		clearOpen:function(endCallback){
			if($("#zfes_edidialog_layer_container")){
				$("#zfes_edidialog_layer_container").empty();
				$("#zfes_edidialog_layer_container").remove();
			}
			endCallback();
		},
		clearLookback:function(endCallback){
			if($("#zfes_edidialog_layer_lookback_container")){
				$("#zfes_edidialog_layer_lookback_container").empty();
				$("#zfes_edidialog_layer_lookback_container").remove();
			}
			endCallback();
		},
		
		close:function(){
			//$("#zfes_layer_div_container").remove();
			//layer.close(zfesLayerEditDg.layerIndex);
			layer.close(zfesLayerEditDg.layerIndex.pop());
		},
		full:function(index){layer.full(zfesLayerEditDg.layerIndex);},
}	 
