/**
 * @author:曹仁道 2016年9月1日 上午10:26:04
 * @描述：
 */
var ZFUpload = {
		//这里用了局部动态加载js和css，建议index中的js和css也采用动态加载，初始值加载主要部分，其他的在需要的时候再加载
	loadJs : function(jsUrls, fnBack) {
		for(var i=0;i<jsUrls.length;i++){
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = jsUrls[i];
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		}
		fnBack();
	},
	loadCss : function(cssUrls, fnBack) {
		for(var i=0;i<cssUrls.length;i++){
			var ga = document.createElement('link');
			ga.rel = 'stylesheet';
			ga.async = true;
			ga.href = cssUrls[i];
			var s = document.getElementsByTagName('link')[0];
			s.parentNode.insertBefore(ga, s);
		}
		fnBack();
	},
	loadInit : function(fnBack) {
		var jsUrls=[];
		jsUrls.push("static/libs/bootstrap-fileinput/js/fileinput.js");
		//jsUrls.push("static/libs/bootstrap-fileinput/js/locales/zh.js");
		ZFUpload.loadJs(jsUrls,function(){
			var cssUrls=[];
			cssUrls.push("static/libs/bootstrap-fileinput/css/fileinput.min.css");
			ZFUpload.loadCss(cssUrls,	function(){
				setTimeout(function(){fnBack();},500);
			});
		});
	},
	singleUpload:function(inputEle,allowFile,params,uploadBack,uploadUrl,deleteUrl,initShowUrls){
		if(zfesUtil.isStrNull(uploadUrl)||zfesUtil.isStrNull(deleteUrl)){
			alertSwal.errorText("请提供文件上传路径和删除文件路径");
			return;
		}
		if(zfesUtil.isStrNull(allowFile)){
			allowFile=null;
		}
        var initialPreview=[];
        if(!zfesUtil.isStrNull(initShowUrls)){
            if($.isArray(initShowUrls)){
                for(var i=0;i<initShowUrls;i++){
                    initialPreview.push("<img style='width:300px;height:200px;' alt='没有找到' src='"+initShowUrls[i]+"'>");
                }
            }else{
                initialPreview.push("<img style='width:300px;height:200px;' alt='没有找到' src='"+initShowUrls+"'>");
            }
        }
		ZFUpload.loadInit(function(){
			 var control = $('#' + inputEle); 
		    control.fileinput({
		        language: 'zh', //设置语言
		        uploadUrl: uploadUrl, //上传的地址
		        uploadExtraData:params,
		        allowedFileExtensions : allowFile,//接收的文件后缀
		        showUpload: true, //是否显示上传按钮
		        //showCaption: false,//是否显示标题
		        enctype: 'multipart/form-data',
                initialPreview:initialPreview,
                allowedPreviewMimeTypes: ['image/jpeg','image/gif','image/png','image/bmp'],
		        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
		        previewFileIconSettings: {
		          'doc': '<i class="fa fa-file-word-o text-primary"></i>',
		          'xls': '<i class="fa fa-file-excel-o text-success"></i>',
		          'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
		          'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
		          'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
		          'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
		      },
		      previewFileExtSettings: {
		        'doc': function(ext) {
		            return ext.match(/(doc|docx)$/i);
		        },
		        'xls': function(ext) {
		            return ext.match(/(xls|xlsx)$/i);
		        },
		        'ppt': function(ext) {
		            return ext.match(/(ppt|pptx)$/i);
		        }
		      }
		    });
		    control.on("fileuploaded", function (event, data, previewId, index) {
		      //$("#myModal").modal("hide");
		    	console.log("previewId:"+previewId+",index:"+index);
		      var resp = data.response;
		      if(!zfesUtil.isStrNull(uploadBack)){
		      	if(!zfesUtil.isStrNull(resp)){
		      		var fileId="";
		      		if(!zfesUtil.isStrNull(resp.JSONMSG)){
		      			fileId=resp.JSONMSG.strData;
		      		}
		      		if(zfesUtil.isStrNull(fileId)&&!zfesUtil.isStrNull(resp.strData)){
		      			fileId=resp.strData;
		      		}
		      		if(zfesUtil.isStrNull(fileId)&&!zfesUtil.isStrNull(resp.JSONMSG)&&!zfesUtil.isStrNull(resp.JSONMSG.message)){
		      			fileId=resp.JSONMSG.message;
		      		}
		      		if(zfesUtil.isStrNull(fileId)&&!zfesUtil.isStrNull(resp.message)){
		      			fileId=resp.message;
		      		}
		      		$("#"+previewId).attr("fileId",fileId);
		      		uploadBack(fileId);
		      	}else{
		      		alertSwal.errorText(resp.message);
		      	}
		      }else{
		      	alert("文件上传成功后必须回调！请提供回调函数");
		      }
		    });
		    control.on('filesuccessremove', function(event, id) {
		    	var fileId=$("#"+id).attr("fileId");
		  		if(!zfesUtil.isStrNull(fileId)){
		  				zfesAjax.ajaxTodo(deleteUrl,{id:fileId}, function(data){
		  					alertSwal.successText("删除成功");
		  			});
		  	 	 }
		    	//alertSwal.successText("删除成功");
		    });
		});
	},
	testUpload:function(inputEle,allowFile,params,uploadBack){
		var uploadUrl=host_auth+"gradms/base/baseFile/uploadFile";
		var deleteUrl=host_auth+"gradms/base/baseFile/deletBaseFile";
		ZFUpload.singleUpload(inputEle,allowFile,params,uploadBack,uploadUrl,deleteUrl);
	},
	batchUpload:function(inputEle,params,uploadBack,deleteBack){
		var uploadUrl="base/baseFile/batchUploadFile";
		var deleteUrl="base/baseFile/deletBaseFile";
		ZFUpload.loadInit(function(){
			 var control = $('#' + inputEle); 
		    control.fileinput({
		        language: 'zh', //设置语言
		        uploadUrl: uploadUrl, //上传的地址
		        uploadAsync: true,
                uploadExtraData:params,
		        //deleteUrl: deleteUrl,//删除地址
		        allowedFileExtensions : ['jpg', 'png','gif','zip'],//接收的文件后缀
		        showUpload: true, //是否显示上传按钮
		        showCaption: false,//是否显示标题
		        enctype: 'multipart/form-data',
                maxFileCount: 5,
                browseClass: "btn btn-primary", //按钮样式
		        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
		    });
		    $("#"+inputEle).on("filebatchuploadsuccess", function (event, data, previewId, index) {
		      //$("#myModal").modal("hide");
		      var resp = data.response;
		      if(!zfesUtil.isStrNull(uploadBack)){
		      	uploadBack(resp);
		      }else{
		      	alert("文件上传成功后必须回调！请提供回调函数");
		      }
		    });
		    $("#"+inputEle).on('filedeleted', function(event, key) {
		      if(!zfesUtil.isStrNull(deleteBack)){
		      	 deleteBack(key);
		      }else{
		      	alertSwal.successText("删除成功");
		      }
		    });
		});
	},
}