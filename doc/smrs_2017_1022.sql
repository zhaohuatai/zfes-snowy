/*
Navicat MySQL Data Transfer

Source Server         : 116.62.131.162
Source Server Version : 50718
Source Host           : 116.62.131.162:3308
Source Database       : smrs

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-10-22 11:27:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_app
-- ----------------------------
DROP TABLE IF EXISTS `auth_app`;
CREATE TABLE `auth_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `appKey` varchar(40) NOT NULL COMMENT '模块标识_U',
  `name` varchar(40) NOT NULL COMMENT '模块名称',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `disIndex` int(11) NOT NULL COMMENT '顺序',
  `remark` varchar(60) DEFAULT NULL COMMENT '描述',
  `appDomain` varchar(300) DEFAULT NULL COMMENT 'app域名',
  `enabled` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`),
  KEY `auth_module_index_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_app
-- ----------------------------
INSERT INTO `auth_app` VALUES ('1', 'APP_ROOT', '基础应用', '1', '1', 'ssss', '', '');
INSERT INTO `auth_app` VALUES ('2', 'APP1', 'APP1', '', '3', 'APP1', null, '');

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `name` varchar(30) NOT NULL COMMENT '用户组名称',
  `description` varchar(60) DEFAULT NULL COMMENT '用户组描述',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '用户组状态',
  `parentId` bigint(20) DEFAULT NULL COMMENT '父用户组',
  `appId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_group_index_parent_id` (`parentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------
INSERT INTO `auth_group` VALUES ('1', '根节点', null, '\0', null, null);
INSERT INTO `auth_group` VALUES ('2', 'A1', null, '\0', '1', null);
INSERT INTO `auth_group` VALUES ('3', 'A2', null, '\0', '1', null);
INSERT INTO `auth_group` VALUES ('4', 'B', null, '\0', '2', null);

-- ----------------------------
-- Table structure for auth_group_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_role`;
CREATE TABLE `auth_group_role` (
  `id` bigint(20) NOT NULL,
  `auth_group_id` bigint(20) NOT NULL,
  `auth_role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_group_role_auth_group_id` (`auth_group_id`),
  KEY `auth_group_role_auth_role_id` (`auth_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_role
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_user`;
CREATE TABLE `auth_group_user` (
  `id` bigint(20) NOT NULL,
  `auth_group_id` bigint(20) NOT NULL,
  `auth_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_group_user_index_auth_group_id` (`auth_group_id`),
  KEY `auth_group_user_index_auth_user_id` (`auth_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `auth_menu`;
CREATE TABLE `auth_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '模块状态',
  `name` varchar(40) NOT NULL,
  `iconCls` varchar(40) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL,
  `showIndex` int(11) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `moduleId` bigint(20) DEFAULT NULL,
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属应用',
  `description` varchar(40) DEFAULT NULL,
  `permissionId` bigint(20) DEFAULT NULL,
  `isCommon` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `auth_menu_index_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_menu
-- ----------------------------
INSERT INTO `auth_menu` VALUES ('4', '', '根节点', '', 'G', '1', '-1', null, 'APP_ROOT', null, null, '');
INSERT INTO `auth_menu` VALUES ('10', '', '权限维护', 'lock', 'G', '1', '4', '1', 'APP_ROOT', null, null, '');
INSERT INTO `auth_menu` VALUES ('11', '', '菜单管理', null, 'M', '1', '10', '1', 'APP_ROOT', '菜单管理', '42', '');
INSERT INTO `auth_menu` VALUES ('12', '', '权限管理', null, 'M', '2', '10', '1', 'APP_ROOT', '1112', '44', '');
INSERT INTO `auth_menu` VALUES ('13', '', '角色管理', null, 'M', '3', '10', '1', 'APP_ROOT', '角色', '43', '');
INSERT INTO `auth_menu` VALUES ('40', '', '应用管理', null, 'M', '1', '10', '1', 'APP_ROOT', 'APP管理', '40', '');
INSERT INTO `auth_menu` VALUES ('97', '', '用户管理', null, 'M', '4', '10', '1', 'APP_ROOT', '用户信息管理', '45', '');
INSERT INTO `auth_menu` VALUES ('98', '', '总览', 'pie-chart', 'M', '1', '4', '3', 'APP_ROOT', null, '1079', '\0');
INSERT INTO `auth_menu` VALUES ('99', '', '商品管理', 'picture', 'M', '2', '4', '3', 'APP_ROOT', '商品管理', '1082', '\0');
INSERT INTO `auth_menu` VALUES ('100', '', '订单管理', 'file', 'M', '3', '4', '3', 'APP_ROOT', null, '1083', '\0');
INSERT INTO `auth_menu` VALUES ('101', '', '会员管理', 'user', 'M', '4', '4', '3', 'APP_ROOT', null, '1084', '\0');
INSERT INTO `auth_menu` VALUES ('102', '', '交易管理', 'pay-circle', 'M', '5', '4', '3', 'APP_ROOT', null, '1085', '\0');
INSERT INTO `auth_menu` VALUES ('103', '', '内容管理', 'file-text', 'M', '6', '4', '3', 'APP_ROOT', null, '1086', '\0');
INSERT INTO `auth_menu` VALUES ('104', '', '配置管理', 'setting', 'M', '7', '4', '3', 'APP_ROOT', null, '1087', '\0');
INSERT INTO `auth_menu` VALUES ('105', '', '分类管理', 'switcher', 'M', '8', '4', '3', 'APP_ROOT', null, '1088', '\0');

-- ----------------------------
-- Table structure for auth_module
-- ----------------------------
DROP TABLE IF EXISTS `auth_module`;
CREATE TABLE `auth_module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(40) NOT NULL COMMENT '模块名称',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '模块状态',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `disIndex` int(11) NOT NULL COMMENT '顺序',
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属app',
  `parentId` bigint(20) NOT NULL COMMENT '父Id',
  `remark` varchar(30) DEFAULT NULL COMMENT '描述',
  `code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_module_index_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_module
-- ----------------------------
INSERT INTO `auth_module` VALUES ('1', '权限模块', '', '', '21', 'APP_ROOT', '1', '权限模块', 'authModule');
INSERT INTO `auth_module` VALUES ('2', '系统模块说明', '', null, '1', 'APP_ROOT', '-1', '根节点', '');
INSERT INTO `auth_module` VALUES ('3', '商城管理', '', null, '22', 'APP_ROOT', '-1', '商城管理', 'weimall');

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permCode` varchar(60) NOT NULL,
  `name` varchar(40) NOT NULL,
  `enabled` bit(1) NOT NULL DEFAULT b'1',
  `type` varchar(1) NOT NULL,
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属应用',
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(60) DEFAULT NULL,
  `moduleId` bigint(20) DEFAULT NULL,
  `isCommon` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_index_code` (`permCode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1094 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'authApp:loadAuthApp', '应用加载', '', 'p', 'APP_ROOT', '/auth/authApp/loadAuthApp', '应用加载', '1', '');
INSERT INTO `auth_permission` VALUES ('2', 'authApp:updateAuthApp', '应用更新', '', 'p', 'APP_ROOT', '/auth/authApp/updateAuthApp', '应用更新', '1', '');
INSERT INTO `auth_permission` VALUES ('3', 'authApp:loadAuthAppList', '应用列表', '', 'p', 'APP_ROOT', '/auth/authApp/loadAuthAppList', 'app应用列表', '1', '');
INSERT INTO `auth_permission` VALUES ('4', 'authApp:addAuthApp', '应用添加', '', 'p', 'APP_ROOT', '/auth/authApp/addAuthApp', '应用添加', '1', '');
INSERT INTO `auth_permission` VALUES ('5', 'authApp:deletAuthApp', '应用删除', '', 'p', 'APP_ROOT', '/auth/authApp/deletAuthApp', '应用删除', '1', '');
INSERT INTO `auth_permission` VALUES ('6', 'authApp:setAuthAppEnabled', '应用状态设置', '', 'p', 'APP_ROOT', '/auth/authApp/setAuthAppEnabled', '应用状态设置', '1', '');
INSERT INTO `auth_permission` VALUES ('7', 'authMenu:loadAuthMenuList', '菜单列表', '', 'p', 'APP_ROOT', '/auth/authMenu/loadAuthMenuList', '菜单列表', '1', '');
INSERT INTO `auth_permission` VALUES ('8', 'authMenu:loadAuthMenu', '菜单加载', '', 'p', 'APP_ROOT', '/auth/authMenu/loadAuthMenu', '菜单加载', '1', '');
INSERT INTO `auth_permission` VALUES ('9', 'authMenu:updateAuthMenu', '菜单更新', '', 'p', 'APP_ROOT', '/auth/authMenu/updateAuthMenu', '菜单更新', '1', '');
INSERT INTO `auth_permission` VALUES ('10', 'authMenu:removeAuthMenu', '菜单删除', '', 'p', 'APP_ROOT', '/auth/authMenu/removeAuthMenu', '菜单删除', '1', '');
INSERT INTO `auth_permission` VALUES ('11', 'authMenu:loadAuthMenuTree', '加载菜单树', '', 'p', 'APP_ROOT', '/auth/authMenu/loadAuthMenuTree', '加载菜单树', '1', '');
INSERT INTO `auth_permission` VALUES ('12', 'authMenu:addAuthMenu', '菜单添加', '', 'p', 'APP_ROOT', '/auth/authMenu/addAuthMenu', '菜单添加', '1', '');
INSERT INTO `auth_permission` VALUES ('13', 'authMenu:setAuthMenuEnabled', '菜单状态设置', '', 'p', 'APP_ROOT', '/auth/authMenu/setAuthMenuEnabled', '菜单状态设置', '1', '');
INSERT INTO `auth_permission` VALUES ('14', 'authModule:updateAuthModule', '系统模块表更新', '', 'p', 'APP_ROOT', '/auth/authModule/updateAuthModule', '系统模块表更新', '1', '');
INSERT INTO `auth_permission` VALUES ('15', 'authModule:loadAuthModule', '模块表加载', '', 'p', 'APP_ROOT', '/auth/authModule/loadAuthModule', '模块表加载', '1', '');
INSERT INTO `auth_permission` VALUES ('16', 'authModule:addAuthModule', '模块表添加', '', 'p', 'APP_ROOT', '/auth/authModule/addAuthModule', '模块表添加', '1', '');
INSERT INTO `auth_permission` VALUES ('17', 'authModule:deletAuthModule', '模块表删除', '', 'p', 'APP_ROOT', '/auth/authModule/deletAuthModule', '模块表删除', '1', '');
INSERT INTO `auth_permission` VALUES ('18', 'authModule:loadAuthModuleList', '模块表列表', '', 'p', 'APP_ROOT', '/auth/authModule/loadAuthModuleList', '模块表列表', '1', '');
INSERT INTO `auth_permission` VALUES ('19', 'authPermission:loadForUpdate', '权限加载', '', 'p', 'APP_ROOT', '/auth/authPermission/loadForUpdate', '权限加载', '1', '');
INSERT INTO `auth_permission` VALUES ('20', 'authPermission:updatePermission', '权限更新', '', 'p', 'APP_ROOT', '/auth/authPermission/updatePermission', '权限更新', '1', '');
INSERT INTO `auth_permission` VALUES ('21', 'authPermission:addPermission', '权限添加', '', 'p', 'APP_ROOT', '/auth/authPermission/addPermission', '权限添加', '1', '');
INSERT INTO `auth_permission` VALUES ('22', 'authPermission:setPermissionEnabled', '权限状态设置', '', 'p', 'APP_ROOT', '/auth/authPermission/setPermissionEnabled', '权限状态设置', '1', '');
INSERT INTO `auth_permission` VALUES ('23', 'authPermission:batchDeleteAuthPermission', '权限批量删除', '', 'p', 'APP_ROOT', '/auth/authPermission/batchDeleteAuthPermission', '权限批量删除', '1', '');
INSERT INTO `auth_permission` VALUES ('24', 'authPermission:loadAuthPermissionList', '权限列表', '', 'p', 'APP_ROOT', '/auth/authPermission/loadAuthPermissionList', '权限列表', '1', '');
INSERT INTO `auth_permission` VALUES ('25', 'authPermission:deletAuthPermission', '权限删除', '', 'p', 'APP_ROOT', '/auth/authPermission/deletAuthPermission', '权限删除', '1', '');
INSERT INTO `auth_permission` VALUES ('26', 'authRole:updateRole', '角色更新', '', 'p', 'APP_ROOT', '/auth/authRole/updateRole', '角色更新', '1', '');
INSERT INTO `auth_permission` VALUES ('27', 'authRole:loadAuthRoleList', '角色列表', '', 'p', 'APP_ROOT', '/auth/authRole/loadAuthRoleList', '角色列表', '1', '');
INSERT INTO `auth_permission` VALUES ('28', 'authRole:deletAuthRole', '角色删除', '', 'p', 'APP_ROOT', '/auth/authRole/deletAuthRole', '角色删除', '1', '');
INSERT INTO `auth_permission` VALUES ('29', 'authRole:loadRole', '角色加载', '', 'p', 'APP_ROOT', '/auth/authRole/loadRole', '角色加载', '1', '');
INSERT INTO `auth_permission` VALUES ('30', 'authRole:permAssign', '权限分配', '', 'p', 'APP_ROOT', '/auth/authRole/addPermsToRole', '权限分配', '1', '');
INSERT INTO `auth_permission` VALUES ('31', 'authRole:setAuthRoleEnabled', '角色状态更新', '', 'p', 'APP_ROOT', '/auth/authRole/setAuthRoleEnabled', '角色状态更新', '1', '');
INSERT INTO `auth_permission` VALUES ('32', 'authRole:addRole', '角色添加', '', 'p', 'APP_ROOT', '/auth/authRole/addRole', '角色添加', '1', '');
INSERT INTO `auth_permission` VALUES ('33', 'authUser:loadUpdateData', '用户加载', '', 'p', 'APP_ROOT', '/auth/authUser/loadUpdateData', '用户加载', '1', '');
INSERT INTO `auth_permission` VALUES ('34', 'authUser:updateUser', '用户更新', '', 'p', 'APP_ROOT', '/auth/authUser/updateUser', '用户更新', '1', '');
INSERT INTO `auth_permission` VALUES ('35', 'authUser:setUserStatus', '更新用户状态', '', 'p', 'APP_ROOT', '/auth/authUser/setUserStatus', '更新用户状态', '1', '');
INSERT INTO `auth_permission` VALUES ('36', 'authUser:roleAssign', '用户角色分配', '', 'p', 'APP_ROOT', '/auth/authUser/addRolesToUser', '用户角色分配', '1', '');
INSERT INTO `auth_permission` VALUES ('37', 'authUser:loadAuthUserList', '用户列表', '', 'p', 'APP_ROOT', '/auth/authUser/loadAuthUserList', '用户列表', '1', '');
INSERT INTO `auth_permission` VALUES ('38', 'authUser:resetAuthUserpwd', '重置密码', '', 'p', 'APP_ROOT', '/auth/authUser/resetAuthUserPwd', '重置密码', '1', '');
INSERT INTO `auth_permission` VALUES ('39', 'authUser:permAssign', '用户权限分配', '', 'p', 'APP_ROOT', '/auth/authUser/addPremsToUser', '用户权限分配', '1', '');
INSERT INTO `auth_permission` VALUES ('40', 'authApp:index', '应用管理', '', 'M', 'APP_ROOT', '/auth/application', null, '1', '');
INSERT INTO `auth_permission` VALUES ('41', 'authModule:index', '模块管理', '', 'M', 'APP_ROOT', '/auth/module', '', '1', '');
INSERT INTO `auth_permission` VALUES ('42', 'authMenu:index', '菜单管理', '', 'M', 'APP_ROOT', '/auth/menu', '', '1', '');
INSERT INTO `auth_permission` VALUES ('43', 'authRole:index', '角色管理', '', 'M', 'APP_ROOT', '/auth/role', '', '1', '');
INSERT INTO `auth_permission` VALUES ('44', 'authPermission:index', '权限管理', '', 'M', 'APP_ROOT', '/auth/permission', '', '1', '');
INSERT INTO `auth_permission` VALUES ('45', 'authUser:index', '账户管理', '', 'M', 'APP_ROOT', '/auth/user', '', '1', '');
INSERT INTO `auth_permission` VALUES ('1000', 'zfesDemo:index', '开发示例', '', 'M', 'APP_ROOT', '/view/zfesDemo/zfesDemo.html', '', '1', '');
INSERT INTO `auth_permission` VALUES ('1001', 'zfesDemo:loadZfesDemo', '示例查询', '', 'p', 'APP_ROOT', '/zfesDemo/loadZfesDemo', '角色列表', '1', '');
INSERT INTO `auth_permission` VALUES ('1002', 'zfesDemo:loadZfesDemoList', '示例加载', '', 'p', 'APP_ROOT', '/zfesDemo/loadZfesDemoDataSet', '角色列表', '1', '');
INSERT INTO `auth_permission` VALUES ('1003', 'zfesDemo:addZfesDemo', '示例添加', '', 'P', 'APP_ROOT', '/zfesDemo/addZfesDemo', null, '2', '\0');
INSERT INTO `auth_permission` VALUES ('1004', 'zfesDemo:updateZfesDemo', '示例更新', '', 'P', 'APP_ROOT', '/zfesDemo/updateZfesDemo', null, '2', '\0');
INSERT INTO `auth_permission` VALUES ('1005', 'zfesDemo:deletZfesDemo', '示例删除', '', 'P', 'APP_ROOT', '/zfesDemo/deletZfesDemo', null, '2', '\0');
INSERT INTO `auth_permission` VALUES ('1006', 'category:loadCategory', '加载分类', '', 'p', 'APP_ROOT', '/admin/category/loadCategory', '加载分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1007', 'category:deleteCategory', '删除分类', '', 'p', 'APP_ROOT', '/admin/category/deleteCategory', '删除分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1008', 'category:disableCategory', '禁用分类', '', 'p', 'APP_ROOT', '/admin/category/disableCategory', '禁用分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1009', 'category:createCategory', '创建分类', '', 'p', 'APP_ROOT', '/admin/category/createCategory', '创建分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1010', 'category:loadCategoryTree', '加载分类列表', '', 'p', 'APP_ROOT', '/admin/category/loadCategoryTree', '加载分类列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1011', 'category:enableCategoryAttr', '使能分类', '', 'p', 'APP_ROOT', '/admin/category/enableCategoryAttr', '使能分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1012', 'category:updateCategoryOrderNum', '更新分类顺序', '', 'p', 'APP_ROOT', '/admin/category/updateCategoryOrderNum', '更新分类顺序', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1013', 'consultation:loadConsultation', '加载留言', '', 'p', 'APP_ROOT', '/admin/consultation/loadConsultation', '加载留言', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1014', 'consultation:deleteConsultation', '删除留言', '', 'p', 'APP_ROOT', '/admin/consultation/deleteConsultation', '删除留言', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1015', 'consultation:loadConsultationReply', '加载留言回复', '', 'p', 'APP_ROOT', '/admin/consultation/loadConsultationReply', '加载留言回复', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1016', 'consultation:deleteConsultationReply', '删除留言回复', '', 'p', 'APP_ROOT', '/admin/consultation/deleteConsultationReply', '删除留言回复', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1017', 'dicSysconfig:updateDicSysconfig', '系统参数更新', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/updateDicSysconfig', '系统参数更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1018', 'dicSysconfig:deletDicSysconfig', '系统参数删除', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/deleteDicSysconfig', '系统参数删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1019', 'dicSysconfig:loadDicSysconfigDataSet', '系统参数列表', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/loadDicSysconfigDataSet', '系统参数列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1020', 'dicSysconfig:addDicSysconfig', '系统参数添加', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/addDicSysconfig', '系统参数添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1021', 'dicSysconfig:loadDicSysconfig', '系统参数加载', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/loadDicSysconfig', '系统参数加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1022', 'memberAccountDeposit:loadMemberAccountDepositDataSet', '会员保证金记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountDeposit/loadMemberAccountDepositDataSet', '会员保证金记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1023', 'memberAccountDeposit:loadMemberAccountDeposit', '会员保证金记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountDeposit/loadMemberAccountDeposit', '会员保证金记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1024', 'memberAccountIntro:loadMemberAccountIntroDataSet', '会员推广收益记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountIntro/loadMemberAccountIntroDataSet', '会员推广收益记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1025', 'memberAccountIntro:loadMemberAccountIntro', '会员推广收益记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountIntro/loadMemberAccountIntro', '会员推广收益记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1026', 'memberAccountLog:loadMemberAccountLogDataSet', '会员费用记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountLog/loadMemberAccountLogDataSet', '会员费用记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1027', 'memberAccountLog:loadMemberAccountLog', '会员费用记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountLog/loadMemberAccountLog', '会员费用记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1028', 'memberAccountRecharge:loadMemberAccountRechargeDataSet', '会员充值记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountRecharge/loadMemberAccountRechargeDataSet', '会员充值记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1029', 'memberAccountRecharge:deletMemberAccountRecharge', '会员充值记录删除', '', 'p', 'APP_ROOT', '/admin/member/memberAccountRecharge/deleteMemberAccountRecharge', '会员充值记录删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1030', 'memberAccountRecharge:loadMemberAccountRecharge', '会员充值记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountRecharge/loadMemberAccountRecharge', '会员充值记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1031', 'memberAccountWithdraw:loadMemberAccountWithdrawDataSet', '会员提现记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountWithdrawCash/loadMemberAccountWithdrawCashDataSet', '会员提现记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1032', 'memberAccountWithdraw:loadMemberAccountWithdrawCash', '会员提现记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountWithdrawCash/loadMemberAccountWithdrawCash', '会员提现记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1033', 'member:setMemberStatus', '会员状态设置', '', 'p', 'APP_ROOT', '/admin/member/setMemberStatus', '会员状态设置', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1034', 'member:loadMemberCount', '会员数', '', 'p', 'APP_ROOT', '/admin/member/loadMemberCount', '会员数', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1035', 'member:loadMember', '会员加载', '', 'p', 'APP_ROOT', '/admin/member/loadMember', '会员加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1036', 'member:loadMemberDataSet', '会员列表', '', 'p', 'APP_ROOT', '/admin/member/loadMemberDataSet', '会员列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1037', 'memberRegist:loadMemberRegistDataSet', '会员注册记录', '', 'p', 'APP_ROOT', '/admin/member/memberRegist/loadMemberRegistDataSet', '会员注册记录', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1038', 'memberRegist:loadMemberRegistById', '会员加载', '', 'p', 'APP_ROOT', '/admin/member/memberRegist/loadMemberRegistById', '会员加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1039', 'memberWeiuser:addMemberWeiuser', '会员微信账号信息添加', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/addMemberWeiuser', '会员微信账号信息添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1040', 'memberWeiuser:updateMemberWeiuser', '会员微信账号信息更新', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/updateMemberWeiuser', '会员微信账号信息更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1041', 'memberWeiuser:deletMemberWeiuser', '会员微信账号信息删除', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/deleteMemberWeiuser', '会员微信账号信息删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1042', 'memberWeiuser:loadMemberWeiuserDataSet', '会员微信账号信息列表', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/loadMemberWeiuserDataSet', '会员微信账号信息列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1043', 'memberWeiuser:loadMemberWeiuser', '会员微信账号信息加载', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/loadMemberWeiuser', '会员微信账号信息加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1044', 'orders:setOrdersStatus', '订单状态设置', '', 'p', 'APP_ROOT', '/admin/orders/setOrdersStatus', '订单状态设置', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1045', 'orders:confirmReceipt', '确认收货', '', 'p', 'APP_ROOT', '/admin/orders/confirmReceipt', '确认收货', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1046', 'orders:loadOrdersListCount', '订单数量', '', 'p', 'APP_ROOT', '/admin/orders/loadOrdersListCount', '订单数量', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1047', 'orders:loadOrdersDataSet', '订单列表', '', 'p', 'APP_ROOT', '/admin/orders/loadOrdersDataSet', '订单列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1048', 'ordersStatusLog:loadOrdersStatusLogDataSet', '订单状态记录列表', '', 'p', 'APP_ROOT', '/orders/ordersStatusLog/loadOrdersStatusLogDataSet', '订单状态记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1049', 'ordersStatusLog:loadOrdersStatusLog', '订单状态记录加载', '', 'p', 'APP_ROOT', '/orders/ordersStatusLog/loadOrdersStatusLog', '订单状态记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1050', 'productAdv:loadProductAdv', '商品广告加载', '', 'p', 'APP_ROOT', '/admin/product/productAdv/loadProductAdv', '商品广告加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1051', 'productAdv:deletProductAdv', '商品广告删除', '', 'p', 'APP_ROOT', '/admin/product/productAdv/deleteProductAdv', '商品广告删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1052', 'productAdv:createProductAdv', '商品广告添加', '', 'p', 'APP_ROOT', '/admin/product/productAdv/createProductAdv', '商品广告添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1053', 'productAdv:updateProductAdv', '商品广告更新', '', 'p', 'APP_ROOT', '/admin/product/productAdv/updateProductAdv', '商品广告更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1054', 'productAdv:setProductAdvIsShow', '商品广告是否显示', '', 'p', 'APP_ROOT', '/admin/product/productAdv/setProductAdvIsShow', '商品广告是否显示', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1055', 'productAdv:loadProductAdvDataSet', '商品广告列表', '', 'p', 'APP_ROOT', '/admin/product/productAdv/loadProductAdvDataSet', '商品广告列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1056', 'product:setProductStatus', '商品状态设置', '', 'p', 'APP_ROOT', '/admin/product/product/setProductStatus', '商品状态设置', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1057', 'product:loadProduct', '商品列表', '', 'p', 'APP_ROOT', '/admin/product/product/loadProduct', '商品列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1058', 'product:loadProductListCount', '商品数量', '', 'p', 'APP_ROOT', '/admin/product/product/loadProductListCount', '商品数量', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1059', 'product:deleteProductImage', '商品图片删除', '', 'p', 'APP_ROOT', '/admin/product/product/deleteProductImage', '商品图片删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1060', 'pubArticleImage:loadPubArticleImage', '图片模式的文章加载', '', 'p', 'APP_ROOT', '/admin/pub/pubArticleImage/loadPubArticleImage', '图片模式的文章加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1061', 'pubArticleImage:loadPubArticleImageDataSet', '图片模式的文章列表', '', 'p', 'APP_ROOT', '/admin/pub/pubArticleImage/loadPubArticleImageDataSet', '图片模式的文章列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1062', 'pubArticleImage:createPubArticleImage', '图片模式的文章添加', '', 'p', 'APP_ROOT', '/admin/pub/pubArticleImage/createPubArticleImage', '图片模式的文章添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1063', 'pubSmsLog:addPubSmsLog', '短信记录添加', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/addPubSmsLog', '短信记录添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1064', 'pubSmsLog:loadPubSmsLog', '短信记录加载', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/loadPubSmsLog', '短信记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1065', 'pubSmsLog:updatePubSmsLog', '短信记录更新', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/updatePubSmsLog', '短信记录更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1066', 'pubSmsLog:deletPubSmsLog', '短信记录删除', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/deletePubSmsLog', '短信记录删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1067', 'pubSmsLog:loadPubSmsLogDataSet', '短信记录列表', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/loadPubSmsLogDataSet', '短信记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1068', 'pubTerm:deletPubTerm', '条款删除', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/deletePubTerm', '条款删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1069', 'pubTerm:loadAllPubTerms', '条款列表', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/loadAllPubTerms', '条款列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1070', 'pubTerm:updatePubTerm', '条款更新', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/updatePubTerm', '条款更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1071', 'pubTerm:addPubTerm', '条款添加', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/createPubTerm', '条款添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1072', 'pubTerm:loadPubTerm', '条款加载', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/loadPubTerm', '条款加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1073', 'pubTerm:loadPubTermDataSet', '条款列表', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/loadPubTermDataSet', '条款列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1074', 'shop:loadShop', '店铺加载', '', 'p', 'APP_ROOT', '/admin/shop/shop/loadShop', '店铺加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1075', 'shop:updateShop', '店铺更新', '', 'p', 'APP_ROOT', '/admin/shop/shop/updateShop', '店铺更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1076', 'shop:deletShop', '店铺删除', '', 'p', 'APP_ROOT', '/admin/shop/shop/deleteShop', '店铺删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1077', 'shop:loadShopDataSet', '店铺列表', '', 'p', 'APP_ROOT', '/admin/shop/shop/loadShopDataSet', '店铺列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1078', 'shop:addShop', '店铺添加', '', 'p', 'APP_ROOT', '/admin/shop/shop/addShop', '店铺添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1079', 'menu:dashboard', '数据总览', '', 'M', 'APP_ROOT', '/dashboard', '数据总览', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1082', 'menu:product', '商品菜单', '', 'M', 'APP_ROOT', '/product', '商品菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1083', 'menu:order', '订单菜单', '', 'M', 'APP_ROOT', '/order', '订单菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1084', 'menu:member', '用户菜单', '', 'M', 'APP_ROOT', '/member', '用户菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1085', 'menu:trading', '交易菜单', '', 'M', 'APP_ROOT', '/trading', '交易菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1086', 'menu:content', '内容菜单', '', 'M', 'APP_ROOT', '/content', '内容菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1087', 'menu:setting', '配置菜单', '', 'M', 'APP_ROOT', '/setting', '配置菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1088', 'menu:category', '分类菜单', '', 'M', 'APP_ROOT', '/category', '分类菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1089', 'product:checkProduct', '商品审核', '', 'p', 'APP_ROOT', '/admin/product/product/checkProduct', '商品审核', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1090', 'product:setProductIsHot', '设置热门商品', '', 'p', 'APP_ROOT', '/admin/product/product/setProductIsHot', '设置热门商品', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1091', 'authUser:createAccount', '创建管理员', '', 'p', 'APP_ROOT', '/auth/authUser/createAccount', '创建管理员账号', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1092', 'orders:loadOrdersDetails', '订单详情加载', '', 'p', 'APP_ROOT', '/orders/orders/loadOrdersDetails', '订单详情加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1093', 'product:loadProductDetail', '商品详情', '', 'p', 'APP_ROOT', '/admin/product/product/loadProductDetail', '商品详情', '3', '\0');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleCode` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `enabled` bit(1) NOT NULL DEFAULT b'1',
  `description` varchar(60) DEFAULT NULL,
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属app',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_role_index_code` (`roleCode`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES ('1', 'ROOT', 'ROOT', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('2', 'ADMIN', '管理员', '', '管理员', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('3', 'BUYER', '买家', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('4', 'SELLER', '卖家', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('5', 'CUSTOM_SERVICE', '客服', '', '客服', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('6', 'MEMBER', '会员', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('8', 'baba', '爸爸', '', '爸爸', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('9', 'mama', '妈妈', '', '妈妈', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('13', '22', '22222223333333', '', '22', 'APP_ROOT');

-- ----------------------------
-- Table structure for auth_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_permission`;
CREATE TABLE `auth_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth_permission_id` bigint(20) NOT NULL,
  `auth_role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_role_permission_index_permission_id` (`auth_permission_id`),
  KEY `auth_role_permission_index_role_id` (`auth_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_role_permission
-- ----------------------------
INSERT INTO `auth_role_permission` VALUES ('1', '4', '1');
INSERT INTO `auth_role_permission` VALUES ('2', '5', '1');
INSERT INTO `auth_role_permission` VALUES ('3', '40', '1');
INSERT INTO `auth_role_permission` VALUES ('4', '1', '1');
INSERT INTO `auth_role_permission` VALUES ('5', '3', '1');
INSERT INTO `auth_role_permission` VALUES ('6', '6', '1');
INSERT INTO `auth_role_permission` VALUES ('7', '2', '1');
INSERT INTO `auth_role_permission` VALUES ('8', '12', '1');
INSERT INTO `auth_role_permission` VALUES ('9', '42', '1');
INSERT INTO `auth_role_permission` VALUES ('10', '8', '1');
INSERT INTO `auth_role_permission` VALUES ('11', '7', '1');
INSERT INTO `auth_role_permission` VALUES ('12', '11', '1');
INSERT INTO `auth_role_permission` VALUES ('13', '10', '1');
INSERT INTO `auth_role_permission` VALUES ('14', '13', '1');
INSERT INTO `auth_role_permission` VALUES ('15', '9', '1');
INSERT INTO `auth_role_permission` VALUES ('16', '16', '1');
INSERT INTO `auth_role_permission` VALUES ('17', '17', '1');
INSERT INTO `auth_role_permission` VALUES ('18', '41', '1');
INSERT INTO `auth_role_permission` VALUES ('19', '15', '1');
INSERT INTO `auth_role_permission` VALUES ('20', '18', '1');
INSERT INTO `auth_role_permission` VALUES ('21', '14', '1');
INSERT INTO `auth_role_permission` VALUES ('22', '21', '1');
INSERT INTO `auth_role_permission` VALUES ('23', '23', '1');
INSERT INTO `auth_role_permission` VALUES ('24', '25', '1');
INSERT INTO `auth_role_permission` VALUES ('25', '44', '1');
INSERT INTO `auth_role_permission` VALUES ('26', '24', '1');
INSERT INTO `auth_role_permission` VALUES ('27', '19', '1');
INSERT INTO `auth_role_permission` VALUES ('28', '22', '1');
INSERT INTO `auth_role_permission` VALUES ('29', '20', '1');
INSERT INTO `auth_role_permission` VALUES ('30', '32', '1');
INSERT INTO `auth_role_permission` VALUES ('31', '28', '1');
INSERT INTO `auth_role_permission` VALUES ('32', '43', '1');
INSERT INTO `auth_role_permission` VALUES ('33', '27', '1');
INSERT INTO `auth_role_permission` VALUES ('34', '29', '1');
INSERT INTO `auth_role_permission` VALUES ('35', '30', '1');
INSERT INTO `auth_role_permission` VALUES ('36', '31', '1');
INSERT INTO `auth_role_permission` VALUES ('37', '26', '1');
INSERT INTO `auth_role_permission` VALUES ('38', '45', '1');
INSERT INTO `auth_role_permission` VALUES ('39', '37', '1');
INSERT INTO `auth_role_permission` VALUES ('40', '33', '1');
INSERT INTO `auth_role_permission` VALUES ('41', '39', '1');
INSERT INTO `auth_role_permission` VALUES ('42', '38', '1');
INSERT INTO `auth_role_permission` VALUES ('43', '36', '1');
INSERT INTO `auth_role_permission` VALUES ('44', '35', '1');
INSERT INTO `auth_role_permission` VALUES ('45', '34', '1');
INSERT INTO `auth_role_permission` VALUES ('46', '1000', '1');
INSERT INTO `auth_role_permission` VALUES ('47', '1001', '1');
INSERT INTO `auth_role_permission` VALUES ('48', '1002', '1');
INSERT INTO `auth_role_permission` VALUES ('49', '1003', '1');
INSERT INTO `auth_role_permission` VALUES ('50', '1004', '1');
INSERT INTO `auth_role_permission` VALUES ('51', '1005', '1');
INSERT INTO `auth_role_permission` VALUES ('53', '1006', '1');
INSERT INTO `auth_role_permission` VALUES ('54', '1007', '1');
INSERT INTO `auth_role_permission` VALUES ('55', '1008', '1');
INSERT INTO `auth_role_permission` VALUES ('56', '1009', '1');
INSERT INTO `auth_role_permission` VALUES ('57', '1010', '1');
INSERT INTO `auth_role_permission` VALUES ('58', '1011', '1');
INSERT INTO `auth_role_permission` VALUES ('59', '1012', '1');
INSERT INTO `auth_role_permission` VALUES ('60', '1013', '1');
INSERT INTO `auth_role_permission` VALUES ('61', '1014', '1');
INSERT INTO `auth_role_permission` VALUES ('62', '1015', '1');
INSERT INTO `auth_role_permission` VALUES ('63', '1016', '1');
INSERT INTO `auth_role_permission` VALUES ('64', '1017', '1');
INSERT INTO `auth_role_permission` VALUES ('65', '1018', '1');
INSERT INTO `auth_role_permission` VALUES ('66', '1019', '1');
INSERT INTO `auth_role_permission` VALUES ('67', '1020', '1');
INSERT INTO `auth_role_permission` VALUES ('68', '1021', '1');
INSERT INTO `auth_role_permission` VALUES ('69', '1022', '1');
INSERT INTO `auth_role_permission` VALUES ('70', '1023', '1');
INSERT INTO `auth_role_permission` VALUES ('71', '1024', '1');
INSERT INTO `auth_role_permission` VALUES ('72', '1025', '1');
INSERT INTO `auth_role_permission` VALUES ('73', '1026', '1');
INSERT INTO `auth_role_permission` VALUES ('74', '1027', '1');
INSERT INTO `auth_role_permission` VALUES ('75', '1028', '1');
INSERT INTO `auth_role_permission` VALUES ('76', '1029', '1');
INSERT INTO `auth_role_permission` VALUES ('77', '1030', '1');
INSERT INTO `auth_role_permission` VALUES ('78', '1031', '1');
INSERT INTO `auth_role_permission` VALUES ('79', '1032', '1');
INSERT INTO `auth_role_permission` VALUES ('80', '1033', '1');
INSERT INTO `auth_role_permission` VALUES ('81', '1034', '1');
INSERT INTO `auth_role_permission` VALUES ('82', '1035', '1');
INSERT INTO `auth_role_permission` VALUES ('83', '1036', '1');
INSERT INTO `auth_role_permission` VALUES ('84', '1037', '1');
INSERT INTO `auth_role_permission` VALUES ('85', '1038', '1');
INSERT INTO `auth_role_permission` VALUES ('86', '1039', '1');
INSERT INTO `auth_role_permission` VALUES ('87', '1040', '1');
INSERT INTO `auth_role_permission` VALUES ('88', '1041', '1');
INSERT INTO `auth_role_permission` VALUES ('89', '1042', '1');
INSERT INTO `auth_role_permission` VALUES ('90', '1043', '1');
INSERT INTO `auth_role_permission` VALUES ('91', '1044', '1');
INSERT INTO `auth_role_permission` VALUES ('92', '1045', '1');
INSERT INTO `auth_role_permission` VALUES ('93', '1046', '1');
INSERT INTO `auth_role_permission` VALUES ('94', '1047', '1');
INSERT INTO `auth_role_permission` VALUES ('95', '1048', '1');
INSERT INTO `auth_role_permission` VALUES ('96', '1049', '1');
INSERT INTO `auth_role_permission` VALUES ('97', '1050', '1');
INSERT INTO `auth_role_permission` VALUES ('98', '1051', '1');
INSERT INTO `auth_role_permission` VALUES ('99', '1052', '1');
INSERT INTO `auth_role_permission` VALUES ('100', '1053', '1');
INSERT INTO `auth_role_permission` VALUES ('101', '1054', '1');
INSERT INTO `auth_role_permission` VALUES ('102', '1055', '1');
INSERT INTO `auth_role_permission` VALUES ('103', '1056', '1');
INSERT INTO `auth_role_permission` VALUES ('104', '1057', '1');
INSERT INTO `auth_role_permission` VALUES ('105', '1058', '1');
INSERT INTO `auth_role_permission` VALUES ('106', '1059', '1');
INSERT INTO `auth_role_permission` VALUES ('107', '1060', '1');
INSERT INTO `auth_role_permission` VALUES ('108', '1061', '1');
INSERT INTO `auth_role_permission` VALUES ('109', '1062', '1');
INSERT INTO `auth_role_permission` VALUES ('110', '1063', '1');
INSERT INTO `auth_role_permission` VALUES ('111', '1064', '1');
INSERT INTO `auth_role_permission` VALUES ('112', '1065', '1');
INSERT INTO `auth_role_permission` VALUES ('113', '1066', '1');
INSERT INTO `auth_role_permission` VALUES ('114', '1067', '1');
INSERT INTO `auth_role_permission` VALUES ('115', '1068', '1');
INSERT INTO `auth_role_permission` VALUES ('116', '1069', '1');
INSERT INTO `auth_role_permission` VALUES ('117', '1070', '1');
INSERT INTO `auth_role_permission` VALUES ('118', '1071', '1');
INSERT INTO `auth_role_permission` VALUES ('119', '1072', '1');
INSERT INTO `auth_role_permission` VALUES ('120', '1073', '1');
INSERT INTO `auth_role_permission` VALUES ('121', '1074', '1');
INSERT INTO `auth_role_permission` VALUES ('122', '1075', '1');
INSERT INTO `auth_role_permission` VALUES ('123', '1076', '1');
INSERT INTO `auth_role_permission` VALUES ('124', '1077', '1');
INSERT INTO `auth_role_permission` VALUES ('125', '1078', '1');
INSERT INTO `auth_role_permission` VALUES ('126', '1079', '1');
INSERT INTO `auth_role_permission` VALUES ('127', '1082', '1');
INSERT INTO `auth_role_permission` VALUES ('128', '1083', '1');
INSERT INTO `auth_role_permission` VALUES ('129', '1084', '1');
INSERT INTO `auth_role_permission` VALUES ('130', '1085', '1');
INSERT INTO `auth_role_permission` VALUES ('131', '1086', '1');
INSERT INTO `auth_role_permission` VALUES ('132', '1087', '1');
INSERT INTO `auth_role_permission` VALUES ('133', '1044', '5');
INSERT INTO `auth_role_permission` VALUES ('134', '1045', '5');
INSERT INTO `auth_role_permission` VALUES ('135', '1046', '5');
INSERT INTO `auth_role_permission` VALUES ('136', '1047', '5');
INSERT INTO `auth_role_permission` VALUES ('137', '1048', '5');
INSERT INTO `auth_role_permission` VALUES ('138', '1049', '5');
INSERT INTO `auth_role_permission` VALUES ('139', '1050', '5');
INSERT INTO `auth_role_permission` VALUES ('140', '1051', '5');
INSERT INTO `auth_role_permission` VALUES ('141', '1052', '5');
INSERT INTO `auth_role_permission` VALUES ('142', '1053', '5');
INSERT INTO `auth_role_permission` VALUES ('143', '1054', '5');
INSERT INTO `auth_role_permission` VALUES ('144', '1055', '5');
INSERT INTO `auth_role_permission` VALUES ('145', '1056', '5');
INSERT INTO `auth_role_permission` VALUES ('146', '1057', '5');
INSERT INTO `auth_role_permission` VALUES ('147', '1058', '5');
INSERT INTO `auth_role_permission` VALUES ('148', '1059', '5');
INSERT INTO `auth_role_permission` VALUES ('158', '1082', '5');
INSERT INTO `auth_role_permission` VALUES ('159', '1083', '5');
INSERT INTO `auth_role_permission` VALUES ('160', '1089', '1');
INSERT INTO `auth_role_permission` VALUES ('161', '1088', '1');
INSERT INTO `auth_role_permission` VALUES ('162', '1090', '1');
INSERT INTO `auth_role_permission` VALUES ('163', '1091', '1');
INSERT INTO `auth_role_permission` VALUES ('165', '7', '13');
INSERT INTO `auth_role_permission` VALUES ('166', '8', '13');
INSERT INTO `auth_role_permission` VALUES ('169', '1', '13');
INSERT INTO `auth_role_permission` VALUES ('171', '1092', '1');
INSERT INTO `auth_role_permission` VALUES ('172', '1093', '1');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `account` varchar(30) NOT NULL COMMENT '用户名',
  `password` varchar(36) NOT NULL COMMENT '密码',
  `salt` varchar(60) NOT NULL COMMENT '加密盐值',
  `openId` varchar(60) DEFAULT '' COMMENT '微信openid',
  `authLevel` int(1) DEFAULT '2' COMMENT '用户状态_D#0游客(未注册) ，1：注册但未审核，2：正常用户#',
  `defaultRole` bigint(20) DEFAULT NULL COMMENT '默认角色',
  `defaultOrg` varchar(50) DEFAULT NULL COMMENT '默认组织机构',
  `defaultPosition` bigint(20) DEFAULT NULL COMMENT '默认职位',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '用户状态',
  `accountExpired` bit(1) NOT NULL DEFAULT b'0' COMMENT '账户是否过期',
  `accountLocked` bit(1) NOT NULL DEFAULT b'0' COMMENT '账户是否所定',
  `credentialsExpired` bit(1) NOT NULL DEFAULT b'0' COMMENT '密码是否过期',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `perName` varchar(80) DEFAULT NULL COMMENT '姓名',
  `perIdCardNum` varchar(20) DEFAULT NULL COMMENT '证件号码',
  `weichat` varchar(40) DEFAULT NULL COMMENT '微信帐号',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `phoneNum` varchar(15) DEFAULT NULL COMMENT '手机',
  `qqNum` varchar(10) DEFAULT NULL COMMENT 'QQ',
  `weiboNum` varchar(50) DEFAULT NULL COMMENT 'MSN',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_index_user_name` (`account`) USING BTREE,
  KEY `auth_user_index_password` (`password`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('1', 'admin', 'e61aab677ca46224d22acbb493247d69', 'a767ec06b36de25389055541a23f630b', '', '2', '1', '10427A08A', '26', '', '\0', '\0', '', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('2', 'test001', '1d958b725884ad926c16e21dc0d8ccaa', '13c468d7fc78a228ceb9cc952dbe176f', '', '2', '15', null, null, '', '\0', '\0', '\0', null, '2017-01-20 10:11:03', null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('3', 'test002', '036927c9c52ea8b13f29cf9c36094d20', '285641464548957Rrrydj4QGiSmyS3CzCSu1L', '', '2', '15', null, null, '', '\0', '\0', '\0', null, '2017-01-20 16:45:39', null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('54', 'zlj', '16e1863f84dab273db4bbb68897e0594', '533be486497e9c6e8ac83dcc4a14d79a', 'o_ZQ4wM5yYa4xRDbyBmh2ESBALmg', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('55', 'quhao', 'd35e1f791f2b00be18b342695154110f', '62dc2ba97bed66278e41cfb15acd9514', 'o_ZQ4wOXyTwTUhMpNbstXwQfEExs', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('56', '中', '3c316ff7a201e55d709bbc518e0c1c10', '552336b1c387baa61949f31d55e84da8', 'o_ZQ4wDp1trl53_La8h-puQywcF4', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('57', '15588873006', 'a3094bb3d994c202adb114e359ccfdcb', 'd0cc118eef92fe31de101c30de1326ba', 'o_ZQ4wEnnbeGa55Y6iuGVZSZwVFA', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for auth_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_permission`;
CREATE TABLE `auth_user_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth_permission_id` bigint(20) NOT NULL,
  `auth_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_user_permission_index_auth_permission_id` (`auth_permission_id`),
  KEY `auth_user_permission_index_auth_user_id` (`auth_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_permission
-- ----------------------------
INSERT INTO `auth_user_permission` VALUES ('1', '0', '1');
INSERT INTO `auth_user_permission` VALUES ('2', '1', '1');
INSERT INTO `auth_user_permission` VALUES ('3', '2', '1');
INSERT INTO `auth_user_permission` VALUES ('4', '3', '1');
INSERT INTO `auth_user_permission` VALUES ('5', '4', '1');
INSERT INTO `auth_user_permission` VALUES ('6', '5', '1');
INSERT INTO `auth_user_permission` VALUES ('7', '6', '1');
INSERT INTO `auth_user_permission` VALUES ('8', '7', '1');
INSERT INTO `auth_user_permission` VALUES ('9', '8', '1');
INSERT INTO `auth_user_permission` VALUES ('10', '9', '1');
INSERT INTO `auth_user_permission` VALUES ('11', '10', '1');

-- ----------------------------
-- Table structure for auth_user_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_role`;
CREATE TABLE `auth_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth_role_id` bigint(20) NOT NULL,
  `auth_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_user_role_index_auth_role_id` (`auth_role_id`) USING BTREE,
  KEY `auth_user_role_index_auth_user_id` (`auth_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_role
-- ----------------------------
INSERT INTO `auth_user_role` VALUES ('5', '1', '1');

-- ----------------------------
-- Table structure for auth_weicert
-- ----------------------------
DROP TABLE IF EXISTS `auth_weicert`;
CREATE TABLE `auth_weicert` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(255) DEFAULT NULL COMMENT '应用ID',
  `cert_file` blob COMMENT '蜜月文件',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_weicert
-- ----------------------------
INSERT INTO `auth_weicert` VALUES ('1', 'wx0dd16298bc16ed63', null, '2016-12-29 10:15:45', '2016-12-29 11:10:59');
INSERT INTO `auth_weicert` VALUES ('2', 'wxffdd5a3f3bd46c78', null, '2017-03-11 10:40:10', '2017-03-11 10:40:10');

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_bin DEFAULT '' COMMENT '品牌名称_Q@like@',
  `logo` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '公司图片',
  `description` text COLLATE utf8_bin COMMENT '品牌描述',
  `siteUrl` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '品牌网址',
  `orderNum` int(11) DEFAULT '0' COMMENT '排序',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', '保时捷', '', null, '', '0', null, null, '1');
INSERT INTO `brand` VALUES ('2', 'wwwwwwwwwwwwww', 'wwwwwwwwwwwwww', 0x7777777777777777777777777777, 'wwwwwwwwwwwwww', '323', null, '2017-08-19 13:18:09', '1');
INSERT INTO `brand` VALUES ('3', 'brand1', 'brand1', 0x6272616E64316272616E64316272616E6431, 'brand1', '1', '2017-08-19 13:06:44', '2017-08-19 13:06:44', '1');

-- ----------------------------
-- Table structure for brand_category
-- ----------------------------
DROP TABLE IF EXISTS `brand_category`;
CREATE TABLE `brand_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brandId` bigint(20) NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  `status` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '状态_D#0禁用，1:正常2：删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of brand_category
-- ----------------------------
INSERT INTO `brand_category` VALUES ('1', '1', '1', '0');
INSERT INTO `brand_category` VALUES ('2', '2', '1', '1');

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键_id',
  `buyerId` bigint(20) NOT NULL COMMENT '会员',
  `shopId` bigint(20) DEFAULT NULL,
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `specificationValue` varchar(255) NOT NULL DEFAULT '',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '数量',
  `createTime` datetime DEFAULT NULL COMMENT '创建人',
  `updateTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除 3已购买,4已过期#',
  PRIMARY KEY (`id`),
  KEY `memberId` (`buyerId`),
  KEY `productId` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车项';

-- ----------------------------
-- Records of cart
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称_Q@like@',
  `imgPath` varchar(255) DEFAULT '' COMMENT '图片路径',
  `parentId` bigint(20) DEFAULT NULL COMMENT '上级分类_Q@=@',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#_Q@=@',
  `treePath` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1B7971ADFBDD5B73` (`parentId`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='商品分类';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('0', '根属性分类(不可删除)', '', null, null, null, null, '1', '');
INSERT INTO `category` VALUES ('1', '书法', null, '0', '3', null, null, '1', '0_');
INSERT INTO `category` VALUES ('2', '国画', null, '0', '7', null, null, '1', '0_');
INSERT INTO `category` VALUES ('3', '油画', null, '0', '4', null, null, '1', '0_');
INSERT INTO `category` VALUES ('4', '水墨', null, '0', '6', null, null, '1', '0_');
INSERT INTO `category` VALUES ('5', '版画', null, '0', '2', null, null, '1', '0_');
INSERT INTO `category` VALUES ('6', '水彩', null, '0', '5', null, null, '1', '0_');
INSERT INTO `category` VALUES ('7', '其它', '', '0', '1', null, null, '1', '0_');

-- ----------------------------
-- Table structure for category_attr
-- ----------------------------
DROP TABLE IF EXISTS `category_attr`;
CREATE TABLE `category_attr` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `categoryId` bigint(20) NOT NULL COMMENT '所属分类_Q@=@',
  `attrName` varchar(60) NOT NULL DEFAULT '' COMMENT '属性名_Q@like@',
  `orderNum` int(11) DEFAULT '0' COMMENT '排序',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#_Q@=@',
  PRIMARY KEY (`id`),
  KEY `i_ca_attr_cid` (`categoryId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='商品模型属性值表';

-- ----------------------------
-- Records of category_attr
-- ----------------------------
INSERT INTO `category_attr` VALUES ('1', '2', '尺寸', '0', null, null, '1');
INSERT INTO `category_attr` VALUES ('2', '7', '操作系统', '0', null, null, '0');
INSERT INTO `category_attr` VALUES ('3', '7', '网络类型', '0', null, null, '0');

-- ----------------------------
-- Table structure for category_attr_value
-- ----------------------------
DROP TABLE IF EXISTS `category_attr_value`;
CREATE TABLE `category_attr_value` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `categoryId` bigint(20) unsigned NOT NULL COMMENT '所属分类_Q@=@',
  `categoryName` varchar(255) DEFAULT NULL,
  `attrId` bigint(20) unsigned NOT NULL COMMENT '所属属性_Q@=@',
  `attrName` varchar(255) DEFAULT NULL,
  `attrValue` varchar(100) NOT NULL DEFAULT '' COMMENT '属性值_Q@=@',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#_Q@=@',
  PRIMARY KEY (`id`),
  KEY `i_categoryId` (`categoryId`),
  KEY `i_attr` (`attrId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='商品模型属性值表';

-- ----------------------------
-- Records of category_attr_value
-- ----------------------------
INSERT INTO `category_attr_value` VALUES ('1', '1', null, '1', null, '3.0英寸一下', '2017-07-06 14:29:03', null, '0');
INSERT INTO `category_attr_value` VALUES ('2', '1', null, '1', null, '3.0-3.9英寸', '2017-07-15 14:13:43', null, '0');
INSERT INTO `category_attr_value` VALUES ('3', '1', null, '1', null, '4.0-4.5英寸', null, null, '0');
INSERT INTO `category_attr_value` VALUES ('4', '1', null, '1', null, '4.6-4.9英寸', null, null, '0');
INSERT INTO `category_attr_value` VALUES ('5', '1', null, '2', null, 'Android/安卓', null, null, '0');
INSERT INTO `category_attr_value` VALUES ('6', '1', null, '2', null, 'iOS', null, null, '0');
INSERT INTO `category_attr_value` VALUES ('7', '1', null, '2', null, '阿里手机系统', null, null, '0');
INSERT INTO `category_attr_value` VALUES ('8', '1', null, '3', null, '移动4G/联通4G/电信4G', null, null, '0');
INSERT INTO `category_attr_value` VALUES ('9', '1', null, '3', null, '联通4G', null, null, '0');

-- ----------------------------
-- Table structure for consultation
-- ----------------------------
DROP TABLE IF EXISTS `consultation`;
CREATE TABLE `consultation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `buyerId` bigint(20) NOT NULL COMMENT '评价会员',
  `productId` bigint(20) NOT NULL COMMENT '订单id',
  `content` varchar(800) NOT NULL DEFAULT '' COMMENT '内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `isAnonymous` varchar(1) NOT NULL DEFAULT '0' COMMENT '是否匿名',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `FK9B6005777C62EDF8` (`buyerId`),
  KEY `orderId` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of consultation
-- ----------------------------
INSERT INTO `consultation` VALUES ('1', '1', '4', '1509', '看起来不错', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('2', '1', '4', '1509', '66666', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('3', '1', '4', '1509', '4444', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('4', '10', '10', '1523', '全文', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('5', '10', '10', '1523', '全文', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('6', '1', '42', '1509', '不', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('7', '1', '42', '1509', '2', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('8', '1', '45', '1509', '看好他', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('9', '1', '51', '1509', 'A', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('10', '54', '54', '4', '。大家觉得就是就是', null, null, '0', '1');
INSERT INTO `consultation` VALUES ('11', '56', '56', '9', '甲', null, null, '0', '1');

-- ----------------------------
-- Table structure for consultation_reply
-- ----------------------------
DROP TABLE IF EXISTS `consultation_reply`;
CREATE TABLE `consultation_reply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `consultationId` bigint(20) NOT NULL COMMENT '评价id',
  `sellerId` bigint(20) DEFAULT NULL COMMENT '卖家',
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `content` varchar(800) NOT NULL COMMENT '回复内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `FK9B6005777C62EDF8` (`consultationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of consultation_reply
-- ----------------------------

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sellerId` bigint(20) DEFAULT NULL,
  `fullCutName` varchar(255) DEFAULT NULL COMMENT '满减送活动名称',
  `fullCutType` varchar(1) DEFAULT NULL COMMENT '满减活动类型_D#1,店内所有商品适用，2，部分商品适用#',
  `startDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '活动开始时间',
  `endDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '活动结束时间',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='满减送';

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('3', '1', '满300包邮', null, '2017-03-12 00:00:00', '2017-03-19 00:00:00', null, null, '');

-- ----------------------------
-- Table structure for coupon_product
-- ----------------------------
DROP TABLE IF EXISTS `coupon_product`;
CREATE TABLE `coupon_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullCutId` bigint(20) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon_product
-- ----------------------------
INSERT INTO `coupon_product` VALUES ('4', '3', '6');

-- ----------------------------
-- Table structure for coupon_set
-- ----------------------------
DROP TABLE IF EXISTS `coupon_set`;
CREATE TABLE `coupon_set` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullCutId` bigint(20) DEFAULT NULL COMMENT '满减送活动id',
  `meetLine` bigint(20) DEFAULT NULL COMMENT '满多少元',
  `isDiscountCash` varchar(1) DEFAULT '0' COMMENT '是否减现金_D# 0 不减 1 减#',
  `discountCash` bigint(20) DEFAULT NULL COMMENT '减现金数',
  `isPostage` varchar(1) DEFAULT '0' COMMENT '是否包邮_D# 0 不包邮 1 包邮#',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon_set
-- ----------------------------
INSERT INTO `coupon_set` VALUES ('3', '3', '300', '1', '30', '1', null, null, '');
INSERT INTO `coupon_set` VALUES ('4', '3', '100', '1', '10', '0', null, null, '');
INSERT INTO `coupon_set` VALUES ('5', '3', '200', '1', '20', '0', null, null, '');

-- ----------------------------
-- Table structure for delivery_set
-- ----------------------------
DROP TABLE IF EXISTS `delivery_set`;
CREATE TABLE `delivery_set` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `templateId` bigint(20) NOT NULL COMMENT '运费模板',
  `areaId` varchar(255) DEFAULT '' COMMENT '地区id',
  `standardId` bigint(20) NOT NULL COMMENT '首费(￥)',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='运费设置';

-- ----------------------------
-- Records of delivery_set
-- ----------------------------
INSERT INTO `delivery_set` VALUES ('13', '5', '4,5,6,7,11,12,13,14,15', '13', null, null);
INSERT INTO `delivery_set` VALUES ('14', '5', '1029,1039,1048,1056,1063,1070', '14', null, null);
INSERT INTO `delivery_set` VALUES ('15', '6', '4,5,6,7,8,9,10,11,12,13,14,15,16,17', '3', null, null);
INSERT INTO `delivery_set` VALUES ('16', '7', '1029,1039,1048,1056,1063,1070', '5', null, null);
INSERT INTO `delivery_set` VALUES ('17', '7', '36,60,75,83,103,123,149,167,179,196,207', '5', null, null);

-- ----------------------------
-- Table structure for delivery_standard
-- ----------------------------
DROP TABLE IF EXISTS `delivery_standard`;
CREATE TABLE `delivery_standard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `templateId` bigint(20) NOT NULL COMMENT '运费模板',
  `startStandard` int(20) NOT NULL COMMENT '首N 件或克',
  `startFee` bigint(20) NOT NULL COMMENT '首费(￥)',
  `addStandard` int(20) NOT NULL COMMENT '续M 件或克',
  `addFee` bigint(20) NOT NULL COMMENT '续费(￥)',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='运费设置';

-- ----------------------------
-- Records of delivery_standard
-- ----------------------------
INSERT INTO `delivery_standard` VALUES ('13', '5', '20', '20', '20', '20', null, '2017-08-16 21:49:53');
INSERT INTO `delivery_standard` VALUES ('14', '5', '4', '5', '6', '7', null, null);
INSERT INTO `delivery_standard` VALUES ('15', '6', '1', '3', '7', '9', null, null);
INSERT INTO `delivery_standard` VALUES ('17', '7', '3', '5', '3', '2', null, null);
INSERT INTO `delivery_standard` VALUES ('18', '6', '2', '23', '23', '23', null, null);
INSERT INTO `delivery_standard` VALUES ('19', '6', '2', '23', '23', '23', '2017-08-16 21:38:32', '2017-08-16 21:38:32');

-- ----------------------------
-- Table structure for delivery_template
-- ----------------------------
DROP TABLE IF EXISTS `delivery_template`;
CREATE TABLE `delivery_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sellerId` bigint(20) NOT NULL COMMENT '卖家',
  `shopId` bigint(20) NOT NULL COMMENT '卖家',
  `name` varchar(60) NOT NULL COMMENT '模板名称',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#_Q@=@',
  `valuationType` varchar(1) DEFAULT NULL COMMENT '计费方式_D#1按件,2按重量#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='运费模板';

-- ----------------------------
-- Records of delivery_template
-- ----------------------------
INSERT INTO `delivery_template` VALUES ('5', '1', '0', 'xxxxxxxxxx', null, null, '1', '1');
INSERT INTO `delivery_template` VALUES ('6', '1', '0', '测试运费1', null, null, '1', '2');
INSERT INTO `delivery_template` VALUES ('7', '1', '0', '计件模板1', null, null, '1', '1');
INSERT INTO `delivery_template` VALUES ('8', '1', '1', '模板xxx', null, null, '2', '1');

-- ----------------------------
-- Table structure for dic_areas
-- ----------------------------
DROP TABLE IF EXISTS `dic_areas`;
CREATE TABLE `dic_areas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `code` varchar(20) DEFAULT '' COMMENT '地区编码',
  `name` varchar(40) DEFAULT '' COMMENT '地区名',
  `type` varchar(1) DEFAULT '' COMMENT '地区类型_D#P:省,C:市，A:县#',
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_code` (`code`) USING BTREE,
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3510 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dic_areas
-- ----------------------------
INSERT INTO `dic_areas` VALUES ('1', '110000', '北京市', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2', '110100', '市辖区', 'C', '1');
INSERT INTO `dic_areas` VALUES ('3', '110101', '东城区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('4', '110102', '西城区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('5', '110105', '朝阳区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('6', '110106', '丰台区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('7', '110107', '石景山区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('8', '110108', '海淀区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('9', '110109', '门头沟区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('10', '110111', '房山区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('11', '110112', '通州区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('12', '110113', '顺义区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('13', '110114', '昌平区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('14', '110115', '大兴区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('15', '110116', '怀柔区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('16', '110117', '平谷区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('17', '110118', '密云区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('18', '110119', '延庆区', 'A', '2');
INSERT INTO `dic_areas` VALUES ('19', '120000', '天津市', 'P', '0');
INSERT INTO `dic_areas` VALUES ('20', '120100', '市辖区', 'C', '19');
INSERT INTO `dic_areas` VALUES ('21', '120101', '和平区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('22', '120102', '河东区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('23', '120103', '河西区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('24', '120104', '南开区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('25', '120105', '河北区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('26', '120106', '红桥区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('27', '120110', '东丽区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('28', '120111', '西青区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('29', '120112', '津南区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('30', '120113', '北辰区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('31', '120114', '武清区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('32', '120115', '宝坻区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('33', '120116', '滨海新区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('34', '120117', '宁河区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('35', '120118', '静海区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('36', '120119', '蓟州区', 'A', '20');
INSERT INTO `dic_areas` VALUES ('37', '130000', '河北省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('38', '130100', '石家庄市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('39', '130101', '市辖区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('40', '130102', '长安区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('41', '130104', '桥西区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('42', '130105', '新华区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('43', '130107', '井陉矿区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('44', '130108', '裕华区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('45', '130109', '藁城区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('46', '130110', '鹿泉区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('47', '130111', '栾城区', 'A', '38');
INSERT INTO `dic_areas` VALUES ('48', '130121', '井陉县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('49', '130123', '正定县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('50', '130125', '行唐县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('51', '130126', '灵寿县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('52', '130127', '高邑县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('53', '130128', '深泽县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('54', '130129', '赞皇县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('55', '130130', '无极县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('56', '130131', '平山县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('57', '130132', '元氏县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('58', '130133', '赵县', 'A', '38');
INSERT INTO `dic_areas` VALUES ('59', '130183', '晋州市', 'A', '38');
INSERT INTO `dic_areas` VALUES ('60', '130184', '新乐市', 'A', '38');
INSERT INTO `dic_areas` VALUES ('61', '130200', '唐山市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('62', '130201', '市辖区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('63', '130202', '路南区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('64', '130203', '路北区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('65', '130204', '古冶区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('66', '130205', '开平区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('67', '130207', '丰南区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('68', '130208', '丰润区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('69', '130209', '曹妃甸区', 'A', '61');
INSERT INTO `dic_areas` VALUES ('70', '130223', '滦县', 'A', '61');
INSERT INTO `dic_areas` VALUES ('71', '130224', '滦南县', 'A', '61');
INSERT INTO `dic_areas` VALUES ('72', '130225', '乐亭县', 'A', '61');
INSERT INTO `dic_areas` VALUES ('73', '130227', '迁西县', 'A', '61');
INSERT INTO `dic_areas` VALUES ('74', '130229', '玉田县', 'A', '61');
INSERT INTO `dic_areas` VALUES ('75', '130281', '遵化市', 'A', '61');
INSERT INTO `dic_areas` VALUES ('76', '130283', '迁安市', 'A', '61');
INSERT INTO `dic_areas` VALUES ('77', '130300', '秦皇岛市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('78', '130301', '市辖区', 'A', '77');
INSERT INTO `dic_areas` VALUES ('79', '130302', '海港区', 'A', '77');
INSERT INTO `dic_areas` VALUES ('80', '130303', '山海关区', 'A', '77');
INSERT INTO `dic_areas` VALUES ('81', '130304', '北戴河区', 'A', '77');
INSERT INTO `dic_areas` VALUES ('82', '130306', '抚宁区', 'A', '77');
INSERT INTO `dic_areas` VALUES ('83', '130321', '青龙满族自治县', 'A', '77');
INSERT INTO `dic_areas` VALUES ('84', '130322', '昌黎县', 'A', '77');
INSERT INTO `dic_areas` VALUES ('85', '130324', '卢龙县', 'A', '77');
INSERT INTO `dic_areas` VALUES ('86', '130400', '邯郸市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('87', '130401', '市辖区', 'A', '86');
INSERT INTO `dic_areas` VALUES ('88', '130402', '邯山区', 'A', '86');
INSERT INTO `dic_areas` VALUES ('89', '130403', '丛台区', 'A', '86');
INSERT INTO `dic_areas` VALUES ('90', '130404', '复兴区', 'A', '86');
INSERT INTO `dic_areas` VALUES ('91', '130406', '峰峰矿区', 'A', '86');
INSERT INTO `dic_areas` VALUES ('92', '130421', '邯郸县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('93', '130423', '临漳县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('94', '130424', '成安县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('95', '130425', '大名县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('96', '130426', '涉县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('97', '130427', '磁县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('98', '130428', '肥乡县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('99', '130429', '永年县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('100', '130430', '邱县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('101', '130431', '鸡泽县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('102', '130432', '广平县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('103', '130433', '馆陶县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('104', '130434', '魏县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('105', '130435', '曲周县', 'A', '86');
INSERT INTO `dic_areas` VALUES ('106', '130481', '武安市', 'A', '86');
INSERT INTO `dic_areas` VALUES ('107', '130500', '邢台市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('108', '130501', '市辖区', 'A', '107');
INSERT INTO `dic_areas` VALUES ('109', '130502', '桥东区', 'A', '107');
INSERT INTO `dic_areas` VALUES ('110', '130503', '桥西区', 'A', '107');
INSERT INTO `dic_areas` VALUES ('111', '130521', '邢台县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('112', '130522', '临城县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('113', '130523', '内丘县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('114', '130524', '柏乡县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('115', '130525', '隆尧县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('116', '130526', '任县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('117', '130527', '南和县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('118', '130528', '宁晋县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('119', '130529', '巨鹿县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('120', '130530', '新河县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('121', '130531', '广宗县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('122', '130532', '平乡县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('123', '130533', '威县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('124', '130534', '清河县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('125', '130535', '临西县', 'A', '107');
INSERT INTO `dic_areas` VALUES ('126', '130581', '南宫市', 'A', '107');
INSERT INTO `dic_areas` VALUES ('127', '130582', '沙河市', 'A', '107');
INSERT INTO `dic_areas` VALUES ('128', '130600', '保定市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('129', '130601', '市辖区', 'A', '128');
INSERT INTO `dic_areas` VALUES ('130', '130602', '竞秀区', 'A', '128');
INSERT INTO `dic_areas` VALUES ('131', '130606', '莲池区', 'A', '128');
INSERT INTO `dic_areas` VALUES ('132', '130607', '满城区', 'A', '128');
INSERT INTO `dic_areas` VALUES ('133', '130608', '清苑区', 'A', '128');
INSERT INTO `dic_areas` VALUES ('134', '130609', '徐水区', 'A', '128');
INSERT INTO `dic_areas` VALUES ('135', '130623', '涞水县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('136', '130624', '阜平县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('137', '130626', '定兴县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('138', '130627', '唐县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('139', '130628', '高阳县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('140', '130629', '容城县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('141', '130630', '涞源县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('142', '130631', '望都县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('143', '130632', '安新县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('144', '130633', '易县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('145', '130634', '曲阳县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('146', '130635', '蠡县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('147', '130636', '顺平县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('148', '130637', '博野县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('149', '130638', '雄县', 'A', '128');
INSERT INTO `dic_areas` VALUES ('150', '130681', '涿州市', 'A', '128');
INSERT INTO `dic_areas` VALUES ('151', '130683', '安国市', 'A', '128');
INSERT INTO `dic_areas` VALUES ('152', '130684', '高碑店市', 'A', '128');
INSERT INTO `dic_areas` VALUES ('153', '130700', '张家口市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('154', '130701', '市辖区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('155', '130702', '桥东区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('156', '130703', '桥西区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('157', '130705', '宣化区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('158', '130706', '下花园区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('159', '130708', '万全区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('160', '130709', '崇礼区', 'A', '153');
INSERT INTO `dic_areas` VALUES ('161', '130722', '张北县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('162', '130723', '康保县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('163', '130724', '沽源县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('164', '130725', '尚义县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('165', '130726', '蔚县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('166', '130727', '阳原县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('167', '130728', '怀安县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('168', '130730', '怀来县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('169', '130731', '涿鹿县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('170', '130732', '赤城县', 'A', '153');
INSERT INTO `dic_areas` VALUES ('171', '130800', '承德市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('172', '130801', '市辖区', 'A', '171');
INSERT INTO `dic_areas` VALUES ('173', '130802', '双桥区', 'A', '171');
INSERT INTO `dic_areas` VALUES ('174', '130803', '双滦区', 'A', '171');
INSERT INTO `dic_areas` VALUES ('175', '130804', '鹰手营子矿区', 'A', '171');
INSERT INTO `dic_areas` VALUES ('176', '130821', '承德县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('177', '130822', '兴隆县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('178', '130823', '平泉县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('179', '130824', '滦平县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('180', '130825', '隆化县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('181', '130826', '丰宁满族自治县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('182', '130827', '宽城满族自治县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('183', '130828', '围场满族蒙古族自治县', 'A', '171');
INSERT INTO `dic_areas` VALUES ('184', '130900', '沧州市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('185', '130901', '市辖区', 'A', '184');
INSERT INTO `dic_areas` VALUES ('186', '130902', '新华区', 'A', '184');
INSERT INTO `dic_areas` VALUES ('187', '130903', '运河区', 'A', '184');
INSERT INTO `dic_areas` VALUES ('188', '130921', '沧县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('189', '130922', '青县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('190', '130923', '东光县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('191', '130924', '海兴县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('192', '130925', '盐山县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('193', '130926', '肃宁县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('194', '130927', '南皮县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('195', '130928', '吴桥县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('196', '130929', '献县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('197', '130930', '孟村回族自治县', 'A', '184');
INSERT INTO `dic_areas` VALUES ('198', '130981', '泊头市', 'A', '184');
INSERT INTO `dic_areas` VALUES ('199', '130982', '任丘市', 'A', '184');
INSERT INTO `dic_areas` VALUES ('200', '130983', '黄骅市', 'A', '184');
INSERT INTO `dic_areas` VALUES ('201', '130984', '河间市', 'A', '184');
INSERT INTO `dic_areas` VALUES ('202', '131000', '廊坊市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('203', '131001', '市辖区', 'A', '202');
INSERT INTO `dic_areas` VALUES ('204', '131002', '安次区', 'A', '202');
INSERT INTO `dic_areas` VALUES ('205', '131003', '广阳区', 'A', '202');
INSERT INTO `dic_areas` VALUES ('206', '131022', '固安县', 'A', '202');
INSERT INTO `dic_areas` VALUES ('207', '131023', '永清县', 'A', '202');
INSERT INTO `dic_areas` VALUES ('208', '131024', '香河县', 'A', '202');
INSERT INTO `dic_areas` VALUES ('209', '131025', '大城县', 'A', '202');
INSERT INTO `dic_areas` VALUES ('210', '131026', '文安县', 'A', '202');
INSERT INTO `dic_areas` VALUES ('211', '131028', '大厂回族自治县', 'A', '202');
INSERT INTO `dic_areas` VALUES ('212', '131081', '霸州市', 'A', '202');
INSERT INTO `dic_areas` VALUES ('213', '131082', '三河市', 'A', '202');
INSERT INTO `dic_areas` VALUES ('214', '131100', '衡水市', 'C', '37');
INSERT INTO `dic_areas` VALUES ('215', '131101', '市辖区', 'A', '214');
INSERT INTO `dic_areas` VALUES ('216', '131102', '桃城区', 'A', '214');
INSERT INTO `dic_areas` VALUES ('217', '131103', '冀州区', 'A', '214');
INSERT INTO `dic_areas` VALUES ('218', '131121', '枣强县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('219', '131122', '武邑县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('220', '131123', '武强县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('221', '131124', '饶阳县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('222', '131125', '安平县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('223', '131126', '故城县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('224', '131127', '景县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('225', '131128', '阜城县', 'A', '214');
INSERT INTO `dic_areas` VALUES ('226', '131182', '深州市', 'A', '214');
INSERT INTO `dic_areas` VALUES ('227', '139000', '省直辖县级行政区划', 'C', '37');
INSERT INTO `dic_areas` VALUES ('228', '139001', '定州市', 'A', '227');
INSERT INTO `dic_areas` VALUES ('229', '139002', '辛集市', 'A', '227');
INSERT INTO `dic_areas` VALUES ('230', '140000', '山西省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('231', '140100', '太原市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('232', '140101', '市辖区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('233', '140105', '小店区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('234', '140106', '迎泽区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('235', '140107', '杏花岭区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('236', '140108', '尖草坪区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('237', '140109', '万柏林区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('238', '140110', '晋源区', 'A', '231');
INSERT INTO `dic_areas` VALUES ('239', '140121', '清徐县', 'A', '231');
INSERT INTO `dic_areas` VALUES ('240', '140122', '阳曲县', 'A', '231');
INSERT INTO `dic_areas` VALUES ('241', '140123', '娄烦县', 'A', '231');
INSERT INTO `dic_areas` VALUES ('242', '140181', '古交市', 'A', '231');
INSERT INTO `dic_areas` VALUES ('243', '140200', '大同市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('244', '140201', '市辖区', 'A', '243');
INSERT INTO `dic_areas` VALUES ('245', '140202', '城区', 'A', '243');
INSERT INTO `dic_areas` VALUES ('246', '140203', '矿区', 'A', '243');
INSERT INTO `dic_areas` VALUES ('247', '140211', '南郊区', 'A', '243');
INSERT INTO `dic_areas` VALUES ('248', '140212', '新荣区', 'A', '243');
INSERT INTO `dic_areas` VALUES ('249', '140221', '阳高县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('250', '140222', '天镇县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('251', '140223', '广灵县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('252', '140224', '灵丘县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('253', '140225', '浑源县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('254', '140226', '左云县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('255', '140227', '大同县', 'A', '243');
INSERT INTO `dic_areas` VALUES ('256', '140300', '阳泉市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('257', '140301', '市辖区', 'A', '256');
INSERT INTO `dic_areas` VALUES ('258', '140302', '城区', 'A', '256');
INSERT INTO `dic_areas` VALUES ('259', '140303', '矿区', 'A', '256');
INSERT INTO `dic_areas` VALUES ('260', '140311', '郊区', 'A', '256');
INSERT INTO `dic_areas` VALUES ('261', '140321', '平定县', 'A', '256');
INSERT INTO `dic_areas` VALUES ('262', '140322', '盂县', 'A', '256');
INSERT INTO `dic_areas` VALUES ('263', '140400', '长治市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('264', '140401', '市辖区', 'A', '263');
INSERT INTO `dic_areas` VALUES ('265', '140402', '城区', 'A', '263');
INSERT INTO `dic_areas` VALUES ('266', '140411', '郊区', 'A', '263');
INSERT INTO `dic_areas` VALUES ('267', '140421', '长治县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('268', '140423', '襄垣县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('269', '140424', '屯留县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('270', '140425', '平顺县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('271', '140426', '黎城县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('272', '140427', '壶关县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('273', '140428', '长子县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('274', '140429', '武乡县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('275', '140430', '沁县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('276', '140431', '沁源县', 'A', '263');
INSERT INTO `dic_areas` VALUES ('277', '140481', '潞城市', 'A', '263');
INSERT INTO `dic_areas` VALUES ('278', '140500', '晋城市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('279', '140501', '市辖区', 'A', '278');
INSERT INTO `dic_areas` VALUES ('280', '140502', '城区', 'A', '278');
INSERT INTO `dic_areas` VALUES ('281', '140521', '沁水县', 'A', '278');
INSERT INTO `dic_areas` VALUES ('282', '140522', '阳城县', 'A', '278');
INSERT INTO `dic_areas` VALUES ('283', '140524', '陵川县', 'A', '278');
INSERT INTO `dic_areas` VALUES ('284', '140525', '泽州县', 'A', '278');
INSERT INTO `dic_areas` VALUES ('285', '140581', '高平市', 'A', '278');
INSERT INTO `dic_areas` VALUES ('286', '140600', '朔州市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('287', '140601', '市辖区', 'A', '286');
INSERT INTO `dic_areas` VALUES ('288', '140602', '朔城区', 'A', '286');
INSERT INTO `dic_areas` VALUES ('289', '140603', '平鲁区', 'A', '286');
INSERT INTO `dic_areas` VALUES ('290', '140621', '山阴县', 'A', '286');
INSERT INTO `dic_areas` VALUES ('291', '140622', '应县', 'A', '286');
INSERT INTO `dic_areas` VALUES ('292', '140623', '右玉县', 'A', '286');
INSERT INTO `dic_areas` VALUES ('293', '140624', '怀仁县', 'A', '286');
INSERT INTO `dic_areas` VALUES ('294', '140700', '晋中市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('295', '140701', '市辖区', 'A', '294');
INSERT INTO `dic_areas` VALUES ('296', '140702', '榆次区', 'A', '294');
INSERT INTO `dic_areas` VALUES ('297', '140721', '榆社县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('298', '140722', '左权县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('299', '140723', '和顺县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('300', '140724', '昔阳县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('301', '140725', '寿阳县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('302', '140726', '太谷县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('303', '140727', '祁县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('304', '140728', '平遥县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('305', '140729', '灵石县', 'A', '294');
INSERT INTO `dic_areas` VALUES ('306', '140781', '介休市', 'A', '294');
INSERT INTO `dic_areas` VALUES ('307', '140800', '运城市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('308', '140801', '市辖区', 'A', '307');
INSERT INTO `dic_areas` VALUES ('309', '140802', '盐湖区', 'A', '307');
INSERT INTO `dic_areas` VALUES ('310', '140821', '临猗县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('311', '140822', '万荣县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('312', '140823', '闻喜县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('313', '140824', '稷山县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('314', '140825', '新绛县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('315', '140826', '绛县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('316', '140827', '垣曲县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('317', '140828', '夏县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('318', '140829', '平陆县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('319', '140830', '芮城县', 'A', '307');
INSERT INTO `dic_areas` VALUES ('320', '140881', '永济市', 'A', '307');
INSERT INTO `dic_areas` VALUES ('321', '140882', '河津市', 'A', '307');
INSERT INTO `dic_areas` VALUES ('322', '140900', '忻州市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('323', '140901', '市辖区', 'A', '322');
INSERT INTO `dic_areas` VALUES ('324', '140902', '忻府区', 'A', '322');
INSERT INTO `dic_areas` VALUES ('325', '140921', '定襄县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('326', '140922', '五台县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('327', '140923', '代县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('328', '140924', '繁峙县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('329', '140925', '宁武县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('330', '140926', '静乐县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('331', '140927', '神池县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('332', '140928', '五寨县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('333', '140929', '岢岚县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('334', '140930', '河曲县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('335', '140931', '保德县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('336', '140932', '偏关县', 'A', '322');
INSERT INTO `dic_areas` VALUES ('337', '140981', '原平市', 'A', '322');
INSERT INTO `dic_areas` VALUES ('338', '141000', '临汾市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('339', '141001', '市辖区', 'A', '338');
INSERT INTO `dic_areas` VALUES ('340', '141002', '尧都区', 'A', '338');
INSERT INTO `dic_areas` VALUES ('341', '141021', '曲沃县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('342', '141022', '翼城县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('343', '141023', '襄汾县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('344', '141024', '洪洞县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('345', '141025', '古县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('346', '141026', '安泽县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('347', '141027', '浮山县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('348', '141028', '吉县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('349', '141029', '乡宁县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('350', '141030', '大宁县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('351', '141031', '隰县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('352', '141032', '永和县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('353', '141033', '蒲县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('354', '141034', '汾西县', 'A', '338');
INSERT INTO `dic_areas` VALUES ('355', '141081', '侯马市', 'A', '338');
INSERT INTO `dic_areas` VALUES ('356', '141082', '霍州市', 'A', '338');
INSERT INTO `dic_areas` VALUES ('357', '141100', '吕梁市', 'C', '230');
INSERT INTO `dic_areas` VALUES ('358', '141101', '市辖区', 'A', '357');
INSERT INTO `dic_areas` VALUES ('359', '141102', '离石区', 'A', '357');
INSERT INTO `dic_areas` VALUES ('360', '141121', '文水县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('361', '141122', '交城县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('362', '141123', '兴县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('363', '141124', '临县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('364', '141125', '柳林县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('365', '141126', '石楼县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('366', '141127', '岚县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('367', '141128', '方山县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('368', '141129', '中阳县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('369', '141130', '交口县', 'A', '357');
INSERT INTO `dic_areas` VALUES ('370', '141181', '孝义市', 'A', '357');
INSERT INTO `dic_areas` VALUES ('371', '141182', '汾阳市', 'A', '357');
INSERT INTO `dic_areas` VALUES ('372', '150000', '内蒙古自治区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('373', '150100', '呼和浩特市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('374', '150101', '市辖区', 'A', '373');
INSERT INTO `dic_areas` VALUES ('375', '150102', '新城区', 'A', '373');
INSERT INTO `dic_areas` VALUES ('376', '150103', '回民区', 'A', '373');
INSERT INTO `dic_areas` VALUES ('377', '150104', '玉泉区', 'A', '373');
INSERT INTO `dic_areas` VALUES ('378', '150105', '赛罕区', 'A', '373');
INSERT INTO `dic_areas` VALUES ('379', '150121', '土默特左旗', 'A', '373');
INSERT INTO `dic_areas` VALUES ('380', '150122', '托克托县', 'A', '373');
INSERT INTO `dic_areas` VALUES ('381', '150123', '和林格尔县', 'A', '373');
INSERT INTO `dic_areas` VALUES ('382', '150124', '清水河县', 'A', '373');
INSERT INTO `dic_areas` VALUES ('383', '150125', '武川县', 'A', '373');
INSERT INTO `dic_areas` VALUES ('384', '150200', '包头市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('385', '150201', '市辖区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('386', '150202', '东河区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('387', '150203', '昆都仑区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('388', '150204', '青山区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('389', '150205', '石拐区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('390', '150206', '白云鄂博矿区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('391', '150207', '九原区', 'A', '384');
INSERT INTO `dic_areas` VALUES ('392', '150221', '土默特右旗', 'A', '384');
INSERT INTO `dic_areas` VALUES ('393', '150222', '固阳县', 'A', '384');
INSERT INTO `dic_areas` VALUES ('394', '150223', '达尔罕茂明安联合旗', 'A', '384');
INSERT INTO `dic_areas` VALUES ('395', '150300', '乌海市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('396', '150301', '市辖区', 'A', '395');
INSERT INTO `dic_areas` VALUES ('397', '150302', '海勃湾区', 'A', '395');
INSERT INTO `dic_areas` VALUES ('398', '150303', '海南区', 'A', '395');
INSERT INTO `dic_areas` VALUES ('399', '150304', '乌达区', 'A', '395');
INSERT INTO `dic_areas` VALUES ('400', '150400', '赤峰市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('401', '150401', '市辖区', 'A', '400');
INSERT INTO `dic_areas` VALUES ('402', '150402', '红山区', 'A', '400');
INSERT INTO `dic_areas` VALUES ('403', '150403', '元宝山区', 'A', '400');
INSERT INTO `dic_areas` VALUES ('404', '150404', '松山区', 'A', '400');
INSERT INTO `dic_areas` VALUES ('405', '150421', '阿鲁科尔沁旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('406', '150422', '巴林左旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('407', '150423', '巴林右旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('408', '150424', '林西县', 'A', '400');
INSERT INTO `dic_areas` VALUES ('409', '150425', '克什克腾旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('410', '150426', '翁牛特旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('411', '150428', '喀喇沁旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('412', '150429', '宁城县', 'A', '400');
INSERT INTO `dic_areas` VALUES ('413', '150430', '敖汉旗', 'A', '400');
INSERT INTO `dic_areas` VALUES ('414', '150500', '通辽市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('415', '150501', '市辖区', 'A', '414');
INSERT INTO `dic_areas` VALUES ('416', '150502', '科尔沁区', 'A', '414');
INSERT INTO `dic_areas` VALUES ('417', '150521', '科尔沁左翼中旗', 'A', '414');
INSERT INTO `dic_areas` VALUES ('418', '150522', '科尔沁左翼后旗', 'A', '414');
INSERT INTO `dic_areas` VALUES ('419', '150523', '开鲁县', 'A', '414');
INSERT INTO `dic_areas` VALUES ('420', '150524', '库伦旗', 'A', '414');
INSERT INTO `dic_areas` VALUES ('421', '150525', '奈曼旗', 'A', '414');
INSERT INTO `dic_areas` VALUES ('422', '150526', '扎鲁特旗', 'A', '414');
INSERT INTO `dic_areas` VALUES ('423', '150581', '霍林郭勒市', 'A', '414');
INSERT INTO `dic_areas` VALUES ('424', '150600', '鄂尔多斯市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('425', '150601', '市辖区', 'A', '424');
INSERT INTO `dic_areas` VALUES ('426', '150602', '东胜区', 'A', '424');
INSERT INTO `dic_areas` VALUES ('427', '150603', '康巴什区', 'A', '424');
INSERT INTO `dic_areas` VALUES ('428', '150621', '达拉特旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('429', '150622', '准格尔旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('430', '150623', '鄂托克前旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('431', '150624', '鄂托克旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('432', '150625', '杭锦旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('433', '150626', '乌审旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('434', '150627', '伊金霍洛旗', 'A', '424');
INSERT INTO `dic_areas` VALUES ('435', '150700', '呼伦贝尔市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('436', '150701', '市辖区', 'A', '435');
INSERT INTO `dic_areas` VALUES ('437', '150702', '海拉尔区', 'A', '435');
INSERT INTO `dic_areas` VALUES ('438', '150703', '扎赉诺尔区', 'A', '435');
INSERT INTO `dic_areas` VALUES ('439', '150721', '阿荣旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('440', '150722', '莫力达瓦达斡尔族自治旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('441', '150723', '鄂伦春自治旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('442', '150724', '鄂温克族自治旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('443', '150725', '陈巴尔虎旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('444', '150726', '新巴尔虎左旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('445', '150727', '新巴尔虎右旗', 'A', '435');
INSERT INTO `dic_areas` VALUES ('446', '150781', '满洲里市', 'A', '435');
INSERT INTO `dic_areas` VALUES ('447', '150782', '牙克石市', 'A', '435');
INSERT INTO `dic_areas` VALUES ('448', '150783', '扎兰屯市', 'A', '435');
INSERT INTO `dic_areas` VALUES ('449', '150784', '额尔古纳市', 'A', '435');
INSERT INTO `dic_areas` VALUES ('450', '150785', '根河市', 'A', '435');
INSERT INTO `dic_areas` VALUES ('451', '150800', '巴彦淖尔市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('452', '150801', '市辖区', 'A', '451');
INSERT INTO `dic_areas` VALUES ('453', '150802', '临河区', 'A', '451');
INSERT INTO `dic_areas` VALUES ('454', '150821', '五原县', 'A', '451');
INSERT INTO `dic_areas` VALUES ('455', '150822', '磴口县', 'A', '451');
INSERT INTO `dic_areas` VALUES ('456', '150823', '乌拉特前旗', 'A', '451');
INSERT INTO `dic_areas` VALUES ('457', '150824', '乌拉特中旗', 'A', '451');
INSERT INTO `dic_areas` VALUES ('458', '150825', '乌拉特后旗', 'A', '451');
INSERT INTO `dic_areas` VALUES ('459', '150826', '杭锦后旗', 'A', '451');
INSERT INTO `dic_areas` VALUES ('460', '150900', '乌兰察布市', 'C', '372');
INSERT INTO `dic_areas` VALUES ('461', '150901', '市辖区', 'A', '460');
INSERT INTO `dic_areas` VALUES ('462', '150902', '集宁区', 'A', '460');
INSERT INTO `dic_areas` VALUES ('463', '150921', '卓资县', 'A', '460');
INSERT INTO `dic_areas` VALUES ('464', '150922', '化德县', 'A', '460');
INSERT INTO `dic_areas` VALUES ('465', '150923', '商都县', 'A', '460');
INSERT INTO `dic_areas` VALUES ('466', '150924', '兴和县', 'A', '460');
INSERT INTO `dic_areas` VALUES ('467', '150925', '凉城县', 'A', '460');
INSERT INTO `dic_areas` VALUES ('468', '150926', '察哈尔右翼前旗', 'A', '460');
INSERT INTO `dic_areas` VALUES ('469', '150927', '察哈尔右翼中旗', 'A', '460');
INSERT INTO `dic_areas` VALUES ('470', '150928', '察哈尔右翼后旗', 'A', '460');
INSERT INTO `dic_areas` VALUES ('471', '150929', '四子王旗', 'A', '460');
INSERT INTO `dic_areas` VALUES ('472', '150981', '丰镇市', 'A', '460');
INSERT INTO `dic_areas` VALUES ('473', '152200', '兴安盟', 'C', '372');
INSERT INTO `dic_areas` VALUES ('474', '152201', '乌兰浩特市', 'A', '473');
INSERT INTO `dic_areas` VALUES ('475', '152202', '阿尔山市', 'A', '473');
INSERT INTO `dic_areas` VALUES ('476', '152221', '科尔沁右翼前旗', 'A', '473');
INSERT INTO `dic_areas` VALUES ('477', '152222', '科尔沁右翼中旗', 'A', '473');
INSERT INTO `dic_areas` VALUES ('478', '152223', '扎赉特旗', 'A', '473');
INSERT INTO `dic_areas` VALUES ('479', '152224', '突泉县', 'A', '473');
INSERT INTO `dic_areas` VALUES ('480', '152500', '锡林郭勒盟', 'C', '372');
INSERT INTO `dic_areas` VALUES ('481', '152501', '二连浩特市', 'A', '480');
INSERT INTO `dic_areas` VALUES ('482', '152502', '锡林浩特市', 'A', '480');
INSERT INTO `dic_areas` VALUES ('483', '152522', '阿巴嘎旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('484', '152523', '苏尼特左旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('485', '152524', '苏尼特右旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('486', '152525', '东乌珠穆沁旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('487', '152526', '西乌珠穆沁旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('488', '152527', '太仆寺旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('489', '152528', '镶黄旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('490', '152529', '正镶白旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('491', '152530', '正蓝旗', 'A', '480');
INSERT INTO `dic_areas` VALUES ('492', '152531', '多伦县', 'A', '480');
INSERT INTO `dic_areas` VALUES ('493', '152900', '阿拉善盟', 'C', '372');
INSERT INTO `dic_areas` VALUES ('494', '152921', '阿拉善左旗', 'A', '493');
INSERT INTO `dic_areas` VALUES ('495', '152922', '阿拉善右旗', 'A', '493');
INSERT INTO `dic_areas` VALUES ('496', '152923', '额济纳旗', 'A', '493');
INSERT INTO `dic_areas` VALUES ('497', '210000', '辽宁省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('498', '210100', '沈阳市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('499', '210101', '市辖区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('500', '210102', '和平区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('501', '210103', '沈河区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('502', '210104', '大东区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('503', '210105', '皇姑区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('504', '210106', '铁西区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('505', '210111', '苏家屯区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('506', '210112', '浑南区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('507', '210113', '沈北新区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('508', '210114', '于洪区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('509', '210115', '辽中区', 'A', '498');
INSERT INTO `dic_areas` VALUES ('510', '210123', '康平县', 'A', '498');
INSERT INTO `dic_areas` VALUES ('511', '210124', '法库县', 'A', '498');
INSERT INTO `dic_areas` VALUES ('512', '210181', '新民市', 'A', '498');
INSERT INTO `dic_areas` VALUES ('513', '210200', '大连市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('514', '210201', '市辖区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('515', '210202', '中山区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('516', '210203', '西岗区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('517', '210204', '沙河口区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('518', '210211', '甘井子区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('519', '210212', '旅顺口区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('520', '210213', '金州区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('521', '210214', '普兰店区', 'A', '513');
INSERT INTO `dic_areas` VALUES ('522', '210224', '长海县', 'A', '513');
INSERT INTO `dic_areas` VALUES ('523', '210281', '瓦房店市', 'A', '513');
INSERT INTO `dic_areas` VALUES ('524', '210283', '庄河市', 'A', '513');
INSERT INTO `dic_areas` VALUES ('525', '210300', '鞍山市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('526', '210301', '市辖区', 'A', '525');
INSERT INTO `dic_areas` VALUES ('527', '210302', '铁东区', 'A', '525');
INSERT INTO `dic_areas` VALUES ('528', '210303', '铁西区', 'A', '525');
INSERT INTO `dic_areas` VALUES ('529', '210304', '立山区', 'A', '525');
INSERT INTO `dic_areas` VALUES ('530', '210311', '千山区', 'A', '525');
INSERT INTO `dic_areas` VALUES ('531', '210321', '台安县', 'A', '525');
INSERT INTO `dic_areas` VALUES ('532', '210323', '岫岩满族自治县', 'A', '525');
INSERT INTO `dic_areas` VALUES ('533', '210381', '海城市', 'A', '525');
INSERT INTO `dic_areas` VALUES ('534', '210400', '抚顺市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('535', '210401', '市辖区', 'A', '534');
INSERT INTO `dic_areas` VALUES ('536', '210402', '新抚区', 'A', '534');
INSERT INTO `dic_areas` VALUES ('537', '210403', '东洲区', 'A', '534');
INSERT INTO `dic_areas` VALUES ('538', '210404', '望花区', 'A', '534');
INSERT INTO `dic_areas` VALUES ('539', '210411', '顺城区', 'A', '534');
INSERT INTO `dic_areas` VALUES ('540', '210421', '抚顺县', 'A', '534');
INSERT INTO `dic_areas` VALUES ('541', '210422', '新宾满族自治县', 'A', '534');
INSERT INTO `dic_areas` VALUES ('542', '210423', '清原满族自治县', 'A', '534');
INSERT INTO `dic_areas` VALUES ('543', '210500', '本溪市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('544', '210501', '市辖区', 'A', '543');
INSERT INTO `dic_areas` VALUES ('545', '210502', '平山区', 'A', '543');
INSERT INTO `dic_areas` VALUES ('546', '210503', '溪湖区', 'A', '543');
INSERT INTO `dic_areas` VALUES ('547', '210504', '明山区', 'A', '543');
INSERT INTO `dic_areas` VALUES ('548', '210505', '南芬区', 'A', '543');
INSERT INTO `dic_areas` VALUES ('549', '210521', '本溪满族自治县', 'A', '543');
INSERT INTO `dic_areas` VALUES ('550', '210522', '桓仁满族自治县', 'A', '543');
INSERT INTO `dic_areas` VALUES ('551', '210600', '丹东市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('552', '210601', '市辖区', 'A', '551');
INSERT INTO `dic_areas` VALUES ('553', '210602', '元宝区', 'A', '551');
INSERT INTO `dic_areas` VALUES ('554', '210603', '振兴区', 'A', '551');
INSERT INTO `dic_areas` VALUES ('555', '210604', '振安区', 'A', '551');
INSERT INTO `dic_areas` VALUES ('556', '210624', '宽甸满族自治县', 'A', '551');
INSERT INTO `dic_areas` VALUES ('557', '210681', '东港市', 'A', '551');
INSERT INTO `dic_areas` VALUES ('558', '210682', '凤城市', 'A', '551');
INSERT INTO `dic_areas` VALUES ('559', '210700', '锦州市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('560', '210701', '市辖区', 'A', '559');
INSERT INTO `dic_areas` VALUES ('561', '210702', '古塔区', 'A', '559');
INSERT INTO `dic_areas` VALUES ('562', '210703', '凌河区', 'A', '559');
INSERT INTO `dic_areas` VALUES ('563', '210711', '太和区', 'A', '559');
INSERT INTO `dic_areas` VALUES ('564', '210726', '黑山县', 'A', '559');
INSERT INTO `dic_areas` VALUES ('565', '210727', '义县', 'A', '559');
INSERT INTO `dic_areas` VALUES ('566', '210781', '凌海市', 'A', '559');
INSERT INTO `dic_areas` VALUES ('567', '210782', '北镇市', 'A', '559');
INSERT INTO `dic_areas` VALUES ('568', '210800', '营口市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('569', '210801', '市辖区', 'A', '568');
INSERT INTO `dic_areas` VALUES ('570', '210802', '站前区', 'A', '568');
INSERT INTO `dic_areas` VALUES ('571', '210803', '西市区', 'A', '568');
INSERT INTO `dic_areas` VALUES ('572', '210804', '鲅鱼圈区', 'A', '568');
INSERT INTO `dic_areas` VALUES ('573', '210811', '老边区', 'A', '568');
INSERT INTO `dic_areas` VALUES ('574', '210881', '盖州市', 'A', '568');
INSERT INTO `dic_areas` VALUES ('575', '210882', '大石桥市', 'A', '568');
INSERT INTO `dic_areas` VALUES ('576', '210900', '阜新市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('577', '210901', '市辖区', 'A', '576');
INSERT INTO `dic_areas` VALUES ('578', '210902', '海州区', 'A', '576');
INSERT INTO `dic_areas` VALUES ('579', '210903', '新邱区', 'A', '576');
INSERT INTO `dic_areas` VALUES ('580', '210904', '太平区', 'A', '576');
INSERT INTO `dic_areas` VALUES ('581', '210905', '清河门区', 'A', '576');
INSERT INTO `dic_areas` VALUES ('582', '210911', '细河区', 'A', '576');
INSERT INTO `dic_areas` VALUES ('583', '210921', '阜新蒙古族自治县', 'A', '576');
INSERT INTO `dic_areas` VALUES ('584', '210922', '彰武县', 'A', '576');
INSERT INTO `dic_areas` VALUES ('585', '211000', '辽阳市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('586', '211001', '市辖区', 'A', '585');
INSERT INTO `dic_areas` VALUES ('587', '211002', '白塔区', 'A', '585');
INSERT INTO `dic_areas` VALUES ('588', '211003', '文圣区', 'A', '585');
INSERT INTO `dic_areas` VALUES ('589', '211004', '宏伟区', 'A', '585');
INSERT INTO `dic_areas` VALUES ('590', '211005', '弓长岭区', 'A', '585');
INSERT INTO `dic_areas` VALUES ('591', '211011', '太子河区', 'A', '585');
INSERT INTO `dic_areas` VALUES ('592', '211021', '辽阳县', 'A', '585');
INSERT INTO `dic_areas` VALUES ('593', '211081', '灯塔市', 'A', '585');
INSERT INTO `dic_areas` VALUES ('594', '211100', '盘锦市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('595', '211101', '市辖区', 'A', '594');
INSERT INTO `dic_areas` VALUES ('596', '211102', '双台子区', 'A', '594');
INSERT INTO `dic_areas` VALUES ('597', '211103', '兴隆台区', 'A', '594');
INSERT INTO `dic_areas` VALUES ('598', '211104', '大洼区', 'A', '594');
INSERT INTO `dic_areas` VALUES ('599', '211122', '盘山县', 'A', '594');
INSERT INTO `dic_areas` VALUES ('600', '211200', '铁岭市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('601', '211201', '市辖区', 'A', '600');
INSERT INTO `dic_areas` VALUES ('602', '211202', '银州区', 'A', '600');
INSERT INTO `dic_areas` VALUES ('603', '211204', '清河区', 'A', '600');
INSERT INTO `dic_areas` VALUES ('604', '211221', '铁岭县', 'A', '600');
INSERT INTO `dic_areas` VALUES ('605', '211223', '西丰县', 'A', '600');
INSERT INTO `dic_areas` VALUES ('606', '211224', '昌图县', 'A', '600');
INSERT INTO `dic_areas` VALUES ('607', '211281', '调兵山市', 'A', '600');
INSERT INTO `dic_areas` VALUES ('608', '211282', '开原市', 'A', '600');
INSERT INTO `dic_areas` VALUES ('609', '211300', '朝阳市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('610', '211301', '市辖区', 'A', '609');
INSERT INTO `dic_areas` VALUES ('611', '211302', '双塔区', 'A', '609');
INSERT INTO `dic_areas` VALUES ('612', '211303', '龙城区', 'A', '609');
INSERT INTO `dic_areas` VALUES ('613', '211321', '朝阳县', 'A', '609');
INSERT INTO `dic_areas` VALUES ('614', '211322', '建平县', 'A', '609');
INSERT INTO `dic_areas` VALUES ('615', '211324', '喀喇沁左翼蒙古族自治县', 'A', '609');
INSERT INTO `dic_areas` VALUES ('616', '211381', '北票市', 'A', '609');
INSERT INTO `dic_areas` VALUES ('617', '211382', '凌源市', 'A', '609');
INSERT INTO `dic_areas` VALUES ('618', '211400', '葫芦岛市', 'C', '497');
INSERT INTO `dic_areas` VALUES ('619', '211401', '市辖区', 'A', '618');
INSERT INTO `dic_areas` VALUES ('620', '211402', '连山区', 'A', '618');
INSERT INTO `dic_areas` VALUES ('621', '211403', '龙港区', 'A', '618');
INSERT INTO `dic_areas` VALUES ('622', '211404', '南票区', 'A', '618');
INSERT INTO `dic_areas` VALUES ('623', '211421', '绥中县', 'A', '618');
INSERT INTO `dic_areas` VALUES ('624', '211422', '建昌县', 'A', '618');
INSERT INTO `dic_areas` VALUES ('625', '211481', '兴城市', 'A', '618');
INSERT INTO `dic_areas` VALUES ('626', '220000', '吉林省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('627', '220100', '长春市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('628', '220101', '市辖区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('629', '220102', '南关区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('630', '220103', '宽城区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('631', '220104', '朝阳区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('632', '220105', '二道区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('633', '220106', '绿园区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('634', '220112', '双阳区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('635', '220113', '九台区', 'A', '627');
INSERT INTO `dic_areas` VALUES ('636', '220122', '农安县', 'A', '627');
INSERT INTO `dic_areas` VALUES ('637', '220182', '榆树市', 'A', '627');
INSERT INTO `dic_areas` VALUES ('638', '220183', '德惠市', 'A', '627');
INSERT INTO `dic_areas` VALUES ('639', '220200', '吉林市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('640', '220201', '市辖区', 'A', '639');
INSERT INTO `dic_areas` VALUES ('641', '220202', '昌邑区', 'A', '639');
INSERT INTO `dic_areas` VALUES ('642', '220203', '龙潭区', 'A', '639');
INSERT INTO `dic_areas` VALUES ('643', '220204', '船营区', 'A', '639');
INSERT INTO `dic_areas` VALUES ('644', '220211', '丰满区', 'A', '639');
INSERT INTO `dic_areas` VALUES ('645', '220221', '永吉县', 'A', '639');
INSERT INTO `dic_areas` VALUES ('646', '220281', '蛟河市', 'A', '639');
INSERT INTO `dic_areas` VALUES ('647', '220282', '桦甸市', 'A', '639');
INSERT INTO `dic_areas` VALUES ('648', '220283', '舒兰市', 'A', '639');
INSERT INTO `dic_areas` VALUES ('649', '220284', '磐石市', 'A', '639');
INSERT INTO `dic_areas` VALUES ('650', '220300', '四平市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('651', '220301', '市辖区', 'A', '650');
INSERT INTO `dic_areas` VALUES ('652', '220302', '铁西区', 'A', '650');
INSERT INTO `dic_areas` VALUES ('653', '220303', '铁东区', 'A', '650');
INSERT INTO `dic_areas` VALUES ('654', '220322', '梨树县', 'A', '650');
INSERT INTO `dic_areas` VALUES ('655', '220323', '伊通满族自治县', 'A', '650');
INSERT INTO `dic_areas` VALUES ('656', '220381', '公主岭市', 'A', '650');
INSERT INTO `dic_areas` VALUES ('657', '220382', '双辽市', 'A', '650');
INSERT INTO `dic_areas` VALUES ('658', '220400', '辽源市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('659', '220401', '市辖区', 'A', '658');
INSERT INTO `dic_areas` VALUES ('660', '220402', '龙山区', 'A', '658');
INSERT INTO `dic_areas` VALUES ('661', '220403', '西安区', 'A', '658');
INSERT INTO `dic_areas` VALUES ('662', '220421', '东丰县', 'A', '658');
INSERT INTO `dic_areas` VALUES ('663', '220422', '东辽县', 'A', '658');
INSERT INTO `dic_areas` VALUES ('664', '220500', '通化市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('665', '220501', '市辖区', 'A', '664');
INSERT INTO `dic_areas` VALUES ('666', '220502', '东昌区', 'A', '664');
INSERT INTO `dic_areas` VALUES ('667', '220503', '二道江区', 'A', '664');
INSERT INTO `dic_areas` VALUES ('668', '220521', '通化县', 'A', '664');
INSERT INTO `dic_areas` VALUES ('669', '220523', '辉南县', 'A', '664');
INSERT INTO `dic_areas` VALUES ('670', '220524', '柳河县', 'A', '664');
INSERT INTO `dic_areas` VALUES ('671', '220581', '梅河口市', 'A', '664');
INSERT INTO `dic_areas` VALUES ('672', '220582', '集安市', 'A', '664');
INSERT INTO `dic_areas` VALUES ('673', '220600', '白山市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('674', '220601', '市辖区', 'A', '673');
INSERT INTO `dic_areas` VALUES ('675', '220602', '浑江区', 'A', '673');
INSERT INTO `dic_areas` VALUES ('676', '220605', '江源区', 'A', '673');
INSERT INTO `dic_areas` VALUES ('677', '220621', '抚松县', 'A', '673');
INSERT INTO `dic_areas` VALUES ('678', '220622', '靖宇县', 'A', '673');
INSERT INTO `dic_areas` VALUES ('679', '220623', '长白朝鲜族自治县', 'A', '673');
INSERT INTO `dic_areas` VALUES ('680', '220681', '临江市', 'A', '673');
INSERT INTO `dic_areas` VALUES ('681', '220700', '松原市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('682', '220701', '市辖区', 'A', '681');
INSERT INTO `dic_areas` VALUES ('683', '220702', '宁江区', 'A', '681');
INSERT INTO `dic_areas` VALUES ('684', '220721', '前郭尔罗斯蒙古族自治县', 'A', '681');
INSERT INTO `dic_areas` VALUES ('685', '220722', '长岭县', 'A', '681');
INSERT INTO `dic_areas` VALUES ('686', '220723', '乾安县', 'A', '681');
INSERT INTO `dic_areas` VALUES ('687', '220781', '扶余市', 'A', '681');
INSERT INTO `dic_areas` VALUES ('688', '220800', '白城市', 'C', '626');
INSERT INTO `dic_areas` VALUES ('689', '220801', '市辖区', 'A', '688');
INSERT INTO `dic_areas` VALUES ('690', '220802', '洮北区', 'A', '688');
INSERT INTO `dic_areas` VALUES ('691', '220821', '镇赉县', 'A', '688');
INSERT INTO `dic_areas` VALUES ('692', '220822', '通榆县', 'A', '688');
INSERT INTO `dic_areas` VALUES ('693', '220881', '洮南市', 'A', '688');
INSERT INTO `dic_areas` VALUES ('694', '220882', '大安市', 'A', '688');
INSERT INTO `dic_areas` VALUES ('695', '222400', '延边朝鲜族自治州', 'C', '626');
INSERT INTO `dic_areas` VALUES ('696', '222401', '延吉市', 'A', '695');
INSERT INTO `dic_areas` VALUES ('697', '222402', '图们市', 'A', '695');
INSERT INTO `dic_areas` VALUES ('698', '222403', '敦化市', 'A', '695');
INSERT INTO `dic_areas` VALUES ('699', '222404', '珲春市', 'A', '695');
INSERT INTO `dic_areas` VALUES ('700', '222405', '龙井市', 'A', '695');
INSERT INTO `dic_areas` VALUES ('701', '222406', '和龙市', 'A', '695');
INSERT INTO `dic_areas` VALUES ('702', '222424', '汪清县', 'A', '695');
INSERT INTO `dic_areas` VALUES ('703', '222426', '安图县', 'A', '695');
INSERT INTO `dic_areas` VALUES ('704', '230000', '黑龙江省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('705', '230100', '哈尔滨市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('706', '230101', '市辖区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('707', '230102', '道里区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('708', '230103', '南岗区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('709', '230104', '道外区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('710', '230108', '平房区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('711', '230109', '松北区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('712', '230110', '香坊区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('713', '230111', '呼兰区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('714', '230112', '阿城区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('715', '230113', '双城区', 'A', '705');
INSERT INTO `dic_areas` VALUES ('716', '230123', '依兰县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('717', '230124', '方正县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('718', '230125', '宾县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('719', '230126', '巴彦县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('720', '230127', '木兰县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('721', '230128', '通河县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('722', '230129', '延寿县', 'A', '705');
INSERT INTO `dic_areas` VALUES ('723', '230183', '尚志市', 'A', '705');
INSERT INTO `dic_areas` VALUES ('724', '230184', '五常市', 'A', '705');
INSERT INTO `dic_areas` VALUES ('725', '230200', '齐齐哈尔市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('726', '230201', '市辖区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('727', '230202', '龙沙区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('728', '230203', '建华区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('729', '230204', '铁锋区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('730', '230205', '昂昂溪区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('731', '230206', '富拉尔基区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('732', '230207', '碾子山区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('733', '230208', '梅里斯达斡尔族区', 'A', '725');
INSERT INTO `dic_areas` VALUES ('734', '230221', '龙江县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('735', '230223', '依安县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('736', '230224', '泰来县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('737', '230225', '甘南县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('738', '230227', '富裕县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('739', '230229', '克山县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('740', '230230', '克东县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('741', '230231', '拜泉县', 'A', '725');
INSERT INTO `dic_areas` VALUES ('742', '230281', '讷河市', 'A', '725');
INSERT INTO `dic_areas` VALUES ('743', '230300', '鸡西市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('744', '230301', '市辖区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('745', '230302', '鸡冠区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('746', '230303', '恒山区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('747', '230304', '滴道区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('748', '230305', '梨树区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('749', '230306', '城子河区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('750', '230307', '麻山区', 'A', '743');
INSERT INTO `dic_areas` VALUES ('751', '230321', '鸡东县', 'A', '743');
INSERT INTO `dic_areas` VALUES ('752', '230381', '虎林市', 'A', '743');
INSERT INTO `dic_areas` VALUES ('753', '230382', '密山市', 'A', '743');
INSERT INTO `dic_areas` VALUES ('754', '230400', '鹤岗市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('755', '230401', '市辖区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('756', '230402', '向阳区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('757', '230403', '工农区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('758', '230404', '南山区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('759', '230405', '兴安区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('760', '230406', '东山区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('761', '230407', '兴山区', 'A', '754');
INSERT INTO `dic_areas` VALUES ('762', '230421', '萝北县', 'A', '754');
INSERT INTO `dic_areas` VALUES ('763', '230422', '绥滨县', 'A', '754');
INSERT INTO `dic_areas` VALUES ('764', '230500', '双鸭山市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('765', '230501', '市辖区', 'A', '764');
INSERT INTO `dic_areas` VALUES ('766', '230502', '尖山区', 'A', '764');
INSERT INTO `dic_areas` VALUES ('767', '230503', '岭东区', 'A', '764');
INSERT INTO `dic_areas` VALUES ('768', '230505', '四方台区', 'A', '764');
INSERT INTO `dic_areas` VALUES ('769', '230506', '宝山区', 'A', '764');
INSERT INTO `dic_areas` VALUES ('770', '230521', '集贤县', 'A', '764');
INSERT INTO `dic_areas` VALUES ('771', '230522', '友谊县', 'A', '764');
INSERT INTO `dic_areas` VALUES ('772', '230523', '宝清县', 'A', '764');
INSERT INTO `dic_areas` VALUES ('773', '230524', '饶河县', 'A', '764');
INSERT INTO `dic_areas` VALUES ('774', '230600', '大庆市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('775', '230601', '市辖区', 'A', '774');
INSERT INTO `dic_areas` VALUES ('776', '230602', '萨尔图区', 'A', '774');
INSERT INTO `dic_areas` VALUES ('777', '230603', '龙凤区', 'A', '774');
INSERT INTO `dic_areas` VALUES ('778', '230604', '让胡路区', 'A', '774');
INSERT INTO `dic_areas` VALUES ('779', '230605', '红岗区', 'A', '774');
INSERT INTO `dic_areas` VALUES ('780', '230606', '大同区', 'A', '774');
INSERT INTO `dic_areas` VALUES ('781', '230621', '肇州县', 'A', '774');
INSERT INTO `dic_areas` VALUES ('782', '230622', '肇源县', 'A', '774');
INSERT INTO `dic_areas` VALUES ('783', '230623', '林甸县', 'A', '774');
INSERT INTO `dic_areas` VALUES ('784', '230624', '杜尔伯特蒙古族自治县', 'A', '774');
INSERT INTO `dic_areas` VALUES ('785', '230700', '伊春市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('786', '230701', '市辖区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('787', '230702', '伊春区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('788', '230703', '南岔区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('789', '230704', '友好区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('790', '230705', '西林区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('791', '230706', '翠峦区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('792', '230707', '新青区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('793', '230708', '美溪区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('794', '230709', '金山屯区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('795', '230710', '五营区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('796', '230711', '乌马河区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('797', '230712', '汤旺河区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('798', '230713', '带岭区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('799', '230714', '乌伊岭区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('800', '230715', '红星区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('801', '230716', '上甘岭区', 'A', '785');
INSERT INTO `dic_areas` VALUES ('802', '230722', '嘉荫县', 'A', '785');
INSERT INTO `dic_areas` VALUES ('803', '230781', '铁力市', 'A', '785');
INSERT INTO `dic_areas` VALUES ('804', '230800', '佳木斯市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('805', '230801', '市辖区', 'A', '804');
INSERT INTO `dic_areas` VALUES ('806', '230803', '向阳区', 'A', '804');
INSERT INTO `dic_areas` VALUES ('807', '230804', '前进区', 'A', '804');
INSERT INTO `dic_areas` VALUES ('808', '230805', '东风区', 'A', '804');
INSERT INTO `dic_areas` VALUES ('809', '230811', '郊区', 'A', '804');
INSERT INTO `dic_areas` VALUES ('810', '230822', '桦南县', 'A', '804');
INSERT INTO `dic_areas` VALUES ('811', '230826', '桦川县', 'A', '804');
INSERT INTO `dic_areas` VALUES ('812', '230828', '汤原县', 'A', '804');
INSERT INTO `dic_areas` VALUES ('813', '230881', '同江市', 'A', '804');
INSERT INTO `dic_areas` VALUES ('814', '230882', '富锦市', 'A', '804');
INSERT INTO `dic_areas` VALUES ('815', '230883', '抚远市', 'A', '804');
INSERT INTO `dic_areas` VALUES ('816', '230900', '七台河市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('817', '230901', '市辖区', 'A', '816');
INSERT INTO `dic_areas` VALUES ('818', '230902', '新兴区', 'A', '816');
INSERT INTO `dic_areas` VALUES ('819', '230903', '桃山区', 'A', '816');
INSERT INTO `dic_areas` VALUES ('820', '230904', '茄子河区', 'A', '816');
INSERT INTO `dic_areas` VALUES ('821', '230921', '勃利县', 'A', '816');
INSERT INTO `dic_areas` VALUES ('822', '231000', '牡丹江市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('823', '231001', '市辖区', 'A', '822');
INSERT INTO `dic_areas` VALUES ('824', '231002', '东安区', 'A', '822');
INSERT INTO `dic_areas` VALUES ('825', '231003', '阳明区', 'A', '822');
INSERT INTO `dic_areas` VALUES ('826', '231004', '爱民区', 'A', '822');
INSERT INTO `dic_areas` VALUES ('827', '231005', '西安区', 'A', '822');
INSERT INTO `dic_areas` VALUES ('828', '231025', '林口县', 'A', '822');
INSERT INTO `dic_areas` VALUES ('829', '231081', '绥芬河市', 'A', '822');
INSERT INTO `dic_areas` VALUES ('830', '231083', '海林市', 'A', '822');
INSERT INTO `dic_areas` VALUES ('831', '231084', '宁安市', 'A', '822');
INSERT INTO `dic_areas` VALUES ('832', '231085', '穆棱市', 'A', '822');
INSERT INTO `dic_areas` VALUES ('833', '231086', '东宁市', 'A', '822');
INSERT INTO `dic_areas` VALUES ('834', '231100', '黑河市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('835', '231101', '市辖区', 'A', '834');
INSERT INTO `dic_areas` VALUES ('836', '231102', '爱辉区', 'A', '834');
INSERT INTO `dic_areas` VALUES ('837', '231121', '嫩江县', 'A', '834');
INSERT INTO `dic_areas` VALUES ('838', '231123', '逊克县', 'A', '834');
INSERT INTO `dic_areas` VALUES ('839', '231124', '孙吴县', 'A', '834');
INSERT INTO `dic_areas` VALUES ('840', '231181', '北安市', 'A', '834');
INSERT INTO `dic_areas` VALUES ('841', '231182', '五大连池市', 'A', '834');
INSERT INTO `dic_areas` VALUES ('842', '231200', '绥化市', 'C', '704');
INSERT INTO `dic_areas` VALUES ('843', '231201', '市辖区', 'A', '842');
INSERT INTO `dic_areas` VALUES ('844', '231202', '北林区', 'A', '842');
INSERT INTO `dic_areas` VALUES ('845', '231221', '望奎县', 'A', '842');
INSERT INTO `dic_areas` VALUES ('846', '231222', '兰西县', 'A', '842');
INSERT INTO `dic_areas` VALUES ('847', '231223', '青冈县', 'A', '842');
INSERT INTO `dic_areas` VALUES ('848', '231224', '庆安县', 'A', '842');
INSERT INTO `dic_areas` VALUES ('849', '231225', '明水县', 'A', '842');
INSERT INTO `dic_areas` VALUES ('850', '231226', '绥棱县', 'A', '842');
INSERT INTO `dic_areas` VALUES ('851', '231281', '安达市', 'A', '842');
INSERT INTO `dic_areas` VALUES ('852', '231282', '肇东市', 'A', '842');
INSERT INTO `dic_areas` VALUES ('853', '231283', '海伦市', 'A', '842');
INSERT INTO `dic_areas` VALUES ('854', '232700', '大兴安岭地区', 'C', '704');
INSERT INTO `dic_areas` VALUES ('855', '232721', '呼玛县', 'A', '854');
INSERT INTO `dic_areas` VALUES ('856', '232722', '塔河县', 'A', '854');
INSERT INTO `dic_areas` VALUES ('857', '232723', '漠河县', 'A', '854');
INSERT INTO `dic_areas` VALUES ('858', '310000', '上海市', 'P', '0');
INSERT INTO `dic_areas` VALUES ('859', '310100', '市辖区', 'C', '858');
INSERT INTO `dic_areas` VALUES ('860', '310101', '黄浦区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('861', '310104', '徐汇区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('862', '310105', '长宁区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('863', '310106', '静安区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('864', '310107', '普陀区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('865', '310109', '虹口区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('866', '310110', '杨浦区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('867', '310112', '闵行区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('868', '310113', '宝山区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('869', '310114', '嘉定区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('870', '310115', '浦东新区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('871', '310116', '金山区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('872', '310117', '松江区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('873', '310118', '青浦区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('874', '310120', '奉贤区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('875', '310151', '崇明区', 'A', '859');
INSERT INTO `dic_areas` VALUES ('876', '320000', '江苏省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('877', '320100', '南京市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('878', '320101', '市辖区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('879', '320102', '玄武区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('880', '320104', '秦淮区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('881', '320105', '建邺区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('882', '320106', '鼓楼区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('883', '320111', '浦口区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('884', '320113', '栖霞区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('885', '320114', '雨花台区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('886', '320115', '江宁区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('887', '320116', '六合区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('888', '320117', '溧水区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('889', '320118', '高淳区', 'A', '877');
INSERT INTO `dic_areas` VALUES ('890', '320200', '无锡市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('891', '320201', '市辖区', 'A', '890');
INSERT INTO `dic_areas` VALUES ('892', '320205', '锡山区', 'A', '890');
INSERT INTO `dic_areas` VALUES ('893', '320206', '惠山区', 'A', '890');
INSERT INTO `dic_areas` VALUES ('894', '320211', '滨湖区', 'A', '890');
INSERT INTO `dic_areas` VALUES ('895', '320213', '梁溪区', 'A', '890');
INSERT INTO `dic_areas` VALUES ('896', '320214', '新吴区', 'A', '890');
INSERT INTO `dic_areas` VALUES ('897', '320281', '江阴市', 'A', '890');
INSERT INTO `dic_areas` VALUES ('898', '320282', '宜兴市', 'A', '890');
INSERT INTO `dic_areas` VALUES ('899', '320300', '徐州市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('900', '320301', '市辖区', 'A', '899');
INSERT INTO `dic_areas` VALUES ('901', '320302', '鼓楼区', 'A', '899');
INSERT INTO `dic_areas` VALUES ('902', '320303', '云龙区', 'A', '899');
INSERT INTO `dic_areas` VALUES ('903', '320305', '贾汪区', 'A', '899');
INSERT INTO `dic_areas` VALUES ('904', '320311', '泉山区', 'A', '899');
INSERT INTO `dic_areas` VALUES ('905', '320312', '铜山区', 'A', '899');
INSERT INTO `dic_areas` VALUES ('906', '320321', '丰县', 'A', '899');
INSERT INTO `dic_areas` VALUES ('907', '320322', '沛县', 'A', '899');
INSERT INTO `dic_areas` VALUES ('908', '320324', '睢宁县', 'A', '899');
INSERT INTO `dic_areas` VALUES ('909', '320381', '新沂市', 'A', '899');
INSERT INTO `dic_areas` VALUES ('910', '320382', '邳州市', 'A', '899');
INSERT INTO `dic_areas` VALUES ('911', '320400', '常州市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('912', '320401', '市辖区', 'A', '911');
INSERT INTO `dic_areas` VALUES ('913', '320402', '天宁区', 'A', '911');
INSERT INTO `dic_areas` VALUES ('914', '320404', '钟楼区', 'A', '911');
INSERT INTO `dic_areas` VALUES ('915', '320411', '新北区', 'A', '911');
INSERT INTO `dic_areas` VALUES ('916', '320412', '武进区', 'A', '911');
INSERT INTO `dic_areas` VALUES ('917', '320413', '金坛区', 'A', '911');
INSERT INTO `dic_areas` VALUES ('918', '320481', '溧阳市', 'A', '911');
INSERT INTO `dic_areas` VALUES ('919', '320500', '苏州市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('920', '320501', '市辖区', 'A', '919');
INSERT INTO `dic_areas` VALUES ('921', '320505', '虎丘区', 'A', '919');
INSERT INTO `dic_areas` VALUES ('922', '320506', '吴中区', 'A', '919');
INSERT INTO `dic_areas` VALUES ('923', '320507', '相城区', 'A', '919');
INSERT INTO `dic_areas` VALUES ('924', '320508', '姑苏区', 'A', '919');
INSERT INTO `dic_areas` VALUES ('925', '320509', '吴江区', 'A', '919');
INSERT INTO `dic_areas` VALUES ('926', '320581', '常熟市', 'A', '919');
INSERT INTO `dic_areas` VALUES ('927', '320582', '张家港市', 'A', '919');
INSERT INTO `dic_areas` VALUES ('928', '320583', '昆山市', 'A', '919');
INSERT INTO `dic_areas` VALUES ('929', '320585', '太仓市', 'A', '919');
INSERT INTO `dic_areas` VALUES ('930', '320600', '南通市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('931', '320601', '市辖区', 'A', '930');
INSERT INTO `dic_areas` VALUES ('932', '320602', '崇川区', 'A', '930');
INSERT INTO `dic_areas` VALUES ('933', '320611', '港闸区', 'A', '930');
INSERT INTO `dic_areas` VALUES ('934', '320612', '通州区', 'A', '930');
INSERT INTO `dic_areas` VALUES ('935', '320621', '海安县', 'A', '930');
INSERT INTO `dic_areas` VALUES ('936', '320623', '如东县', 'A', '930');
INSERT INTO `dic_areas` VALUES ('937', '320681', '启东市', 'A', '930');
INSERT INTO `dic_areas` VALUES ('938', '320682', '如皋市', 'A', '930');
INSERT INTO `dic_areas` VALUES ('939', '320684', '海门市', 'A', '930');
INSERT INTO `dic_areas` VALUES ('940', '320700', '连云港市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('941', '320701', '市辖区', 'A', '940');
INSERT INTO `dic_areas` VALUES ('942', '320703', '连云区', 'A', '940');
INSERT INTO `dic_areas` VALUES ('943', '320706', '海州区', 'A', '940');
INSERT INTO `dic_areas` VALUES ('944', '320707', '赣榆区', 'A', '940');
INSERT INTO `dic_areas` VALUES ('945', '320722', '东海县', 'A', '940');
INSERT INTO `dic_areas` VALUES ('946', '320723', '灌云县', 'A', '940');
INSERT INTO `dic_areas` VALUES ('947', '320724', '灌南县', 'A', '940');
INSERT INTO `dic_areas` VALUES ('948', '320800', '淮安市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('949', '320801', '市辖区', 'A', '948');
INSERT INTO `dic_areas` VALUES ('950', '320803', '淮安区', 'A', '948');
INSERT INTO `dic_areas` VALUES ('951', '320804', '淮阴区', 'A', '948');
INSERT INTO `dic_areas` VALUES ('952', '320812', '清江浦区', 'A', '948');
INSERT INTO `dic_areas` VALUES ('953', '320813', '洪泽区', 'A', '948');
INSERT INTO `dic_areas` VALUES ('954', '320826', '涟水县', 'A', '948');
INSERT INTO `dic_areas` VALUES ('955', '320830', '盱眙县', 'A', '948');
INSERT INTO `dic_areas` VALUES ('956', '320831', '金湖县', 'A', '948');
INSERT INTO `dic_areas` VALUES ('957', '320900', '盐城市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('958', '320901', '市辖区', 'A', '957');
INSERT INTO `dic_areas` VALUES ('959', '320902', '亭湖区', 'A', '957');
INSERT INTO `dic_areas` VALUES ('960', '320903', '盐都区', 'A', '957');
INSERT INTO `dic_areas` VALUES ('961', '320904', '大丰区', 'A', '957');
INSERT INTO `dic_areas` VALUES ('962', '320921', '响水县', 'A', '957');
INSERT INTO `dic_areas` VALUES ('963', '320922', '滨海县', 'A', '957');
INSERT INTO `dic_areas` VALUES ('964', '320923', '阜宁县', 'A', '957');
INSERT INTO `dic_areas` VALUES ('965', '320924', '射阳县', 'A', '957');
INSERT INTO `dic_areas` VALUES ('966', '320925', '建湖县', 'A', '957');
INSERT INTO `dic_areas` VALUES ('967', '320981', '东台市', 'A', '957');
INSERT INTO `dic_areas` VALUES ('968', '321000', '扬州市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('969', '321001', '市辖区', 'A', '968');
INSERT INTO `dic_areas` VALUES ('970', '321002', '广陵区', 'A', '968');
INSERT INTO `dic_areas` VALUES ('971', '321003', '邗江区', 'A', '968');
INSERT INTO `dic_areas` VALUES ('972', '321012', '江都区', 'A', '968');
INSERT INTO `dic_areas` VALUES ('973', '321023', '宝应县', 'A', '968');
INSERT INTO `dic_areas` VALUES ('974', '321081', '仪征市', 'A', '968');
INSERT INTO `dic_areas` VALUES ('975', '321084', '高邮市', 'A', '968');
INSERT INTO `dic_areas` VALUES ('976', '321100', '镇江市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('977', '321101', '市辖区', 'A', '976');
INSERT INTO `dic_areas` VALUES ('978', '321102', '京口区', 'A', '976');
INSERT INTO `dic_areas` VALUES ('979', '321111', '润州区', 'A', '976');
INSERT INTO `dic_areas` VALUES ('980', '321112', '丹徒区', 'A', '976');
INSERT INTO `dic_areas` VALUES ('981', '321181', '丹阳市', 'A', '976');
INSERT INTO `dic_areas` VALUES ('982', '321182', '扬中市', 'A', '976');
INSERT INTO `dic_areas` VALUES ('983', '321183', '句容市', 'A', '976');
INSERT INTO `dic_areas` VALUES ('984', '321200', '泰州市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('985', '321201', '市辖区', 'A', '984');
INSERT INTO `dic_areas` VALUES ('986', '321202', '海陵区', 'A', '984');
INSERT INTO `dic_areas` VALUES ('987', '321203', '高港区', 'A', '984');
INSERT INTO `dic_areas` VALUES ('988', '321204', '姜堰区', 'A', '984');
INSERT INTO `dic_areas` VALUES ('989', '321281', '兴化市', 'A', '984');
INSERT INTO `dic_areas` VALUES ('990', '321282', '靖江市', 'A', '984');
INSERT INTO `dic_areas` VALUES ('991', '321283', '泰兴市', 'A', '984');
INSERT INTO `dic_areas` VALUES ('992', '321300', '宿迁市', 'C', '876');
INSERT INTO `dic_areas` VALUES ('993', '321301', '市辖区', 'A', '992');
INSERT INTO `dic_areas` VALUES ('994', '321302', '宿城区', 'A', '992');
INSERT INTO `dic_areas` VALUES ('995', '321311', '宿豫区', 'A', '992');
INSERT INTO `dic_areas` VALUES ('996', '321322', '沭阳县', 'A', '992');
INSERT INTO `dic_areas` VALUES ('997', '321323', '泗阳县', 'A', '992');
INSERT INTO `dic_areas` VALUES ('998', '321324', '泗洪县', 'A', '992');
INSERT INTO `dic_areas` VALUES ('999', '330000', '浙江省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1000', '330100', '杭州市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1001', '330101', '市辖区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1002', '330102', '上城区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1003', '330103', '下城区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1004', '330104', '江干区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1005', '330105', '拱墅区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1006', '330106', '西湖区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1007', '330108', '滨江区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1008', '330109', '萧山区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1009', '330110', '余杭区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1010', '330111', '富阳区', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1011', '330122', '桐庐县', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1012', '330127', '淳安县', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1013', '330182', '建德市', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1014', '330185', '临安市', 'A', '1000');
INSERT INTO `dic_areas` VALUES ('1015', '330200', '宁波市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1016', '330201', '市辖区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1017', '330203', '海曙区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1018', '330204', '江东区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1019', '330205', '江北区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1020', '330206', '北仑区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1021', '330211', '镇海区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1022', '330212', '鄞州区', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1023', '330225', '象山县', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1024', '330226', '宁海县', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1025', '330281', '余姚市', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1026', '330282', '慈溪市', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1027', '330283', '奉化市', 'A', '1015');
INSERT INTO `dic_areas` VALUES ('1028', '330300', '温州市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1029', '330301', '市辖区', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1030', '330302', '鹿城区', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1031', '330303', '龙湾区', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1032', '330304', '瓯海区', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1033', '330305', '洞头区', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1034', '330324', '永嘉县', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1035', '330326', '平阳县', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1036', '330327', '苍南县', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1037', '330328', '文成县', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1038', '330329', '泰顺县', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1039', '330381', '瑞安市', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1040', '330382', '乐清市', 'A', '1028');
INSERT INTO `dic_areas` VALUES ('1041', '330400', '嘉兴市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1042', '330401', '市辖区', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1043', '330402', '南湖区', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1044', '330411', '秀洲区', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1045', '330421', '嘉善县', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1046', '330424', '海盐县', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1047', '330481', '海宁市', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1048', '330482', '平湖市', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1049', '330483', '桐乡市', 'A', '1041');
INSERT INTO `dic_areas` VALUES ('1050', '330500', '湖州市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1051', '330501', '市辖区', 'A', '1050');
INSERT INTO `dic_areas` VALUES ('1052', '330502', '吴兴区', 'A', '1050');
INSERT INTO `dic_areas` VALUES ('1053', '330503', '南浔区', 'A', '1050');
INSERT INTO `dic_areas` VALUES ('1054', '330521', '德清县', 'A', '1050');
INSERT INTO `dic_areas` VALUES ('1055', '330522', '长兴县', 'A', '1050');
INSERT INTO `dic_areas` VALUES ('1056', '330523', '安吉县', 'A', '1050');
INSERT INTO `dic_areas` VALUES ('1057', '330600', '绍兴市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1058', '330601', '市辖区', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1059', '330602', '越城区', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1060', '330603', '柯桥区', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1061', '330604', '上虞区', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1062', '330624', '新昌县', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1063', '330681', '诸暨市', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1064', '330683', '嵊州市', 'A', '1057');
INSERT INTO `dic_areas` VALUES ('1065', '330700', '金华市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1066', '330701', '市辖区', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1067', '330702', '婺城区', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1068', '330703', '金东区', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1069', '330723', '武义县', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1070', '330726', '浦江县', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1071', '330727', '磐安县', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1072', '330781', '兰溪市', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1073', '330782', '义乌市', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1074', '330783', '东阳市', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1075', '330784', '永康市', 'A', '1065');
INSERT INTO `dic_areas` VALUES ('1076', '330800', '衢州市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1077', '330801', '市辖区', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1078', '330802', '柯城区', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1079', '330803', '衢江区', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1080', '330822', '常山县', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1081', '330824', '开化县', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1082', '330825', '龙游县', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1083', '330881', '江山市', 'A', '1076');
INSERT INTO `dic_areas` VALUES ('1084', '330900', '舟山市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1085', '330901', '市辖区', 'A', '1084');
INSERT INTO `dic_areas` VALUES ('1086', '330902', '定海区', 'A', '1084');
INSERT INTO `dic_areas` VALUES ('1087', '330903', '普陀区', 'A', '1084');
INSERT INTO `dic_areas` VALUES ('1088', '330921', '岱山县', 'A', '1084');
INSERT INTO `dic_areas` VALUES ('1089', '330922', '嵊泗县', 'A', '1084');
INSERT INTO `dic_areas` VALUES ('1090', '331000', '台州市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1091', '331001', '市辖区', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1092', '331002', '椒江区', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1093', '331003', '黄岩区', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1094', '331004', '路桥区', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1095', '331021', '玉环县', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1096', '331022', '三门县', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1097', '331023', '天台县', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1098', '331024', '仙居县', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1099', '331081', '温岭市', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1100', '331082', '临海市', 'A', '1090');
INSERT INTO `dic_areas` VALUES ('1101', '331100', '丽水市', 'C', '999');
INSERT INTO `dic_areas` VALUES ('1102', '331101', '市辖区', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1103', '331102', '莲都区', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1104', '331121', '青田县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1105', '331122', '缙云县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1106', '331123', '遂昌县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1107', '331124', '松阳县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1108', '331125', '云和县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1109', '331126', '庆元县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1110', '331127', '景宁畲族自治县', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1111', '331181', '龙泉市', 'A', '1101');
INSERT INTO `dic_areas` VALUES ('1112', '340000', '安徽省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1113', '340100', '合肥市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1114', '340101', '市辖区', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1115', '340102', '瑶海区', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1116', '340103', '庐阳区', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1117', '340104', '蜀山区', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1118', '340111', '包河区', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1119', '340121', '长丰县', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1120', '340122', '肥东县', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1121', '340123', '肥西县', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1122', '340124', '庐江县', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1123', '340181', '巢湖市', 'A', '1113');
INSERT INTO `dic_areas` VALUES ('1124', '340200', '芜湖市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1125', '340201', '市辖区', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1126', '340202', '镜湖区', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1127', '340203', '弋江区', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1128', '340207', '鸠江区', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1129', '340208', '三山区', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1130', '340221', '芜湖县', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1131', '340222', '繁昌县', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1132', '340223', '南陵县', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1133', '340225', '无为县', 'A', '1124');
INSERT INTO `dic_areas` VALUES ('1134', '340300', '蚌埠市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1135', '340301', '市辖区', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1136', '340302', '龙子湖区', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1137', '340303', '蚌山区', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1138', '340304', '禹会区', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1139', '340311', '淮上区', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1140', '340321', '怀远县', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1141', '340322', '五河县', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1142', '340323', '固镇县', 'A', '1134');
INSERT INTO `dic_areas` VALUES ('1143', '340400', '淮南市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1144', '340401', '市辖区', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1145', '340402', '大通区', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1146', '340403', '田家庵区', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1147', '340404', '谢家集区', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1148', '340405', '八公山区', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1149', '340406', '潘集区', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1150', '340421', '凤台县', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1151', '340422', '寿县', 'A', '1143');
INSERT INTO `dic_areas` VALUES ('1152', '340500', '马鞍山市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1153', '340501', '市辖区', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1154', '340503', '花山区', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1155', '340504', '雨山区', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1156', '340506', '博望区', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1157', '340521', '当涂县', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1158', '340522', '含山县', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1159', '340523', '和县', 'A', '1152');
INSERT INTO `dic_areas` VALUES ('1160', '340600', '淮北市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1161', '340601', '市辖区', 'A', '1160');
INSERT INTO `dic_areas` VALUES ('1162', '340602', '杜集区', 'A', '1160');
INSERT INTO `dic_areas` VALUES ('1163', '340603', '相山区', 'A', '1160');
INSERT INTO `dic_areas` VALUES ('1164', '340604', '烈山区', 'A', '1160');
INSERT INTO `dic_areas` VALUES ('1165', '340621', '濉溪县', 'A', '1160');
INSERT INTO `dic_areas` VALUES ('1166', '340700', '铜陵市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1167', '340701', '市辖区', 'A', '1166');
INSERT INTO `dic_areas` VALUES ('1168', '340705', '铜官区', 'A', '1166');
INSERT INTO `dic_areas` VALUES ('1169', '340706', '义安区', 'A', '1166');
INSERT INTO `dic_areas` VALUES ('1170', '340711', '郊区', 'A', '1166');
INSERT INTO `dic_areas` VALUES ('1171', '340722', '枞阳县', 'A', '1166');
INSERT INTO `dic_areas` VALUES ('1172', '340800', '安庆市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1173', '340801', '市辖区', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1174', '340802', '迎江区', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1175', '340803', '大观区', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1176', '340811', '宜秀区', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1177', '340822', '怀宁县', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1178', '340824', '潜山县', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1179', '340825', '太湖县', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1180', '340826', '宿松县', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1181', '340827', '望江县', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1182', '340828', '岳西县', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1183', '340881', '桐城市', 'A', '1172');
INSERT INTO `dic_areas` VALUES ('1184', '341000', '黄山市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1185', '341001', '市辖区', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1186', '341002', '屯溪区', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1187', '341003', '黄山区', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1188', '341004', '徽州区', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1189', '341021', '歙县', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1190', '341022', '休宁县', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1191', '341023', '黟县', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1192', '341024', '祁门县', 'A', '1184');
INSERT INTO `dic_areas` VALUES ('1193', '341100', '滁州市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1194', '341101', '市辖区', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1195', '341102', '琅琊区', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1196', '341103', '南谯区', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1197', '341122', '来安县', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1198', '341124', '全椒县', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1199', '341125', '定远县', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1200', '341126', '凤阳县', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1201', '341181', '天长市', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1202', '341182', '明光市', 'A', '1193');
INSERT INTO `dic_areas` VALUES ('1203', '341200', '阜阳市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1204', '341201', '市辖区', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1205', '341202', '颍州区', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1206', '341203', '颍东区', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1207', '341204', '颍泉区', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1208', '341221', '临泉县', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1209', '341222', '太和县', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1210', '341225', '阜南县', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1211', '341226', '颍上县', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1212', '341282', '界首市', 'A', '1203');
INSERT INTO `dic_areas` VALUES ('1213', '341300', '宿州市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1214', '341301', '市辖区', 'A', '1213');
INSERT INTO `dic_areas` VALUES ('1215', '341302', '埇桥区', 'A', '1213');
INSERT INTO `dic_areas` VALUES ('1216', '341321', '砀山县', 'A', '1213');
INSERT INTO `dic_areas` VALUES ('1217', '341322', '萧县', 'A', '1213');
INSERT INTO `dic_areas` VALUES ('1218', '341323', '灵璧县', 'A', '1213');
INSERT INTO `dic_areas` VALUES ('1219', '341324', '泗县', 'A', '1213');
INSERT INTO `dic_areas` VALUES ('1220', '341500', '六安市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1221', '341501', '市辖区', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1222', '341502', '金安区', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1223', '341503', '裕安区', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1224', '341504', '叶集区', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1225', '341522', '霍邱县', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1226', '341523', '舒城县', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1227', '341524', '金寨县', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1228', '341525', '霍山县', 'A', '1220');
INSERT INTO `dic_areas` VALUES ('1229', '341600', '亳州市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1230', '341601', '市辖区', 'A', '1229');
INSERT INTO `dic_areas` VALUES ('1231', '341602', '谯城区', 'A', '1229');
INSERT INTO `dic_areas` VALUES ('1232', '341621', '涡阳县', 'A', '1229');
INSERT INTO `dic_areas` VALUES ('1233', '341622', '蒙城县', 'A', '1229');
INSERT INTO `dic_areas` VALUES ('1234', '341623', '利辛县', 'A', '1229');
INSERT INTO `dic_areas` VALUES ('1235', '341700', '池州市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1236', '341701', '市辖区', 'A', '1235');
INSERT INTO `dic_areas` VALUES ('1237', '341702', '贵池区', 'A', '1235');
INSERT INTO `dic_areas` VALUES ('1238', '341721', '东至县', 'A', '1235');
INSERT INTO `dic_areas` VALUES ('1239', '341722', '石台县', 'A', '1235');
INSERT INTO `dic_areas` VALUES ('1240', '341723', '青阳县', 'A', '1235');
INSERT INTO `dic_areas` VALUES ('1241', '341800', '宣城市', 'C', '1112');
INSERT INTO `dic_areas` VALUES ('1242', '341801', '市辖区', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1243', '341802', '宣州区', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1244', '341821', '郎溪县', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1245', '341822', '广德县', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1246', '341823', '泾县', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1247', '341824', '绩溪县', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1248', '341825', '旌德县', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1249', '341881', '宁国市', 'A', '1241');
INSERT INTO `dic_areas` VALUES ('1250', '350000', '福建省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1251', '350100', '福州市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1252', '350101', '市辖区', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1253', '350102', '鼓楼区', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1254', '350103', '台江区', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1255', '350104', '仓山区', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1256', '350105', '马尾区', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1257', '350111', '晋安区', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1258', '350121', '闽侯县', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1259', '350122', '连江县', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1260', '350123', '罗源县', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1261', '350124', '闽清县', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1262', '350125', '永泰县', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1263', '350128', '平潭县', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1264', '350181', '福清市', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1265', '350182', '长乐市', 'A', '1251');
INSERT INTO `dic_areas` VALUES ('1266', '350200', '厦门市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1267', '350201', '市辖区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1268', '350203', '思明区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1269', '350205', '海沧区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1270', '350206', '湖里区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1271', '350211', '集美区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1272', '350212', '同安区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1273', '350213', '翔安区', 'A', '1266');
INSERT INTO `dic_areas` VALUES ('1274', '350300', '莆田市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1275', '350301', '市辖区', 'A', '1274');
INSERT INTO `dic_areas` VALUES ('1276', '350302', '城厢区', 'A', '1274');
INSERT INTO `dic_areas` VALUES ('1277', '350303', '涵江区', 'A', '1274');
INSERT INTO `dic_areas` VALUES ('1278', '350304', '荔城区', 'A', '1274');
INSERT INTO `dic_areas` VALUES ('1279', '350305', '秀屿区', 'A', '1274');
INSERT INTO `dic_areas` VALUES ('1280', '350322', '仙游县', 'A', '1274');
INSERT INTO `dic_areas` VALUES ('1281', '350400', '三明市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1282', '350401', '市辖区', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1283', '350402', '梅列区', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1284', '350403', '三元区', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1285', '350421', '明溪县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1286', '350423', '清流县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1287', '350424', '宁化县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1288', '350425', '大田县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1289', '350426', '尤溪县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1290', '350427', '沙县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1291', '350428', '将乐县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1292', '350429', '泰宁县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1293', '350430', '建宁县', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1294', '350481', '永安市', 'A', '1281');
INSERT INTO `dic_areas` VALUES ('1295', '350500', '泉州市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1296', '350501', '市辖区', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1297', '350502', '鲤城区', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1298', '350503', '丰泽区', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1299', '350504', '洛江区', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1300', '350505', '泉港区', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1301', '350521', '惠安县', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1302', '350524', '安溪县', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1303', '350525', '永春县', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1304', '350526', '德化县', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1305', '350527', '金门县', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1306', '350581', '石狮市', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1307', '350582', '晋江市', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1308', '350583', '南安市', 'A', '1295');
INSERT INTO `dic_areas` VALUES ('1309', '350600', '漳州市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1310', '350601', '市辖区', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1311', '350602', '芗城区', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1312', '350603', '龙文区', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1313', '350622', '云霄县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1314', '350623', '漳浦县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1315', '350624', '诏安县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1316', '350625', '长泰县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1317', '350626', '东山县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1318', '350627', '南靖县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1319', '350628', '平和县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1320', '350629', '华安县', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1321', '350681', '龙海市', 'A', '1309');
INSERT INTO `dic_areas` VALUES ('1322', '350700', '南平市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1323', '350701', '市辖区', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1324', '350702', '延平区', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1325', '350703', '建阳区', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1326', '350721', '顺昌县', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1327', '350722', '浦城县', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1328', '350723', '光泽县', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1329', '350724', '松溪县', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1330', '350725', '政和县', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1331', '350781', '邵武市', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1332', '350782', '武夷山市', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1333', '350783', '建瓯市', 'A', '1322');
INSERT INTO `dic_areas` VALUES ('1334', '350800', '龙岩市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1335', '350801', '市辖区', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1336', '350802', '新罗区', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1337', '350803', '永定区', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1338', '350821', '长汀县', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1339', '350823', '上杭县', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1340', '350824', '武平县', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1341', '350825', '连城县', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1342', '350881', '漳平市', 'A', '1334');
INSERT INTO `dic_areas` VALUES ('1343', '350900', '宁德市', 'C', '1250');
INSERT INTO `dic_areas` VALUES ('1344', '350901', '市辖区', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1345', '350902', '蕉城区', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1346', '350921', '霞浦县', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1347', '350922', '古田县', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1348', '350923', '屏南县', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1349', '350924', '寿宁县', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1350', '350925', '周宁县', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1351', '350926', '柘荣县', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1352', '350981', '福安市', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1353', '350982', '福鼎市', 'A', '1343');
INSERT INTO `dic_areas` VALUES ('1354', '360000', '江西省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1355', '360100', '南昌市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1356', '360101', '市辖区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1357', '360102', '东湖区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1358', '360103', '西湖区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1359', '360104', '青云谱区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1360', '360105', '湾里区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1361', '360111', '青山湖区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1362', '360112', '新建区', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1363', '360121', '南昌县', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1364', '360123', '安义县', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1365', '360124', '进贤县', 'A', '1355');
INSERT INTO `dic_areas` VALUES ('1366', '360200', '景德镇市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1367', '360201', '市辖区', 'A', '1366');
INSERT INTO `dic_areas` VALUES ('1368', '360202', '昌江区', 'A', '1366');
INSERT INTO `dic_areas` VALUES ('1369', '360203', '珠山区', 'A', '1366');
INSERT INTO `dic_areas` VALUES ('1370', '360222', '浮梁县', 'A', '1366');
INSERT INTO `dic_areas` VALUES ('1371', '360281', '乐平市', 'A', '1366');
INSERT INTO `dic_areas` VALUES ('1372', '360300', '萍乡市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1373', '360301', '市辖区', 'A', '1372');
INSERT INTO `dic_areas` VALUES ('1374', '360302', '安源区', 'A', '1372');
INSERT INTO `dic_areas` VALUES ('1375', '360313', '湘东区', 'A', '1372');
INSERT INTO `dic_areas` VALUES ('1376', '360321', '莲花县', 'A', '1372');
INSERT INTO `dic_areas` VALUES ('1377', '360322', '上栗县', 'A', '1372');
INSERT INTO `dic_areas` VALUES ('1378', '360323', '芦溪县', 'A', '1372');
INSERT INTO `dic_areas` VALUES ('1379', '360400', '九江市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1380', '360401', '市辖区', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1381', '360402', '濂溪区', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1382', '360403', '浔阳区', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1383', '360421', '九江县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1384', '360423', '武宁县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1385', '360424', '修水县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1386', '360425', '永修县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1387', '360426', '德安县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1388', '360428', '都昌县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1389', '360429', '湖口县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1390', '360430', '彭泽县', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1391', '360481', '瑞昌市', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1392', '360482', '共青城市', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1393', '360483', '庐山市', 'A', '1379');
INSERT INTO `dic_areas` VALUES ('1394', '360500', '新余市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1395', '360501', '市辖区', 'A', '1394');
INSERT INTO `dic_areas` VALUES ('1396', '360502', '渝水区', 'A', '1394');
INSERT INTO `dic_areas` VALUES ('1397', '360521', '分宜县', 'A', '1394');
INSERT INTO `dic_areas` VALUES ('1398', '360600', '鹰潭市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1399', '360601', '市辖区', 'A', '1398');
INSERT INTO `dic_areas` VALUES ('1400', '360602', '月湖区', 'A', '1398');
INSERT INTO `dic_areas` VALUES ('1401', '360622', '余江县', 'A', '1398');
INSERT INTO `dic_areas` VALUES ('1402', '360681', '贵溪市', 'A', '1398');
INSERT INTO `dic_areas` VALUES ('1403', '360700', '赣州市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1404', '360701', '市辖区', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1405', '360702', '章贡区', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1406', '360703', '南康区', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1407', '360721', '赣县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1408', '360722', '信丰县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1409', '360723', '大余县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1410', '360724', '上犹县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1411', '360725', '崇义县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1412', '360726', '安远县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1413', '360727', '龙南县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1414', '360728', '定南县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1415', '360729', '全南县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1416', '360730', '宁都县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1417', '360731', '于都县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1418', '360732', '兴国县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1419', '360733', '会昌县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1420', '360734', '寻乌县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1421', '360735', '石城县', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1422', '360781', '瑞金市', 'A', '1403');
INSERT INTO `dic_areas` VALUES ('1423', '360800', '吉安市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1424', '360801', '市辖区', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1425', '360802', '吉州区', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1426', '360803', '青原区', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1427', '360821', '吉安县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1428', '360822', '吉水县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1429', '360823', '峡江县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1430', '360824', '新干县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1431', '360825', '永丰县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1432', '360826', '泰和县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1433', '360827', '遂川县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1434', '360828', '万安县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1435', '360829', '安福县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1436', '360830', '永新县', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1437', '360881', '井冈山市', 'A', '1423');
INSERT INTO `dic_areas` VALUES ('1438', '360900', '宜春市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1439', '360901', '市辖区', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1440', '360902', '袁州区', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1441', '360921', '奉新县', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1442', '360922', '万载县', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1443', '360923', '上高县', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1444', '360924', '宜丰县', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1445', '360925', '靖安县', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1446', '360926', '铜鼓县', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1447', '360981', '丰城市', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1448', '360982', '樟树市', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1449', '360983', '高安市', 'A', '1438');
INSERT INTO `dic_areas` VALUES ('1450', '361000', '抚州市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1451', '361001', '市辖区', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1452', '361002', '临川区', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1453', '361021', '南城县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1454', '361022', '黎川县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1455', '361023', '南丰县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1456', '361024', '崇仁县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1457', '361025', '乐安县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1458', '361026', '宜黄县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1459', '361027', '金溪县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1460', '361028', '资溪县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1461', '361029', '东乡县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1462', '361030', '广昌县', 'A', '1450');
INSERT INTO `dic_areas` VALUES ('1463', '361100', '上饶市', 'C', '1354');
INSERT INTO `dic_areas` VALUES ('1464', '361101', '市辖区', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1465', '361102', '信州区', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1466', '361103', '广丰区', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1467', '361121', '上饶县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1468', '361123', '玉山县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1469', '361124', '铅山县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1470', '361125', '横峰县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1471', '361126', '弋阳县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1472', '361127', '余干县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1473', '361128', '鄱阳县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1474', '361129', '万年县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1475', '361130', '婺源县', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1476', '361181', '德兴市', 'A', '1463');
INSERT INTO `dic_areas` VALUES ('1477', '370000', '山东省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1478', '370100', '济南市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1479', '370101', '市辖区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1480', '370102', '历下区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1481', '370103', '市中区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1482', '370104', '槐荫区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1483', '370105', '天桥区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1484', '370112', '历城区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1485', '370113', '长清区', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1486', '370124', '平阴县', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1487', '370125', '济阳县', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1488', '370126', '商河县', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1489', '370181', '章丘市', 'A', '1478');
INSERT INTO `dic_areas` VALUES ('1490', '370200', '青岛市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1491', '370201', '市辖区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1492', '370202', '市南区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1493', '370203', '市北区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1494', '370211', '黄岛区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1495', '370212', '崂山区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1496', '370213', '李沧区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1497', '370214', '城阳区', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1498', '370281', '胶州市', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1499', '370282', '即墨市', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1500', '370283', '平度市', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1501', '370285', '莱西市', 'A', '1490');
INSERT INTO `dic_areas` VALUES ('1502', '370300', '淄博市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1503', '370301', '市辖区', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1504', '370302', '淄川区', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1505', '370303', '张店区', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1506', '370304', '博山区', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1507', '370305', '临淄区', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1508', '370306', '周村区', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1509', '370321', '桓台县', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1510', '370322', '高青县', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1511', '370323', '沂源县', 'A', '1502');
INSERT INTO `dic_areas` VALUES ('1512', '370400', '枣庄市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1513', '370401', '市辖区', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1514', '370402', '市中区', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1515', '370403', '薛城区', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1516', '370404', '峄城区', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1517', '370405', '台儿庄区', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1518', '370406', '山亭区', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1519', '370481', '滕州市', 'A', '1512');
INSERT INTO `dic_areas` VALUES ('1520', '370500', '东营市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1521', '370501', '市辖区', 'A', '1520');
INSERT INTO `dic_areas` VALUES ('1522', '370502', '东营区', 'A', '1520');
INSERT INTO `dic_areas` VALUES ('1523', '370503', '河口区', 'A', '1520');
INSERT INTO `dic_areas` VALUES ('1524', '370505', '垦利区', 'A', '1520');
INSERT INTO `dic_areas` VALUES ('1525', '370522', '利津县', 'A', '1520');
INSERT INTO `dic_areas` VALUES ('1526', '370523', '广饶县', 'A', '1520');
INSERT INTO `dic_areas` VALUES ('1527', '370600', '烟台市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1528', '370601', '市辖区', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1529', '370602', '芝罘区', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1530', '370611', '福山区', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1531', '370612', '牟平区', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1532', '370613', '莱山区', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1533', '370634', '长岛县', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1534', '370681', '龙口市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1535', '370682', '莱阳市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1536', '370683', '莱州市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1537', '370684', '蓬莱市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1538', '370685', '招远市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1539', '370686', '栖霞市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1540', '370687', '海阳市', 'A', '1527');
INSERT INTO `dic_areas` VALUES ('1541', '370700', '潍坊市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1542', '370701', '市辖区', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1543', '370702', '潍城区', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1544', '370703', '寒亭区', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1545', '370704', '坊子区', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1546', '370705', '奎文区', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1547', '370724', '临朐县', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1548', '370725', '昌乐县', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1549', '370781', '青州市', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1550', '370782', '诸城市', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1551', '370783', '寿光市', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1552', '370784', '安丘市', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1553', '370785', '高密市', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1554', '370786', '昌邑市', 'A', '1541');
INSERT INTO `dic_areas` VALUES ('1555', '370800', '济宁市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1556', '370801', '市辖区', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1557', '370811', '任城区', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1558', '370812', '兖州区', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1559', '370826', '微山县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1560', '370827', '鱼台县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1561', '370828', '金乡县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1562', '370829', '嘉祥县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1563', '370830', '汶上县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1564', '370831', '泗水县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1565', '370832', '梁山县', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1566', '370881', '曲阜市', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1567', '370883', '邹城市', 'A', '1555');
INSERT INTO `dic_areas` VALUES ('1568', '370900', '泰安市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1569', '370901', '市辖区', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1570', '370902', '泰山区', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1571', '370911', '岱岳区', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1572', '370921', '宁阳县', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1573', '370923', '东平县', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1574', '370982', '新泰市', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1575', '370983', '肥城市', 'A', '1568');
INSERT INTO `dic_areas` VALUES ('1576', '371000', '威海市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1577', '371001', '市辖区', 'A', '1576');
INSERT INTO `dic_areas` VALUES ('1578', '371002', '环翠区', 'A', '1576');
INSERT INTO `dic_areas` VALUES ('1579', '371003', '文登区', 'A', '1576');
INSERT INTO `dic_areas` VALUES ('1580', '371082', '荣成市', 'A', '1576');
INSERT INTO `dic_areas` VALUES ('1581', '371083', '乳山市', 'A', '1576');
INSERT INTO `dic_areas` VALUES ('1582', '371100', '日照市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1583', '371101', '市辖区', 'A', '1582');
INSERT INTO `dic_areas` VALUES ('1584', '371102', '东港区', 'A', '1582');
INSERT INTO `dic_areas` VALUES ('1585', '371103', '岚山区', 'A', '1582');
INSERT INTO `dic_areas` VALUES ('1586', '371121', '五莲县', 'A', '1582');
INSERT INTO `dic_areas` VALUES ('1587', '371122', '莒县', 'A', '1582');
INSERT INTO `dic_areas` VALUES ('1588', '371200', '莱芜市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1589', '371201', '市辖区', 'A', '1588');
INSERT INTO `dic_areas` VALUES ('1590', '371202', '莱城区', 'A', '1588');
INSERT INTO `dic_areas` VALUES ('1591', '371203', '钢城区', 'A', '1588');
INSERT INTO `dic_areas` VALUES ('1592', '371300', '临沂市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1593', '371301', '市辖区', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1594', '371302', '兰山区', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1595', '371311', '罗庄区', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1596', '371312', '河东区', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1597', '371321', '沂南县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1598', '371322', '郯城县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1599', '371323', '沂水县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1600', '371324', '兰陵县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1601', '371325', '费县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1602', '371326', '平邑县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1603', '371327', '莒南县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1604', '371328', '蒙阴县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1605', '371329', '临沭县', 'A', '1592');
INSERT INTO `dic_areas` VALUES ('1606', '371400', '德州市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1607', '371401', '市辖区', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1608', '371402', '德城区', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1609', '371403', '陵城区', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1610', '371422', '宁津县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1611', '371423', '庆云县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1612', '371424', '临邑县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1613', '371425', '齐河县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1614', '371426', '平原县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1615', '371427', '夏津县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1616', '371428', '武城县', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1617', '371481', '乐陵市', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1618', '371482', '禹城市', 'A', '1606');
INSERT INTO `dic_areas` VALUES ('1619', '371500', '聊城市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1620', '371501', '市辖区', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1621', '371502', '东昌府区', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1622', '371521', '阳谷县', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1623', '371522', '莘县', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1624', '371523', '茌平县', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1625', '371524', '东阿县', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1626', '371525', '冠县', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1627', '371526', '高唐县', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1628', '371581', '临清市', 'A', '1619');
INSERT INTO `dic_areas` VALUES ('1629', '371600', '滨州市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1630', '371601', '市辖区', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1631', '371602', '滨城区', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1632', '371603', '沾化区', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1633', '371621', '惠民县', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1634', '371622', '阳信县', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1635', '371623', '无棣县', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1636', '371625', '博兴县', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1637', '371626', '邹平县', 'A', '1629');
INSERT INTO `dic_areas` VALUES ('1638', '371700', '菏泽市', 'C', '1477');
INSERT INTO `dic_areas` VALUES ('1639', '371701', '市辖区', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1640', '371702', '牡丹区', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1641', '371703', '定陶区', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1642', '371721', '曹县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1643', '371722', '单县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1644', '371723', '成武县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1645', '371724', '巨野县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1646', '371725', '郓城县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1647', '371726', '鄄城县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1648', '371728', '东明县', 'A', '1638');
INSERT INTO `dic_areas` VALUES ('1649', '410000', '河南省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1650', '410100', '郑州市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1651', '410101', '市辖区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1652', '410102', '中原区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1653', '410103', '二七区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1654', '410104', '管城回族区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1655', '410105', '金水区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1656', '410106', '上街区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1657', '410108', '惠济区', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1658', '410122', '中牟县', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1659', '410181', '巩义市', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1660', '410182', '荥阳市', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1661', '410183', '新密市', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1662', '410184', '新郑市', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1663', '410185', '登封市', 'A', '1650');
INSERT INTO `dic_areas` VALUES ('1664', '410200', '开封市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1665', '410201', '市辖区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1666', '410202', '龙亭区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1667', '410203', '顺河回族区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1668', '410204', '鼓楼区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1669', '410205', '禹王台区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1670', '410211', '金明区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1671', '410212', '祥符区', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1672', '410221', '杞县', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1673', '410222', '通许县', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1674', '410223', '尉氏县', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1675', '410225', '兰考县', 'A', '1664');
INSERT INTO `dic_areas` VALUES ('1676', '410300', '洛阳市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1677', '410301', '市辖区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1678', '410302', '老城区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1679', '410303', '西工区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1680', '410304', '瀍河回族区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1681', '410305', '涧西区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1682', '410306', '吉利区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1683', '410311', '洛龙区', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1684', '410322', '孟津县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1685', '410323', '新安县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1686', '410324', '栾川县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1687', '410325', '嵩县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1688', '410326', '汝阳县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1689', '410327', '宜阳县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1690', '410328', '洛宁县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1691', '410329', '伊川县', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1692', '410381', '偃师市', 'A', '1676');
INSERT INTO `dic_areas` VALUES ('1693', '410400', '平顶山市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1694', '410401', '市辖区', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1695', '410402', '新华区', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1696', '410403', '卫东区', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1697', '410404', '石龙区', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1698', '410411', '湛河区', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1699', '410421', '宝丰县', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1700', '410422', '叶县', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1701', '410423', '鲁山县', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1702', '410425', '郏县', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1703', '410481', '舞钢市', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1704', '410482', '汝州市', 'A', '1693');
INSERT INTO `dic_areas` VALUES ('1705', '410500', '安阳市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1706', '410501', '市辖区', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1707', '410502', '文峰区', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1708', '410503', '北关区', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1709', '410505', '殷都区', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1710', '410506', '龙安区', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1711', '410522', '安阳县', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1712', '410523', '汤阴县', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1713', '410526', '滑县', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1714', '410527', '内黄县', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1715', '410581', '林州市', 'A', '1705');
INSERT INTO `dic_areas` VALUES ('1716', '410600', '鹤壁市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1717', '410601', '市辖区', 'A', '1716');
INSERT INTO `dic_areas` VALUES ('1718', '410602', '鹤山区', 'A', '1716');
INSERT INTO `dic_areas` VALUES ('1719', '410603', '山城区', 'A', '1716');
INSERT INTO `dic_areas` VALUES ('1720', '410611', '淇滨区', 'A', '1716');
INSERT INTO `dic_areas` VALUES ('1721', '410621', '浚县', 'A', '1716');
INSERT INTO `dic_areas` VALUES ('1722', '410622', '淇县', 'A', '1716');
INSERT INTO `dic_areas` VALUES ('1723', '410700', '新乡市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1724', '410701', '市辖区', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1725', '410702', '红旗区', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1726', '410703', '卫滨区', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1727', '410704', '凤泉区', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1728', '410711', '牧野区', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1729', '410721', '新乡县', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1730', '410724', '获嘉县', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1731', '410725', '原阳县', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1732', '410726', '延津县', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1733', '410727', '封丘县', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1734', '410728', '长垣县', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1735', '410781', '卫辉市', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1736', '410782', '辉县市', 'A', '1723');
INSERT INTO `dic_areas` VALUES ('1737', '410800', '焦作市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1738', '410801', '市辖区', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1739', '410802', '解放区', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1740', '410803', '中站区', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1741', '410804', '马村区', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1742', '410811', '山阳区', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1743', '410821', '修武县', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1744', '410822', '博爱县', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1745', '410823', '武陟县', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1746', '410825', '温县', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1747', '410882', '沁阳市', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1748', '410883', '孟州市', 'A', '1737');
INSERT INTO `dic_areas` VALUES ('1749', '410900', '濮阳市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1750', '410901', '市辖区', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1751', '410902', '华龙区', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1752', '410922', '清丰县', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1753', '410923', '南乐县', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1754', '410926', '范县', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1755', '410927', '台前县', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1756', '410928', '濮阳县', 'A', '1749');
INSERT INTO `dic_areas` VALUES ('1757', '411000', '许昌市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1758', '411001', '市辖区', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1759', '411002', '魏都区', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1760', '411023', '许昌县', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1761', '411024', '鄢陵县', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1762', '411025', '襄城县', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1763', '411081', '禹州市', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1764', '411082', '长葛市', 'A', '1757');
INSERT INTO `dic_areas` VALUES ('1765', '411100', '漯河市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1766', '411101', '市辖区', 'A', '1765');
INSERT INTO `dic_areas` VALUES ('1767', '411102', '源汇区', 'A', '1765');
INSERT INTO `dic_areas` VALUES ('1768', '411103', '郾城区', 'A', '1765');
INSERT INTO `dic_areas` VALUES ('1769', '411104', '召陵区', 'A', '1765');
INSERT INTO `dic_areas` VALUES ('1770', '411121', '舞阳县', 'A', '1765');
INSERT INTO `dic_areas` VALUES ('1771', '411122', '临颍县', 'A', '1765');
INSERT INTO `dic_areas` VALUES ('1772', '411200', '三门峡市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1773', '411201', '市辖区', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1774', '411202', '湖滨区', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1775', '411203', '陕州区', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1776', '411221', '渑池县', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1777', '411224', '卢氏县', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1778', '411281', '义马市', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1779', '411282', '灵宝市', 'A', '1772');
INSERT INTO `dic_areas` VALUES ('1780', '411300', '南阳市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1781', '411301', '市辖区', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1782', '411302', '宛城区', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1783', '411303', '卧龙区', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1784', '411321', '南召县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1785', '411322', '方城县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1786', '411323', '西峡县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1787', '411324', '镇平县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1788', '411325', '内乡县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1789', '411326', '淅川县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1790', '411327', '社旗县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1791', '411328', '唐河县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1792', '411329', '新野县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1793', '411330', '桐柏县', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1794', '411381', '邓州市', 'A', '1780');
INSERT INTO `dic_areas` VALUES ('1795', '411400', '商丘市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1796', '411401', '市辖区', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1797', '411402', '梁园区', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1798', '411403', '睢阳区', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1799', '411421', '民权县', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1800', '411422', '睢县', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1801', '411423', '宁陵县', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1802', '411424', '柘城县', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1803', '411425', '虞城县', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1804', '411426', '夏邑县', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1805', '411481', '永城市', 'A', '1795');
INSERT INTO `dic_areas` VALUES ('1806', '411500', '信阳市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1807', '411501', '市辖区', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1808', '411502', '浉河区', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1809', '411503', '平桥区', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1810', '411521', '罗山县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1811', '411522', '光山县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1812', '411523', '新县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1813', '411524', '商城县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1814', '411525', '固始县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1815', '411526', '潢川县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1816', '411527', '淮滨县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1817', '411528', '息县', 'A', '1806');
INSERT INTO `dic_areas` VALUES ('1818', '411600', '周口市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1819', '411601', '市辖区', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1820', '411602', '川汇区', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1821', '411621', '扶沟县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1822', '411622', '西华县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1823', '411623', '商水县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1824', '411624', '沈丘县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1825', '411625', '郸城县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1826', '411626', '淮阳县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1827', '411627', '太康县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1828', '411628', '鹿邑县', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1829', '411681', '项城市', 'A', '1818');
INSERT INTO `dic_areas` VALUES ('1830', '411700', '驻马店市', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1831', '411701', '市辖区', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1832', '411702', '驿城区', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1833', '411721', '西平县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1834', '411722', '上蔡县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1835', '411723', '平舆县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1836', '411724', '正阳县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1837', '411725', '确山县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1838', '411726', '泌阳县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1839', '411727', '汝南县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1840', '411728', '遂平县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1841', '411729', '新蔡县', 'A', '1830');
INSERT INTO `dic_areas` VALUES ('1842', '419000', '省直辖县级行政区划', 'C', '1649');
INSERT INTO `dic_areas` VALUES ('1843', '419001', '济源市', 'A', '1842');
INSERT INTO `dic_areas` VALUES ('1844', '420000', '湖北省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1845', '420100', '武汉市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1846', '420101', '市辖区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1847', '420102', '江岸区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1848', '420103', '江汉区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1849', '420104', '硚口区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1850', '420105', '汉阳区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1851', '420106', '武昌区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1852', '420107', '青山区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1853', '420111', '洪山区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1854', '420112', '东西湖区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1855', '420113', '汉南区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1856', '420114', '蔡甸区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1857', '420115', '江夏区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1858', '420116', '黄陂区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1859', '420117', '新洲区', 'A', '1845');
INSERT INTO `dic_areas` VALUES ('1860', '420200', '黄石市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1861', '420201', '市辖区', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1862', '420202', '黄石港区', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1863', '420203', '西塞山区', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1864', '420204', '下陆区', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1865', '420205', '铁山区', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1866', '420222', '阳新县', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1867', '420281', '大冶市', 'A', '1860');
INSERT INTO `dic_areas` VALUES ('1868', '420300', '十堰市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1869', '420301', '市辖区', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1870', '420302', '茅箭区', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1871', '420303', '张湾区', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1872', '420304', '郧阳区', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1873', '420322', '郧西县', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1874', '420323', '竹山县', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1875', '420324', '竹溪县', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1876', '420325', '房县', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1877', '420381', '丹江口市', 'A', '1868');
INSERT INTO `dic_areas` VALUES ('1878', '420500', '宜昌市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1879', '420501', '市辖区', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1880', '420502', '西陵区', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1881', '420503', '伍家岗区', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1882', '420504', '点军区', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1883', '420505', '猇亭区', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1884', '420506', '夷陵区', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1885', '420525', '远安县', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1886', '420526', '兴山县', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1887', '420527', '秭归县', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1888', '420528', '长阳土家族自治县', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1889', '420529', '五峰土家族自治县', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1890', '420581', '宜都市', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1891', '420582', '当阳市', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1892', '420583', '枝江市', 'A', '1878');
INSERT INTO `dic_areas` VALUES ('1893', '420600', '襄阳市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1894', '420601', '市辖区', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1895', '420602', '襄城区', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1896', '420606', '樊城区', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1897', '420607', '襄州区', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1898', '420624', '南漳县', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1899', '420625', '谷城县', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1900', '420626', '保康县', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1901', '420682', '老河口市', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1902', '420683', '枣阳市', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1903', '420684', '宜城市', 'A', '1893');
INSERT INTO `dic_areas` VALUES ('1904', '420700', '鄂州市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1905', '420701', '市辖区', 'A', '1904');
INSERT INTO `dic_areas` VALUES ('1906', '420702', '梁子湖区', 'A', '1904');
INSERT INTO `dic_areas` VALUES ('1907', '420703', '华容区', 'A', '1904');
INSERT INTO `dic_areas` VALUES ('1908', '420704', '鄂城区', 'A', '1904');
INSERT INTO `dic_areas` VALUES ('1909', '420800', '荆门市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1910', '420801', '市辖区', 'A', '1909');
INSERT INTO `dic_areas` VALUES ('1911', '420802', '东宝区', 'A', '1909');
INSERT INTO `dic_areas` VALUES ('1912', '420804', '掇刀区', 'A', '1909');
INSERT INTO `dic_areas` VALUES ('1913', '420821', '京山县', 'A', '1909');
INSERT INTO `dic_areas` VALUES ('1914', '420822', '沙洋县', 'A', '1909');
INSERT INTO `dic_areas` VALUES ('1915', '420881', '钟祥市', 'A', '1909');
INSERT INTO `dic_areas` VALUES ('1916', '420900', '孝感市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1917', '420901', '市辖区', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1918', '420902', '孝南区', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1919', '420921', '孝昌县', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1920', '420922', '大悟县', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1921', '420923', '云梦县', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1922', '420981', '应城市', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1923', '420982', '安陆市', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1924', '420984', '汉川市', 'A', '1916');
INSERT INTO `dic_areas` VALUES ('1925', '421000', '荆州市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1926', '421001', '市辖区', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1927', '421002', '沙市区', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1928', '421003', '荆州区', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1929', '421022', '公安县', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1930', '421023', '监利县', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1931', '421024', '江陵县', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1932', '421081', '石首市', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1933', '421083', '洪湖市', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1934', '421087', '松滋市', 'A', '1925');
INSERT INTO `dic_areas` VALUES ('1935', '421100', '黄冈市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1936', '421101', '市辖区', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1937', '421102', '黄州区', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1938', '421121', '团风县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1939', '421122', '红安县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1940', '421123', '罗田县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1941', '421124', '英山县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1942', '421125', '浠水县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1943', '421126', '蕲春县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1944', '421127', '黄梅县', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1945', '421181', '麻城市', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1946', '421182', '武穴市', 'A', '1935');
INSERT INTO `dic_areas` VALUES ('1947', '421200', '咸宁市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1948', '421201', '市辖区', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1949', '421202', '咸安区', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1950', '421221', '嘉鱼县', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1951', '421222', '通城县', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1952', '421223', '崇阳县', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1953', '421224', '通山县', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1954', '421281', '赤壁市', 'A', '1947');
INSERT INTO `dic_areas` VALUES ('1955', '421300', '随州市', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1956', '421301', '市辖区', 'A', '1955');
INSERT INTO `dic_areas` VALUES ('1957', '421303', '曾都区', 'A', '1955');
INSERT INTO `dic_areas` VALUES ('1958', '421321', '随县', 'A', '1955');
INSERT INTO `dic_areas` VALUES ('1959', '421381', '广水市', 'A', '1955');
INSERT INTO `dic_areas` VALUES ('1960', '422800', '恩施土家族苗族自治州', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1961', '422801', '恩施市', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1962', '422802', '利川市', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1963', '422822', '建始县', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1964', '422823', '巴东县', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1965', '422825', '宣恩县', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1966', '422826', '咸丰县', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1967', '422827', '来凤县', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1968', '422828', '鹤峰县', 'A', '1960');
INSERT INTO `dic_areas` VALUES ('1969', '429000', '省直辖县级行政区划', 'C', '1844');
INSERT INTO `dic_areas` VALUES ('1970', '429004', '仙桃市', 'A', '1969');
INSERT INTO `dic_areas` VALUES ('1971', '429005', '潜江市', 'A', '1969');
INSERT INTO `dic_areas` VALUES ('1972', '429006', '天门市', 'A', '1969');
INSERT INTO `dic_areas` VALUES ('1973', '429021', '神农架林区', 'A', '1969');
INSERT INTO `dic_areas` VALUES ('1974', '430000', '湖南省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('1975', '430100', '长沙市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('1976', '430101', '市辖区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1977', '430102', '芙蓉区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1978', '430103', '天心区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1979', '430104', '岳麓区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1980', '430105', '开福区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1981', '430111', '雨花区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1982', '430112', '望城区', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1983', '430121', '长沙县', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1984', '430124', '宁乡县', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1985', '430181', '浏阳市', 'A', '1975');
INSERT INTO `dic_areas` VALUES ('1986', '430200', '株洲市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('1987', '430201', '市辖区', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1988', '430202', '荷塘区', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1989', '430203', '芦淞区', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1990', '430204', '石峰区', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1991', '430211', '天元区', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1992', '430221', '株洲县', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1993', '430223', '攸县', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1994', '430224', '茶陵县', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1995', '430225', '炎陵县', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1996', '430281', '醴陵市', 'A', '1986');
INSERT INTO `dic_areas` VALUES ('1997', '430300', '湘潭市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('1998', '430301', '市辖区', 'A', '1997');
INSERT INTO `dic_areas` VALUES ('1999', '430302', '雨湖区', 'A', '1997');
INSERT INTO `dic_areas` VALUES ('2000', '430304', '岳塘区', 'A', '1997');
INSERT INTO `dic_areas` VALUES ('2001', '430321', '湘潭县', 'A', '1997');
INSERT INTO `dic_areas` VALUES ('2002', '430381', '湘乡市', 'A', '1997');
INSERT INTO `dic_areas` VALUES ('2003', '430382', '韶山市', 'A', '1997');
INSERT INTO `dic_areas` VALUES ('2004', '430400', '衡阳市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2005', '430401', '市辖区', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2006', '430405', '珠晖区', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2007', '430406', '雁峰区', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2008', '430407', '石鼓区', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2009', '430408', '蒸湘区', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2010', '430412', '南岳区', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2011', '430421', '衡阳县', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2012', '430422', '衡南县', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2013', '430423', '衡山县', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2014', '430424', '衡东县', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2015', '430426', '祁东县', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2016', '430481', '耒阳市', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2017', '430482', '常宁市', 'A', '2004');
INSERT INTO `dic_areas` VALUES ('2018', '430500', '邵阳市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2019', '430501', '市辖区', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2020', '430502', '双清区', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2021', '430503', '大祥区', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2022', '430511', '北塔区', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2023', '430521', '邵东县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2024', '430522', '新邵县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2025', '430523', '邵阳县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2026', '430524', '隆回县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2027', '430525', '洞口县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2028', '430527', '绥宁县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2029', '430528', '新宁县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2030', '430529', '城步苗族自治县', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2031', '430581', '武冈市', 'A', '2018');
INSERT INTO `dic_areas` VALUES ('2032', '430600', '岳阳市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2033', '430601', '市辖区', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2034', '430602', '岳阳楼区', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2035', '430603', '云溪区', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2036', '430611', '君山区', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2037', '430621', '岳阳县', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2038', '430623', '华容县', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2039', '430624', '湘阴县', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2040', '430626', '平江县', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2041', '430681', '汨罗市', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2042', '430682', '临湘市', 'A', '2032');
INSERT INTO `dic_areas` VALUES ('2043', '430700', '常德市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2044', '430701', '市辖区', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2045', '430702', '武陵区', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2046', '430703', '鼎城区', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2047', '430721', '安乡县', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2048', '430722', '汉寿县', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2049', '430723', '澧县', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2050', '430724', '临澧县', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2051', '430725', '桃源县', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2052', '430726', '石门县', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2053', '430781', '津市市', 'A', '2043');
INSERT INTO `dic_areas` VALUES ('2054', '430800', '张家界市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2055', '430801', '市辖区', 'A', '2054');
INSERT INTO `dic_areas` VALUES ('2056', '430802', '永定区', 'A', '2054');
INSERT INTO `dic_areas` VALUES ('2057', '430811', '武陵源区', 'A', '2054');
INSERT INTO `dic_areas` VALUES ('2058', '430821', '慈利县', 'A', '2054');
INSERT INTO `dic_areas` VALUES ('2059', '430822', '桑植县', 'A', '2054');
INSERT INTO `dic_areas` VALUES ('2060', '430900', '益阳市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2061', '430901', '市辖区', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2062', '430902', '资阳区', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2063', '430903', '赫山区', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2064', '430921', '南县', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2065', '430922', '桃江县', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2066', '430923', '安化县', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2067', '430981', '沅江市', 'A', '2060');
INSERT INTO `dic_areas` VALUES ('2068', '431000', '郴州市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2069', '431001', '市辖区', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2070', '431002', '北湖区', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2071', '431003', '苏仙区', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2072', '431021', '桂阳县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2073', '431022', '宜章县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2074', '431023', '永兴县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2075', '431024', '嘉禾县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2076', '431025', '临武县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2077', '431026', '汝城县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2078', '431027', '桂东县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2079', '431028', '安仁县', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2080', '431081', '资兴市', 'A', '2068');
INSERT INTO `dic_areas` VALUES ('2081', '431100', '永州市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2082', '431101', '市辖区', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2083', '431102', '零陵区', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2084', '431103', '冷水滩区', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2085', '431121', '祁阳县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2086', '431122', '东安县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2087', '431123', '双牌县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2088', '431124', '道县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2089', '431125', '江永县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2090', '431126', '宁远县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2091', '431127', '蓝山县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2092', '431128', '新田县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2093', '431129', '江华瑶族自治县', 'A', '2081');
INSERT INTO `dic_areas` VALUES ('2094', '431200', '怀化市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2095', '431201', '市辖区', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2096', '431202', '鹤城区', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2097', '431221', '中方县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2098', '431222', '沅陵县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2099', '431223', '辰溪县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2100', '431224', '溆浦县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2101', '431225', '会同县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2102', '431226', '麻阳苗族自治县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2103', '431227', '新晃侗族自治县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2104', '431228', '芷江侗族自治县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2105', '431229', '靖州苗族侗族自治县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2106', '431230', '通道侗族自治县', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2107', '431281', '洪江市', 'A', '2094');
INSERT INTO `dic_areas` VALUES ('2108', '431300', '娄底市', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2109', '431301', '市辖区', 'A', '2108');
INSERT INTO `dic_areas` VALUES ('2110', '431302', '娄星区', 'A', '2108');
INSERT INTO `dic_areas` VALUES ('2111', '431321', '双峰县', 'A', '2108');
INSERT INTO `dic_areas` VALUES ('2112', '431322', '新化县', 'A', '2108');
INSERT INTO `dic_areas` VALUES ('2113', '431381', '冷水江市', 'A', '2108');
INSERT INTO `dic_areas` VALUES ('2114', '431382', '涟源市', 'A', '2108');
INSERT INTO `dic_areas` VALUES ('2115', '433100', '湘西土家族苗族自治州', 'C', '1974');
INSERT INTO `dic_areas` VALUES ('2116', '433101', '吉首市', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2117', '433122', '泸溪县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2118', '433123', '凤凰县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2119', '433124', '花垣县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2120', '433125', '保靖县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2121', '433126', '古丈县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2122', '433127', '永顺县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2123', '433130', '龙山县', 'A', '2115');
INSERT INTO `dic_areas` VALUES ('2124', '440000', '广东省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2125', '440100', '广州市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2126', '440101', '市辖区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2127', '440103', '荔湾区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2128', '440104', '越秀区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2129', '440105', '海珠区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2130', '440106', '天河区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2131', '440111', '白云区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2132', '440112', '黄埔区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2133', '440113', '番禺区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2134', '440114', '花都区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2135', '440115', '南沙区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2136', '440117', '从化区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2137', '440118', '增城区', 'A', '2125');
INSERT INTO `dic_areas` VALUES ('2138', '440200', '韶关市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2139', '440201', '市辖区', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2140', '440203', '武江区', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2141', '440204', '浈江区', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2142', '440205', '曲江区', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2143', '440222', '始兴县', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2144', '440224', '仁化县', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2145', '440229', '翁源县', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2146', '440232', '乳源瑶族自治县', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2147', '440233', '新丰县', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2148', '440281', '乐昌市', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2149', '440282', '南雄市', 'A', '2138');
INSERT INTO `dic_areas` VALUES ('2150', '440300', '深圳市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2151', '440301', '市辖区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2152', '440303', '罗湖区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2153', '440304', '福田区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2154', '440305', '南山区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2155', '440306', '宝安区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2156', '440307', '龙岗区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2157', '440308', '盐田区', 'A', '2150');
INSERT INTO `dic_areas` VALUES ('2158', '440400', '珠海市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2159', '440401', '市辖区', 'A', '2158');
INSERT INTO `dic_areas` VALUES ('2160', '440402', '香洲区', 'A', '2158');
INSERT INTO `dic_areas` VALUES ('2161', '440403', '斗门区', 'A', '2158');
INSERT INTO `dic_areas` VALUES ('2162', '440404', '金湾区', 'A', '2158');
INSERT INTO `dic_areas` VALUES ('2163', '440500', '汕头市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2164', '440501', '市辖区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2165', '440507', '龙湖区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2166', '440511', '金平区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2167', '440512', '濠江区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2168', '440513', '潮阳区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2169', '440514', '潮南区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2170', '440515', '澄海区', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2171', '440523', '南澳县', 'A', '2163');
INSERT INTO `dic_areas` VALUES ('2172', '440600', '佛山市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2173', '440601', '市辖区', 'A', '2172');
INSERT INTO `dic_areas` VALUES ('2174', '440604', '禅城区', 'A', '2172');
INSERT INTO `dic_areas` VALUES ('2175', '440605', '南海区', 'A', '2172');
INSERT INTO `dic_areas` VALUES ('2176', '440606', '顺德区', 'A', '2172');
INSERT INTO `dic_areas` VALUES ('2177', '440607', '三水区', 'A', '2172');
INSERT INTO `dic_areas` VALUES ('2178', '440608', '高明区', 'A', '2172');
INSERT INTO `dic_areas` VALUES ('2179', '440700', '江门市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2180', '440701', '市辖区', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2181', '440703', '蓬江区', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2182', '440704', '江海区', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2183', '440705', '新会区', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2184', '440781', '台山市', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2185', '440783', '开平市', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2186', '440784', '鹤山市', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2187', '440785', '恩平市', 'A', '2179');
INSERT INTO `dic_areas` VALUES ('2188', '440800', '湛江市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2189', '440801', '市辖区', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2190', '440802', '赤坎区', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2191', '440803', '霞山区', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2192', '440804', '坡头区', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2193', '440811', '麻章区', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2194', '440823', '遂溪县', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2195', '440825', '徐闻县', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2196', '440881', '廉江市', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2197', '440882', '雷州市', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2198', '440883', '吴川市', 'A', '2188');
INSERT INTO `dic_areas` VALUES ('2199', '440900', '茂名市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2200', '440901', '市辖区', 'A', '2199');
INSERT INTO `dic_areas` VALUES ('2201', '440902', '茂南区', 'A', '2199');
INSERT INTO `dic_areas` VALUES ('2202', '440904', '电白区', 'A', '2199');
INSERT INTO `dic_areas` VALUES ('2203', '440981', '高州市', 'A', '2199');
INSERT INTO `dic_areas` VALUES ('2204', '440982', '化州市', 'A', '2199');
INSERT INTO `dic_areas` VALUES ('2205', '440983', '信宜市', 'A', '2199');
INSERT INTO `dic_areas` VALUES ('2206', '441200', '肇庆市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2207', '441201', '市辖区', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2208', '441202', '端州区', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2209', '441203', '鼎湖区', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2210', '441204', '高要区', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2211', '441223', '广宁县', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2212', '441224', '怀集县', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2213', '441225', '封开县', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2214', '441226', '德庆县', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2215', '441284', '四会市', 'A', '2206');
INSERT INTO `dic_areas` VALUES ('2216', '441300', '惠州市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2217', '441301', '市辖区', 'A', '2216');
INSERT INTO `dic_areas` VALUES ('2218', '441302', '惠城区', 'A', '2216');
INSERT INTO `dic_areas` VALUES ('2219', '441303', '惠阳区', 'A', '2216');
INSERT INTO `dic_areas` VALUES ('2220', '441322', '博罗县', 'A', '2216');
INSERT INTO `dic_areas` VALUES ('2221', '441323', '惠东县', 'A', '2216');
INSERT INTO `dic_areas` VALUES ('2222', '441324', '龙门县', 'A', '2216');
INSERT INTO `dic_areas` VALUES ('2223', '441400', '梅州市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2224', '441401', '市辖区', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2225', '441402', '梅江区', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2226', '441403', '梅县区', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2227', '441422', '大埔县', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2228', '441423', '丰顺县', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2229', '441424', '五华县', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2230', '441426', '平远县', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2231', '441427', '蕉岭县', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2232', '441481', '兴宁市', 'A', '2223');
INSERT INTO `dic_areas` VALUES ('2233', '441500', '汕尾市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2234', '441501', '市辖区', 'A', '2233');
INSERT INTO `dic_areas` VALUES ('2235', '441502', '城区', 'A', '2233');
INSERT INTO `dic_areas` VALUES ('2236', '441521', '海丰县', 'A', '2233');
INSERT INTO `dic_areas` VALUES ('2237', '441523', '陆河县', 'A', '2233');
INSERT INTO `dic_areas` VALUES ('2238', '441581', '陆丰市', 'A', '2233');
INSERT INTO `dic_areas` VALUES ('2239', '441600', '河源市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2240', '441601', '市辖区', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2241', '441602', '源城区', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2242', '441621', '紫金县', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2243', '441622', '龙川县', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2244', '441623', '连平县', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2245', '441624', '和平县', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2246', '441625', '东源县', 'A', '2239');
INSERT INTO `dic_areas` VALUES ('2247', '441700', '阳江市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2248', '441701', '市辖区', 'A', '2247');
INSERT INTO `dic_areas` VALUES ('2249', '441702', '江城区', 'A', '2247');
INSERT INTO `dic_areas` VALUES ('2250', '441704', '阳东区', 'A', '2247');
INSERT INTO `dic_areas` VALUES ('2251', '441721', '阳西县', 'A', '2247');
INSERT INTO `dic_areas` VALUES ('2252', '441781', '阳春市', 'A', '2247');
INSERT INTO `dic_areas` VALUES ('2253', '441800', '清远市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2254', '441801', '市辖区', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2255', '441802', '清城区', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2256', '441803', '清新区', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2257', '441821', '佛冈县', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2258', '441823', '阳山县', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2259', '441825', '连山壮族瑶族自治县', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2260', '441826', '连南瑶族自治县', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2261', '441881', '英德市', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2262', '441882', '连州市', 'A', '2253');
INSERT INTO `dic_areas` VALUES ('2263', '441900', '东莞市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2264', '442000', '中山市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2265', '445100', '潮州市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2266', '445101', '市辖区', 'A', '2265');
INSERT INTO `dic_areas` VALUES ('2267', '445102', '湘桥区', 'A', '2265');
INSERT INTO `dic_areas` VALUES ('2268', '445103', '潮安区', 'A', '2265');
INSERT INTO `dic_areas` VALUES ('2269', '445122', '饶平县', 'A', '2265');
INSERT INTO `dic_areas` VALUES ('2270', '445200', '揭阳市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2271', '445201', '市辖区', 'A', '2270');
INSERT INTO `dic_areas` VALUES ('2272', '445202', '榕城区', 'A', '2270');
INSERT INTO `dic_areas` VALUES ('2273', '445203', '揭东区', 'A', '2270');
INSERT INTO `dic_areas` VALUES ('2274', '445222', '揭西县', 'A', '2270');
INSERT INTO `dic_areas` VALUES ('2275', '445224', '惠来县', 'A', '2270');
INSERT INTO `dic_areas` VALUES ('2276', '445281', '普宁市', 'A', '2270');
INSERT INTO `dic_areas` VALUES ('2277', '445300', '云浮市', 'C', '2124');
INSERT INTO `dic_areas` VALUES ('2278', '445301', '市辖区', 'A', '2277');
INSERT INTO `dic_areas` VALUES ('2279', '445302', '云城区', 'A', '2277');
INSERT INTO `dic_areas` VALUES ('2280', '445303', '云安区', 'A', '2277');
INSERT INTO `dic_areas` VALUES ('2281', '445321', '新兴县', 'A', '2277');
INSERT INTO `dic_areas` VALUES ('2282', '445322', '郁南县', 'A', '2277');
INSERT INTO `dic_areas` VALUES ('2283', '445381', '罗定市', 'A', '2277');
INSERT INTO `dic_areas` VALUES ('2284', '450000', '广西壮族自治区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2285', '450100', '南宁市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2286', '450101', '市辖区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2287', '450102', '兴宁区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2288', '450103', '青秀区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2289', '450105', '江南区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2290', '450107', '西乡塘区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2291', '450108', '良庆区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2292', '450109', '邕宁区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2293', '450110', '武鸣区', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2294', '450123', '隆安县', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2295', '450124', '马山县', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2296', '450125', '上林县', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2297', '450126', '宾阳县', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2298', '450127', '横县', 'A', '2285');
INSERT INTO `dic_areas` VALUES ('2299', '450200', '柳州市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2300', '450201', '市辖区', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2301', '450202', '城中区', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2302', '450203', '鱼峰区', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2303', '450204', '柳南区', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2304', '450205', '柳北区', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2305', '450206', '柳江区', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2306', '450222', '柳城县', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2307', '450223', '鹿寨县', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2308', '450224', '融安县', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2309', '450225', '融水苗族自治县', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2310', '450226', '三江侗族自治县', 'A', '2299');
INSERT INTO `dic_areas` VALUES ('2311', '450300', '桂林市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2312', '450301', '市辖区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2313', '450302', '秀峰区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2314', '450303', '叠彩区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2315', '450304', '象山区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2316', '450305', '七星区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2317', '450311', '雁山区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2318', '450312', '临桂区', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2319', '450321', '阳朔县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2320', '450323', '灵川县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2321', '450324', '全州县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2322', '450325', '兴安县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2323', '450326', '永福县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2324', '450327', '灌阳县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2325', '450328', '龙胜各族自治县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2326', '450329', '资源县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2327', '450330', '平乐县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2328', '450331', '荔浦县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2329', '450332', '恭城瑶族自治县', 'A', '2311');
INSERT INTO `dic_areas` VALUES ('2330', '450400', '梧州市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2331', '450401', '市辖区', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2332', '450403', '万秀区', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2333', '450405', '长洲区', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2334', '450406', '龙圩区', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2335', '450421', '苍梧县', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2336', '450422', '藤县', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2337', '450423', '蒙山县', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2338', '450481', '岑溪市', 'A', '2330');
INSERT INTO `dic_areas` VALUES ('2339', '450500', '北海市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2340', '450501', '市辖区', 'A', '2339');
INSERT INTO `dic_areas` VALUES ('2341', '450502', '海城区', 'A', '2339');
INSERT INTO `dic_areas` VALUES ('2342', '450503', '银海区', 'A', '2339');
INSERT INTO `dic_areas` VALUES ('2343', '450512', '铁山港区', 'A', '2339');
INSERT INTO `dic_areas` VALUES ('2344', '450521', '合浦县', 'A', '2339');
INSERT INTO `dic_areas` VALUES ('2345', '450600', '防城港市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2346', '450601', '市辖区', 'A', '2345');
INSERT INTO `dic_areas` VALUES ('2347', '450602', '港口区', 'A', '2345');
INSERT INTO `dic_areas` VALUES ('2348', '450603', '防城区', 'A', '2345');
INSERT INTO `dic_areas` VALUES ('2349', '450621', '上思县', 'A', '2345');
INSERT INTO `dic_areas` VALUES ('2350', '450681', '东兴市', 'A', '2345');
INSERT INTO `dic_areas` VALUES ('2351', '450700', '钦州市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2352', '450701', '市辖区', 'A', '2351');
INSERT INTO `dic_areas` VALUES ('2353', '450702', '钦南区', 'A', '2351');
INSERT INTO `dic_areas` VALUES ('2354', '450703', '钦北区', 'A', '2351');
INSERT INTO `dic_areas` VALUES ('2355', '450721', '灵山县', 'A', '2351');
INSERT INTO `dic_areas` VALUES ('2356', '450722', '浦北县', 'A', '2351');
INSERT INTO `dic_areas` VALUES ('2357', '450800', '贵港市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2358', '450801', '市辖区', 'A', '2357');
INSERT INTO `dic_areas` VALUES ('2359', '450802', '港北区', 'A', '2357');
INSERT INTO `dic_areas` VALUES ('2360', '450803', '港南区', 'A', '2357');
INSERT INTO `dic_areas` VALUES ('2361', '450804', '覃塘区', 'A', '2357');
INSERT INTO `dic_areas` VALUES ('2362', '450821', '平南县', 'A', '2357');
INSERT INTO `dic_areas` VALUES ('2363', '450881', '桂平市', 'A', '2357');
INSERT INTO `dic_areas` VALUES ('2364', '450900', '玉林市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2365', '450901', '市辖区', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2366', '450902', '玉州区', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2367', '450903', '福绵区', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2368', '450921', '容县', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2369', '450922', '陆川县', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2370', '450923', '博白县', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2371', '450924', '兴业县', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2372', '450981', '北流市', 'A', '2364');
INSERT INTO `dic_areas` VALUES ('2373', '451000', '百色市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2374', '451001', '市辖区', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2375', '451002', '右江区', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2376', '451021', '田阳县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2377', '451022', '田东县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2378', '451023', '平果县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2379', '451024', '德保县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2380', '451026', '那坡县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2381', '451027', '凌云县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2382', '451028', '乐业县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2383', '451029', '田林县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2384', '451030', '西林县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2385', '451031', '隆林各族自治县', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2386', '451081', '靖西市', 'A', '2373');
INSERT INTO `dic_areas` VALUES ('2387', '451100', '贺州市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2388', '451101', '市辖区', 'A', '2387');
INSERT INTO `dic_areas` VALUES ('2389', '451102', '八步区', 'A', '2387');
INSERT INTO `dic_areas` VALUES ('2390', '451103', '平桂区', 'A', '2387');
INSERT INTO `dic_areas` VALUES ('2391', '451121', '昭平县', 'A', '2387');
INSERT INTO `dic_areas` VALUES ('2392', '451122', '钟山县', 'A', '2387');
INSERT INTO `dic_areas` VALUES ('2393', '451123', '富川瑶族自治县', 'A', '2387');
INSERT INTO `dic_areas` VALUES ('2394', '451200', '河池市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2395', '451201', '市辖区', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2396', '451202', '金城江区', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2397', '451221', '南丹县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2398', '451222', '天峨县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2399', '451223', '凤山县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2400', '451224', '东兰县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2401', '451225', '罗城仫佬族自治县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2402', '451226', '环江毛南族自治县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2403', '451227', '巴马瑶族自治县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2404', '451228', '都安瑶族自治县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2405', '451229', '大化瑶族自治县', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2406', '451281', '宜州市', 'A', '2394');
INSERT INTO `dic_areas` VALUES ('2407', '451300', '来宾市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2408', '451301', '市辖区', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2409', '451302', '兴宾区', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2410', '451321', '忻城县', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2411', '451322', '象州县', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2412', '451323', '武宣县', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2413', '451324', '金秀瑶族自治县', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2414', '451381', '合山市', 'A', '2407');
INSERT INTO `dic_areas` VALUES ('2415', '451400', '崇左市', 'C', '2284');
INSERT INTO `dic_areas` VALUES ('2416', '451401', '市辖区', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2417', '451402', '江州区', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2418', '451421', '扶绥县', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2419', '451422', '宁明县', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2420', '451423', '龙州县', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2421', '451424', '大新县', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2422', '451425', '天等县', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2423', '451481', '凭祥市', 'A', '2415');
INSERT INTO `dic_areas` VALUES ('2424', '460000', '海南省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2425', '460100', '海口市', 'C', '2424');
INSERT INTO `dic_areas` VALUES ('2426', '460101', '市辖区', 'A', '2425');
INSERT INTO `dic_areas` VALUES ('2427', '460105', '秀英区', 'A', '2425');
INSERT INTO `dic_areas` VALUES ('2428', '460106', '龙华区', 'A', '2425');
INSERT INTO `dic_areas` VALUES ('2429', '460107', '琼山区', 'A', '2425');
INSERT INTO `dic_areas` VALUES ('2430', '460108', '美兰区', 'A', '2425');
INSERT INTO `dic_areas` VALUES ('2431', '460200', '三亚市', 'C', '2424');
INSERT INTO `dic_areas` VALUES ('2432', '460201', '市辖区', 'A', '2431');
INSERT INTO `dic_areas` VALUES ('2433', '460202', '海棠区', 'A', '2431');
INSERT INTO `dic_areas` VALUES ('2434', '460203', '吉阳区', 'A', '2431');
INSERT INTO `dic_areas` VALUES ('2435', '460204', '天涯区', 'A', '2431');
INSERT INTO `dic_areas` VALUES ('2436', '460205', '崖州区', 'A', '2431');
INSERT INTO `dic_areas` VALUES ('2437', '460300', '三沙市', 'C', '2424');
INSERT INTO `dic_areas` VALUES ('2438', '460400', '儋州市', 'C', '2424');
INSERT INTO `dic_areas` VALUES ('2439', '469000', '省直辖县级行政区划', 'C', '2424');
INSERT INTO `dic_areas` VALUES ('2440', '469001', '五指山市', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2441', '469002', '琼海市', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2442', '469005', '文昌市', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2443', '469006', '万宁市', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2444', '469007', '东方市', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2445', '469021', '定安县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2446', '469022', '屯昌县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2447', '469023', '澄迈县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2448', '469024', '临高县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2449', '469025', '白沙黎族自治县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2450', '469026', '昌江黎族自治县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2451', '469027', '乐东黎族自治县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2452', '469028', '陵水黎族自治县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2453', '469029', '保亭黎族苗族自治县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2454', '469030', '琼中黎族苗族自治县', 'A', '2439');
INSERT INTO `dic_areas` VALUES ('2455', '500000', '重庆市', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2456', '500100', '市辖区', 'C', '2455');
INSERT INTO `dic_areas` VALUES ('2457', '500101', '万州区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2458', '500102', '涪陵区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2459', '500103', '渝中区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2460', '500104', '大渡口区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2461', '500105', '江北区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2462', '500106', '沙坪坝区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2463', '500107', '九龙坡区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2464', '500108', '南岸区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2465', '500109', '北碚区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2466', '500110', '綦江区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2467', '500111', '大足区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2468', '500112', '渝北区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2469', '500113', '巴南区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2470', '500114', '黔江区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2471', '500115', '长寿区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2472', '500116', '江津区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2473', '500117', '合川区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2474', '500118', '永川区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2475', '500119', '南川区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2476', '500120', '璧山区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2477', '500151', '铜梁区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2478', '500152', '潼南区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2479', '500153', '荣昌区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2480', '500154', '开州区', 'A', '2456');
INSERT INTO `dic_areas` VALUES ('2481', '500200', '县', 'C', '2455');
INSERT INTO `dic_areas` VALUES ('2482', '500228', '梁平县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2483', '500229', '城口县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2484', '500230', '丰都县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2485', '500231', '垫江县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2486', '500232', '武隆县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2487', '500233', '忠县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2488', '500235', '云阳县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2489', '500236', '奉节县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2490', '500237', '巫山县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2491', '500238', '巫溪县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2492', '500240', '石柱土家族自治县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2493', '500241', '秀山土家族苗族自治县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2494', '500242', '酉阳土家族苗族自治县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2495', '500243', '彭水苗族土家族自治县', 'A', '2481');
INSERT INTO `dic_areas` VALUES ('2496', '510000', '四川省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2497', '510100', '成都市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2498', '510101', '市辖区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2499', '510104', '锦江区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2500', '510105', '青羊区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2501', '510106', '金牛区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2502', '510107', '武侯区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2503', '510108', '成华区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2504', '510112', '龙泉驿区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2505', '510113', '青白江区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2506', '510114', '新都区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2507', '510115', '温江区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2508', '510116', '双流区', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2509', '510121', '金堂县', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2510', '510124', '郫县', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2511', '510129', '大邑县', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2512', '510131', '蒲江县', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2513', '510132', '新津县', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2514', '510181', '都江堰市', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2515', '510182', '彭州市', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2516', '510183', '邛崃市', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2517', '510184', '崇州市', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2518', '510185', '简阳市', 'A', '2497');
INSERT INTO `dic_areas` VALUES ('2519', '510300', '自贡市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2520', '510301', '市辖区', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2521', '510302', '自流井区', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2522', '510303', '贡井区', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2523', '510304', '大安区', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2524', '510311', '沿滩区', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2525', '510321', '荣县', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2526', '510322', '富顺县', 'A', '2519');
INSERT INTO `dic_areas` VALUES ('2527', '510400', '攀枝花市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2528', '510401', '市辖区', 'A', '2527');
INSERT INTO `dic_areas` VALUES ('2529', '510402', '东区', 'A', '2527');
INSERT INTO `dic_areas` VALUES ('2530', '510403', '西区', 'A', '2527');
INSERT INTO `dic_areas` VALUES ('2531', '510411', '仁和区', 'A', '2527');
INSERT INTO `dic_areas` VALUES ('2532', '510421', '米易县', 'A', '2527');
INSERT INTO `dic_areas` VALUES ('2533', '510422', '盐边县', 'A', '2527');
INSERT INTO `dic_areas` VALUES ('2534', '510500', '泸州市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2535', '510501', '市辖区', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2536', '510502', '江阳区', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2537', '510503', '纳溪区', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2538', '510504', '龙马潭区', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2539', '510521', '泸县', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2540', '510522', '合江县', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2541', '510524', '叙永县', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2542', '510525', '古蔺县', 'A', '2534');
INSERT INTO `dic_areas` VALUES ('2543', '510600', '德阳市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2544', '510601', '市辖区', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2545', '510603', '旌阳区', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2546', '510623', '中江县', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2547', '510626', '罗江县', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2548', '510681', '广汉市', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2549', '510682', '什邡市', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2550', '510683', '绵竹市', 'A', '2543');
INSERT INTO `dic_areas` VALUES ('2551', '510700', '绵阳市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2552', '510701', '市辖区', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2553', '510703', '涪城区', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2554', '510704', '游仙区', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2555', '510705', '安州区', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2556', '510722', '三台县', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2557', '510723', '盐亭县', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2558', '510725', '梓潼县', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2559', '510726', '北川羌族自治县', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2560', '510727', '平武县', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2561', '510781', '江油市', 'A', '2551');
INSERT INTO `dic_areas` VALUES ('2562', '510800', '广元市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2563', '510801', '市辖区', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2564', '510802', '利州区', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2565', '510811', '昭化区', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2566', '510812', '朝天区', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2567', '510821', '旺苍县', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2568', '510822', '青川县', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2569', '510823', '剑阁县', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2570', '510824', '苍溪县', 'A', '2562');
INSERT INTO `dic_areas` VALUES ('2571', '510900', '遂宁市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2572', '510901', '市辖区', 'A', '2571');
INSERT INTO `dic_areas` VALUES ('2573', '510903', '船山区', 'A', '2571');
INSERT INTO `dic_areas` VALUES ('2574', '510904', '安居区', 'A', '2571');
INSERT INTO `dic_areas` VALUES ('2575', '510921', '蓬溪县', 'A', '2571');
INSERT INTO `dic_areas` VALUES ('2576', '510922', '射洪县', 'A', '2571');
INSERT INTO `dic_areas` VALUES ('2577', '510923', '大英县', 'A', '2571');
INSERT INTO `dic_areas` VALUES ('2578', '511000', '内江市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2579', '511001', '市辖区', 'A', '2578');
INSERT INTO `dic_areas` VALUES ('2580', '511002', '市中区', 'A', '2578');
INSERT INTO `dic_areas` VALUES ('2581', '511011', '东兴区', 'A', '2578');
INSERT INTO `dic_areas` VALUES ('2582', '511024', '威远县', 'A', '2578');
INSERT INTO `dic_areas` VALUES ('2583', '511025', '资中县', 'A', '2578');
INSERT INTO `dic_areas` VALUES ('2584', '511028', '隆昌县', 'A', '2578');
INSERT INTO `dic_areas` VALUES ('2585', '511100', '乐山市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2586', '511101', '市辖区', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2587', '511102', '市中区', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2588', '511111', '沙湾区', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2589', '511112', '五通桥区', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2590', '511113', '金口河区', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2591', '511123', '犍为县', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2592', '511124', '井研县', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2593', '511126', '夹江县', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2594', '511129', '沐川县', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2595', '511132', '峨边彝族自治县', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2596', '511133', '马边彝族自治县', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2597', '511181', '峨眉山市', 'A', '2585');
INSERT INTO `dic_areas` VALUES ('2598', '511300', '南充市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2599', '511301', '市辖区', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2600', '511302', '顺庆区', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2601', '511303', '高坪区', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2602', '511304', '嘉陵区', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2603', '511321', '南部县', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2604', '511322', '营山县', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2605', '511323', '蓬安县', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2606', '511324', '仪陇县', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2607', '511325', '西充县', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2608', '511381', '阆中市', 'A', '2598');
INSERT INTO `dic_areas` VALUES ('2609', '511400', '眉山市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2610', '511401', '市辖区', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2611', '511402', '东坡区', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2612', '511403', '彭山区', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2613', '511421', '仁寿县', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2614', '511423', '洪雅县', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2615', '511424', '丹棱县', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2616', '511425', '青神县', 'A', '2609');
INSERT INTO `dic_areas` VALUES ('2617', '511500', '宜宾市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2618', '511501', '市辖区', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2619', '511502', '翠屏区', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2620', '511503', '南溪区', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2621', '511521', '宜宾县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2622', '511523', '江安县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2623', '511524', '长宁县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2624', '511525', '高县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2625', '511526', '珙县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2626', '511527', '筠连县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2627', '511528', '兴文县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2628', '511529', '屏山县', 'A', '2617');
INSERT INTO `dic_areas` VALUES ('2629', '511600', '广安市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2630', '511601', '市辖区', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2631', '511602', '广安区', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2632', '511603', '前锋区', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2633', '511621', '岳池县', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2634', '511622', '武胜县', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2635', '511623', '邻水县', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2636', '511681', '华蓥市', 'A', '2629');
INSERT INTO `dic_areas` VALUES ('2637', '511700', '达州市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2638', '511701', '市辖区', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2639', '511702', '通川区', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2640', '511703', '达川区', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2641', '511722', '宣汉县', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2642', '511723', '开江县', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2643', '511724', '大竹县', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2644', '511725', '渠县', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2645', '511781', '万源市', 'A', '2637');
INSERT INTO `dic_areas` VALUES ('2646', '511800', '雅安市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2647', '511801', '市辖区', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2648', '511802', '雨城区', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2649', '511803', '名山区', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2650', '511822', '荥经县', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2651', '511823', '汉源县', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2652', '511824', '石棉县', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2653', '511825', '天全县', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2654', '511826', '芦山县', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2655', '511827', '宝兴县', 'A', '2646');
INSERT INTO `dic_areas` VALUES ('2656', '511900', '巴中市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2657', '511901', '市辖区', 'A', '2656');
INSERT INTO `dic_areas` VALUES ('2658', '511902', '巴州区', 'A', '2656');
INSERT INTO `dic_areas` VALUES ('2659', '511903', '恩阳区', 'A', '2656');
INSERT INTO `dic_areas` VALUES ('2660', '511921', '通江县', 'A', '2656');
INSERT INTO `dic_areas` VALUES ('2661', '511922', '南江县', 'A', '2656');
INSERT INTO `dic_areas` VALUES ('2662', '511923', '平昌县', 'A', '2656');
INSERT INTO `dic_areas` VALUES ('2663', '512000', '资阳市', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2664', '512001', '市辖区', 'A', '2663');
INSERT INTO `dic_areas` VALUES ('2665', '512002', '雁江区', 'A', '2663');
INSERT INTO `dic_areas` VALUES ('2666', '512021', '安岳县', 'A', '2663');
INSERT INTO `dic_areas` VALUES ('2667', '512022', '乐至县', 'A', '2663');
INSERT INTO `dic_areas` VALUES ('2668', '513200', '阿坝藏族羌族自治州', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2669', '513201', '马尔康市', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2670', '513221', '汶川县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2671', '513222', '理县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2672', '513223', '茂县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2673', '513224', '松潘县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2674', '513225', '九寨沟县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2675', '513226', '金川县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2676', '513227', '小金县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2677', '513228', '黑水县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2678', '513230', '壤塘县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2679', '513231', '阿坝县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2680', '513232', '若尔盖县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2681', '513233', '红原县', 'A', '2668');
INSERT INTO `dic_areas` VALUES ('2682', '513300', '甘孜藏族自治州', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2683', '513301', '康定市', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2684', '513322', '泸定县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2685', '513323', '丹巴县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2686', '513324', '九龙县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2687', '513325', '雅江县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2688', '513326', '道孚县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2689', '513327', '炉霍县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2690', '513328', '甘孜县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2691', '513329', '新龙县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2692', '513330', '德格县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2693', '513331', '白玉县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2694', '513332', '石渠县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2695', '513333', '色达县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2696', '513334', '理塘县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2697', '513335', '巴塘县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2698', '513336', '乡城县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2699', '513337', '稻城县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2700', '513338', '得荣县', 'A', '2682');
INSERT INTO `dic_areas` VALUES ('2701', '513400', '凉山彝族自治州', 'C', '2496');
INSERT INTO `dic_areas` VALUES ('2702', '513401', '西昌市', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2703', '513422', '木里藏族自治县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2704', '513423', '盐源县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2705', '513424', '德昌县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2706', '513425', '会理县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2707', '513426', '会东县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2708', '513427', '宁南县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2709', '513428', '普格县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2710', '513429', '布拖县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2711', '513430', '金阳县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2712', '513431', '昭觉县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2713', '513432', '喜德县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2714', '513433', '冕宁县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2715', '513434', '越西县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2716', '513435', '甘洛县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2717', '513436', '美姑县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2718', '513437', '雷波县', 'A', '2701');
INSERT INTO `dic_areas` VALUES ('2719', '520000', '贵州省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2720', '520100', '贵阳市', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2721', '520101', '市辖区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2722', '520102', '南明区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2723', '520103', '云岩区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2724', '520111', '花溪区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2725', '520112', '乌当区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2726', '520113', '白云区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2727', '520115', '观山湖区', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2728', '520121', '开阳县', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2729', '520122', '息烽县', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2730', '520123', '修文县', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2731', '520181', '清镇市', 'A', '2720');
INSERT INTO `dic_areas` VALUES ('2732', '520200', '六盘水市', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2733', '520201', '钟山区', 'A', '2732');
INSERT INTO `dic_areas` VALUES ('2734', '520203', '六枝特区', 'A', '2732');
INSERT INTO `dic_areas` VALUES ('2735', '520221', '水城县', 'A', '2732');
INSERT INTO `dic_areas` VALUES ('2736', '520222', '盘县', 'A', '2732');
INSERT INTO `dic_areas` VALUES ('2737', '520300', '遵义市', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2738', '520301', '市辖区', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2739', '520302', '红花岗区', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2740', '520303', '汇川区', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2741', '520304', '播州区', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2742', '520322', '桐梓县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2743', '520323', '绥阳县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2744', '520324', '正安县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2745', '520325', '道真仡佬族苗族自治县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2746', '520326', '务川仡佬族苗族自治县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2747', '520327', '凤冈县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2748', '520328', '湄潭县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2749', '520329', '余庆县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2750', '520330', '习水县', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2751', '520381', '赤水市', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2752', '520382', '仁怀市', 'A', '2737');
INSERT INTO `dic_areas` VALUES ('2753', '520400', '安顺市', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2754', '520401', '市辖区', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2755', '520402', '西秀区', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2756', '520403', '平坝区', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2757', '520422', '普定县', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2758', '520423', '镇宁布依族苗族自治县', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2759', '520424', '关岭布依族苗族自治县', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2760', '520425', '紫云苗族布依族自治县', 'A', '2753');
INSERT INTO `dic_areas` VALUES ('2761', '520500', '毕节市', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2762', '520501', '市辖区', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2763', '520502', '七星关区', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2764', '520521', '大方县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2765', '520522', '黔西县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2766', '520523', '金沙县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2767', '520524', '织金县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2768', '520525', '纳雍县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2769', '520526', '威宁彝族回族苗族自治县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2770', '520527', '赫章县', 'A', '2761');
INSERT INTO `dic_areas` VALUES ('2771', '520600', '铜仁市', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2772', '520601', '市辖区', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2773', '520602', '碧江区', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2774', '520603', '万山区', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2775', '520621', '江口县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2776', '520622', '玉屏侗族自治县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2777', '520623', '石阡县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2778', '520624', '思南县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2779', '520625', '印江土家族苗族自治县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2780', '520626', '德江县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2781', '520627', '沿河土家族自治县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2782', '520628', '松桃苗族自治县', 'A', '2771');
INSERT INTO `dic_areas` VALUES ('2783', '522300', '黔西南布依族苗族自治州', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2784', '522301', '兴义市', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2785', '522322', '兴仁县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2786', '522323', '普安县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2787', '522324', '晴隆县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2788', '522325', '贞丰县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2789', '522326', '望谟县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2790', '522327', '册亨县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2791', '522328', '安龙县', 'A', '2783');
INSERT INTO `dic_areas` VALUES ('2792', '522600', '黔东南苗族侗族自治州', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2793', '522601', '凯里市', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2794', '522622', '黄平县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2795', '522623', '施秉县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2796', '522624', '三穗县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2797', '522625', '镇远县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2798', '522626', '岑巩县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2799', '522627', '天柱县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2800', '522628', '锦屏县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2801', '522629', '剑河县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2802', '522630', '台江县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2803', '522631', '黎平县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2804', '522632', '榕江县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2805', '522633', '从江县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2806', '522634', '雷山县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2807', '522635', '麻江县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2808', '522636', '丹寨县', 'A', '2792');
INSERT INTO `dic_areas` VALUES ('2809', '522700', '黔南布依族苗族自治州', 'C', '2719');
INSERT INTO `dic_areas` VALUES ('2810', '522701', '都匀市', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2811', '522702', '福泉市', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2812', '522722', '荔波县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2813', '522723', '贵定县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2814', '522725', '瓮安县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2815', '522726', '独山县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2816', '522727', '平塘县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2817', '522728', '罗甸县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2818', '522729', '长顺县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2819', '522730', '龙里县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2820', '522731', '惠水县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2821', '522732', '三都水族自治县', 'A', '2809');
INSERT INTO `dic_areas` VALUES ('2822', '530000', '云南省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2823', '530100', '昆明市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2824', '530101', '市辖区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2825', '530102', '五华区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2826', '530103', '盘龙区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2827', '530111', '官渡区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2828', '530112', '西山区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2829', '530113', '东川区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2830', '530114', '呈贡区', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2831', '530122', '晋宁县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2832', '530124', '富民县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2833', '530125', '宜良县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2834', '530126', '石林彝族自治县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2835', '530127', '嵩明县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2836', '530128', '禄劝彝族苗族自治县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2837', '530129', '寻甸回族彝族自治县', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2838', '530181', '安宁市', 'A', '2823');
INSERT INTO `dic_areas` VALUES ('2839', '530300', '曲靖市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2840', '530301', '市辖区', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2841', '530302', '麒麟区', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2842', '530303', '沾益区', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2843', '530321', '马龙县', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2844', '530322', '陆良县', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2845', '530323', '师宗县', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2846', '530324', '罗平县', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2847', '530325', '富源县', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2848', '530326', '会泽县', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2849', '530381', '宣威市', 'A', '2839');
INSERT INTO `dic_areas` VALUES ('2850', '530400', '玉溪市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2851', '530401', '市辖区', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2852', '530402', '红塔区', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2853', '530403', '江川区', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2854', '530422', '澄江县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2855', '530423', '通海县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2856', '530424', '华宁县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2857', '530425', '易门县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2858', '530426', '峨山彝族自治县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2859', '530427', '新平彝族傣族自治县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2860', '530428', '元江哈尼族彝族傣族自治县', 'A', '2850');
INSERT INTO `dic_areas` VALUES ('2861', '530500', '保山市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2862', '530501', '市辖区', 'A', '2861');
INSERT INTO `dic_areas` VALUES ('2863', '530502', '隆阳区', 'A', '2861');
INSERT INTO `dic_areas` VALUES ('2864', '530521', '施甸县', 'A', '2861');
INSERT INTO `dic_areas` VALUES ('2865', '530523', '龙陵县', 'A', '2861');
INSERT INTO `dic_areas` VALUES ('2866', '530524', '昌宁县', 'A', '2861');
INSERT INTO `dic_areas` VALUES ('2867', '530581', '腾冲市', 'A', '2861');
INSERT INTO `dic_areas` VALUES ('2868', '530600', '昭通市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2869', '530601', '市辖区', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2870', '530602', '昭阳区', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2871', '530621', '鲁甸县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2872', '530622', '巧家县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2873', '530623', '盐津县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2874', '530624', '大关县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2875', '530625', '永善县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2876', '530626', '绥江县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2877', '530627', '镇雄县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2878', '530628', '彝良县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2879', '530629', '威信县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2880', '530630', '水富县', 'A', '2868');
INSERT INTO `dic_areas` VALUES ('2881', '530700', '丽江市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2882', '530701', '市辖区', 'A', '2881');
INSERT INTO `dic_areas` VALUES ('2883', '530702', '古城区', 'A', '2881');
INSERT INTO `dic_areas` VALUES ('2884', '530721', '玉龙纳西族自治县', 'A', '2881');
INSERT INTO `dic_areas` VALUES ('2885', '530722', '永胜县', 'A', '2881');
INSERT INTO `dic_areas` VALUES ('2886', '530723', '华坪县', 'A', '2881');
INSERT INTO `dic_areas` VALUES ('2887', '530724', '宁蒗彝族自治县', 'A', '2881');
INSERT INTO `dic_areas` VALUES ('2888', '530800', '普洱市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2889', '530801', '市辖区', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2890', '530802', '思茅区', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2891', '530821', '宁洱哈尼族彝族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2892', '530822', '墨江哈尼族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2893', '530823', '景东彝族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2894', '530824', '景谷傣族彝族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2895', '530825', '镇沅彝族哈尼族拉祜族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2896', '530826', '江城哈尼族彝族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2897', '530827', '孟连傣族拉祜族佤族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2898', '530828', '澜沧拉祜族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2899', '530829', '西盟佤族自治县', 'A', '2888');
INSERT INTO `dic_areas` VALUES ('2900', '530900', '临沧市', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2901', '530901', '市辖区', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2902', '530902', '临翔区', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2903', '530921', '凤庆县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2904', '530922', '云县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2905', '530923', '永德县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2906', '530924', '镇康县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2907', '530925', '双江拉祜族佤族布朗族傣族自治县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2908', '530926', '耿马傣族佤族自治县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2909', '530927', '沧源佤族自治县', 'A', '2900');
INSERT INTO `dic_areas` VALUES ('2910', '532300', '楚雄彝族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2911', '532301', '楚雄市', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2912', '532322', '双柏县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2913', '532323', '牟定县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2914', '532324', '南华县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2915', '532325', '姚安县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2916', '532326', '大姚县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2917', '532327', '永仁县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2918', '532328', '元谋县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2919', '532329', '武定县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2920', '532331', '禄丰县', 'A', '2910');
INSERT INTO `dic_areas` VALUES ('2921', '532500', '红河哈尼族彝族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2922', '532501', '个旧市', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2923', '532502', '开远市', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2924', '532503', '蒙自市', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2925', '532504', '弥勒市', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2926', '532523', '屏边苗族自治县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2927', '532524', '建水县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2928', '532525', '石屏县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2929', '532527', '泸西县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2930', '532528', '元阳县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2931', '532529', '红河县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2932', '532530', '金平苗族瑶族傣族自治县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2933', '532531', '绿春县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2934', '532532', '河口瑶族自治县', 'A', '2921');
INSERT INTO `dic_areas` VALUES ('2935', '532600', '文山壮族苗族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2936', '532601', '文山市', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2937', '532622', '砚山县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2938', '532623', '西畴县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2939', '532624', '麻栗坡县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2940', '532625', '马关县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2941', '532626', '丘北县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2942', '532627', '广南县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2943', '532628', '富宁县', 'A', '2935');
INSERT INTO `dic_areas` VALUES ('2944', '532800', '西双版纳傣族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2945', '532801', '景洪市', 'A', '2944');
INSERT INTO `dic_areas` VALUES ('2946', '532822', '勐海县', 'A', '2944');
INSERT INTO `dic_areas` VALUES ('2947', '532823', '勐腊县', 'A', '2944');
INSERT INTO `dic_areas` VALUES ('2948', '532900', '大理白族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2949', '532901', '大理市', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2950', '532922', '漾濞彝族自治县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2951', '532923', '祥云县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2952', '532924', '宾川县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2953', '532925', '弥渡县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2954', '532926', '南涧彝族自治县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2955', '532927', '巍山彝族回族自治县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2956', '532928', '永平县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2957', '532929', '云龙县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2958', '532930', '洱源县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2959', '532931', '剑川县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2960', '532932', '鹤庆县', 'A', '2948');
INSERT INTO `dic_areas` VALUES ('2961', '533100', '德宏傣族景颇族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2962', '533102', '瑞丽市', 'A', '2961');
INSERT INTO `dic_areas` VALUES ('2963', '533103', '芒市', 'A', '2961');
INSERT INTO `dic_areas` VALUES ('2964', '533122', '梁河县', 'A', '2961');
INSERT INTO `dic_areas` VALUES ('2965', '533123', '盈江县', 'A', '2961');
INSERT INTO `dic_areas` VALUES ('2966', '533124', '陇川县', 'A', '2961');
INSERT INTO `dic_areas` VALUES ('2967', '533300', '怒江傈僳族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2968', '533301', '泸水市', 'A', '2967');
INSERT INTO `dic_areas` VALUES ('2969', '533323', '福贡县', 'A', '2967');
INSERT INTO `dic_areas` VALUES ('2970', '533324', '贡山独龙族怒族自治县', 'A', '2967');
INSERT INTO `dic_areas` VALUES ('2971', '533325', '兰坪白族普米族自治县', 'A', '2967');
INSERT INTO `dic_areas` VALUES ('2972', '533400', '迪庆藏族自治州', 'C', '2822');
INSERT INTO `dic_areas` VALUES ('2973', '533401', '香格里拉市', 'A', '2972');
INSERT INTO `dic_areas` VALUES ('2974', '533422', '德钦县', 'A', '2972');
INSERT INTO `dic_areas` VALUES ('2975', '533423', '维西傈僳族自治县', 'A', '2972');
INSERT INTO `dic_areas` VALUES ('2976', '540000', '西藏自治区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('2977', '540100', '拉萨市', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('2978', '540101', '市辖区', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2979', '540102', '城关区', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2980', '540103', '堆龙德庆区', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2981', '540121', '林周县', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2982', '540122', '当雄县', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2983', '540123', '尼木县', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2984', '540124', '曲水县', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2985', '540126', '达孜县', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2986', '540127', '墨竹工卡县', 'A', '2977');
INSERT INTO `dic_areas` VALUES ('2987', '540200', '日喀则市', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('2988', '540202', '桑珠孜区', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2989', '540221', '南木林县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2990', '540222', '江孜县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2991', '540223', '定日县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2992', '540224', '萨迦县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2993', '540225', '拉孜县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2994', '540226', '昂仁县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2995', '540227', '谢通门县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2996', '540228', '白朗县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2997', '540229', '仁布县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2998', '540230', '康马县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('2999', '540231', '定结县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3000', '540232', '仲巴县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3001', '540233', '亚东县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3002', '540234', '吉隆县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3003', '540235', '聂拉木县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3004', '540236', '萨嘎县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3005', '540237', '岗巴县', 'A', '2987');
INSERT INTO `dic_areas` VALUES ('3006', '540300', '昌都市', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('3007', '540302', '卡若区', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3008', '540321', '江达县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3009', '540322', '贡觉县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3010', '540323', '类乌齐县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3011', '540324', '丁青县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3012', '540325', '察雅县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3013', '540326', '八宿县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3014', '540327', '左贡县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3015', '540328', '芒康县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3016', '540329', '洛隆县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3017', '540330', '边坝县', 'A', '3006');
INSERT INTO `dic_areas` VALUES ('3018', '540400', '林芝市', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('3019', '540402', '巴宜区', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3020', '540421', '工布江达县', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3021', '540422', '米林县', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3022', '540423', '墨脱县', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3023', '540424', '波密县', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3024', '540425', '察隅县', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3025', '540426', '朗县', 'A', '3018');
INSERT INTO `dic_areas` VALUES ('3026', '540500', '山南市', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('3027', '540501', '市辖区', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3028', '540502', '乃东区', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3029', '540521', '扎囊县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3030', '540522', '贡嘎县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3031', '540523', '桑日县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3032', '540524', '琼结县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3033', '540525', '曲松县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3034', '540526', '措美县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3035', '540527', '洛扎县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3036', '540528', '加查县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3037', '540529', '隆子县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3038', '540530', '错那县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3039', '540531', '浪卡子县', 'A', '3026');
INSERT INTO `dic_areas` VALUES ('3040', '542400', '那曲地区', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('3041', '542421', '那曲县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3042', '542422', '嘉黎县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3043', '542423', '比如县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3044', '542424', '聂荣县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3045', '542425', '安多县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3046', '542426', '申扎县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3047', '542427', '索县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3048', '542428', '班戈县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3049', '542429', '巴青县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3050', '542430', '尼玛县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3051', '542431', '双湖县', 'A', '3040');
INSERT INTO `dic_areas` VALUES ('3052', '542500', '阿里地区', 'C', '2976');
INSERT INTO `dic_areas` VALUES ('3053', '542521', '普兰县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3054', '542522', '札达县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3055', '542523', '噶尔县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3056', '542524', '日土县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3057', '542525', '革吉县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3058', '542526', '改则县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3059', '542527', '措勤县', 'A', '3052');
INSERT INTO `dic_areas` VALUES ('3060', '610000', '陕西省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3061', '610100', '西安市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3062', '610101', '市辖区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3063', '610102', '新城区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3064', '610103', '碑林区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3065', '610104', '莲湖区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3066', '610111', '灞桥区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3067', '610112', '未央区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3068', '610113', '雁塔区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3069', '610114', '阎良区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3070', '610115', '临潼区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3071', '610116', '长安区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3072', '610117', '高陵区', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3073', '610122', '蓝田县', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3074', '610124', '周至县', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3075', '610125', '户县', 'A', '3061');
INSERT INTO `dic_areas` VALUES ('3076', '610200', '铜川市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3077', '610201', '市辖区', 'A', '3076');
INSERT INTO `dic_areas` VALUES ('3078', '610202', '王益区', 'A', '3076');
INSERT INTO `dic_areas` VALUES ('3079', '610203', '印台区', 'A', '3076');
INSERT INTO `dic_areas` VALUES ('3080', '610204', '耀州区', 'A', '3076');
INSERT INTO `dic_areas` VALUES ('3081', '610222', '宜君县', 'A', '3076');
INSERT INTO `dic_areas` VALUES ('3082', '610300', '宝鸡市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3083', '610301', '市辖区', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3084', '610302', '渭滨区', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3085', '610303', '金台区', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3086', '610304', '陈仓区', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3087', '610322', '凤翔县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3088', '610323', '岐山县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3089', '610324', '扶风县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3090', '610326', '眉县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3091', '610327', '陇县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3092', '610328', '千阳县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3093', '610329', '麟游县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3094', '610330', '凤县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3095', '610331', '太白县', 'A', '3082');
INSERT INTO `dic_areas` VALUES ('3096', '610400', '咸阳市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3097', '610401', '市辖区', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3098', '610402', '秦都区', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3099', '610403', '杨陵区', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3100', '610404', '渭城区', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3101', '610422', '三原县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3102', '610423', '泾阳县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3103', '610424', '乾县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3104', '610425', '礼泉县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3105', '610426', '永寿县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3106', '610427', '彬县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3107', '610428', '长武县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3108', '610429', '旬邑县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3109', '610430', '淳化县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3110', '610431', '武功县', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3111', '610481', '兴平市', 'A', '3096');
INSERT INTO `dic_areas` VALUES ('3112', '610500', '渭南市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3113', '610501', '市辖区', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3114', '610502', '临渭区', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3115', '610503', '华州区', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3116', '610522', '潼关县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3117', '610523', '大荔县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3118', '610524', '合阳县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3119', '610525', '澄城县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3120', '610526', '蒲城县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3121', '610527', '白水县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3122', '610528', '富平县', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3123', '610581', '韩城市', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3124', '610582', '华阴市', 'A', '3112');
INSERT INTO `dic_areas` VALUES ('3125', '610600', '延安市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3126', '610601', '市辖区', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3127', '610602', '宝塔区', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3128', '610603', '安塞区', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3129', '610621', '延长县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3130', '610622', '延川县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3131', '610623', '子长县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3132', '610625', '志丹县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3133', '610626', '吴起县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3134', '610627', '甘泉县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3135', '610628', '富县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3136', '610629', '洛川县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3137', '610630', '宜川县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3138', '610631', '黄龙县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3139', '610632', '黄陵县', 'A', '3125');
INSERT INTO `dic_areas` VALUES ('3140', '610700', '汉中市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3141', '610701', '市辖区', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3142', '610702', '汉台区', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3143', '610721', '南郑县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3144', '610722', '城固县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3145', '610723', '洋县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3146', '610724', '西乡县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3147', '610725', '勉县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3148', '610726', '宁强县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3149', '610727', '略阳县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3150', '610728', '镇巴县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3151', '610729', '留坝县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3152', '610730', '佛坪县', 'A', '3140');
INSERT INTO `dic_areas` VALUES ('3153', '610800', '榆林市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3154', '610801', '市辖区', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3155', '610802', '榆阳区', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3156', '610803', '横山区', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3157', '610821', '神木县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3158', '610822', '府谷县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3159', '610824', '靖边县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3160', '610825', '定边县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3161', '610826', '绥德县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3162', '610827', '米脂县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3163', '610828', '佳县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3164', '610829', '吴堡县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3165', '610830', '清涧县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3166', '610831', '子洲县', 'A', '3153');
INSERT INTO `dic_areas` VALUES ('3167', '610900', '安康市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3168', '610901', '市辖区', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3169', '610902', '汉滨区', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3170', '610921', '汉阴县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3171', '610922', '石泉县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3172', '610923', '宁陕县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3173', '610924', '紫阳县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3174', '610925', '岚皋县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3175', '610926', '平利县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3176', '610927', '镇坪县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3177', '610928', '旬阳县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3178', '610929', '白河县', 'A', '3167');
INSERT INTO `dic_areas` VALUES ('3179', '611000', '商洛市', 'C', '3060');
INSERT INTO `dic_areas` VALUES ('3180', '611001', '市辖区', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3181', '611002', '商州区', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3182', '611021', '洛南县', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3183', '611022', '丹凤县', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3184', '611023', '商南县', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3185', '611024', '山阳县', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3186', '611025', '镇安县', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3187', '611026', '柞水县', 'A', '3179');
INSERT INTO `dic_areas` VALUES ('3188', '620000', '甘肃省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3189', '620100', '兰州市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3190', '620101', '市辖区', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3191', '620102', '城关区', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3192', '620103', '七里河区', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3193', '620104', '西固区', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3194', '620105', '安宁区', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3195', '620111', '红古区', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3196', '620121', '永登县', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3197', '620122', '皋兰县', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3198', '620123', '榆中县', 'A', '3189');
INSERT INTO `dic_areas` VALUES ('3199', '620200', '嘉峪关市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3200', '620201', '市辖区', 'A', '3199');
INSERT INTO `dic_areas` VALUES ('3201', '620300', '金昌市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3202', '620301', '市辖区', 'A', '3201');
INSERT INTO `dic_areas` VALUES ('3203', '620302', '金川区', 'A', '3201');
INSERT INTO `dic_areas` VALUES ('3204', '620321', '永昌县', 'A', '3201');
INSERT INTO `dic_areas` VALUES ('3205', '620400', '白银市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3206', '620401', '市辖区', 'A', '3205');
INSERT INTO `dic_areas` VALUES ('3207', '620402', '白银区', 'A', '3205');
INSERT INTO `dic_areas` VALUES ('3208', '620403', '平川区', 'A', '3205');
INSERT INTO `dic_areas` VALUES ('3209', '620421', '靖远县', 'A', '3205');
INSERT INTO `dic_areas` VALUES ('3210', '620422', '会宁县', 'A', '3205');
INSERT INTO `dic_areas` VALUES ('3211', '620423', '景泰县', 'A', '3205');
INSERT INTO `dic_areas` VALUES ('3212', '620500', '天水市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3213', '620501', '市辖区', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3214', '620502', '秦州区', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3215', '620503', '麦积区', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3216', '620521', '清水县', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3217', '620522', '秦安县', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3218', '620523', '甘谷县', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3219', '620524', '武山县', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3220', '620525', '张家川回族自治县', 'A', '3212');
INSERT INTO `dic_areas` VALUES ('3221', '620600', '武威市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3222', '620601', '市辖区', 'A', '3221');
INSERT INTO `dic_areas` VALUES ('3223', '620602', '凉州区', 'A', '3221');
INSERT INTO `dic_areas` VALUES ('3224', '620621', '民勤县', 'A', '3221');
INSERT INTO `dic_areas` VALUES ('3225', '620622', '古浪县', 'A', '3221');
INSERT INTO `dic_areas` VALUES ('3226', '620623', '天祝藏族自治县', 'A', '3221');
INSERT INTO `dic_areas` VALUES ('3227', '620700', '张掖市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3228', '620701', '市辖区', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3229', '620702', '甘州区', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3230', '620721', '肃南裕固族自治县', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3231', '620722', '民乐县', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3232', '620723', '临泽县', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3233', '620724', '高台县', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3234', '620725', '山丹县', 'A', '3227');
INSERT INTO `dic_areas` VALUES ('3235', '620800', '平凉市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3236', '620801', '市辖区', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3237', '620802', '崆峒区', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3238', '620821', '泾川县', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3239', '620822', '灵台县', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3240', '620823', '崇信县', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3241', '620824', '华亭县', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3242', '620825', '庄浪县', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3243', '620826', '静宁县', 'A', '3235');
INSERT INTO `dic_areas` VALUES ('3244', '620900', '酒泉市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3245', '620901', '市辖区', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3246', '620902', '肃州区', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3247', '620921', '金塔县', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3248', '620922', '瓜州县', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3249', '620923', '肃北蒙古族自治县', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3250', '620924', '阿克塞哈萨克族自治县', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3251', '620981', '玉门市', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3252', '620982', '敦煌市', 'A', '3244');
INSERT INTO `dic_areas` VALUES ('3253', '621000', '庆阳市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3254', '621001', '市辖区', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3255', '621002', '西峰区', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3256', '621021', '庆城县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3257', '621022', '环县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3258', '621023', '华池县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3259', '621024', '合水县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3260', '621025', '正宁县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3261', '621026', '宁县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3262', '621027', '镇原县', 'A', '3253');
INSERT INTO `dic_areas` VALUES ('3263', '621100', '定西市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3264', '621101', '市辖区', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3265', '621102', '安定区', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3266', '621121', '通渭县', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3267', '621122', '陇西县', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3268', '621123', '渭源县', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3269', '621124', '临洮县', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3270', '621125', '漳县', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3271', '621126', '岷县', 'A', '3263');
INSERT INTO `dic_areas` VALUES ('3272', '621200', '陇南市', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3273', '621201', '市辖区', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3274', '621202', '武都区', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3275', '621221', '成县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3276', '621222', '文县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3277', '621223', '宕昌县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3278', '621224', '康县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3279', '621225', '西和县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3280', '621226', '礼县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3281', '621227', '徽县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3282', '621228', '两当县', 'A', '3272');
INSERT INTO `dic_areas` VALUES ('3283', '622900', '临夏回族自治州', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3284', '622901', '临夏市', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3285', '622921', '临夏县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3286', '622922', '康乐县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3287', '622923', '永靖县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3288', '622924', '广河县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3289', '622925', '和政县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3290', '622926', '东乡族自治县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3291', '622927', '积石山保安族东乡族撒拉族自治县', 'A', '3283');
INSERT INTO `dic_areas` VALUES ('3292', '623000', '甘南藏族自治州', 'C', '3188');
INSERT INTO `dic_areas` VALUES ('3293', '623001', '合作市', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3294', '623021', '临潭县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3295', '623022', '卓尼县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3296', '623023', '舟曲县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3297', '623024', '迭部县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3298', '623025', '玛曲县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3299', '623026', '碌曲县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3300', '623027', '夏河县', 'A', '3292');
INSERT INTO `dic_areas` VALUES ('3301', '630000', '青海省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3302', '630100', '西宁市', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3303', '630101', '市辖区', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3304', '630102', '城东区', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3305', '630103', '城中区', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3306', '630104', '城西区', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3307', '630105', '城北区', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3308', '630121', '大通回族土族自治县', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3309', '630122', '湟中县', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3310', '630123', '湟源县', 'A', '3302');
INSERT INTO `dic_areas` VALUES ('3311', '630200', '海东市', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3312', '630202', '乐都区', 'A', '3311');
INSERT INTO `dic_areas` VALUES ('3313', '630203', '平安区', 'A', '3311');
INSERT INTO `dic_areas` VALUES ('3314', '630222', '民和回族土族自治县', 'A', '3311');
INSERT INTO `dic_areas` VALUES ('3315', '630223', '互助土族自治县', 'A', '3311');
INSERT INTO `dic_areas` VALUES ('3316', '630224', '化隆回族自治县', 'A', '3311');
INSERT INTO `dic_areas` VALUES ('3317', '630225', '循化撒拉族自治县', 'A', '3311');
INSERT INTO `dic_areas` VALUES ('3318', '632200', '海北藏族自治州', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3319', '632221', '门源回族自治县', 'A', '3318');
INSERT INTO `dic_areas` VALUES ('3320', '632222', '祁连县', 'A', '3318');
INSERT INTO `dic_areas` VALUES ('3321', '632223', '海晏县', 'A', '3318');
INSERT INTO `dic_areas` VALUES ('3322', '632224', '刚察县', 'A', '3318');
INSERT INTO `dic_areas` VALUES ('3323', '632300', '黄南藏族自治州', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3324', '632321', '同仁县', 'A', '3323');
INSERT INTO `dic_areas` VALUES ('3325', '632322', '尖扎县', 'A', '3323');
INSERT INTO `dic_areas` VALUES ('3326', '632323', '泽库县', 'A', '3323');
INSERT INTO `dic_areas` VALUES ('3327', '632324', '河南蒙古族自治县', 'A', '3323');
INSERT INTO `dic_areas` VALUES ('3328', '632500', '海南藏族自治州', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3329', '632521', '共和县', 'A', '3328');
INSERT INTO `dic_areas` VALUES ('3330', '632522', '同德县', 'A', '3328');
INSERT INTO `dic_areas` VALUES ('3331', '632523', '贵德县', 'A', '3328');
INSERT INTO `dic_areas` VALUES ('3332', '632524', '兴海县', 'A', '3328');
INSERT INTO `dic_areas` VALUES ('3333', '632525', '贵南县', 'A', '3328');
INSERT INTO `dic_areas` VALUES ('3334', '632600', '果洛藏族自治州', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3335', '632621', '玛沁县', 'A', '3334');
INSERT INTO `dic_areas` VALUES ('3336', '632622', '班玛县', 'A', '3334');
INSERT INTO `dic_areas` VALUES ('3337', '632623', '甘德县', 'A', '3334');
INSERT INTO `dic_areas` VALUES ('3338', '632624', '达日县', 'A', '3334');
INSERT INTO `dic_areas` VALUES ('3339', '632625', '久治县', 'A', '3334');
INSERT INTO `dic_areas` VALUES ('3340', '632626', '玛多县', 'A', '3334');
INSERT INTO `dic_areas` VALUES ('3341', '632700', '玉树藏族自治州', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3342', '632701', '玉树市', 'A', '3341');
INSERT INTO `dic_areas` VALUES ('3343', '632722', '杂多县', 'A', '3341');
INSERT INTO `dic_areas` VALUES ('3344', '632723', '称多县', 'A', '3341');
INSERT INTO `dic_areas` VALUES ('3345', '632724', '治多县', 'A', '3341');
INSERT INTO `dic_areas` VALUES ('3346', '632725', '囊谦县', 'A', '3341');
INSERT INTO `dic_areas` VALUES ('3347', '632726', '曲麻莱县', 'A', '3341');
INSERT INTO `dic_areas` VALUES ('3348', '632800', '海西蒙古族藏族自治州', 'C', '3301');
INSERT INTO `dic_areas` VALUES ('3349', '632801', '格尔木市', 'A', '3348');
INSERT INTO `dic_areas` VALUES ('3350', '632802', '德令哈市', 'A', '3348');
INSERT INTO `dic_areas` VALUES ('3351', '632821', '乌兰县', 'A', '3348');
INSERT INTO `dic_areas` VALUES ('3352', '632822', '都兰县', 'A', '3348');
INSERT INTO `dic_areas` VALUES ('3353', '632823', '天峻县', 'A', '3348');
INSERT INTO `dic_areas` VALUES ('3354', '640000', '宁夏回族自治区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3355', '640100', '银川市', 'C', '3354');
INSERT INTO `dic_areas` VALUES ('3356', '640101', '市辖区', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3357', '640104', '兴庆区', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3358', '640105', '西夏区', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3359', '640106', '金凤区', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3360', '640121', '永宁县', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3361', '640122', '贺兰县', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3362', '640181', '灵武市', 'A', '3355');
INSERT INTO `dic_areas` VALUES ('3363', '640200', '石嘴山市', 'C', '3354');
INSERT INTO `dic_areas` VALUES ('3364', '640201', '市辖区', 'A', '3363');
INSERT INTO `dic_areas` VALUES ('3365', '640202', '大武口区', 'A', '3363');
INSERT INTO `dic_areas` VALUES ('3366', '640205', '惠农区', 'A', '3363');
INSERT INTO `dic_areas` VALUES ('3367', '640221', '平罗县', 'A', '3363');
INSERT INTO `dic_areas` VALUES ('3368', '640300', '吴忠市', 'C', '3354');
INSERT INTO `dic_areas` VALUES ('3369', '640301', '市辖区', 'A', '3368');
INSERT INTO `dic_areas` VALUES ('3370', '640302', '利通区', 'A', '3368');
INSERT INTO `dic_areas` VALUES ('3371', '640303', '红寺堡区', 'A', '3368');
INSERT INTO `dic_areas` VALUES ('3372', '640323', '盐池县', 'A', '3368');
INSERT INTO `dic_areas` VALUES ('3373', '640324', '同心县', 'A', '3368');
INSERT INTO `dic_areas` VALUES ('3374', '640381', '青铜峡市', 'A', '3368');
INSERT INTO `dic_areas` VALUES ('3375', '640400', '固原市', 'C', '3354');
INSERT INTO `dic_areas` VALUES ('3376', '640401', '市辖区', 'A', '3375');
INSERT INTO `dic_areas` VALUES ('3377', '640402', '原州区', 'A', '3375');
INSERT INTO `dic_areas` VALUES ('3378', '640422', '西吉县', 'A', '3375');
INSERT INTO `dic_areas` VALUES ('3379', '640423', '隆德县', 'A', '3375');
INSERT INTO `dic_areas` VALUES ('3380', '640424', '泾源县', 'A', '3375');
INSERT INTO `dic_areas` VALUES ('3381', '640425', '彭阳县', 'A', '3375');
INSERT INTO `dic_areas` VALUES ('3382', '640500', '中卫市', 'C', '3354');
INSERT INTO `dic_areas` VALUES ('3383', '640501', '市辖区', 'A', '3382');
INSERT INTO `dic_areas` VALUES ('3384', '640502', '沙坡头区', 'A', '3382');
INSERT INTO `dic_areas` VALUES ('3385', '640521', '中宁县', 'A', '3382');
INSERT INTO `dic_areas` VALUES ('3386', '640522', '海原县', 'A', '3382');
INSERT INTO `dic_areas` VALUES ('3387', '650000', '新疆维吾尔自治区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3388', '650100', '乌鲁木齐市', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3389', '650101', '市辖区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3390', '650102', '天山区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3391', '650103', '沙依巴克区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3392', '650104', '新市区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3393', '650105', '水磨沟区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3394', '650106', '头屯河区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3395', '650107', '达坂城区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3396', '650109', '米东区', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3397', '650121', '乌鲁木齐县', 'A', '3388');
INSERT INTO `dic_areas` VALUES ('3398', '650200', '克拉玛依市', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3399', '650201', '市辖区', 'A', '3398');
INSERT INTO `dic_areas` VALUES ('3400', '650202', '独山子区', 'A', '3398');
INSERT INTO `dic_areas` VALUES ('3401', '650203', '克拉玛依区', 'A', '3398');
INSERT INTO `dic_areas` VALUES ('3402', '650204', '白碱滩区', 'A', '3398');
INSERT INTO `dic_areas` VALUES ('3403', '650205', '乌尔禾区', 'A', '3398');
INSERT INTO `dic_areas` VALUES ('3404', '650400', '吐鲁番市', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3405', '650402', '高昌区', 'A', '3404');
INSERT INTO `dic_areas` VALUES ('3406', '650421', '鄯善县', 'A', '3404');
INSERT INTO `dic_areas` VALUES ('3407', '650422', '托克逊县', 'A', '3404');
INSERT INTO `dic_areas` VALUES ('3408', '650500', '哈密市', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3409', '650502', '伊州区', 'A', '3408');
INSERT INTO `dic_areas` VALUES ('3410', '650521', '巴里坤哈萨克自治县', 'A', '3408');
INSERT INTO `dic_areas` VALUES ('3411', '650522', '伊吾县', 'A', '3408');
INSERT INTO `dic_areas` VALUES ('3412', '652300', '昌吉回族自治州', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3413', '652301', '昌吉市', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3414', '652302', '阜康市', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3415', '652323', '呼图壁县', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3416', '652324', '玛纳斯县', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3417', '652325', '奇台县', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3418', '652327', '吉木萨尔县', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3419', '652328', '木垒哈萨克自治县', 'A', '3412');
INSERT INTO `dic_areas` VALUES ('3420', '652700', '博尔塔拉蒙古自治州', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3421', '652701', '博乐市', 'A', '3420');
INSERT INTO `dic_areas` VALUES ('3422', '652702', '阿拉山口市', 'A', '3420');
INSERT INTO `dic_areas` VALUES ('3423', '652722', '精河县', 'A', '3420');
INSERT INTO `dic_areas` VALUES ('3424', '652723', '温泉县', 'A', '3420');
INSERT INTO `dic_areas` VALUES ('3425', '652800', '巴音郭楞蒙古自治州', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3426', '652801', '库尔勒市', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3427', '652822', '轮台县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3428', '652823', '尉犁县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3429', '652824', '若羌县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3430', '652825', '且末县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3431', '652826', '焉耆回族自治县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3432', '652827', '和静县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3433', '652828', '和硕县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3434', '652829', '博湖县', 'A', '3425');
INSERT INTO `dic_areas` VALUES ('3435', '652900', '阿克苏地区', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3436', '652901', '阿克苏市', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3437', '652922', '温宿县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3438', '652923', '库车县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3439', '652924', '沙雅县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3440', '652925', '新和县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3441', '652926', '拜城县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3442', '652927', '乌什县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3443', '652928', '阿瓦提县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3444', '652929', '柯坪县', 'A', '3435');
INSERT INTO `dic_areas` VALUES ('3445', '653000', '克孜勒苏柯尔克孜自治州', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3446', '653001', '阿图什市', 'A', '3445');
INSERT INTO `dic_areas` VALUES ('3447', '653022', '阿克陶县', 'A', '3445');
INSERT INTO `dic_areas` VALUES ('3448', '653023', '阿合奇县', 'A', '3445');
INSERT INTO `dic_areas` VALUES ('3449', '653024', '乌恰县', 'A', '3445');
INSERT INTO `dic_areas` VALUES ('3450', '653100', '喀什地区', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3451', '653101', '喀什市', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3452', '653121', '疏附县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3453', '653122', '疏勒县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3454', '653123', '英吉沙县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3455', '653124', '泽普县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3456', '653125', '莎车县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3457', '653126', '叶城县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3458', '653127', '麦盖提县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3459', '653128', '岳普湖县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3460', '653129', '伽师县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3461', '653130', '巴楚县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3462', '653131', '塔什库尔干塔吉克自治县', 'A', '3450');
INSERT INTO `dic_areas` VALUES ('3463', '653200', '和田地区', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3464', '653201', '和田市', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3465', '653221', '和田县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3466', '653222', '墨玉县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3467', '653223', '皮山县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3468', '653224', '洛浦县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3469', '653225', '策勒县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3470', '653226', '于田县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3471', '653227', '民丰县', 'A', '3463');
INSERT INTO `dic_areas` VALUES ('3472', '654000', '伊犁哈萨克自治州', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3473', '654002', '伊宁市', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3474', '654003', '奎屯市', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3475', '654004', '霍尔果斯市', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3476', '654021', '伊宁县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3477', '654022', '察布查尔锡伯自治县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3478', '654023', '霍城县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3479', '654024', '巩留县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3480', '654025', '新源县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3481', '654026', '昭苏县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3482', '654027', '特克斯县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3483', '654028', '尼勒克县', 'A', '3472');
INSERT INTO `dic_areas` VALUES ('3484', '654200', '塔城地区', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3485', '654201', '塔城市', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3486', '654202', '乌苏市', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3487', '654221', '额敏县', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3488', '654223', '沙湾县', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3489', '654224', '托里县', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3490', '654225', '裕民县', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3491', '654226', '和布克赛尔蒙古自治县', 'A', '3484');
INSERT INTO `dic_areas` VALUES ('3492', '654300', '阿勒泰地区', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3493', '654301', '阿勒泰市', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3494', '654321', '布尔津县', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3495', '654322', '富蕴县', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3496', '654323', '福海县', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3497', '654324', '哈巴河县', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3498', '654325', '青河县', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3499', '654326', '吉木乃县', 'A', '3492');
INSERT INTO `dic_areas` VALUES ('3500', '659000', '自治区直辖县级行政区划', 'C', '3387');
INSERT INTO `dic_areas` VALUES ('3501', '659001', '石河子市', 'A', '3500');
INSERT INTO `dic_areas` VALUES ('3502', '659002', '阿拉尔市', 'A', '3500');
INSERT INTO `dic_areas` VALUES ('3503', '659003', '图木舒克市', 'A', '3500');
INSERT INTO `dic_areas` VALUES ('3504', '659004', '五家渠市', 'A', '3500');
INSERT INTO `dic_areas` VALUES ('3505', '659006', '铁门关市', 'A', '3500');
INSERT INTO `dic_areas` VALUES ('3506', '710000', '台湾省', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3507', '810000', '香港特别行政区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3508', '820000', '澳门特别行政区', 'P', '0');
INSERT INTO `dic_areas` VALUES ('3509', '000000', '中国', 'H', null);

-- ----------------------------
-- Table structure for dic_sysconfig
-- ----------------------------
DROP TABLE IF EXISTS `dic_sysconfig`;
CREATE TABLE `dic_sysconfig` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `confCode` varchar(40) NOT NULL DEFAULT '' COMMENT '配置编码',
  `confName` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `confValue` varchar(255) DEFAULT '' COMMENT '配置值',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `i_confCode` (`confCode`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of dic_sysconfig
-- ----------------------------
INSERT INTO `dic_sysconfig` VALUES ('1', 'ali_accessKeyId', '阿里云accessKeyId', 'LTAIlF6FKDB61Blf', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('2', 'ali_accessKeySecret', '阿里云accessKeySecret', 'giqZkFubnOJVYhr2hLWvzkKnk9jkZw', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('4', 'regist_chekcode_expireTime', '注册短信验证码有效时间', '90', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('6', 'wei_AppId', 'APPID', 'wx69363f145b06ea8f', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('7', 'wei_MchId', 'MchID', '1485381352', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('8', 'wei_AppSecret', 'AppSecret', '43a9861de5368024fc4b3654c344c13a', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('9', 'wei_pay_body', 'wei_pay_body', '济南珩强商贸有限公司-商品', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('10', 'wei_pay_notifyUrl', '微信支付通知链接', 'http://shuimors.ujn.edu.cn/wxpay/handlePayNotify', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('12', 'order_close_time', '订单支付超时时间，单位分钟', '20', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('13', 'wei_ApiSecret', 'API密钥', 'b51c7a335b5b4a3a949050c8cab7a44f', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('14', 'wei_pay_productOrders_notifyUrl', '商品下单路径', 'http://shuimors.ujn.edu.cn/wxpay/handlePayProductOrdersNotify', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('15', 'wei_pay_memberShip_notifyUrl', '会员费路径', 'http://shuimors.ujn.edu.cn/wxpay/handlePayWeiMemberShipNotify', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('16', 'wei_pay_productDeposit_notifyUrl', '商品保证金支付路径', 'http://shuimors.ujn.edu.cn/wxpay/handlePayWeiProductDepositNotify', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('17', 'member_memberShip_fee', '会员费金额（单位分）', '1', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('18', 'product_deposit_fee', '保证金金额（单位分）', '2', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('21', 'wei_pay_to_user_rate', '微信之付，转到会员比率', '0.99', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('22', 'platform_specail_account_id', '平台账户', '99', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('23', 'ali_sms_tpl_regist_code', '阿里注册短信模板码', 'SMS_103115002', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('24', 'ali_sms_tpl_orders_code', '阿里通知短信模板码', 'SMS_103135002', null, null, '1');
INSERT INTO `dic_sysconfig` VALUES ('25', 'ali_sms_sign_name', '阿里注册短信签名', '水墨人生网', '2017-10-10 11:25:26', null, '1');

-- ----------------------------
-- Table structure for express_company
-- ----------------------------
DROP TABLE IF EXISTS `express_company`;
CREATE TABLE `express_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expressKey` varchar(30) DEFAULT NULL COMMENT '快递公司key',
  `expressCode` varchar(20) DEFAULT NULL COMMENT '快递公司编码',
  `expressName` varchar(60) DEFAULT NULL COMMENT '快递公司名称',
  PRIMARY KEY (`id`),
  KEY `expressKey` (`expressKey`),
  KEY `expressCode` (`expressCode`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of express_company
-- ----------------------------
INSERT INTO `express_company` VALUES ('1', 'EMS_GJTK', 'EMS', 'EMS国际特快专递');
INSERT INTO `express_company` VALUES ('2', 'EMS_GRTK', 'EMS', 'EMS国内特快专递');
INSERT INTO `express_company` VALUES ('3', 'EMS_GRTK_DS', 'EMS', 'EMS国内特快专递(代收货款)');
INSERT INTO `express_company` VALUES ('4', 'HHTTKD', 'TTKDEX', '海航天天快递');
INSERT INTO `express_company` VALUES ('5', 'SFXY', 'SF', '顺丰速运');
INSERT INTO `express_company` VALUES ('6', 'SFXY_HG', 'SF', '顺丰速运(香港)');
INSERT INTO `express_company` VALUES ('7', 'STKDDF', 'STO', '申通快递到付详情单');
INSERT INTO `express_company` VALUES ('8', 'STKDXQ', 'STO', '申通快递详情单');
INSERT INTO `express_company` VALUES ('9', 'STTBWL', 'STO', '申通淘宝物流');
INSERT INTO `express_company` VALUES ('10', 'TTKDYDA', 'TTKDEX', '天天快递运单A');
INSERT INTO `express_company` VALUES ('11', 'TTKDYDB', 'TTKDEX', '天天快递运单B');
INSERT INTO `express_company` VALUES ('12', 'YDKY', 'YUNDA', '韵达快运');
INSERT INTO `express_company` VALUES ('13', 'YTXD', 'YTO', '圆通速递');
INSERT INTO `express_company` VALUES ('14', 'YZBGD', 'POST', '邮政包裹单');
INSERT INTO `express_company` VALUES ('15', 'YZWLXQ', 'POST', '邮政物流详情单');
INSERT INTO `express_company` VALUES ('16', 'ZTXD', 'ZTO', '中通速递详情单');
INSERT INTO `express_company` VALUES ('17', 'KZND', 'NEDA', '港中能达速递详情单');
INSERT INTO `express_company` VALUES ('18', 'HTKY', 'HTKY', '汇通快运详情单');
INSERT INTO `express_company` VALUES ('19', 'QFKD', 'QFKD', '全峰快递');
INSERT INTO `express_company` VALUES ('20', 'ZJS_KD', 'ZJS', '宅急送快递单');
INSERT INTO `express_company` VALUES ('21', 'FEDEX', 'FEDEX', '联邦快递(国内)');
INSERT INTO `express_company` VALUES ('22', 'DBL', 'DBL', '德邦物流');
INSERT INTO `express_company` VALUES ('23', 'STARS', 'WLB-STARS', '星辰急便');
INSERT INTO `express_company` VALUES ('24', 'CCES', 'CCES', 'CCES快递详情单');
INSERT INTO `express_company` VALUES ('25', 'LB', 'LB', '龙邦物流');
INSERT INTO `express_company` VALUES ('26', 'FAST', 'FAST', '快捷速递');
INSERT INTO `express_company` VALUES ('27', 'UC', 'UC', '优速物流');
INSERT INTO `express_company` VALUES ('28', 'QRT', 'QRT', '全日通快递');
INSERT INTO `express_company` VALUES ('29', 'ZJS_DY', 'ZJS', '宅急送代收运单');
INSERT INTO `express_company` VALUES ('30', 'STARS_XF', 'WLB-STARS', '星辰急便鑫飞鸿');
INSERT INTO `express_company` VALUES ('31', 'CCES_DS', 'CCES', 'CCES快递详情单(代收货款)');
INSERT INTO `express_company` VALUES ('32', 'HTKY2', 'HTKY', '汇通快运详情单2');
INSERT INTO `express_company` VALUES ('33', 'HTKY3', 'HTKY', '汇通快运详情单3');
INSERT INTO `express_company` VALUES ('34', 'DBL2', 'DBL', '德邦物流2');
INSERT INTO `express_company` VALUES ('35', 'ZTXD2', 'ZTO', '中通速递详情单2');
INSERT INTO `express_company` VALUES ('36', 'ZTXD3', 'ZTO', '中通速递详情单3');
INSERT INTO `express_company` VALUES ('37', 'SUER', 'OTHER', '速尔快递');
INSERT INTO `express_company` VALUES ('38', 'YTZG', 'OTHER', '运通中港');
INSERT INTO `express_company` VALUES ('39', 'GTO_GD', 'GTO', '国通快递-广东省');
INSERT INTO `express_company` VALUES ('40', 'GTO', 'GTO', '国通快递-全国');
INSERT INTO `express_company` VALUES ('41', 'EMS_JJ', 'EMS', 'EMS国内经济快递');
INSERT INTO `express_company` VALUES ('42', 'YZXB', 'POSTB', '邮政国内小包');
INSERT INTO `express_company` VALUES ('43', 'YQWL', 'SHQ', '华强物流');
INSERT INTO `express_company` VALUES ('44', 'YTXDVIP', 'YTO', '圆通速递VIP');
INSERT INTO `express_company` VALUES ('45', 'YNPS', 'OTHER', '特能配送');
INSERT INTO `express_company` VALUES ('46', 'SJTC', 'OTHER', '世纪同城');
INSERT INTO `express_company` VALUES ('47', 'LTS', 'LTS', '联昊通速递');
INSERT INTO `express_company` VALUES ('48', 'KZND2', 'NEDA', '港中能达详情单');
INSERT INTO `express_company` VALUES ('49', 'QYKD', 'UAPEX', '全一快递');
INSERT INTO `express_company` VALUES ('50', 'YCT', 'YCT', '黑猫宅急便');
INSERT INTO `express_company` VALUES ('51', 'TKKD', 'OTHER', '同康快递');
INSERT INTO `express_company` VALUES ('52', 'ZXKD', 'OTHER', '中星速递');
INSERT INTO `express_company` VALUES ('53', 'XBXD', 'XB', '新邦速递');
INSERT INTO `express_company` VALUES ('54', 'YZSWXB', 'POSTB', '邮政商务小包');
INSERT INTO `express_company` VALUES ('55', 'CY', 'CYEXP', '长宇物流');
INSERT INTO `express_company` VALUES ('56', 'YTXDNEW', 'YTO', '圆通速递(最新)');
INSERT INTO `express_company` VALUES ('57', 'YDKY2', 'YUNDA', '韵达快运2');
INSERT INTO `express_company` VALUES ('58', 'HQKD', 'ZHQKD', '汇强快递');
INSERT INTO `express_company` VALUES ('59', 'QFKD2', 'QFKD', '全峰快递2');
INSERT INTO `express_company` VALUES ('60', 'HTKY4', 'HTKY', '汇通快运(新)');
INSERT INTO `express_company` VALUES ('61', 'TTKDYDC', 'TTKDEX', '天天快递');
INSERT INTO `express_company` VALUES ('62', 'HQKD2', 'ZHQKD', '汇强快递2');
INSERT INTO `express_company` VALUES ('63', 'STKDXQ2', 'STO', '申通快递详情单2');
INSERT INTO `express_company` VALUES ('64', 'YDKY3', 'YUNDA', '韵达快运3');
INSERT INTO `express_company` VALUES ('65', 'UCNEW', 'UC', '优速快递');
INSERT INTO `express_company` VALUES ('66', 'YTXD3', 'YTO', '圆通速递3');
INSERT INTO `express_company` VALUES ('67', 'STKDXQ3', 'STO', '申通快递详情单3');
INSERT INTO `express_company` VALUES ('68', 'HTKY5', 'HTKY', '百世汇通');
INSERT INTO `express_company` VALUES ('69', 'SFXYNEW', 'SF', '顺丰速运(新版)');
INSERT INTO `express_company` VALUES ('70', 'UC3', 'UC', '优速快递3');
INSERT INTO `express_company` VALUES ('71', 'YDKY4', 'YUNDA', '韵达快运4');
INSERT INTO `express_company` VALUES ('72', 'ZYXD', 'QRT', '增益速递');
INSERT INTO `express_company` VALUES ('73', 'XFWL', 'XFWL', '信丰物流');
INSERT INTO `express_company` VALUES ('74', 'ZTXD4', 'ZTO', '中通速递详情单4');
INSERT INTO `express_company` VALUES ('75', 'HTKY6', 'HTKY', '百世汇通2');
INSERT INTO `express_company` VALUES ('76', 'GTO2', 'GTO', '国通快递3');
INSERT INTO `express_company` VALUES ('77', 'FAST2', 'FAST', '快捷速递2');
INSERT INTO `express_company` VALUES ('78', 'ZJSNEW', 'ZJS', '宅急送(新)');
INSERT INTO `express_company` VALUES ('79', 'YDKY5', 'YUNDA', '韵达热敏打印');

-- ----------------------------
-- Table structure for express_img
-- ----------------------------
DROP TABLE IF EXISTS `express_img`;
CREATE TABLE `express_img` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expressKey` varchar(30) DEFAULT '' COMMENT '快递公司',
  `expressImg` varchar(255) DEFAULT '' COMMENT '快递公司图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of express_img
-- ----------------------------
INSERT INTO `express_img` VALUES ('1', 'EMS_GJTK', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2yQOxXvBXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('2', 'EMS_GRTK', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2u9ioXvVXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('3', 'EMS_GRTK_DS', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2CNtUXpNaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('4', 'HHTTKD', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T25LSBXApaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('5', 'SFXY', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2zY5BXAlaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('6', 'SFXY_HG', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2O75uXxtXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('7', 'STKDDF', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T22ACkXv4XXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('8', 'STKDXQ', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2EKSoXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('9', 'STTBWL', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2HYayXx0aXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('10', 'TTKDYDA', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2rKOeXqlaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('11', 'TTKDYDB', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2QJNAXyRaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('12', 'YDKY', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2wa4BXqpbXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('13', 'YTXD', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2TaWeXqXaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('14', 'YZBGD', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2E0OfXqlXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('15', 'YZWLXQ', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2Eg49XDFXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('16', 'ZTXD', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2e19zXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('17', 'KZND', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T26EzAXfdbXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('18', 'HTKY', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T21wmoXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('19', 'QFKD', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2T8GoXqdXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('20', 'ZJS_KD', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2dOeoXvVXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('21', 'FEDEX', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T22ZSoXydaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('22', 'DBL', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2nQ9AXqlXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('23', 'STARS', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T291yyXBlaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('24', 'CCES', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2MKewXyNaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('25', 'LB', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2yzGeXq4XXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('26', 'FAST', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2UOuoXqlXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('27', 'UC', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2u55kXvtaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('28', 'QRT', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2yHCAXtxXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('29', 'ZJS_DY', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T25larXD8XXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('30', 'STARS_XF', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2otSEXy8aXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('31', 'CCES_DS', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2MHirXp4XXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('32', 'HTKY2', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2389uXx4aXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('33', 'HTKY3', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2mdifXvVXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('34', 'DBL2', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2vDiSXulXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('35', 'ZTXD2', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2S1OfXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('36', 'ZTXD3', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2JnD7Xd0bXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('37', 'SUER', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2p.SEXyBaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('38', 'YTZG', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2CdOfXqlXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('39', 'GTO_GD', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2XYh_XwFaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('40', 'GTO', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2cXh8XBpaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('41', 'EMS_JJ', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2RZCoXqdXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('42', 'YZXB', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2nlQNXclbXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('43', 'YQWL', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2zLqpXvNXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('44', 'YTXDVIP', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2LZiyXAdaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('45', 'YNPS', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2inytXzFaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('46', 'SJTC', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2.i5zXBJaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('47', 'LTS', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2NHKyXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('48', 'KZND2', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2NQ9uXCdaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('49', 'QYKD', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2EOKsXyRaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('50', 'YCT', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2BR.TXaxbXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('51', 'TKKD', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T21OGXXvRXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('52', 'ZXKD', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2oRaGXA8aXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('53', 'XBXD', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2E05eXvNXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('54', 'YZSWXB', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2TySzXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('55', 'CY', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T20J1wXypaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('56', 'YTXDNEW', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2ZA5oXwFaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('57', 'YDKY', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2ieuwXvVXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('58', 'HQKD', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2QVF.XA4aXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('59', 'QFKD2', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2Bi9pXABaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('60', 'HTKY4', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T23mqxXpxXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('61', 'TTKDYDC', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2kVOpXzNaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('62', 'HQKD2', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2IyyrXv0XXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('63', 'STKDXQ2', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2acCxXDtaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('64', 'YDKY', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2wMOvXBNaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('65', 'UCNEW', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2Di1zXqdXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('66', 'YTXD3', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2h.qXXq0XXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('67', 'STKDXQ3', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2jU8FXrtaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('68', 'HTKY5', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T21s8BXwlaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('69', 'SFXYNEW', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2VSaGXqhXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('70', 'UC3', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2V7KkXvBaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('71', 'YDKY', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2bv9DXqlXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('72', 'ZYXD', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2nq9oXvdXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('73', 'XFWL', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2iHtsXytaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('74', 'ZTXD4', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2fW5WXvFaXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('75', 'HTKY6', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2oRi7XqlXXXXXXXXX_!!648670600.jpg');
INSERT INTO `express_img` VALUES ('76', 'GTO2', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2mJxhXFVXXXXXXXXX_!!648670600.png');
INSERT INTO `express_img` VALUES ('77', 'FAST2', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T29UtfXI0XXXXXXXXX_!!648670600.png');
INSERT INTO `express_img` VALUES ('78', 'ZJSNEW', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2HCahXOVXXXXXXXXX_!!648670600.png');
INSERT INTO `express_img` VALUES ('79', 'YZBGD', 'upload/huajinhua88/E_5c306f8f-0ab7-46b5-be8c-34383b617dc2.jpg');

-- ----------------------------
-- Table structure for express_template
-- ----------------------------
DROP TABLE IF EXISTS `express_template`;
CREATE TABLE `express_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expressName` varchar(30) DEFAULT NULL COMMENT '快递公司名',
  `expressKey` varchar(30) DEFAULT NULL COMMENT '快递公司key',
  `expressTplContent` text COMMENT '快递模板',
  `expressDesignhtml` text COMMENT '快递html模板',
  `expressBgimg` varchar(255) DEFAULT NULL COMMENT '图片',
  `isDefault` int(10) DEFAULT '0',
  `pagewidth` int(10) DEFAULT NULL,
  `pageheight` int(10) DEFAULT NULL,
  `offsetx` int(10) DEFAULT NULL,
  `offsety` int(10) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of express_template
-- ----------------------------
INSERT INTO `express_template` VALUES ('12', '海航天天快递', 'HHTTKD', '<span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 131px; left: 96px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 240px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderPost\" style=\"position: absolute; width: 150px; height: auto; top: 166px; left: 485px;\">寄件人邮编<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 70px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 131px; left: 96px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 240px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 166px; left: 485px;\"><font>{senderPost}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 70px;\"><font>{senderMobile}</font></div>', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T25LSBXApaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('13', '天天快递运单A', 'TTKDYDA', '<span class=\"ui-draggable unseled\" style=\"left: 476px; top: 151px; width: 150px; height: auto; position: absolute;\" key=\"senderMobile\">寄件人手机<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 103px; top: 137px; width: 150px; height: auto; position: absolute;\" key=\"senderCity\">寄件人所在市<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 68px; top: 186px; width: 150px; height: auto; position: absolute;\" key=\"senderAddress\">寄件人完整地址<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 454px; top: 205px; width: 150px; height: auto; position: absolute;\" key=\"senderPost\">寄件人邮编<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 250px; top: 195px; width: 150px; height: auto; position: absolute;\" key=\"senderName\">寄件人姓名<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 274px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 355px; left: 82px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 227px; left: 101px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 317px; left: 591px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 185px; left: 623px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"left: 476px; top: 151px; width: 150px; height: auto; position: absolute;\"><font>{senderMobile}</font></div><div style=\"left: 103px; top: 137px; width: 150px; height: auto; position: absolute;\"><font>{senderCity}</font></div><div style=\"left: 68px; top: 186px; width: 150px; height: auto; position: absolute;\"><font>{senderAddress}</font></div><div style=\"left: 454px; top: 205px; width: 150px; height: auto; position: absolute;\"><font>{senderPost}</font></div><div style=\"left: 250px; top: 195px; width: 150px; height: auto; position: absolute;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 274px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 355px; left: 82px;\"><font>{senderMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 227px; left: 101px;\"><font>{senderMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 317px; left: 591px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 185px; left: 623px;\"><font>{senderTel}</font></div>', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2rKOeXqlaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('14', '韵达快运', 'YDKY', '<span class=\"ui-draggable seled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 147px; left: 191px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 147px; left: 191px;\"><font>{senderName}</font></div>', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2ieuwXvVXXXXXXXXX_!!648670600.jpg', '0', '220', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('15', '申通快递详情单', 'STKDXQ', '<span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 104px; left: 91px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderCity\" style=\"position: absolute; width: 150px; height: auto; top: 180px; left: 118px;\">寄件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 113px; left: 441px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 104px; left: 91px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 180px; left: 118px;\"><font>{senderCity}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 113px; left: 441px;\"><font>{senderMobile}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2EKSoXqhXXXXXXXXX_!!648670600.jpg', '0', '210', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('16', '顺丰速运', 'SFXY', '<span class=\"ui-draggable ui-resizable unseled\" style=\"left: 96px; top: 257px; width: 150px; height: auto; position: absolute;\" key=\"senderProvince\">寄件人所在省<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable ui-resizable unseled\" style=\"left: 97px; top: 177px; width: 126px; height: 19px; font-size: 14px; font-weight: bold; position: absolute;\" key=\"senderTel\">寄件人电话<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable ui-resizable unseled\" style=\"left: 243px; top: 174px; width: 107px; height: 19px; position: absolute;\" key=\"senderTel\">寄件人电话<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"senderName\" style=\"position: absolute; width: 80px; height: 17px; top: 132px; left: 281px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span>', '<div style=\"left: 96px; top: 257px; width: 150px; height: auto; position: absolute;\"><font>{senderProvince}</font></div><div style=\"left: 97px; top: 177px; width: 126px; height: 19px; font-size: 14px; font-weight: bold; position: absolute;\"><font>{senderTel}</font></div><div style=\"left: 243px; top: 174px; width: 107px; height: 19px; position: absolute;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 80px; height: 17px; top: 132px; left: 281px;\"><font>{senderName}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2zY5BXAlaXXXXXXXX_!!648670600.jpg', '1', '210', '137', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('17', '圆通速递', 'YTXD', '<span class=\"ui-draggable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 165px; left: 220px; font-weight: bold; font-family: 黑体; font-size: 16px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 199px; left: 161px; font-weight: bold; font-family: 黑体; font-size: 16px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 133px; left: 496px; font-weight: bold; font-family: 黑体; font-size: 16px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 218px; left: 546px; font-weight: normal; font-family: 楷体; font-size: 24px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 129px; left: 118px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 165px; left: 220px; font-weight: bold; font-family: 黑体; font-size: 16px;\"><font>{senderMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 199px; left: 161px; font-weight: bold; font-family: 黑体; font-size: 16px;\"><font>{senderProvince}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 133px; left: 496px; font-weight: bold; font-family: 黑体; font-size: 16px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 218px; left: 546px; font-weight: normal; font-family: 楷体; font-size: 24px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 129px; left: 118px;\"><font>{senderTel}</font></div>', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2TaWeXqXaXXXXXXXX_!!648670600.jpg', '0', '210', '130', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('18', '申通淘宝物流', 'STTBWL', '<span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 197px; left: 139px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderCity\" style=\"position: absolute; width: 150px; height: auto; top: 250px; left: 159px;\">寄件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 182px; left: 507px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderTown\" style=\"position: absolute; width: 150px; height: auto; top: 240px; left: 479px;\">寄件人所在区/县<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 197px; left: 139px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 250px; left: 159px;\"><font>{senderCity}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 182px; left: 507px;\"><font>{senderMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 240px; left: 479px;\"><font>{senderTown}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2HYayXx0aXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('19', '全峰快递', 'QFKD', '<span class=\"ui-draggable unseled\" style=\"left: 285px; top: 190px; width: 150px; height: auto; font-weight: normal; position: absolute;\" key=\"senderPost\">寄件人邮编<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable seled\" style=\"left: 266px; top: 220px; width: 150px; height: auto; font-family: 楷体; font-size: 24px; font-weight: normal; position: absolute;\" key=\"senderMobile\">寄件人手机<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 85px; top: 218px; width: 150px; height: auto; position: absolute;\" key=\"senderTel\">寄件人电话<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 96px; top: 156px; width: 150px; height: auto; position: absolute;\" key=\"senderAddress\">寄件人完整地址<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 271px; top: 91px; width: 150px; height: auto; position: absolute;\" key=\"senderCity\">寄件人所在市<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span><span class=\"ui-draggable unseled\" style=\"left: 113px; top: 92px; width: 150px; height: auto; position: absolute;\" key=\"senderName\">寄件人姓名<a title=\"删除\" class=\"delX\" onclick=\"ExpBuilder.delElement(this)\" href=\"javascript:void(0);\"></a></span>', '<div style=\"left: 285px; top: 190px; width: 150px; height: auto; font-weight: normal; position: absolute;\"><font>{senderPost}</font></div><div style=\"left: 266px; top: 220px; width: 150px; height: auto; font-family: 楷体; font-size: 24px; font-weight: normal; position: absolute;\"><font>{senderMobile}</font></div><div style=\"left: 85px; top: 218px; width: 150px; height: auto; position: absolute;\"><font>{senderTel}</font></div><div style=\"left: 96px; top: 156px; width: 150px; height: auto; position: absolute;\"><font>{senderAddress}</font></div><div style=\"left: 271px; top: 91px; width: 150px; height: auto; position: absolute;\"><font>{senderCity}</font></div><div style=\"left: 113px; top: 92px; width: 150px; height: auto; position: absolute;\"><font>{senderName}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2T8GoXqdXXXXXXXXX_!!648670600.jpg', '0', '210', '130', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('20', '邮政包裹单', 'YZBGD', '<span class=\"ui-draggable ui-resizable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 216px; left: 332px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 331px; left: 157px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"seller_name\" style=\"position: absolute; width: 150px; height: auto; top: 63px; left: 57px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"seller_phone\" style=\"position: absolute; width: 150px; height: auto; top: 105px; left: 96px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span><span class=\"ui-draggable seled ui-resizable\" key=\"city\" style=\"position: absolute; width: 150px; height: auto; top: 191px; left: 86px;\">寄件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-e\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-s\"></div><div style=\"z-index: 90;\" class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 216px; left: 332px;\"><font>{senderProvince}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 331px; left: 157px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 63px; left: 57px;\"><font>{seller_name}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 105px; left: 96px;\"><font>{seller_phone}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 191px; left: 86px;\"><font>{city}</font></div>', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2E0OfXqlXXXXXXXXX_!!648670600.jpg', '0', '210', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('21', '宅急送快递单', 'ZJS_KD', '<span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 162px; left: 220px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 211px; left: 111px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderTown\" style=\"position: absolute; width: 150px; height: auto; top: 284px; left: 219px;\">寄件人所在区/县<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 162px; left: 220px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 211px; left: 111px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 284px; left: 219px;\"><font>{senderTown}</font></div>', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2dOeoXvVXXXXXXXXX_!!648670600.jpg', '0', '210', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('22', '港中能达速递详情单', 'KZND', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\"><font>{senderProvince}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\"><font>{senderMobile}</font></div>', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T26EzAXfdbXXXXXXXX_!!648670600.jpg', '0', '210', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('23', 'EMS国内特快专递(代收货款)', 'EMS_GRTK_DS', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2CNtUXpNaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('24', '星辰急便', 'STARS', '<span class=\"ui-draggable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 177px; left: 165px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderCity\" style=\"position: absolute; width: 150px; height: auto; top: 242px; left: 204px;\">寄件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 177px; left: 165px;\"><font>{senderMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 242px; left: 204px;\"><font>{senderCity}</font></div>', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T291yyXBlaXXXXXXXX_!!648670600.jpg', '0', '210', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('25', '联邦快递(国内)', 'FEDEX', '<span class=\"ui-draggable seled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 168px; left: 203px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 168px; left: 203px;\"><font>{senderName}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T22ZSoXydaXXXXXXXX_!!648670600.jpg', '0', '213', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('26', '申通快递到付详情单', 'STKDDF', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img04.taobaocdn.com/imgextra/i4/648670600/T22ACkXv4XXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('27', '顺丰速运', 'SFXY', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2zY5BXAlaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('28', '韵达快运', 'YDKY', '<span class=\"ui-draggable ui-resizable unseled\" key=\"contactName\" style=\"position: absolute; width: 104px; height: 20px; top: 86px; left: 95px; font-weight: bold;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"province\" style=\"position: absolute; width: 113px; height: auto; top: 86px; left: 254px; font-weight: bold;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"addr\" style=\"position: absolute; width: 271px; height: auto; top: 146px; left: 95px; font-weight: bold;\">寄件人详细地址<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 223px; left: 434px; font-weight: bold;\" key=\"receiverName\" class=\"ui-draggable ui-resizable unseled\">收件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 127px; height: auto; top: 89px; left: 427px; font-weight: bold;\" key=\"receiverCity\" class=\"ui-draggable ui-resizable unseled\">收件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 115px; height: auto; top: 105px; left: 610px; font-weight: bold;\" key=\"receiverMobile\" class=\"ui-draggable ui-resizable unseled\">收件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 146px; left: 438px;\" key=\"receiverAddress\" class=\"ui-draggable seled ui-resizable\">收件人所在地址<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 104px; height: 20px; top: 86px; left: 95px; font-weight: bold;\"><font>{contactName}</font></div><div style=\"position: absolute; width: 113px; height: auto; top: 86px; left: 254px; font-weight: bold;\"><font>{province}</font></div><div style=\"position: absolute; width: 271px; height: auto; top: 146px; left: 95px; font-weight: bold;\"><font>{addr}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 223px; left: 434px; font-weight: bold;\"><font>{receiverName}</font></div><div style=\"position: absolute; width: 127px; height: auto; top: 89px; left: 427px; font-weight: bold;\"><font>{receiverCity}</font></div><div style=\"position: absolute; width: 115px; height: auto; top: 105px; left: 610px; font-weight: bold;\"><font>{receiverMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 146px; left: 438px;\"><font>{receiverAddress}</font></div>', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2wa4BXqpbXXXXXXXX_!!648670600.jpg', '1', '220', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('29', '港中能达速递详情单', 'KZND', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img02.taobaocdn.com/imgextra/i2/648670600/T26EzAXfdbXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('30', '海航天天快递', 'HHTTKD', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img01.taobaocdn.com/imgextra/i1/648670600/T25LSBXApaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('31', '申通淘宝物流', 'STTBWL', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2HYayXx0aXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('32', '顺丰速运(香港)', 'SFXY_HG', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2HYayXx0aXXXXXXXX_!!648670600.jpg', '0', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('33', '申通快递详情单', 'STKDXQ', '<span style=\"position: absolute; width: 135px; height: 23px; top: 104px; left: 143px; font-weight: bold; font-size: 14px;\" key=\"mobilePhone\" class=\"ui-draggable ui-resizable unseled\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 251px; height: auto; top: 187px; left: 305px; font-weight: bold; font-size: 29px;\" key=\"city\" class=\"ui-draggable ui-resizable unseled\">寄件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 140px; left: 478px; font-weight: bold; font-size: 29px;\" key=\"addr\" class=\"ui-draggable ui-resizable unseled\">寄件人详细地址<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 181px; height: 35px; top: 147px; left: 74px; font-weight: bold; font-size: 29px;\" key=\"province\" class=\"ui-draggable ui-resizable unseled\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 212px; left: 74px;\" key=\"receiverMobile\" class=\"ui-draggable ui-resizable unseled\">收件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 321px; left: 94px;\" key=\"buyerMessage\" class=\"ui-draggable ui-resizable seled\">买家留言<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 135px; height: 23px; top: 104px; left: 143px; font-weight: bold; font-size: 14px;\"><font>{mobilePhone}</font></div><div style=\"position: absolute; width: 251px; height: auto; top: 187px; left: 305px; font-weight: bold; font-size: 29px;\"><font>{city}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 140px; left: 478px; font-weight: bold; font-size: 29px;\"><font>{addr}</font></div><div style=\"position: absolute; width: 181px; height: 35px; top: 147px; left: 74px; font-weight: bold; font-size: 29px;\"><font>{province}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 212px; left: 74px;\"><font>{receiverMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 321px; left: 94px;\"><font>{buyerMessage}</font></div>', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2ieuwXvVXXXXXXXXX_!!648670600.jpg', '0', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('34', '圆通速递', 'YTXD', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2TaWeXqXaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('35', '中通速递详情单', 'ZTXD', '<span class=\"ui-draggable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 164px; left: 436px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span><span class=\"ui-draggable seled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a></span>', null, 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2e19zXqhXXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('36', '天天快递运单A', 'TTKDYDA', '<span style=\"position: absolute; width: 150px; height: auto; top: 127px; left: 197px;\" key=\"zipCode\" class=\"ui-draggable ui-resizable unseled\">寄件人邮编<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 174px; left: 180px;\" key=\"province\" class=\"ui-draggable ui-resizable unseled\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 212px; left: 31px;\" key=\"province\" class=\"ui-draggable seled ui-resizable\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', null, 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2rKOeXqlaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('37', '顺丰速运', 'SFXY', '<span style=\"position: absolute; width: 150px; height: auto; top: 171px; left: 106px;\" key=\"phone\" class=\"ui-draggable ui-resizable unseled\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 120px; left: 120px;\" key=\"phone\" class=\"ui-draggable ui-resizable unseled\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 309px; left: 135px;\" key=\"province\" class=\"ui-draggable ui-resizable unseled\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 258px; left: 111px;\" key=\"buyerNick\" class=\"ui-draggable seled ui-resizable\">买家昵称<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 171px; left: 106px;\"><font>{phone}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 120px; left: 120px;\"><font>{phone}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 309px; left: 135px;\"><font>{province}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 258px; left: 111px;\"><font>{buyerNick}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2zY5BXAlaXXXXXXXX_!!648670600.jpg', '0', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('42', '韵达快运', 'YDKY', '<span style=\"position: absolute; width: 96px; height: auto; top: 86px; left: 100px;\" key=\"contact_name\" class=\"ui-draggable ui-resizable seled\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 273px; height: auto; top: 163px; left: 78px;\" key=\"addr\" class=\"ui-draggable ui-resizable unseled\">寄件人详细地址<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 240px;\" key=\"province\" class=\"ui-draggable ui-resizable unseled\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 250px; height: auto; top: 115px; left: 107px;\" key=\"seller_company\" class=\"ui-draggable ui-resizable unseled\">寄件人公司<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 196px; left: 77px;\" key=\"memo\" class=\"ui-draggable ui-resizable unseled\">寄件人备注<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 101px; height: auto; top: 224px; left: 445px;\" key=\"receiverName\" class=\"ui-draggable ui-resizable unseled\">收件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 226px; left: 158px;\" key=\"phone\" class=\"ui-draggable ui-resizable unseled\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 121px; height: auto; top: 228px; left: 242px;\" key=\"zip_code\" class=\"ui-draggable ui-resizable unseled\">寄件人邮编<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 602px;\" key=\"receiverPhone\" class=\"ui-draggable ui-resizable unseled\">收件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 407px;\" key=\"receiverProvince\" class=\"ui-draggable ui-resizable unseled\">收件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 450px;\" key=\"receiverCity\" class=\"ui-draggable ui-resizable unseled\">收件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 268px; height: auto; top: 144px; left: 444px;\" key=\"receiverAddr\" class=\"ui-draggable ui-resizable unseled\">收件人所在地址<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 212px; height: auto; top: 184px; left: 413px;\" key=\"buyerMemo\" class=\"ui-draggable ui-resizable unseled\">买家留言<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 278px; left: 147px;\" key=\"totalPrice\" class=\"ui-draggable ui-resizable unseled\">订单总价<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 304px; left: 162px;\" key=\"freight\" class=\"ui-draggable ui-resizable unseled\">运费<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 8px; left: 26px;\" key=\"orderSn\" class=\"ui-draggable ui-resizable unseled\">订单编号<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 6px; left: 294px;\" key=\"orderCreated\" class=\"ui-draggable ui-resizable unseled\">下单时间<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 86px; left: 285px;\" key=\"city\" class=\"ui-draggable ui-resizable unseled\">寄件人所在市<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 317px;\" key=\"country\" class=\"ui-draggable ui-resizable unseled\">寄件人所在区/县<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 96px; height: auto; top: 86px; left: 100px;\"><font>{contact_name}</font></div><div style=\"position: absolute; width: 273px; height: auto; top: 163px; left: 78px;\"><font>{addr}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 240px;\"><font>{province}</font></div><div style=\"position: absolute; width: 250px; height: auto; top: 115px; left: 107px;\"><font>{seller_company}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 196px; left: 77px;\"><font>{memo}</font></div><div style=\"position: absolute; width: 101px; height: auto; top: 224px; left: 445px;\"><font>{receiverName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 226px; left: 158px;\"><font>{phone}</font></div><div style=\"position: absolute; width: 121px; height: auto; top: 228px; left: 242px;\"><font>{zip_code}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 602px;\"><font>{receiverPhone}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 407px;\"><font>{receiverProvince}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 450px;\"><font>{receiverCity}</font></div><div style=\"position: absolute; width: 268px; height: auto; top: 144px; left: 444px;\"><font>{receiverAddr}</font></div><div style=\"position: absolute; width: 212px; height: auto; top: 184px; left: 413px;\"><font>{buyerMemo}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 278px; left: 147px;\"><font>{totalPrice}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 304px; left: 162px;\"><font>{freight}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 8px; left: 26px;\"><font>{orderSn}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 6px; left: 294px;\"><font>{orderCreated}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 86px; left: 285px;\"><font>{city}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 85px; left: 317px;\"><font>{country}</font></div>', 'http://img04.taobaocdn.com/imgextra/i4/648670600/T2wa4BXqpbXXXXXXXX_!!648670600.jpg', '1', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('48', '中通速递详情单', 'ZTXD', '<span style=\"position: absolute; width: 150px; height: auto; top: 133px; left: 219px;\" key=\"contactName\" class=\"ui-draggable ui-resizable unseled\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 89px; left: 464px;\" key=\"receiverName\" class=\"ui-draggable ui-resizable unseled\">收件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 185px; left: 101px;\" key=\"province\" class=\"ui-draggable ui-resizable seled\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 133px; left: 219px;\"><font>{contactName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 89px; left: 464px;\"><font>{receiverName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 185px; left: 101px;\"><font>{province}</font></div>', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T2e19zXqhXXXXXXXXX_!!648670600.jpg', '0', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('49', '天天快递运单A', 'TTKDYDA', '<span style=\"position: absolute; width: 150px; height: auto; top: 127px; left: 197px;\" key=\"zipCode\" class=\"ui-draggable ui-resizable unseled\">寄件人邮编<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 174px; left: 180px;\" key=\"province\" class=\"ui-draggable ui-resizable unseled\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span style=\"position: absolute; width: 150px; height: auto; top: 212px; left: 31px;\" key=\"province\" class=\"ui-draggable seled ui-resizable\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 127px; left: 197px;\"><font>{zipCode}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 174px; left: 180px;\"><font>{province}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 212px; left: 31px;\"><font>{province}</font></div>', 'http://img02.taobaocdn.com/imgextra/i2/648670600/T2rKOeXqlaXXXXXXXX_!!648670600.jpg', '0', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('50', '申通快递详情单', 'STKDXQ', '<span style=\"position: absolute; width: 150px; height: auto; top: 107px; left: 233px;\" key=\"zipCode\" class=\"ui-draggable ui-resizable seled\">寄件人邮编<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 107px; left: 233px;\"><font>{zipCode}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2EKSoXqhXXXXXXXXX_!!648670600.jpg', '0', '210', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('51', '申通淘宝物流', 'STTBWL', '<span class=\"ui-draggable ui-resizable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 159px; left: 118px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable seled ui-resizable\" key=\"senderTown\" style=\"position: absolute; width: 150px; height: auto; top: 160px; left: 416px;\">寄件人所在区/县<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 159px; left: 118px;\"><font>{senderMobile}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 160px; left: 416px;\"><font>{senderTown}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2HYayXx0aXXXXXXXX_!!648670600.jpg', '0', '210', '170', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('52', '顺丰速运(香港)', 'SFXY_HG', '<span class=\"ui-draggable ui-resizable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"senderProvince\" style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\">寄件人所在省<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 135px; left: 152px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 190px; left: 131px;\"><font>{senderProvince}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 189px; left: 322px;\"><font>{senderMobile}</font></div>', 'http://img03.taobaocdn.com/imgextra/i3/648670600/T2O75uXxtXXXXXXXXX_!!648670600.jpg', '0', '230', '127', '0', '0', null, null, '');
INSERT INTO `express_template` VALUES ('53', '海航天天快递', 'HHTTKD', '<span class=\"ui-draggable ui-resizable unseled\" key=\"senderName\" style=\"position: absolute; width: 150px; height: auto; top: 131px; left: 96px;\">寄件人姓名<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"senderTel\" style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 240px;\">寄件人电话<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable ui-resizable unseled\" key=\"senderPost\" style=\"position: absolute; width: 150px; height: auto; top: 166px; left: 485px;\">寄件人邮编<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span><span class=\"ui-draggable seled ui-resizable\" key=\"senderMobile\" style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 70px;\">寄件人手机<a class=\"delX\" href=\"javascript:void(0);\" title=\"删除\" onclick=\"ExpBuilder.delElement(this)\"></a><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></span>', '<div style=\"position: absolute; width: 150px; height: auto; top: 131px; left: 96px;\"><font>{senderName}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 240px;\"><font>{senderTel}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 166px; left: 485px;\"><font>{senderPost}</font></div><div style=\"position: absolute; width: 150px; height: auto; top: 257px; left: 70px;\"><font>{senderMobile}</font></div>', 'http://img01.taobaocdn.com/imgextra/i1/648670600/T25LSBXApaXXXXXXXX_!!648670600.jpg', '0', '0', '0', '0', '0', null, null, '');

-- ----------------------------
-- Table structure for favorite
-- ----------------------------
DROP TABLE IF EXISTS `favorite`;
CREATE TABLE `favorite` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `buyerId` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `productId` bigint(20) NOT NULL COMMENT '商品id',
  `createTime` datetime DEFAULT NULL COMMENT '创建人',
  `updateTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除 3失效#',
  PRIMARY KEY (`id`),
  KEY `m_id` (`buyerId`),
  KEY `productId` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='商品收藏';

-- ----------------------------
-- Records of favorite
-- ----------------------------
INSERT INTO `favorite` VALUES ('1', '54', '4', null, null, '2');
INSERT INTO `favorite` VALUES ('2', '56', '13', null, null, '2');

-- ----------------------------
-- Table structure for full_cut
-- ----------------------------
DROP TABLE IF EXISTS `full_cut`;
CREATE TABLE `full_cut` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `sellerId` bigint(20) DEFAULT NULL,
  `fullCutName` varchar(255) DEFAULT NULL COMMENT '满减送活动名称',
  `startDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '活动开始时间',
  `endDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '活动结束时间',
  `isAllProduct` varchar(1) DEFAULT '2' COMMENT '是否使用所以商品_D#1.所有商品，2：部分商品#',
  `fullCutType` varchar(1) DEFAULT NULL COMMENT '满减活动类型_D#1,满金额减现金，2，满金额打折，3,满数量减现金，4，满数量打折#',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除，3结束#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='满减送';

-- ----------------------------
-- Records of full_cut
-- ----------------------------
INSERT INTO `full_cut` VALUES ('3', null, '1', '满300包邮', '2017-03-12 00:00:00', '2017-03-19 00:00:00', null, null, null, null, '');

-- ----------------------------
-- Table structure for full_cut_product
-- ----------------------------
DROP TABLE IF EXISTS `full_cut_product`;
CREATE TABLE `full_cut_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullCutId` bigint(20) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  `specificationValue` varchar(255) DEFAULT '' COMMENT '参与促销的规格',
  `isUnifiedSpec` varchar(1) NOT NULL DEFAULT '1' COMMENT '是否统一规格促销_D# 1统一规格 2不统一规格#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of full_cut_product
-- ----------------------------
INSERT INTO `full_cut_product` VALUES ('4', '3', '6', '', '1');

-- ----------------------------
-- Table structure for full_cut_set
-- ----------------------------
DROP TABLE IF EXISTS `full_cut_set`;
CREATE TABLE `full_cut_set` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fullCutId` bigint(20) DEFAULT NULL COMMENT '满减送活动id',
  `meetLine` bigint(20) DEFAULT NULL COMMENT '满多少元',
  `discountCash` bigint(20) DEFAULT NULL COMMENT '减现金数',
  `disCountZhekou` float(5,2) DEFAULT NULL COMMENT '折扣值',
  `isPostage` varchar(1) DEFAULT '0' COMMENT '是否包邮_D# 0 不包邮 1 包邮#',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of full_cut_set
-- ----------------------------
INSERT INTO `full_cut_set` VALUES ('3', '3', '300', '30', '0.00', '1', null, null, '');
INSERT INTO `full_cut_set` VALUES ('4', '3', '100', '10', '0.00', '0', null, null, '');
INSERT INTO `full_cut_set` VALUES ('5', '3', '200', '20', '0.00', '0', null, null, '');
INSERT INTO `full_cut_set` VALUES ('6', '3', '2', '30', '0.00', '1', null, null, '');
INSERT INTO `full_cut_set` VALUES ('7', '3', '5', '10', '0.00', '0', null, null, '');
INSERT INTO `full_cut_set` VALUES ('8', '3', '10', '20', '0.00', '0', null, null, '');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `shopId` bigint(20) DEFAULT NULL COMMENT '所属店铺',
  `openId` varchar(36) DEFAULT NULL COMMENT '微信openid',
  `nickName` varchar(60) DEFAULT '' COMMENT '昵称',
  `name` varchar(60) DEFAULT '' COMMENT '真实姓名_Q@like@',
  `memberNum` varchar(30) DEFAULT '' COMMENT '会员号_U_Q@like@',
  `idCardNum` varchar(30) DEFAULT '' COMMENT '身份证号码',
  `phone` varchar(30) DEFAULT '' COMMENT '电话_Q@like@',
  `email` varchar(60) DEFAULT NULL COMMENT '邮箱_U_Q@like@',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别',
  `birthday` varchar(20) DEFAULT '' COMMENT '生日',
  `point` int(11) DEFAULT '0' COMMENT '积分',
  `introducer` bigint(20) DEFAULT NULL COMMENT '推荐人',
  `type` varchar(1) DEFAULT '' COMMENT '会员类型',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '修改时间',
  `status` varchar(1) DEFAULT '1' COMMENT '状态_D# 0禁用，1正常(审核通过)2  注册完成,3已缴纳保证金， 5注销，6删除,#',
  `currentMoney` bigint(20) DEFAULT '0' COMMENT '当前账户金额',
  `invitationCode` varchar(20) DEFAULT '' COMMENT '推广码',
  PRIMARY KEY (`id`),
  KEY `invitationCode` (`invitationCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('54', '54', 'o_ZQ4wM5yYa4xRDbyBmh2ESBALmg', 'zlj', '朱连江', '', '', '13793172269', null, '1', '', '0', null, '', null, null, '1', '16', 'p81mql');
INSERT INTO `member` VALUES ('55', '55', 'o_ZQ4wOXyTwTUhMpNbstXwQfEExs', '11', '22', '', '', '18953136382', null, '1', '', '0', null, '', null, null, '1', '1', 'wg6cit');
INSERT INTO `member` VALUES ('56', '56', 'o_ZQ4wDp1trl53_La8h-puQywcF4', '中', '中', '', '', '13954190863', null, '0', '', '0', null, '', null, null, '1', '7', 'lqz5vm');
INSERT INTO `member` VALUES ('57', '57', 'o_ZQ4wEnnbeGa55Y6iuGVZSZwVFA', '15588873006', '张为强', '', '', '15588873006', null, '1', '', '0', null, '', null, null, '1', '0', '87362e');

-- ----------------------------
-- Table structure for member_account_deposit
-- ----------------------------
DROP TABLE IF EXISTS `member_account_deposit`;
CREATE TABLE `member_account_deposit` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `productId` bigint(20) DEFAULT NULL COMMENT '商品',
  `specificationValue` varchar(255) DEFAULT '',
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '保证金缴纳单号',
  `paymentNo` varchar(60) DEFAULT '' COMMENT '第三方支付单号',
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `amount` bigint(20) DEFAULT '0' COMMENT '金额',
  `payStatus` varchar(1) DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `paymentType` varchar(1) DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) DEFAULT '' COMMENT '充值方式名称',
  `transactionTime` datetime DEFAULT NULL COMMENT '交易产生时间',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `depositType` varchar(1) DEFAULT '1' COMMENT '类型_D##1 保证金缴纳，2：保证金退费##',
  `prepayId` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `m_id` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='在线充值表';

-- ----------------------------
-- Records of member_account_deposit
-- ----------------------------
INSERT INTO `member_account_deposit` VALUES ('1', '54', '4', '', '17101421281110000000000002', '', '4200000013201710148086100435', '1', '1', '1', '微信支付', '2017-10-14 21:30:11', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('2', '55', '7', '', '17101421461410000000000006', '', '4200000001201710148098369040', '1', '1', '1', '微信支付', '2017-10-14 21:53:17', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('3', '56', '9', '', '17101517350410000000000006', '', '4200000011201710158246746652', '1', '1', '1', '微信支付', '2017-10-15 17:35:22', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('4', '56', '13', '', '17101518121010000000000007', '', '4200000004201710158256707158', '2', '1', '1', '微信支付', '2017-10-15 18:12:34', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('5', '54', '14', '', '17101518181010000000000008', '', '4200000013201710158257070544', '2', '1', '1', '微信支付', '2017-10-15 18:18:49', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('7', '57', '33', '', '17101519522810000000000013', '', '', '0', '0', '0', '', null, '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('8', '56', '25', '', '17101519543010000000000014', '', '4200000023201710158280542854', '2', '1', '1', '微信支付', '2017-10-15 19:59:56', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('9', '54', '38', '', '17101520154610000000000016', '', '4200000012201710158279563245', '2', '1', '1', '微信支付', '2017-10-15 20:16:22', '', '', '', '1', null);
INSERT INTO `member_account_deposit` VALUES ('10', '55', '58', '', '17102017224810000000000018', '', '4200000007201710209230815385', '2', '1', '1', '微信支付', '2017-10-20 17:25:19', '', '', '', '1', 'wx20171020172519fe1750233b0114474433');
INSERT INTO `member_account_deposit` VALUES ('11', '54', '64', '', '17102210201710000000000019', '', '4200000020201710229570720937', '2', '1', '1', '微信支付', '2017-10-22 10:25:06', '', '', '', '1', 'wx20171022102506606006db3d0370076154');

-- ----------------------------
-- Table structure for member_account_deposit_return
-- ----------------------------
DROP TABLE IF EXISTS `member_account_deposit_return`;
CREATE TABLE `member_account_deposit_return` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `productId` bigint(20) DEFAULT NULL COMMENT '商品',
  `specificationValue` varchar(255) DEFAULT NULL,
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '保证金缴纳单号',
  `paymentNo` varchar(60) DEFAULT '' COMMENT '第三方支付单号',
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `amount` bigint(20) DEFAULT '0' COMMENT '金额',
  `payStatus` varchar(1) DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `paymentType` varchar(1) NOT NULL DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) NOT NULL DEFAULT '' COMMENT '充值方式名称',
  `transactionTime` datetime NOT NULL COMMENT '交易产生时间',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `depositType` varchar(1) DEFAULT '1' COMMENT '类型_D##1 保证金缴纳，2：保证金退费##',
  PRIMARY KEY (`id`),
  KEY `m_id` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线充值表';

-- ----------------------------
-- Records of member_account_deposit_return
-- ----------------------------

-- ----------------------------
-- Table structure for member_account_intro
-- ----------------------------
DROP TABLE IF EXISTS `member_account_intro`;
CREATE TABLE `member_account_intro` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` bigint(20) DEFAULT NULL COMMENT '会员',
  `fromIntroMemId` bigint(20) DEFAULT NULL COMMENT '收益来源会员',
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '推广收益单号',
  `paymentNo` varchar(60) DEFAULT '' COMMENT '第三方支付单号',
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `amount` bigint(20) DEFAULT '0' COMMENT '收益金额',
  `payStatus` varchar(1) DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `paymentType` varchar(1) NOT NULL DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) NOT NULL DEFAULT '' COMMENT '充值方式名称',
  `transactionTime` datetime NOT NULL COMMENT '提交时间',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_account_intro
-- ----------------------------

-- ----------------------------
-- Table structure for member_account_log
-- ----------------------------
DROP TABLE IF EXISTS `member_account_log`;
CREATE TABLE `member_account_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` bigint(20) NOT NULL COMMENT '会员',
  `changeType` varchar(1) DEFAULT NULL COMMENT '交易类型_D#1为充值,2,为提现,3,缴纳商品保证金，4，商品保证金退回，5,会员推广收益，6为管理员调节,7,商品出售，8，购买商品， 99为其它类型#',
  `amount` bigint(20) DEFAULT '0' COMMENT '收益金额',
  `transactionTime` datetime DEFAULT NULL COMMENT '交易产生时间',
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '业务单号',
  `payStatus` varchar(1) DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_account_log
-- ----------------------------
INSERT INTO `member_account_log` VALUES ('1', '54', '9', '1', '2017-10-14 21:26:43', '17101421251310000000000001', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('2', '54', '3', '1', '2017-10-14 21:30:19', '17101421281110000000000002', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('3', '55', '9', '1', '2017-10-14 21:34:40', '17101421343010000000000003', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('4', '55', '8', '3', '2017-10-14 21:36:14', '17101421360110000000000002', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('6', '54', '7', '3', '2017-10-14 21:44:04', '17101421360110000000000002', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('7', '54', '4', '1', '2017-10-14 21:44:04', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('8', '55', '3', '1', '2017-10-14 21:53:22', '17101421461410000000000006', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('9', '55', '4', '1', '2017-10-14 21:53:53', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('10', '56', '9', '1', '2017-10-15 17:32:18', '17101517320410000000000005', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('11', '56', '3', '1', '2017-10-15 17:35:28', '17101517350410000000000006', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('12', '54', '8', '1', '2017-10-15 17:43:04', '17101517424110000000000004', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('13', '56', '7', '1', '2017-10-15 17:46:23', '17101517424110000000000004', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('14', '56', '4', '1', '2017-10-15 17:46:23', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('15', '56', '3', '2', '2017-10-15 18:12:40', '17101518121010000000000007', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('16', '54', '8', '1', '2017-10-15 18:15:12', '17101518144310000000000006', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('17', '56', '7', '1', '2017-10-15 18:15:58', '17101518144310000000000006', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('18', '56', '4', '2', '2017-10-15 18:15:58', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('19', '54', '3', '2', '2017-10-15 18:18:57', '17101518181010000000000008', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('20', '56', '8', '3', '2017-10-15 18:21:16', '17101518201410000000000008', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('21', '54', '7', '3', '2017-10-15 18:23:58', '17101518201410000000000008', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('22', '54', '4', '2', '2017-10-15 18:23:58', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('23', '57', '9', '1', '2017-10-15 19:38:03', '17101519373210000000000012', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('24', '56', '3', '2', '2017-10-15 20:00:01', '17101519543010000000000014', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('25', '55', '8', '1', '2017-10-15 20:00:47', '17101520001810000000000010', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('26', '56', '7', '1', '2017-10-15 20:01:29', '17101520001810000000000010', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('27', '56', '4', '2', '2017-10-15 20:01:29', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('28', '54', '3', '2', '2017-10-15 20:16:33', '17101520154610000000000016', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('29', '56', '8', '3', '2017-10-15 20:17:16', '17101520170510000000000012', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('30', '54', '7', '3', '2017-10-15 20:18:09', '17101520170510000000000012', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('31', '54', '4', '2', '2017-10-15 20:18:09', '', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('32', '55', '3', '2', '2017-10-20 17:25:25', '17102017224810000000000018', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('33', '54', '3', '2', '2017-10-22 10:25:15', '17102210201710000000000019', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('34', '55', '8', '2', '2017-10-22 10:57:41', '1710221026009000000000000014', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('35', '54', '7', '1', '2017-10-22 11:01:04', '1710221026009000000000000014', '1', '', '', '');
INSERT INTO `member_account_log` VALUES ('36', '54', '4', '1', '2017-10-22 11:01:04', '', '1', '', '', '');

-- ----------------------------
-- Table structure for member_account_membership
-- ----------------------------
DROP TABLE IF EXISTS `member_account_membership`;
CREATE TABLE `member_account_membership` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) unsigned DEFAULT NULL COMMENT '用户id',
  `transactionNo` varchar(60) DEFAULT '' COMMENT '充值单号',
  `paymentNo` varchar(60) DEFAULT '' COMMENT '第三方支付单号',
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `amount` bigint(20) DEFAULT '0' COMMENT '金额',
  `payStatus` varchar(1) DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `paymentType` varchar(1) DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) DEFAULT '' COMMENT '充值方式名称',
  `transactionTime` datetime DEFAULT NULL COMMENT '提交时间时间',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `prepayId` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `m_id` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='在线充值表';

-- ----------------------------
-- Records of member_account_membership
-- ----------------------------
INSERT INTO `member_account_membership` VALUES ('1', '54', '17101421251310000000000001', '', '4200000010201710148085433656', '1', '1', '1', '微信支付', '2017-10-14 21:26:36', '', '', '', '');
INSERT INTO `member_account_membership` VALUES ('2', '55', '17101421343010000000000003', '', '4200000016201710148091536149', '1', '1', '1', '微信支付', '2017-10-14 21:34:35', '', '', '', '');
INSERT INTO `member_account_membership` VALUES ('3', '56', '17101517320410000000000005', '', '4200000013201710158246597239', '1', '1', '1', '微信支付', '2017-10-15 17:32:12', '', '', '', '');
INSERT INTO `member_account_membership` VALUES ('4', '57', '17101519373210000000000012', '', '4200000019201710158269664879', '1', '1', '1', '微信支付', '2017-10-15 19:37:52', '', '', '', '');

-- ----------------------------
-- Table structure for member_account_recharge
-- ----------------------------
DROP TABLE IF EXISTS `member_account_recharge`;
CREATE TABLE `member_account_recharge` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '充值单号',
  `paymentNo` varchar(60) DEFAULT '' COMMENT '第三方支付单号',
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `amount` bigint(20) NOT NULL DEFAULT '0' COMMENT '充值金额',
  `payStatus` varchar(1) NOT NULL DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `paymentType` varchar(1) NOT NULL DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) NOT NULL DEFAULT '' COMMENT '充值方式名称',
  `transactionTime` datetime NOT NULL COMMENT '提交时间时间',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `m_id` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线充值表';

-- ----------------------------
-- Records of member_account_recharge
-- ----------------------------

-- ----------------------------
-- Table structure for member_account_withdraw_cash
-- ----------------------------
DROP TABLE IF EXISTS `member_account_withdraw_cash`;
CREATE TABLE `member_account_withdraw_cash` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '提现交易单号',
  `paymentNo` varchar(60) DEFAULT '' COMMENT '第三方支付单号',
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `amount` bigint(20) DEFAULT '0' COMMENT '收益金额',
  `payStatus` varchar(1) DEFAULT '0' COMMENT '充值状态 0:未成功 1:充值成功',
  `paymentType` varchar(1) NOT NULL DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) NOT NULL DEFAULT '' COMMENT '充值方式名称',
  `transactionTime` datetime NOT NULL COMMENT '交易产生时间',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(500) DEFAULT '' COMMENT '交易失败原因',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `m_id` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线充值表';

-- ----------------------------
-- Records of member_account_withdraw_cash
-- ----------------------------

-- ----------------------------
-- Table structure for member_images
-- ----------------------------
DROP TABLE IF EXISTS `member_images`;
CREATE TABLE `member_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) NOT NULL COMMENT '会员ID',
  `imgPath` varchar(255) DEFAULT '' COMMENT '图片路径',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '修改时间',
  `status` varchar(1) DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  `isDefault` varchar(1) DEFAULT NULL COMMENT '是否默认_D#0不是 1是#',
  PRIMARY KEY (`id`),
  KEY `seller_image_id_ref` (`memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订购应用的用户上传的图片';

-- ----------------------------
-- Records of member_images
-- ----------------------------

-- ----------------------------
-- Table structure for member_introduct
-- ----------------------------
DROP TABLE IF EXISTS `member_introduct`;
CREATE TABLE `member_introduct` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) DEFAULT NULL COMMENT ' 会员ID',
  `introductorId` bigint(6) DEFAULT NULL COMMENT '推荐人ID',
  `extentionMap` varchar(300) DEFAULT NULL COMMENT '推荐关系',
  `introductTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '推荐时间',
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `introductorId` (`introductorId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_introduct
-- ----------------------------

-- ----------------------------
-- Table structure for member_receiver
-- ----------------------------
DROP TABLE IF EXISTS `member_receiver`;
CREATE TABLE `member_receiver` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `buyerId` bigint(20) NOT NULL COMMENT '会员',
  `receiverName` varchar(60) NOT NULL COMMENT '收货人',
  `phone` varchar(20) NOT NULL COMMENT '电话',
  `province` bigint(20) DEFAULT NULL COMMENT '省',
  `city` bigint(20) DEFAULT NULL COMMENT '市',
  `area` bigint(20) DEFAULT NULL COMMENT '县',
  `address` varchar(300) DEFAULT '' COMMENT '收货地址',
  `zipCode` varchar(10) DEFAULT '' COMMENT '邮编',
  `isDefault` varchar(1) NOT NULL DEFAULT '0' COMMENT '是否默认_D#0否 1是#',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `FK22D1EC4E7C62EDF8` (`buyerId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='收货地址';

-- ----------------------------
-- Records of member_receiver
-- ----------------------------
INSERT INTO `member_receiver` VALUES ('1', '55', '瞿浩', '1895136382', '497', '498', '499', '济南大学', '636363', '0', null, null, '1');
INSERT INTO `member_receiver` VALUES ('2', '56', '', '', '1', '2', '3', '', '', '1', null, null, '1');
INSERT INTO `member_receiver` VALUES ('3', '54', '干活', '13793172639', '1', '2', '3', '好纠结', '', '1', null, null, '1');
INSERT INTO `member_receiver` VALUES ('4', '55', '2', '3', '1', '2', '3', '', '', '0', null, null, '1');

-- ----------------------------
-- Table structure for member_regist
-- ----------------------------
DROP TABLE IF EXISTS `member_regist`;
CREATE TABLE `member_regist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(30) NOT NULL DEFAULT '' COMMENT '电话',
  `checkCode` varchar(6) NOT NULL DEFAULT '' COMMENT '短信验证码',
  `ipLong` bigint(20) DEFAULT NULL COMMENT 'ip',
  `ip` varchar(36) DEFAULT '' COMMENT 'ip',
  `createAt` datetime NOT NULL COMMENT '验证开始时间',
  `expireAt` datetime NOT NULL COMMENT '验证过期时间',
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_regist
-- ----------------------------
INSERT INTO `member_regist` VALUES ('1', '18953136382', '470552', '1893728950', '112.224.2.182', '2017-10-14 18:29:35', '2017-10-14 18:31:06');
INSERT INTO `member_regist` VALUES ('2', '13793172269', '826704', '3747809669', '223.99.9.133', '2017-10-14 18:49:46', '2017-10-14 18:51:17');
INSERT INTO `member_regist` VALUES ('3', '13793172269', '846930', '3747809669', '223.99.9.133', '2017-10-14 21:24:27', '2017-10-14 21:25:58');
INSERT INTO `member_regist` VALUES ('4', '18953136382', '968081', '3747809669', '223.99.9.133', '2017-10-14 21:33:58', '2017-10-14 21:35:29');
INSERT INTO `member_regist` VALUES ('5', '13954190863', '279520', '3747809669', '223.99.9.133', '2017-10-15 17:30:40', '2017-10-15 17:32:11');
INSERT INTO `member_regist` VALUES ('6', '15588873006', '781837', '1893732832', '112.224.17.224', '2017-10-15 19:35:42', '2017-10-15 19:37:13');

-- ----------------------------
-- Table structure for member_weiuser
-- ----------------------------
DROP TABLE IF EXISTS `member_weiuser`;
CREATE TABLE `member_weiuser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) NOT NULL COMMENT '注册用户',
  `app_id` varchar(255) NOT NULL COMMENT '微信公众号id',
  `authorizer_access_token` varchar(255) DEFAULT NULL COMMENT '授权token',
  `authorizer_refresh_token` varchar(255) DEFAULT NULL COMMENT '刷新token',
  `expires_in` int(11) DEFAULT NULL COMMENT 'token过期时长',
  `select_func_info` text COMMENT '授权时候用户选择的权限',
  `nick_name` varchar(255) DEFAULT NULL,
  `head_img` varchar(255) DEFAULT NULL,
  `service_type_info` varchar(255) DEFAULT NULL,
  `verify_type_info` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `principal_name` varchar(255) DEFAULT NULL,
  `business_info` varchar(255) DEFAULT NULL COMMENT ' 用以了解以下功能的开通状况（0代表未开通，1代表已开通）',
  `alias` varchar(255) DEFAULT NULL COMMENT '授权方公众号所设置的微信号，可能为空',
  `qrcode_url` varchar(255) DEFAULT NULL,
  `func_info` text COMMENT '公众号拥有的全部接口权限',
  `pay_mch_id` varchar(255) DEFAULT NULL,
  `pay_secret_key` varchar(255) DEFAULT NULL,
  `is_used` bit(1) DEFAULT NULL COMMENT '是否当前使用的公众号',
  `active` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_seller_id_ref` (`memberId`),
  CONSTRAINT `member_weiuser_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `t_seller_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授权公众号详细信息';

-- ----------------------------
-- Records of member_weiuser
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL COMMENT '主键_id',
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `buyerId` bigint(20) NOT NULL COMMENT '买家',
  `receiverId` bigint(20) DEFAULT NULL COMMENT '收货地址',
  `ordersn` varchar(100) NOT NULL DEFAULT '' COMMENT '订单编号',
  `orderStatus` varchar(2) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '订单状态 _D# 1等待付款，2已付款,4已发货，5确认收货，8已评价9交易完成，10交易关闭#',
  `orderTime` datetime DEFAULT NULL COMMENT '下单时间',
  `paymentStatus` varchar(1) NOT NULL COMMENT '支付状态_D#1 未支付；2部分支付；3已支付；4部分退款；5已退款# ',
  `paymentTime` datetime DEFAULT NULL COMMENT '支付时间',
  `deliveryStatus` varchar(1) NOT NULL COMMENT '配送状态 1未发货；2部分发货；3已发货；4部分退货；5已退货',
  `deliveryTime` datetime DEFAULT NULL COMMENT '发货时间',
  `promotionPirce` bigint(20) DEFAULT NULL COMMENT '优惠金额_D#暂时不用#',
  `discountPrice` bigint(20) DEFAULT NULL COMMENT '改价金额_D#暂时不用#',
  `totalPrice` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后价格',
  `postFee` bigint(20) DEFAULT NULL COMMENT '运费',
  `payFee` bigint(20) DEFAULT NULL COMMENT '支付费用',
  `point` int(11) DEFAULT NULL COMMENT '赠送积分',
  `memo` varchar(100) DEFAULT '' COMMENT '备注',
  `tradeNo` varchar(60) DEFAULT NULL,
  `transactionId` varchar(64) DEFAULT '' COMMENT '微信等支付交易号',
  `invoiceTitle` varchar(100) DEFAULT '' COMMENT '发票抬头',
  `isInvoice` varchar(1) DEFAULT '0' COMMENT '是否需要发票_D#1开发票，0 不开发票#',
  `isDeleted` varchar(1) DEFAULT '1' COMMENT '是否删除_D#0删除，1正常#',
  `trackingNum` varchar(30) DEFAULT '' COMMENT '物流单号',
  `expressCompanyId` bigint(20) DEFAULT NULL COMMENT '物流公司',
  `paymentType` varchar(1) DEFAULT '0' COMMENT '支付方式',
  `paymentTypeName` varchar(80) DEFAULT '' COMMENT '充值方式名称',
  `prepayId` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '54', '55', '1', '17101421360110000000000002', '5', '2017-10-14 21:36:02', '1', null, '1', null, null, null, '1', '2', '3', null, '', 'FMwMHPom3UtiUnqyJsR9pw', '4200000014201710148089838938', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('3', '56', '54', '3', '17101517424110000000000004', '5', '2017-10-15 17:42:42', '1', null, '1', null, null, null, '1', '0', '1', null, '', 'KPRJqLxUGrybcr8LWtqMVd', '4200000009201710158249258769', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('5', '56', '54', '3', '17101518144310000000000006', '5', '2017-10-15 18:14:44', '1', null, '1', null, null, null, '1', '0', '1', null, '', 'YMFUVLu4o9jDKHwbJeiPvg', '4200000014201710158258459223', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('7', '54', '56', '2', '17101518201410000000000008', '8', '2017-10-15 18:20:15', '1', null, '1', null, null, null, '2', '1', '3', null, '', 'BzDZzZoePvcUch8jUPjZqi', '4200000009201710158257179516', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('9', '56', '55', '1', '17101520001810000000000010', '8', '2017-10-15 20:00:18', '1', null, '1', null, null, null, '1', '0', '1', null, '', '5MZEgP9tB7AaXWMQ3KZQhA', '4200000015201710158278784603', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('11', '54', '56', '2', '17101520170510000000000012', '8', '2017-10-15 20:17:06', '1', null, '1', null, null, null, '1', '2', '3', null, '', 'WGspTGNUbFHztorWUrgquW', '4200000022201710158279618203', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('13', '55', '54', null, '1710202038008000000000000013', '1', '2017-10-20 20:38:09', '1', null, '1', null, null, null, '4400', '0', '4400', null, '', '3TtC9pYfT3Sq9qg7C6RqVR', '', '', '0', '1', '', null, '0', '', '');
INSERT INTO `orders` VALUES ('14', '54', '55', null, '1710221026009000000000000014', '5', '2017-10-22 10:26:09', '1', '2017-10-22 10:57:33', '1', null, null, null, '2', '0', '2', null, '', '2uZw7JXUWY3FnouMM6ULzn', '4200000025201710229573718502', '', '0', '1', '', null, '1', '微信支付', 'wx20171022105733976742c8ca0242296566');

-- ----------------------------
-- Table structure for orders_item
-- ----------------------------
DROP TABLE IF EXISTS `orders_item`;
CREATE TABLE `orders_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键_id',
  `orderId` bigint(20) NOT NULL COMMENT '订单',
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `productGoodsId` bigint(20) NOT NULL COMMENT '商品',
  `specificationValue` varchar(100) DEFAULT NULL,
  `name` varchar(255) NOT NULL COMMENT '商品名称',
  `price` bigint(20) NOT NULL COMMENT '商品价格',
  `productImg` varchar(255) NOT NULL COMMENT '商品缩略图',
  `amount` int(11) NOT NULL COMMENT '数量',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#_Q@=@',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='订单项';

-- ----------------------------
-- Records of orders_item
-- ----------------------------
INSERT INTO `orders_item` VALUES ('1', '1', '4', '1', '0', '蒙娜丽莎', '1', 'img/product/2017/10/14/WM32smBANu8hzZw7L6HXxo.png', '1', '2017-10-14 21:36:01', '2017-10-14 21:36:01', '1');
INSERT INTO `orders_item` VALUES ('2', '3', '9', '3', '0', '中', '1', 'img/product/2017/10/15/2ssqRV34mfACVEmTK1oJKa.png', '1', '2017-10-15 17:42:41', '2017-10-15 17:42:41', '1');
INSERT INTO `orders_item` VALUES ('3', '5', '13', '4', '0', '中', '1', 'img/product/2017/10/15/PsPy1Ah1csRPMeyEiTUx1s.png', '1', '2017-10-15 18:14:43', '2017-10-15 18:14:43', '1');
INSERT INTO `orders_item` VALUES ('4', '7', '14', '5', '0', '环境', '2', 'img/product/2017/10/15/CpiSy5JoeMpZuDmfqzpKKb.jpg', '1', '2017-10-15 18:20:14', '2017-10-15 18:20:14', '1');
INSERT INTO `orders_item` VALUES ('5', '9', '25', '6', '0', '中', '1', 'img/product/2017/10/15/2Rbm1yxcWqeyD8L74ApucF.png', '1', '2017-10-15 20:00:18', '2017-10-15 20:00:18', '1');
INSERT INTO `orders_item` VALUES ('6', '11', '38', '11', '0', '与', '1', 'img/product/2017/10/15/NEv4THpryzs7n5G6EUjbmt.jpg', '1', '2017-10-15 20:17:05', '2017-10-15 20:17:05', '1');
INSERT INTO `orders_item` VALUES ('7', '13', '58', '12', '0', '444', '4400', 'img/product/2017/10/20/EcFQxA1Ge6xAZ9wWF8ahRC.jpg', '1', '2017-10-20 20:38:08', '2017-10-20 20:38:08', '1');
INSERT INTO `orders_item` VALUES ('8', '14', '64', '14', '0', 'ii', '2', 'img/product/2017/10/22/FHCV6kZHUjhnHXTHH5VtFF.jpg', '1', '2017-10-22 10:26:09', '2017-10-22 10:26:09', '1');

-- ----------------------------
-- Table structure for orders_pay_unify
-- ----------------------------
DROP TABLE IF EXISTS `orders_pay_unify`;
CREATE TABLE `orders_pay_unify` (
  `id` bigint(20) NOT NULL COMMENT '主键_id',
  `orderId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `prepayId` varchar(1) NOT NULL COMMENT '配送状态 1未发货；2部分发货；3已发货；4部分退货；5已退货',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除,3结束#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of orders_pay_unify
-- ----------------------------

-- ----------------------------
-- Table structure for orders_refund
-- ----------------------------
DROP TABLE IF EXISTS `orders_refund`;
CREATE TABLE `orders_refund` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) NOT NULL COMMENT '会员id',
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `orderId` bigint(20) NOT NULL COMMENT '订单',
  `refundMoney` bigint(20) NOT NULL DEFAULT '0' COMMENT '退款金额',
  `ordersn` varchar(60) NOT NULL DEFAULT '' COMMENT '订单编号',
  `transactionId` varchar(60) DEFAULT NULL COMMENT '交易号',
  `disposeTime` datetime DEFAULT NULL COMMENT '处理时间',
  `disposeUser` varchar(50) DEFAULT '' COMMENT '处理人员',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `redundStatus` varchar(1) NOT NULL DEFAULT '0' COMMENT '退款状态，0:申请退款 1:退款失败 2:退款成功',
  `goodsId` bigint(20) NOT NULL COMMENT '商品',
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`),
  KEY `productId` (`productId`),
  KEY `orderId` (`orderId`),
  KEY `ordersn` (`ordersn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款';

-- ----------------------------
-- Records of orders_refund
-- ----------------------------

-- ----------------------------
-- Table structure for orders_status_log
-- ----------------------------
DROP TABLE IF EXISTS `orders_status_log`;
CREATE TABLE `orders_status_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键_id',
  `orderId` bigint(20) DEFAULT NULL COMMENT '订单ID',
  `operator` bigint(20) NOT NULL COMMENT '操作人',
  `orderStatus` varchar(2) CHARACTER SET utf8mb4 NOT NULL COMMENT '订单状态 _D# 1未确认，2已确认,3已完成,4已取消#',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='订单';

-- ----------------------------
-- Records of orders_status_log
-- ----------------------------
INSERT INTO `orders_status_log` VALUES ('2', '1', '55', '5', '2017-10-14 21:44:04', '2017-10-14 21:44:04', '1', '');
INSERT INTO `orders_status_log` VALUES ('3', '3', '54', '5', '2017-10-15 17:46:24', '2017-10-15 17:46:24', '1', '');
INSERT INTO `orders_status_log` VALUES ('4', '5', '54', '5', '2017-10-15 18:15:58', '2017-10-15 18:15:58', '1', '');
INSERT INTO `orders_status_log` VALUES ('5', '7', '56', '5', '2017-10-15 18:23:59', '2017-10-15 18:23:59', '1', '');
INSERT INTO `orders_status_log` VALUES ('6', '9', '55', '5', '2017-10-15 20:01:29', '2017-10-15 20:01:29', '1', '');
INSERT INTO `orders_status_log` VALUES ('7', '11', '56', '5', '2017-10-15 20:18:09', '2017-10-15 20:18:09', '1', '');
INSERT INTO `orders_status_log` VALUES ('8', '14', '55', '5', '2017-10-22 11:01:04', '2017-10-22 11:01:04', '1', '');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `shopId` bigint(20) DEFAULT NULL,
  `brandId` bigint(20) DEFAULT NULL COMMENT '品牌',
  `sellerId` bigint(20) NOT NULL COMMENT '卖家',
  `productsn` varchar(40) NOT NULL COMMENT '货品编号_U',
  `productName` varchar(255) NOT NULL DEFAULT '' COMMENT '货品名',
  `sellPrice` varchar(60) NOT NULL COMMENT '销售价',
  `minSellPrice` bigint(20) DEFAULT NULL COMMENT '最小销售价',
  `maxSellPrice` bigint(20) DEFAULT NULL COMMENT '最大销售价',
  `marketPrice` bigint(20) DEFAULT NULL COMMENT '市场价',
  `stock` int(11) DEFAULT NULL COMMENT '总库存',
  `upTime` datetime DEFAULT NULL COMMENT '上架时间',
  `dowTime` datetime DEFAULT NULL COMMENT '下架时间',
  `favoriteCount` int(11) NOT NULL DEFAULT '0' COMMENT '收藏数量',
  `visitCount` int(11) NOT NULL DEFAULT '0' COMMENT '浏览数量',
  `commentCount` int(11) NOT NULL DEFAULT '0' COMMENT '评价数量',
  `consultationCount` int(11) DEFAULT NULL COMMENT '留言数量',
  `saleCount` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `productStatus` varchar(1) NOT NULL DEFAULT '1' COMMENT '货品状态_D# 1正常 2已删除 3下架 4申请上架 5拒绝上架,6审核通过(等待交保证金)#\r\n',
  `isUnSpec` varchar(1) NOT NULL DEFAULT '1' COMMENT '是否统一规格_D# 1统一规格 0不统一规格#',
  `deliveryType` varchar(1) DEFAULT '1' COMMENT '是否统一邮费_D# 1 统一，2 按模板#,3面交',
  `deliveryFee` bigint(20) DEFAULT NULL COMMENT '是否统一邮费_邮费',
  `deliveryTemplateId` bigint(20) DEFAULT NULL COMMENT '邮费模板id',
  `deliveryWeight` bigint(20) DEFAULT NULL COMMENT '统一规格物流重量',
  `mainImage` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `isNew` varchar(1) DEFAULT '0' COMMENT '是否新品_D#1新品，0：非新品#',
  `isHot` varchar(1) DEFAULT '0' COMMENT '是否热销_D#1新品，0：非新品#',
  `isFreeDelivery` varchar(1) DEFAULT '1' COMMENT '是否包邮_D#1,包邮，0 不包邮#',
  `author` varchar(60) DEFAULT '' COMMENT '作品作者',
  `age` varchar(20) DEFAULT '' COMMENT '作品年代',
  `sizex` varchar(30) DEFAULT NULL COMMENT '作品尺寸',
  `depositId` bigint(20) DEFAULT NULL COMMENT '待缴纳保证金记录id',
  PRIMARY KEY (`id`),
  KEY `prod_seller_ref` (`sellerId`),
  KEY `productsn` (`productsn`),
  FULLTEXT KEY `productName` (`productName`) /*!50100 WITH PARSER `ngram` */ 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('4', '54', null, '54', 'U991mqJUTsJjyfj6X4W9HA', '蒙娜丽莎', '1', '0', '0', '0', '1', null, null, '0', '14', '0', null, '0', '2017-10-14 21:28:01', '2017-10-14 21:28:01', '7', '1', '1', '2', null, null, 'img/product/2017/10/14/WM32smBANu8hzZw7L6HXxo.png', '0', '1', '0', '侏罗纪', '68', null, '1');
INSERT INTO `product` VALUES ('7', '55', null, '55', 'SULCFtVDQNFRrGnH2cWyhm', '九龙壁', '100000', '0', '0', '0', '1', null, null, '0', '2', '0', null, '0', '2017-10-14 21:45:53', '2017-10-14 21:45:53', '3', '1', '3', '0', null, null, 'img/product/2017/10/14/RBbhRB3Q5SytxnidiJeM98.png', '0', '0', '0', '浩', '1233', null, '2');
INSERT INTO `product` VALUES ('9', '56', null, '56', 'Buj86BsKBmxzPjTn1ojR3r', '中', '1', '0', '0', '0', '1', null, null, '0', '14', '0', null, '0', '2017-10-15 17:34:17', '2017-10-15 17:34:17', '7', '1', '1', '0', null, null, 'img/product/2017/10/15/2ssqRV34mfACVEmTK1oJKa.png', '0', '1', '0', '口丫', '', null, '3');
INSERT INTO `product` VALUES ('13', '56', null, '56', 'MXP239xBsvB85QrYTHusEE', '中', '1', '0', '0', '0', '1', null, null, '0', '3', '0', null, '0', '2017-10-15 18:09:04', '2017-10-15 18:09:04', '7', '1', '1', '0', null, null, 'img/product/2017/10/15/PsPy1Ah1csRPMeyEiTUx1s.png', '0', '1', '0', '中', '中', null, '4');
INSERT INTO `product` VALUES ('14', '54', null, '54', 'N6duxdRY4SgGKjsqqXrjVq', '环境', '2', '0', '0', '0', '1', null, null, '0', '3', '0', null, '0', '2017-10-15 18:17:40', '2017-10-15 18:17:40', '7', '1', '1', '1', null, null, 'img/product/2017/10/15/CpiSy5JoeMpZuDmfqzpKKb.jpg', '0', '0', '0', '朱连江', '68', null, '5');
INSERT INTO `product` VALUES ('25', '56', null, '56', 'JWGTP2j7TJwX8CjzvMGMEU', '中', '1', '0', '0', '0', '1', null, null, '0', '1', '0', null, '0', '2017-10-15 18:44:49', '2017-10-15 18:44:49', '7', '1', '1', '0', null, null, 'img/product/2017/10/15/2Rbm1yxcWqeyD8L74ApucF.png', '0', '0', '0', '中', '', null, '8');
INSERT INTO `product` VALUES ('26', '56', null, '56', 'GZx4pHg6wcyEfxX8VxG2pZ', '冲', '1', '0', '0', '0', '1', null, null, '0', '4', '0', null, '0', '2017-10-15 19:02:17', '2017-10-15 19:02:17', '5', '1', '1', '0', null, null, 'img/product/2017/10/15/Kn97n64nWYG85iYzF8X3nn.png', '0', '0', '0', '中', '', null, null);
INSERT INTO `product` VALUES ('28', '56', null, '56', '3wrLGfD7UPMvXHkRkdzZp7', '中', '1', '0', '0', '0', '1', null, null, '0', '5', '0', null, '0', '2017-10-15 19:04:57', '2017-10-15 19:04:57', '4', '1', '1', '0', null, null, 'img/product/2017/10/15/XmxKEHoGzqapTV92ounPXs.png', '0', '0', '0', '中', '', null, null);
INSERT INTO `product` VALUES ('29', '56', null, '56', 'DHBKTLZnVGhQ6jiDECGKJV', '中', '1', '0', '0', '0', '1', null, null, '0', '4', '0', null, '0', '2017-10-15 19:06:42', '2017-10-15 19:06:42', '4', '1', '1', '0', null, null, '', '0', '0', '0', '中', '', null, null);
INSERT INTO `product` VALUES ('33', '57', null, '57', 'RHxCyQSFCDLDkPKtqFq87P', '丨', '100', '0', '0', '0', '1', null, null, '0', '13', '0', null, '0', '2017-10-15 19:39:05', '2017-10-15 19:39:05', '6', '1', '3', '0', null, null, 'img/product/2017/10/15/MxPnBMw89cRCnkfHfFz4H6.jpg', '0', '0', '0', '李', '', null, '7');
INSERT INTO `product` VALUES ('38', '54', null, '54', 'M6neT3SiqaFcnR4cMAwRV7', '与', '1', '0', '0', '0', '1', null, null, '0', '6', '0', null, '0', '2017-10-15 20:14:26', '2017-10-15 20:14:26', '7', '1', '1', '2', null, null, 'img/product/2017/10/15/NEv4THpryzs7n5G6EUjbmt.jpg', '0', '0', '0', '岂不是', '58', null, '9');
INSERT INTO `product` VALUES ('58', '55', null, '55', 'M7tCzFS17QuX2HNxqV97pr', '444', '200', '200', '200', '0', '1', '2017-10-20 17:43:14', null, '0', '7', '0', null, '0', '2017-10-20 16:05:24', '2017-10-20 16:05:24', '7', '1', '3', '0', null, null, 'img/product/2017/10/20/EcFQxA1Ge6xAZ9wWF8ahRC.jpg', '0', '0', '0', '444', '444', null, '10');
INSERT INTO `product` VALUES ('59', '55', null, '55', 'HGsDKeXSUUygvzsheRh321', '55', '500', '0', '0', '0', '1', null, null, '0', '1', '0', null, '0', '2017-10-20 16:16:43', '2017-10-20 16:16:43', '5', '1', '3', '0', null, null, 'img/product/2017/10/20/35wJY6WNw7KoJpADmf61Mo.jpg', '0', '0', '0', '55', '55', null, null);
INSERT INTO `product` VALUES ('64', '54', null, '54', 'P1YY44JTD2Gae6eApfqcA5', 'ii', '2', '0', '0', '0', '1', null, null, '0', '4', '0', null, '0', '2017-10-22 10:19:07', '2017-10-22 10:19:07', '7', '1', '3', '0', null, null, 'img/product/2017/10/22/FHCV6kZHUjhnHXTHH5VtFF.jpg', '0', '0', '0', 'uu', '', null, '11');

-- ----------------------------
-- Table structure for product_adv
-- ----------------------------
DROP TABLE IF EXISTS `product_adv`;
CREATE TABLE `product_adv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `showIndex` int(11) NOT NULL COMMENT '显示顺序',
  `isShow` varchar(1) DEFAULT '0' COMMENT '是否显示',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prod_seller_ref` (`productId`),
  KEY `prod_categ_id_ref` (`showIndex`),
  KEY `productAttrId` (`isShow`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_adv
-- ----------------------------
INSERT INTO `product_adv` VALUES ('1', '38', '0', '1', '2017-10-19 10:39:29', null, '2', 'img/productAdv/2017/10/19/NBGRLhtiShuVfHYNHAJFSu.png');
INSERT INTO `product_adv` VALUES ('2', '33', '0', '1', '2017-10-19 10:41:41', null, '1', 'img/productAdv/2017/10/19/PFV1ViZ9sF1cUz3PqxYvtQ.png');

-- ----------------------------
-- Table structure for product_attr
-- ----------------------------
DROP TABLE IF EXISTS `product_attr`;
CREATE TABLE `product_attr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `productCategoryId` bigint(20) NOT NULL COMMENT '商品分类',
  `productAttrId` bigint(20) DEFAULT NULL,
  `productAttrValueId` bigint(20) DEFAULT NULL COMMENT '属性',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `prod_seller_ref` (`productId`),
  KEY `prod_categ_id_ref` (`productCategoryId`),
  KEY `productAttrValueId` (`productAttrValueId`),
  KEY `productAttrId` (`productAttrId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_attr
-- ----------------------------
INSERT INTO `product_attr` VALUES ('1', '4', '1', null, null, '2017-10-14 21:28:01', '2017-10-14 21:28:01', '1');
INSERT INTO `product_attr` VALUES ('2', '7', '1', null, null, '2017-10-14 21:45:53', '2017-10-14 21:45:53', '1');
INSERT INTO `product_attr` VALUES ('3', '9', '1', null, null, '2017-10-15 17:34:17', '2017-10-15 17:34:17', '1');
INSERT INTO `product_attr` VALUES ('4', '13', '1', null, null, '2017-10-15 18:09:04', '2017-10-15 18:09:04', '1');
INSERT INTO `product_attr` VALUES ('5', '14', '6', null, null, '2017-10-15 18:17:40', '2017-10-15 18:17:40', '1');
INSERT INTO `product_attr` VALUES ('6', '25', '1', null, null, '2017-10-15 18:44:49', '2017-10-15 18:44:49', '1');
INSERT INTO `product_attr` VALUES ('7', '26', '1', null, null, '2017-10-15 19:02:17', '2017-10-15 19:02:17', '1');
INSERT INTO `product_attr` VALUES ('8', '28', '1', null, null, '2017-10-15 19:04:57', '2017-10-15 19:04:57', '1');
INSERT INTO `product_attr` VALUES ('9', '29', '1', null, null, '2017-10-15 19:06:42', '2017-10-15 19:06:42', '1');
INSERT INTO `product_attr` VALUES ('10', '33', '1', null, null, '2017-10-15 19:39:05', '2017-10-15 19:39:05', '1');
INSERT INTO `product_attr` VALUES ('11', '38', '4', null, null, '2017-10-15 20:14:26', '2017-10-15 20:14:26', '1');
INSERT INTO `product_attr` VALUES ('12', '58', '1', null, null, '2017-10-20 16:05:24', '2017-10-20 16:05:24', '1');
INSERT INTO `product_attr` VALUES ('13', '59', '1', null, null, '2017-10-20 16:16:43', '2017-10-20 16:16:43', '1');
INSERT INTO `product_attr` VALUES ('14', '64', '1', null, null, '2017-10-22 10:19:07', '2017-10-22 10:19:07', '1');

-- ----------------------------
-- Table structure for product_detail
-- ----------------------------
DROP TABLE IF EXISTS `product_detail`;
CREATE TABLE `product_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) DEFAULT NULL COMMENT '商品',
  `introduction` mediumtext COMMENT '商品详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_detail
-- ----------------------------
INSERT INTO `product_detail` VALUES ('1', '59', '55');
INSERT INTO `product_detail` VALUES ('2', '64', 'djhs');

-- ----------------------------
-- Table structure for product_goods
-- ----------------------------
DROP TABLE IF EXISTS `product_goods`;
CREATE TABLE `product_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键_id',
  `productId` bigint(20) NOT NULL,
  `specificationValue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL DEFAULT '' COMMENT '规格值',
  `sellPrice` bigint(20) DEFAULT NULL COMMENT '价格',
  `marketPrice` bigint(20) DEFAULT NULL COMMENT '市场价',
  `stock` int(11) DEFAULT '0' COMMENT '库存',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `goodsStatus` varchar(1) NOT NULL DEFAULT '1' COMMENT '货品状态_D# 1正常 2已删除 3下架 4申请上架 5拒绝上架#',
  `isUnSpec` varchar(1) NOT NULL DEFAULT '1' COMMENT '是否统一规格_D# 1统一规格 2不统一规格#',
  `weight` int(11) DEFAULT NULL COMMENT '重量',
  PRIMARY KEY (`id`),
  KEY `productId` (`productId`),
  KEY `specificationValue` (`specificationValue`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='商品不同规格对用的不同，价格，库存，物流重量的数据保存在这个表';

-- ----------------------------
-- Records of product_goods
-- ----------------------------
INSERT INTO `product_goods` VALUES ('1', '4', '0', '1', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('2', '7', '0', '100000', null, '1', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('3', '9', '0', '1', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('4', '13', '0', '1', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('5', '14', '0', '2', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('6', '25', '0', '1', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('7', '26', '0', '1', null, '1', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('8', '28', '0', '1', null, '1', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('9', '29', '0', '1', null, '1', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('10', '33', '0', '100', null, '1', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('11', '38', '0', '1', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('12', '58', '0', '4400', null, '0', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('13', '59', '0', '500', null, '1', null, null, '1', '1', null);
INSERT INTO `product_goods` VALUES ('14', '64', '0', '2', null, '0', null, null, '1', '1', null);

-- ----------------------------
-- Table structure for product_goods_stock
-- ----------------------------
DROP TABLE IF EXISTS `product_goods_stock`;
CREATE TABLE `product_goods_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `profuctGoodsId` bigint(20) DEFAULT NULL COMMENT '商品Id',
  `sellerId` bigint(20) NOT NULL COMMENT '卖家',
  `goodssn` varchar(40) NOT NULL COMMENT '商品编号',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `goodsStatus` varchar(1) DEFAULT '1' COMMENT '商品状态 1在售 2已售 ',
  PRIMARY KEY (`id`),
  KEY `prod_seller_ref` (`sellerId`),
  KEY `productsn` (`goodssn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_goods_stock
-- ----------------------------

-- ----------------------------
-- Table structure for product_image
-- ----------------------------
DROP TABLE IF EXISTS `product_image`;
CREATE TABLE `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) NOT NULL COMMENT '商品id',
  `source` varchar(255) DEFAULT '' COMMENT '原图片',
  `large` varchar(255) DEFAULT '' COMMENT '大图片',
  `medium` varchar(255) DEFAULT '' COMMENT '中图片',
  `thumbnail` varchar(255) DEFAULT '' COMMENT '缩略图',
  `orderNum` int(11) DEFAULT '0' COMMENT '排序',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `FK66470ABC79F8D99A` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='商品图片';

-- ----------------------------
-- Records of product_image
-- ----------------------------
INSERT INTO `product_image` VALUES ('1', '4', 'img/product/2017/10/14/WM32smBANu8hzZw7L6HXxo.png', '', '', '', '1', '2017-10-14 21:28:00', '2017-10-14 21:28:00', '1');
INSERT INTO `product_image` VALUES ('2', '7', 'img/product/2017/10/14/RBbhRB3Q5SytxnidiJeM98.png', '', '', '', '1', '2017-10-14 21:45:49', '2017-10-14 21:45:49', '1');
INSERT INTO `product_image` VALUES ('3', '9', 'img/product/2017/10/15/2ssqRV34mfACVEmTK1oJKa.png', '', '', '', '1', '2017-10-15 17:33:02', '2017-10-15 17:33:02', '1');
INSERT INTO `product_image` VALUES ('4', '13', 'img/product/2017/10/15/PsPy1Ah1csRPMeyEiTUx1s.png', '', '', '', '1', '2017-10-15 18:09:03', '2017-10-15 18:09:03', '1');
INSERT INTO `product_image` VALUES ('5', '14', 'img/product/2017/10/15/CpiSy5JoeMpZuDmfqzpKKb.jpg', '', '', '', '1', '2017-10-15 18:17:29', '2017-10-15 18:17:29', '1');
INSERT INTO `product_image` VALUES ('6', '25', 'img/product/2017/10/15/2Rbm1yxcWqeyD8L74ApucF.png', '', '', '', '1', '2017-10-15 18:44:47', '2017-10-15 18:44:47', '1');
INSERT INTO `product_image` VALUES ('7', '26', 'img/product/2017/10/15/Kn97n64nWYG85iYzF8X3nn.png', '', '', '', '1', '2017-10-15 19:01:49', '2017-10-15 19:01:49', '1');
INSERT INTO `product_image` VALUES ('8', '26', 'img/product/2017/10/15/Wys3YfxvNU7CVkCezUU3ks.png', '', '', '', '2', '2017-10-15 19:01:56', '2017-10-15 19:01:56', '1');
INSERT INTO `product_image` VALUES ('9', '28', 'img/product/2017/10/15/XmxKEHoGzqapTV92ounPXs.png', '', '', '', '1', '2017-10-15 19:04:49', '2017-10-15 19:04:49', '1');
INSERT INTO `product_image` VALUES ('10', '28', 'img/product/2017/10/15/uevrzqTh2YwXnYpufaBYM.png', '', '', '', '2', '2017-10-15 19:04:55', '2017-10-15 19:04:55', '1');
INSERT INTO `product_image` VALUES ('11', '29', 'img/product/2017/10/15/6qSKZKFg7uhF3gxqnaoobs.png', '', '', '', '1', '2017-10-15 19:06:42', '2017-10-15 19:06:42', '1');
INSERT INTO `product_image` VALUES ('12', '33', 'img/product/2017/10/15/MxPnBMw89cRCnkfHfFz4H6.jpg', '', '', '', '1', '2017-10-15 19:38:56', '2017-10-15 19:38:56', '1');
INSERT INTO `product_image` VALUES ('13', '38', 'img/product/2017/10/15/NEv4THpryzs7n5G6EUjbmt.jpg', '', '', '', '1', '2017-10-15 20:14:25', '2017-10-15 20:14:25', '1');
INSERT INTO `product_image` VALUES ('14', '48', 'img/product/2017/10/18/FSkC69L8L84CaUUhYoRpkq.png', '', '', '', '1', '2017-10-18 22:01:13', '2017-10-18 22:01:13', '1');
INSERT INTO `product_image` VALUES ('15', '49', 'img/product/2017/10/18/YS7PUnn8DghLV3pLfufiau.png', '', '', '', '1', '2017-10-18 22:05:27', '2017-10-18 22:05:27', '1');
INSERT INTO `product_image` VALUES ('16', '50', 'img/product/2017/10/18/UREHWksQVfq6QRpFxYxML.png', '', '', '', '1', '2017-10-18 22:05:44', '2017-10-18 22:05:44', '1');
INSERT INTO `product_image` VALUES ('17', '51', 'img/product/2017/10/18/DREBDS2CxnWonxazoT2Adu.png', '', '', '', '1', '2017-10-18 22:06:56', '2017-10-18 22:06:56', '1');
INSERT INTO `product_image` VALUES ('18', '55', 'img/product/2017/10/20/EyfzJrRkAmTcMVox6kVog2.jpg', '', '', '', '1', '2017-10-20 14:55:19', '2017-10-20 14:55:19', '1');
INSERT INTO `product_image` VALUES ('19', '58', 'img/product/2017/10/20/EcFQxA1Ge6xAZ9wWF8ahRC.jpg', '', '', '', '1', '2017-10-20 16:05:21', '2017-10-20 16:05:21', '1');
INSERT INTO `product_image` VALUES ('20', '59', 'img/product/2017/10/20/35wJY6WNw7KoJpADmf61Mo.jpg', '', '', '', '1', '2017-10-20 16:16:40', '2017-10-20 16:16:40', '1');
INSERT INTO `product_image` VALUES ('21', '62', 'img/product/2017/10/22/UoBh6Fe2ou5aghqt3uK9mw.jpg', '', '', '', '1', '2017-10-22 09:49:10', '2017-10-22 09:49:10', '1');
INSERT INTO `product_image` VALUES ('22', '64', 'img/product/2017/10/22/FHCV6kZHUjhnHXTHH5VtFF.jpg', '', '', '', '1', '2017-10-22 10:19:01', '2017-10-22 10:19:01', '1');
INSERT INTO `product_image` VALUES ('23', '67', 'img/product/2017/10/22/76CU6WA2bTdTLfZRLfum34.jpg', '', '', '', '1', '2017-10-22 11:09:11', '2017-10-22 11:09:11', '1');

-- ----------------------------
-- Table structure for product_specification
-- ----------------------------
DROP TABLE IF EXISTS `product_specification`;
CREATE TABLE `product_specification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `specificationId` bigint(20) NOT NULL COMMENT '规格',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_specification
-- ----------------------------
INSERT INTO `product_specification` VALUES ('1', '4', '0', '2017-10-14 21:28:01', '2017-10-14 21:28:01', '1');
INSERT INTO `product_specification` VALUES ('2', '7', '0', '2017-10-14 21:45:53', '2017-10-14 21:45:53', '1');
INSERT INTO `product_specification` VALUES ('3', '9', '0', '2017-10-15 17:34:17', '2017-10-15 17:34:17', '1');
INSERT INTO `product_specification` VALUES ('4', '13', '0', '2017-10-15 18:09:04', '2017-10-15 18:09:04', '1');
INSERT INTO `product_specification` VALUES ('5', '14', '0', '2017-10-15 18:17:40', '2017-10-15 18:17:40', '1');
INSERT INTO `product_specification` VALUES ('6', '25', '0', '2017-10-15 18:44:49', '2017-10-15 18:44:49', '1');
INSERT INTO `product_specification` VALUES ('7', '26', '0', '2017-10-15 19:02:17', '2017-10-15 19:02:17', '1');
INSERT INTO `product_specification` VALUES ('8', '28', '0', '2017-10-15 19:04:57', '2017-10-15 19:04:57', '1');
INSERT INTO `product_specification` VALUES ('9', '29', '0', '2017-10-15 19:06:42', '2017-10-15 19:06:42', '1');
INSERT INTO `product_specification` VALUES ('10', '33', '0', '2017-10-15 19:39:05', '2017-10-15 19:39:05', '1');
INSERT INTO `product_specification` VALUES ('11', '38', '0', '2017-10-15 20:14:26', '2017-10-15 20:14:26', '1');
INSERT INTO `product_specification` VALUES ('12', '58', '0', '2017-10-20 16:05:24', '2017-10-20 16:05:24', '1');
INSERT INTO `product_specification` VALUES ('13', '59', '0', '2017-10-20 16:16:43', '2017-10-20 16:16:43', '1');
INSERT INTO `product_specification` VALUES ('14', '64', '0', '2017-10-22 10:19:07', '2017-10-22 10:19:07', '1');

-- ----------------------------
-- Table structure for product_specification_value
-- ----------------------------
DROP TABLE IF EXISTS `product_specification_value`;
CREATE TABLE `product_specification_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `specificationId` bigint(20) DEFAULT NULL COMMENT '规格',
  `specificationValueId` bigint(20) NOT NULL COMMENT '规格值',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_specification_value
-- ----------------------------
INSERT INTO `product_specification_value` VALUES ('1', '4', '0', '0', '2017-10-14 21:28:01', '2017-10-14 21:28:01', '1');
INSERT INTO `product_specification_value` VALUES ('2', '7', '0', '0', '2017-10-14 21:45:53', '2017-10-14 21:45:53', '1');
INSERT INTO `product_specification_value` VALUES ('3', '9', '0', '0', '2017-10-15 17:34:17', '2017-10-15 17:34:17', '1');
INSERT INTO `product_specification_value` VALUES ('4', '13', '0', '0', '2017-10-15 18:09:04', '2017-10-15 18:09:04', '1');
INSERT INTO `product_specification_value` VALUES ('5', '14', '0', '0', '2017-10-15 18:17:40', '2017-10-15 18:17:40', '1');
INSERT INTO `product_specification_value` VALUES ('6', '25', '0', '0', '2017-10-15 18:44:49', '2017-10-15 18:44:49', '1');
INSERT INTO `product_specification_value` VALUES ('7', '26', '0', '0', '2017-10-15 19:02:17', '2017-10-15 19:02:17', '1');
INSERT INTO `product_specification_value` VALUES ('8', '28', '0', '0', '2017-10-15 19:04:57', '2017-10-15 19:04:57', '1');
INSERT INTO `product_specification_value` VALUES ('9', '29', '0', '0', '2017-10-15 19:06:42', '2017-10-15 19:06:42', '1');
INSERT INTO `product_specification_value` VALUES ('10', '33', '0', '0', '2017-10-15 19:39:05', '2017-10-15 19:39:05', '1');
INSERT INTO `product_specification_value` VALUES ('11', '38', '0', '0', '2017-10-15 20:14:26', '2017-10-15 20:14:26', '1');
INSERT INTO `product_specification_value` VALUES ('12', '58', '0', '0', '2017-10-20 16:05:24', '2017-10-20 16:05:24', '1');
INSERT INTO `product_specification_value` VALUES ('13', '59', '0', '0', '2017-10-20 16:16:43', '2017-10-20 16:16:43', '1');
INSERT INTO `product_specification_value` VALUES ('14', '64', '0', '0', '2017-10-22 10:19:07', '2017-10-22 10:19:07', '1');

-- ----------------------------
-- Table structure for product_undelivery_area
-- ----------------------------
DROP TABLE IF EXISTS `product_undelivery_area`;
CREATE TABLE `product_undelivery_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shopId` bigint(20) DEFAULT NULL,
  `brandId` bigint(20) DEFAULT NULL COMMENT '品牌',
  `sellerId` bigint(20) NOT NULL COMMENT '卖家',
  `areaId` varchar(255) DEFAULT NULL COMMENT '地区id',
  PRIMARY KEY (`id`),
  KEY `prod_seller_ref` (`sellerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_undelivery_area
-- ----------------------------

-- ----------------------------
-- Table structure for product_visit
-- ----------------------------
DROP TABLE IF EXISTS `product_visit`;
CREATE TABLE `product_visit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buyerId` bigint(20) DEFAULT NULL,
  `productId` bigint(20) DEFAULT NULL,
  `visitCount` int(11) NOT NULL DEFAULT '0' COMMENT '浏览数量',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_visit
-- ----------------------------
INSERT INTO `product_visit` VALUES ('1', '54', '4', '12', '2017-10-15 17:57:18', '2017-10-14 21:33:50');
INSERT INTO `product_visit` VALUES ('2', '55', '4', '2', '2017-10-14 21:35:57', '2017-10-14 21:35:23');
INSERT INTO `product_visit` VALUES ('3', '56', '9', '8', '2017-10-15 17:56:29', '2017-10-15 17:37:06');
INSERT INTO `product_visit` VALUES ('4', '54', '9', '6', '2017-10-15 17:57:25', '2017-10-15 17:41:02');
INSERT INTO `product_visit` VALUES ('5', '56', '13', '1', '2017-10-15 18:13:59', '2017-10-15 18:13:59');
INSERT INTO `product_visit` VALUES ('6', '54', '13', '2', '2017-10-15 18:14:31', '2017-10-15 18:14:05');
INSERT INTO `product_visit` VALUES ('7', '56', '14', '3', '2017-10-15 18:23:09', '2017-10-15 18:19:41');
INSERT INTO `product_visit` VALUES ('8', '56', '26', '4', '2017-10-15 19:03:40', '2017-10-15 19:02:28');
INSERT INTO `product_visit` VALUES ('9', '56', '28', '5', '2017-10-15 19:58:17', '2017-10-15 19:05:07');
INSERT INTO `product_visit` VALUES ('10', '57', '33', '1', '2017-10-15 19:39:27', '2017-10-15 19:39:27');
INSERT INTO `product_visit` VALUES ('11', '56', '29', '4', '2017-10-19 19:22:37', '2017-10-15 19:40:49');
INSERT INTO `product_visit` VALUES ('12', '55', '25', '1', '2017-10-15 20:00:14', '2017-10-15 20:00:14');
INSERT INTO `product_visit` VALUES ('13', '56', '38', '6', '2017-10-15 20:19:15', '2017-10-15 20:16:56');
INSERT INTO `product_visit` VALUES ('14', '55', '7', '2', '2017-10-20 16:28:18', '2017-10-18 21:39:38');
INSERT INTO `product_visit` VALUES ('15', '55', '33', '1', '2017-10-20 16:04:27', '2017-10-20 16:04:27');
INSERT INTO `product_visit` VALUES ('16', '55', '58', '4', '2017-10-20 18:02:43', '2017-10-20 16:40:35');
INSERT INTO `product_visit` VALUES ('17', '55', '59', '1', '2017-10-20 16:43:31', '2017-10-20 16:43:31');
INSERT INTO `product_visit` VALUES ('18', '56', '33', '11', '2017-10-20 22:22:13', '2017-10-20 17:44:56');
INSERT INTO `product_visit` VALUES ('19', '56', '58', '2', '2017-10-20 17:46:14', '2017-10-20 17:45:20');
INSERT INTO `product_visit` VALUES ('20', '54', '58', '1', '2017-10-20 20:38:02', '2017-10-20 20:38:02');
INSERT INTO `product_visit` VALUES ('21', '54', '64', '3', '2017-10-22 10:32:06', '2017-10-22 10:19:26');
INSERT INTO `product_visit` VALUES ('22', '55', '64', '1', '2017-10-22 10:25:59', '2017-10-22 10:25:59');

-- ----------------------------
-- Table structure for promotion
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shopId` bigint(20) DEFAULT NULL,
  `sellerId` bigint(20) NOT NULL COMMENT '会员',
  `promotionName` varchar(64) NOT NULL COMMENT '优惠活动名',
  `promotionTag` varchar(255) NOT NULL COMMENT '活动标签',
  `startTime` datetime NOT NULL,
  `endTime` datetime NOT NULL,
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除,3结束#',
  PRIMARY KEY (`id`),
  KEY `memberId` (`sellerId`),
  KEY `promotionName` (`promotionName`),
  KEY `promotionTag` (`promotionTag`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='限时打折';

-- ----------------------------
-- Records of promotion
-- ----------------------------
INSERT INTO `promotion` VALUES ('1', '1', '1', 'asd', 'sad', '2017-07-09 15:35:00', '2017-08-03 15:35:05', null, null, '1');
INSERT INTO `promotion` VALUES ('13', '1', '1', '34234', '234234', '2019-06-06 00:00:00', '2019-06-07 00:00:00', '2017-08-17 12:02:39', '2017-08-17 12:02:39', '1');
INSERT INTO `promotion` VALUES ('14', '1', '1', '34234', '234234', '2019-06-06 00:00:00', '2019-06-07 00:00:00', '2017-08-17 13:51:04', '2017-08-17 13:51:04', '1');
INSERT INTO `promotion` VALUES ('15', '1', '1', '34234', '234234', '2019-06-06 00:00:00', '2019-06-07 00:00:00', '2017-08-17 13:51:59', '2017-08-17 13:51:59', '1');

-- ----------------------------
-- Table structure for promotion_set
-- ----------------------------
DROP TABLE IF EXISTS `promotion_set`;
CREATE TABLE `promotion_set` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promotionId` bigint(20) NOT NULL COMMENT '优惠活动',
  `productId` bigint(20) NOT NULL COMMENT '商品',
  `specificationValue` varchar(255) DEFAULT '' COMMENT '参与促销的规格',
  `isUnifiedSpec` varchar(1) NOT NULL DEFAULT '1' COMMENT '是否统一规格促销_D# 1统一规格 2不统一规格#',
  `promotionType` varchar(1) NOT NULL COMMENT '促销类型1 打折，2 减价',
  `promotionSetZhekou` float(5,2) NOT NULL COMMENT '折扣值',
  `promotionSetJianjia` bigint(20) NOT NULL COMMENT '减价金额',
  `updateTime` datetime DEFAULT NULL COMMENT '更新日期',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `set_promotion_id_ref` (`promotionId`),
  KEY `set_product_id_ref` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion_set
-- ----------------------------
INSERT INTO `promotion_set` VALUES ('9', '1', '1', '1,6', '1', '1', '5.00', '50', null, null, '1');
INSERT INTO `promotion_set` VALUES ('10', '1', '1', '1,6', '1', '1', '1.00', '1', null, null, '1');
INSERT INTO `promotion_set` VALUES ('12', '13', '1', '0', '1', '1', '1.00', '0', null, null, '1');
INSERT INTO `promotion_set` VALUES ('13', '13', '1', '0', '1', '1', '1.00', '0', null, null, '1');
INSERT INTO `promotion_set` VALUES ('14', '14', '1', '0', '1', '1', '1.00', '0', null, null, '1');
INSERT INTO `promotion_set` VALUES ('15', '14', '1', '0', '1', '1', '1.00', '0', null, null, '1');
INSERT INTO `promotion_set` VALUES ('16', '15', '1', '0', '1', '1', '1.00', '0', null, null, '1');
INSERT INTO `promotion_set` VALUES ('17', '15', '1', '0', '1', '1', '1.00', '0', null, null, '1');

-- ----------------------------
-- Table structure for pub_adv
-- ----------------------------
DROP TABLE IF EXISTS `pub_adv`;
CREATE TABLE `pub_adv` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT '' COMMENT '广告标题',
  `type` varchar(1) NOT NULL DEFAULT '0' COMMENT '广告类型_D#0图片;1flash;2代码3文字#',
  `positionId` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT '位置id',
  `startTime` datetime DEFAULT NULL COMMENT '开始时间',
  `endTime` datetime DEFAULT NULL COMMENT '结束时间',
  `adCode` varchar(1000) DEFAULT '' COMMENT '广告内容',
  `adLink` varchar(255) DEFAULT '' COMMENT '广告链接地址',
  `linkPerson` varchar(255) DEFAULT '' COMMENT '联系人',
  `linkEmail` varchar(255) DEFAULT '' COMMENT '联系邮箱',
  `linkPhone` varchar(20) DEFAULT '' COMMENT '联系电话',
  `description` text COMMENT '内容',
  `clickCount` int(11) NOT NULL DEFAULT '0' COMMENT '点击数',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `position_id` (`positionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告';

-- ----------------------------
-- Records of pub_adv
-- ----------------------------

-- ----------------------------
-- Table structure for pub_adv_position
-- ----------------------------
DROP TABLE IF EXISTS `pub_adv_position`;
CREATE TABLE `pub_adv_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '位置名称',
  `width` smallint(5) NOT NULL DEFAULT '0' COMMENT '宽',
  `height` smallint(5) NOT NULL DEFAULT '0' COMMENT '高',
  `playType` varchar(1) NOT NULL DEFAULT '0' COMMENT '1为单个2为列表3为随机',
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '状态0正常1关闭',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告位位置';

-- ----------------------------
-- Records of pub_adv_position
-- ----------------------------

-- ----------------------------
-- Table structure for pub_article
-- ----------------------------
DROP TABLE IF EXISTS `pub_article`;
CREATE TABLE `pub_article` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT '' COMMENT '标题',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  `edittime` datetime DEFAULT NULL COMMENT '最后编辑时间',
  `content` text COMMENT '内容',
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '状态0,1,2',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章';

-- ----------------------------
-- Records of pub_article
-- ----------------------------

-- ----------------------------
-- Table structure for pub_article_image
-- ----------------------------
DROP TABLE IF EXISTS `pub_article_image`;
CREATE TABLE `pub_article_image` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pubArticleId` bigint(20) DEFAULT NULL COMMENT '所属文章',
  `mainImagePath` varchar(255) DEFAULT '' COMMENT '主图',
  `showIndex` int(11) DEFAULT NULL COMMENT '显示顺序',
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '状态0,1,2',
  `thumbnailImagePath` varchar(255) DEFAULT '' COMMENT '缩略图',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='文章';

-- ----------------------------
-- Records of pub_article_image
-- ----------------------------
INSERT INTO `pub_article_image` VALUES ('6', null, 'img/pubArticle/2017/10/19/6xE6etqeg4Y2twMSS1iq7L.png', null, '1', 'img/pubArticle/2017/10/19/4ySjqJLBVANrWUd3gCtQZJ.png');
INSERT INTO `pub_article_image` VALUES ('7', null, 'img/pubArticle/2017/10/19/LGVWrtjYWTrz7mKGKYDJPq.png', null, '1', 'img/pubArticle/2017/10/19/3NHg13GkdzEzVNwGJGLJaE.jpg');
INSERT INTO `pub_article_image` VALUES ('8', null, 'img/pubArticle/2017/10/19/4ZFn6H14msZkz7Brcr42q6.png', '2', '1', 'img/pubArticle/2017/10/19/AAT6vbfYf8NLbLmm9xBBx3.png');
INSERT INTO `pub_article_image` VALUES ('9', null, 'img/pubArticle/2017/10/19/5Yn13vgecTTDTKAbSDjRaT.png', '3', '1', 'img/pubArticle/2017/10/19/77MMQdpZ7JS6dryGYhMJDZ.png');
INSERT INTO `pub_article_image` VALUES ('10', null, 'img/pubArticle/2017/10/19/8BykoELML78AoTzcvq6Yec.png', '5', '1', 'img/pubArticle/2017/10/19/9Ad6jZrfzR11cGMbDRC2HU.png');
INSERT INTO `pub_article_image` VALUES ('11', null, 'img/pubArticle/2017/10/19/MqdRf5UfAHHGsJp8uuwATd.png', '2', '1', 'img/pubArticle/2017/10/19/WmGrM3wEwwA4wc1sQtDKr9.png');

-- ----------------------------
-- Table structure for pub_autoid
-- ----------------------------
DROP TABLE IF EXISTS `pub_autoid`;
CREATE TABLE `pub_autoid` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stub` char(1) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pub_autoid
-- ----------------------------

-- ----------------------------
-- Table structure for pub_autoid_order
-- ----------------------------
DROP TABLE IF EXISTS `pub_autoid_order`;
CREATE TABLE `pub_autoid_order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stub` char(1) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pub_autoid_order
-- ----------------------------
INSERT INTO `pub_autoid_order` VALUES ('14', 'a');

-- ----------------------------
-- Table structure for pub_autoid_product
-- ----------------------------
DROP TABLE IF EXISTS `pub_autoid_product`;
CREATE TABLE `pub_autoid_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stub` char(1) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pub_autoid_product
-- ----------------------------
INSERT INTO `pub_autoid_product` VALUES ('68', 'a');

-- ----------------------------
-- Table structure for pub_autoid_review
-- ----------------------------
DROP TABLE IF EXISTS `pub_autoid_review`;
CREATE TABLE `pub_autoid_review` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stub` char(1) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stub` (`stub`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pub_autoid_review
-- ----------------------------
INSERT INTO `pub_autoid_review` VALUES ('19', 'a');

-- ----------------------------
-- Table structure for pub_sms_log
-- ----------------------------
DROP TABLE IF EXISTS `pub_sms_log`;
CREATE TABLE `pub_sms_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `creatTime` datetime DEFAULT NULL COMMENT '发送时间',
  `content` varchar(255) DEFAULT '' COMMENT '发送内容',
  `smsServerRes` varchar(60) DEFAULT '' COMMENT '短信发我端响应',
  `toPhone` varchar(20) DEFAULT '' COMMENT '目标号码',
  `paramJsonStr` varchar(255) DEFAULT NULL,
  `SignName` varchar(60) DEFAULT '' COMMENT '短信签名',
  `TemplateCode` varchar(60) DEFAULT '' COMMENT '短信模板',
  `RequestId` varchar(60) DEFAULT '' COMMENT '请求ID',
  `Code` varchar(36) DEFAULT '' COMMENT '状态码',
  `Message` varchar(36) DEFAULT '' COMMENT '状态码的描述',
  `BizId` varchar(36) DEFAULT '' COMMENT '发送回执ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章分类';

-- ----------------------------
-- Records of pub_sms_log
-- ----------------------------
INSERT INTO `pub_sms_log` VALUES ('1', '2017-10-22 10:57:42', '', '', '13793172269', '', '水墨人生网', 'SMS_103115002', '', '', '', '');

-- ----------------------------
-- Table structure for pub_term
-- ----------------------------
DROP TABLE IF EXISTS `pub_term`;
CREATE TABLE `pub_term` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT '' COMMENT '标题',
  `addtime` datetime DEFAULT NULL COMMENT '添加时间',
  `edittime` datetime DEFAULT NULL COMMENT '最后编辑时间',
  `content` text COMMENT '内容',
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '状态 0正常 1等待审核',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章';

-- ----------------------------
-- Records of pub_term
-- ----------------------------

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `buyerId` bigint(20) NOT NULL COMMENT '评价会员',
  `orderId` bigint(20) NOT NULL COMMENT '订单id',
  `isAnonymous` varchar(1) NOT NULL DEFAULT '1' COMMENT '是否匿名',
  `isAddReview` varchar(1) NOT NULL DEFAULT '0' COMMENT '是否追评_D#0不是追评，1是追评#',
  `isHaveImage` varchar(1) DEFAULT '0' COMMENT '是否有图片_D#0没有图片，1有图片#',
  `reviewMsxf` int(11) DEFAULT '5',
  `reviewMjfw` int(11) DEFAULT '5',
  `reviewWlfw` int(11) DEFAULT '5',
  `reviewSppj` varchar(400) DEFAULT NULL,
  `reviewFwpj` varchar(400) DEFAULT NULL,
  `ip` varchar(30) DEFAULT '' COMMENT 'IP',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`),
  KEY `FK9B6005777C62EDF8` (`buyerId`),
  KEY `orderId` (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES ('1', '54', '56', '7', '0', '0', '0', '5', '5', '5', '中', '', '', '2017-10-15 18:24:52', '2017-10-15 18:24:52', '1');
INSERT INTO `review` VALUES ('2', '56', '55', '9', '0', '0', '0', '5', '5', '5', '不错', '', '', '2017-10-15 20:01:45', '2017-10-15 20:01:45', '1');
INSERT INTO `review` VALUES ('3', '54', '56', '11', '0', '0', '0', '5', '5', '5', '甲', '', '', '2017-10-15 20:20:45', '2017-10-15 20:20:45', '1');

-- ----------------------------
-- Table structure for review_content
-- ----------------------------
DROP TABLE IF EXISTS `review_content`;
CREATE TABLE `review_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `reviewDefineId` bigint(20) DEFAULT NULL COMMENT '评价项',
  `reviewId` bigint(20) DEFAULT NULL,
  `score` int(11) NOT NULL COMMENT '评分',
  `content` varchar(600) NOT NULL DEFAULT '' COMMENT '内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`),
  KEY `reviewDefineId` (`reviewDefineId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of review_content
-- ----------------------------

-- ----------------------------
-- Table structure for review_define
-- ----------------------------
DROP TABLE IF EXISTS `review_define`;
CREATE TABLE `review_define` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `reviewName` varchar(60) NOT NULL DEFAULT '' COMMENT '评价名称',
  `reviewCode` varchar(20) NOT NULL DEFAULT '' COMMENT '评价编码',
  `reviewType` varchar(1) DEFAULT '' COMMENT '评价类型',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of review_define
-- ----------------------------

-- ----------------------------
-- Table structure for review_image
-- ----------------------------
DROP TABLE IF EXISTS `review_image`;
CREATE TABLE `review_image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `reviewId` bigint(20) DEFAULT NULL,
  `imgPath` varchar(255) DEFAULT NULL COMMENT '图片Url',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of review_image
-- ----------------------------

-- ----------------------------
-- Table structure for review_reply
-- ----------------------------
DROP TABLE IF EXISTS `review_reply`;
CREATE TABLE `review_reply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `reviewId` bigint(20) NOT NULL COMMENT '评价id',
  `content` varchar(1000) NOT NULL COMMENT '回复内容',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '' COMMENT '状态_D#0禁用 1正常 2删除#',
  `sellerId` bigint(20) DEFAULT NULL COMMENT '卖家',
  `buyerId` bigint(20) DEFAULT NULL COMMENT '买家',
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  PRIMARY KEY (`id`),
  KEY `FK9B6005777C62EDF8` (`reviewId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品评论';

-- ----------------------------
-- Records of review_reply
-- ----------------------------

-- ----------------------------
-- Table structure for seller_addr
-- ----------------------------
DROP TABLE IF EXISTS `seller_addr`;
CREATE TABLE `seller_addr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shopId` bigint(20) DEFAULT NULL COMMENT '店铺',
  `sellerId` bigint(20) NOT NULL COMMENT '卖家',
  `areaId` bigint(20) DEFAULT NULL,
  `contactCame` varchar(255) DEFAULT NULL,
  `country` bigint(20) DEFAULT NULL,
  `city` bigint(20) DEFAULT NULL COMMENT '市',
  `area` bigint(20) DEFAULT NULL COMMENT '县',
  `province` bigint(20) DEFAULT NULL COMMENT '省',
  `address` varchar(400) DEFAULT NULL COMMENT '发货地址',
  `memo` varchar(255) DEFAULT NULL COMMENT '备注',
  `phone` varchar(30) DEFAULT NULL COMMENT '电话',
  `sellerCompany` varchar(255) DEFAULT NULL COMMENT '卖家公司',
  `zipCode` varchar(30) DEFAULT NULL COMMENT '邮编',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  `isDefault` varchar(1) DEFAULT '1' COMMENT '是否默认_D#1默认，0非默认#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seller_addr
-- ----------------------------

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` bigint(20) NOT NULL COMMENT '所属会员',
  `shopName` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `logo` varchar(255) DEFAULT '' COMMENT '店铺图片',
  `tel` varchar(20) DEFAULT '' COMMENT '电话',
  `email` varchar(255) DEFAULT '' COMMENT '邮箱',
  `customerUrl` varchar(255) DEFAULT '' COMMENT 'web客服地址，在产品页面优先展示，没有时展示电话',
  `businessLicense` varchar(255) DEFAULT '' COMMENT '营业执照',
  `province` bigint(11) DEFAULT NULL COMMENT '省',
  `city` bigint(11) DEFAULT NULL COMMENT '市',
  `area` bigint(11) DEFAULT NULL COMMENT '区',
  `address` varchar(255) DEFAULT '' COMMENT '地址',
  `description` varchar(255) DEFAULT '' COMMENT '店铺介绍',
  `goodsComment` int(11) unsigned DEFAULT '0' COMMENT '好评数',
  `level` int(11) unsigned DEFAULT '0' COMMENT '店铺等级',
  `balance` bigint(11) DEFAULT '0' COMMENT '账户余额',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '修改时间',
  `status` varchar(1) DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COMMENT='企业用户';

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES ('54', '54', 'zlj的店铺', '', '', '', '', '', null, null, null, '', '', '0', '0', '0', null, null, '0');
INSERT INTO `shop` VALUES ('55', '55', 'quhao的店铺', '', '', '', '', '', null, null, null, '', '', '0', '0', '0', null, null, '0');
INSERT INTO `shop` VALUES ('56', '56', '中的店铺', '', '', '', '', '', null, null, null, '', '', '0', '0', '0', null, null, '0');
INSERT INTO `shop` VALUES ('57', '57', '15588873006的店铺', '', '', '', '', '', null, null, null, '', '', '0', '0', '0', null, null, '0');

-- ----------------------------
-- Table structure for specification
-- ----------------------------
DROP TABLE IF EXISTS `specification`;
CREATE TABLE `specification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '规格名称_Q@like@',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除,3申请添加,4审核未通过#_Q@=@',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `addUser` bigint(20) DEFAULT NULL COMMENT '申请添加人员',
  `checkUser` bigint(20) DEFAULT NULL COMMENT '审核人员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='规格';

-- ----------------------------
-- Records of specification
-- ----------------------------
INSERT INTO `specification` VALUES ('0', '统一规格(不要删除)', '0', null, null, '1', null, null, null);
INSERT INTO `specification` VALUES ('1', '颜色', '1', '2017-01-04 10:13:55', '2017-01-04 10:14:05', '1', null, null, null);
INSERT INTO `specification` VALUES ('2', '尺寸', '2', '2017-01-04 10:13:58', '2017-01-04 10:14:08', '1', null, null, null);
INSERT INTO `specification` VALUES ('3', '尺码', '3', '2017-01-04 10:14:01', '2017-01-04 10:14:11', '1', null, null, null);

-- ----------------------------
-- Table structure for specification_value
-- ----------------------------
DROP TABLE IF EXISTS `specification_value`;
CREATE TABLE `specification_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `specificationId` bigint(20) NOT NULL COMMENT '所属规格_Q@=@',
  `specificationName` varchar(255) NOT NULL DEFAULT '' COMMENT '规格名称_Q@like@',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '规格值名称_Q@like@',
  `image` varchar(255) DEFAULT '' COMMENT '图片',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `createTime` datetime DEFAULT NULL COMMENT '创建日期',
  `updateTime` datetime DEFAULT NULL COMMENT '最后修改日期',
  `status` varchar(1) NOT NULL DEFAULT '1' COMMENT '状态_D#0禁用 1正常 2删除,3申请添加,4审核未通过#_Q@=@',
  `addUser` bigint(20) DEFAULT NULL COMMENT '申请添加人员',
  `checkUser` bigint(20) DEFAULT NULL COMMENT '审核人员',
  PRIMARY KEY (`id`),
  KEY `FK5E624376629A04C2` (`specificationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规格值';

-- ----------------------------
-- Records of specification_value
-- ----------------------------

-- ----------------------------
-- Table structure for sys_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization`;
CREATE TABLE `sys_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '组织机构Id',
  `name` varchar(40) NOT NULL COMMENT '名称',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否可用',
  `modifyTime` datetime DEFAULT NULL COMMENT '修改时间',
  `parentId` bigint(20) DEFAULT NULL COMMENT '父级Id',
  `remark` varchar(60) DEFAULT NULL COMMENT '备注',
  `creater` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `description` varchar(60) DEFAULT NULL COMMENT '描述',
  `appId` bigint(20) DEFAULT NULL COMMENT '所属APP',
  `code` varchar(20) NOT NULL COMMENT '编码',
  `orgTreeCode` varchar(50) DEFAULT NULL COMMENT '唯一编码Code',
  `level` int(11) DEFAULT NULL COMMENT '层次',
  `combineCode` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_department_index_parent_id` (`parentId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1042748 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_organization
-- ----------------------------
INSERT INTO `sys_organization` VALUES ('1', '组织机构', '', null, '-1', null, null, null, null, null, '', null, '0', null);
INSERT INTO `sys_organization` VALUES ('1042713', '济南大学', '', null, '1', null, '3', '2016-11-18 00:00:00', '济南大学', '1', '10427', '10427', '1', null);
INSERT INTO `sys_organization` VALUES ('1042718', '计算机学院', '', null, '1042713', '计算机学院', '3', '2016-11-24 00:00:00', '计算机学院', '1', '05', '10427A05A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042719', '机械工程学院', '', null, '1042713', '山东济南', '3', '2016-11-24 00:00:00', '山东济南', '1', '01', '10427A01A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042720', '管理学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '03', '10427A03A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042721', '电子信息学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '04', '10427A04A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042722', '自动化学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '06', '10427A06A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042723', '理学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '07', '10427A07A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042724', '通信工程学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '08', '10427A08A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042725', '软件工程学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '10', '10427A10A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042726', '外国语学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '11', '10427A11A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042727', '会计学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '14', '10427A14A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042728', '经济学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '15', '10427A15A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042729', '生命信息与仪器工程学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '19', '10427A19A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042730', '材料与环境工程学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '20', '10427A20A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042731', '数字媒体与艺术设计学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '22', '10427A22A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042732', '马克思主义学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '23', '10427A23A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042733', '研究生院、党委研究生工作部', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '41', '10427A41A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042734', '国际教育学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '17', '10427A17A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042735', '计划财务处', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '63', '10427A63A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042736', '网络信息中心', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '72', '10427A72A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042737', '实验室与设备管理处', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '67', '10427A67A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042738', '学生处、人民武装部', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '65', '10427A65A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042739', '保卫处', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '64', '10427A64A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042740', '科学技术研究部', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '60', '10427A60A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042741', '教务处', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '59', '10427A59A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042742', '团委', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '56', '10427A56A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042743', '人文与法学院', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '12', '10427A12A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042744', '体育与艺术教学部', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '13', '10427A13A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042745', 'MBA中心', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', 'z8', '10427Az8A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042746', '后勤服务总公司（杭州文一教育发展有限公司）', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '79', '10427A79A', '2', null);
INSERT INTO `sys_organization` VALUES ('1042747', '图书馆', '', null, '1042713', null, '3', '2016-11-25 00:00:00', null, '1', '71', '10427╫71╫', '2', null);

-- ----------------------------
-- Table structure for sys_organization_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization_type`;
CREATE TABLE `sys_organization_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sequence` int(11) DEFAULT NULL COMMENT '顺序',
  `remark` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  `type` varchar(5) COLLATE utf8_bin NOT NULL COMMENT '条目类型##1：是否培养单位 2：组织机构信息##',
  `typeName` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '类型名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of sys_organization_type
-- ----------------------------
INSERT INTO `sys_organization_type` VALUES ('1', '1', '培养单位', '1', '');
INSERT INTO `sys_organization_type` VALUES ('2', '2', '学校', '2', '');
INSERT INTO `sys_organization_type` VALUES ('3', '3', '学院', '2', '');
INSERT INTO `sys_organization_type` VALUES ('4', '4', '系部', '2', '');
INSERT INTO `sys_organization_type` VALUES ('5', '5', '实验室', '2', '');
INSERT INTO `sys_organization_type` VALUES ('6', '6', '重点实验室', '2', '');

-- ----------------------------
-- Table structure for sys_organization_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_organization_user`;
CREATE TABLE `sys_organization_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `orgTreeCode` varchar(60) NOT NULL COMMENT '组织机构唯一编码',
  `authUserId` bigint(20) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`),
  KEY `sys_department_user_index_sys_department_id` (`orgTreeCode`),
  KEY `sys_department_user_index_auth_user_id` (`authUserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_organization_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '名称',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否可用',
  `creater` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `orgTreeCode` varchar(50) DEFAULT NULL COMMENT '所属组织机构',
  `authRoleId` bigint(20) DEFAULT NULL COMMENT '用户角色',
  `remark` varchar(60) DEFAULT NULL COMMENT '备注',
  `code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_position_index_depatment_id` (`orgTreeCode`),
  KEY `sys_position_index_auth_role_id` (`authRoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_position
-- ----------------------------

-- ----------------------------
-- Table structure for sys_position_auth_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_position_auth_user`;
CREATE TABLE `sys_position_auth_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `positionId` bigint(20) NOT NULL COMMENT '所属部门',
  `authUserId` bigint(20) NOT NULL COMMENT '用户',
  PRIMARY KEY (`id`),
  KEY `sys_position_rbac_user_index_position_id` (`positionId`),
  KEY `sys_position_rbac_user_index_auth_user_id` (`authUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_position_auth_user
-- ----------------------------
INSERT INTO `sys_position_auth_user` VALUES ('32', '32', '2610');
INSERT INTO `sys_position_auth_user` VALUES ('33', '32', '2611');

-- ----------------------------
-- Table structure for sys_sync_biz
-- ----------------------------
DROP TABLE IF EXISTS `sys_sync_biz`;
CREATE TABLE `sys_sync_biz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `syncBizName` varchar(30) DEFAULT NULL,
  `syncBizCode` varchar(30) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sync_biz
-- ----------------------------

-- ----------------------------
-- Table structure for sys_sync_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_sync_config`;
CREATE TABLE `sys_sync_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(60) DEFAULT NULL COMMENT '同步表名',
  `setFieldName` varchar(60) DEFAULT NULL COMMENT '字段名',
  `whereFieldName` varchar(60) DEFAULT NULL COMMENT '查询字段',
  `newValue` varchar(60) DEFAULT NULL COMMENT '新值',
  `whereValue` varchar(60) DEFAULT NULL COMMENT '条件值',
  `syncBizId` bigint(20) DEFAULT NULL COMMENT '业务点',
  `enabled` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sync_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_sync_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_sync_task`;
CREATE TABLE `sys_sync_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sybncBizId` bigint(60) DEFAULT NULL COMMENT '所属业务点',
  `createTime` datetime DEFAULT NULL,
  `excuteTimes` int(11) DEFAULT NULL COMMENT '执行次数',
  `isExcuteOver` bit(60) DEFAULT b'0' COMMENT '是否结束',
  `isExcuteSuccess` bit(60) DEFAULT b'0' COMMENT '是否成功',
  `startTime` datetime DEFAULT NULL COMMENT '开始时间',
  `enTime` datetime DEFAULT NULL COMMENT '结束时间',
  `remark` varchar(60) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sync_task
-- ----------------------------

-- ----------------------------
-- Table structure for wxpay_error_log
-- ----------------------------
DROP TABLE IF EXISTS `wxpay_error_log`;
CREATE TABLE `wxpay_error_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` bigint(20) NOT NULL COMMENT '会员_D#缴纳会员费或者购买商品用到#',
  `productId` bigint(20) DEFAULT NULL COMMENT '商品_D#缴纳保证金时用到#',
  `orderId` bigint(20) DEFAULT NULL COMMENT '订单_D#购买商品时用到#',
  `changeType` varchar(1) NOT NULL COMMENT '交易类型_D#1为充值,2,为提现,3,缴纳商品保证金，4，商品保证金退回，5,会员推广收益，6为管理员调节,7,商品出售， 99为其它类型#',
  `amount` bigint(20) DEFAULT '0' COMMENT '交易金额',
  `transactionTime` datetime NOT NULL COMMENT '交易产生时间',
  `transactionNo` varchar(60) DEFAULT '' COMMENT '业务单号',
  `failCode` varchar(30) DEFAULT '' COMMENT '交易失败码',
  `failMsg` varchar(800) DEFAULT '' COMMENT '交易失败错误栈',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `memberId` (`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxpay_error_log
-- ----------------------------
INSERT INTO `wxpay_error_log` VALUES ('1', '39', null, null, '9', '1', '2017-10-09 21:45:06', '17100921100610000000001559', 'FAIL', 'OK', '');
INSERT INTO `wxpay_error_log` VALUES ('2', '39', null, null, '9', '1', '2017-10-09 21:45:18', '17100921100610000000001559', 'FAIL', 'OK', '');
INSERT INTO `wxpay_error_log` VALUES ('3', '39', null, null, '9', '1', '2017-10-09 21:45:44', '17100921100610000000001559', 'FAIL', 'OK', '');
INSERT INTO `wxpay_error_log` VALUES ('4', '39', null, null, '9', '1', '2017-10-09 21:46:04', '17100921100610000000001559', 'FAIL', 'OK', '');
INSERT INTO `wxpay_error_log` VALUES ('5', '39', null, null, '9', '1', '2017-10-09 21:48:35', '', 'FAIL', '缺少参数', '');
INSERT INTO `wxpay_error_log` VALUES ('6', '41', null, null, '8', '4284843900', '2017-10-09 22:13:30', '17100922131810000000001498', 'FAIL', 'invalid total_fee', '');
INSERT INTO `wxpay_error_log` VALUES ('7', '41', null, null, '8', '4284843900', '2017-10-09 22:16:48', '17100922131810000000001498', 'FAIL', 'invalid total_fee', '');
INSERT INTO `wxpay_error_log` VALUES ('8', '45', null, null, '8', '0', '2017-10-11 10:05:36', '17101110052510000000001524', 'FAIL', 'invalid total_fee', '');
INSERT INTO `wxpay_error_log` VALUES ('9', '55', null, null, '3', '1', '2017-10-14 21:50:08', '17101421461410000000000004', 'FAIL', 'OK', '');
INSERT INTO `wxpay_error_log` VALUES ('10', '55', null, null, '3', '1', '2017-10-14 21:50:45', '17101421461410000000000004', 'FAIL', 'OK', '');
INSERT INTO `wxpay_error_log` VALUES ('11', '55', null, null, '3', '1', '2017-10-14 21:51:42', '17101421461410000000000004', 'FAIL', 'OK', '');

-- ----------------------------
-- Table structure for wxpay_notify_log
-- ----------------------------
DROP TABLE IF EXISTS `wxpay_notify_log`;
CREATE TABLE `wxpay_notify_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `notifyTime` datetime NOT NULL COMMENT '交易产生时间',
  `transactionNo` varchar(60) DEFAULT '' COMMENT '业务单号',
  `transactionId` varchar(60) DEFAULT '' COMMENT '支付单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxpay_notify_log
-- ----------------------------
INSERT INTO `wxpay_notify_log` VALUES ('8', '2017-10-09 20:48:05', '17100920473710000000001558', '4200000010201710097100314481');
INSERT INTO `wxpay_notify_log` VALUES ('9', '2017-10-09 21:10:18', '17100921100610000000001559', '4200000026201710097102990497');
INSERT INTO `wxpay_notify_log` VALUES ('10', '2017-10-09 21:50:18', '17100921500710000000001560', '4200000012201710097108938362');
INSERT INTO `wxpay_notify_log` VALUES ('11', '2017-10-09 21:51:07', '17100916305210000000001552', '4200000004201710097108991306');
INSERT INTO `wxpay_notify_log` VALUES ('12', '2017-10-09 22:05:44', '17100922052310000000001561', '4200000025201710097107851340');
INSERT INTO `wxpay_notify_log` VALUES ('13', '2017-10-09 22:19:12', '17100922131810000000001498', '4200000025201710097114418047');
INSERT INTO `wxpay_notify_log` VALUES ('14', '2017-10-10 10:54:59', '17101010201410000000001564', '4200000002201710107184340378');
INSERT INTO `wxpay_notify_log` VALUES ('15', '2017-10-10 13:20:51', '17101013203410000000001565', '4200000029201710107210111329');
INSERT INTO `wxpay_notify_log` VALUES ('16', '2017-10-10 20:44:47', '17101019182810000000001569', '4200000019201710107296650045');
INSERT INTO `wxpay_notify_log` VALUES ('17', '2017-10-10 20:58:55', '17101020515810000000001570', '4200000029201710107295738252');
INSERT INTO `wxpay_notify_log` VALUES ('18', '2017-10-10 21:07:24', '17101021042410000000001573', '4200000026201710107296165227');
INSERT INTO `wxpay_notify_log` VALUES ('19', '2017-10-10 21:38:15', '17101021374010000000001585', '4200000029201710107301902685');
INSERT INTO `wxpay_notify_log` VALUES ('20', '2017-10-10 23:09:55', '17101023081010000000001588', '4200000007201710107317515509');
INSERT INTO `wxpay_notify_log` VALUES ('21', '2017-10-10 23:15:13', '17101023145710000000001589', '4200000024201710107319584725');
INSERT INTO `wxpay_notify_log` VALUES ('22', '2017-10-10 23:36:25', '17101023352510000000001590', '4200000027201710107323114481');
INSERT INTO `wxpay_notify_log` VALUES ('23', '2017-10-10 23:41:51', '17101023404610000000001591', '4200000020201710107324553468');
INSERT INTO `wxpay_notify_log` VALUES ('24', '2017-10-10 23:42:43', '17101023422710000000001504', '4200000003201710107324589617');
INSERT INTO `wxpay_notify_log` VALUES ('25', '2017-10-11 00:10:30', '17101100100810000000001516', '4200000001201710117325292521');
INSERT INTO `wxpay_notify_log` VALUES ('26', '2017-10-11 09:31:29', '17101109305610000000001593', '4200000022201710117362025029');
INSERT INTO `wxpay_notify_log` VALUES ('27', '2017-10-11 09:33:50', '17101109331710000000001595', '4200000004201710117363784497');
INSERT INTO `wxpay_notify_log` VALUES ('28', '2017-10-11 09:35:23', '17101109330210000000001594', '4200000008201710117363810360');
INSERT INTO `wxpay_notify_log` VALUES ('29', '2017-10-11 09:49:18', '17101109483510000000001596', '4200000004201710117368624704');
INSERT INTO `wxpay_notify_log` VALUES ('30', '2017-10-11 09:51:12', '17101109500710000000001520', '4200000015201710117368749038');
INSERT INTO `wxpay_notify_log` VALUES ('31', '2017-10-11 10:04:12', '17101109484110000000001597', '4200000001201710117369205989');
INSERT INTO `wxpay_notify_log` VALUES ('32', '2017-10-11 10:13:44', '17101110124210000000001600', '4200000029201710117371326106');
INSERT INTO `wxpay_notify_log` VALUES ('33', '2017-10-11 10:18:43', '17101110173610000000001601', '4200000009201710117374642001');
INSERT INTO `wxpay_notify_log` VALUES ('34', '2017-10-14 18:48:19', '17101418301710000000000001', '4200000007201710148055292096');
INSERT INTO `wxpay_notify_log` VALUES ('35', '2017-10-14 18:51:31', '17101418504110000000000002', '4200000021201710148055520853');
INSERT INTO `wxpay_notify_log` VALUES ('36', '2017-10-14 18:52:33', '17101418512910000000000003', '4200000010201710148055602268');
INSERT INTO `wxpay_notify_log` VALUES ('37', '2017-10-14 19:08:55', '17101419080410000000000004', '4200000029201710148059639489');
INSERT INTO `wxpay_notify_log` VALUES ('38', '2017-10-14 19:44:30', '17101419441010000000000012', '4200000019201710148065968735');
INSERT INTO `wxpay_notify_log` VALUES ('39', '2017-10-14 20:54:17', '17101420533610000000000005', '4200000012201710148078159333');
INSERT INTO `wxpay_notify_log` VALUES ('40', '2017-10-14 20:59:16', '17101420562610000000000014', '4200000020201710148083781742');
INSERT INTO `wxpay_notify_log` VALUES ('41', '2017-10-14 21:26:43', '17101421251310000000000001', '4200000010201710148085433656');
INSERT INTO `wxpay_notify_log` VALUES ('42', '2017-10-14 21:30:19', '17101421281110000000000002', '4200000013201710148086100435');
INSERT INTO `wxpay_notify_log` VALUES ('43', '2017-10-14 21:34:40', '17101421343010000000000003', '4200000016201710148091536149');
INSERT INTO `wxpay_notify_log` VALUES ('44', '2017-10-14 21:36:14', '17101421360110000000000002', '4200000014201710148089838938');
INSERT INTO `wxpay_notify_log` VALUES ('45', '2017-10-14 21:53:22', '17101421461410000000000006', '4200000001201710148098369040');
INSERT INTO `wxpay_notify_log` VALUES ('46', '2017-10-15 17:32:18', '17101517320410000000000005', '4200000013201710158246597239');
INSERT INTO `wxpay_notify_log` VALUES ('47', '2017-10-15 17:35:28', '17101517350410000000000006', '4200000011201710158246746652');
INSERT INTO `wxpay_notify_log` VALUES ('48', '2017-10-15 17:43:04', '17101517424110000000000004', '4200000009201710158249258769');
INSERT INTO `wxpay_notify_log` VALUES ('49', '2017-10-15 18:12:40', '17101518121010000000000007', '4200000004201710158256707158');
INSERT INTO `wxpay_notify_log` VALUES ('50', '2017-10-15 18:15:12', '17101518144310000000000006', '4200000014201710158258459223');
INSERT INTO `wxpay_notify_log` VALUES ('51', '2017-10-15 18:18:57', '17101518181010000000000008', '4200000013201710158257070544');
INSERT INTO `wxpay_notify_log` VALUES ('52', '2017-10-15 18:21:16', '17101518201410000000000008', '4200000009201710158257179516');
INSERT INTO `wxpay_notify_log` VALUES ('53', '2017-10-15 19:38:03', '17101519373210000000000012', '4200000019201710158269664879');
INSERT INTO `wxpay_notify_log` VALUES ('54', '2017-10-15 20:00:01', '17101519543010000000000014', '4200000023201710158280542854');
INSERT INTO `wxpay_notify_log` VALUES ('55', '2017-10-15 20:00:47', '17101520001810000000000010', '4200000015201710158278784603');
INSERT INTO `wxpay_notify_log` VALUES ('56', '2017-10-15 20:16:33', '17101520154610000000000016', '4200000012201710158279563245');
INSERT INTO `wxpay_notify_log` VALUES ('57', '2017-10-15 20:17:16', '17101520170510000000000012', '4200000022201710158279618203');
INSERT INTO `wxpay_notify_log` VALUES ('58', '2017-10-20 17:25:25', '17102017224810000000000018', '4200000007201710209230815385');
INSERT INTO `wxpay_notify_log` VALUES ('59', '2017-10-22 10:25:15', '17102210201710000000000019', '4200000020201710229570720937');
INSERT INTO `wxpay_notify_log` VALUES ('60', '2017-10-22 10:57:41', '1710221026009000000000000014', '4200000025201710229573718502');

-- ----------------------------
-- Table structure for wxpay_unify
-- ----------------------------
DROP TABLE IF EXISTS `wxpay_unify`;
CREATE TABLE `wxpay_unify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `memberId` bigint(20) NOT NULL COMMENT '会员_D#缴纳会员费或者购买商品用到#',
  `productId` bigint(20) DEFAULT NULL COMMENT '商品_D#缴纳保证金时用到#',
  `orderId` bigint(20) DEFAULT NULL COMMENT '订单_D#购买商品时用到#',
  `prepayId` varchar(60) NOT NULL COMMENT '交易类型_D#1为充值,2,为提现,3,缴纳商品保证金，4，商品保证金退回，5,会员推广收益，6为管理员调节,7,商品出售， 99为其它类型#',
  `transactionNo` varchar(60) NOT NULL DEFAULT '' COMMENT '业务单号',
  `creatTime` datetime DEFAULT NULL COMMENT '创建日期',
  `status` varchar(1) DEFAULT '' COMMENT '状态_D#1有效，0：失效#',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxpay_unify
-- ----------------------------
