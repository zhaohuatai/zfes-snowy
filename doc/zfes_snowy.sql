/*
Navicat MySQL Data Transfer

Source Server         : localhost3308
Source Server Version : 50719
Source Host           : localhost:3308
Source Database       : zfes_snowy

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-10-26 08:59:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_app
-- ----------------------------
DROP TABLE IF EXISTS `auth_app`;
CREATE TABLE `auth_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `appKey` varchar(40) NOT NULL COMMENT '模块标识_U',
  `name` varchar(40) NOT NULL COMMENT '模块名称',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `disIndex` int(11) NOT NULL COMMENT '顺序',
  `remark` varchar(60) DEFAULT NULL COMMENT '描述',
  `appDomain` varchar(300) DEFAULT NULL COMMENT 'app域名',
  `enabled` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`),
  KEY `auth_module_index_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_app
-- ----------------------------
INSERT INTO `auth_app` VALUES ('1', 'APP_ROOT', '基础应用', '1', '1', 'ssss', '', '');
INSERT INTO `auth_app` VALUES ('2', 'APP1', 'APP1', '', '3', 'APP1', null, '');

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `name` varchar(30) NOT NULL COMMENT '用户组名称',
  `description` varchar(60) DEFAULT NULL COMMENT '用户组描述',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '用户组状态',
  `parentId` bigint(20) DEFAULT NULL COMMENT '父用户组',
  `appId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_group_index_parent_id` (`parentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------
INSERT INTO `auth_group` VALUES ('1', '根节点', null, '\0', null, null);
INSERT INTO `auth_group` VALUES ('2', 'A1', null, '\0', '1', null);
INSERT INTO `auth_group` VALUES ('3', 'A2', null, '\0', '1', null);
INSERT INTO `auth_group` VALUES ('4', 'B', null, '\0', '2', null);

-- ----------------------------
-- Table structure for auth_group_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_role`;
CREATE TABLE `auth_group_role` (
  `id` bigint(20) NOT NULL,
  `auth_group_id` bigint(20) NOT NULL,
  `auth_role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_group_role_auth_group_id` (`auth_group_id`),
  KEY `auth_group_role_auth_role_id` (`auth_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_role
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_user`;
CREATE TABLE `auth_group_user` (
  `id` bigint(20) NOT NULL,
  `auth_group_id` bigint(20) NOT NULL,
  `auth_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_group_user_index_auth_group_id` (`auth_group_id`),
  KEY `auth_group_user_index_auth_user_id` (`auth_user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `auth_menu`;
CREATE TABLE `auth_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '模块状态',
  `name` varchar(40) NOT NULL,
  `iconCls` varchar(40) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL,
  `showIndex` int(11) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `moduleId` bigint(20) DEFAULT NULL,
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属应用',
  `description` varchar(40) DEFAULT NULL,
  `permissionId` bigint(20) DEFAULT NULL,
  `isCommon` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `auth_menu_index_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_menu
-- ----------------------------
INSERT INTO `auth_menu` VALUES ('4', '', '根节点', '', 'G', '1', '-1', null, 'APP_ROOT', null, null, '');
INSERT INTO `auth_menu` VALUES ('10', '', '权限维护', 'lock', 'G', '1', '4', '1', 'APP_ROOT', null, null, '');
INSERT INTO `auth_menu` VALUES ('11', '', '菜单管理', null, 'M', '1', '10', '1', 'APP_ROOT', '菜单管理', '42', '');
INSERT INTO `auth_menu` VALUES ('12', '', '权限管理', null, 'M', '2', '10', '1', 'APP_ROOT', '1112', '44', '');
INSERT INTO `auth_menu` VALUES ('13', '', '角色管理', null, 'M', '3', '10', '1', 'APP_ROOT', '角色', '43', '');
INSERT INTO `auth_menu` VALUES ('40', '', '应用管理', null, 'M', '1', '10', '1', 'APP_ROOT', 'APP管理', '40', '');
INSERT INTO `auth_menu` VALUES ('97', '', '用户管理', null, 'M', '4', '10', '1', 'APP_ROOT', '用户信息管理', '45', '');
INSERT INTO `auth_menu` VALUES ('98', '', '总览', 'pie-chart', 'M', '1', '4', '3', 'APP_ROOT', null, '1079', '\0');
INSERT INTO `auth_menu` VALUES ('99', '', '商品管理', 'picture', 'M', '2', '4', '3', 'APP_ROOT', '商品管理', '1082', '\0');
INSERT INTO `auth_menu` VALUES ('100', '', '订单管理', 'file', 'M', '3', '4', '3', 'APP_ROOT', null, '1083', '\0');
INSERT INTO `auth_menu` VALUES ('101', '', '会员管理', 'user', 'M', '4', '4', '3', 'APP_ROOT', null, '1084', '\0');
INSERT INTO `auth_menu` VALUES ('102', '', '交易管理', 'pay-circle', 'M', '5', '4', '3', 'APP_ROOT', null, '1085', '\0');
INSERT INTO `auth_menu` VALUES ('103', '', '内容管理', 'file-text', 'M', '6', '4', '3', 'APP_ROOT', null, '1086', '\0');
INSERT INTO `auth_menu` VALUES ('104', '', '配置管理', 'setting', 'M', '7', '4', '3', 'APP_ROOT', null, '1087', '\0');
INSERT INTO `auth_menu` VALUES ('105', '', '分类管理', 'switcher', 'M', '8', '4', '3', 'APP_ROOT', null, '1088', '\0');

-- ----------------------------
-- Table structure for auth_module
-- ----------------------------
DROP TABLE IF EXISTS `auth_module`;
CREATE TABLE `auth_module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(40) NOT NULL COMMENT '模块名称',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '模块状态',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `disIndex` int(11) NOT NULL COMMENT '顺序',
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属app',
  `parentId` bigint(20) NOT NULL COMMENT '父Id',
  `remark` varchar(30) DEFAULT NULL COMMENT '描述',
  `code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_module_index_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_module
-- ----------------------------
INSERT INTO `auth_module` VALUES ('1', '权限模块', '', '', '21', 'APP_ROOT', '1', '权限模块', 'authModule');
INSERT INTO `auth_module` VALUES ('2', '系统模块说明', '', null, '1', 'APP_ROOT', '-1', '根节点', '');
INSERT INTO `auth_module` VALUES ('3', '商城管理', '', null, '22', 'APP_ROOT', '-1', '商城管理', 'weimall');

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permCode` varchar(60) NOT NULL,
  `name` varchar(40) NOT NULL,
  `enabled` bit(1) NOT NULL DEFAULT b'1',
  `type` varchar(1) NOT NULL,
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属应用',
  `url` varchar(255) DEFAULT NULL,
  `description` varchar(60) DEFAULT NULL,
  `moduleId` bigint(20) DEFAULT NULL,
  `isCommon` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_index_code` (`permCode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1094 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'authApp:loadAuthApp', '应用加载', '', 'p', 'APP_ROOT', '/auth/authApp/loadAuthApp', '应用加载', '1', '');
INSERT INTO `auth_permission` VALUES ('2', 'authApp:updateAuthApp', '应用更新', '', 'p', 'APP_ROOT', '/auth/authApp/updateAuthApp', '应用更新', '1', '');
INSERT INTO `auth_permission` VALUES ('3', 'authApp:loadAuthAppList', '应用列表', '', 'p', 'APP_ROOT', '/auth/authApp/loadAuthAppList', 'app应用列表', '1', '');
INSERT INTO `auth_permission` VALUES ('4', 'authApp:addAuthApp', '应用添加', '', 'p', 'APP_ROOT', '/auth/authApp/addAuthApp', '应用添加', '1', '');
INSERT INTO `auth_permission` VALUES ('5', 'authApp:deletAuthApp', '应用删除', '', 'p', 'APP_ROOT', '/auth/authApp/deletAuthApp', '应用删除', '1', '');
INSERT INTO `auth_permission` VALUES ('6', 'authApp:setAuthAppEnabled', '应用状态设置', '', 'p', 'APP_ROOT', '/auth/authApp/setAuthAppEnabled', '应用状态设置', '1', '');
INSERT INTO `auth_permission` VALUES ('7', 'authMenu:loadAuthMenuList', '菜单列表', '', 'p', 'APP_ROOT', '/auth/authMenu/loadAuthMenuList', '菜单列表', '1', '');
INSERT INTO `auth_permission` VALUES ('8', 'authMenu:loadAuthMenu', '菜单加载', '', 'p', 'APP_ROOT', '/auth/authMenu/loadAuthMenu', '菜单加载', '1', '');
INSERT INTO `auth_permission` VALUES ('9', 'authMenu:updateAuthMenu', '菜单更新', '', 'p', 'APP_ROOT', '/auth/authMenu/updateAuthMenu', '菜单更新', '1', '');
INSERT INTO `auth_permission` VALUES ('10', 'authMenu:removeAuthMenu', '菜单删除', '', 'p', 'APP_ROOT', '/auth/authMenu/removeAuthMenu', '菜单删除', '1', '');
INSERT INTO `auth_permission` VALUES ('11', 'authMenu:loadAuthMenuTree', '加载菜单树', '', 'p', 'APP_ROOT', '/auth/authMenu/loadAuthMenuTree', '加载菜单树', '1', '');
INSERT INTO `auth_permission` VALUES ('12', 'authMenu:addAuthMenu', '菜单添加', '', 'p', 'APP_ROOT', '/auth/authMenu/addAuthMenu', '菜单添加', '1', '');
INSERT INTO `auth_permission` VALUES ('13', 'authMenu:setAuthMenuEnabled', '菜单状态设置', '', 'p', 'APP_ROOT', '/auth/authMenu/setAuthMenuEnabled', '菜单状态设置', '1', '');
INSERT INTO `auth_permission` VALUES ('14', 'authModule:updateAuthModule', '系统模块表更新', '', 'p', 'APP_ROOT', '/auth/authModule/updateAuthModule', '系统模块表更新', '1', '');
INSERT INTO `auth_permission` VALUES ('15', 'authModule:loadAuthModule', '模块表加载', '', 'p', 'APP_ROOT', '/auth/authModule/loadAuthModule', '模块表加载', '1', '');
INSERT INTO `auth_permission` VALUES ('16', 'authModule:addAuthModule', '模块表添加', '', 'p', 'APP_ROOT', '/auth/authModule/addAuthModule', '模块表添加', '1', '');
INSERT INTO `auth_permission` VALUES ('17', 'authModule:deletAuthModule', '模块表删除', '', 'p', 'APP_ROOT', '/auth/authModule/deletAuthModule', '模块表删除', '1', '');
INSERT INTO `auth_permission` VALUES ('18', 'authModule:loadAuthModuleList', '模块表列表', '', 'p', 'APP_ROOT', '/auth/authModule/loadAuthModuleList', '模块表列表', '1', '');
INSERT INTO `auth_permission` VALUES ('19', 'authPermission:loadForUpdate', '权限加载', '', 'p', 'APP_ROOT', '/auth/authPermission/loadForUpdate', '权限加载', '1', '');
INSERT INTO `auth_permission` VALUES ('20', 'authPermission:updatePermission', '权限更新', '', 'p', 'APP_ROOT', '/auth/authPermission/updatePermission', '权限更新', '1', '');
INSERT INTO `auth_permission` VALUES ('21', 'authPermission:addPermission', '权限添加', '', 'p', 'APP_ROOT', '/auth/authPermission/addPermission', '权限添加', '1', '');
INSERT INTO `auth_permission` VALUES ('22', 'authPermission:setPermissionEnabled', '权限状态设置', '', 'p', 'APP_ROOT', '/auth/authPermission/setPermissionEnabled', '权限状态设置', '1', '');
INSERT INTO `auth_permission` VALUES ('23', 'authPermission:batchDeleteAuthPermission', '权限批量删除', '', 'p', 'APP_ROOT', '/auth/authPermission/batchDeleteAuthPermission', '权限批量删除', '1', '');
INSERT INTO `auth_permission` VALUES ('24', 'authPermission:loadAuthPermissionList', '权限列表', '', 'p', 'APP_ROOT', '/auth/authPermission/loadAuthPermissionList', '权限列表', '1', '');
INSERT INTO `auth_permission` VALUES ('25', 'authPermission:deletAuthPermission', '权限删除', '', 'p', 'APP_ROOT', '/auth/authPermission/deletAuthPermission', '权限删除', '1', '');
INSERT INTO `auth_permission` VALUES ('26', 'authRole:updateRole', '角色更新', '', 'p', 'APP_ROOT', '/auth/authRole/updateRole', '角色更新', '1', '');
INSERT INTO `auth_permission` VALUES ('27', 'authRole:loadAuthRoleList', '角色列表', '', 'p', 'APP_ROOT', '/auth/authRole/loadAuthRoleList', '角色列表', '1', '');
INSERT INTO `auth_permission` VALUES ('28', 'authRole:deletAuthRole', '角色删除', '', 'p', 'APP_ROOT', '/auth/authRole/deletAuthRole', '角色删除', '1', '');
INSERT INTO `auth_permission` VALUES ('29', 'authRole:loadRole', '角色加载', '', 'p', 'APP_ROOT', '/auth/authRole/loadRole', '角色加载', '1', '');
INSERT INTO `auth_permission` VALUES ('30', 'authRole:permAssign', '权限分配', '', 'p', 'APP_ROOT', '/auth/authRole/addPermsToRole', '权限分配', '1', '');
INSERT INTO `auth_permission` VALUES ('31', 'authRole:setAuthRoleEnabled', '角色状态更新', '', 'p', 'APP_ROOT', '/auth/authRole/setAuthRoleEnabled', '角色状态更新', '1', '');
INSERT INTO `auth_permission` VALUES ('32', 'authRole:addRole', '角色添加', '', 'p', 'APP_ROOT', '/auth/authRole/addRole', '角色添加', '1', '');
INSERT INTO `auth_permission` VALUES ('33', 'authUser:loadUpdateData', '用户加载', '', 'p', 'APP_ROOT', '/auth/authUser/loadUpdateData', '用户加载', '1', '');
INSERT INTO `auth_permission` VALUES ('34', 'authUser:updateUser', '用户更新', '', 'p', 'APP_ROOT', '/auth/authUser/updateUser', '用户更新', '1', '');
INSERT INTO `auth_permission` VALUES ('35', 'authUser:setUserStatus', '更新用户状态', '', 'p', 'APP_ROOT', '/auth/authUser/setUserStatus', '更新用户状态', '1', '');
INSERT INTO `auth_permission` VALUES ('36', 'authUser:roleAssign', '用户角色分配', '', 'p', 'APP_ROOT', '/auth/authUser/addRolesToUser', '用户角色分配', '1', '');
INSERT INTO `auth_permission` VALUES ('37', 'authUser:loadAuthUserList', '用户列表', '', 'p', 'APP_ROOT', '/auth/authUser/loadAuthUserList', '用户列表', '1', '');
INSERT INTO `auth_permission` VALUES ('38', 'authUser:resetAuthUserpwd', '重置密码', '', 'p', 'APP_ROOT', '/auth/authUser/resetAuthUserPwd', '重置密码', '1', '');
INSERT INTO `auth_permission` VALUES ('39', 'authUser:permAssign', '用户权限分配', '', 'p', 'APP_ROOT', '/auth/authUser/addPremsToUser', '用户权限分配', '1', '');
INSERT INTO `auth_permission` VALUES ('40', 'authApp:index', '应用管理', '', 'M', 'APP_ROOT', '/auth/application', null, '1', '');
INSERT INTO `auth_permission` VALUES ('41', 'authModule:index', '模块管理', '', 'M', 'APP_ROOT', '/auth/module', '', '1', '');
INSERT INTO `auth_permission` VALUES ('42', 'authMenu:index', '菜单管理', '', 'M', 'APP_ROOT', '/auth/menu', '', '1', '');
INSERT INTO `auth_permission` VALUES ('43', 'authRole:index', '角色管理', '', 'M', 'APP_ROOT', '/auth/role', '', '1', '');
INSERT INTO `auth_permission` VALUES ('44', 'authPermission:index', '权限管理', '', 'M', 'APP_ROOT', '/auth/permission', '', '1', '');
INSERT INTO `auth_permission` VALUES ('45', 'authUser:index', '账户管理', '', 'M', 'APP_ROOT', '/auth/user', '', '1', '');
INSERT INTO `auth_permission` VALUES ('1000', 'zfesDemo:index', '开发示例', '', 'M', 'APP_ROOT', '/view/zfesDemo/zfesDemo.html', '', '1', '');
INSERT INTO `auth_permission` VALUES ('1001', 'zfesDemo:loadZfesDemo', '示例查询', '', 'p', 'APP_ROOT', '/zfesDemo/loadZfesDemo', '角色列表', '1', '');
INSERT INTO `auth_permission` VALUES ('1002', 'zfesDemo:loadZfesDemoList', '示例加载', '', 'p', 'APP_ROOT', '/zfesDemo/loadZfesDemoDataSet', '角色列表', '1', '');
INSERT INTO `auth_permission` VALUES ('1003', 'zfesDemo:addZfesDemo', '示例添加', '', 'P', 'APP_ROOT', '/zfesDemo/addZfesDemo', null, '2', '\0');
INSERT INTO `auth_permission` VALUES ('1004', 'zfesDemo:updateZfesDemo', '示例更新', '', 'P', 'APP_ROOT', '/zfesDemo/updateZfesDemo', null, '2', '\0');
INSERT INTO `auth_permission` VALUES ('1005', 'zfesDemo:deletZfesDemo', '示例删除', '', 'P', 'APP_ROOT', '/zfesDemo/deletZfesDemo', null, '2', '\0');
INSERT INTO `auth_permission` VALUES ('1006', 'category:loadCategory', '加载分类', '', 'p', 'APP_ROOT', '/admin/category/loadCategory', '加载分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1007', 'category:deleteCategory', '删除分类', '', 'p', 'APP_ROOT', '/admin/category/deleteCategory', '删除分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1008', 'category:disableCategory', '禁用分类', '', 'p', 'APP_ROOT', '/admin/category/disableCategory', '禁用分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1009', 'category:createCategory', '创建分类', '', 'p', 'APP_ROOT', '/admin/category/createCategory', '创建分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1010', 'category:loadCategoryTree', '加载分类列表', '', 'p', 'APP_ROOT', '/admin/category/loadCategoryTree', '加载分类列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1011', 'category:enableCategoryAttr', '使能分类', '', 'p', 'APP_ROOT', '/admin/category/enableCategoryAttr', '使能分类', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1012', 'category:updateCategoryOrderNum', '更新分类顺序', '', 'p', 'APP_ROOT', '/admin/category/updateCategoryOrderNum', '更新分类顺序', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1013', 'consultation:loadConsultation', '加载留言', '', 'p', 'APP_ROOT', '/admin/consultation/loadConsultation', '加载留言', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1014', 'consultation:deleteConsultation', '删除留言', '', 'p', 'APP_ROOT', '/admin/consultation/deleteConsultation', '删除留言', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1015', 'consultation:loadConsultationReply', '加载留言回复', '', 'p', 'APP_ROOT', '/admin/consultation/loadConsultationReply', '加载留言回复', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1016', 'consultation:deleteConsultationReply', '删除留言回复', '', 'p', 'APP_ROOT', '/admin/consultation/deleteConsultationReply', '删除留言回复', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1017', 'dicSysconfig:updateDicSysconfig', '系统参数更新', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/updateDicSysconfig', '系统参数更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1018', 'dicSysconfig:deletDicSysconfig', '系统参数删除', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/deleteDicSysconfig', '系统参数删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1019', 'dicSysconfig:loadDicSysconfigDataSet', '系统参数列表', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/loadDicSysconfigDataSet', '系统参数列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1020', 'dicSysconfig:addDicSysconfig', '系统参数添加', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/addDicSysconfig', '系统参数添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1021', 'dicSysconfig:loadDicSysconfig', '系统参数加载', '', 'p', 'APP_ROOT', '/admin/dic/dicSysconfig/loadDicSysconfig', '系统参数加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1022', 'memberAccountDeposit:loadMemberAccountDepositDataSet', '会员保证金记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountDeposit/loadMemberAccountDepositDataSet', '会员保证金记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1023', 'memberAccountDeposit:loadMemberAccountDeposit', '会员保证金记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountDeposit/loadMemberAccountDeposit', '会员保证金记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1024', 'memberAccountIntro:loadMemberAccountIntroDataSet', '会员推广收益记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountIntro/loadMemberAccountIntroDataSet', '会员推广收益记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1025', 'memberAccountIntro:loadMemberAccountIntro', '会员推广收益记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountIntro/loadMemberAccountIntro', '会员推广收益记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1026', 'memberAccountLog:loadMemberAccountLogDataSet', '会员费用记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountLog/loadMemberAccountLogDataSet', '会员费用记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1027', 'memberAccountLog:loadMemberAccountLog', '会员费用记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountLog/loadMemberAccountLog', '会员费用记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1028', 'memberAccountRecharge:loadMemberAccountRechargeDataSet', '会员充值记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountRecharge/loadMemberAccountRechargeDataSet', '会员充值记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1029', 'memberAccountRecharge:deletMemberAccountRecharge', '会员充值记录删除', '', 'p', 'APP_ROOT', '/admin/member/memberAccountRecharge/deleteMemberAccountRecharge', '会员充值记录删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1030', 'memberAccountRecharge:loadMemberAccountRecharge', '会员充值记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountRecharge/loadMemberAccountRecharge', '会员充值记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1031', 'memberAccountWithdraw:loadMemberAccountWithdrawDataSet', '会员提现记录列表', '', 'p', 'APP_ROOT', '/admin/member/memberAccountWithdrawCash/loadMemberAccountWithdrawCashDataSet', '会员提现记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1032', 'memberAccountWithdraw:loadMemberAccountWithdrawCash', '会员提现记录加载', '', 'p', 'APP_ROOT', '/admin/member/memberAccountWithdrawCash/loadMemberAccountWithdrawCash', '会员提现记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1033', 'member:setMemberStatus', '会员状态设置', '', 'p', 'APP_ROOT', '/admin/member/setMemberStatus', '会员状态设置', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1034', 'member:loadMemberCount', '会员数', '', 'p', 'APP_ROOT', '/admin/member/loadMemberCount', '会员数', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1035', 'member:loadMember', '会员加载', '', 'p', 'APP_ROOT', '/admin/member/loadMember', '会员加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1036', 'member:loadMemberDataSet', '会员列表', '', 'p', 'APP_ROOT', '/admin/member/loadMemberDataSet', '会员列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1037', 'memberRegist:loadMemberRegistDataSet', '会员注册记录', '', 'p', 'APP_ROOT', '/admin/member/memberRegist/loadMemberRegistDataSet', '会员注册记录', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1038', 'memberRegist:loadMemberRegistById', '会员加载', '', 'p', 'APP_ROOT', '/admin/member/memberRegist/loadMemberRegistById', '会员加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1039', 'memberWeiuser:addMemberWeiuser', '会员微信账号信息添加', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/addMemberWeiuser', '会员微信账号信息添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1040', 'memberWeiuser:updateMemberWeiuser', '会员微信账号信息更新', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/updateMemberWeiuser', '会员微信账号信息更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1041', 'memberWeiuser:deletMemberWeiuser', '会员微信账号信息删除', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/deleteMemberWeiuser', '会员微信账号信息删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1042', 'memberWeiuser:loadMemberWeiuserDataSet', '会员微信账号信息列表', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/loadMemberWeiuserDataSet', '会员微信账号信息列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1043', 'memberWeiuser:loadMemberWeiuser', '会员微信账号信息加载', '', 'p', 'APP_ROOT', '/admin/member/memberWeiuser/loadMemberWeiuser', '会员微信账号信息加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1044', 'orders:setOrdersStatus', '订单状态设置', '', 'p', 'APP_ROOT', '/admin/orders/setOrdersStatus', '订单状态设置', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1045', 'orders:confirmReceipt', '确认收货', '', 'p', 'APP_ROOT', '/admin/orders/confirmReceipt', '确认收货', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1046', 'orders:loadOrdersListCount', '订单数量', '', 'p', 'APP_ROOT', '/admin/orders/loadOrdersListCount', '订单数量', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1047', 'orders:loadOrdersDataSet', '订单列表', '', 'p', 'APP_ROOT', '/admin/orders/loadOrdersDataSet', '订单列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1048', 'ordersStatusLog:loadOrdersStatusLogDataSet', '订单状态记录列表', '', 'p', 'APP_ROOT', '/orders/ordersStatusLog/loadOrdersStatusLogDataSet', '订单状态记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1049', 'ordersStatusLog:loadOrdersStatusLog', '订单状态记录加载', '', 'p', 'APP_ROOT', '/orders/ordersStatusLog/loadOrdersStatusLog', '订单状态记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1050', 'productAdv:loadProductAdv', '商品广告加载', '', 'p', 'APP_ROOT', '/admin/product/productAdv/loadProductAdv', '商品广告加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1051', 'productAdv:deletProductAdv', '商品广告删除', '', 'p', 'APP_ROOT', '/admin/product/productAdv/deleteProductAdv', '商品广告删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1052', 'productAdv:createProductAdv', '商品广告添加', '', 'p', 'APP_ROOT', '/admin/product/productAdv/createProductAdv', '商品广告添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1053', 'productAdv:updateProductAdv', '商品广告更新', '', 'p', 'APP_ROOT', '/admin/product/productAdv/updateProductAdv', '商品广告更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1054', 'productAdv:setProductAdvIsShow', '商品广告是否显示', '', 'p', 'APP_ROOT', '/admin/product/productAdv/setProductAdvIsShow', '商品广告是否显示', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1055', 'productAdv:loadProductAdvDataSet', '商品广告列表', '', 'p', 'APP_ROOT', '/admin/product/productAdv/loadProductAdvDataSet', '商品广告列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1056', 'product:setProductStatus', '商品状态设置', '', 'p', 'APP_ROOT', '/admin/product/product/setProductStatus', '商品状态设置', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1057', 'product:loadProduct', '商品列表', '', 'p', 'APP_ROOT', '/admin/product/product/loadProduct', '商品列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1058', 'product:loadProductListCount', '商品数量', '', 'p', 'APP_ROOT', '/admin/product/product/loadProductListCount', '商品数量', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1059', 'product:deleteProductImage', '商品图片删除', '', 'p', 'APP_ROOT', '/admin/product/product/deleteProductImage', '商品图片删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1060', 'pubArticleImage:loadPubArticleImage', '图片模式的文章加载', '', 'p', 'APP_ROOT', '/admin/pub/pubArticleImage/loadPubArticleImage', '图片模式的文章加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1061', 'pubArticleImage:loadPubArticleImageDataSet', '图片模式的文章列表', '', 'p', 'APP_ROOT', '/admin/pub/pubArticleImage/loadPubArticleImageDataSet', '图片模式的文章列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1062', 'pubArticleImage:createPubArticleImage', '图片模式的文章添加', '', 'p', 'APP_ROOT', '/admin/pub/pubArticleImage/createPubArticleImage', '图片模式的文章添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1063', 'pubSmsLog:addPubSmsLog', '短信记录添加', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/addPubSmsLog', '短信记录添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1064', 'pubSmsLog:loadPubSmsLog', '短信记录加载', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/loadPubSmsLog', '短信记录加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1065', 'pubSmsLog:updatePubSmsLog', '短信记录更新', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/updatePubSmsLog', '短信记录更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1066', 'pubSmsLog:deletPubSmsLog', '短信记录删除', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/deletePubSmsLog', '短信记录删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1067', 'pubSmsLog:loadPubSmsLogDataSet', '短信记录列表', '', 'p', 'APP_ROOT', '/admin/pub/pubSmsLog/loadPubSmsLogDataSet', '短信记录列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1068', 'pubTerm:deletPubTerm', '条款删除', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/deletePubTerm', '条款删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1069', 'pubTerm:loadAllPubTerms', '条款列表', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/loadAllPubTerms', '条款列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1070', 'pubTerm:updatePubTerm', '条款更新', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/updatePubTerm', '条款更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1071', 'pubTerm:addPubTerm', '条款添加', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/createPubTerm', '条款添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1072', 'pubTerm:loadPubTerm', '条款加载', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/loadPubTerm', '条款加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1073', 'pubTerm:loadPubTermDataSet', '条款列表', '', 'p', 'APP_ROOT', '/admin/pub/pubTerm/loadPubTermDataSet', '条款列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1074', 'shop:loadShop', '店铺加载', '', 'p', 'APP_ROOT', '/admin/shop/shop/loadShop', '店铺加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1075', 'shop:updateShop', '店铺更新', '', 'p', 'APP_ROOT', '/admin/shop/shop/updateShop', '店铺更新', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1076', 'shop:deletShop', '店铺删除', '', 'p', 'APP_ROOT', '/admin/shop/shop/deleteShop', '店铺删除', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1077', 'shop:loadShopDataSet', '店铺列表', '', 'p', 'APP_ROOT', '/admin/shop/shop/loadShopDataSet', '店铺列表', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1078', 'shop:addShop', '店铺添加', '', 'p', 'APP_ROOT', '/admin/shop/shop/addShop', '店铺添加', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1079', 'menu:dashboard', '数据总览', '', 'M', 'APP_ROOT', '/dashboard', '数据总览', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1082', 'menu:product', '商品菜单', '', 'M', 'APP_ROOT', '/product', '商品菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1083', 'menu:order', '订单菜单', '', 'M', 'APP_ROOT', '/order', '订单菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1084', 'menu:member', '用户菜单', '', 'M', 'APP_ROOT', '/member', '用户菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1085', 'menu:trading', '交易菜单', '', 'M', 'APP_ROOT', '/trading', '交易菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1086', 'menu:content', '内容菜单', '', 'M', 'APP_ROOT', '/content', '内容菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1087', 'menu:setting', '配置菜单', '', 'M', 'APP_ROOT', '/setting', '配置菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1088', 'menu:category', '分类菜单', '', 'M', 'APP_ROOT', '/category', '分类菜单', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1089', 'product:checkProduct', '商品审核', '', 'p', 'APP_ROOT', '/admin/product/product/checkProduct', '商品审核', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1090', 'product:setProductIsHot', '设置热门商品', '', 'p', 'APP_ROOT', '/admin/product/product/setProductIsHot', '设置热门商品', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1091', 'authUser:createAccount', '创建管理员', '', 'p', 'APP_ROOT', '/auth/authUser/createAccount', '创建管理员账号', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1092', 'orders:loadOrdersDetails', '订单详情加载', '', 'p', 'APP_ROOT', '/orders/orders/loadOrdersDetails', '订单详情加载', '3', '\0');
INSERT INTO `auth_permission` VALUES ('1093', 'product:loadProductDetail', '商品详情', '', 'p', 'APP_ROOT', '/admin/product/product/loadProductDetail', '商品详情', '3', '\0');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleCode` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `enabled` bit(1) NOT NULL DEFAULT b'1',
  `description` varchar(60) DEFAULT NULL,
  `appKey` varchar(40) DEFAULT NULL COMMENT '所属app',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_role_index_code` (`roleCode`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES ('1', 'ROOT', 'ROOT', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('2', 'ADMIN', '管理员', '', '管理员', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('3', 'BUYER', '买家', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('4', 'SELLER', '卖家', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('5', 'CUSTOM_SERVICE', '客服', '', '客服', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('6', 'MEMBER', '会员', '', null, 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('8', 'baba', '爸爸', '', '爸爸', 'APP_ROOT');
INSERT INTO `auth_role` VALUES ('9', 'mama', '妈妈', '', '妈妈', 'APP_ROOT');

-- ----------------------------
-- Table structure for auth_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_permission`;
CREATE TABLE `auth_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth_permission_id` bigint(20) NOT NULL,
  `auth_role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_role_permission_index_permission_id` (`auth_permission_id`),
  KEY `auth_role_permission_index_role_id` (`auth_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_role_permission
-- ----------------------------
INSERT INTO `auth_role_permission` VALUES ('1', '4', '1');
INSERT INTO `auth_role_permission` VALUES ('2', '5', '1');
INSERT INTO `auth_role_permission` VALUES ('3', '40', '1');
INSERT INTO `auth_role_permission` VALUES ('4', '1', '1');
INSERT INTO `auth_role_permission` VALUES ('5', '3', '1');
INSERT INTO `auth_role_permission` VALUES ('6', '6', '1');
INSERT INTO `auth_role_permission` VALUES ('7', '2', '1');
INSERT INTO `auth_role_permission` VALUES ('8', '12', '1');
INSERT INTO `auth_role_permission` VALUES ('9', '42', '1');
INSERT INTO `auth_role_permission` VALUES ('10', '8', '1');
INSERT INTO `auth_role_permission` VALUES ('11', '7', '1');
INSERT INTO `auth_role_permission` VALUES ('12', '11', '1');
INSERT INTO `auth_role_permission` VALUES ('13', '10', '1');
INSERT INTO `auth_role_permission` VALUES ('14', '13', '1');
INSERT INTO `auth_role_permission` VALUES ('15', '9', '1');
INSERT INTO `auth_role_permission` VALUES ('16', '16', '1');
INSERT INTO `auth_role_permission` VALUES ('17', '17', '1');
INSERT INTO `auth_role_permission` VALUES ('18', '41', '1');
INSERT INTO `auth_role_permission` VALUES ('19', '15', '1');
INSERT INTO `auth_role_permission` VALUES ('20', '18', '1');
INSERT INTO `auth_role_permission` VALUES ('21', '14', '1');
INSERT INTO `auth_role_permission` VALUES ('22', '21', '1');
INSERT INTO `auth_role_permission` VALUES ('23', '23', '1');
INSERT INTO `auth_role_permission` VALUES ('24', '25', '1');
INSERT INTO `auth_role_permission` VALUES ('25', '44', '1');
INSERT INTO `auth_role_permission` VALUES ('26', '24', '1');
INSERT INTO `auth_role_permission` VALUES ('27', '19', '1');
INSERT INTO `auth_role_permission` VALUES ('28', '22', '1');
INSERT INTO `auth_role_permission` VALUES ('29', '20', '1');
INSERT INTO `auth_role_permission` VALUES ('30', '32', '1');
INSERT INTO `auth_role_permission` VALUES ('31', '28', '1');
INSERT INTO `auth_role_permission` VALUES ('32', '43', '1');
INSERT INTO `auth_role_permission` VALUES ('33', '27', '1');
INSERT INTO `auth_role_permission` VALUES ('34', '29', '1');
INSERT INTO `auth_role_permission` VALUES ('35', '30', '1');
INSERT INTO `auth_role_permission` VALUES ('36', '31', '1');
INSERT INTO `auth_role_permission` VALUES ('37', '26', '1');
INSERT INTO `auth_role_permission` VALUES ('38', '45', '1');
INSERT INTO `auth_role_permission` VALUES ('39', '37', '1');
INSERT INTO `auth_role_permission` VALUES ('40', '33', '1');
INSERT INTO `auth_role_permission` VALUES ('41', '39', '1');
INSERT INTO `auth_role_permission` VALUES ('42', '38', '1');
INSERT INTO `auth_role_permission` VALUES ('43', '36', '1');
INSERT INTO `auth_role_permission` VALUES ('44', '35', '1');
INSERT INTO `auth_role_permission` VALUES ('45', '34', '1');
INSERT INTO `auth_role_permission` VALUES ('46', '1000', '1');
INSERT INTO `auth_role_permission` VALUES ('47', '1001', '1');
INSERT INTO `auth_role_permission` VALUES ('48', '1002', '1');
INSERT INTO `auth_role_permission` VALUES ('49', '1003', '1');
INSERT INTO `auth_role_permission` VALUES ('50', '1004', '1');
INSERT INTO `auth_role_permission` VALUES ('51', '1005', '1');
INSERT INTO `auth_role_permission` VALUES ('53', '1006', '1');
INSERT INTO `auth_role_permission` VALUES ('54', '1007', '1');
INSERT INTO `auth_role_permission` VALUES ('55', '1008', '1');
INSERT INTO `auth_role_permission` VALUES ('56', '1009', '1');
INSERT INTO `auth_role_permission` VALUES ('57', '1010', '1');
INSERT INTO `auth_role_permission` VALUES ('58', '1011', '1');
INSERT INTO `auth_role_permission` VALUES ('59', '1012', '1');
INSERT INTO `auth_role_permission` VALUES ('60', '1013', '1');
INSERT INTO `auth_role_permission` VALUES ('61', '1014', '1');
INSERT INTO `auth_role_permission` VALUES ('62', '1015', '1');
INSERT INTO `auth_role_permission` VALUES ('63', '1016', '1');
INSERT INTO `auth_role_permission` VALUES ('64', '1017', '1');
INSERT INTO `auth_role_permission` VALUES ('65', '1018', '1');
INSERT INTO `auth_role_permission` VALUES ('66', '1019', '1');
INSERT INTO `auth_role_permission` VALUES ('67', '1020', '1');
INSERT INTO `auth_role_permission` VALUES ('68', '1021', '1');
INSERT INTO `auth_role_permission` VALUES ('69', '1022', '1');
INSERT INTO `auth_role_permission` VALUES ('70', '1023', '1');
INSERT INTO `auth_role_permission` VALUES ('71', '1024', '1');
INSERT INTO `auth_role_permission` VALUES ('72', '1025', '1');
INSERT INTO `auth_role_permission` VALUES ('73', '1026', '1');
INSERT INTO `auth_role_permission` VALUES ('74', '1027', '1');
INSERT INTO `auth_role_permission` VALUES ('75', '1028', '1');
INSERT INTO `auth_role_permission` VALUES ('76', '1029', '1');
INSERT INTO `auth_role_permission` VALUES ('77', '1030', '1');
INSERT INTO `auth_role_permission` VALUES ('78', '1031', '1');
INSERT INTO `auth_role_permission` VALUES ('79', '1032', '1');
INSERT INTO `auth_role_permission` VALUES ('80', '1033', '1');
INSERT INTO `auth_role_permission` VALUES ('81', '1034', '1');
INSERT INTO `auth_role_permission` VALUES ('82', '1035', '1');
INSERT INTO `auth_role_permission` VALUES ('83', '1036', '1');
INSERT INTO `auth_role_permission` VALUES ('84', '1037', '1');
INSERT INTO `auth_role_permission` VALUES ('85', '1038', '1');
INSERT INTO `auth_role_permission` VALUES ('86', '1039', '1');
INSERT INTO `auth_role_permission` VALUES ('87', '1040', '1');
INSERT INTO `auth_role_permission` VALUES ('88', '1041', '1');
INSERT INTO `auth_role_permission` VALUES ('89', '1042', '1');
INSERT INTO `auth_role_permission` VALUES ('90', '1043', '1');
INSERT INTO `auth_role_permission` VALUES ('91', '1044', '1');
INSERT INTO `auth_role_permission` VALUES ('92', '1045', '1');
INSERT INTO `auth_role_permission` VALUES ('93', '1046', '1');
INSERT INTO `auth_role_permission` VALUES ('94', '1047', '1');
INSERT INTO `auth_role_permission` VALUES ('95', '1048', '1');
INSERT INTO `auth_role_permission` VALUES ('96', '1049', '1');
INSERT INTO `auth_role_permission` VALUES ('97', '1050', '1');
INSERT INTO `auth_role_permission` VALUES ('98', '1051', '1');
INSERT INTO `auth_role_permission` VALUES ('99', '1052', '1');
INSERT INTO `auth_role_permission` VALUES ('100', '1053', '1');
INSERT INTO `auth_role_permission` VALUES ('101', '1054', '1');
INSERT INTO `auth_role_permission` VALUES ('102', '1055', '1');
INSERT INTO `auth_role_permission` VALUES ('103', '1056', '1');
INSERT INTO `auth_role_permission` VALUES ('104', '1057', '1');
INSERT INTO `auth_role_permission` VALUES ('105', '1058', '1');
INSERT INTO `auth_role_permission` VALUES ('106', '1059', '1');
INSERT INTO `auth_role_permission` VALUES ('107', '1060', '1');
INSERT INTO `auth_role_permission` VALUES ('108', '1061', '1');
INSERT INTO `auth_role_permission` VALUES ('109', '1062', '1');
INSERT INTO `auth_role_permission` VALUES ('110', '1063', '1');
INSERT INTO `auth_role_permission` VALUES ('111', '1064', '1');
INSERT INTO `auth_role_permission` VALUES ('112', '1065', '1');
INSERT INTO `auth_role_permission` VALUES ('113', '1066', '1');
INSERT INTO `auth_role_permission` VALUES ('114', '1067', '1');
INSERT INTO `auth_role_permission` VALUES ('115', '1068', '1');
INSERT INTO `auth_role_permission` VALUES ('116', '1069', '1');
INSERT INTO `auth_role_permission` VALUES ('117', '1070', '1');
INSERT INTO `auth_role_permission` VALUES ('118', '1071', '1');
INSERT INTO `auth_role_permission` VALUES ('119', '1072', '1');
INSERT INTO `auth_role_permission` VALUES ('120', '1073', '1');
INSERT INTO `auth_role_permission` VALUES ('121', '1074', '1');
INSERT INTO `auth_role_permission` VALUES ('122', '1075', '1');
INSERT INTO `auth_role_permission` VALUES ('123', '1076', '1');
INSERT INTO `auth_role_permission` VALUES ('124', '1077', '1');
INSERT INTO `auth_role_permission` VALUES ('125', '1078', '1');
INSERT INTO `auth_role_permission` VALUES ('126', '1079', '1');
INSERT INTO `auth_role_permission` VALUES ('127', '1082', '1');
INSERT INTO `auth_role_permission` VALUES ('128', '1083', '1');
INSERT INTO `auth_role_permission` VALUES ('129', '1084', '1');
INSERT INTO `auth_role_permission` VALUES ('130', '1085', '1');
INSERT INTO `auth_role_permission` VALUES ('131', '1086', '1');
INSERT INTO `auth_role_permission` VALUES ('132', '1087', '1');
INSERT INTO `auth_role_permission` VALUES ('133', '1044', '5');
INSERT INTO `auth_role_permission` VALUES ('134', '1045', '5');
INSERT INTO `auth_role_permission` VALUES ('135', '1046', '5');
INSERT INTO `auth_role_permission` VALUES ('136', '1047', '5');
INSERT INTO `auth_role_permission` VALUES ('137', '1048', '5');
INSERT INTO `auth_role_permission` VALUES ('138', '1049', '5');
INSERT INTO `auth_role_permission` VALUES ('139', '1050', '5');
INSERT INTO `auth_role_permission` VALUES ('140', '1051', '5');
INSERT INTO `auth_role_permission` VALUES ('141', '1052', '5');
INSERT INTO `auth_role_permission` VALUES ('142', '1053', '5');
INSERT INTO `auth_role_permission` VALUES ('143', '1054', '5');
INSERT INTO `auth_role_permission` VALUES ('144', '1055', '5');
INSERT INTO `auth_role_permission` VALUES ('145', '1056', '5');
INSERT INTO `auth_role_permission` VALUES ('146', '1057', '5');
INSERT INTO `auth_role_permission` VALUES ('147', '1058', '5');
INSERT INTO `auth_role_permission` VALUES ('148', '1059', '5');
INSERT INTO `auth_role_permission` VALUES ('158', '1082', '5');
INSERT INTO `auth_role_permission` VALUES ('159', '1083', '5');
INSERT INTO `auth_role_permission` VALUES ('160', '1089', '1');
INSERT INTO `auth_role_permission` VALUES ('161', '1088', '1');
INSERT INTO `auth_role_permission` VALUES ('162', '1090', '1');
INSERT INTO `auth_role_permission` VALUES ('163', '1091', '1');
INSERT INTO `auth_role_permission` VALUES ('165', '7', '13');
INSERT INTO `auth_role_permission` VALUES ('166', '8', '13');
INSERT INTO `auth_role_permission` VALUES ('169', '1', '13');
INSERT INTO `auth_role_permission` VALUES ('171', '1092', '1');
INSERT INTO `auth_role_permission` VALUES ('172', '1093', '1');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `account` varchar(30) NOT NULL COMMENT '用户名',
  `password` varchar(36) NOT NULL COMMENT '密码',
  `salt` varchar(60) NOT NULL COMMENT '加密盐值',
  `openId` varchar(60) DEFAULT '' COMMENT '微信openid',
  `authLevel` int(1) DEFAULT '2' COMMENT '用户状态_D#0游客(未注册) ，1：注册但未审核，2：正常用户#',
  `defaultRole` bigint(20) DEFAULT NULL COMMENT '默认角色',
  `defaultOrg` varchar(50) DEFAULT NULL COMMENT '默认组织机构',
  `defaultPosition` bigint(20) DEFAULT NULL COMMENT '默认职位',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '用户状态',
  `accountExpired` bit(1) NOT NULL DEFAULT b'0' COMMENT '账户是否过期',
  `accountLocked` bit(1) NOT NULL DEFAULT b'0' COMMENT '账户是否所定',
  `credentialsExpired` bit(1) NOT NULL DEFAULT b'0' COMMENT '密码是否过期',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `perName` varchar(80) DEFAULT NULL COMMENT '姓名',
  `perIdCardNum` varchar(20) DEFAULT NULL COMMENT '证件号码',
  `weichat` varchar(40) DEFAULT NULL COMMENT '微信帐号',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `phoneNum` varchar(15) DEFAULT NULL COMMENT '手机',
  `qqNum` varchar(10) DEFAULT NULL COMMENT 'QQ',
  `weiboNum` varchar(50) DEFAULT NULL COMMENT 'MSN',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_index_user_name` (`account`) USING BTREE,
  KEY `auth_user_index_password` (`password`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES ('1', 'admin', 'e61aab677ca46224d22acbb493247d69', 'a767ec06b36de25389055541a23f630b', '', '2', '1', '10427A08A', '26', '', '\0', '\0', '', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('2', 'test001', '1d958b725884ad926c16e21dc0d8ccaa', '13c468d7fc78a228ceb9cc952dbe176f', '', '2', '15', null, null, '', '\0', '\0', '\0', null, '2017-01-20 10:11:03', null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('3', 'test002', '036927c9c52ea8b13f29cf9c36094d20', '285641464548957Rrrydj4QGiSmyS3CzCSu1L', '', '2', '15', null, null, '', '\0', '\0', '\0', null, '2017-01-20 16:45:39', null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('54', 'zlj', '16e1863f84dab273db4bbb68897e0594', '533be486497e9c6e8ac83dcc4a14d79a', 'o_ZQ4wM5yYa4xRDbyBmh2ESBALmg', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('55', 'quhao', 'd35e1f791f2b00be18b342695154110f', '62dc2ba97bed66278e41cfb15acd9514', 'o_ZQ4wOXyTwTUhMpNbstXwQfEExs', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('56', '中', '3c316ff7a201e55d709bbc518e0c1c10', '552336b1c387baa61949f31d55e84da8', 'o_ZQ4wDp1trl53_La8h-puQywcF4', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_user` VALUES ('57', '15588873006', 'a3094bb3d994c202adb114e359ccfdcb', 'd0cc118eef92fe31de101c30de1326ba', 'o_ZQ4wEnnbeGa55Y6iuGVZSZwVFA', '2', '6', null, null, '', '\0', '\0', '\0', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for auth_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_permission`;
CREATE TABLE `auth_user_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth_permission_id` bigint(20) NOT NULL,
  `auth_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_user_permission_index_auth_permission_id` (`auth_permission_id`),
  KEY `auth_user_permission_index_auth_user_id` (`auth_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_permission
-- ----------------------------
INSERT INTO `auth_user_permission` VALUES ('1', '0', '1');
INSERT INTO `auth_user_permission` VALUES ('2', '1', '1');
INSERT INTO `auth_user_permission` VALUES ('3', '2', '1');
INSERT INTO `auth_user_permission` VALUES ('4', '3', '1');
INSERT INTO `auth_user_permission` VALUES ('5', '4', '1');
INSERT INTO `auth_user_permission` VALUES ('6', '5', '1');
INSERT INTO `auth_user_permission` VALUES ('7', '6', '1');
INSERT INTO `auth_user_permission` VALUES ('8', '7', '1');
INSERT INTO `auth_user_permission` VALUES ('9', '8', '1');
INSERT INTO `auth_user_permission` VALUES ('10', '9', '1');
INSERT INTO `auth_user_permission` VALUES ('11', '10', '1');

-- ----------------------------
-- Table structure for auth_user_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_role`;
CREATE TABLE `auth_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth_role_id` bigint(20) NOT NULL,
  `auth_user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_user_role_index_auth_role_id` (`auth_role_id`) USING BTREE,
  KEY `auth_user_role_index_auth_user_id` (`auth_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_role
-- ----------------------------
INSERT INTO `auth_user_role` VALUES ('5', '1', '1');

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '名称',
  `enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否可用',
  `creater` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `orgTreeCode` varchar(50) DEFAULT NULL COMMENT '所属组织机构',
  `authRoleId` bigint(20) DEFAULT NULL COMMENT '用户角色',
  `remark` varchar(60) DEFAULT NULL COMMENT '备注',
  `code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_position_index_depatment_id` (`orgTreeCode`),
  KEY `sys_position_index_auth_role_id` (`authRoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_position
-- ----------------------------

-- ----------------------------
-- Table structure for sys_position_auth_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_position_auth_user`;
CREATE TABLE `sys_position_auth_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `positionId` bigint(20) NOT NULL COMMENT '所属部门',
  `authUserId` bigint(20) NOT NULL COMMENT '用户',
  PRIMARY KEY (`id`),
  KEY `sys_position_rbac_user_index_position_id` (`positionId`),
  KEY `sys_position_rbac_user_index_auth_user_id` (`authUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_position_auth_user
-- ----------------------------
INSERT INTO `sys_position_auth_user` VALUES ('32', '32', '2610');
INSERT INTO `sys_position_auth_user` VALUES ('33', '32', '2611');

-- ----------------------------
-- Table structure for sys_sync_biz
-- ----------------------------
DROP TABLE IF EXISTS `sys_sync_biz`;
CREATE TABLE `sys_sync_biz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `syncBizName` varchar(30) DEFAULT NULL,
  `syncBizCode` varchar(30) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sync_biz
-- ----------------------------

-- ----------------------------
-- Table structure for sys_sync_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_sync_config`;
CREATE TABLE `sys_sync_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(60) DEFAULT NULL COMMENT '同步表名',
  `setFieldName` varchar(60) DEFAULT NULL COMMENT '字段名',
  `whereFieldName` varchar(60) DEFAULT NULL COMMENT '查询字段',
  `newValue` varchar(60) DEFAULT NULL COMMENT '新值',
  `whereValue` varchar(60) DEFAULT NULL COMMENT '条件值',
  `syncBizId` bigint(20) DEFAULT NULL COMMENT '业务点',
  `enabled` bit(1) DEFAULT NULL COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sync_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_sync_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_sync_task`;
CREATE TABLE `sys_sync_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sybncBizId` bigint(60) DEFAULT NULL COMMENT '所属业务点',
  `createTime` datetime DEFAULT NULL,
  `excuteTimes` int(11) DEFAULT NULL COMMENT '执行次数',
  `isExcuteOver` bit(60) DEFAULT b'0' COMMENT '是否结束',
  `isExcuteSuccess` bit(60) DEFAULT b'0' COMMENT '是否成功',
  `startTime` datetime DEFAULT NULL COMMENT '开始时间',
  `enTime` datetime DEFAULT NULL COMMENT '结束时间',
  `remark` varchar(60) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_sync_task
-- ----------------------------
