package zfes.snowy.projects.demo.model;
import org.zfes.snowy.base.dao.BaseModel;
public class ZfesDemo extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public ZfesDemo() {}
	public ZfesDemo(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->

	 @javax.validation.constraints.NotNull(message="不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="长度不能大于60")
	  private java.lang.String permcode;
	 @javax.validation.constraints.NotNull(message="不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=40,message="长度不能大于40")
	  private java.lang.String name;
	 @javax.validation.constraints.NotNull(message="不能为空")
	
	  private java.lang.Boolean enabled;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=255,message="长度不能大于255")
	  private java.lang.String url;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="长度不能大于60")
	  private java.lang.String description;
	 @javax.validation.constraints.NotNull(message="不能为空")
	
	  private java.lang.Boolean iscommon;
	
	//<-------------------------------------------->
	public void setPermcode(java.lang.String permcode){
		this.permcode=permcode==null?null:permcode.trim();
	}
	public java.lang.String getPermcode(){
		return this.permcode;
	}
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}
	public void setUrl(java.lang.String url){
		this.url=url==null?null:url.trim();
	}
	public java.lang.String getUrl(){
		return this.url;
	}
	public void setDescription(java.lang.String description){
		this.description=description==null?null:description.trim();
	}
	public java.lang.String getDescription(){
		return this.description;
	}
	public void setIscommon(java.lang.Boolean iscommon){
	 this.iscommon=iscommon;
	}
	public java.lang.Boolean getIscommon(){
		return this.iscommon;
	}

}
