package zfes.snowy.projects.demo.controller;

import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.base.web.controller.BaseController;
import zfes.snowy.projects.demo.model.ZfesDemo;
import zfes.snowy.projects.demo.service.IZfesDemoService;
@Controller 
@RequestMapping("/jwt")
public class ZfesDemoController2 extends BaseController {

	private String viewPathPrefix="/zfes/demo";
	
	@Autowired
	private IZfesDemoService zfesDemoService;
	
	@RequestMapping(value={"/",""}, method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public Object index(){
		return forward(viewPathPrefix,"zfesDemo.html");
    }
	
	//@RequiresPermissions(value="zfesDemo:loadZfesDemoDataSet",desc="示例模型列表")
    @RequestMapping(value="/loadZfesDemoDataSet", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadZfesDemoDataSet(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  zfesDemoService.loadZfesDemoDataSet(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="zfesDemo:addZfesDemo",desc="示例模型添加")
    @RequestMapping(value="/addZfesDemo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addZfesDemo(ZfesDemo zfesDemo){
		zfesDemoService.createZfesDemo(zfesDemo);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="zfesDemo:loadZfesDemo",desc="示例模型加载")
    @RequestMapping(value="/loadZfesDemo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadZfesDemo(Long id){
		Optional<ZfesDemo> ops = zfesDemoService.loadZfesDemoById(id);
		return ajaxQuery("zfesDemo",ops);
    }
	
	@RequiresPermissions(value="zfesDemo:updateZfesDemo",desc="示例模型更新")
    @RequestMapping(value="/updateZfesDemo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateZfesDemo(ZfesDemo zfesDemo){
		zfesDemoService.updateZfesDemo(zfesDemo);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="zfesDemo:deletZfesDemo",desc="示例模型删除")
    @RequestMapping(value="/deleteZfesDemo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deleteZfesDemo(Long id){
		zfesDemoService.deleteZfesDemo(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

}