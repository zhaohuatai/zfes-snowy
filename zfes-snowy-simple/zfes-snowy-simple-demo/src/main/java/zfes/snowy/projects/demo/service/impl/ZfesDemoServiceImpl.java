package zfes.snowy.projects.demo.service.impl;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
//import org.zfes.snowy.base.service.BaseService;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.base.datasource.TargetDataSource;

import zfes.snowy.projects.demo.service.IZfesDemoService;
import zfes.snowy.projects.demo.dao.ZfesDemoMapper;
import zfes.snowy.projects.demo.model.ZfesDemo;
//extends BaseService 
@Service
public class ZfesDemoServiceImpl implements IZfesDemoService{
 	@Autowired
	private ZfesDemoMapper zfesDemoMapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createZfesDemo(ZfesDemo zfesDemo) {
		ZBeanUtil.validateBean(zfesDemo);
		zfesDemo.setId(IDGenerator.genLongId());
		zfesDemoMapper.insertSelective(zfesDemo);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<ZfesDemo> loadZfesDemoById(Long id) {
		if(id!=null){
			return Optional.ofNullable(zfesDemoMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateZfesDemo(ZfesDemo zfesDemo) {
		ZBeanUtil.validateBean(zfesDemo);
		zfesDemoMapper.updateByPrimaryKeySelective(zfesDemo);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deleteZfesDemo(Long id) {
		if(id!=null){
			zfesDemoMapper.deleteByPrimaryKey(id);
			return;
		}
		ZAlert.Error("请选择要删除的数据");
		
	}
	@TargetDataSource(name="ds1")
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadZfesDemoDataSet(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		DataSet ds=DataSet.newDS(zfesDemoMapper.selectListCount(pm), zfesDemoMapper.selectList(pm));
		return ds;
	}
	
}