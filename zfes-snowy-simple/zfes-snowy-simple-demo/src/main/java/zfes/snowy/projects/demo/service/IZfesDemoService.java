package zfes.snowy.projects.demo.service;

import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import zfes.snowy.projects.demo.model.ZfesDemo;
public interface IZfesDemoService{

	 Optional<ZfesDemo> loadZfesDemoById(Long id);
	
	 void createZfesDemo(ZfesDemo zfesDemo);
	
	 void updateZfesDemo(ZfesDemo zfesDemo);

	 void deleteZfesDemo(Long id);
	
	 DataSet loadZfesDemoDataSet(Map<String, Object> params);
	 

}