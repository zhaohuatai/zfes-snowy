package zfes.snowy.projects.ss.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.zfes.snowy.ss.config.configrations.WebMvcConfiguration;

@Configuration
public class CustomWebMvcConfiguration extends WebMvcConfiguration{
   
    /**
     * 跨域配置
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
    	System.out.println("CustomWebMvcConfiguration--addCorsMappings");
        registry.addMapping("/api/**").allowedOrigins("http://localhost:8080");
        super.addCorsMappings(registry);
    }

}
