package zfes.snowy.projects.demo.dao;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import zfes.snowy.projects.demo.model.ZfesDemo;
@Mapper
public interface ZfesDemoMapper extends BaseMapper<ZfesDemo>{
	
	Long selectListCount(ParamMap params);
	
	List<Map<String, Object>> selectList(ParamMap params);
	
	
}
