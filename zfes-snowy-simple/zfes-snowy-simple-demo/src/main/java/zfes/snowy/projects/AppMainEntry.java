package zfes.snowy.projects;


import javax.security.auth.message.config.AuthConfigFactory;
import org.apache.catalina.authenticator.jaspic.AuthConfigFactoryImpl;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.zfes.snowy.ss.config.dynamicds.DynamicDataSourceRegister;
@EnableAsync
@EnableScheduling
@EnableCaching
@EnableAutoConfiguration
@ServletComponentScan
@MapperScan(annotationClass=Mapper.class,basePackages={"zfes.snowy.projects","org.zfes.snowy"})
@ComponentScan(basePackages = {"zfes.snowy.projects","org.zfes.snowy"})
@EnableConfigurationProperties
@Import({DynamicDataSourceRegister.class})
public class AppMainEntry{

    public static void main(String[] args) throws Exception {
	  	  if (AuthConfigFactory.getFactory() == null) {
	      AuthConfigFactory.setFactory(new AuthConfigFactoryImpl());
	  }
	SpringApplication.run(AppMainEntry.class, args);
}
    
}
