package zfes.snowy.projects.ss.config;


import javax.servlet.DispatcherType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.zfes.snowy.ss.config.configrations.WebRegisteConfiguration;
import org.zfes.snowy.ss.config.properties.ShiroConfigProperties;
@Configuration
public class CustomWebRegisteConfiguration extends WebRegisteConfiguration{
	@Autowired
	private  ShiroConfigProperties shiroConfigProperties; 
	  
	@Bean  
	public FilterRegistrationBean filterRegistrationBean() {  
	   FilterRegistrationBean filterRegistration = new FilterRegistrationBean();  
	   filterRegistration.setFilter(new DelegatingFilterProxy("shiroFilter"));   
	   filterRegistration.setEnabled(true);  
	   filterRegistration.addUrlPatterns(shiroConfigProperties.getUrlPatterns());
	   filterRegistration.setAsyncSupported(shiroConfigProperties.getAsyncSupported());
	   filterRegistration.setDispatcherTypes(DispatcherType.REQUEST,DispatcherType.ASYNC);  
	   return filterRegistration;  
	}  
	
	 //@Bean  
	 public ServletRegistrationBean testServletRegistration() {  
	    ServletRegistrationBean  registration = new ServletRegistrationBean();  
	    registration.addUrlMappings("/");  
	    return registration;  
	}  
	  
}
