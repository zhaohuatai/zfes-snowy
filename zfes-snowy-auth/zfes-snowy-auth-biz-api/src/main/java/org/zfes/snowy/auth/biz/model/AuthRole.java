/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.zfes.snowy.base.dao.BaseModel;
public class AuthRole extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthRole() {}
	public AuthRole(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	@NotNull(message="角色编码不能为空")
	@javax.validation.constraints.NotBlank(message="角色编码不能为空")
	@Length(max=40,min=1,message="角色编码长度不能超过40")
	private java.lang.String roleCode;
	
//	@javax.validation.constraints.NotNull(message="所属APP不能为空")
//	private java.lang.Long appId;
	@javax.validation.constraints.NotNull(message="所属应用不能为空")
	private java.lang.String appKey;
	
	@NotNull(message="角色名称不能为空")
	@javax.validation.constraints.NotBlank(message="角色名称不能为空")
	@Length(max=40,min=1,message="角色名称长度不能超过40")
	private java.lang.String name;
	
	@NotNull(message="角色状态不能为空")
	private java.lang.Boolean enabled;
	
	@Length(max=60,min=0,message="角色描述长度不能超过60")
	private java.lang.String description;
	//<-------------------------------------------->
	public void setRoleCode(java.lang.String roleCode){
		this.roleCode=roleCode==null?null:roleCode.trim();
	}
	public java.lang.String getRoleCode(){
		return this.roleCode;
	}
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}
	public void setDescription(java.lang.String description){
		this.description=description==null?null:description.trim();
	}
	public java.lang.String getDescription(){
		return this.description;
	}
//	public java.lang.Long getAppId() {
//		return appId;
//	}
//	public void setAppId(java.lang.Long appId) {
//		this.appId = appId;
//	}
	public java.lang.String getAppKey() {
		return appKey;
	}
	public void setAppKey(java.lang.String appKey) {
		this.appKey=appKey==null?null:appKey.trim();
	}

}
