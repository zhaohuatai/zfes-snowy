/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import java.util.Date;

import org.zfes.snowy.base.dao.BaseModel;

public class AuthUser extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthUser() {}
	public AuthUser(Long id) {
		this.setId(id);
	}
	
	//<-------------------------------------------->
	public static AuthUser  newDeafultAuthUser(String account,  String password, String salt, Long defaultRole) {
		AuthUser user=new AuthUser( account,   password,  salt,  defaultRole,null,  null,  true,  false,  false, false ) ;
		return user;
}
	 public static AuthUser  newDeafultAuthUser(String userName,  String password, String salt, Long defaultRole,Long defaultPosition, String defaultOrg ) {
			AuthUser user=new AuthUser( userName,   password,  salt,  defaultRole,defaultPosition,  defaultOrg,  true,  false,  false, false ) ;
			return user;
	}
	 
	 public AuthUser(String account,  String password, String salt, 
			 Long defaultRole,Long defaultPosition, String defaultOrg, 
			 Boolean enabled, Boolean accountExpired, Boolean accountLocked,
				Boolean credentialsExpired ) {
			super();
			this.account = account;
			this.password = password;
			this.salt = salt;
			this.defaultRole = defaultRole;
			this.defaultPosition = defaultPosition;
			this.defaultOrg = defaultOrg;
			this.enabled = enabled;
			this.accountExpired = accountExpired;
			this.accountLocked = accountLocked;
			this.credentialsExpired = credentialsExpired;
	}
	 
	 public AuthUser(String userName,  String password, String salt, Long defaultRole,
			Long defaultPosition, String defaultOrg, Boolean enabled, Boolean accountExpired, Boolean accountLocked,
			Boolean credentialsExpired, String description,  Date createTime) {
		super();
		this.account = userName;
		this.password = password;
		this.salt = salt;
		this.defaultRole = defaultRole;
		this.defaultPosition = defaultPosition;
		this.defaultOrg = defaultOrg;
		this.enabled = enabled;
		this.accountExpired = accountExpired;
		this.accountLocked = accountLocked;
		this.credentialsExpired = credentialsExpired;
		this.description = description;
		this.createTime = createTime;
	}

	@javax.validation.constraints.NotNull(message="用户名不能为空")
	 @javax.validation.constraints.NotBlank(message="用户名不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="用户名长度不能大于30")
	 private java.lang.String account;
	
	/**
	 * openid
	 */
	private String openId;
	/**
	 * 用户状态_D#0游客(未注册) ，1：注册但未审核，2：正常用户#
	 */
	private Integer authLevel;
	
	 
	 @javax.validation.constraints.NotNull(message="密码不能为空")
	 @javax.validation.constraints.NotBlank(message="密码不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=36,message="密码长度不能大于36")
	 private java.lang.String password;

	 @javax.validation.constraints.NotNull(message="加密盐值不能为空")
	 @javax.validation.constraints.NotBlank(message="加密盐值不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="加密盐值长度不能大于60")
	 private java.lang.String salt;
	
	 @javax.validation.constraints.NotNull(message="默认角色不能为空")
	 private java.lang.Long defaultRole;
	 private java.lang.Long defaultPosition;
	 private java.lang.String defaultOrg;
	 
	 @javax.validation.constraints.NotNull(message="用户状态不能为空")
	 private java.lang.Boolean enabled;
	
	 @javax.validation.constraints.NotNull(message="账户是否过期不能为空")
	 private java.lang.Boolean accountExpired;
	
	 @javax.validation.constraints.NotNull(message="账户是否所定不能为空")
	 private java.lang.Boolean accountLocked;
	
	 @javax.validation.constraints.NotNull(message="密码是否过期不能为空")
	 private java.lang.Boolean credentialsExpired;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=100,message="描述长度不能大于100")
	 private java.lang.String description;
	
	 private java.util.Date createTime;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=100,message="姓名长度不能大于80")
	 private java.lang.String perName;	
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="证件号码长度不能大于30")
	 private java.lang.String perIdCardNum;
	 @org.hibernate.validator.constraints.Length(min=0,max=40,message="微信帐号长度不能大于40")
	 private java.lang.String weichat;
	 @org.hibernate.validator.constraints.Length(min=0,max=50,message="电子邮箱长度不能大于50")
	 private java.lang.String email;
	 @org.hibernate.validator.constraints.Length(min=0,max=15,message="手机长度不能大于15")
	 private java.lang.String phoneNum;
	 @org.hibernate.validator.constraints.Length(min=0,max=10,message="QQ长度不能大于10")
	 private java.lang.String qqNum;
	 @org.hibernate.validator.constraints.Length(min=0,max=50,message="MSN长度不能大于50")
	 private java.lang.String weiboNum;
	 
	//<-------------------------------------------->
	public void setAccount(java.lang.String account){
		this.account=account==null?null:account.trim();
	}
	public java.lang.String getAccount(){
		return this.account;
	}
	public void setPassword(java.lang.String password){
		this.password=password==null?null:password.trim();
	}
	public java.lang.String getPassword(){
		return this.password;
	}
	
	public void setSalt(java.lang.String salt){
		this.salt=salt==null?null:salt.trim();
	}
	public java.lang.String getSalt(){
		return this.salt;
	}
	
	public java.lang.Long getDefaultRole() {
		return defaultRole;
	}
	public void setDefaultRole(java.lang.Long defaultRole) {
		this.defaultRole = defaultRole;
	}
	public void setDefaultPosition(java.lang.Long defaultPosition){
	 this.defaultPosition=defaultPosition;
	}
	public java.lang.Long getDefaultPosition(){
		return this.defaultPosition;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}
	public void setAccountExpired(java.lang.Boolean accountExpired){
	 this.accountExpired=accountExpired;
	}
	public java.lang.Boolean getAccountExpired(){
		return this.accountExpired;
	}
	public void setAccountLocked(java.lang.Boolean accountLocked){
	 this.accountLocked=accountLocked;
	}
	public java.lang.Boolean getAccountLocked(){
		return this.accountLocked;
	}
	public void setCredentialsExpired(java.lang.Boolean credentialsExpired){
	 this.credentialsExpired=credentialsExpired;
	}
	public java.lang.Boolean getCredentialsExpired(){
		return this.credentialsExpired;
	}
	public void setDescription(java.lang.String description){
		this.description=description==null?null:description.trim();
	}
	public java.lang.String getDescription(){
		return this.description;
	}
	
	public void setCreateTime(java.util.Date createTime){
	 this.createTime=createTime;
	}
	public java.util.Date getCreateTime(){
		return this.createTime;
	}
	public java.lang.String getDefaultOrg() {
		return defaultOrg;
	}
	public void setDefaultOrg(java.lang.String defaultOrg) {
		this.defaultOrg = defaultOrg;
	}
	public java.lang.String getPerName() {
		return perName;
	}
	public void setPerName(java.lang.String perName) {
		this.perName = perName;
	}
	public java.lang.String getPerIdCardNum() {
		return perIdCardNum;
	}
	public void setPerIdCardNum(java.lang.String perIdCardNum) {
		this.perIdCardNum = perIdCardNum;
	}
	public java.lang.String getWeichat() {
		return weichat;
	}
	public void setWeichat(java.lang.String weichat) {
		this.weichat = weichat;
	}
	public java.lang.String getEmail() {
		return email;
	}
	public void setEmail(java.lang.String email) {
		this.email = email;
	}
	public java.lang.String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(java.lang.String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public java.lang.String getQqNum() {
		return qqNum;
	}
	public void setQqNum(java.lang.String qqNum) {
		this.qqNum = qqNum;
	}
	public java.lang.String getWeiboNum() {
		return weiboNum;
	}
	public void setWeiboNum(java.lang.String weiboNum) {
		this.weiboNum = weiboNum;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public Integer getAuthLevel() {
		return authLevel;
	}
	public void setAuthLevel(Integer authLevel) {
		this.authLevel = authLevel;
	}
	
	
}
