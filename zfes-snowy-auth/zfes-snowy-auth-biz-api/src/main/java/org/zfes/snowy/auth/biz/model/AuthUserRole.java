/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;
import org.zfes.snowy.base.dao.BaseModel;
public class AuthUserRole extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthUserRole() {}
	public AuthUserRole(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->

	 public AuthUserRole(Long id,Long authRoleId, Long authUserId) {
		super();
		this.setId(id);
		this.authRoleId = authRoleId;
		this.authUserId = authUserId;
	}

	@javax.validation.constraints.NotNull(message="不能为空")
	 @javax.validation.constraints.NotBlank(message="不能为空")
	  private java.lang.Long authRoleId;
	 @javax.validation.constraints.NotNull(message="不能为空")
	 @javax.validation.constraints.NotBlank(message="不能为空")
	
	  private java.lang.Long authUserId;
	
	//<-------------------------------------------->
	public void setAuthRoleId(java.lang.Long authRoleId){
	 this.authRoleId=authRoleId;
	}
	public java.lang.Long getAuthRoleId(){
		return this.authRoleId;
	}
	public void setAuthUserId(java.lang.Long authUserId){
	 this.authUserId=authUserId;
	}
	public java.lang.Long getAuthUserId(){
		return this.authUserId;
	}

}
