/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.util;


import org.zfes.snowy.auth.biz.model.AuthPermission;
import org.zfes.snowy.core.util.ZAssert;

public class AuthPermissionUtil {

	public static void validateUpdate(AuthPermission authPermission){
		ZAssert.notNull(authPermission.getId(), "权限ID不能为空");
		validateCreate(authPermission);
	}
	public static void validateCreate(AuthPermission authPermission){
		ZAssert.notNull(authPermission, "提交参数为空");
		ZAssert.notNull(authPermission.getPermCode(), "权限编码不能为空");
		ZAssert.notNull(authPermission.getName(), "权限名称不能为空");
		ZAssert.notNull(authPermission.getEnabled(), "权限状态不能为空");
	}
    public static void main(String[] args) {    
    
    
    }   
    
}
