/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.auth.biz.model.AuthGroup;
import org.zfes.snowy.core.data.DataSet;
public interface IAuthGroupService{

	public Optional<AuthGroup> loadAuthGroup(Long id);
		
	public void createAuthGroup(AuthGroup authGroup);
	
	public void updateAuthGroup(AuthGroup authGroup);

	public void deletAuthGroup(Long id);
	
	
	void setAuthGroupEnabled(List<Long> ids, Boolean enabled);
	
	
	
	/**
	 * @param  params : { name | Str | 名称 | like , enabled | Boolean |可用状态 | =  , parentId |Long|父ID| =  }
	 * @return
	 */
	public DataSet loadAuthGroupList(Map<String, Object> params);
	
	 

}