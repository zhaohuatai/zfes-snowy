/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;

public interface IAuthGroupUserService{

    List<Long> loadGroupIdsByUserId(Long userId,Boolean enabled);
    
    List<Long> loadUserIdsByGroupId(Long groupId,Boolean enabled);
	
   void addUserToGroupUser(List<Long> userIds,Long groupId);
   
   void removeUserFromGroupUser(List<Long> userIds,Long groupId);

}