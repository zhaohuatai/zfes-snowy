/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;

public class AuthRolePermission extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthRolePermission() {}
	
	public AuthRolePermission(Long id) {
		this.setId(id);
	}
	
	public AuthRolePermission(Long id,Long authPermissionId, Long authRoleId) {
		this.setId(id);
		this.authPermissionId = authPermissionId;
		this.authRoleId = authRoleId;
	}


	//<-------------------------------------------->
	private java.lang.Long authPermissionId;
	private java.lang.Long authRoleId;

	public java.lang.Long getAuthPermissionId() {
		return authPermissionId;
	}
	public void setAuthPermissionId(java.lang.Long authPermissionId) {
		this.authPermissionId = authPermissionId;
	}
	public java.lang.Long getAuthRoleId() {
		return authRoleId;
	}
	public void setAuthRoleId(java.lang.Long authRoleId) {
		this.authRoleId = authRoleId;
	}
	
	//<-------------------------------------------->


}
