/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.util;
import java.math.BigInteger;
import java.util.Map;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZStrUtil;
public class SysOrganizationUtil {
//	public static Long genOrgIsCultivateMappingCode(String isCultivateUnit){
//		long mappingcode = 0L;
//       if(AppBizConst.isCultivateUnit.isCultivateUnit.equals(isCultivateUnit)){
//    	   mappingcode=1L;
//       }		
//		return mappingcode;
//	
//	}

	public static BigInteger genOrgantionMappingCode(Map<String, Object> params) {
		int n = 0;
		String str = String.format("%060d", n);
		String sequences=(String) params.get("sequence");
		BigInteger mappCode=genBigInteger(str,sequences);
		return mappCode;
	}

	private static BigInteger genBigInteger(String str, String sequences) {
		
		if(ZStrUtil.hasText(sequences)){
			StringBuilder sb = new StringBuilder(str);
			String[] seqTemp=sequences.split(",");
			for(String seq :seqTemp){
				sb=changBinNumber(sb,Integer.parseInt(seq));
			}
			String abcStr = sb.toString();
		   BigInteger srcb= new BigInteger(abcStr,2);
		   return srcb;
			
		}
		return null;
		
	}

	private static StringBuilder changBinNumber(StringBuilder sb, int bitNum) {
		// TODO Auto-generated method stub
		if(bitNum > 60){
			ZAlert.Error("组织机构最多支持60位类型");
		}
		if(!ZObjectUtil.isEmpty(sb)){
			sb=sb.replace(60-bitNum, 60-bitNum+1, "1");
		}
		return sb;
	}

	
}
