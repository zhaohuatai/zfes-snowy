/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;

public class AuthGroupUser extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthGroupUser() {}
	public AuthGroupUser(Long id) {
		this.setId(id);
	}
	public AuthGroupUser(Long id,Long authGroupId, Long authUserId) {
		super();
		this.setId(id);
		this.authGroupId = authGroupId;
		this.authUserId = authUserId;
	}
	//<-------------------------------------------->
	private java.lang.Long authGroupId;
	private java.lang.Long authUserId;
	
	//<-------------------------------------------->
	public void setAuthGroupId(java.lang.Long authGroupId){
	 this.authGroupId=authGroupId;
	}
	public java.lang.Long getAuthGroupId(){
		return this.authGroupId;
	}
	public void setAuthUserId(java.lang.Long authUserId){
	 this.authUserId=authUserId;
	}
	public java.lang.Long getAuthUserId(){
		return this.authUserId;
	}

}
