/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.auth.biz.model.AuthMenu;
import org.zfes.snowy.auth.biz.view.MenuView;
public interface IAuthMenuService{
	
	 Optional<MenuView> loadUserMenus(Long userId,String appkey,Boolean isLoadFromAllRole);

	/**
	 * 
	 * @param params ={display: like}
	 * @return
	 */
	 DataSet loadAuthMenuTreeGrid(String appKey,Map<String, Object> params);
	
	 List<Map<String, Object>> loadAuthMenuTree(Map<String, Object> params);
	
   /**
    * 菜单添加
    * @param authMenu
    */
	 void createAuthMenu(AuthMenu authMenu);
    /**
     * 根据Id 查询菜单信息
     * @param id
     * @return
     */
     Optional<AuthMenu> loadAuthMenuById(Long id);
    /**
     * 更新菜单信息
     * @param authMenu
     */

	 void updateAuthMenu(AuthMenu authMenu);

   
	 void setAuthMenuEnabled(List<Long> ids, Boolean enabled);
	 
	 
	 void batchRemoveAuthMenu(List<Long> ids);
	 
	 void removeAuthMenu(Long id);
	
}