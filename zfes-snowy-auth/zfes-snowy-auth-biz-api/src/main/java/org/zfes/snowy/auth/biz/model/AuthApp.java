/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;
public class AuthApp extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthApp() {}
	public AuthApp(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="应用标识长度不能大于60")
	  private java.lang.String appKey;
	 
	 @javax.validation.constraints.NotNull(message="应用名称不能为空")
	 @javax.validation.constraints.NotBlank(message="应用名称不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=40,message="应用名称长度不能大于40")
	  private java.lang.String name;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=20,message="图标长度不能大于20")
	  private java.lang.String icon;
	 
	 @javax.validation.constraints.NotNull(message="顺序不能为空")
	  private java.lang.Integer disIndex;
	 
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="描述长度不能大于60")
	  private java.lang.String remark;
	 
	 @org.hibernate.validator.constraints.Length(min=0,max=300,message="应用域名不能大于300")
	  private java.lang.String appDomain;
		
	 @javax.validation.constraints.NotNull(message="APP状态不能为空")
	 private java.lang.Boolean enabled;
	//<-------------------------------------------->
	public void setAppKey(java.lang.String appKey){
		this.appKey=appKey==null?null:appKey.trim();
	}
	public java.lang.String getAppKey(){
		return this.appKey;
	}
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setIcon(java.lang.String icon){
		this.icon=icon==null?null:icon.trim();
	}
	public java.lang.String getIcon(){
		return this.icon;
	}
	public void setDisIndex(java.lang.Integer disIndex){
	 this.disIndex=disIndex;
	}
	public java.lang.Integer getDisIndex(){
		return this.disIndex;
	}
	public void setRemark(java.lang.String remark){
		this.remark=remark==null?null:remark.trim();
	}
	public java.lang.String getRemark(){
		return this.remark;
	}
	public java.lang.String getAppDomain() {
		return appDomain;
	}
	public void setAppDomain(java.lang.String appDomain) {
		this.appDomain = appDomain;
	}
	public java.lang.Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(java.lang.Boolean enabled) {
		this.enabled = enabled;
	}
	

}
