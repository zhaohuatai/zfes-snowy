/**

 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */

package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.zfes.snowy.auth.biz.model.AuthPermission;
import org.zfes.snowy.core.data.DataSet;

/**
 * 
* @ClassName :IRbacPermissionService     
* @Description :   
* @createTime :2015年4月3日  下午4:36:57   
* @author ：zhaohuatai   
* @version :1.0
 */
public interface IAuthPermissionService {
		 
//--------------------------------basic ---------------------------------------	
	 void createAuthPermission(AuthPermission authPermission);
	 
	 void updateAuthPermission(AuthPermission authPermission);
	 
	 void deletAuthPermission(Long id);
	 
//	 void batchDeletePermission(List<Long> permissionIds);
	
	 Optional<AuthPermission> loadAuthPermissionById(Long id);
	
	 Optional<AuthPermission>  loadAuthPermissionByCode(String permCode);
	
	/**	
	 * @param params={ name:(like), permCode:(like), enabled:(=),appId:(=), moduleId:(=)}
	 * @return
	 */
	DataSet loadAuthPermissionDataSet(String appKey , Map<String, Object> params);
    
    List<AuthPermission> loadPermListByIds(List<Long> permsIdList);
    
    List<AuthPermission> loadPermListByCodes(List<String> permsCodeList);
    
    /**
     * 更新权限的状态
     * @param id : 权限Id
     * @param enabled : 是否可用  0：不可用  1：可用
     */
	 void setPermissionEnabled(List<Long> ids, Boolean enabled);
	 
//	 void batchDeleteAuthPermission(List<Long> ids);

}
