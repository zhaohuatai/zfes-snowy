/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.ComboboVo;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.auth.biz.model.AuthModule;
public interface IAuthModuleService{

	 Optional<AuthModule> loadAuthModule(Long id);
		
	 void createAuthModule(AuthModule authModule);
	
	 void updateAuthModule(AuthModule authModule);

	 void deletAuthModule(Long id);
	
	 void setAuthModuleEnabled(List<Long> ids, Boolean enabled);
	
	 List<ComboboVo> loadModuleComboboVo(String appKey,Boolean firstEmpty);
	
	/**
	 * @param params: {name like , enabled = , parentId = , appId = }
	 * @return
	 */
	 DataSet loadAuthModuleList(String appKey,Map<String, Object> params);

}