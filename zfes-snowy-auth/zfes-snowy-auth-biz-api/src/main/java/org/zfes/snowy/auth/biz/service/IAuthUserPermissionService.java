/**

 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */

package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;

import org.zfes.snowy.core.data.DataSet;

  /**
    * 负责UserPermission  ， UserPermissionReject
	* @ClassName :IRbacPermissionService     
	* @Description :   
	* @createTime :2015年4月3日  下午4:36:57   
	* @author ：zhaohuatai   
	* @version :1.0
	*/
public interface IAuthUserPermissionService {
	 
//--------------------------------user----perm--------------------------------------------	
	  
	  List<String> loadPermCodesInUserPermByUserId(Long userId,String appkey,Boolean enabled);
	
	  List<Long> loadPermIdsInUserPermByUserId(Long userId,String appkey, Boolean enabled);
	 
	  void  addPermsToUserPerm(List<Long> permissionIds,Long userId);
		 
	  void  removePermsFromUserPerm(List<Long> permissionIds,Long userId);

	  /**
	   * 
	   * @param isInUser
	   * @param params {String appKey*, Long moduleId, String permCode,String name, Long userId*}
	   * @return
	   */
	  DataSet loadPermDataSetForUserAssign(Boolean isInUser ,Map<String, Object> params ) ;
}
