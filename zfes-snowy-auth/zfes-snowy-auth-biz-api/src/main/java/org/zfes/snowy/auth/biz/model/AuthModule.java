/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;
import org.zfes.snowy.base.dao.BaseModel;
public class AuthModule extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthModule() {}
	public AuthModule(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	 @javax.validation.constraints.NotNull(message="模块名称不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=40,message="模块名称长度不能大于40")
	 private java.lang.String name;
	 @javax.validation.constraints.NotNull(message="模块状态不能为空")
	 private java.lang.Boolean enabled;
	 @org.hibernate.validator.constraints.Length(min=0,max=20,message="图标长度不能大于20")
	 private java.lang.String icon;
	 @javax.validation.constraints.NotNull(message="顺序不能为空")
	 private java.lang.Integer disIndex;
	 
//	 @javax.validation.constraints.NotNull(message="所属APP不能为空")
//	 private java.lang.Long appId;
	
	@javax.validation.constraints.NotNull(message="所属应用不能为空")
	private java.lang.String appKey;
		
	 @javax.validation.constraints.NotNull(message="父模块不能为空")
	 private java.lang.Long parentId;
	 
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="编码长度不能大于30")
	 private java.lang.String code;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="描述长度不能大于30")
	 private java.lang.String remark;
	
	//<-------------------------------------------->
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}
	public void setIcon(java.lang.String icon){
		this.icon=icon==null?null:icon.trim();
	}
	public java.lang.String getIcon(){
		return this.icon;
	}
	public void setRemark(java.lang.String remark){
		this.remark=remark==null?null:remark.trim();
	}
	public java.lang.String getRemark(){
		return this.remark;
	}
	 public java.lang.String getCode() {
			return code;
		}
	public void setCode(java.lang.String code) {
			this.code = code;
	}
	public java.lang.Integer getDisIndex() {
		return disIndex;
	}
	public void setDisIndex(java.lang.Integer disIndex) {
		this.disIndex = disIndex;
	}
//	public java.lang.Long getAppId() {
//		return appId;
//	}
//	public void setAppId(java.lang.Long appId) {
//		this.appId = appId;
//	}
	public java.lang.Long getParentId() {
		return parentId;
	}
	public void setParentId(java.lang.Long parentId) {
		this.parentId = parentId;
	}
	public java.lang.String getAppKey() {
		return appKey;
	}
	public void setAppKey(java.lang.String appKey) {
		this.appKey=appKey==null?null:appKey.trim();
	}

}
