/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.util;

import java.util.Collections;
import java.util.List;

import org.zfes.snowy.auth.biz.model.AuthMenu;
import org.zfes.snowy.auth.biz.model.AuthPermission;
import org.zfes.snowy.auth.biz.view.MenuView;
import org.zfes.snowy.core.exceptions.ServiceLogicalException;
import com.google.common.collect.Lists;




public class MenuUtil {

	public static AuthMenu findRootMenu(List<AuthMenu> allMenus){
		if(allMenus!=null&&allMenus.size()>0){
			for(AuthMenu menu:allMenus){
				if(menu.getParentId()==-1){
					return menu ;
				}
			}
		}
		return null;
	}
	
	public static List<AuthMenu>  findChildMeus(AuthMenu target,List<AuthMenu> allMenus){
		List<AuthMenu> childMenus=Lists.newArrayList();
		if(target==null||allMenus==null||allMenus.size()==0){
			return childMenus;
		}
		for(AuthMenu menu:allMenus){
			if(menu.getParentId()==target.getId()||menu.getParentId().equals(target.getId())){
				childMenus.add(menu);
			}
		}
		return childMenus;
	}
	
	public static AuthPermission findPermission(AuthMenu target,List<AuthPermission> permList){
		if(target==null||permList==null||permList.size()==0){
			return null;
		}
		for(AuthPermission perm:permList){
			if(target.getPermissionId()==null||perm.getId()==null){
				continue;
			}
			if(target.getPermissionId().equals(perm.getId())){
				return perm;
			}
		}
		return null;
	}
	

	private static void isCanBeAddToTree(AuthMenu authMenu,List<AuthPermission> permList,List<AuthMenu> allMenus){
		if(findPermission(authMenu, permList)!=null){
			throw new ServiceLogicalException("");
		}
		List<AuthMenu> childMenus= findChildMeus( authMenu, allMenus);
		if(childMenus!=null&&!childMenus.isEmpty()){
			for (AuthMenu childAuthMenu : childMenus){
				isCanBeAddToTree( childAuthMenu,permList, allMenus);
			}
		}
	}
	public static void traverseMenuTree(AuthMenu rootMenu,MenuView rootViewNode,List<AuthPermission> permList,List<AuthMenu> allMenus) {
		rootViewNode.setId(rootMenu.getId());
		rootViewNode.setTitle(rootMenu.getName());
		rootViewNode.setAppKey(rootMenu.getAppKey());
		rootViewNode.setUrl(rootMenu.getPermissionUrl());
		rootViewNode.setIcon(rootMenu.getIconcls());
		
		AuthPermission permssion=findPermission(rootMenu,permList);
		if(permssion!=null){
			rootViewNode.setUrl(permssion.getUrl());
		}
		List<AuthMenu> childMenus=findChildMeus(rootMenu, allMenus);
		if (childMenus == null || childMenus.isEmpty()) {
			return ;
		}
		for (AuthMenu childAuthMenu : childMenus) {
			try{
				isCanBeAddToTree( childAuthMenu, permList, allMenus);
			}catch(ServiceLogicalException e){
				MenuView menuNode=new MenuView();
				menuNode.setParentId(rootViewNode.getId());
				menuNode.setAppKey(childAuthMenu.getAppKey());
				rootViewNode.addChild(menuNode);
				traverseMenuTree(childAuthMenu,menuNode,permList,allMenus);
			}
		}
		
	}
	
	public static List<MenuView> findMenuItems(Long parentMenuId,List<AuthMenu> allMenus){
		if(allMenus==null||allMenus.isEmpty()||parentMenuId==null||parentMenuId==0){
			return Collections.emptyList();
		}
		List<MenuView> menuViewList=Lists.newArrayList();
		for(AuthMenu menu:allMenus){
			System.out.println(menu.getParentId());
	
			if(parentMenuId==menu.getParentId()||parentMenuId.equals(menu.getParentId())){
				MenuView view=new MenuView();
				view.setTitle(menu.getName());
				view.setUrl(menu.getPermissionUrl());
				view.setIcon(menu.getIconcls());
				view.setTarget("");
				view.setAppKey(menu.getAppKey());
				view.setParentId(menu.getParentId());
//				view.setAppKey(menu.getAppKey());
//				view.setItems(null);
				menuViewList.add(view);
			}
		}
		return menuViewList;
	}
	
	
	
}
