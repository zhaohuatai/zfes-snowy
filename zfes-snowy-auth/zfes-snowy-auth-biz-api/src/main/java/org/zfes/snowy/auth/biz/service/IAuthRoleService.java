/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
import org.zfes.snowy.auth.biz.model.AuthRole;
/**
 *  负债 
 * @author root
 *
 */
public interface IAuthRoleService {
	
	  void creatAuthRole(AuthRole authRole);
	 
	  void updateAuthRole(AuthRole authRole);
	 
	  Optional<AuthRole> loadAuthRole(Long id);
	 
	  Optional<AuthRole> loadAuthRoleByCode(String roleCode);
	
	  void deleteAuthRole(Long id);
		
//	 /**
//	  * 1.delete from RbacRolePermission <br/>
//	  * 2.delete from RbacGroupRole <br/>
//	  * 3.delete from RbacUserRole <br/>
//	  * 4.delete from RbacUserRoleReject <br/>
//	  * 5.delete from RbacRole <br/>
//	  * @param ids
//	  */
//	  void batchDeleteAuthRole(List<Long> roleIds);
	 
	/**
	 * 
	 * @param params params ={ name:(like), roleCode:(like), enabled:(=) , appId:(=) }
	 * @return
	 */
	DataSet loadAuthRoleList(String appKey,Map<String,Object> params);

	List<ComboboVo> loadRoleComboboVo(String appKey,Boolean firstEmpty);
	
	void setAuthRoleEnabled(List<Long> ids, Boolean enabled);

	/**
	 * 获取下拉框角色信息
	 * @param firstEmpty
	 * @return
	 */
	List<ComboboVo> loadRoleComboboVo2(Boolean firstEmpty);
}
