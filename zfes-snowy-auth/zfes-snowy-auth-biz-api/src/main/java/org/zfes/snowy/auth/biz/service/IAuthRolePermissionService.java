/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;

import org.zfes.snowy.core.data.DataSet;


public interface IAuthRolePermissionService{

	 List<Long> loadPermIdsByRoleId(Long roleId,String appkey,Boolean enabled);
	 
	 List<Long> loadPermIdsByRoleCode(String roleCode,String appkey,Boolean enabled);
	 
	 List<String> loadPermCodesByRoleId(Long roleId,String appkey,Boolean enabled) ;
	 
	 List<String> loadPermCodesByRoleCode(String roleCode,String appkey,Boolean enabled);
	 
	 void addPermsToRolePerm(List<Long> permIds,Long roleId);
	 
	 void removePermsFromRolePerm(List<Long> permIds,Long roleId);
	 /**
	  * 
	  * @param params {String appKey*, Long moduleId, String permCode,String name, Long roleId*}
	  * @return
	  */
	 public DataSet loadPermDataSetForRoleAssign(Boolean isInRole ,Map<String, Object> params );
}