/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.auth.biz.model.AuthUser;
public interface IAuthUserService{
	
	  Optional<AuthUser> loadAuthUser(Long id);
	 
	  Optional<AuthUser> loadByAccount(String account);
	  Optional<AuthUser> loadByOpenId(String openId);
	  void updateAuthUser(AuthUser authUser);
	
	  Long createUser(AuthUser authUser);	
	  
//	  void deleteAuthUser(Long userId);
	  
	 // void setAuthUserPwd(Long userIds, String password,String salt);
	  
	  void setAuthUserPwd(Long userId, String password);
	  
	  void changePwd(Long userId, String newpassword,String oldpassword);
	  
//	  void batchDeleteAuthUser(List<Long> userIds);
	  void setAccountExpired(List<Long> userIds, Boolean accountExpired);
	  void setAccountLocked(List<Long> userIds, Boolean accountLocked);
	  void setCredentialsExpired(List<Long> userIds, Boolean credentialsExpired);
		
	   void setAuthLevel(Long userId,int authLevel);
	  
	  void setDefaultRole(List<Long> userIds, Long roleId);
	  
	  DataSet loadAuthUserList(Map<String,Object> params);

	  void setUserStatus(List<Long> userIds, Boolean enabled, Boolean accountExpired, Boolean accountLocked,Boolean credentialsExpired);

	Long createAccount(String account, String password, Long roleId);

	Long createAccount(String account, String password, String roleCode);
	  
	 
}