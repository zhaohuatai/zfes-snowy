/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;

import org.zfes.snowy.core.data.DataSet;

public interface IAuthUserRoleService {
	 
	  List<String> loadRoleCodesInUserRoleByUserId(Long userId,String appkey,Boolean enabled);
	
	  List<Long> loadRoleIdsInUserRoleByUserId(Long userId,String appkey,Boolean enabled);
	  
	  List<Long> loadUserIdsInUserRoleByRoleId(Long roleId,String appkey,Boolean enabled);
	  
	  List<Long> loadUserIdsInUserRoleByRoleCode(String roleCode,String appkey,Boolean enabled);

	  
	  void addRolesToUserRole(List<Long> roleIds,Long userId);
	  
	  void addUsersToUserRole(List<Long> userIds,Long roleId);
	  
	  void removeRolesFromUserRole(List<Long> roleIds,Long userId);
	  
	  void removeUsersFromUserRole(List<Long> userIds,Long roleId);

	          
	  DataSet loadRoleDataSetForUserAssign(Boolean isInUser ,Map<String, Object> params ) ;

}


