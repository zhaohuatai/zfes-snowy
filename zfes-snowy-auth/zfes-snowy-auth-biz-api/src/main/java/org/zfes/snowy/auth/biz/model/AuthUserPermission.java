/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;

public class AuthUserPermission extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthUserPermission() {}
	public AuthUserPermission(Long id) {
		this.setId(id);
	}
	
	public AuthUserPermission(Long id,Long authPermissionId, Long authUserId) {
		super();
		this.setId(id);
		this.authPermissionId = authPermissionId;
		this.authUserId = authUserId;
	}

	//<-------------------------------------------->
	private java.lang.Long authPermissionId;
	private java.lang.Long authUserId;
	
	//<-------------------------------------------->
	public void setAuthPermissionId(java.lang.Long authPermissionId){
	 this.authPermissionId=authPermissionId;
	}
	public java.lang.Long getAuthPermissionId(){
		return this.authPermissionId;
	}
	public void setAuthUserId(java.lang.Long authUserId){
	 this.authUserId=authUserId;
	}
	public java.lang.Long getAuthUserId(){
		return this.authUserId;
	}

}
