/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
import org.zfes.snowy.auth.biz.model.AuthApp;
/**
 * 
 * @author zhaoht
 *
 */
public interface IAuthAppService{

	 Optional<AuthApp> loadAuthAppById(Long id);
	
	 Optional<AuthApp> loadAuthAppByAppKey(String appkey);
		
	 void createAuthApp(AuthApp authApp);
	
	 void updateAuthApp(AuthApp authApp);

	 void deletAuthApp(Long id);
	 
	 ///--------------------------------------------
	  
	 List<AuthApp> loadAllAuthAppList(Boolean enabled);

	 Optional<Long> loadIdByAppkey(String key,Boolean enabled);
		 
	 Optional<String> loadAppKeyById(Long id,Boolean enabled);
		 
	 List<ComboboVo> loadComboboVo(Boolean firstEmpty);

	 void setAuthAppEnabled(List<Long> ids, Boolean enabled);
	 
	 /**
	  * 
	  * @param params :{ appkey|Str|like, name|Str|like , enabled|Boolean|= , isDeployed|Boolean|= } 
	  * @return
	  */
	 DataSet loadAuthAppList(Map<String, Object> params);

		 
}