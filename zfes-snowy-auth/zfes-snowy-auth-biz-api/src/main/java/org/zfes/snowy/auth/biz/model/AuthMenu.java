/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;
import org.zfes.snowy.base.dao.enums.NotDBField;

public class AuthMenu extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthMenu() {}
	public AuthMenu(Long id) {
		this.setId(id);
	}
//<-------------------------------------------->
	private java.lang.Integer showIndex;
	
	@javax.validation.constraints.NotNull(message="菜单名称为空")
	private java.lang.String name;
	
	private java.lang.String iconcls;
	
	private java.lang.String type;
	
	@javax.validation.constraints.NotNull(message="父级菜单为空")
	private java.lang.Long parentId;
	
	private java.lang.Long moduleId;
	
	@javax.validation.constraints.NotNull(message="关联权限不能为空")
	private java.lang.Long permissionId;
	
	private java.lang.String description;
	
//	@javax.validation.constraints.NotNull(message="所属应用不能为空")
//	private java.lang.Long appId;
	
	@NotDBField
	private java.lang.String permissionUrl;
	
	@javax.validation.constraints.NotNull(message="所属应用不能为空")
	private java.lang.String appKey;
	
	@javax.validation.constraints.NotNull(message="用户组状态不能为空")
	private java.lang.Boolean enabled;
	
	private java.lang.Boolean isCommon;
	
	//<-------------------------------------------->
	public void setShowIndex(java.lang.Integer showIndex){
	 this.showIndex=showIndex;
	}
	public java.lang.Integer getShowIndex(){
		return this.showIndex;
	}
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setIconcls(java.lang.String iconcls){
		this.iconcls=iconcls==null?null:iconcls.trim();
	}
	public java.lang.String getIconcls(){
		return this.iconcls;
	}
	public void setType(java.lang.String type){
		this.type=type==null?null:type.trim();
	}
	public java.lang.String getType(){
		return this.type;
	}
	public void setParentId(java.lang.Long parentId){
	 this.parentId=parentId;
	}
	public java.lang.Long getParentId(){
		return this.parentId;
	}
	public void setModuleId(java.lang.Long moduleId){
	 this.moduleId=moduleId;
	}
	public java.lang.Long getModuleId(){
		return this.moduleId;
	}
	public void setPermissionId(java.lang.Long permissionId){
	 this.permissionId=permissionId;
	}
	public java.lang.Long getPermissionId(){
		return this.permissionId;
	}
	public void setDescription(java.lang.String description){
		this.description=description==null?null:description.trim();
	}
	public java.lang.String getDescription(){
		return this.description;
	}
	public java.lang.String getPermissionUrl() {
		return permissionUrl;
	}
	public void setPermissionUrl(java.lang.String permissionUrl) {
		this.permissionUrl = permissionUrl;
	}
//	public java.lang.Long getAppId() {
//		return appId;
//	}
//	public void setAppId(java.lang.Long appId) {
//		this.appId = appId;
//	}
	public java.lang.String getAppKey() {
		return appKey;
	}
	public void setAppKey(java.lang.String appKey) {
		this.appKey=appKey==null?null:appKey.trim();
	}
	public java.lang.Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(java.lang.Boolean enabled) {
		this.enabled = enabled;
	}
	public java.lang.Boolean getIsCommon() {
		return isCommon;
	}
	public void setIsCommon(java.lang.Boolean isCommon) {
		this.isCommon = isCommon;
	}

}
