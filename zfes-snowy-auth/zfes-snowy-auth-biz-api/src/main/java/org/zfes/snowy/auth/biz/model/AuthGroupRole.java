/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;
@Deprecated
public class AuthGroupRole extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthGroupRole() {}
	public AuthGroupRole(Long id) {
		this.setId(id);
	}
	
	
	public AuthGroupRole(Long id,Long authGroupId, Long authRoleId) {
		super();
		this.setId(id);
		this.authGroupId = authGroupId;
		this.authRoleId = authRoleId;
	}


	//<-------------------------------------------->
	private java.lang.Long authGroupId;
	private java.lang.Long authRoleId;
	
	//<-------------------------------------------->
	public void setAuthGroupId(java.lang.Long authGroupId){
	 this.authGroupId=authGroupId;
	}
	public java.lang.Long getAuthGroupId(){
		return this.authGroupId;
	}
	public void setAuthRoleId(java.lang.Long authRoleId){
	 this.authRoleId=authRoleId;
	}
	public java.lang.Long getAuthRoleId(){
		return this.authRoleId;
	}

}
