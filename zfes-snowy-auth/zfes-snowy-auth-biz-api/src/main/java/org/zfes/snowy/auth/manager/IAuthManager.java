/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.manager;

import java.util.List;
import java.util.Optional;

import org.zfes.snowy.auth.biz.model.AuthUser;
/**
 * 
 * @author zhaoht
 *
 */
public interface IAuthManager{
	
	 Optional<AuthUser> loadUserByName(String username);
 
	 List<String> loadRoleCodesUserHave(Long userId,String appkey,Boolean enabled);
	 
	 Optional<String> loadUserDefualtRoleCode(Long userId,Boolean enabled);
	 
	 List<String> loadPermCodesInUserDefaultRole(Long userId,String appkey,Boolean enabled);
	 
	 List<String> loadPermCodesInUserAllRoles(Long userId,String appkey,Boolean enabled);
	 
	 List<Long> loadPermIdsInUserAllRoles(Long userId,String appkey,Boolean enabled);
	 
	 List<Long> loadPermIdsInUserDefaultRole(Long userId,String appkey,Boolean enabled);

	
}