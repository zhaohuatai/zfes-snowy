/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;

public class AuthPermission extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthPermission() {}
	public AuthPermission(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->
	
	@javax.validation.constraints.NotBlank(message="权限编码不能为空")
	@org.hibernate.validator.constraints.Length(min=1,max=200,message="权限编码长度不能大于60字符")
	private java.lang.String permCode;
	
//	@javax.validation.constraints.NotNull(message="所属APP不能为空")
//	private java.lang.Long appId;
	
//	@NotDBField
	@javax.validation.constraints.NotNull(message="所属应用不能为空")
	private java.lang.String appKey;
	
	@javax.validation.constraints.NotNull(message="所属模块不能为空")
	private java.lang.Long moduleId;
	
	@javax.validation.constraints.NotBlank(message="权限名称不能为空")
	@org.hibernate.validator.constraints.Length(min=1,max=40,message="权限名称长度不能大于40字符")
	private java.lang.String name;
	
	@javax.validation.constraints.NotNull(message="权限状态不能为空")
	private java.lang.Boolean enabled;
	
	private java.lang.Boolean isCommon;
	
	private java.lang.String type;
	
	@javax.validation.constraints.NotBlank(message="权限URL不能为空")
	@org.hibernate.validator.constraints.Length(min=1,max=255,message="权限URL长度不能大于255字符")
	private java.lang.String url;
	
	@org.hibernate.validator.constraints.Length(min=1,max=50,message="权限描述不能超过50字符")
	private java.lang.String description;
	
	public java.lang.Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(java.lang.Long moduleId) {
		this.moduleId = moduleId;
	}

	//<-------------------------------------------->
	public void setPermCode(java.lang.String permCode){
		this.permCode=permCode==null?null:permCode.trim();
	}
	public java.lang.String getPermCode(){
		return this.permCode;
	}
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}
	public void setType(java.lang.String type){
		this.type=type==null?null:type.trim();
	}
	public java.lang.String getType(){
		return this.type;
	}
	public void setUrl(java.lang.String url){
		this.url=url==null?null:url.trim();
	}
	public java.lang.String getUrl(){
		return this.url;
	}
	public void setDescription(java.lang.String description){
		this.description=description==null?null:description.trim();
	}
	public java.lang.String getDescription(){
		return this.description;
	}
//	public java.lang.Long getAppId() {
//		return appId;
//	}
//	public void setAppId(java.lang.Long appId) {
//		this.appId = appId;
//	}
	public java.lang.String getAppKey() {
		return appKey;
	}
	public void setAppKey(java.lang.String appKey) {
		this.appKey=appKey==null?null:appKey.trim();
	}
	public java.lang.Boolean getIsCommon() {
		return isCommon;
	}
	public void setIsCommon(java.lang.Boolean isCommon) {
		this.isCommon = isCommon;
	}

}
