/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.model;

import org.zfes.snowy.base.dao.BaseModel;

public class AuthGroup extends BaseModel{

	private static final long serialVersionUID = 1L;
	
	public AuthGroup() {}
	public AuthGroup(Long id) {
		this.setId(id);
	}
	//<-------------------------------------------->

	 @javax.validation.constraints.NotBlank(message="用户组名称不能为空")
	 @org.hibernate.validator.constraints.Length(min=0,max=30,message="用户组名称长度不能大于30")
	 private java.lang.String name;
	
	 @org.hibernate.validator.constraints.Length(min=0,max=60,message="用户组描述长度不能大于60")
	 private java.lang.String description;
	
	 @javax.validation.constraints.NotNull(message="用户组状态不能为NULL")
	 private java.lang.Boolean enabled;
	
	 @javax.validation.constraints.NotNull(message="父用户组不能空")
	 private java.lang.Long parentId;
	
	//<-------------------------------------------->
	public void setName(java.lang.String name){
		this.name=name==null?null:name.trim();
	}
	public java.lang.String getName(){
		return this.name;
	}
	public void setDescription(java.lang.String description){
		this.description=description==null?null:description.trim();
	}
	public java.lang.String getDescription(){
		return this.description;
	}
	public void setEnabled(java.lang.Boolean enabled){
	 this.enabled=enabled;
	}
	public java.lang.Boolean getEnabled(){
		return this.enabled;
	}
	public void setParentId(java.lang.Long parentId){
	 this.parentId=parentId;
	}
	public java.lang.Long getParentId(){
		return this.parentId;
	}

}
