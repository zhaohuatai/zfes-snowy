/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authc.AuthenticationException;
import org.zfes.snowy.auth.shiro.exceptions.CsrfTokenException;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.JSONMSG;
import org.zfes.snowy.core.util.ZWebUtil;

public class SimpleAuthecCsrfFailureHandler extends AbstractAuthecHandler {
	
	private String defaultCsrfValidateFailureUrl="4001.html";
	
	private boolean csrfValidateForwardToDestination = false;
	
	public void onAuthenticationFailure(HttpServletRequest request,HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		int erroCode=-1;
		if(exception instanceof CsrfTokenException){
			erroCode=5001;
		}else if(exception instanceof CsrfTokenException){
			
		}
		
		if(ZWebUtil.isAjax(request)){
			ZWebUtil.respJSON(response, JSONMSG.newMSG(FrameConst.SC.SC_401, "认证失败【"+erroCode+"】"));
			return;
		}
		if(csrfValidateForwardToDestination){
			saveRequestAndForward( request,  response, getDefaultCsrfValidateFailureUrl()) ;
			return;
		}else{
			saveRequestAndRedirect( request,  response, getDefaultCsrfValidateFailureUrl());
			return;
		}
	}
	public String getDefaultCsrfValidateFailureUrl() {
		return defaultCsrfValidateFailureUrl;
	}
	public void setDefaultCsrfValidateFailureUrl(String defaultCsrfValidateFailureUrl) {
		this.defaultCsrfValidateFailureUrl = defaultCsrfValidateFailureUrl;
	}
	public boolean isCsrfValidateForwardToDestination() {
		return csrfValidateForwardToDestination;
	}
	public void setCsrfValidateForwardToDestination(boolean csrfValidateForwardToDestination) {
		this.csrfValidateForwardToDestination = csrfValidateForwardToDestination;
	}
}
