/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.filter.authc;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.zfes.snowy.auth.AuthConsts;
import org.zfes.snowy.auth.shiro.handlers.IAuthecLogoutHandler;
import org.zfes.snowy.core.util.CookieUtil;
import org.zfes.snowy.core.util.ZWebUtil;

public class SnowyLogoutFilter extends PathMatchingFilter {
	
    protected IAuthecLogoutHandler  authecLogoutHandler;
    
	@Override
	protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue)throws Exception {
		 String url=WebUtils.toHttp(request).getRequestURL().toString();
	   		System.out.println("SnowyShiroLogoutFilter||"+url);
		
		HttpServletRequest httpRequest = ZWebUtil.toHttp(request);
		HttpServletResponse httpResponse = ZWebUtil.toHttp(response);
	 	Subject subject =SecurityUtils.getSubject(); 
	 	subject.logout();
	 	CookieUtil.deleteCookie(httpRequest, httpResponse, AuthConsts.COOKIES.authSid,null);
	 	CookieUtil.deleteCookie(httpRequest, httpResponse, AuthConsts.COOKIES.syncsid,null);
	 	authecLogoutHandler.onAuthenticationLogout(httpRequest, httpResponse);
	 	return false;
	}

	public IAuthecLogoutHandler getAuthecLogoutHandler() {
		return authecLogoutHandler;
	}

	public void setAuthecLogoutHandler(IAuthecLogoutHandler authecLogoutHandler) {
		this.authecLogoutHandler = authecLogoutHandler;
	}
	
	
	
}
