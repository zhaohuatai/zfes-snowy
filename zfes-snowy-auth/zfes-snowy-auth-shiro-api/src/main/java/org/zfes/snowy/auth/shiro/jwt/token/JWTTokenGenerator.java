/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.jwt.token;

import java.util.Date;
import org.zfes.snowy.core.util.UUIDUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
//iss;// (Issuer) Claim
//sub;// (Subject) Claim
//aud;// (Audience) Claim
//exp;// (Expiration Time) Claim
//nbf;// (Not Before) Claim
//iat;// (Issued At) Claim
//jti;// (JWT ID) Claim
public class JWTTokenGenerator {

	
	public static String generateCsrfToken(String username)  {
		String tokenId=UUIDUtil.base58Uuid();
		String tokenStr= Jwts.builder()
						// .setIssuer("")// (Issuer) Claim
						 .setSubject(username)// (Subject) Claim
						 //.setAudience("")// (Audience) Claim
						// .setExpiration(new Date())// (Expiration Time) Claim
						// .setNotBefore(new Date())// (Not Before) Claim
						 .setIssuedAt(new Date())// (Issued At) Claim
						 .setId(tokenId)// (JWT ID) Claim
//						 .setClaims(Maps.newHashMap())
						// TokenPubkeyUtil.readCsrfVerifyPublicKey()
		                 .signWith(SignatureAlgorithm.HS512,"FFWzDthvTpBALzFNJvtKEF")
		                 .compact();
		return tokenStr;
	}
	
	public static JwtAuthToken createToken(String username)  {
		String tokenId=UUIDUtil.base58Uuid();
		String tokenStr= Jwts.builder()
						 .setIssuer("")// (Issuer) Claim
						 .setSubject(username)// (Subject) Claim
						 .setAudience("")// (Audience) Claim
						// .setExpiration(new Date())// (Expiration Time) Claim
						// .setNotBefore(new Date())// (Not Before) Claim
						 .setIssuedAt(new Date())// (Issued At) Claim
						 .setId(tokenId)// (JWT ID) Claim
//						 .setClaims(Maps.newHashMap())
		                 .signWith(SignatureAlgorithm.HS512, "FFWzDthvTpBALzFNJvtKEF")
		                 .compact();
		JwtAuthToken token=new JwtAuthToken(tokenStr);
		token.setUsername(username);
		return token;
	}
	public static void main(String[] args) {
		System.out.println(createToken("admin").getToken());
	}
	
}
