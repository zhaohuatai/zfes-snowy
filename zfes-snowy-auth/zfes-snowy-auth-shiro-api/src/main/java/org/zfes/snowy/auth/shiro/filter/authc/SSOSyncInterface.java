/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.filter.authc;

import org.apache.shiro.session.Session;

public interface SSOSyncInterface {
	
	default void syncClientCache(Session session,String appKey){}
}
