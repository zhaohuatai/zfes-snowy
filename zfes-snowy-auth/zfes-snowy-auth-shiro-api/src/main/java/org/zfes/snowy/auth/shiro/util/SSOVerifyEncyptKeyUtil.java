/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.util;

import java.util.Map;

import org.zfes.snowy.auth.AuthConsts;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.core.util.encypt.AESUtil;
import org.zfes.snowy.core.util.encypt.EncyptFileUtil;
import org.zfes.snowy.core.util.encypt.RSAUtil;

public class SSOVerifyEncyptKeyUtil {
	
	private static String ssoSignPublicKey=null;
	private static String ssoSignPrivateKey=null;
	private static String ssoAesPublicKey=null;
	
	private static final String maven_middle_path="src/main/resources/";
	private static final String target="target";
	private static final String clazzPath=Thread.currentThread().getContextClassLoader().getResource("").getPath().replaceAll("%20", " ");
	private static final String path=ZStrUtil.substringBeforeLast(clazzPath,target )+maven_middle_path+AuthConsts.ENCYPT.signature_path;
	
	public static void createSsoSignKeys(){
	    Map<String, Object> keyMap = RSAUtil.genKeyPair();
	    String rsaPublicKey = RSAUtil.getPublicKey(keyMap);
	    String rsaPrivateKey = RSAUtil.getPrivateKey(keyMap);
	  
	    String aesPublicKey=AESUtil.initkey();
	    System.out.println("rsaPublicKey1|"+rsaPublicKey);
	    System.out.println("rsaPrivateKey1|"+rsaPrivateKey);
	    System.out.println("aesPublicKey1|"+aesPublicKey);
	    EncyptFileUtil.saveKey(rsaPublicKey, path,AuthConsts.ENCYPT.sso_rsa_publicKey_file_name);
	    EncyptFileUtil.saveKey(rsaPrivateKey, path,AuthConsts.ENCYPT.sso_rsa_privateKey_file_name);
	    EncyptFileUtil.saveKey(aesPublicKey, path,AuthConsts.ENCYPT.sso_aes_publicKey_file_name);
	}
	public static String readSSOAesPublicKey(){
		if(ssoAesPublicKey==null){
			synchronized (SSOVerifyEncyptKeyUtil.class) {
				 if (ssoAesPublicKey == null) {
					 ssoAesPublicKey=EncyptFileUtil.readKey(path,AuthConsts.ENCYPT.sso_aes_publicKey_file_name);
				       if(!ZStrUtil.hasText(ssoAesPublicKey)){
				    	   throw new AppRuntimeException("系统未生成 ssoAesPublicKey");
				       }
			        }  
			}
		}
		return ssoAesPublicKey;
	}
	/**
	 * <h1>只是读取随机产生的RSA密钥对</h1>
	 * @return
	 */
	public static String readSSOSignPublicKey(){
		if(ssoSignPublicKey==null){
			synchronized (SSOVerifyEncyptKeyUtil.class) {
				 if (ssoSignPublicKey == null) {
				       ssoSignPublicKey=EncyptFileUtil.readKey(path,AuthConsts.ENCYPT.sso_rsa_publicKey_file_name);
				       if(!ZStrUtil.hasText(ssoSignPublicKey)){
				    	   throw new AppRuntimeException("系统未生成 ssoSignPublicKey");
				       }
			        }  
			}
		}
		return ssoSignPublicKey;
	}
	/**
	 * <h1>只是读取随机产生的RSA密钥对</h1>
	 * @return
	 */
	public static String readSSOSignPrivateKey(){
		if(ssoSignPrivateKey==null){
			synchronized (SSOVerifyEncyptKeyUtil.class) {
				 if (ssoSignPrivateKey == null) { 
					    ssoSignPrivateKey=EncyptFileUtil.readKey(path,AuthConsts.ENCYPT.sso_rsa_privateKey_file_name);
					    if(!ZStrUtil.hasText(ssoSignPrivateKey)){
					    	   throw new AppRuntimeException("系统未生成 ssoSignPrivateKey");
					       }
			        }  
			}
		}
		return ssoSignPrivateKey;
	}
	//------------------------------------------------------------------------------

	

	public static void main(String[] args){    
		
		SSOVerifyEncyptKeyUtil.createSsoSignKeys();
//		System.out.println(readJwtVerifyPublicKey()) ; 
		//
		//createSsoSignKeys(); 
		//String privateKey=readSsoSignPrivateKey();
		//String publicKey=readSsoSignPublicKey();
		//String data=UUIDUtil.base58Uuid();
		//System.out.println("data: "+data); 
		//System.out.println("PrivateKey: "+privateKey); 
		//System.out.println("PublicKey: "+publicKey); 
		//
		//String sign=RSAUtil.sign(data.getBytes(), privateKey);
		//System.out.println("signData: "+publicKey);
		//
		//boolean status=RSAUtil.verify(data.getBytes(), publicKey, sign);//签名验证
		//System.out.println("status: "+status);
	}
	
}
