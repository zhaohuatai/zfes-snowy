/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.handlers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.JSONMSG;
import org.zfes.snowy.core.util.ZWebUtil;

public class SimpleAuthecLogoutHandler extends AbstractAuthecHandler {
	
	private String defaultLogoutDirectUrl="/login.html";
	
	private boolean logoutForwardToDestination = false;
	
	public void onAuthenticationLogout(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException {
		if(ZWebUtil.isAjax(request)){
			ZWebUtil.respJSON(response, JSONMSG.newMSG(FrameConst.SC.SC_200, "退出成功"));
			return;
		}
		if(logoutForwardToDestination){
			saveRequestAndForward( request,  response, getDefaultLogoutDirectUrl()) ;
			return;
		}else{
			saveRequestAndRedirect( request,  response, getDefaultLogoutDirectUrl());
			return;
		}
	
	}


	public String getDefaultLogoutDirectUrl() {
		return defaultLogoutDirectUrl;
	}
	public void setDefaultLogoutDirectUrl(String defaultLogoutDirectUrl) {
		this.defaultLogoutDirectUrl = defaultLogoutDirectUrl;
	}
	public boolean isLogoutForwardToDestination() {
		return logoutForwardToDestination;
	}
	public void setLogoutForwardToDestination(boolean logoutForwardToDestination) {
		this.logoutForwardToDestination = logoutForwardToDestination;
	}
	
	
}
