/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.cache.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.CollectionUtils;
import org.zfes.snowy.common.nosql.redis.jedis.SnowyJedisManager;
import org.zfes.snowy.core.exceptions.AppRuntimeException;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * authenticationCacheName: sh_auhen
 * authorizationCacheName : sh_auhrz
 * 
 * hash 存储：
 * authenticationCacheName key = sh_auhen_appkey :value -->Map<k,v>
 * 
 * authorizationCacheName  key = sh_auhrz_appkey :value -->Map<k,v>
 * 
 * @author zhaohuatai
 *
 * @param <K>
 * @param <V>
 */
public class ShiroRedisCache<K, V> implements Cache<K, V> {


	private SnowyJedisManager jedisManager;
	
    private String name;
    

    public ShiroRedisCache(String name, SnowyJedisManager jedisManager) {
        this.name = name;
        this.jedisManager = jedisManager;
    }
    

    /**
     * 例如： shiro-cache:shiro.authorizationCache:[authinfo]
     */
    public String getName() {
        if (name == null)
           throw new AppRuntimeException("shiro-cache name can not be null");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#get(java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    @Override
    public V get(K key) throws CacheException {
    	keys();
    	Object value=jedisManager.hashForObjGet(name, key.toString());
    	return (V) value;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#put(java.lang.Object, java.lang.Object)
     */
    @Override
    public V put(K key, V value) throws CacheException {
        V previos = get(key);
        try {
        	jedisManager.hashForObjSet(name, key.toString(), value);
        } catch (Exception e) {
        	throw new CacheException(e);
        }
        return previos;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#remove(java.lang.Object)
     */
    @Override
    public V remove(K key) throws CacheException {
        V previos = get(key);
        try {
        	jedisManager.hashRemoveBinary(name, key.toString());
        } catch (Exception e) {
        	throw new CacheException(e);
        }
        return previos;
    }
  
    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#clear()
     */
    @Override
    public void clear() throws CacheException {
    	  try {
    		  jedisManager.delBinary(name);
          } catch (Exception e) {
        	  throw new CacheException(e);
          }
    	
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#size()
     */
    @Override
    public int size() {
    	try {
            return (int) jedisManager.hashLenBinary(name);
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }
    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#keys()
     */
    @SuppressWarnings("unchecked")
	@Override
    public Set<K> keys() {
    	try {
    		Set<String> keys =jedisManager.hashKeysBinary(name);
            if (CollectionUtils.isEmpty(keys)) {
            	return Collections.emptySet();
            }else{
            	return keys.stream().map(item->(K)item).collect(Collectors.toSet());
            }
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.cache.Cache#values()
     */
	@SuppressWarnings("unchecked")
	@Override
    public Collection<V> values() {
    	try {
    		Map<String,Object> values=jedisManager.hashForObjGetAll(name);
    		if (!CollectionUtils.isEmpty(values)) {
    			return (Collection<V>) values.values();
        	}
            return Collections.emptyList();
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }


   
//--------------------------------------------
    
}
