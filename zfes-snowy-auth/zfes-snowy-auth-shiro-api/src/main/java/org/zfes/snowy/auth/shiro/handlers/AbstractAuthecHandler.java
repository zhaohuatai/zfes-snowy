/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.ZStrUtil;

public abstract class AbstractAuthecHandler implements IAuthecSuccessHandler,IAuthecFailureHandler, IAuthecLogoutHandler,IUnAuthecHandler{
	
	
	
	@Override  
	public void onAuthenticationFailure(HttpServletRequest request,HttpServletResponse response, AuthenticationException exception)throws IOException, ServletException{
		//to do nothing	
	}
	@Override  
	public void onAuthenticationFailure(HttpServletRequest request,HttpServletResponse response, String exception)throws IOException, ServletException{
		//to do nothing	
	}
	@Override  
	public void onAuthenticationLogout(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException{
	//to do nothing	
	}
	@Override  
	public void onAuthenticationUnAuthec(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException{
		//to do nothing	
	}
	
	@Override  
	public void onAuthenticationSuccess(HttpServletRequest request,HttpServletResponse response, Subject subject,boolean isGenCSRFToken)throws IOException, ServletException{
		//to do nothing	
	}
	   protected void saveRequestAndRedirect(ServletRequest request, ServletResponse response,String url) throws IOException {
		  saveRequest(request);
		  redirect(request, response,url);
		}
		
	   protected void saveRequestAndForward(ServletRequest request, ServletResponse response,String url) throws IOException {
			  saveRequest(request);
			  redirect(request, response,url);
		}
		
	   protected void saveRequest(ServletRequest request) {
		  WebUtils.saveRequest(request);
		}
		
	   protected void redirect(ServletRequest request, ServletResponse response,String url) throws IOException {
			if(!ZStrUtil.hasText(url)){
				throw new AppRuntimeException("未配置登陆入口页面");
			}
		  WebUtils.issueRedirect(request, response, url);
		}
	   protected void forWordToLogin(ServletRequest request, ServletResponse response,String url) throws IOException {
			if(!ZStrUtil.hasText(url)){
				throw new AppRuntimeException("未配置登陆入口页面");
			}
		  WebUtils.issueRedirect(request, response, url);
		}


}
