package org.zfes.snowy.auth.shiro.weichat.token;

import org.apache.shiro.authc.AuthenticationToken;

public class WeiChatAuthToken implements AuthenticationToken{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	private String appid;
//	private String secret;
//	private String code;
//	private String grant_type;
//	
//	
//	private String openid;
//	private String access_token;

	
    private String username;

   
    public WeiChatAuthToken(String username, char[] password) {
		super();
		this.username = username;
		this.password = password;
	}

	private char[] password;
	
	@Override
	public Object getPrincipal() {
		return getUsername();
	}

	@Override
	public Object getCredentials() {
		return getPassword();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

}
