/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.exceptions;

import org.apache.shiro.authc.AuthenticationException;

public class StatelessTokenException extends AuthenticationException {
	
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new StatelessTokenException.
     */
    public StatelessTokenException() {
        super();
    }

    /**
     * Constructs a new StatelessTokenException.
     *
     * @param message the reason for the exception
     */
    public StatelessTokenException(String message) {
        super(message);
    }

    /**
     * Constructs a new StatelessTokenException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public StatelessTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new StatelessTokenException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public StatelessTokenException(String message, Throwable cause) {
        super(message, cause);
    }
   
}
