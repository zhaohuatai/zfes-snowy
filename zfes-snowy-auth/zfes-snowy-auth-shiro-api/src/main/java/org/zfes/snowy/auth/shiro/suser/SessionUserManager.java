/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.suser;

import java.util.Optional;

public interface SessionUserManager {
	public Optional<SessionUser> loadSessionUser(String username);
	
	public Optional<SessionUser> loadSessionUserByOpenId(String openId);
}
