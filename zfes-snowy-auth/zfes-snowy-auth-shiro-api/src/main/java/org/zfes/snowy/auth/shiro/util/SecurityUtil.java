/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.util;
import java.util.Optional;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.zfes.snowy.auth.AuthConsts;
import org.zfes.snowy.auth.shiro.suser.SessionUser;

public class SecurityUtil {
	
	public static Optional<Session> getShiroSession() {
		Subject subject=SecurityUtils.getSubject();
		if(subject==null){
			return Optional.empty();
		}
		return Optional.ofNullable(subject.getSession());
	}
	
	
	public static Optional<SessionUser> getSessionUser(){
		Optional<Session> sessionOp =getShiroSession();
		if(!sessionOp.isPresent()) {
			return Optional.empty();
		}
		return Optional.ofNullable((SessionUser)sessionOp.get().getAttribute(AuthConsts.SESSION.SESSION_USER_KEY));
	}
	
	
	 public static boolean isAuthenticated() {  
	     return getSubject().isAuthenticated();  
	 }  
	  
	 public static Subject getSubject() {  
	        return SecurityUtils.getSubject();  
	 }  
	    
	
	public static Optional<Long> getUserId() {
		Optional<SessionUser> sessionUserOp = getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getUserId());
		}
		return Optional.empty();
	}
	
	public  static Optional<String> getUserName() {
		Subject subject = getSubject();
		PrincipalCollection collection = subject.getPrincipals();
		if (null != collection && !collection.isEmpty()) {
			return Optional.ofNullable((String) collection.iterator().next());
		}
		return Optional.empty();
	}
	
	public static Optional<String> getDefaltRoleCode() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getRoleCode());
		}
		return Optional.empty();
	}
	public static Optional<String> getDefaltRoleName() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getRoleName());
		}
		return  Optional.empty();
	}
	public static  Optional<Long>  getCurrentOrgId() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getOrgId());
		}
		return Optional.empty();
	}
	public static Optional<String> getCurrentOrgName() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getOrgName());
		}
		return Optional.empty();
	}
	public static  Optional<String>  getCurrentPositionName() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getPositionName());
		}
		return null;
	}
	public static  Optional<Long>  getCurrentPositionId() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getPositionId());
		}
		return Optional.empty();
	}
	public static  Optional<String>  getCurrentPositionCode() {
		Optional<SessionUser> sessionUserOp =getSessionUser();
		if(sessionUserOp.isPresent()){
			return Optional.ofNullable(sessionUserOp.get().getPositionCode());
		}
		return Optional.empty();
	}
	
	

}
