package org.zfes.snowy.auth.shiro.cas;

import org.apache.shiro.authc.RememberMeAuthenticationToken;

public class SnowyCasToken implements RememberMeAuthenticationToken {
    
    private static final long serialVersionUID = 8587329689973009598L;
    
    // the service ticket returned by the CAS server
    private String ticket = null;
    
    // the user identifier
    private String userId = null;
    
    // is the user in a remember me mode ?
    private boolean isRememberMe = false;
    
    private char[] password;
    
    
    public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public SnowyCasToken(String ticket) {
        this.ticket = ticket;
    }
    
    public Object getPrincipal() {
        return userId;
    }
    
    public Object getCredentials() {
        return getPassword();
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public boolean isRememberMe() {
        return isRememberMe;
    }
    
    public void setRememberMe(boolean isRememberMe) {
        this.isRememberMe = isRememberMe;
    }

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getUserId() {
		return userId;
	}
    
    
    
}
