///**
// * Copyright (c) 2016-2020 https://github.com/zhaohuatai
// *
// * contact z_huatai@qq.com
// *  
// */
//package org.zfes.snowy.auth.shiro.session.repository.impl;
//
//import org.apache.shiro.session.Session;
//import org.springframework.util.CollectionUtils;
//import org.zfes.snowy.auth.shiro.session.repository.IShiroSessionRepository;
//import org.zfes.snowy.common.nosql.redis.jedis.SnowyJedisManager;
//import org.zfes.snowy.core.exceptions.AppRuntimeException;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
///*
// * 存储形式： 字符串 sessionid: session 序列化内容
// * 
// * List：sessionid队列，方便分页进行数据拉取校验
// * 
// * 
// */
///**
// * 
// * hash 存储：
// * activeSessionsCacheName key = sh_sessions :valye -->Map<sessionid,session序列化数据>
// * 
// * list 同步存储 sessionids
// * 
// */
//
//public class RedisShiroSessionRepository2 implements IShiroSessionRepository {
//	
//
//    private int sessionLiveTime = 18000;
//    
//    private static final String redisSessionIdListKeyName = "sh_sessionids";
//    
//    private static final String redisSessionHashKeyName="sh_sessions";
//    
//    private SnowyJedisManager jedisManager;
//     
//    @Override
//    public void saveSession(Session session) {
//        if (session == null || session.getId() == null)
//            throw new NullPointerException("session is empty");
//        try {
//        	boolean  hashExists =jedisManager.hashExistsBinary(redisSessionHashKeyName, (String)session.getId());
//        	if(!hashExists) {
//        		jedisManager.listForObjAdd(redisSessionIdListKeyName, (String)session.getId(), true);
//        		jedisManager.hashForObjSet(redisSessionHashKeyName, (String)session.getId(), session);
//        	}
//        } catch (Exception e) {
//        	throw new AppRuntimeException(e);
//        }
//    }
//    @Override
//    public void updateSession(Session session) {
//        if (session == null || session.getId() == null)
//            throw new NullPointerException("session is empty");
//        try {
//        	jedisManager.hashForObjSet(redisSessionHashKeyName, (String)session.getId(), session);
//        } catch (Exception e) {
//        	throw new AppRuntimeException(e);
//        }
//    }
//    @Override
//    public void deleteSession(String id) {
//        if (id == null) {
//            throw new NullPointerException("session id is empty");
//        }
//        try {
//            jedisManager.hashRemoveBinary(redisSessionHashKeyName, id);
//            getJedisManager().listForObjRemove(redisSessionIdListKeyName, -1, id);
//        } catch (Exception e) {
//        	throw new AppRuntimeException(e);
//        }
//    }
//    @Override
//    public Session getSession(String id) {
//        if (id == null){
//        	 throw new NullPointerException("session id is empty");
//        }
//        Session session = null;
//        try {
//        	session= (Session) jedisManager.hashForObjGet(redisSessionHashKeyName, id);
//        } catch (Exception e) {
//        	throw new AppRuntimeException(e);
//        }
//        return session;
//    }
//
//    @Override
//    public Collection<Session> getAllSessions() {
//    	try {
//        	Map<String, Object>  values=jedisManager.hashForObjGetAll(redisSessionHashKeyName);
//    		if (!CollectionUtils.isEmpty(values)) {
//    			return values.values().stream().map(val-> (Session)val).collect(Collectors.toSet());
//    		}
//        	return Collections.emptyList();
//    	}catch (Exception t) {
//     	   throw new AppRuntimeException(t);
//        }
//    }
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public Collection<String> getSessionIdsInSegment(long start, long end) {
//		List<String> values=(List<String> )(jedisManager.listForObjGet(redisSessionIdListKeyName, start, end));
//		return values;
//	}
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<Session> getSessionInSegment(long start, long end) {
//		Collection<String> ids=getSessionIdsInSegment( start,  end);
//		List<Session> sessions=(List<Session>)jedisManager.hashForObjMGet(redisSessionHashKeyName,(List<String>) ids);
//		return sessions;
//	}
//	
//    public SnowyJedisManager getJedisManager() {
//        return jedisManager;
//    }
//
//    public void setJedisManager(SnowyJedisManager jedisManager) {
//        this.jedisManager = jedisManager;
//    }
//
//
//	public int getSessionLiveTime() {
//		return sessionLiveTime;
//	}
//	@Override
//	public void setSessionLiveTime(int sessionLiveTime) {
//		this.sessionLiveTime = sessionLiveTime;
//	}
//
//
//
//
//}
