package org.zfes.snowy.auth.shiro.cas;

import org.apache.shiro.authc.AuthenticationException;

public class SnowyCasAuthenticationException extends AuthenticationException{
	 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public SnowyCasAuthenticationException() {
	        super();
	    }

	    public SnowyCasAuthenticationException(String message) {
	        super(message);
	    }

	    public SnowyCasAuthenticationException(Throwable cause) {
	        super(cause);
	    }

	    public SnowyCasAuthenticationException(String message, Throwable cause) {
	        super(message, cause);
	    }
}
