/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth;
public class AuthConsts {
	public static final String ROLE_ROOT = "ROOT";
	
	public static final class LOGIN{
		public static final String DEFAULT_USERNAME_PARAM = "username";
		public static final String DEFAULT_PASSWORD_PARAM = "password";
		public static final String DEFAULT_REMEMBER_ME_PARAM = "rememberMe";
		public static final String DEFAULT_CAPTCHA_PARAM = "captcha";
		public static final String DEFAULT_LOGINORGIN_PARAM = "loginOrgin";
		
		
		public static final String DEFAULT_LOGIN_SUCCESS_URL = "/index.html";
		public static final String DEFAULT_LOGIN_FAILURE_URL = "/login.html";
		public static final String DEFAULT_LOGIN_PORT_URL = "/login.html";
		public static final String DEFAULT_LOGIN_SUBMIT_URL = "/api/auth/login";
		public static final String DEFAULT_LOGIN_WEICHAT_WEICHAT_AUTH_CALLBACK_URL = "/api/auth/weichat/auth/callback";
		public static final String DEFAULT_LOGINOUT_URL = "/api/auth/logout";
		
	}
	public static final class CAPTCHA{
		public static final String DEFAULT_CAPTCHA_PARAM = "captcha";
		public static final String DEFAULT_CAPTCHA_IMAGE_URL = "/api/auth/captcha";
	}
	
	 public static final String DEFAULT_LOGIN_PORT_URL = "/login.html";
	 
	public static final Long CLIENT_AUTHORIZATION_CACHE_VERSION=-1L;
	
	public static final String DEFAULT_SSO_SERVER_URL = "shiroLoginFailure";
	
	public static final String DEFAULT_ERROR_KEY_ATTRIBUTE_NAME = "/sso/auth/appauth.sso";
	

	
	public static final String PERMISSIVE = "permissive";

	public static final class COMPASS_CREDENTIALS{
		public static final String compass_salt = "snowy_zfes";
		public static final String compass_password = "snowy_zfes";
		public static final String pageSalt = "zhtframework_94DABGioQOq2tTUO0AXYow";
	}

	public static final class SESSION{
		public static final String SESSION_USER_KEY="_sessionuserkey_";
		// PC,MOB
		public static final String SESSION_LOGINORGIN_KEY="_loginorginkey_";
		
		public static final String SESSION_CSRF_TOKEN="_zfes_csrf_token_";
		
	}
	
	public static final class HEADER{
		public static final String CSRF_TOKEN="zfesct";
		
	}
	
	public static final class COOKIES{
		public static final String authSid="sid";
		public static final String syncsid= "syncsid";
		public static final String authRememberMe="rememberMe";
	}
	
	public static final class ENCYPT{
		public static final String signature_path="signaturekey"+"/";
		
		public static final String csrf_rsa_privateKey_file_name="csrf_rsa_privateKey.key";
		public static final String csrf_rsa_data_file_name="csrf_rsa_data.dat";
		
		public static final String jwt_rsa_privateKey_file_name="jwt_rsa_privateKey.key";
		public static final String jwt_rsa_data_file_name="jwt_rsa_data.dat";
		
		public static final String sso_rsa_privateKey_file_name= "sso_rsa_privateKey.key";
		public static final String sso_rsa_publicKey_file_name = "sso_rsa_publicKey.key";
		public static final String sso_aes_publicKey_file_name = "sso_aes_publicKey.key";
	}
	
	
	public static final String authorCacheVersion="_acv";
	
	
	public static final String authSid="sid";
	
	public static final class JWT{
		 public static final String DEFAULT_JWT_LOGIN_PORT_URL = "/login.html";
		 public static final String DEFAULT_JWT_LOGIN_SUBMIT_URL = "/api/auth/jwt/login";
	}
	
	
	public static final class SSO{
		
		public static final String ssoSidParam="s_sid";
		public static final String ssoTokenParam="s_tk_";
		public static final String ssoSignParam="s_si_";
		
		public static final String ssoCenterSyncPath="/api/auth/sso/center/sync.so";
		
		public static final String ssoClientSyncPath="/api/auth/sso/client/sync.so";
		
		
		
		public static final String ssoOriginUrl="originurl";//请求url
		public static final String queryStringParam="querystring";
	}

	
}