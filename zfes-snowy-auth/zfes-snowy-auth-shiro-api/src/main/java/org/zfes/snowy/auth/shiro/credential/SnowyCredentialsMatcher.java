/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.credential;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zfes.snowy.auth.shiro.jwt.token.JWTTokenParser;
import org.zfes.snowy.auth.shiro.jwt.token.JwtAuthToken;
import org.zfes.snowy.auth.shiro.util.JWTTokenVerifyPublicKeyUtil;
import org.zfes.snowy.auth.shiro.weichat.token.WeiChatAuthToken;
import org.zfes.snowy.core.util.ZStrUtil;

public class SnowyCredentialsMatcher extends HashedCredentialsMatcher{
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
    	logger.debug("doCredentialsMatch:||token: "+token+"||AuthenticationInfo: "+info);
    	if(token instanceof JwtAuthToken){
    		String jwtTokenStr=((JwtAuthToken)token).getToken();
    		if(ZStrUtil.hasText(jwtTokenStr)){
    	    	return JWTTokenParser.isJwtAuthTokenLegal(jwtTokenStr,JWTTokenVerifyPublicKeyUtil.readJwtVerifyPublicKey());
    		}
    		return false;
    	}else if(token instanceof WeiChatAuthToken) {
    		logger.debug("token instanceof WeiChatAuthToken: ");
    		return true;
    	}
        Object tokenHashedCredentials = hashProvidedCredentials(token, info);
        Object accountCredentials = getCredentials(info);
        return equals(tokenHashedCredentials, accountCredentials);
    }

}
