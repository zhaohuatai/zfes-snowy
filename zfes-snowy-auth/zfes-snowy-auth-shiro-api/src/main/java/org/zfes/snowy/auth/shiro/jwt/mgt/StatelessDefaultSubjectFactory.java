package org.zfes.snowy.auth.shiro.jwt.mgt;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSubjectFactory;
import org.zfes.snowy.auth.shiro.jwt.token.JwtAuthToken;

public class StatelessDefaultSubjectFactory extends DefaultWebSubjectFactory {
	   @Override  
	    public Subject createSubject(SubjectContext context) {  
	        //不创建session  
		   AuthenticationToken token =context.getAuthenticationToken();
		   if(token!=null&&token instanceof JwtAuthToken){
			   context.setSessionCreationEnabled(false);  
		   }
	        return super.createSubject(context);  
	    } 
}
