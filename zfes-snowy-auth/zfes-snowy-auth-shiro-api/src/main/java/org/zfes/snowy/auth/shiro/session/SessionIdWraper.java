/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.session;

import java.io.Serializable;


/**
 * protsttuff 直接序列化字符串，体积太大，所以此处进行Wraper
 * @author zhaohuatai
 *
 */
public class SessionIdWraper implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SessionIdWraper(){};
	public SessionIdWraper(Serializable sessionId) {
		super();
		this.sessionId = sessionId;
	}
	
	private Serializable sessionId;

	public Serializable getSessionId() {
		return sessionId;
	}
	public void setSessionId(Serializable sessionId) {
		this.sessionId = sessionId;
	}
	
	


}
