/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.realm;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public abstract class SnowyRemoteRealm extends AuthorizingRealm {

	public abstract AuthenticationInfo  remoteDoGetAuthenticationInfo(String appKey,AuthenticationToken authcToken);
	 
	public abstract AuthorizationInfo remoteDoGetAuthorizationInfo(String appKey,PrincipalCollection principals);
	
	
	public abstract String getAppKey()  ;
	
	public abstract void setAppKey(String appKey) ;
	
	
}
