/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.handlers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.JSONMSG;
import org.zfes.snowy.core.util.ZWebUtil;

public class SimpleUnauthecHandler extends AbstractAuthecHandler {
	
	private String defaultUnAuthecDirectUrl="/login.html";
	
	private boolean unAuthecForwardToDestination = false;
	
	public void onAuthenticationUnAuthec(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException {
		if(ZWebUtil.isAjax(request)){
			saveRequest(request);
			ZWebUtil.respJSON(response, JSONMSG.newMSG(FrameConst.SC.SC_301, "未登录"));
			return;
		}
		if(unAuthecForwardToDestination){
			ZWebUtil.forward(request, response, getDefaultUnAuthecDirectUrl());
			//saveRequestAndForward( request,  response, getDefaultUnAuthecDirectUrl()) ;
			return;
		}else{
			saveRequestAndRedirect( request,  response, getDefaultUnAuthecDirectUrl());
			return;
		}
	
	}


	
	public String getDefaultUnAuthecDirectUrl() {
		return defaultUnAuthecDirectUrl;
	}



	public void setDefaultUnAuthecDirectUrl(String defaultUnAuthecDirectUrl) {
		this.defaultUnAuthecDirectUrl = defaultUnAuthecDirectUrl;
	}



	public boolean isUnAuthecForwardToDestination() {
		return unAuthecForwardToDestination;
	}



	public void setUnAuthecForwardToDestination(boolean unAuthecForwardToDestination) {
		this.unAuthecForwardToDestination = unAuthecForwardToDestination;
	}




	
	
}
