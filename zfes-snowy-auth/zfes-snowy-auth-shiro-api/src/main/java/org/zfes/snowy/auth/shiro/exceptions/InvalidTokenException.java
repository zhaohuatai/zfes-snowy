/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.exceptions;

/**
 * Exception thrown when attempting to interact with the system under an established StatelessToken
 * when that StatelessToken is considered invalid.  The meaning of the term 'invalid' is based on
 * application behavior.  For example, a StatelessToken is considered invalid if it has been explicitly
 * stopped (e.g. when a user logs-out or when explicitly
 * {@link Session#stop() stopped} programmatically.  A Session can also be
 * considered invalid if it has expired.
 *
 * @see StoppedTokenException
 * @see ExpiredTokenException
 * @see UnknownTokenException
 * @since 0.1
 */
public class InvalidTokenException extends StatelessTokenException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new InvalidSessionException.
     */
    public InvalidTokenException() {
        super();
    }

    /**
     * Constructs a new InvalidSessionException.
     *
     * @param message the reason for the exception
     */
    public InvalidTokenException(String message) {
        super(message);
    }

    /**
     * Constructs a new InvalidSessionException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public InvalidTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new InvalidSessionException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public InvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }

}
