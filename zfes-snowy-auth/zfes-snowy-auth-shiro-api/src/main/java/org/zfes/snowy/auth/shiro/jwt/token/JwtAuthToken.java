/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.jwt.token;


import org.apache.shiro.authc.AuthenticationToken;
import org.zfes.snowy.auth.AuthConsts;
public class JwtAuthToken implements AuthenticationToken{
//	private String iss;// (Issuer) Claim
//	private String sub;// (Subject) Claim
//	private String aud;// (Audience) Claim
//	private String exp;// (Expiration Time) Claim
//	private String nbf;// (Not Before) Claim
//	private String iat;// (Issued At) Claim
//	private String jti;// (JWT ID) Claim
	
	private static final long serialVersionUID = 1L;
	private String token;
	private String username;
	private static final String password=AuthConsts.COMPASS_CREDENTIALS.compass_salt;
	
	public JwtAuthToken() {}
	public JwtAuthToken(String token) {
	   this();
	   this.token = token;
	}
	
	@Override
	public Object getCredentials() { //凭据  ---密码
		return password;
	}
	@Override
	public Object getPrincipal() {//身份  --用户名
		return username;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
