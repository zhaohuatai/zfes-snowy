/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.session.dao;

import java.io.Serializable;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
//重构 EnterpriseCacheSessionDAO
public class SameToEnterpriseCacheSessionDAO  extends CachingSessionDAO{
    
	@Override
	protected void doDelete(Session session) {
//继承自CachingSessionDAO
//		SessionDAO中接口 delete  在AbstractSessionDAO没有实现，留到直接子类CachingSessionDAO中有实现：
			
//	    public void delete(Session session) {
//	        uncache(session);
//	        doDelete(session);
//	    }
//此处可以做一些 删除之后的事情，但是 此处不用做任何事情------do nothing
		System.out.println("------doDelete----");
	}

	@Override
	protected void doUpdate(Session session) {
		System.out.println("------doUpdate----");
//继承自CachingSessionDAO
//	SessionDAO中接口 update  在AbstractSessionDAO没有实现，留到直接子类CachingSessionDAO中有实现：
		
//	    public void update(Session session) throws UnknownSessionException {
//	        doUpdate(session);
//	        if (session instanceof ValidatingSession) {
//	            if (((ValidatingSession) session).isValid()) {
//	                cache(session, session.getId());
//	            } else {
//	                uncache(session);
//	            }
//	        } else {
//	            cache(session, session.getId());
//	        }

//此处可以做一些 初始化的事情，但是 此处不用做任何事情------do nothing
	}

	@Override
	protected Serializable doCreate(Session session) {
		System.out.println("------doCreate----");
//继承自AbstractSessionDAO
		
// AbstractSessionDAO中 create的实现： 
//	    public Serializable create(Session session) {
//	        Serializable sessionId = doCreate(session);
//	        verifySessionId(sessionId);
//	        return sessionId;
//	    }
		
// 直接父类中：CachingSessionDAO中  create  的实现：
//	    public Serializable create(Session session) {
//	        Serializable sessionId = super.create(session);//此处--->AbstractSessionDAO中：Serializable sessionId = doCreate(session);
//	        cache(session, sessionId);
//	        return sessionId;
//	    }
//所以此处只要返回sessionId即可,生成ID即可，在CachingSessionDAO 中才会保存，不会导致id错乱
		Serializable sessionId = generateSessionId(session);
	    assignSessionId(session, sessionId);
	    return sessionId;
	}

	@Override
	protected Session doReadSession(Serializable session) {
		System.out.println("------doReadSession----");
//继承自AbstractSessionDAO
		
// AbstractSessionDAO中 readSession的实现： 		
//	    public Session readSession(Serializable sessionId) throws UnknownSessionException {
//	        Session s = doReadSession(sessionId);
//	        if (s == null) {
//	            throw new UnknownSessionException("There is no session with id [" + sessionId + "]");
//	        }
//	        return s;
//	    }
// 直接父类中：CachingSessionDAO中  create  的实现：		
//	    public Session readSession(Serializable sessionId) throws UnknownSessionException {
//	        Session s = getCachedSession(sessionId);
//	        if (s == null) {
//	            s = super.readSession(sessionId);
//	        }
//	        return s;
//	    }		
		
		return null;//此处只要返回null即可
	}

	

	
	
}
