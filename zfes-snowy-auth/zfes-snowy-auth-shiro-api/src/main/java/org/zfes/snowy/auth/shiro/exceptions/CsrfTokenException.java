/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.exceptions;


public class CsrfTokenException extends StatelessTokenException {
	
	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new StatelessTokenException.
     */
    public CsrfTokenException() {
        super();
    }

    /**
     * Constructs a new StatelessTokenException.
     *
     * @param message the reason for the exception
     */
    public CsrfTokenException(String message) {
        super(message);
    }

    /**
     * Constructs a new StatelessTokenException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public CsrfTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new StatelessTokenException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public CsrfTokenException(String message, Throwable cause) {
        super(message, cause);
    }
   
}
