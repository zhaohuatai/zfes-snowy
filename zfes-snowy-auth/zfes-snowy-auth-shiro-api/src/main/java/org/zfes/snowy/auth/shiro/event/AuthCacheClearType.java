/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.event;

public enum AuthCacheClearType {
		cachedAuthenticationInfo,
		allCachedAuthenticationInfo,
		cachedAuthorizationInfoBySubject,
		cachedAuthorizationInfoByRole,
		allCachedAuthorizationInfo,
		allCache
}
