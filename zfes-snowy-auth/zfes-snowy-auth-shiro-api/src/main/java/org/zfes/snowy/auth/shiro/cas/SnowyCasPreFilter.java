/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.zfes.snowy.auth.shiro.cas;

import org.apache.shiro.web.servlet.OncePerRequestFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;


public class SnowyCasPreFilter extends OncePerRequestFilter {

	//cas server的登录url
	private String casServerLoinUlr;
	
	//cas sever 认证通过回调地址
	private String casAuthCallbackUlr;
	
	
	@Override
	protected void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain)
			throws ServletException, IOException {
//		WebUtils.toHttp(response).sendRedirect("http://127.0.0.1:8080/cas/?service=http://172.25.253.9:8083/km-gradms-core-server/api/cas");
		WebUtils.toHttp(response).sendRedirect(casServerLoinUlr+"?service="+casAuthCallbackUlr);
		
	}


	public String getCasServerLoinUlr() {
		return casServerLoinUlr;
	}


	public void setCasServerLoinUlr(String casServerLoinUlr) {
		this.casServerLoinUlr = casServerLoinUlr;
	}


	public String getCasAuthCallbackUlr() {
		return casAuthCallbackUlr;
	}


	public void setCasAuthCallbackUlr(String casAuthCallbackUlr) {
		this.casAuthCallbackUlr = casAuthCallbackUlr;
	}

	
    
    
}
