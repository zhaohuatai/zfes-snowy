/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.auth.shiro.filter.authc;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 只用来拦截登陆url，功能属性配置同SnowyTokenAuthenticationFilter.java
 * 
 * @author root
 *
 */
public class SnowyLoginPassThruAuthenticationFilter extends SnowyTokenAuthenticationFilter {
	@Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return super.isAccessAllowed(request, response, mappedValue) && (!isLoginSubmission(request, response) );
    }
	 

}
