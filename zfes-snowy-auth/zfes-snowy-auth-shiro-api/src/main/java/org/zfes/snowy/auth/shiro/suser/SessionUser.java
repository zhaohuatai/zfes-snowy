/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.suser;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/**
 * 
* @ClassName :SessionUser     
* @Description :   
* @createTime :2015年4月3日  下午4:17:50   
* @author ：zhaohuatai   
* @version :1.0
 */
public class SessionUser implements Serializable{
	private static final long serialVersionUID = 1L;
	
	protected Long userId;
	protected String username;
	protected String openId;
	protected Long roleId;
	protected String roleCode;
	protected String roleName;
	
	protected Long positionId;
	protected String positionName;
	protected String positionCode;
	
	protected Long orgId;
	protected String orgName;
	protected String orgCode;
	protected String orgTreeCode;
	/**
	 * 认证级别【登录用户】：例如 未注册，已注册，等 <br>
	 * 
	 * 0:游客(未注册)，1：用户注册但未审核，2：正常用户,
	 */
	protected int authLevel;
	
	private Map<String,Object> externMsg=new HashMap<>();
	
	public String getOrgTreeCode() {
		return orgTreeCode;
	}
	public SessionUser setOrgTreeCode(String orgTreeCode) {
		this.orgTreeCode = orgTreeCode;
		return this;
	}
	public String getPositionCode() {
		return positionCode;
	}
	public SessionUser setPositionCode(String positionCode) {
		this.positionCode = positionCode;
		return this;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public SessionUser setOrgCode(String orgCode) {
		this.orgCode = orgCode;
		return this;
	}
	public Long getUserId() {
		return userId;
	}
	public SessionUser setUserId(Long userId) {
		this.userId = userId;
		return this;
	}
	public String getUsername() {
		return username;
	}
	public SessionUser setUsername(String username) {
		this.username = username;
		return this;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public SessionUser setRoleCode(String roleCode) {
		this.roleCode = roleCode;
		return this;
	}
	public String getRoleName() {
		return roleName;
	}
	public SessionUser setRoleName(String roleName) {
		this.roleName = roleName;
		return this;
	}
	public Long getPositionId() {
		return positionId;
	}
	public SessionUser setPositionId(Long positionId) {
		this.positionId = positionId;
		return this;
	}
	public String getPositionName() {
		return positionName;
	}
	public SessionUser setPositionName(String positionName) {
		this.positionName = positionName;
		return this;
	}
   

	public Long getOrgId() {
		return orgId;
	}
	public SessionUser setOrgId(Long orgId) {
		this.orgId = orgId;
		return this;
	}
	public String getOrgName() {
		return orgName;
	}
	public SessionUser setOrgName(String orgName) {
		this.orgName = orgName;
		return this;
	}
	public Map<String, Object> getExternMsg() {
		return externMsg;
	}
	public SessionUser setExternMsg(Map<String, Object> externMsg) {
		this.externMsg = externMsg;
		return this;
	}
	public int getAuthLevel() {
		return authLevel;
	}
	public SessionUser setAuthLevel(int authLevel) {
		this.authLevel = authLevel;
		return this;
	}
	public String getOpenId() {
		return openId;
	}
	public SessionUser setOpenId(String openId) {
		this.openId = openId;
		return this;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "SessionUser [userId=" + userId + ", username=" + username + ", openId=" + openId + ", roleCode="
				+ roleCode + ", roleName=" + roleName + ", positionId=" + positionId + ", positionName=" + positionName
				+ ", positionCode=" + positionCode + ", orgId=" + orgId + ", orgName=" + orgName + ", orgCode="
				+ orgCode + ", orgTreeCode=" + orgTreeCode + ", authLevel=" + authLevel + ", externMsg=" + externMsg
				+ "]";
	}


	
}
