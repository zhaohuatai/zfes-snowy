/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.remote;
import java.io.Serializable;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;

public interface IRemoteAuthService  {
	
	@Deprecated
	public Session getSession(Serializable sessionId);
	@Deprecated
	public Serializable createSession(Session session);
	@Deprecated
    public void updateSession(Session session);
	@Deprecated
    public void deleteSession(Session session);

//-----------------------------------------------------------
    public AuthenticationInfo doGetRemoteAuthenticationInfo(String appKey,AuthenticationToken token);
    
    public AuthorizationInfo doGetRemoteAuthorizationInfo(String appKey,PrincipalCollection principals);
    



}
