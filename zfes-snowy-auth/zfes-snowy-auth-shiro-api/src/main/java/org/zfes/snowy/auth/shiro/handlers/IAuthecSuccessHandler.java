/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.subject.Subject;

public interface IAuthecSuccessHandler {
	
	
	void onAuthenticationSuccess(HttpServletRequest request,HttpServletResponse response, Subject subject,boolean isGenCSRFToken)throws IOException, ServletException;
	
}
