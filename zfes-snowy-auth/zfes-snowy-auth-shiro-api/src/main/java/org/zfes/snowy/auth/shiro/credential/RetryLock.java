/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.credential;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class RetryLock implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AtomicInteger retryCount;
	private Long lastRetryTime;
	
	
	public RetryLock(AtomicInteger retryCount, Long lastRetryTime) {
		super();
		this.retryCount = retryCount;
		this.lastRetryTime = lastRetryTime;
	}
	public AtomicInteger getRetryCount() {
		return retryCount;
	}
	public void setRetryCount(AtomicInteger retryCount) {
		this.retryCount = retryCount;
	}
	public Long getLastRetryTime() {
		return lastRetryTime;
	}
	public void setLastRetryTime(Long lastRetryTime) {
		this.lastRetryTime = lastRetryTime;
	}
	
	
}
