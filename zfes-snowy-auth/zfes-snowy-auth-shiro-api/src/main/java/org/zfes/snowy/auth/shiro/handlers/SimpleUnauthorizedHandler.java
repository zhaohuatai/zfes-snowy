/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.handlers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.JSONMSG;
import org.zfes.snowy.core.util.ZWebUtil;

public class SimpleUnauthorizedHandler extends AbstractAuthorizHandler {
	
	private String defaultUnauthorizedUrl="pages/401.html";
	
	private boolean unauthorizedForwardToDestination = false;
	
	public void onUnauthorized(HttpServletRequest request,HttpServletResponse response)throws IOException{
		if(ZWebUtil.isAjax(request)){
			ZWebUtil.respJSON(response, JSONMSG.newMSG(FrameConst.SC.SC_401, "授权失败"));
			return;
		}
		if(unauthorizedForwardToDestination){
			saveRequestAndForward( request,  response, getDefaultUnauthorizedUrl()) ;
			return;
		}else{
			saveRequestAndRedirect( request,  response, getDefaultUnauthorizedUrl());
			return;
		}
	}

	public String getDefaultUnauthorizedUrl() {
		return defaultUnauthorizedUrl;
	}

	public void setDefaultUnauthorizedUrl(String defaultUnauthorizedUrl) {
		this.defaultUnauthorizedUrl = defaultUnauthorizedUrl;
	}

	public boolean isUnauthorizedForwardToDestination() {
		return unauthorizedForwardToDestination;
	}

	public void setUnauthorizedForwardToDestination(boolean unauthorizedForwardToDestination) {
		this.unauthorizedForwardToDestination = unauthorizedForwardToDestination;
	}

	
	
}
