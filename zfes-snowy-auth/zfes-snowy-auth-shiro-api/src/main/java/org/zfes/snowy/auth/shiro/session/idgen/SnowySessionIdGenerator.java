package org.zfes.snowy.auth.shiro.session.idgen;

import java.io.Serializable;
import java.util.UUID;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;

public class SnowySessionIdGenerator implements SessionIdGenerator {

   
    public Serializable generateId(Session session) {
    	return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
