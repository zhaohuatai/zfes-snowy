/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.session.dao;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.zfes.snowy.auth.shiro.session.repository.IShiroSessionRepository;

import java.io.Serializable;
import java.util.Collection;

/**
 * 
* @ClassName :ShiroRedisSessionDAO_A     
* @Description :   
* @createTime :2015年5月7日  上午9:29:54   
* @author ：zhaohuatai   
* @version :1.0
 */

public class SnowyShiroSessionDAO extends AbstractSessionDAO {
	
	private String activeSessionsCacheName = "sh_sessions";
	
    private int globalSessionTimeout = 18000;
    
	/**
	 * ShiroSession DAO存取接口
	 */
    private IShiroSessionRepository shiroSessionRepository;
    
    public IShiroSessionRepository getShiroSessionRepository() {
        return shiroSessionRepository;
    }

    public void setShiroSessionRepository( IShiroSessionRepository shiroSessionRepository) {
        this.shiroSessionRepository = shiroSessionRepository;
       // this.shiroSessionRepository.setSessionLiveTime(globalSessionTimeout);
    }
    //-------------------------------------------------------------------------------------
    /*
     * (non-Javadoc)
     * @see org.apache.shiro.session.mgt.eis.SessionDAO#update(org.apache.shiro.session.Session)
     */
    @Override
    public void update(Session session) throws UnknownSessionException {
    	System.out.println("update sessionid:"+session.getId());
        getShiroSessionRepository().updateSession(session);
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.session.mgt.eis.SessionDAO#delete(org.apache.shiro.session.Session)
     */
    @Override
    public void delete(Session session) {
    	//System.out.println("delete sessionid:"+session.getId());
        Serializable id = session.getId();
        if (id != null) {
            getShiroSessionRepository().deleteSession((String)id);
        }
    }

    /*
     * 注意此处：在线用户量大的话 会对内存 形成巨大压力，
     * 后期意向改成分批处理
     * (non-Javadoc)
     * @see org.apache.shiro.session.mgt.eis.SessionDAO#getActiveSessions()
     */
    @Override
    public Collection<Session> getActiveSessions() {
    	// 	System.out.println("getActiveSessions");
        return getShiroSessionRepository().getAllSessions();
    }

	/*
	 * (non-Javadoc)
	 * @see org.apache.shiro.session.mgt.eis.AbstractSessionDAO#doCreate(org.apache.shiro.session.Session)
	 */
    @Override
    protected Serializable doCreate(Session session) {
    	//	System.out.println("doCreate sessionid:"+session.getId());
// AbstractSessionDAO中 create的实现： 
//	    public Serializable create(Session session) {
//	        Serializable sessionId = doCreate(session);
//	        verifySessionId(sessionId);
//	        return sessionId;
//	    }
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        getShiroSessionRepository().saveSession(session);
        return sessionId;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.shiro.session.mgt.eis.AbstractSessionDAO#doReadSession(java.io.Serializable)
     */
    @Override
    protected Session doReadSession(Serializable sessionId) {
    	//System.out.println("doReadSession sessionid:"+sessionId);
    	//	System.out.println("doReadSession");
    	// AbstractSessionDAO中 readSession的实现： 		
//	    public Session readSession(Serializable sessionId) throws UnknownSessionException {
//	        Session s = doReadSession(sessionId);
//	        if (s == null) {
//	            throw new UnknownSessionException("There is no session with id [" + sessionId + "]");
//	        }
//	        return s;
//	    }
        return getShiroSessionRepository().getSession((String)sessionId);
    }

	public String getActiveSessionsCacheName() {
		return activeSessionsCacheName;
	}

	public void setActiveSessionsCacheName(String activeSessionsCacheName) {
		this.activeSessionsCacheName = activeSessionsCacheName;
	}

	public int getGlobalSessionTimeout() {
		return globalSessionTimeout;
	}

	public void setGlobalSessionTimeout(int globalSessionTimeout) {
		this.globalSessionTimeout = globalSessionTimeout;
	}

   

}
