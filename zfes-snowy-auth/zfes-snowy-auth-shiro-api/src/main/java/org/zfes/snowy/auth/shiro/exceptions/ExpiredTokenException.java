/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.exceptions;


/**
 * A special case of a ExpiredStatelessTokenException.  An expired StatelessToken is a StatelessToken that has
 * stopped explicitly due to inactivity (i.e. time-out), as opposed to stopping due to log-out or
 * other reason.
 *
 * @since 0.1
 */
public class ExpiredTokenException extends InvalidTokenException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new ExpiredSessionException.
     */
    public ExpiredTokenException() {
        super();
    }

    /**
     * Constructs a new ExpiredSessionException.
     *
     * @param message the reason for the exception
     */
    public ExpiredTokenException(String message) {
        super(message);
    }

    /**
     * Constructs a new ExpiredSessionException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public ExpiredTokenException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new ExpiredSessionException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public ExpiredTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
