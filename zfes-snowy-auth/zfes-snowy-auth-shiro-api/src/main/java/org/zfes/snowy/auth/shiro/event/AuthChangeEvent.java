/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.event;

import org.apache.shiro.subject.Subject;

public class AuthChangeEvent {

	private Subject subject;
	private String roleCode;
	private AuthCacheClearType cacheType;

	public AuthChangeEvent(Subject subject, AuthCacheClearType cacheType) {
		super();
		this.subject = subject;
		this.cacheType = cacheType;
	}
	public AuthChangeEvent(Subject subject, AuthCacheClearType cacheType,String roleCode) {
		super();
		this.subject = subject;
		this.cacheType = cacheType;
		this.roleCode = roleCode;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public AuthChangeEvent setRoleCode(String roleCode) {
		this.roleCode = roleCode;
		return this;
	}


	public Subject getSubject() {
		return subject;
	}

	public AuthChangeEvent setSubject(Subject subject) {
		this.subject = subject;
		return this;
	}


	public AuthCacheClearType getCacheType() {
		return cacheType;
	}


	public AuthChangeEvent setCacheType(AuthCacheClearType cacheType) {
		this.cacheType = cacheType;
		return this;
	}


	
	
}
