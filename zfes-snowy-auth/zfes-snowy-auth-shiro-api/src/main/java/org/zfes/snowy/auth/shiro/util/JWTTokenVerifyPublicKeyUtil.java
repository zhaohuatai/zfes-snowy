/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.util;

import java.util.Map;

import org.zfes.snowy.auth.AuthConsts;
import org.zfes.snowy.core.exceptions.AppRuntimeException;
import org.zfes.snowy.core.util.UUIDUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.core.util.encypt.EncyptFileUtil;
import org.zfes.snowy.core.util.encypt.RSAUtil;

public class JWTTokenVerifyPublicKeyUtil {
	
	private static String csrfVerifyPublicKey=null;
	private static String jwtVerifyPublicKey=null;
	private static final String maven_middle_path="/src/main/resources/";
	private static final String target="target";
	/**
	 * <h1>读取非对称加密RSA加密之后的数据，该数据作为后面操作中对称加密的公钥</h1>
	 * @return
	 */
	public static void createCsrfVerifyKey(){
		createTokenVerifyPublicKey(AuthConsts.ENCYPT.csrf_rsa_data_file_name,AuthConsts.ENCYPT.csrf_rsa_privateKey_file_name);
	}
	/**
	 * <h1>读取非对称加密RSA加密之后的数据，该数据作为后面操作中对称加密的公钥</h1>
	 * @return
	 */
	public static String readCsrfVerifyPublicKey(){
		if(csrfVerifyPublicKey==null){
			synchronized (JWTTokenVerifyPublicKeyUtil.class) {
				 if (csrfVerifyPublicKey == null) {  
					 csrfVerifyPublicKey =readTokenVerifyPublicKey(AuthConsts.ENCYPT.csrf_rsa_data_file_name,AuthConsts.ENCYPT.csrf_rsa_privateKey_file_name);
			        }  
			}
		}
		if(!ZStrUtil.hasText(csrfVerifyPublicKey)){
			throw new AppRuntimeException("系统未生成csrf Verify PublicKey");
		}
		return csrfVerifyPublicKey;
	}
//-------------------------------------------------------------------------------------------------------------	
	/**
	 * <h1>读取非对称加密RSA加密之后的数据，该数据作为后面操作中对称加密的公钥</h1>
	 * @return
	 */
	public static void createJwtVerifyKey(){
		createTokenVerifyPublicKey(AuthConsts.ENCYPT.jwt_rsa_data_file_name,AuthConsts.ENCYPT.jwt_rsa_privateKey_file_name);
	}
	public static void main(String[] args) {
		createJwtVerifyKey();
	}
	/**
	 * <h1>读取非对称加密RSA加密之后的数据，该数据作为后面操作中对称加密的公钥</h1>
	 * @return
	 */
	public static String readJwtVerifyPublicKey(){
		if(jwtVerifyPublicKey==null){
			synchronized (JWTTokenVerifyPublicKeyUtil.class) {
				 if (jwtVerifyPublicKey == null) {  
					 jwtVerifyPublicKey =readTokenVerifyPublicKey(AuthConsts.ENCYPT.jwt_rsa_data_file_name,AuthConsts.ENCYPT.jwt_rsa_privateKey_file_name);
			        }  
			}
		}
		if(!ZStrUtil.hasText(jwtVerifyPublicKey)){
			throw new AppRuntimeException("系统未生成jwt Verify PublicKey");
		}
		return jwtVerifyPublicKey;
	}
	//-------------------------------------------------------------------------------------------------------------
	private static void createTokenVerifyPublicKey(String encryptedDataFileName,String privateKeyDataFileName){
		
	    Map<String, Object> keyMap = RSAUtil.genKeyPair();
	  
	    String publicKey  = RSAUtil.getPublicKey(keyMap);
	    String privateKey = RSAUtil.getPrivateKey(keyMap);
	    byte[] data       = UUIDUtil.base58Uuid().getBytes(); 
	    
        byte[] encryptedData = RSAUtil.encryptByPublicKey(data, publicKey);
        
        
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath().replaceAll("%20", " ");
        path=ZStrUtil.substringBeforeLast(path,target )+maven_middle_path+AuthConsts.ENCYPT.signature_path;
        
        EncyptFileUtil.saveData(encryptedData,path,encryptedDataFileName);//保存用公钥加密后的---数据
        EncyptFileUtil.saveKey(privateKey, path,privateKeyDataFileName);//保存私钥
	}
	private static String readTokenVerifyPublicKey(String encryptedDataFileName,String privateKeyDataFileName){
		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath()+AuthConsts.ENCYPT.signature_path.replaceAll("%20", " ");
		byte[] data= EncyptFileUtil.readData(path,encryptedDataFileName);
       
        String privateKey=EncyptFileUtil.readKey(path,privateKeyDataFileName);
        
        byte[] encryptedData = RSAUtil.decryptByPrivateKey(data, privateKey);
        String sourceData = new String(encryptedData);
        return sourceData;
	}
	
	
}
