/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.filter.authc;


import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;

public abstract class SnowyAccessControlFilter extends PathMatchingFilter {
	
	 protected Subject getSubject(ServletRequest request, ServletResponse response) {
	     return SecurityUtils.getSubject();
	 }
	
	  @Override
	  public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
	     return isAccessAllowed(request, response, mappedValue) || onAccessDenied(request, response, mappedValue);
	  }
	  
	  protected boolean onAccessDenied(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
	       return onAccessDenied(request, response);
	  }	
	  
//--------------------------------sub class extend-------------------------------------------------	  
	  protected abstract boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue)throws Exception ;
	  
	  protected abstract boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception;
	  

//-------------------------------------------------------------------------------------------------------	  
}
