/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.cache.manger;

import org.apache.shiro.ShiroException;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.util.Destroyable;
import org.apache.shiro.util.Initializable;
import org.zfes.snowy.auth.shiro.cache.cache.ShiroRedisCache;
import org.zfes.snowy.common.nosql.redis.jedis.SnowyJedisManager;

/**
 *该类提供Cache的一个实现<br/>
* @ClassName :JedisShiroCacheManager     
* @Description :   
* @createTime :2015年5月7日  上午9:42:59   
* @author ：zhaohuatai   
* @version :1.0
 */
public class ShiroRedisCacheManager implements org.apache.shiro.cache.CacheManager ,Initializable, Destroyable {
	
    private SnowyJedisManager jedisManager;
    
    

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException{
    	
        return new ShiroRedisCache<K, V>(name, getJedisManager());
        
    }

    @Override
    public void destroy() {
    }

	@Override
	public void init() throws ShiroException {
		
		
	}
//----------------------------------------------------------
    
    public SnowyJedisManager getJedisManager() {
        return jedisManager;
    }

    public void setJedisManager(SnowyJedisManager jedisManager) {
        this.jedisManager = jedisManager;
    }
    

	//--------------------------------------------------------------------------------
  
}
