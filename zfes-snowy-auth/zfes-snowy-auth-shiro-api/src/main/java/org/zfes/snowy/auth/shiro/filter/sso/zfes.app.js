
var host_auth="";

var hostArrays=[host_gradms_core,km_gradms_recruit];
var zfesApp = {
		sync_api:"/go/app/sync/sid/api.php",
		syncsid:"syncsid",
		setCookie:function setCookie(c_name,value,expiredays,path){
			path=(path==undefined|| path==null)?"/":path+"/"
			var exdate=new Date()
			exdate.setDate(exdate.getDate()+expiredays)
			document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toUTCString())+";path="+path//HttpOnly;
		},
		getCookie:function getCookie(name) { 
		    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
		    if(arr=document.cookie.match(reg))
		        return unescape(arr[2]); 
		    else 
		        return null; 
		},
		isSidSyn:function(){
			var syncsid=zfesApp.getCookie(zfesApp.syncsid);
			if(syncsid=="true"){
				return true;
			}
			return false;
		},	
		serverHostSyncSid:function(syncCookieServerHost , syncCookieClientHost){
			if(zfesApp.isSidSyn()){
				//return;
			}
			 $.ajax({ 
				 url : syncCookieServerHost+zfesApp.sync_api,
				 dataType : "jsonp",
				 jsonp : "jsonpCallback",
		         success : function(json){
		        	 let s_si_=json.s_si_;
		        	 let s_tk_=json.s_tk_;
		        	 let param={"s_si_":s_to_,"s_tk_":s_tk_};
		           	 $.ajax({ 
						 url : syncCookieClientHost+zfesApp.sync_api,
						 dataType : "jsonp",
						 jsonp : "jsonpCallback",
						 data : param,
				         success : function(json){},
				         error:function(){ 
				        	 alert("认证中心：\n\n["+syncCookieClientHost+"]\n\n系统未启动或者出现错误：请联系运维人员");
				            	if (window.stop){
				            		window.stop();
				            	}else{
				            		document.execCommand("Stop");
				            	}
				            }
				        });
		            },
		            error:function(){ 
		            	//alert("["+syncCookieServerHost+"]\n\n系统未启动或者出现错误：请联系运维人员");
		            	if (window.stop){
		            		//window.stop();
		            	}else{
		            		//document.execCommand("Stop");
		            	}
		            }
		        });
		},
		syncHostSid:function(syncCookieServerHost , syncCookieClientHost){
			if(zfesApp.isSidSyn()){
				//return;
			}
			 $.ajax({ 
				 url : syncCookieServerHost+zfesApp.sync_api,
				 dataType : "jsonp",
				 jsonp : "jsonpCallback",
		         success : function(json){
		        	 var ssoSign=json.ssoSign;
		        	 var ssoToken=json.ssoToken;
		        	 var param={"ssoSign":ssoSign,"ssoToken":ssoToken};
		           	 $.ajax({ 
						 url : syncCookieClientHost+zfesApp.sync_api,
						 dataType : "jsonp",
						 jsonp : "jsonpCallback",
						 data : param,
				         success : function(json){},
				         error:function(){ 
				        	 alert("["+syncCookieClientHost+"]\n\n系统未启动或者出现错误：请联系运维人员");
				            	if (window.stop){
				            		window.stop();
				            	}else{
				            		document.execCommand("Stop");
				            	}
				            }
				        });
		            },
		            error:function(){ 
		            	alert("["+syncCookieServerHost+"]\n\n系统未启动或者出现错误：请联系运维人员");
		            	if (window.stop){
		            		window.stop();
		            	}else{
		            		document.execCommand("Stop");
		            	}
		            }
		        });
		}
}
