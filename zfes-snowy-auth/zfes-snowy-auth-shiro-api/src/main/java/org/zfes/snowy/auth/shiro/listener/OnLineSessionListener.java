/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.listener;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;


public class OnLineSessionListener implements SessionListener{

	@Override
	public void onExpiration(Session session) {
	
	}

	@Override
	public void onStart(Session session) {
	}

	@Override
	public void onStop(Session session) {
		
	}


}
