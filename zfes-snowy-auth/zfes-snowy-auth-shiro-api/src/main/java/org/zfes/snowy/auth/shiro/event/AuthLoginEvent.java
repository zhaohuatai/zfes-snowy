/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.event;

import org.apache.shiro.subject.Subject;

public class AuthLoginEvent {

	private Subject subject;
	private String userName;

	public AuthLoginEvent(Subject subject, String userName) {
		super();
		this.subject = subject;
		this.userName = userName;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
