/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.filter.authc;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.apache.shiro.web.servlet.AdviceFilter;
import org.apache.shiro.web.util.WebUtils;
import org.zfes.snowy.auth.shiro.handlers.IAuthecFailureHandler;
import org.zfes.snowy.core.util.ZStrUtil;
/**
 * 未实现： 多终端登陆类型，多session控制<br>
 * @author root
 *
 */
public class SnowyMultiLoginLimitFilter extends AdviceFilter{

	private  SessionDAO shiroSessionDAO;
	private Integer maxSession;
	private IAuthecFailureHandler authecFailureHandler;
	
	@Override
	protected void postHandle(ServletRequest request, ServletResponse response)throws Exception {
		Subject subject = SecurityUtils.getSubject();
		//登陆失败------
		if(!subject.isAuthenticated() && !subject.isRemembered()) {
			  //如果没有登录， 直接进行之后的流程
		
			authecFailureHandler.onAuthenticationFailure(WebUtils.toHttp(request), WebUtils.toHttp(response), new AuthenticationException("来自Session策略的异常---认证失败") );
		}
		Session newLonginSession = subject.getSession();
		String username = (String) subject.getPrincipal();
		Serializable newLonginSessionId = newLonginSession.getId();
		Collection<Session> activeSessions = shiroSessionDAO.getActiveSessions();
		if(activeSessions!=null){
			List<Session> sessionList=activeSessions.parallelStream().filter( sessionx->ZStrUtil.equals(String.valueOf(sessionx.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY)), username)).collect(Collectors.toList());
			if(sessionList!=null){
				if(sessionList.size() <= maxSession){
					super.postHandle(request, response);
					return;
				}
				for(Session sessionz:sessionList){
					
					if(!sessionz.getId().equals(newLonginSessionId)){
//						shiroSessionDAO.delete(sessionz);
						sessionz.stop();
					}
					
				}
			}
		
		}
//		if(activeSessions!=null&&activeSessions.size()>0){
//			for(Session session : activeSessions){
//				if(null != session&&ZStrUtil.equals(String.valueOf(session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY)), username)){
//					if(!session.getId().equals(newLonginSessionId)){
//						shiroSessionDAO.delete(session);
//					}
//				}
//				
//			}
//		}
		
		super.postHandle(request, response);
	}

	public SessionDAO getShiroSessionDAO() {
		return shiroSessionDAO;
	}

	public void setShiroSessionDAO(SessionDAO shiroSessionDAO) {
		this.shiroSessionDAO = shiroSessionDAO;
	}

	public Integer getMaxSession() {
		return maxSession;
	}
	public void setMaxSession(Integer maxSession) {
		this.maxSession = maxSession;
	}

	
	
	

}
