/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.shiro.session;

import java.io.Serializable;

import javax.annotation.Resource;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.zfes.snowy.auth.shiro.remote.IRemoteAuthService;
import org.zfes.snowy.core.util.AppCtxUtil;
@Deprecated
public class AppClientSessionDAO extends CachingSessionDAO{
	
	@Resource(name="remoteAuthService")
    private IRemoteAuthService remoteAuthService;

    public void setRemoteAuthService(IRemoteAuthService remoteAuthService) {
        this.remoteAuthService = remoteAuthService;
    }
   public IRemoteAuthService getRemoteAuthService() {
	   if(remoteAuthService==null){
		   remoteAuthService=AppCtxUtil.getBean("remoteAuthService");
	   }
	   return remoteAuthService;
   }

    
	@Override
    protected void doDelete(Session session) {
    	//System.out.println("AppClientSessionDAO--doDelete:--id:"+session.getId());
    	remoteAuthService.deleteSession( session);
    }

    @Override
    protected void doUpdate(Session session) {
    	// System.out.println("AppClientSessionDAO--doUpdate:--id:"+session.getId());
    	remoteAuthService.updateSession( session);
    }

  
    @Override
    protected Serializable doCreate(Session session) {
    	//System.out.println("AppClientSessionDAO--doCreate:--id:"+session.getId());
        Serializable sessionId = remoteAuthService.createSession(session);
        assignSessionId(session, sessionId);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
    	//System.out.println("AppClientSessionDAO--doReadSession:--id:"+sessionId);
    	Session session=remoteAuthService.getSession( sessionId);
        return session;
    }
}
