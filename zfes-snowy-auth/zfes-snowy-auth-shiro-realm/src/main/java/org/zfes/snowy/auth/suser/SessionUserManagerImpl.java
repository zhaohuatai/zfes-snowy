/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.suser;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zfes.snowy.auth.biz.model.AuthRole;
import org.zfes.snowy.auth.biz.model.AuthUser;
import org.zfes.snowy.auth.biz.service.IAuthRoleService;
import org.zfes.snowy.auth.biz.service.IAuthUserService;
import org.zfes.snowy.auth.shiro.suser.SessionUser;
import org.zfes.snowy.auth.shiro.suser.SessionUserManager;

@Service
public class SessionUserManagerImpl implements SessionUserManager{

	@Autowired
	IAuthRoleService  authRoleService;
	@Autowired
	IAuthUserService authUserService;

	
	@Override
	public Optional<SessionUser>  loadSessionUserByOpenId(String openId) {
		
		Optional<AuthUser> userOp= authUserService.loadByOpenId(openId);
		if(!userOp.isPresent()){
			return Optional.empty();
		}
		SessionUser sessionUser=new SessionUser();
		AuthUser user=userOp.get();
		sessionUser.setUserId(user.getId());
		sessionUser.setUsername(userOp.get().getAccount());
		sessionUser.setAuthLevel(user.getAuthLevel()==null?0:user.getAuthLevel());
		
		Optional<AuthRole> roleOp=authRoleService.loadAuthRole(user.getDefaultRole());
		if(roleOp.isPresent()){
			AuthRole role=roleOp.get();
			sessionUser.setRoleName(role.getName());
			sessionUser.setRoleCode(role.getRoleCode());
		}
		sessionUser.setPositionId(user.getDefaultPosition());
		return Optional.of(sessionUser);
	}
	@Override
	public Optional<SessionUser>  loadSessionUser(String userName) {
		
		Optional<AuthUser> userOp= authUserService.loadByAccount(userName);
		if(!userOp.isPresent()){
			return Optional.empty();
		}
		SessionUser sessionUser=new SessionUser();
		AuthUser user=userOp.get();
		sessionUser.setUserId(user.getId());
		sessionUser.setUsername(userName);
		sessionUser.setAuthLevel(user.getAuthLevel()==null?0:user.getAuthLevel());
		
		Optional<AuthRole> roleOp=authRoleService.loadAuthRole(user.getDefaultRole());
		if(roleOp.isPresent()){
			AuthRole role=roleOp.get();
			sessionUser.setRoleName(role.getName());
			sessionUser.setRoleCode(role.getRoleCode());
			sessionUser.setRoleId(role.getId());
		}
		sessionUser.setPositionId(user.getDefaultPosition());
//		Optional<SysPosition> position=sysPositionService.loadSysPosition(user.getDefaultPosition());
//		if(position.isPresent()){
//			sessionUser.setPositionName(position.get().getName());
//			sessionUser.setPositionCode(position.get().getCode());
//		}
		return Optional.of(sessionUser);
	}

	public IAuthRoleService getAuthRoleService() {
		return authRoleService;
	}

	public void setAuthRoleService(IAuthRoleService authRoleService) {
		this.authRoleService = authRoleService;
	}

	public IAuthUserService getAuthUserService() {
		return authUserService;
	}

	public void setAuthUserService(IAuthUserService authUserService) {
		this.authUserService = authUserService;
	}
	
}
