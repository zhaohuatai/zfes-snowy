/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.auth.shiro.remote;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.zfes.snowy.auth.shiro.realm.SnowyRemoteRealm;
import org.zfes.snowy.auth.shiro.remote.IRemoteAuthService;
import java.io.Serializable;

public class RemoteAuthServiceImpl  implements IRemoteAuthService {

    private SnowyRemoteRealm snowyRealm;
    
    private SessionDAO sessionDAO;
    @Deprecated
    @Override
    public Session getSession( Serializable sessionId) {
    	return sessionDAO.readSession(sessionId);
    }
    @Deprecated
    @Override
    public Serializable createSession(Session session) {
        return sessionDAO.create(session);
    }
    @Deprecated
    @Override
    public void updateSession( Session session) {
        sessionDAO.update(session);
    }
    @Deprecated
    @Override
    public void deleteSession(Session session) {
        sessionDAO.delete(session);
    }
    public AuthorizationInfo doGetRemoteAuthorizationInfo(String appKey, PrincipalCollection principals){
    	return snowyRealm.remoteDoGetAuthorizationInfo(appKey,principals);
    }

	@Override
	public AuthenticationInfo doGetRemoteAuthenticationInfo(String appKey,AuthenticationToken token) {
		return snowyRealm.remoteDoGetAuthenticationInfo(appKey,token);
	}

	
	public SnowyRemoteRealm getSnowyRealm() {
		return snowyRealm;
	}
	public void setSnowyRealm(SnowyRemoteRealm snowyRealm) {
		this.snowyRealm = snowyRealm;
	}
	public SessionDAO getSessionDAO() {
		return sessionDAO;
	}

	public void setSessionDAO(SessionDAO sessionDAO) {
		this.sessionDAO = sessionDAO;
	}


}
