/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.manager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthApp;
import org.zfes.snowy.auth.biz.service.IAuthAppService;
import org.zfes.snowy.auth.biz.service.IAuthMenuService;
import org.zfes.snowy.auth.biz.view.MenuView;
import org.zfes.snowy.auth.shiro.util.SecurityUtil;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.util.ZStrUtil;

@Controller
@RequestMapping("/auth")
public class AuthController extends BaseController  {
	@Autowired
	private IAuthAppService authAppService;
	@Autowired
	private IAuthMenuService authMenuService;
	@Autowired
	private IAuthManager authManager;
	
	@ResponseBody
	@RequestMapping(value="/loadMenu")
	public Object loadMenu(String appkey) {
		Optional<Long> userIdOp=SecurityUtil.getUserId();
		Optional<MenuView> menuView =authMenuService.loadUserMenus(userIdOp.get(),appkey,false);
		if(!menuView.isPresent()){
			return ajaxDoneError("未查询到菜单数据");
		}
		return ajaxDoneSuccess("").put("navMenuData", menuView.get().getItems());
	}
	
	@ResponseBody 
	@RequiresAuthentication
    @RequestMapping(value="/loadAuthCodes")
    public Object loadAuthCodes(String appkey) {
		Long userId=SecurityUtil.getUserId().get();
		if(userId==null){
			return  Collections.emptyList();
		}
		if(ZStrUtil.hasNoText(appkey)) {
			appkey="APP_ROOT";
		}
		List<String> permCodes=authManager.loadPermCodesInUserDefaultRole(userId, appkey, true);//authPermissionService.loadPermCodesInUserPermByUserId(userId,appkey,true);
		Optional<String> roleCodes=authManager.loadUserDefualtRoleCode(userId, true);
		return ajaxQuery("permCodes", permCodes).put("roleCodes", roleCodes);
    }
	
	@ResponseBody 
	@RequiresAuthentication
    @RequestMapping(value="/loadAllEnabledPermCodes")
	public Object loadAllEnabledPermCodes(String appkey){
		//loadSessionUser();
		Long userId=SecurityUtil.getUserId().get();
		if(userId==null){
			return  Collections.emptyList();
		}
		if(!ZStrUtil.hasText(appkey)){
			return Collections.emptyList();
		}
		Optional<AuthApp> app=authAppService.loadAuthAppByAppKey(appkey);
		if(!app.isPresent()){
			return Collections.emptyList();
		}
		List<Long> appIds=new ArrayList<>();
		appIds.add(app.get().getId());
		List<String> permsIdList=null;//=authPermissionService.loadAllEnabledPermCodes(userId,appIds);
		return permsIdList;
	}
	
	
//	@ResponseBody 
//    @RequestMapping(value="/loadPositionCodes")
//	@RequiresAuthentication
//    public Object loadPositionCodes() {
//		Long userId=SecurityUtil.getUserId().get();
//		if(userId==null){
//			return  Collections.emptyList();
//		}
//		List<String> positionCodes=positionAuthUserService.loadPositionCodesByUserId(userId, true);
//		return positionCodes;
//    }
	
	@RequestMapping(value="/switchPosition")
	public @ResponseBody Object switchPosition(Long positionId){
		//Subject subject=SecurityUtil.getSubject();
		//sysPositionService.switchPosition(subject,positionId);
		return ajaxDoneSuccess("数据操作成功 ");
	}
	
	@RequestMapping(value="/switchRole")
	public @ResponseBody Object switchRole(Long roleId){
		//Subject subject=SecurityUtil.getSubject();
		return ajaxDoneSuccess("数据操作成功 ");
	}
}