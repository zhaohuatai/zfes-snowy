/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zfes.snowy.auth.biz.dao.AuthGroupUserMapper;
import org.zfes.snowy.auth.biz.model.AuthGroupUser;
import org.zfes.snowy.auth.biz.service.IAuthGroupUserService;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.core.util.ZAlert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

@Service
@Lazy(true)
public class AuthGroupUserServiceImpl implements IAuthGroupUserService{
 	@Autowired
	private AuthGroupUserMapper authGroupUserMapper;
 	
 	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<Long> loadGroupIdsByUserId(Long userId, Boolean enabled) {
		if(userId != null){
			return	authGroupUserMapper.selectGroupIdsByUserId(userId,  enabled);
		}
		return Collections.emptyList();
	}
 	
 	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<Long> loadUserIdsByGroupId(Long groupId, Boolean enabled) {
		if(groupId != null){
			return	authGroupUserMapper.selectUserIdsByGroupId(groupId, enabled);
		}
		return Collections.emptyList();
	}
 	
 	@Transactional(rollbackFor=Exception.class)
	@Override
	public void addUserToGroupUser(List<Long> userIds, Long groupId) {
		if(userIds==null||userIds.isEmpty()||groupId==null){
			ZAlert.serviceLogicalError("请选择数据");
		}
		List<AuthGroupUser> aguList = userIds.stream().distinct()
				.map(userId -> new AuthGroupUser(IDGenerator.genLongId(),groupId, userId))
				.collect(Collectors.toList());
		
		authGroupUserMapper.batchInsertGroupUser(aguList);
	}
 	
 	@Transactional(rollbackFor=Exception.class)
	@Override
	public void removeUserFromGroupUser(List<Long> userIds, Long groupId) {
		if(userIds==null||userIds.isEmpty()||groupId==null){
			ZAlert.serviceLogicalError("请选择数据");
		}
		authGroupUserMapper.deleteUsersFromGroupUser(userIds, groupId);
	}
	
	
}