/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthModule;
import org.zfes.snowy.auth.biz.service.IAuthModuleService;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
@Controller 
@RequestMapping("/auth/authModule")
@Lazy(true)
public class AuthModuleController extends BaseController {

	@Autowired
	private IAuthModuleService authModuleService;
	
	@RequiresPermissions(value="authModule:loadAuthModuleList",desc="模块表列表")
    @RequestMapping(value="/loadAuthModuleList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthModuleList(String appKey,@RequestParam Map<String,Object> params){
	    DataSet dataSet=  authModuleService.loadAuthModuleList(appKey,params);
	 
	    return dataSet;
    }
	
	@RequiresPermissions(value="authModule:loadAuthModule",desc="模块表加载")
    @RequestMapping(value="/loadAuthModule", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthModule(Long id){
		Optional<AuthModule> authModule=authModuleService.loadAuthModule(id);
		return ajaxQuery("authModule", authModule);
    }
	@RequiresPermissions(value="authModule:addAuthModule",desc="模块表添加")
    @RequestMapping(value="/addAuthModule", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addAuthModule(AuthModule authModule){
		authModuleService.createAuthModule(authModule);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authModule:updateAuthModule",desc="系统模块表更新")
    @RequestMapping(value="/updateAuthModule", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateAuthModule(AuthModule authModule){
		authModuleService.updateAuthModule(authModule);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authModule:deletAuthModule",desc="模块表删除")
    @RequestMapping(value="/deletAuthModule", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletAuthModule(Long id){
		authModuleService.deletAuthModule(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
    @RequestMapping(value="/loadModuleComboboVo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadModuleComboboVo(Boolean firstEmpty,String appKey){
		 List<ComboboVo> moduleSelectVo=authModuleService.loadModuleComboboVo(appKey,firstEmpty);
		 return ajaxQuery("snowyselectdata", moduleSelectVo);
    }
}