/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthUser;
import org.zfes.snowy.auth.biz.service.IAuthUserPermissionService;
import org.zfes.snowy.auth.biz.service.IAuthUserRoleService;
import org.zfes.snowy.auth.biz.service.IAuthUserService;
import org.zfes.snowy.auth.shiro.util.SecurityUtil;
import org.zfes.snowy.auth.shiro.util.ShiroMD5EndecryptUtil;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.util.ZAssert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
@Controller 
@RequestMapping("/auth/authUser")
@Lazy(true)
public class AuthUserController extends BaseController {
	@Autowired
	private IAuthUserService authUserService;
	@Autowired
	private IAuthUserRoleService authUserRoleService;
	@Autowired
	private IAuthUserPermissionService authUserPermissionService;

	@RequiresPermissions(value="authUser:loadAuthUserList",desc="用户列表")
    @RequestMapping(value="/loadAuthUserList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthUserList(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  authUserService.loadAuthUserList(params);
	    return dataSet;
    }
	@RequiresPermissions(value="authUser:loadUpdateData",desc="用户加载")
    @RequestMapping(value="/loadUpdateData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadUpdateData(Long id){
		Optional<AuthUser> authUser=authUserService.loadAuthUser(id);
		return ajaxQuery("authUser", authUser);
    }
	
	@RequiresPermissions(value="authUser:updateUser",desc="用户更新")
    @RequestMapping(value="/updateUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateUser(AuthUser authUser){
		authUserService.updateAuthUser(authUser);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	//@RequiresPermissions(value="authUser:createAuthUser",desc="用户新增")
    @RequestMapping(value="/createAuthUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
   public @ResponseBody Object createAuthUser(AuthUser authUser) {
		Long userId=authUserService.createUser(authUser);
		return ajaxDoneSuccess("数据操作成功 ").put("userId", userId);
	}

	@RequiresPermissions(value="authUser:createAccount",desc="用户新增")
    @RequestMapping(value="/createAccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
   public @ResponseBody Object createAccount(String account,String password,String roleCode) {
		Long userId=authUserService.createAccount(account,password,roleCode);
		return ajaxDoneSuccess("数据操作成功 ").put("userId", userId);
	}
	//---------------------------------------------------------------------------------------
	
	@RequiresPermissions(value="authUser:setUserStatus",desc="更新用户状态")
	@RequestMapping(value="/setUserStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object setUserStatus(
			@ListParam(Long.class) List<Long> userIds,
			Boolean enabled, 
			Boolean accountExpired, 
			Boolean accountLocked,
			Boolean credentialsExpired ){
		authUserService.setUserStatus(userIds, enabled,accountExpired,accountLocked,credentialsExpired);
		return ajaxDoneSuccess("数据操作成功");
	}
//------------------------------role---------------------------------------------------------	
	
	 @RequiresPermissions(value="authUser:roleAssign",desc="用户角色分配")
	 @RequestMapping(value="/loadRoleDataSetForUserAssign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	 public @ResponseBody  Object loadRoleDataSetForUserAssign(Boolean isInUser,@RequestParam Map<String,Object> params){
		 DataSet dataSet=  authUserRoleService.loadRoleDataSetForUserAssign(isInUser, params);
		 return dataSet;
	 }
	
	@RequiresPermissions(value="authUser:roleAssign",desc="用户角色分配")
	@RequestMapping(value="/addRolesToUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object addRolesToUser(@ListParam(Long.class) List<Long> roleIds,Long userId){
		authUserRoleService.addRolesToUserRole(roleIds, userId);
		return ajaxDoneSuccess("数据操作成功");
	}
	@RequiresPermissions(value="authUser:roleAssign",desc="用户角色分配")
	@RequestMapping(value="/removeRoleFromUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object removeRoleFromUser(@ListParam(Long.class) List<Long> roleIds,Long userId){
		authUserRoleService.removeRolesFromUserRole(roleIds, userId);
		return ajaxDoneSuccess("数据操作成功");
	}
	
//-------------------------------permission--------------------------------------------------------	
	
	 @RequiresPermissions(value="authUser:permAssign",desc="用户权限分配")
	 @RequestMapping(value="/loadPermDataSetForUserAssign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	 public @ResponseBody  Object loadPermDataSetForUserAssign(Boolean isInUser,@RequestParam Map<String,Object> params){
		 DataSet dataSet=  authUserPermissionService.loadPermDataSetForUserAssign(isInUser, params);
		 return dataSet;
	 }
	 
	@RequiresPermissions(value="authUser:permAssign",desc="用户权限分配")
	@RequestMapping(value="/addPremsToUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object addPremsToUser(@ListParam(Long.class) List<Long> premIds,Long userId){
		authUserPermissionService.addPermsToUserPerm(premIds, userId);
		return ajaxDoneSuccess("数据操作成功");
	}
	
	@RequiresPermissions(value="authUser:permAssign",desc="用户权限分配")
	@RequestMapping(value="/removePremsFromUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object removePremsFromUser(@ListParam(Long.class) List<Long> permIds,Long userId){
		authUserPermissionService.removePermsFromUserPerm(permIds, userId);
		return ajaxDoneSuccess("数据操作成功");
	}
	
	//---------------------------------------------------------------------------------------
	
	//@RequiresPermissions(value="authUser:setAuthUserPwd",desc="重置用户密码")
	@RequestMapping(value="/setAuthUserPwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object setAuthUserPwd(Long userId, String password) {
		authUserService.setAuthUserPwd(userId, password);
		return ajaxDoneSuccess("数据操作成功");
	}
	
	@RequestMapping(value="/changePwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object changePwd(String newPassword, String oldPassword) {
		authUserService.changePwd(SecurityUtil.getUserId().get(), newPassword,oldPassword);
		return ajaxDoneSuccess("数据操作成功");
	}
	
	@RequiresPermissions(value="authUser:changePwd",desc="修改用户密码")
	@RequestMapping(value="/doChangePwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody Object doChangePwd(Long userId, String newPassword, String oldPassword) {
		authUserService.changePwd(userId, newPassword,oldPassword);
		return ajaxDoneSuccess("数据操作成功");
	}
	
	
	
}