/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.core.util.ZObjectUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.auth.biz.service.IAuthModuleService;
import org.zfes.snowy.auth.biz.dao.AuthModuleMapper;
import org.zfes.snowy.auth.biz.model.AuthModule;

@Service
@Lazy(true)
public class AuthModuleServiceImpl implements IAuthModuleService{
 	@Autowired
	private AuthModuleMapper authModuleMapper;
	
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createAuthModule(AuthModule authModule) {
		ZBeanUtil.validateBean(authModule);
		authModule.setId(IDGenerator.genLongId());
		authModuleMapper.insertSelective(authModule);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<AuthModule> loadAuthModule(Long id) {
		if(!ZObjectUtil.isEmpty(id)){
			return Optional.ofNullable(authModuleMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateAuthModule(AuthModule authModule) {
		ZBeanUtil.validateBean(authModule);
		authModuleMapper.updateByPrimaryKeySelective(authModule);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletAuthModule(Long id) {
		ZAssert.notNull(id, "请选择要删除的数据");
		authModuleMapper.deleteByPrimaryKey(id);
	}
	 
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadAuthModuleList(String appKey,Map<String, Object> params) {
		if(ZStrUtil.hasNoText(appKey)){
			return DataSet.emptyDS();
		}
		ParamMap pm=ParamMap.filterParam(params);
		pm.getStr("name").ifPresent(val -> pm.updateParam("name", "%"+val+"%"));
		
		DataSet ds=DataSet.newDS(authModuleMapper.selectAuthModuleListCount(pm), authModuleMapper.selectAuthModuleList(pm));
		return ds;
	}

	@Override
	public List<ComboboVo> loadModuleComboboVo(String appKey,Boolean firstEmpty) {
		if(ZStrUtil.hasNoText(appKey)){
			return Collections.emptyList();
		}
		List<ComboboVo> moduleSelectVo= authModuleMapper.selectComboboVO(appKey);
		 List<ComboboVo> moduleSelectVo2=new ArrayList<>();
		 if(firstEmpty!=null&&firstEmpty){
			 moduleSelectVo2.add(0, ComboboVo.newEmptyFirst()); 
		 }
		 moduleSelectVo2.addAll(moduleSelectVo);
		 
		return moduleSelectVo2;
	}

	@Override
	public void setAuthModuleEnabled(List<Long> ids, Boolean enabled) {
		ZAssert.notEmpty(ids, "请选择数据");
		ZAssert.notNull(enabled, "状态错误");
		authModuleMapper.updateEnabled( ids,  enabled);
	}
}