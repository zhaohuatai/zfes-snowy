/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthMenu;
import org.zfes.snowy.auth.biz.service.IAuthMenuService;
import org.zfes.snowy.auth.biz.service.IAuthPermissionService;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
@Controller 
@RequestMapping("/auth/authMenu")
@Lazy(true)
public class AuthMenuController extends BaseController {

	@Autowired
	private IAuthMenuService authMenuService;
	@Autowired
	private IAuthPermissionService authPermissionService;
	
	@RequiresPermissions(value="authMenu:loadAuthMenuList",desc="菜单列表")
    @RequestMapping(value="/loadAuthMenuList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody Object loadAuthMenuList(String appKey, @RequestParam Map<String,Object> params){
	    DataSet dataSet =  authMenuService.loadAuthMenuTreeGrid(appKey,params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="authMenu:addAuthMenu",desc="菜单添加")
    @RequestMapping(value="/addAuthMenu", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addAuthMenu(AuthMenu authMenu){
    	authMenuService.createAuthMenu(authMenu);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
    
	@RequiresPermissions(value="authMenu:loadAuthMenu",desc="菜单加载")
    @RequestMapping(value="/loadAuthMenu", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthMenu(Long id){
    	Optional<AuthMenu> authMenu=authMenuService.loadAuthMenuById(id);
    	return ajaxQuery("authMenu",authMenu);
    }
	
	@RequiresPermissions(value="authMenu:updateAuthMenu",desc="菜单更新")
    @RequestMapping(value="/updateAuthMenu", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateAuthMenu(AuthMenu authMenu){
    	authMenuService.updateAuthMenu(authMenu);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authMenu:setAuthMenuEnabled",desc="菜单状态设置")
    @RequestMapping(value="/setAuthMenuEnabled", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object setAuthMenuEnabled(@ListParam(Long.class)List<Long> ids,Boolean  enabled){
		authMenuService.setAuthMenuEnabled(ids, enabled);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authMenu:removeAuthMenu",desc="菜单删除")
    @RequestMapping(value="/removeAuthMenu", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object removeAuthMenu(@ListParam(Long.class)List<Long> ids){
		authMenuService.batchRemoveAuthMenu(ids);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
    @RequiresPermissions(value="authMenu:loadAuthMenuTree",desc="加载菜单树")
    @RequestMapping(value="/loadAuthMenuTree", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody  Object loadAuthMenuTree(@RequestParam Map<String,Object> params){
		List<Map<String,Object>>  authMenuTree=authMenuService.loadAuthMenuTree(params);
		return authMenuTree;
	}
    //--------------------------------------------------
    
	//@RequiresPermissions(value={"authMenu:addAuthMenu","authMenu:updateAuthMenu"},logical=Logical.OR ,desc="权限列表")
    @RequestMapping(value="/loadAuthPermLookback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthPermLookback(String appKey,@RequestParam Map<String,Object> params){
	    DataSet dataSet=  authPermissionService.loadAuthPermissionDataSet(appKey,params);
	    return dataSet;
    }
	
	
}