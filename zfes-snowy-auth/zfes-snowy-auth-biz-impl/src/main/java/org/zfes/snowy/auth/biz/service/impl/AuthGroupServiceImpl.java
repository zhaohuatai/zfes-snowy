/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service.impl;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zfes.snowy.auth.biz.dao.AuthGroupMapper;
import org.zfes.snowy.auth.biz.model.AuthGroup;
import org.zfes.snowy.auth.biz.service.IAuthGroupService;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.core.consts.FrameConst;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

@Service
@Lazy(true)
public class AuthGroupServiceImpl implements IAuthGroupService{
 	@Autowired
	private AuthGroupMapper authGroupMapper;
	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createAuthGroup(AuthGroup authGroup) {
		ZBeanUtil.validateBean(authGroup);
		if(authGroup.getParentId()==null){
			ZAlert.Error("父节点不可为空");
		}
		authGroup.setId(IDGenerator.genLongId());
		authGroupMapper.insertSelective(authGroup);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<AuthGroup> loadAuthGroup(Long id) {
		if(id!=null){
			return Optional.ofNullable(authGroupMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
		
	}
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateAuthGroup(AuthGroup authGroup) {
		ZBeanUtil.validateBean(authGroup);
		if(FrameConst.DB_TABLE.TREE_TABLE_ROOTNODE_PARENT_FK.equals(String.valueOf(authGroup.getParentId()))){
			ZAlert.Error("系统根节点数据不允许编辑");
		}
		authGroupMapper.updateByPrimaryKeySelective(authGroup);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletAuthGroup(Long id) {
		if(id==null){
			ZAlert.Error("请选择要删除的数据");
		}
		AuthGroup group=authGroupMapper.selectByPrimaryKey(id);
		ZAssert.notNull(group, "未查询到该数据");
		ZAssert.notNull(group.getParentId(), "该节点未发现父节点ID，数据错误，");
		if(FrameConst.DB_TABLE.TREE_TABLE_ROOTNODE_PARENT_FK.equals(String.valueOf(group.getParentId()))){
			ZAlert.Error("根节点数据不允许删除");
		}
		authGroupMapper.deleteByPrimaryKey(id);
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadAuthGroupList(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		pm.getObj("name").ifPresent(v->pm.updateParam( "name", "%"+v+"%"));
		pm.getLong("parentId").ifPresent(v->pm.updateParam( "parentId",v));
		pm.getBoolean("enabled").ifPresent(v-> pm.updateParam("enabled", v));
		DataSet ds=DataSet.newDS(authGroupMapper.selectAuthGroupListCount(pm), authGroupMapper.selectAuthGroupList(pm));
		return ds;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=false)
	@Override
	public void setAuthGroupEnabled(List<Long> ids, Boolean enabled) {
		authGroupMapper.updateEnabled( ids,  enabled);
	}
}