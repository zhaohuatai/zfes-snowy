/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthRole;
import org.zfes.snowy.auth.biz.service.IAuthRolePermissionService;
import org.zfes.snowy.auth.biz.service.IAuthRoleService;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
@Controller 
@RequestMapping("/auth/authRole")
@Lazy(true)
public class AuthRoleController extends BaseController {

	
	@Autowired
	private IAuthRoleService authRoleService;
	@Autowired
	private IAuthRolePermissionService authRolePermissionService;

	/**
	 * @param params={ name:(like), roleCode:(like), enabled:(=) }
	 * @return
	 */
	@RequiresPermissions(value="authRole:loadAuthRoleList",desc="角色列表")
    @RequestMapping(value="/loadAuthRoleList")
    public @ResponseBody  Object loadAuthRoleList(String appKey,@RequestParam Map<String,Object> params){
	    DataSet dataSet=  authRoleService.loadAuthRoleList(appKey,params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="authRole:addRole",desc="角色添加")
    @RequestMapping(value="/addRole")
    public  @ResponseBody Object addRole(AuthRole authRole){
		authRoleService.creatAuthRole(authRole);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authRole:loadRole",desc="角色加载")
    @RequestMapping(value="/loadRole")
    public @ResponseBody  Object loadRole(Long id){
		Optional<AuthRole> authRole=authRoleService.loadAuthRole(id);
		return ajaxQuery("authRole", authRole);
    }
	
	@RequiresPermissions(value="authRole:updateRole",desc="角色更新")
    @RequestMapping(value="/updateRole")
    public @ResponseBody  Object updateRole(AuthRole authRole){
		authRoleService.updateAuthRole(authRole);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	@RequiresPermissions(value="authRole:deletAuthRole",desc="角色删除")
    @RequestMapping(value="/deletAuthRole", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletAuthRole(Long id){
		authRoleService.deleteAuthRole(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authRole:setAuthRoleEnabled",desc="角色状态更新")
    @RequestMapping(value="/setAuthRoleEnabled", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object setAuthRoleEnabled(@ListParam(Long.class)List<Long> ids,Boolean enabled){
		authRoleService.setAuthRoleEnabled(ids, enabled);
		return ajaxDoneSuccess("数据操作成功 ");
    }

	 @RequestMapping(value="/loadRoleComboboVo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	 public @ResponseBody  Object loadRoleComboboVo(String appKey,Boolean firstEmpty){
			 List<ComboboVo> roleSelectVo=authRoleService.loadRoleComboboVo(appKey,firstEmpty);
			return ajaxQuery("snowyselectdata", roleSelectVo);
	 }

	@RequestMapping(value="/loadRoleComboboVo2", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody  Object loadRoleComboboVo2(Boolean firstEmpty){
		List<ComboboVo> roleSelectVo=authRoleService.loadRoleComboboVo2(firstEmpty);
		return ajaxQuery("snowyselectdata", roleSelectVo);
	}
//-------------------------------------------------------------------------------------------------------------------	 
	 @RequiresPermissions(value="authRole:permAssign",desc="权限分配")
	 @RequestMapping(value="/loadPermDataSetForRoleAssign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	 public @ResponseBody  Object loadPermDataSetForRoleAssign(Boolean isInRole,@RequestParam Map<String,Object> params){
		 DataSet dataSet=  authRolePermissionService.loadPermDataSetForRoleAssign(isInRole, params);
		 return dataSet;
	 }
	 
	@RequiresPermissions(value="authRole:permAssign",desc="权限分配")
	@RequestMapping(value="/removePermsFromRole", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody  Object removePermsFromRole(@ListParam(Long.class)List<Long> permIds,Long roleId){
		authRolePermissionService.removePermsFromRolePerm(permIds, roleId);
		return ajaxDoneSuccess("数据操作成功 ");
	}
		
	@RequiresPermissions(value="authRole:permAssign",desc="权限分配")
	@RequestMapping(value="/addPermsToRole", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody  Object addPermsToRole(@ListParam(Long.class)List<Long> permIds,Long roleId){
		authRolePermissionService.addPermsToRolePerm(permIds, roleId);
		return ajaxDoneSuccess("数据操作成功 ");
	}
	
	
}