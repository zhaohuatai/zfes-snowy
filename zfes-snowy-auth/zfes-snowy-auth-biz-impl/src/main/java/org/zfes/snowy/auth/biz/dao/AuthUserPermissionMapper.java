/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthUserPermission;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
@Mapper
@Lazy(true)
public interface AuthUserPermissionMapper extends BaseMapper<AuthUserPermission>{
	
	
	List<Long> selectPermIdsByUserId(@Param("userId")Long userId,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	
	List<String> selectPermCodesByUserId(@Param("userId")Long userId,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	
	int deletePermFromUserPermission(@Param("permIds")List<Long> permIds,@Param("userId")Long userId);

	int  batchInsertUserPermission(@Param("userPermList")List<AuthUserPermission> userPermList);
	/**
	 * 
	 * @param params String appKey, Long moduleId, String permCode, [Long roleId,] 
	 */
	List<Map<String, Object>> selectPermsInUser(ParamMap params);
	Long selectPermsInUserCount(ParamMap params);
	/**
	 * 
	 * @param params String appKey, Long moduleId, String permCode, [Long roleId,]
	 */
	List<Map<String, Object>> selectPermsNotInUser(ParamMap params);
	Long selectPermsNotInUserCount(ParamMap params);
	
	
	
}
