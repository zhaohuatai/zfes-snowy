/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.manager;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zfes.snowy.auth.biz.model.AuthRole;
import org.zfes.snowy.auth.biz.model.AuthUser;
import org.zfes.snowy.auth.biz.service.IAuthAppService;
import org.zfes.snowy.auth.biz.service.IAuthPermissionService;
import org.zfes.snowy.auth.biz.service.IAuthRolePermissionService;
import org.zfes.snowy.auth.biz.service.IAuthRoleService;
import org.zfes.snowy.auth.biz.service.IAuthUserPermissionService;
import org.zfes.snowy.auth.biz.service.IAuthUserRoleService;
import org.zfes.snowy.auth.biz.service.IAuthUserService;

import com.google.common.collect.Lists;

/**
 * 
 * @author zhaoht
 *
 */
@Component
public class AuthManagerImpl implements IAuthManager{

	@Autowired
	IAuthPermissionService authPermissionService;
	@Autowired
	IAuthUserPermissionService authUserPermissionService;
	@Autowired
	IAuthRoleService authRoleService;
	@Autowired
	IAuthRolePermissionService authRolePermissionService;
	@Autowired
	IAuthUserRoleService authUserRoleService;
	@Autowired
	IAuthUserService authUserService;
	
	@Autowired
	private IAuthAppService authAppService;
	public Optional<Long> getAppId(String appkey){
		Optional<Long> appId=authAppService.loadIdByAppkey(appkey, true);
		return appId;
	}
	
	@Override
	public List<String> loadRoleCodesUserHave(Long userId,String appkey,Boolean enabled) {
		
		List<String> roleCodes = authUserRoleService.loadRoleCodesInUserRoleByUserId(userId, appkey, enabled);
		
		roleCodes = roleCodes.stream().distinct().collect(Collectors.toList());
		return roleCodes;
	}
	@Override
	public Optional<String> loadUserDefualtRoleCode(Long userId,Boolean enabled){
		Optional<AuthUser>  authUserOp=authUserService.loadAuthUser(userId);
		if(!authUserOp.isPresent()){
			return Optional.empty();
		}
		Long roleId=authUserOp.get().getDefaultRole();
		Optional<AuthRole> roleOp=authRoleService.loadAuthRole(roleId);
		if(!roleOp.isPresent()||!roleOp.get().getEnabled()){
			return Optional.empty();
		}
		return Optional.of(roleOp.get().getRoleCode());
	}

	@Override
	public List<String> loadPermCodesInUserDefaultRole(Long userId, String appkey, Boolean enabled) {
		Optional<AuthUser> authUSerOp= authUserService.loadAuthUser(userId);
		if(!authUSerOp.isPresent()){
			return Collections.emptyList();
		}
		Long roleId=authUSerOp.get().getDefaultRole();
		
		List<String> permCodesFinal= Lists.newArrayList();
		
		List<String> permCodesFromRole = authRolePermissionService.loadPermCodesByRoleId( roleId,  appkey,  enabled);
		
		List<String> permCodesFromUserPerm = authUserPermissionService.loadPermCodesInUserPermByUserId(userId, appkey, enabled);
		
	
		permCodesFinal.addAll(permCodesFromRole);
		
		permCodesFinal.addAll(permCodesFromUserPerm);
		
		
		return permCodesFinal;
	}

	@Override
	public List<String> loadPermCodesInUserAllRoles(Long userId, String appkey, Boolean enabled) {
		
		List<String> roleCodes=loadRoleCodesUserHave( userId, appkey, enabled);
		
		if(roleCodes==null||roleCodes.isEmpty()){
			return Collections.emptyList();
		}
		
		List<String> permCodesFinal= Lists.newArrayList();
		
		for(String roleCode: roleCodes){
			List<String>  permCodesFromRole = authRolePermissionService.loadPermCodesByRoleCode(roleCode, appkey, enabled);
			permCodesFinal.addAll(permCodesFromRole);
		}
		
		List<String> permCodesFromUserPerm = authUserPermissionService.loadPermCodesInUserPermByUserId(userId, appkey, enabled);
		
		permCodesFinal.addAll(permCodesFromUserPerm);
		return permCodesFinal;
	}


	@Override
	public List<Long> loadPermIdsInUserAllRoles(Long userId, String appkey, Boolean enabled) {
		List<String> roleCodes=loadRoleCodesUserHave( userId, appkey, enabled);
		
		if(roleCodes==null||roleCodes.isEmpty()){
			return Collections.emptyList();
		}
		
		List<Long> permIdsFinal= Lists.newArrayList();
		
		for(String roleCode: roleCodes){
			List<Long>  permIdsFromRole = authRolePermissionService.loadPermIdsByRoleCode(roleCode, appkey, enabled);
			permIdsFinal.addAll(permIdsFromRole);
		}
		
		List<Long> permIdsFromUserPerm = authUserPermissionService.loadPermIdsInUserPermByUserId(userId, appkey, enabled);
		
		permIdsFinal.addAll(permIdsFromUserPerm);
		
		
		return permIdsFinal;
	}


	@Override
	public List<Long> loadPermIdsInUserDefaultRole(Long userId, String appkey, Boolean enabled) {
		Optional<AuthUser> authUSerOp= authUserService.loadAuthUser(userId);
		if(!authUSerOp.isPresent()){
			return Collections.emptyList();
		}
		Long roleId=authUSerOp.get().getDefaultRole();
		
		List<Long> permIdsFinal= Lists.newArrayList();
		
		List<Long> permIdsFromRole = authRolePermissionService.loadPermIdsByRoleId(roleId, appkey, enabled);
		
		List<Long> permIdsFromUserPerm = authUserPermissionService.loadPermIdsInUserPermByUserId(userId, appkey, enabled);
		
	
		permIdsFinal.addAll(permIdsFromRole);
		
		permIdsFinal.addAll(permIdsFromUserPerm);
		
		
		return permIdsFinal;
	}


	@Override
	public Optional<AuthUser> loadUserByName(String username) {
		return authUserService.loadByAccount(username);
	}

	

}