/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.ComboboVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthApp;
@Mapper
@Lazy(true)
public interface AuthAppMapper extends BaseMapper<AuthApp>{
	
	AuthApp selectByAppkey(@Param("appKey") String appKey);
	
	Long selectAuthAppListCount(ParamMap params);
	
	List<Map<String, Object>> selectAuthAppList(ParamMap params);
 
	List<AuthApp> selectAllAuthAppsList(@Param("enabled")Boolean enabled);
	
	Long selectIdByAppkey(@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	 
	String selectAppKeyById(@Param("id")Long id,@Param("enabled")Boolean enabled);
	 
	void updateAuthAppEnabled(@Param("ids")List<Long> ids,@Param("enabled") Boolean enabled);
	
	List<ComboboVo> selectComboboVO();
}
