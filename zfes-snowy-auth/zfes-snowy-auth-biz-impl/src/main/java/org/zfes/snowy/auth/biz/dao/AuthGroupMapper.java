/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthGroup;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
@Mapper
@Lazy(true)
public interface AuthGroupMapper extends BaseMapper<AuthGroup>{

	Long selectAuthGroupListCount(ParamMap params);
	
	List<Map<String, Object>> selectAuthGroupList(ParamMap params);

	void updateEnabled(List<Long> ids, Boolean enabled);
	
}
