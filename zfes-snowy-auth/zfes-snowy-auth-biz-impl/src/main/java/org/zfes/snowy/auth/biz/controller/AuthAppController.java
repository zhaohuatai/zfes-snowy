/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.controller;


import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthApp;
import org.zfes.snowy.auth.biz.service.IAuthAppService;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
@Controller 
@RequestMapping("/auth/authApp")
@Lazy(true)
public class AuthAppController extends BaseController {

	@Autowired
	private IAuthAppService authAppService;
	
	@RequiresPermissions(value="authApp:loadAuthAppList",desc="应用列表")
    @RequestMapping(value="/loadAuthAppList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthAppList(@RequestParam Map<String,Object> params){
	    DataSet dataSet=  authAppService.loadAuthAppList(params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="authApp:loadAuthApp",desc="应用加载")
    @RequestMapping(value="/loadAuthApp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthApp(Long id){
		Optional<AuthApp> authApp=authAppService.loadAuthAppById(id);
		return ajaxQuery("authApp",authApp);
    }
	
	@RequiresPermissions(value="authApp:addAuthApp",desc="应用添加")
    @RequestMapping(value="/addAuthApp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addAuthApp(AuthApp authApp){
		authAppService.createAuthApp(authApp);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authApp:updateAuthApp",desc="应用更新")
    @RequestMapping(value="/updateAuthApp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updateAuthApp(AuthApp authApp){
		authAppService.updateAuthApp(authApp);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authApp:setAuthAppEnabled",desc="应用状态设置")
    @RequestMapping(value="/setAuthAppEnabled", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object setAuthAppEnabled(@ListParam(Long.class)List<Long> ids,Boolean  enabled){
		authAppService.setAuthAppEnabled(ids,enabled);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authApp:deletAuthApp",desc="应用删除")
    @RequestMapping(value="/deletAuthApp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletAuthApp(Long id){
		authAppService.deletAuthApp(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
    @RequestMapping(value="/loadAuthAppComboboVo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadAuthAppComboboVo(Boolean firstEmpty ){
		 List<ComboboVo> appList=authAppService.loadComboboVo(firstEmpty);
		 return ajaxQuery("snowyselectdata", appList);
    }
	

}