/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.controller;

import org.springframework.stereotype.Controller;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ResponseBody;
import org.zfes.snowy.auth.biz.model.AuthPermission;
import org.zfes.snowy.auth.biz.service.IAuthPermissionService;
import org.zfes.snowy.base.web.annos.ListParam;
import org.zfes.snowy.base.web.controller.BaseController;
import org.zfes.snowy.core.data.DataSet;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
@Controller 
@RequestMapping("/auth/authPermission")
@Lazy(true)
public class AuthPermissionController extends BaseController {
	
	
	@Autowired
	private IAuthPermissionService authPermissionService;

	@RequiresPermissions(value="authPermission:loadAuthPermissionList",desc="权限列表")
    @RequestMapping(value="/loadAuthPermissionList",  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadauthPermissionList(String appKey,@RequestParam Map<String,Object> params){
	    DataSet dataSet=  authPermissionService.loadAuthPermissionDataSet(appKey,params);
	    return dataSet;
    }
	
	@RequiresPermissions(value="authPermission:addPermission",desc="权限添加")
    @RequestMapping(value="/addPermission", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public  @ResponseBody Object addPermission(AuthPermission authPermission){
		authPermissionService.createAuthPermission(authPermission);
    	return ajaxDoneSuccess("数据操作成功 ");
    }
	
	@RequiresPermissions(value="authPermission:loadForUpdate",desc="权限加载")
    @RequestMapping(value="/loadForUpdate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object loadForUpdate(Long id){
		Optional<AuthPermission> authPermission=authPermissionService.loadAuthPermissionById(id);
		return ajaxQuery("authPermission", authPermission);
    }
	
	@RequiresPermissions(value="authPermission:updatePermission",desc="权限更新")
    @RequestMapping(value="/updatePermission", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object updatePermission(AuthPermission authPermission){
		authPermissionService.updateAuthPermission(authPermission);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	
	
	@RequiresPermissions(value="authPermission:setPermissionEnabled",desc="权限状态设置")
    @RequestMapping(value="/setPermissionEnabled", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object setPermissionEnabled(@ListParam(Long.class)List<Long> ids,Boolean  enabled){
		authPermissionService.setPermissionEnabled(ids, enabled);
		return ajaxDoneSuccess("数据操作成功 ");
    }
	

	
	
	@RequiresPermissions(value="authPermission:deletAuthPermission",desc="权限删除")
    @RequestMapping(value="/deletAuthPermission", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object deletAuthPermission(Long id){
		authPermissionService.deletAuthPermission(id);
		return ajaxDoneSuccess("数据操作成功 ");
    }

	@RequiresPermissions(value="authPermission:batchDeleteAuthPermission",desc="权限批量删除")
    @RequestMapping(value="/batchDeleteAuthPermission", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody  Object batchDeleteAuthPermission(@ListParam(Long.class)List<Long> ids,Boolean enabled){
		authPermissionService.setPermissionEnabled(ids,enabled);
	    return ajaxDoneSuccess("数据操作成功 ");
    }
	

}