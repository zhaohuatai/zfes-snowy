/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthPermission;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
@Mapper
@Lazy(true)
public interface AuthPermissionMapper extends BaseMapper<AuthPermission>{
	
	AuthPermission selectByCode(@Param("permCode")String permCode);
	
	int batchInsertAuthPermission(@Param("authPermissionList")List<AuthPermission> authPermissionList);
	
	int batchDeletePermission(@Param("permIds")List<Long> permIds);

	Long selectAuthPermissionListCount(ParamMap params);
	
	List<Map<String, Object>> selectAuthPermissionList(ParamMap params);
	
	int updateEnabled(@Param("ids")List<Long> ids, @Param("enabled")Boolean enabled);
	 
	List<AuthPermission> selectPermListByIds(@Param("permIds")List<Long> permIds);
	
	List<AuthPermission> selectPermListByCodes(@Param("permCodes")List<String> permCodes);
}