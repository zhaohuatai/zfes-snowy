/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthUserRole;
@Mapper
@Lazy(true)
public interface AuthUserRoleMapper extends BaseMapper<AuthUserRole>{
	
	  List<String> selectRoleCodesByUserId(@Param("userId")Long userId,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
		
	  List<Long> selectRoleIdsByUserId(@Param("userId")Long userId, @Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	  
	  List<Long> selectUserIdsByRoleId(@Param("roleId")Long roleId,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	  
	  List<Long> selectUserIdsByRoleCode(@Param("roleCode")String roleCode,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);

	  int batchInsertUserRole(@Param("userRoleList")List<AuthUserRole> userRoleList);
	  
	  int deleteRolesFromUserRole(@Param("roleIds")List<Long> roleIds,@Param("userId")Long userId);
	  
	  int deleteUsersFromUserRole(@Param("userIds")List<Long> userIds,@Param("userId")Long roleId);
	  
		/**
		 * 
		 * @param params String appKey, Long moduleId, String permCode, [Long userId,]
		 */
		List<Map<String, Object>> selectRolesNotInUser(ParamMap params);
		Long selectRolesNotInUserCount(ParamMap params);
		
		/**
		 * 
		 * @param params String appKey, Long moduleId, String permCode, [Long userId,] 
		 */
		List<Map<String, Object>> selectRolesInUser(ParamMap params);
		Long selectRolesInUserCount(ParamMap params);
		
		
}
