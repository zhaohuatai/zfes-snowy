/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthUser;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
@Mapper
@Lazy(true)
public interface AuthUserMapper extends BaseMapper<AuthUser>{

	AuthUser selectByAccount(@Param("account")String account);
	AuthUser selectByOpenId(@Param("openId")String openId);
	
	int batchDeleteAuthUser(@Param("userIds")List<Long> userIds);
	int batchInsertAuthUser(@Param("authUserList")List<AuthUser> authUserList);
	Long selectDefaultRoleId(@Param("userId")Long userId);
	String selectDefaultRoleCode(@Param("userId")Long userId);
	
	int updateEnabled(@Param("userIds")List<Long> userIds, @Param("enabled")Boolean enabled);
	int updateAccountExpired(@Param("userIds")List<Long> userIds, @Param("accountExpired")Boolean accountExpired);
	int updateAccountLocked(@Param("userIds")List<Long> userIds, @Param("accountLocked")Boolean accountLocked);
	int updateCredentialsExpired(@Param("userIds")List<Long> userIds, @Param("credentialsExpired")Boolean credentialsExpired);
	
	int updateDefaultRole(@Param("userIds")List<Long> userIds, @Param("roleId")Long roleId);
	int updateDefaultPosition(@Param("userIds")List<Long> userIds, @Param("positionId")Long positionId);
	int updateDefaultOrg(@Param("userIds")List<Long> userIds, @Param("defaultOrg")String defaultOrg);
	
	int updateAuthLevel(@Param("userId")Long userId, @Param("authLevel")Integer authLevel);
	
	List<Map<String,Object>> selectAuthUserList(Map<String,Object> params);
	Long selectAuthUserListCount(Map<String,Object> params);
	
	
	void updateUserPwd(@Param("userId")Long userId,@Param("password")String password,@Param("salt")String salt);
}
