/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthRole;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.core.data.ComboboVo;
@Mapper
@Lazy(true)
public interface AuthRoleMapper extends BaseMapper<AuthRole>{
	
	AuthRole selectAuthRoleByCode(@Param("roleCode")String roleCode);
	
	int  batchInsertAuthRole(@Param("authRoleList")List<AuthRole> authRoleList);
	
	int  batchDeleteAuthRole(@Param("roleIds")List<Long> roleIds);
	
	List<ComboboVo> selectComboboVo(@Param("appKey")String appKey);

	void updateEnabled(@Param("ids")List<Long> ids, @Param("enabled")Boolean enabled);
	
	List<AuthRole> selectByIds(@Param("ids")List<Long> ids);
	
	/**
	 * @param params ={ name:(like), roleCode:(like), enabled:(=), appId:(=) }
	 * @return
	 */
	public List<Map<String,Object>> selectAuthRoleList(Map<String,Object> params);
	/**
	 * 
	 * @param params ={ name:(like), roleCode:(like), enabled:(=), appId:(=) }
	 * @return
	 */
	public Long selectAuthRoleListCount(Map<String,Object> params);
}
