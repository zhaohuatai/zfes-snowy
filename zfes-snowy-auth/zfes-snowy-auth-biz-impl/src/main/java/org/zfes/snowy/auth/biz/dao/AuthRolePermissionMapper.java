/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthRolePermission;
import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
@Mapper
@Lazy(true)
public interface AuthRolePermissionMapper extends BaseMapper<AuthRolePermission>{
	
	
	List<Long> selectPermIdsByRoleId(@Param("roleId")Long roleId,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	
	List<String> selectPermCodesByRoleId(@Param("roleId")Long roleId,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	
	List<String> selectPermCodesByRoleCode(@Param("roleCode")String roleCode, @Param("appKey")String appKey,@Param("enabled")Boolean enabled);
	
	List<Long> selectPermIdsByRoleCode(@Param("roleCode")String roleCode,@Param("appKey")String appKey,@Param("enabled")Boolean enabled);

	int deleteFromRolePermission(@Param("permIds")List<Long> permIds, @Param("roleId")Long roleId);
	
	int  batchInsertRolePermission(@Param("rolePermissionList")List<AuthRolePermission> rolePermissionList);
	

	/**
	 * 
	 * @param params String appKey, Long moduleId, String permCode, [Long roleId,]
	 */
	List<Map<String, Object>> selectPermsNotInRole(ParamMap params);
	Long selectPermsNotInRoleCount(ParamMap params);
	
	/**
	 * 
	 * @param params String appKey, Long moduleId, String permCode, [Long roleId,] 
	 */
	List<Map<String, Object>> selectPermsInRole(ParamMap params);
	Long selectPermsInRoleCount(ParamMap params);
}
