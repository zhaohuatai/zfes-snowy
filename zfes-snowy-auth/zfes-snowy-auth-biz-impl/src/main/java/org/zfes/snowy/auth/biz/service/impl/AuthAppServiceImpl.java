/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.core.data.ComboboVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.auth.biz.service.IAuthAppService;
import org.zfes.snowy.auth.biz.dao.AuthAppMapper;
import org.zfes.snowy.auth.biz.model.AuthApp;

@Service
@Lazy(true)
public class AuthAppServiceImpl implements IAuthAppService{
 	@Autowired
	private AuthAppMapper authAppMapper;
 	
	@Transactional(rollbackFor=Exception.class)
 	@Override
	public void createAuthApp(AuthApp authApp) {
		ZBeanUtil.validateBean(authApp);
		AuthApp AuthApp_1=authAppMapper.selectByAppkey(authApp.getAppKey());
		if(AuthApp_1!=null){
			ZAlert.Error("应用标识已经存在");
		}
		authApp.setId(IDGenerator.genLongId());
		authAppMapper.insertSelective(authApp);
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<AuthApp> loadAuthAppById(Long id) {
		if(id!=null){
			return Optional.ofNullable(authAppMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
	}
	//@Cacheable
	@Transactional(rollbackFor=Exception.class,readOnly=true)	
	@Override
	public Optional<AuthApp> loadAuthAppByAppKey(String appkey) {
		if(ZStrUtil.hasText(appkey)){
			return Optional.ofNullable(authAppMapper.selectByAppkey(appkey));
		}
		return Optional.empty();
	}
	
	//@CacheEvict
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void updateAuthApp(AuthApp authApp) {
		ZBeanUtil.validateBean(authApp);
		AuthApp AuthApp_1=authAppMapper.selectByAppkey(authApp.getAppKey());
		if(AuthApp_1!=null&&!AuthApp_1.getId().equals(authApp.getId())){
			ZAlert.Error("模块标识已经存在");
		}
		authAppMapper.updateByPrimaryKeySelective(authApp);
	}
	//@CacheEvict
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletAuthApp(Long id) {
		ZAssert.notNull(id, "请选择数据");
		authAppMapper.deleteByPrimaryKey(id);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadAuthAppList(Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		pm.getObj("appkey").ifPresent(v->pm.updateParam( "appkey", "%"+v+"%"));
		pm.getObj("name").ifPresent(v->pm.updateParam( "name", "%"+v+"%"));
		DataSet ds=DataSet.newDS(authAppMapper.selectAuthAppListCount(pm), authAppMapper.selectAuthAppList(pm));
		return ds;
	}
	//@Cacheable
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<ComboboVo> loadComboboVo(Boolean firstEmpty) {
		List<ComboboVo> appList=authAppMapper.selectComboboVO();
		 List<ComboboVo> appList2=new ArrayList<>();
		 if(firstEmpty!=null&&firstEmpty){
			 appList2.add(0, ComboboVo.newEmptyFirst()); 
		 }
		 appList2.addAll(appList);
		return appList2;
	}
	
	//@Cacheable()
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<AuthApp> loadAllAuthAppList(Boolean enabled) {
		return authAppMapper.selectAllAuthAppsList(enabled);
	}
	
	//@Cacheable()
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<Long> loadIdByAppkey(String key,Boolean enabled){
		 if(ZStrUtil.hasText(key)){
			 return Optional.ofNullable(authAppMapper.selectIdByAppkey(key, enabled));
		 }
		 return Optional.empty();
	 }
	
	//@Cacheable()
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<String> loadAppKeyById(Long id,Boolean enabled){
		 if(id!=null){
			 return Optional.of(authAppMapper.selectAppKeyById(id, enabled));
		 }
		 return Optional.empty();
	}
	@Transactional(rollbackFor=Exception.class,readOnly=false)
	@Override
	public void setAuthAppEnabled(List<Long> ids, Boolean enabled) {
		ZAssert.notNull(ids, "请选择数据");
		ZAssert.notNull(enabled, "状态参数不能为空");
		authAppMapper.updateAuthAppEnabled( ids,  enabled);
		
	}
	
}