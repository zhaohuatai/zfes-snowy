/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.manager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.zfes.snowy.auth.biz.service.IAuthMenuService;
import org.zfes.snowy.auth.biz.view.MenuView;
import org.zfes.snowy.core.data.JSONMSG;


public class AuthBizManager {

	private IAuthMenuService authMenuService;
	
	private IAuthManager authManager;
	
	public JSONMSG loadMenu(String appkey,Long userId) {
		if(userId==null){
			return JSONMSG.newMSG(300, "加载菜单过程中用户参数错误");
		}
		Optional<MenuView> menuView =authMenuService.loadUserMenus(userId,appkey,false);
		if(!menuView.isPresent()){
			return JSONMSG.newMSG(200).put("navMenuData", Collections.emptyList());
		}
		return JSONMSG.newMSG(200).put("navMenuData", menuView.get().getItems());
	}
	
    public JSONMSG loadRoleCodes(String appkey,Long userId) {
		if(userId==null){
			return JSONMSG.newMSG(300, "加载角色过程中用户参数错误");
		}
		List<String> roleCodes=authManager.loadRoleCodesUserHave(userId, appkey, true);
		return JSONMSG.newMSG(200).put("roleCodes", roleCodes);
    }
    public JSONMSG loadAuthCodes(String appkey,Long userId) {
    	if(userId==null){
			return JSONMSG.newMSG(300, "加载角色过程中用户参数错误");
		}
		List<String> permCodes=authManager.loadPermCodesInUserDefaultRole(userId, appkey, true);//authPermissionService.loadPermCodesInUserPermByUserId(userId,appkey,true);
		Optional<String> roleCodes=authManager.loadUserDefualtRoleCode(userId, true);
		return JSONMSG.newMSG(200).put("permCodes", permCodes).put("roleCodes", roleCodes);
    }

	public IAuthMenuService getAuthMenuService() {
		return authMenuService;
	}

	public void setAuthMenuService(IAuthMenuService authMenuService) {
		this.authMenuService = authMenuService;
	}

	public IAuthManager getAuthManager() {
		return authManager;
	}

	public void setAuthManager(IAuthManager authManager) {
		this.authManager = authManager;
	}
	
}