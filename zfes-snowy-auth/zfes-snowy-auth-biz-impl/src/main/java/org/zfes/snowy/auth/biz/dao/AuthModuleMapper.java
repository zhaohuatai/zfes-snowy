/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.data.ComboboVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthModule;
@Mapper
@Lazy(true)
public interface AuthModuleMapper extends BaseMapper<AuthModule>{
	
	Long selectAuthModuleListCount(ParamMap params);
	
	List<Map<String, Object>> selectAuthModuleList(ParamMap params);
	
	void updateEnabled(@Param("ids")List<Long> ids, @Param("enabled")Boolean enabled);
	
	List<ComboboVo> selectComboboVO(@Param("appKey")String  appKey);
	
}
