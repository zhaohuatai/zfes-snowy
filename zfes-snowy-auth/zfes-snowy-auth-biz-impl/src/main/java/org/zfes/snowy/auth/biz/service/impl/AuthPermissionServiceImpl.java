/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.zfes.snowy.auth.biz.dao.AuthPermissionMapper;
import org.zfes.snowy.auth.biz.model.AuthPermission;
import org.zfes.snowy.auth.biz.service.IAuthPermissionService;
import org.zfes.snowy.auth.shiro.event.AuthCacheClearType;
import org.zfes.snowy.auth.shiro.event.AuthChangeEvent;
import org.zfes.snowy.core.idcenter.IDGenerator;
import org.zfes.snowy.core.data.DataSet;
import org.zfes.snowy.base.dao.params.ParamMap;
import org.zfes.snowy.core.util.ZAlert;
import org.zfes.snowy.core.util.ZAssert;
import org.zfes.snowy.core.util.ZBeanUtil;
import org.zfes.snowy.core.util.ZStrUtil;
@Service
@Lazy(true)
public class AuthPermissionServiceImpl  implements IAuthPermissionService{
	@Autowired
	private AuthPermissionMapper authPermissionMapper;
	@Autowired  
	private  ApplicationEventPublisher publisher;
//----------------------------------------------------------------------------------------------------	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void createAuthPermission(AuthPermission authPermission) {
		String url=authPermission.getUrl();
		ZAssert.hasText(url, "【"+authPermission.getPermCode()+"】url不能为空");
		ZBeanUtil.validateBean(authPermission);
		AuthPermission Perm=authPermissionMapper.selectByCode(authPermission.getPermCode());
		if(Perm!=null){
			ZAlert.Error("该权限编码已经存在");
		}
		authPermission.setId(IDGenerator.genLongId());
		authPermissionMapper.insertSelective(authPermission);
	}
	@Transactional(rollbackFor=Exception.class,readOnly=false)
	@Override
	public void updateAuthPermission(AuthPermission authPermission) {
		ZBeanUtil.validateBean(authPermission);
		AuthPermission perm=authPermissionMapper.selectByCode(authPermission.getPermCode());
		if(perm!=null && !(perm.getId().equals(authPermission.getId()))){
			ZAlert.Error("该权限编码已经存在");
		}
		authPermissionMapper.updateByPrimaryKeySelective(authPermission);
		
		this.publisher.publishEvent(new AuthChangeEvent(SecurityUtils.getSubject(),AuthCacheClearType.allCachedAuthorizationInfo));
		
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deletAuthPermission(Long id) {
		ZAssert.notNull(id, "请选择数据");
		authPermissionMapper.deleteByPrimaryKey(id);
		this.publisher.publishEvent(new AuthChangeEvent(SecurityUtils.getSubject(),AuthCacheClearType.allCachedAuthorizationInfo));
	}
	
//	@Transactional(rollbackFor=Exception.class)
//	@Override
//	public void batchDeletePermission(List<Long> permissionIds) {
//		ZAssert.notEmpty(permissionIds, "请选择数据");
//		authPermissionMapper.batchDeletePermission(permissionIds);
//	}

	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public Optional<AuthPermission> loadAuthPermissionById(Long id){
		if(!Objects.isNull(id)){
			return Optional.ofNullable(authPermissionMapper.selectByPrimaryKey(id));
		}
		return Optional.empty();
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	public  Optional<AuthPermission>  loadAuthPermissionByCode(String permCode){
		if(ZStrUtil.hasText(permCode)){
			return Optional.ofNullable(authPermissionMapper.selectByCode(permCode));
		}
		return Optional.empty();
	}

	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public DataSet loadAuthPermissionDataSet(String appKey,Map<String, Object> params) {
		ParamMap pm=ParamMap.filterParam(params);
		pm.getStr("name").ifPresent(v->pm.updateParam( "name", "%"+v+"%"));
		pm.getStr("permCode").ifPresent(v->pm.updateParam( "permCode", "%"+v+"%"));
		pm.getBoolean("enabled").ifPresent(v->pm.updateParam( "enabled", v));
		pm.getLong("moduleId").ifPresent(v->pm.updateParam( "moduleId", v));
		pm.updateParam("appKey", appKey);
		DataSet ds=DataSet.newDS(authPermissionMapper.selectAuthPermissionListCount(pm), authPermissionMapper.selectAuthPermissionList(pm));
		return ds;
	}
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<AuthPermission> loadPermListByIds(List<Long> permsIdList){
		return authPermissionMapper.selectPermListByIds(permsIdList);
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=true)
	@Override
	public List<AuthPermission> loadPermListByCodes(List<String> permsCodeList){
		return authPermissionMapper.selectPermListByCodes(permsCodeList);
	}
	
	@Transactional(rollbackFor=Exception.class,readOnly=false)	
	@Override
	public void setPermissionEnabled(List<Long> ids, Boolean enabled) {
		ZAssert.notEmpty(ids, "请选择数据");
		ZAssert.notNull(enabled, "状态参数");
		authPermissionMapper.updateEnabled(ids,enabled);
	}
//	@Transactional(rollbackFor=Exception.class,readOnly=false)	
//	@Override
//	public void batchDeleteAuthPermission(List<Long> ids){
//		ZAssert.notEmpty(ids, "请选择数据");
//		authPermissionMapper.batchDeletePermission(ids);
//	}
	 
}
