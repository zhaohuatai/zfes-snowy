///**
// * Copyright (c) 2016-2020 https://github.com/zhaohuatai
// *
// * contact z_huatai@qq.com
// *  
// */
//package org.zfes.snowy.auth.biz.dao;
//import java.util.List;
//
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Param;
//import org.zfes.snowy.auth.biz.model.AuthGroupRole;
//
//import org.zfes.snowy.base.dao.mybatis.BaseMapper;
//@Mapper
//public interface AuthGroupRoleMapper extends BaseMapper<AuthGroupRole>{
//  
//	 List<Long> selectRoleIdsByGroupId(@Param("groupId")Long groupId, @Param("enabled") Boolean enabled);
//	
//	 List<String> selectRoleCodesByGroupId(@Param("groupId")Long groupId, @Param("enabled") Boolean enabled);
//	
//	 List<Long> selectGroupIdsByRoleId(@Param("roleId")Long roleId, @Param("enabled") Boolean enabled);
//	
//	int deleteRolesFromGroupRole(@Param("roleIds")List<Long> roleIds,@Param("groupId")Long groupId);
//
//	int  batchInsertGroupRole(@Param("authGroupRoleList")List<AuthGroupRole> authGroupRoleList);
//
//}
