/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthGroupUser;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
@Mapper
@Lazy(true)
public interface AuthGroupUserMapper extends BaseMapper<AuthGroupUser>{
	
	public List<Long> selectUserIdsByGroupId(@Param("groupId")Long groupId, @Param("enabled") Boolean enabled);
	
	public List<Long> selectGroupIdsByUserId(@Param("userId")Long userId,  @Param("enabled") Boolean enabled);

	int deleteUsersFromGroupUser(@Param("userIds")List<Long> userIds, @Param("groupId")Long groupId);

	int batchInsertGroupUser(@Param("authGroupUserList")List<AuthGroupUser> authGroupUserList);
}
