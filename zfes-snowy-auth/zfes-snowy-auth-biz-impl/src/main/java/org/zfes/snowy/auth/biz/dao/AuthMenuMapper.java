/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact z_huatai@qq.com
 *  
 */
package org.zfes.snowy.auth.biz.dao;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.context.annotation.Lazy;
import org.zfes.snowy.auth.biz.model.AuthMenu;

import org.zfes.snowy.base.dao.mybatis.BaseMapper;
import org.zfes.snowy.base.dao.params.ParamMap;
@Mapper
@Lazy(true)
public interface AuthMenuMapper extends BaseMapper<AuthMenu>{

	List<Map<String,Object>> selectAuthMenuList(Map<String,Object> params);
	
	Long selectAuthMenuListCount(Map<String,Object> params);

	void updateEnabled(@Param("ids")List<Long> ids, @Param("enabled")Boolean enabled);
	
    /**
     * 加载菜单树
     * @param pm
     * @return
     */
	List<Map<String, Object>> selectAuthMenuTree(ParamMap pm);
  
	AuthMenu selectRootMenu();
	
	List<AuthMenu> selectByAppKey(@Param("appKey")String appKey);

	void batchDelete(@Param("ids")List<Long> ids);


	
}
