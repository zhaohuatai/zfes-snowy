/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Component
@ConfigurationProperties(prefix = "server.tomcat")
public class TomcatConfigProperties  {
	
	private @Value("${enableApr:false}") Boolean enableApr;

	public Boolean getEnableApr() {
		return enableApr;
	}

	public void setEnableApr(Boolean enableApr) {
		this.enableApr = enableApr;
	}
	
	
	

	
}
