/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;

import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShiroLifecycleConfiguration {
	 @Bean(name = "lifecycleBeanPostProcessor")
	 public LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
	    return new LifecycleBeanPostProcessor();
	 }
	 
}
