/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;


import javax.servlet.DispatcherType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.zfes.snowy.ss.config.properties.ShiroConfigProperties;
@Configuration
public class WebRegisteConfiguration {
	@Autowired
	private  ShiroConfigProperties shiroConfigProperties; 
//	@Autowired  
//	private  ApplicationEventPublisher publisher;
	
//	@Bean  
//	public AppExceptionHandler exceptionHandler() {  
//		AppExceptionHandler exceptionHandler = new AppExceptionHandler();  
//		exceptionHandler.setPublisher(publisher);
//	   return exceptionHandler;  
//	}  
	
	@Bean  
	public FilterRegistrationBean filterRegistrationBean() {  
	   FilterRegistrationBean filterRegistration = new FilterRegistrationBean();  
	   filterRegistration.setFilter(new DelegatingFilterProxy("shiroFilter"));   
	   filterRegistration.setEnabled(true);  
	   filterRegistration.addUrlPatterns(shiroConfigProperties.getUrlPatterns());
	   filterRegistration.setAsyncSupported(shiroConfigProperties.getAsyncSupported());
	   filterRegistration.setDispatcherTypes(DispatcherType.REQUEST,DispatcherType.ASYNC);  
	   return filterRegistration;  
	}  
	
	 //@Bean  
	 public ServletRegistrationBean testServletRegistration() {  
	    ServletRegistrationBean  registration = new ServletRegistrationBean();  
	    registration.addUrlMappings("/");  
	    return registration;  
	}  
	  
}
