/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.converter.HttpMessageConverter;
import org.zfes.snowy.base.web.json.FastJsonBigNumFilter;
import org.zfes.snowy.base.web.json.FastJsonByteArrayTextFilter;
import org.zfes.snowy.ss.config.properties.FastJsonConfigProperties;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.google.common.collect.Lists;
@Configuration
@ConditionalOnClass({JSON.class})
public class FastJsonConfigration {
	
	@Autowired
	private   FastJsonConfigProperties fastJsonProperties;
	@Lazy(true)
	@Bean
	public HttpMessageConverter<?> fastJsonHttpMessageConverter() {
	       FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
	       FastJsonConfig fastJsonConfig = new FastJsonConfig();
	      
	       fastJsonConfig.setSerializerFeatures(initSerializerFeature( ));
	       
	       fastJsonConfig.setDateFormat(fastJsonProperties.getDateFormat());
	       
	       fastJsonConfig.setSerializeFilters( initSerializeFilters());
	       
	       
	       fastConverter.setFastJsonConfig(fastJsonConfig);
	       HttpMessageConverter<?> converter = fastConverter;
	       return converter;
	    }
	private  SerializerFeature[] initSerializerFeature() {
		 List<SerializerFeature> featrues=Lists.newArrayList();
	       
	       if(fastJsonProperties.getWriteMapNullValue()){
	    	   featrues.add(SerializerFeature.WriteMapNullValue);
	    	}
	       
	       if(fastJsonProperties.getWriteNullListAsEmpty()){//List字段如果为null,输出为[],而非null
	    	   featrues.add(SerializerFeature.WriteNullListAsEmpty);
	    	}
	       
	       if(fastJsonProperties.getQuoteFieldNames()){ //输出key时是否使用双引号,默认为true
	    	   featrues.add(SerializerFeature.QuoteFieldNames);
	    	 }
	       
	       if(fastJsonProperties.getWriteNullStringAsEmpty()){ //字符类型字段如果为null,输出为”“,而非null
	    	   featrues.add(SerializerFeature.WriteNullStringAsEmpty);
	    	}
	       
	       if(fastJsonProperties.getWriteDateUseDateFormat()){
	    	   featrues.add(SerializerFeature.WriteDateUseDateFormat);
	    	 }
	       
	       if(fastJsonProperties.getDisableCircularReferenceDetect()){//消除循环引用
	    	   featrues.add(SerializerFeature.DisableCircularReferenceDetect);
	    	}
	       featrues.add(SerializerFeature.PrettyFormat);
	       
	       SerializerFeature[] serializerFeatureArray = new SerializerFeature[featrues.size()];
	       featrues.toArray(serializerFeatureArray);
	       return serializerFeatureArray;
	}
	
	 public SerializeFilter[] initSerializeFilters(){
		List<SerializeFilter> filters=new ArrayList<>();
		if(fastJsonProperties.getEnableBigNumFilter()){
			filters.add(new FastJsonBigNumFilter());
		}
		if(fastJsonProperties.getEnableByteArrayTextFilter()){
			filters.add(new FastJsonByteArrayTextFilter());
		}
		SerializeFilter[] filterArrays= new SerializeFilter[filters.size()];
		filters.toArray(filterArrays);
		return filterArrays;
	}
}
