/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;


import org.springframework.context.annotation.Configuration;

import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Lazy;
@Lazy(true)
@Configuration
@ImportResource(locations={"classpath:config/spring/spring-config-captcha.xml"})
public class KaptchaConfiguration {
	
}
