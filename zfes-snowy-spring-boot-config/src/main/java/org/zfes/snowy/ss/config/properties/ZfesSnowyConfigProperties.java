/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Component
@ConfigurationProperties(prefix = "snowy")
public class ZfesSnowyConfigProperties  {
	
	@Value("${snowy.shardRedisWokers:false}")
	private Boolean shardRedisWokers;
	
	@Value("${snowy.serializeType:fast}") 
	private String serializeType;

	public String getSerializeType() {
		return serializeType;
	}

	public void setSerializeType(String serializeType) {
		this.serializeType = serializeType;
	}
	public Boolean getShardRedisWokers() {
		return shardRedisWokers;
	}

	public void setShardRedisWokers(Boolean shardRedisWokers) {
		this.shardRedisWokers = shardRedisWokers;
	}
	
}
