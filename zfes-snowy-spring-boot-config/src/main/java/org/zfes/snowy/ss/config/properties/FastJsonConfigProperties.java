/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Component
@ConfigurationProperties(prefix = "fastjson")
public class FastJsonConfigProperties  {
	
	@Value("${fastjson.writeMapNullValue:true}")
	private Boolean writeMapNullValue;
	
	@Value("${fastjson.writeNullListAsEmpty:true}")
	private Boolean writeNullListAsEmpty;
	
	@Value("${fastjson.quoteFieldNames:true}")
	private Boolean quoteFieldNames;
	
	@Value("${fastjson.writeNullStringAsEmpty:true}")
	private Boolean writeNullStringAsEmpty;
	
	@Value("${fastjson.writeDateUseDateFormat:true}")
	private Boolean writeDateUseDateFormat;
	
	@Value("${fastjson.DisableCircularReferenceDetect:true}")
	private Boolean disableCircularReferenceDetect;
	
	@Value("${fastjson.enableBigNumFilter:true}")
	private Boolean enableBigNumFilter;
	
	@Value("${fastjson.enableByteArrayTextFilter:true}")
	private Boolean enableByteArrayTextFilter;
	
	@Value("${fastjson.dateFormat:yyyy-MM-dd HH:mm:ss}")
	private String dateFormat;

	public Boolean getWriteMapNullValue() {
		return writeMapNullValue;
	}

	public void setWriteMapNullValue(Boolean writeMapNullValue) {
		this.writeMapNullValue = writeMapNullValue;
	}

	public Boolean getWriteNullListAsEmpty() {
		return writeNullListAsEmpty;
	}

	public void setWriteNullListAsEmpty(Boolean writeNullListAsEmpty) {
		this.writeNullListAsEmpty = writeNullListAsEmpty;
	}

	public Boolean getQuoteFieldNames() {
		return quoteFieldNames;
	}

	public void setQuoteFieldNames(Boolean quoteFieldNames) {
		this.quoteFieldNames = quoteFieldNames;
	}

	public Boolean getWriteNullStringAsEmpty() {
		return writeNullStringAsEmpty;
	}

	public void setWriteNullStringAsEmpty(Boolean writeNullStringAsEmpty) {
		this.writeNullStringAsEmpty = writeNullStringAsEmpty;
	}

	public Boolean getWriteDateUseDateFormat() {
		return writeDateUseDateFormat;
	}

	public void setWriteDateUseDateFormat(Boolean writeDateUseDateFormat) {
		this.writeDateUseDateFormat = writeDateUseDateFormat;
	}

	public Boolean getDisableCircularReferenceDetect() {
		return disableCircularReferenceDetect;
	}

	public void setDisableCircularReferenceDetect(Boolean disableCircularReferenceDetect) {
		this.disableCircularReferenceDetect = disableCircularReferenceDetect;
	}

	public Boolean getEnableBigNumFilter() {
		return enableBigNumFilter;
	}

	public void setEnableBigNumFilter(Boolean enableBigNumFilter) {
		this.enableBigNumFilter = enableBigNumFilter;
	}

	public Boolean getEnableByteArrayTextFilter() {
		return enableByteArrayTextFilter;
	}

	public void setEnableByteArrayTextFilter(Boolean enableByteArrayTextFilter) {
		this.enableByteArrayTextFilter = enableByteArrayTextFilter;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	

	
}
