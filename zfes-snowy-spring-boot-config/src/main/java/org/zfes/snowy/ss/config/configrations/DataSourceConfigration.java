///**
// * Copyright (c) 2016-2020 https://github.com/zhaohuatai
// *
// * contact 824069438@qq.com
// *  
// */
//package org.zfes.snowy.ss.config.configrations;
//
//import java.sql.SQLException;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.boot.jdbc.DatabaseDriver;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import org.zfes.snowy.core.util.ZStrUtil;
//
//import com.alibaba.druid.pool.DruidDataSource;
//
//@Configuration
//@EnableTransactionManagement 
//@EnableConfigurationProperties
//public class DataSourceConfigration {
//
//	@Autowired
//	private DataSourceProperties dataSourceProperties;
//	
//    @Bean
//    @ConfigurationProperties("spring.datasource.druid")
//    public DruidDataSource dataSource() {
//        DruidDataSource dataSource = new DruidDataSource();
//        dataSource.setDriverClassName(dataSourceProperties.determineDriverClassName());
//        dataSource.setUrl(dataSourceProperties.determineUrl());
//        dataSource.setUsername(dataSourceProperties.determineUsername());
//        dataSource.setPassword(dataSourceProperties.determinePassword());
//        DatabaseDriver databaseDriver = DatabaseDriver.fromJdbcUrl(dataSourceProperties.determineUrl());
//        String validationQuery = databaseDriver.getValidationQuery();
//        if (validationQuery != null) {
//            dataSource.setTestOnBorrow(false);
//            dataSource.setValidationQuery(validationQuery);
//        }
//        try {
//        	String dr=dataSourceProperties.getDriverClassName();
//        	if(ZStrUtil.hasText(dr)&&dr.toLowerCase().indexOf("sqlite")<0){
//        		dataSource.setFilters("stat,wall");
//        	}
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return dataSource;
//    }
////    @Bean
////    @Order(-1)
////    public DynamicDataSourceAspect dynamicDataSourceAspect(){
////    	DynamicDataSourceAspect spect=new DynamicDataSourceAspect();
////    	return spect;
////    }
//    
//    
//    
////      spring.datasource.primary.url=jdbc:mysql://localhost:3306/test1
////    	spring.datasource.primary.username=root
////    	spring.datasource.primary.password=root
////    	spring.datasource.primary.driver-class-name=com.mysql.jdbc.Driver
////
////    	spring.datasource.secondary.url=jdbc:mysql://localhost:3306/test2
////    	spring.datasource.secondary.username=root
////    	spring.datasource.secondary.password=root
////    	spring.datasource.secondary.driver-class-name=com.mysql.jdbc.Driver
////		 @Bean(name = "primaryDataSource")
////		 @Qualifier("primaryDataSource")
////		 @ConfigurationProperties(prefix = "spring.datasource.primary")
////		 public DataSource primaryDataSource(){
////		   return DataSourceBuilder.create().build();
////		 }
////		 
////	    @Primary
////	    @Bean(name = "secondaryDataSource")
////	    @Qualifier("secondaryDataSource")
////	    @ConfigurationProperties(prefix = "spring.datasource.secondary")
////	    public DataSource secondaryDataSource(){
////	        return DataSourceBuilder.create().build();
////	    }
//	    
//}
