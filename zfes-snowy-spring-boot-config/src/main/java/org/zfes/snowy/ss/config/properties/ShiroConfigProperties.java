/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.properties;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Component
@ConfigurationProperties(prefix = "shiro")
public class ShiroConfigProperties  {
	
	
	private  Map<String,String> filterChainDefinitionMap = new LinkedHashMap<>();
	
	private  Map<String,String> filtersMap = new LinkedHashMap<>();
	
	private @Value("${urlPatterns:/*}") String urlPatterns;
	
	private @Value("${asyncSupported:true}") Boolean asyncSupported;
	
	private @Value("${defaultAuthecFailureUrl:/login.html}") String defaultAuthecFailureUrl;
	private @Value("${authecFailureForwardToDestination:false}") Boolean authecFailureForwardToDestination;
	
	private @Value("${defaultLogoutDirectUrl:/login.html}") String defaultLogoutDirectUrl;
	private @Value("${logoutForwardToDestination:false}") Boolean logoutForwardToDestination;
	
	private @Value("${defaultAuthecSuccessUrl:/index.html}") String defaultAuthecSuccessUrl;
	private @Value("${authecSuccessForwardToDestination:false}") Boolean authecSuccessForwardToDestination;
	
	private @Value("${defaultUnAuthecDirectUrl:/login.html}") String defaultUnAuthecDirectUrl;
	private @Value("${unAuthecDirectForwardToDestination:false}") Boolean unAuthecForwardToDestination;
	
	private @Value("${defaultUnauthorizedUrl:/pages/401.html}") String defaultUnauthorizedUrl;
	private @Value("${unauthorizedForwardToDestination:false}") Boolean unauthorizedForwardToDestination;
	
	private @Value("${defaultCsrfValidateFailureUrl:/pages/4001.html}") String defaultCsrfValidateFailureUrl;
	private @Value("${csrfValidateForwardToDestination:false}") Boolean csrfValidateForwardToDestination;
	private @Value("${csrfTokenName:snowy_csrf_token}") String csrfTokenName;
	
//--------------------------------AuthenticationFilter-----------------------------------------------------
	private @Value("${loginPortUrl:/login.htm}") String loginPortUrl;//extern from SnowyShiroAccessControlFilter
	
	private @Value("${loginSubmitUrl:/api/auth/login}") String loginSubmitUrl;//extern from SnowyShiroAuthenticationFilter
	
	private @Value("${ssoEnabled:false}") Boolean ssoEnabled;//extern from SnowyShiroTokenAuthenticationFilter
	private @Value("${usernameParam:username}")String usernameParam;//extern from SnowyShiroTokenAuthenticationFilter
	private @Value("${passwordParam:password}")String passwordParam;//extern from SnowyShiroTokenAuthenticationFilter
	private @Value("${rememberMeParam:rememberMe}")String rememberMeParam ;//extern from SnowyShiroTokenAuthenticationFilter
	private @Value("${loginOrginParam:loginOrgin}") String  loginOrginParam;
	
	
	//-------------SnowyShiroCapchaProducerFilter------------------------------------------
	private @Value("${captchaImageUrl:/api/auth/captcha}") String captchaImageUrl;
	
	//-------------userRealm------------------------------------------
	private @Value("${authorizationCachingEnabled:true}") Boolean authorizationCachingEnabled;
	private @Value("${authorizationCacheName:sh_auhrz:}") String authorizationCacheName;
	
	private @Value("${authenticationCachingEnabled:false}") Boolean authenticationCachingEnabled;
	private @Value("${authenticationCacheName:sh_auhen:}") String authenticationCacheName;
	 
	//-------credentialsMatcher--------
	private @Value("${hashAlgorithmName:MD5}") String hashAlgorithmName;
	private @Value("${storedCredentialsHexEncoded:true}") Boolean storedCredentialsHexEncoded;
	private @Value("${hashIterations:2}") Integer hashIterations;
	
	//-------------sessionManager------------------------------------------
	private @Value("${globalSessionTimeout:1800000}") Integer globalSessionTimeout;
	private @Value("${sessionValidationSchedulerEnabled:true}") Boolean sessionValidationSchedulerEnabled;
	private @Value("${deleteInvalidSessions:true}") Boolean deleteInvalidSessions;
	private @Value("${sessionIdCookieEnabled:true}") Boolean sessionIdCookieEnabled;
	
	private @Value("${sessionDaoType:false}") String sessionDaoType;
	//-------------sessionValidationScheduler------------------------------------------
	private @Value("${interval:1800000}") Integer interval;
	
	//-------------sessionDAO------------------------------------------
	private @Value("${activeSessionsCacheName:sh_sessions:}") String activeSessionsCacheName;
	
	//-------------rememberMeManager-----------------------------------
	private @Value("${rememberMePublickKey:4AvVhmFLUs0KTA3Kprsdag==}") String rememberMePublickKey;
	
	//-------------sessionIdCookie-----------------------------------
	private @Value("${sessionCookieName:sid}") String sessionCookieName;
	private @Value("${sessionCookieHttpOnly:true}") Boolean sessionCookieHttpOnly;
	private @Value("${sessionCookieMaxAge:-1}") Integer sessionCookieMaxAge;
	
	//-------------rememberMeCookie-----------------------------------
	private @Value("${rememberMeCookieName:rememberMe}") String rememberMeCookieName;
	private @Value("${rememberMeCookieHttpOnly:true}") Boolean rememberMeCookieHttpOnly;
	private @Value("${rememberMeCookieMaxAge:2592000}") Integer rememberMeCookieMaxAge;
	
	private @Value("${shiroCacheType:memory}") String shiroCacheType;// memory ehcache redis
	private @Value("${cacheConfig:classpath:config/ehcache/ehcache-shiro.xml}") String cacheConfig;
	private @Value("${captchaParam:captcha}") String captchaParam;
	
	
	private @Value("${isEnableSessionStrategy:false}") Boolean isEnableSessionStrategy;
	private @Value("${isEnableCSRFToken:false}") Boolean isEnableCSRFToken;
	
	private @Value("${maxSession:1}") Integer maxSession;
	
//	private @Value("${clientAppKey:}") String clientAppKey;
	
	private @Value("${isClientApp:false}") Boolean isClientApp;
	
	private @Value("${shiroUseRedisCluster:false}") Boolean shiroUseRedisCluster;
	
	
	private @Value("${useRedisValidation:false}") Boolean useRedisValidation;
	
	private @Value("${ssoCenterSyncPath:/api/auth/sso/center/sync.so}") String ssoCenterSyncPath;
	private @Value("${ssoClientSyncPath:/api/auth/sso/client/sync.so}") String ssoClientSyncPath;
	
	private @Value("${ssoCenterDomain:}") String ssoCenterDomain;
	
	private List<String> allowedSSOClientDomains;
	
	
	public Boolean getAsyncSupported() {
		return asyncSupported;
	}
	public void setAsyncSupported(Boolean asyncSupported) {
		this.asyncSupported = asyncSupported;
	}
	
	public String getDefaultAuthecFailureUrl() {
		return defaultAuthecFailureUrl;
	}
	public void setDefaultAuthecFailureUrl(String defaultAuthecFailureUrl) {
		this.defaultAuthecFailureUrl = defaultAuthecFailureUrl;
	}
	public String getDefaultLogoutDirectUrl() {
		return defaultLogoutDirectUrl;
	}
	public void setDefaultLogoutDirectUrl(String defaultLogoutDirectUrl) {
		this.defaultLogoutDirectUrl = defaultLogoutDirectUrl;
	}
	public String getDefaultAuthecSuccessUrl() {
		return defaultAuthecSuccessUrl;
	}
	public void setDefaultAuthecSuccessUrl(String defaultAuthecSuccessUrl) {
		this.defaultAuthecSuccessUrl = defaultAuthecSuccessUrl;
	}
	
	public String getDefaultUnAuthecDirectUrl() {
		return defaultUnAuthecDirectUrl;
	}
	public void setDefaultUnAuthecDirectUrl(String defaultUnAuthecDirectUrl) {
		this.defaultUnAuthecDirectUrl = defaultUnAuthecDirectUrl;
	}
	public String getDefaultUnauthorizedUrl() {
		return defaultUnauthorizedUrl;
	}
	public void setDefaultUnauthorizedUrl(String defaultUnauthorizedUrl) {
		this.defaultUnauthorizedUrl = defaultUnauthorizedUrl;
	}
	public Boolean getAuthorizationCachingEnabled() {
		return authorizationCachingEnabled;
	}
	public void setAuthorizationCachingEnabled(Boolean authorizationCachingEnabled) {
		this.authorizationCachingEnabled = authorizationCachingEnabled;
	}
	public String getAuthorizationCacheName() {
		return authorizationCacheName;
	}
	public void setAuthorizationCacheName(String authorizationCacheName) {
		this.authorizationCacheName = authorizationCacheName;
	}
	public Boolean getAuthenticationCachingEnabled() {
		return authenticationCachingEnabled;
	}
	public void setAuthenticationCachingEnabled(Boolean authenticationCachingEnabled) {
		this.authenticationCachingEnabled = authenticationCachingEnabled;
	}
	public String getAuthenticationCacheName() {
		return authenticationCacheName;
	}
	public void setAuthenticationCacheName(String authenticationCacheName) {
		this.authenticationCacheName = authenticationCacheName;
	}
	public String getHashAlgorithmName() {
		return hashAlgorithmName;
	}
	public void setHashAlgorithmName(String hashAlgorithmName) {
		this.hashAlgorithmName = hashAlgorithmName;
	}
	public Boolean getStoredCredentialsHexEncoded() {
		return storedCredentialsHexEncoded;
	}
	public void setStoredCredentialsHexEncoded(Boolean storedCredentialsHexEncoded) {
		this.storedCredentialsHexEncoded = storedCredentialsHexEncoded;
	}
	public Integer getHashIterations() {
		return hashIterations;
	}
	public void setHashIterations(Integer hashIterations) {
		this.hashIterations = hashIterations;
	}
	public Integer getGlobalSessionTimeout() {
		return globalSessionTimeout;
	}
	public void setGlobalSessionTimeout(Integer globalSessionTimeout) {
		this.globalSessionTimeout = globalSessionTimeout;
	}
	public Boolean getSessionValidationSchedulerEnabled() {
		return sessionValidationSchedulerEnabled;
	}
	public void setSessionValidationSchedulerEnabled(Boolean sessionValidationSchedulerEnabled) {
		this.sessionValidationSchedulerEnabled = sessionValidationSchedulerEnabled;
	}
	public Boolean getDeleteInvalidSessions() {
		return deleteInvalidSessions;
	}
	public void setDeleteInvalidSessions(Boolean deleteInvalidSessions) {
		this.deleteInvalidSessions = deleteInvalidSessions;
	}
	public Boolean getSessionIdCookieEnabled() {
		return sessionIdCookieEnabled;
	}
	public void setSessionIdCookieEnabled(Boolean sessionIdCookieEnabled) {
		this.sessionIdCookieEnabled = sessionIdCookieEnabled;
	}
	public Integer getInterval() {
		return interval;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public String getActiveSessionsCacheName() {
		return activeSessionsCacheName;
	}
	public void setActiveSessionsCacheName(String activeSessionsCacheName) {
		this.activeSessionsCacheName = activeSessionsCacheName;
	}
	public String getRememberMePublickKey() {
		return rememberMePublickKey;
	}
	public void setRememberMePublickKey(String rememberMePublickKey) {
		this.rememberMePublickKey = rememberMePublickKey;
	}
	public String getSessionCookieName() {
		return sessionCookieName;
	}
	public void setSessionCookieName(String sessionCookieName) {
		this.sessionCookieName = sessionCookieName;
	}
	public Boolean getSessionCookieHttpOnly() {
		return sessionCookieHttpOnly;
	}
	public void setSessionCookieHttpOnly(Boolean sessionCookieHttpOnly) {
		this.sessionCookieHttpOnly = sessionCookieHttpOnly;
	}
	public Integer getSessionCookieMaxAge() {
		return sessionCookieMaxAge;
	}
	public void setSessionCookieMaxAge(Integer sessionCookieMaxAge) {
		this.sessionCookieMaxAge = sessionCookieMaxAge;
	}
	public String getRememberMeCookieName() {
		return rememberMeCookieName;
	}
	public void setRememberMeCookieName(String rememberMeCookieName) {
		this.rememberMeCookieName = rememberMeCookieName;
	}
	public Boolean getRememberMeCookieHttpOnly() {
		return rememberMeCookieHttpOnly;
	}
	public void setRememberMeCookieHttpOnly(Boolean rememberMeCookieHttpOnly) {
		this.rememberMeCookieHttpOnly = rememberMeCookieHttpOnly;
	}
	public Integer getRememberMeCookieMaxAge() {
		return rememberMeCookieMaxAge;
	}
	public void setRememberMeCookieMaxAge(Integer rememberMeCookieMaxAge) {
		this.rememberMeCookieMaxAge = rememberMeCookieMaxAge;
	}
	public String getUrlPatterns() {
		return urlPatterns;
	}
	public void setUrlPatterns(String urlPatterns) {
		this.urlPatterns = urlPatterns;
	}
	public String getShiroCacheType() {
		return shiroCacheType;
	}
	public void setShiroCacheType(String shiroCacheType) {
		this.shiroCacheType = shiroCacheType;
	}
	public String getCacheConfig() {
		return cacheConfig;
	}
	public void setCacheConfig(String cacheConfig) {
		this.cacheConfig = cacheConfig;
	}
	
	public Map<String, String> getFilterChainDefinitionMap() {
		return filterChainDefinitionMap;
	}
	public void setFilterChainDefinitionMap(Map<String, String> filterChainDefinitionMap) {
		this.filterChainDefinitionMap = filterChainDefinitionMap;
	}
	public String getSessionDaoType() {
		return sessionDaoType;
	}
	public void setSessionDaoType(String sessionDaoType) {
		this.sessionDaoType = sessionDaoType;
	}
	public Boolean getAuthecFailureForwardToDestination() {
		return authecFailureForwardToDestination;
	}
	public void setAuthecFailureForwardToDestination(Boolean authecFailureForwardToDestination) {
		this.authecFailureForwardToDestination = authecFailureForwardToDestination;
	}
	public Boolean getLogoutForwardToDestination() {
		return logoutForwardToDestination;
	}
	public void setLogoutForwardToDestination(Boolean logoutForwardToDestination) {
		this.logoutForwardToDestination = logoutForwardToDestination;
	}
	public Boolean getAuthecSuccessForwardToDestination() {
		return authecSuccessForwardToDestination;
	}
	public void setAuthecSuccessForwardToDestination(Boolean authecSuccessForwardToDestination) {
		this.authecSuccessForwardToDestination = authecSuccessForwardToDestination;
	}
	public Boolean getUnAuthecForwardToDestination() {
		return unAuthecForwardToDestination;
	}
	public void setUnAuthecForwardToDestination(Boolean unAuthecForwardToDestination) {
		this.unAuthecForwardToDestination = unAuthecForwardToDestination;
	}
	public Boolean getUnauthorizedForwardToDestination() {
		return unauthorizedForwardToDestination;
	}
	public void setUnauthorizedForwardToDestination(Boolean unauthorizedForwardToDestination) {
		this.unauthorizedForwardToDestination = unauthorizedForwardToDestination;
	}
	public String getDefaultCsrfValidateFailureUrl() {
		return defaultCsrfValidateFailureUrl;
	}
	public void setDefaultCsrfValidateFailureUrl(String defaultCsrfValidateFailureUrl) {
		this.defaultCsrfValidateFailureUrl = defaultCsrfValidateFailureUrl;
	}
	public Boolean getCsrfValidateForwardToDestination() {
		return csrfValidateForwardToDestination;
	}
	public void setCsrfValidateForwardToDestination(Boolean csrfValidateForwardToDestination) {
		this.csrfValidateForwardToDestination = csrfValidateForwardToDestination;
	}
	public String getCsrfTokenName() {
		return csrfTokenName;
	}
	public void setCsrfTokenName(String csrfTokenName) {
		this.csrfTokenName = csrfTokenName;
	}
	public String getCaptchaImageUrl() {
		return captchaImageUrl;
	}
	public void setCaptchaImageUrl(String captchaImageUrl) {
		this.captchaImageUrl = captchaImageUrl;
	}
	public String getLoginPortUrl() {
		return loginPortUrl;
	}
	public void setLoginPortUrl(String loginPortUrl) {
		this.loginPortUrl = loginPortUrl;
	}
	public String getLoginSubmitUrl() {
		return loginSubmitUrl;
	}
	public void setLoginSubmitUrl(String loginSubmitUrl) {
		this.loginSubmitUrl = loginSubmitUrl;
	}

	public String getUsernameParam() {
		return usernameParam;
	}
	public void setUsernameParam(String usernameParam) {
		this.usernameParam = usernameParam;
	}
	public String getPasswordParam() {
		return passwordParam;
	}
	public void setPasswordParam(String passwordParam) {
		this.passwordParam = passwordParam;
	}
	public String getRememberMeParam() {
		return rememberMeParam;
	}
	public void setRememberMeParam(String rememberMeParam) {
		this.rememberMeParam = rememberMeParam;
	}

	public Boolean getSsoEnabled() {
		return ssoEnabled;
	}
	public void setSsoEnabled(Boolean ssoEnabled) {
		this.ssoEnabled = ssoEnabled;
	}
	public String getCaptchaParam() {
		return captchaParam;
	}
	public void setCaptchaParam(String captchaParam) {
		this.captchaParam = captchaParam;
	}
	public Boolean getIsEnableSessionStrategy() {
		return isEnableSessionStrategy;
	}
	public void setIsEnableSessionStrategy(Boolean isEnableSessionStrategy) {
		this.isEnableSessionStrategy = isEnableSessionStrategy;
	}
	public Integer getMaxSession() {
		return maxSession;
	}
	public void setMaxSession(Integer maxSession) {
		this.maxSession = maxSession;
	}
	public String getLoginOrginParam() {
		return loginOrginParam;
	}
	public void setLoginOrginParam(String loginOrginParam) {
		this.loginOrginParam = loginOrginParam;
	}
	

	public Boolean getUseRedisValidation() {
		return useRedisValidation;
	}
	public void setUseRedisValidation(Boolean useRedisValidation) {
		this.useRedisValidation = useRedisValidation;
	}
	public Boolean getIsClientApp() {
		return isClientApp;
	}
	public void setIsClientApp(Boolean isClientApp) {
		this.isClientApp = isClientApp;
	}
	public Map<String, String> getFiltersMap() {
		return filtersMap;
	}
	public void setFiltersMap(Map<String, String> filtersMap) {
		this.filtersMap = filtersMap;
	}
	public Boolean getIsEnableCSRFToken() {
		return isEnableCSRFToken;
	}
	public void setIsEnableCSRFToken(Boolean isEnableCSRFToken) {
		this.isEnableCSRFToken = isEnableCSRFToken;
	}
	public String getSsoCenterSyncPath() {
		return ssoCenterSyncPath;
	}
	public void setSsoCenterSyncPath(String ssoCenterSyncPath) {
		this.ssoCenterSyncPath = ssoCenterSyncPath;
	}
	public String getSsoClientSyncPath() {
		return ssoClientSyncPath;
	}
	public void setSsoClientSyncPath(String ssoClientSyncPath) {
		this.ssoClientSyncPath = ssoClientSyncPath;
	}

	public List<String> getAllowedSSOClientDomains() {
		return allowedSSOClientDomains;
	}
	public void setAllowedSSOClientDomains(List<String> allowedSSOClientDomains) {
		this.allowedSSOClientDomains = allowedSSOClientDomains;
	}
	public String getSsoCenterDomain() {
		return ssoCenterDomain;
	}
	public void setSsoCenterDomain(String ssoCenterDomain) {
		this.ssoCenterDomain = ssoCenterDomain;
	}
	public Boolean getShiroUseRedisCluster() {
		return shiroUseRedisCluster;
	}
	public void setShiroUseRedisCluster(Boolean shiroUseRedisCluster) {
		this.shiroUseRedisCluster = shiroUseRedisCluster;
	}
}
