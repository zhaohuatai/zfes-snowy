/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

//@Configuration
public class AsyncSchedulingCustomConfigurer implements SchedulingConfigurer{
	    
		@Override
		public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
			TaskScheduler scheduler = this.taskScheduler();
			taskRegistrar.setTaskScheduler(scheduler);
			
		}
		
		@Bean(destroyMethod="shutdown")
		public ThreadPoolTaskScheduler taskScheduler(){
			ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
			scheduler.setPoolSize(5);
			scheduler.setThreadNamePrefix("task-");
			scheduler.setAwaitTerminationSeconds(60);
			scheduler.setWaitForTasksToCompleteOnShutdown(true);
			return scheduler;
		}

}
