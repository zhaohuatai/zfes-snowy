/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;

import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties.Cluster;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.zfes.snowy.common.nosql.redis.jedis.ShardedJedisManager;
import org.zfes.snowy.common.nosql.redis.jedis.SnowyJedisManager;
import org.zfes.snowy.core.util.ZStrUtil;
import org.zfes.snowy.ss.config.custom.RedisCustomSerializer;
import org.zfes.snowy.ss.config.properties.ZfesSnowyConfigProperties;

import com.google.common.collect.Lists;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;
@Configuration
public class RedisShardConfigration {
	@Autowired
	private ZfesSnowyConfigProperties snowyConfigProperties;
	@Autowired
	private RedisProperties redisProperties;
	
	
	@Bean
	@Lazy(true)
	public SnowyJedisManager JedisManager(){
		ShardedJedisManager jedisManager=new ShardedJedisManager();
		jedisManager.setShardedJedisPool( getShardedJedisPool());
		return jedisManager;
	}
	
		@Lazy(true)
	 	 @Bean 
	 	public ShardedJedisPool getShardedJedisPool(){
	 		Cluster cluster=redisProperties.getCluster();
	 		List<String> nodes=Lists.newArrayList();
	 		if(cluster!=null){
	 			nodes=redisProperties.getCluster().getNodes();
	 		}
	 		List<JedisShardInfo> shards=Lists.newArrayList();
	 		
	 		for(int i=0;i<nodes.size();i++){
	 			String node=nodes.get(i);
	 			String host=ZStrUtil.substringBefore(node, ":");
	 			int port = NumberUtils.toInt(ZStrUtil.substringAfter(node, ":"));
	 			JedisShardInfo shard=new JedisShardInfo(host, port, "instance:0"+i);
	 			shards.add(shard);
	 		}
	 		ShardedJedisPool shardedJedisPool=  new ShardedJedisPool(getConnectionFactory().getPoolConfig(),shards);
	 		return shardedJedisPool;
	 	}
	 	 
	 	 
		 @Lazy(true)
	 	 @ConfigurationProperties(prefix="spring.redis")  
	 	 @Bean  
	     public JedisConnectionFactory getConnectionFactory(){  
	         JedisConnectionFactory factory = new JedisConnectionFactory();  
	         return factory;  
	     }
	 	 
	 	 
	    
	 /*	 @Bean  
	 	  public RedisTemplate<?, ?> getRedisTemplate(){  
	 	     RedisTemplate<?, ?> template = new StringRedisTemplate(getConnectionFactory());  
	 	     return template;  
	 	  }  */
	    @SuppressWarnings("rawtypes")
		@Bean
		@Lazy(true)
	    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
	        RedisTemplate<String, Object> template = new RedisTemplate<String,Object>();
	        template.setConnectionFactory(getConnectionFactory());
	        template.setKeySerializer(new RedisCustomSerializer<String>(snowyConfigProperties.getSerializeType()));
	        template.setValueSerializer(new RedisCustomSerializer(snowyConfigProperties.getSerializeType()));
	        return template;
	    }
}
