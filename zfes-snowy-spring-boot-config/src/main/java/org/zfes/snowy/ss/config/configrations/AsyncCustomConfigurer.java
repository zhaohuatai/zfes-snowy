/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */

package org.zfes.snowy.ss.config.configrations;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//通过@Async注解表明该方法是一个异步方法，如果注解在类级别，则表明该类所有的方法都是异步方法，而这里的方法自动被注入使用ThreadPoolTaskExecutor作为TaskExecutor
//@Configuration
public class AsyncCustomConfigurer implements AsyncConfigurer{
	@Value("${async.corePoolSize:1}")
	private int corePoolSize;
	@Value("${async.maxPoolSize:30}")
	private int maxPoolSize;
	@Value("${async.keepAliveSeconds:60}")
	private int keepAliveSeconds;
	@Value("${async.queueCapacity:100}")
	private int queueCapacity;
	@Value("${async.allowCoreThreadTimeOut:false}")
	private boolean allowCoreThreadTimeOut ;
	@Value("${async.threadNamePrefix:ZfesExecutor-}")
	private String threadNamePrefix;
	    @Override
	    public Executor getAsyncExecutor() {//实现AsyncConfigurer接口并重写getAsyncExecutor方法，并返回一个ThreadPoolTaskExecutor，这样我们就获得了一个基于线程池TaskExecutor
		  
	    	final  ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
	            taskExecutor.setCorePoolSize(corePoolSize);
	            taskExecutor.setMaxPoolSize(maxPoolSize);
	            taskExecutor.setQueueCapacity(queueCapacity);
	            taskExecutor.setKeepAliveSeconds(keepAliveSeconds);
	            taskExecutor.setAllowCoreThreadTimeOut(allowCoreThreadTimeOut);
	            
	            taskExecutor.setThreadNamePrefix(threadNamePrefix);
	            taskExecutor.initialize();
	            return taskExecutor;
	    }
	    @Override
	    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
	    	  return new AsyncCustomUncaughtExceptionHandler();
	    }
	    

}
