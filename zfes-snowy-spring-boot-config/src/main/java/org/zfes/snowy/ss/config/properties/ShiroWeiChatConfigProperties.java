/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Component
@ConfigurationProperties(prefix = "shiro.weichat")
public class ShiroWeiChatConfigProperties  {
	
	private @Value("${weichat.weiChatAuthCallbackUrl:/api/auth/weichat/auth/callback}") String weiChatAuthCallbackUrl;//
	private @Value("${weiChatAppid:}") String weiChatAppid;//
	private @Value("${weiChatSecret:}") String weiChatSecret;//
	
	
	public String getWeiChatAuthCallbackUrl() {
		return weiChatAuthCallbackUrl;
	}
	public void setWeiChatAuthCallbackUrl(String weiChatAuthCallbackUrl) {
		this.weiChatAuthCallbackUrl = weiChatAuthCallbackUrl;
	}
	public String getWeiChatAppid() {
		return weiChatAppid;
	}
	public void setWeiChatAppid(String weiChatAppid) {
		this.weiChatAppid = weiChatAppid;
	}
	public String getWeiChatSecret() {
		return weiChatSecret;
	}
	public void setWeiChatSecret(String weiChatSecret) {
		this.weiChatSecret = weiChatSecret;
	}
	
	
}
