/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;

import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.zfes.snowy.base.web.resolver.ArrayListResolver;
import org.zfes.snowy.base.web.resolver.ArrayResolver;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter{
	@Autowired
	FastJsonConfigration fastJsonConfigration;
	
    @Override  
    public void addResourceHandlers(ResourceHandlerRegistry registry) {  
      //  registry.addResourceHandler("/images/**").addResourceLocations("D:\\\\files\\upload\\"); 
    	//registry.addResourceHandler("/**").addResourceLocations("classpath:/view/"); 
        super.addResourceHandlers(registry);
    }  
    
	@Override 
	public void configurePathMatch(PathMatchConfigurer configurer) {
	    configurer.setUseSuffixPatternMatch(false).//设计人员希望系统对外暴露的URL不会识别和匹配.*后缀。在这个例子中，就意味着Spring会将9781-1234-1111.1当做一个{isbn}参数传给BookController。
	            setUseTrailingSlashMatch(true);//表示系统不区分URL的最后一个字符是否是斜杠/。在这个例子中，就意味着http://localhost:8080/books/9781-1234-1111和http://localhost:8080/books/9781-1234-1111/含义相同。
	}
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

       // registry.addInterceptor(null).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    
    @Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    	   configurer
    	   .favorPathExtension(false)//true，开启扩展名支持，false关闭支持
    	   .useJaf(false)  
           .favorParameter(true)//用于开启 /userinfo/123?format=json的支持
           .parameterName("mediaType")  
           .ignoreAcceptHeader(true)//设置为true以忽略对Accept Header的支持
           .defaultContentType(MediaType.APPLICATION_JSON) //在没有扩展名和参数时即: "/user/1" 时的默认展现形式
           .mediaType("json", MediaType.APPLICATION_JSON_UTF8)
           .mediaType("xml", MediaType.APPLICATION_XML)
           .mediaType("text/html", MediaType.TEXT_HTML);
	}
//    <mvc:message-converters register-defaults="true">
//    <bean class="org.springframework.http.converter.StringHttpMessageConverter">
//    <property name="supportedMediaTypes" value="text/html;charset=UTF-8"/>
//    </bean>
//    </mvc:message-converters>
//    </mvc:annotation-driven>
    
    @Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
    	registry.enableContentNegotiation(new FastJsonJsonView());
	}
    /**
     * 异步配置
     */
    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        configurer.setTaskExecutor(new ConcurrentTaskExecutor(Executors.newFixedThreadPool(5)));
    }
    /**
     * 跨域配置
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
       // registry.addMapping("/api/**").allowedOrigins("http://localhost:8080");
    }
    
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new ArrayListResolver());
		argumentResolvers.add(new ArrayResolver());
	}

	/**
	 * 采用fastjson 
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
	     converters.add(fastJsonConfigration.fastJsonHttpMessageConverter());
	     
	     StringHttpMessageConverter stringConverter= new StringHttpMessageConverter();
	        stringConverter.setDefaultCharset(Charset.forName("UTF-8"));
	        converters.add(stringConverter);
	        
	}
}
