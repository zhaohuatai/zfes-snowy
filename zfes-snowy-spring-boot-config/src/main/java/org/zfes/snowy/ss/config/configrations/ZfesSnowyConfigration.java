/**
 * Copyright (c) 2016-2020 https://github.com/zhaohuatai
 *
 * contact 824069438@qq.com
 *  
 */
package org.zfes.snowy.ss.config.configrations;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zfes.snowy.core.util.AppCtxUtil;
@Configuration
public class ZfesSnowyConfigration {
	@Bean
	//@Order(-1)
	public AppCtxUtil appCtxUtil() {
		AppCtxUtil appCtxUtil=new AppCtxUtil();
//		if(!AppCtxUtil.getAppKey().isPresent()){
//			throw new AppRuntimeException("spring.application.name 未正确设置 【APPKEY】");
//		}
		return appCtxUtil;
	}
	
}
